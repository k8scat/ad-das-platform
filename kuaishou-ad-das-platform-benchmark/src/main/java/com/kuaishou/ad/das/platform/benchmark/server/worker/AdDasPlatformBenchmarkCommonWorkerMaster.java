package com.kuaishou.ad.das.platform.benchmark.server.worker;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import com.kuaishou.ad.das.platform.core.benchmark.server.AdDasBenchmarkAbstractWorkerMaster;
import com.kuaishou.framework.rpc.server.WarmUp;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-07-04
 */
@Lazy
@Service
public class AdDasPlatformBenchmarkCommonWorkerMaster
        extends AdDasBenchmarkAbstractWorkerMaster
        implements WarmUp {


    @Override
    public void warmup() throws Exception {

    }
}