package com.kuaishou.ad.das.platform.benchmark.server.rpc

import com.kuaishou.framework.rpc.config.RpcConfig
import com.kuaishou.infra.grpc.constant.RpcStubHolder
import com.kuaishou.protobuf.ad.das.platform.AdDasBenchmarkCommonServiceGrpc
import com.kuaishou.protobuf.ad.das.platform.AdDasBenchmarkCoreServiceGrpc
import com.kuaishou.protobuf.ad.das.platform.AdDasBenchmarkServiceGrpc
import kuaishou.common.BizDef

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-07-03
 */
@Suppress("EnumEntryName")
enum class AdDasPlatformBenchmarkRpcConfig(private val port: Int,
                                           private val grpcClass: Class<out RpcStubHolder>,
                                           private val qualifier: String? = null,
                                           private val supportGray: Boolean = false,
                                           private val supportDebug: Boolean = true,
                                           private val usingKess: Boolean = true) : RpcConfig {

    adDasPlatformBenchmarkBaseWorker(20789, AdDasBenchmarkServiceGrpc::class.java),
    adDasPlatformBenchmarkCoreWorker(20790, AdDasBenchmarkCoreServiceGrpc::class.java),
    adDasPlatformBenchmarkCommonWorker(20791, AdDasBenchmarkCommonServiceGrpc::class.java);

    override fun grpcClass(): Class<out RpcStubHolder>? {
        return grpcClass
    }

    override fun bizName(): String {
        return name
    }

    override fun qualifier(): String? {
        return qualifier
    }

    override fun port(): Int {
        return port
    }

    override fun usingNewZk(): Boolean {
        return true
    }

    override fun supportDebug(): Boolean {
        return supportDebug
    }

    override fun supportGray(): Boolean {
        return supportGray
    }

    override fun usingKess(): Boolean {
        return usingKess
    }

    override fun bizDef(): BizDef {
        return BizDef.AD_DSP
    }
}
