package com.kuaishou.ad.das.platform.benchmark.server.worker;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import com.kuaishou.ad.das.platform.core.benchmark.server.watcher.AdDasBenchmarkBenchmarkWorkerWatcher;
import com.kuaishou.framework.rpc.server.WarmUp;
import com.kuaishou.protobuf.ad.das.platform.AdDasBenchmarkServiceGrpc;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-06-01
 */
@Lazy
@Service
public class AdDasBenchmarkBaseWorker
        extends AdDasBenchmarkServiceGrpc.AdDasBenchmarkServiceImplBase
        implements WarmUp {

    @Autowired
    private AdDasBenchmarkBenchmarkWorkerWatcher adBenchmarkWatcher;

    @Override
    public void warmup() throws Exception {

    }
}
