package com.kuaishou.ad.das.platform.benchmark.server.worker;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import com.kuaishou.ad.das.platform.core.benchmark.server.watcher.AdDasBenchmarkBenchmarkWorkerWatcher;
import com.kuaishou.protobuf.ad.das.platform.AdDasBenchmarkCoreServiceGrpc;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-06-01
 */
@Lazy
@Service
public class AdDasPlatformBenchmarkCoreWorker extends AdDasBenchmarkCoreServiceGrpc.AdDasBenchmarkCoreServiceImplBase {

    @Autowired
    private AdDasBenchmarkBenchmarkWorkerWatcher adBenchmarkWatcher;
}
