package com.kuaishou.ad.das.platform.utils.dasenum;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-01-19
 */
@Getter
@AllArgsConstructor
public enum AdDasModifyPublishStatusEnum {

    WAITING(-1, "未开始"),
    PUBLISHING(0, "发布中"),
    PUBLISH_SUC(1, "发布成功"),
    PUBLISH_FAILED(2, "发布异常");

    private Integer code;
    private String desc;

}
