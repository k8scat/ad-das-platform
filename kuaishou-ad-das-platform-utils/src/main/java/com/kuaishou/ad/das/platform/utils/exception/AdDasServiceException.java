package com.kuaishou.ad.das.platform.utils.exception;

import lombok.Getter;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-12-29
 */
@Getter
public class AdDasServiceException extends RuntimeException {

    private int code;
    private String errorMsg;
    private String errorUrl;

    public AdDasServiceException(int code, String errorMsg) {
        super(errorMsg);
        this.code = code;
        this.errorMsg = errorMsg;
        this.errorUrl = null;
    }

    private AdDasServiceException(int code, String errorMsg, String errorUrl) {
        super(errorMsg);
        this.code = code;
        this.errorMsg = errorMsg;
        this.errorUrl = errorUrl;
    }

    public static AdDasServiceException ofMessage(int code, String errorMsg) {

        return new AdDasServiceException(code, errorMsg);
    }

    public static AdDasServiceException ofMessage(ErrorCode errorCode) {

        return new AdDasServiceException(errorCode.getCode(), errorCode.getMsg());
    }
}

