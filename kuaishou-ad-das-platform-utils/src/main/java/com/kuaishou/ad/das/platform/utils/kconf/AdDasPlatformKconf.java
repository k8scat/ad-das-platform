package com.kuaishou.ad.das.platform.utils.kconf;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.compress.utils.Lists;

import com.kuaishou.ad.das.platform.utils.model.kconf.AdDasBenchamrkTaskIdMapModel;
import com.kuaishou.ad.das.platform.utils.model.kconf.AdDasBenchmarkHdfsPathMapModel;
import com.kuaishou.ad.das.platform.utils.model.kconf.AdDasBenchmarkTaskMapModel;
import com.kuaishou.kconf.client.Kconf;
import com.kuaishou.kconf.client.Kconfs;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-05-31
 */
public class AdDasPlatformKconf {

    public static final Map<String, Integer> ZK_NAMESPACE_CONFIG_MAP = new HashMap<String, Integer>() {
        {
            put("chenjiaxiong", 1);
            put("niushaofeng", 1);
            put("guochaochao", 3);
            put("gaojunke", 3);
            put("liluxi", 3);
            put("shihonghua", 3);
            put("zhangxudong03", 3);
            put("wangruining", 3);
            put("lanxiaobin", 3);
        }
    };

    public static final Map<String, Integer> ZK_NAMESPACE_INCREMENT_BACKTRACK_MAP = new HashMap<String, Integer>() {
        {
            put("chenjiaxiong", 1);
            put("caoyinhang", 1);
            put("niushaofeng", 1);
            put("guochaochao", 3);
            put("gaojunke", 3);
            put("liluxi", 3);
            put("shihonghua", 3);
            put("zhangxudong03", 3);
            put("wangruining", 3);
            put("lanxiaobin", 3);
        }
    };

    public static Kconf<Map<String, Integer>> adDasPlatformUserMsgs = Kconfs.ofIntegerMap(
            "ad.das.adDasPlatformUserMsgs", ZK_NAMESPACE_CONFIG_MAP).build();

    //增量回溯权限验证
    public static Kconf<Map<String, Integer>> adDasPlatformBacktrackUserMsgs = Kconfs.ofIntegerMap(
            "ad.das.adDasPlatformBacktrackUserMsgs", ZK_NAMESPACE_INCREMENT_BACKTRACK_MAP).build();

    public static final Kconf<Map<String, String>> AD_DAS_DATASOURCE_DB_MAPPING =
            Kconfs.ofStringMap("ad.das.adDasDataSource2DbMapping", null).build();

    /**
     * 基准多环境 任务配置 =========================================================================
     */
    public static final Kconf<AdDasBenchmarkTaskMapModel> AD_DAS_BENCHMARK_TASK_MAP_MODEL_KCONF =
            Kconfs.ofJson("ad.das.adDasBenchmarkTaskMapModel",
                    AdDasBenchmarkTaskMapModel.getDefault(), AdDasBenchmarkTaskMapModel.class)
                    .build();

    public static final Kconf<AdDasBenchamrkTaskIdMapModel> AD_DAS_BENCHMARK_TASK_ID_MAP_MODEL_KCONF =
            Kconfs.ofJson("ad.das.adDasBenchmarkTaskIdMapModel",
                    AdDasBenchamrkTaskIdMapModel.getDefault(), AdDasBenchamrkTaskIdMapModel.class)
                    .build();

    public static final Kconf<AdDasBenchmarkHdfsPathMapModel> AD_DAS_BENCHMARK_HDFS_PATH_MAP_MODEL_KCONF =
            Kconfs.ofJson("ad.adcore.adDasBenchmarkHdfsPathMapModel",
                    AdDasBenchmarkHdfsPathMapModel.getDefault(), AdDasBenchmarkHdfsPathMapModel.class)
                    .build();

    /**
     * 从cache 中reload class
     */
    public static Kconf<Boolean> adDasPlatformLoadClassFromCache =
            Kconfs.ofBoolean("ad.adcore.adDasPlatformLoadClassFromCache", true)
                    .build();


    public static final Kconf<Integer> AD_DAS_INCREMENT_BATCH_FILLING_THREAD_COUNT =
            Kconfs.ofInteger("ad.das.adDasIncrementBatchFillingThreadCount", 200).build();

    public static final Kconf<Long> AD_DAS_PLATFORM_BENCHMARK_TIME_INTERVAL =
            Kconfs.ofLong("ad.das.adDasPlatformBenchmarkTimeInterval", 1).build();

    public static final Kconf<Long> AD_DAS_PLATFORM_BENCHMARK_MAX_COUNT =
            Kconfs.ofLong("ad.das.adDasPlatformBenchmarkMaxCount", 5).build();

    public static final Kconf<Long> AD_DAS_PLATFORM_BENCHMARK_NET_PERIOD =
            Kconfs.ofLong("ad.das.adDasPlatformBenchmarkNetPeriod", 1).build();

    public static final Kconf<Map<String, List<String>>> DAS_ZOOK_KEEPER =
            Kconfs.ofListMap("ad.das.adDasPlatformPrivateZK", Collections
                    .emptyMap(), String.class, String.class)
                    .build();

    public static final Kconf<Map<String, String>> DAS_PLATFORM_ZK_NAME_SPACE =
            Kconfs.ofStringMap("ad.das.adDasPlatformZkNameSpace", Collections
                    .emptyMap())
                    .build();

    public static final Kconf<Map<String, String>> DAS_BAENCHMARK_COMMON_PATH =
            Kconfs.ofStringMap("ad.das.adDasPlatformBenchmarkCommonPath", Collections
                    .emptyMap())
                    .build();

    public static final Kconf<Map<String, String>> DAS_BAENCHMARK_CLIENT_PATH =
            Kconfs.ofStringMap("ad.das.adDasPlatformBenchmarkClientPath", Collections
                    .emptyMap())
                    .build();

    public static Kconf<Map<String, String>> adDasPlatformDataAdapterMap =
            Kconfs.ofStringMap("ad.das.adDasPlatformDataAdapterMap", Collections
                    .emptyMap())
                    .build();

    public static Kconf<Map<String, Integer>> adDasPlatformClientRunStartConfig =
            Kconfs.ofIntegerMap("ad.das.adDasPlatformClientRunStartConfig", Collections
                    .emptyMap())
                    .build();

    /**
     * 基准 worker 服务 状态配置
     * 1. 升级状态 - 2
     * 2. 服务状态 - 1
     */
    public static Kconf<Map<String, Integer>> adDasPlatformWorkerStatusConfig =
            Kconfs.ofIntegerMap("ad.das.adDasPlatformWorkerStatusConfig", Collections
                    .emptyMap())
                    .build();

    /**
     * client 服务定时 规则配置
     * {"adCoreClient": "1-1"}
     */
    public static Kconf<Map<String, String>> adDasPlatformClientCronConfig =
            Kconfs.ofStringMap("ad.das.adDasPlatformClientCronConfig", Collections
                    .emptyMap())
                    .build();

    public static Kconf<Map<String, String>> adDasIncrementDataSourceShardKeyConfig =
            Kconfs.ofStringMap("ad.das.adDasIncrementDataSourceShardKeyConfig", Collections
                    .emptyMap())
                    .build();

    public static Kconf<String> adDasPlatformProtoEnumVersionConfig = Kconfs
            .ofString("ad.das.adDasPlatformProtoEnumVersionConfig", "kuaishou-ad-reco-base-proto-1.0.99.jar")
            .build();

    public static Kconf<String> adDasPlatformProtoCommonVersionConfig = Kconfs
            .ofString("ad.das.adDasPlatformProtoCommonVersionConfig", "kuaishou-ad-new-common-proto-1.0.99.jar")
            .build();

    public static final Kconf<Integer> AD_DAS_FINANCE_BATCH_QUERY_ACCOUNTID_SIZE =
            Kconfs.ofInteger("ad.das.adDasfinanceBatchQueryAccountIdSize", 2000).build();

    public static Kconf<String> adDasPlatformOncall = Kconfs
            .ofString("ad.das.adDasPlatformOncall", "chenjiaxiong")
            .build();

    public static Kconf<List<String>> adDasPlatformChangLogs = Kconfs
            .ofStringList("ad.das.adDasPlatformChangLogs", Lists.newArrayList())
            .build();

    public static Kconf<Long> adDasPlatformGitPipelineId = Kconfs
            .ofLong("ad.das.adDasPlatformGitPipelineId", 594079L)
            .build();

}

