package com.kuaishou.ad.das.platform.utils;

import java.io.IOException;
import java.util.Map;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;

import groovy.util.logging.Slf4j;

/**
 * @author caoyinhang <caoyinhang@kuaishou.com>
 * Created on 2021-07-08
 */
@Slf4j
public class AdDasHttpUtils {

    public static String doPost(String url, Map<String, String> headerMap, Map<String, Object> params)
            throws IOException {

        HttpClient httpClient = new HttpClient();
        PostMethod post = new PostMethod(url);

        headerMap.forEach((key, value) -> {
            post.addRequestHeader(key,value);
        });

        httpClient.executeMethod(post);
        String info=new String(post.getResponseBody(),"utf-8");
        return info;
    }
}
