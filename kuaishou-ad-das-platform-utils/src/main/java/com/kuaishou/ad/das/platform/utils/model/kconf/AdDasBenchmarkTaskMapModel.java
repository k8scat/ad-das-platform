package com.kuaishou.ad.das.platform.utils.model.kconf;

import java.io.Serializable;
import java.util.Map;

import com.google.common.collect.ImmutableMap;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-03-08
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AdDasBenchmarkTaskMapModel implements Serializable {

    private static final long serialVersionUID = -4179730585094484620L;

    private Map<String, Map<String, String>> taskMapData;

    private static final AdDasBenchmarkTaskMapModel DEFAULT = AdDasBenchmarkTaskMapModel
            .builder().taskMapData(ImmutableMap.of()).build();

    public static AdDasBenchmarkTaskMapModel getDefault() {
        return DEFAULT;
    }

    public Map<String, String> getTaskMap(String nodeEnv) {
        return taskMapData.getOrDefault(nodeEnv, null);
    }

    public boolean judgeNodeEnvConfig(String nodeEnv) {
        return taskMapData.containsKey(nodeEnv);
    }
}
