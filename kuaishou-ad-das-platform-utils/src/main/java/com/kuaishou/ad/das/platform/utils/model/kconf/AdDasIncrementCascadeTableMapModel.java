package com.kuaishou.ad.das.platform.utils.model.kconf;

import java.io.Serializable;
import java.util.Map;
import java.util.Set;

import com.google.common.collect.ImmutableMap;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-03-28
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AdDasIncrementCascadeTableMapModel implements Serializable {
    private static final long serialVersionUID = 6730232564547463753L;

    private Map<String, Map<String, Set<String>>> cascadeTableMapData;

    private static final AdDasIncrementCascadeTableMapModel DEFAULT = AdDasIncrementCascadeTableMapModel
            .builder().cascadeTableMapData(ImmutableMap.of()).build();

    public static AdDasIncrementCascadeTableMapModel getDefault() {
        return DEFAULT;
    }

    public Map<String, Set<String>> getCascadeTableMap(String serviceType) {
        return cascadeTableMapData.getOrDefault(serviceType, null);
    }

    public boolean judgeServiceTypeConfig(String serviceType) {
        return cascadeTableMapData.containsKey(serviceType);
    }

}
