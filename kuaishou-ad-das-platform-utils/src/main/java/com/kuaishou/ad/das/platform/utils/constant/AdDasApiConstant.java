package com.kuaishou.ad.das.platform.utils.constant;

import java.util.concurrent.TimeUnit;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-05-31
 */
public class AdDasApiConstant {

    /**
     * 登出状态标记
     */
    public static final String CAS_LOGOUT_MARK = "LOGOUT";
    public static final long CAS_TOKEN_VALID_TIME = TimeUnit.MINUTES.toMillis(360);
    /**
     * cookie 有效时间
     */
    public static final int CAS_TOKEN_EXPIRE_TIME = -1;

    public static final String DAS_API_BENCHMARK_PATH = "/das/platform/api/benchmark";

    public static final String DAS_PLATFORM_API_PATH = "/das/platform/api";

    public static final String DAS_API_PATH = "/das/api";

    /**
     * 数据流组接口统一前缀
     */
    public static final String DAS_API_DATA_STREAM_GROUP_PATH = "/das/api/dataSteamGroup";
    /**
     * 编码方式
     */
    public static final String CHARACTER_ENCODING = "UTF-8";

    /**
     * 增量 clickUrl
     */
    public static final String DAS_INCREMENT_CLICKURL = "#/increTableUpdate?modifyId=%s&modifyType=3";

    /**
     * 基准 clickUrl
     */
    public static final String DAS_BENCHMARK_CLICKURL = "#/benchmarkChangeNew?modifyId=%s&modifyType=2";
}
