package com.kuaishou.ad.das.platform.utils.dasenum;

import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;


import lombok.Getter;

/**
 * FIXME  后期改为ZK 配置
 *
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-12-08
 */
@Getter
public enum AdDasServiceEnum {

    AD_DAS_API("ad.das.api", 1),
    AD_DAS_PLATFORM_API("ad.das.platform.api", 1),
    AD_DAS_BASE("ad.das.base", 1),
    AD_DAS_INCREMENT("ad.das.increment", 1),

    AD_DAS_PLATFORM_MODIFY_SERVICE("ad.das.platform.modify.service", 1),

    ;

    private String name;

    private Integer modifyType;

    AdDasServiceEnum(String name, Integer modifyType) {
        this.name = name;
        this.modifyType = modifyType;
    }

    public static Set<String> getServiceEnumSet() {
        return Arrays.stream(AdDasServiceEnum.values())
                .map(enumValue -> enumValue.getName())
                .collect(Collectors.toSet());
    }
}
