package com.kuaishou.ad.das.platform.utils.dasenum;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-07-10
 */
public enum  AdDasBenchmarkStatusEnum {

    SUCESS_STATUS(0, "sucess"),

    RESOURCE_ERROR(1, "资源定义错误"),
    MISS_HANDLER_ERROR(2, "资源对应的handler未找到"),
    QUERY_DATA_ERROR(3, "取mysql中数失败"),
    WRITE_DATA_2_HDFS_ERROR(4, "将数据写入HDFS失败"),
    QUERY_MERGE_DATA_ERROR(5, "跨库取数失败"),
    CONVERT_DATA_ERROR(6, "转换数据失败"),

    RUNTIME_ERROR(100, "slave运行错误"),
    RUNTIME_PARSE_ERROR(101, "slave解析资源信息错误"),
    RUNTIME_SLAVE_ERROR(102, "slave运行严重错误, 可能发生OOM, 请关注"),

    ;

    private int status;

    private String reason;

    AdDasBenchmarkStatusEnum(int status, String reason) {
        this.status = status;
        this.reason = reason;
    }

    public int getStatus() {
        return status;
    }

    public String getReason() {
        return reason;
    }
}
