package com.kuaishou.ad.das.platform.utils.model.kconf;

import java.io.Serializable;
import java.util.Map;
import java.util.Set;

import lombok.Data;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-01-09
 */
@Data
public class AdDasIncrementFillingTable implements Serializable {

    private static final long serialVersionUID = -4844600358062048614L;


    /**
     *   {
     *     "CREATIVE": {
     *         "ad_dsp_creative": ["ad_dsp_creative_support_info"],
     *         "ad_dsp_creative_support_info": ["ad_dsp_creative","ad_dsp_creative_advanced_program"],
     *         "ad_dsp_creative_advanced_program": ["ad_dsp_creative_support_info"]
     *     },
     *     "UNIT": {
     *         "ad_dsp_unit": ["ad_dsp_unit_support_info"]
     *     }
     *   }
     */
    private Map<String, Map<String, Set<String>>> fillingTable;

    public boolean judgeLevel(String level) {

        return this.fillingTable.containsKey(level);
    }

    public Map<String, Set<String>> getLevelMap(String level) {
        return this.fillingTable.get(level);
    }

    public boolean judgeLevelTable(String level, String tableName) {

        return this.fillingTable.containsKey(level) && fillingTable.get(level).containsKey(tableName);
    }

    public Set<String> getLevelTableNameSet(String level, String tableName) {

        if (judgeLevelTable(level, tableName)) {
            return this.fillingTable.get(level).get(tableName);
        }
        return null;
    }
}
