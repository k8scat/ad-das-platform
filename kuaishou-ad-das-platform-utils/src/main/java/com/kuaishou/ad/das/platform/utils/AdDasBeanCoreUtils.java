package com.kuaishou.ad.das.platform.utils;

import static com.google.common.base.Predicates.not;
import static java.util.stream.Collectors.toSet;

import java.beans.FeatureDescriptor;
import java.beans.PropertyDescriptor;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Stream;

import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;

import com.google.common.base.Preconditions;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-08-10
 */
public class AdDasBeanCoreUtils {

    private static ConcurrentHashMap<Class<?>, Set<PropertyDescriptor>> class2Properties = new ConcurrentHashMap<>();

    public static Collection<String> getAllProperties(Class<?> targetClass) {
        Set<PropertyDescriptor> propertyDescriptors = class2Properties.computeIfAbsent(targetClass, classValue ->
                Arrays.stream(BeanUtilsBean.getInstance()
                        .getPropertyUtils().getPropertyDescriptors(classValue))
                        .filter(propertyDescriptor -> !StringUtils
                                .equalsIgnoreCase(propertyDescriptor.getName(), Class.class.getSimpleName()))
                        .collect(toSet())
        );
        return propertyDescriptors.stream().map(PropertyDescriptor::getName).collect(toSet());
    }


    private static String[] getNullPropertyNames(Object source) {
        Preconditions.checkNotNull(source, "%s should not be null", source);
        final BeanWrapper wrappedSource = new BeanWrapperImpl(source);
        return Stream.of(wrappedSource.getPropertyDescriptors())
                .map(FeatureDescriptor::getName)
                .filter(propertyName -> wrappedSource.getPropertyValue(propertyName) == null)
                .toArray(String[]::new);
    }

    public static String[] getNotNullPropertyNames(Object source) {
        Preconditions.checkNotNull(source, "%s should not be null", source);
        final BeanWrapper wrappedSource = new BeanWrapperImpl(source);
        return Stream.of(wrappedSource.getPropertyDescriptors())
                .map(FeatureDescriptor::getName)
                .filter(propertyName -> wrappedSource.getPropertyValue(propertyName) != null)
                .toArray(String[]::new);
    }

    private static String[] getIgnorePropertyNames(Object source, List<String> properties) {
        final BeanWrapper wrappedSource = new BeanWrapperImpl(source);
        return Stream.of(wrappedSource.getPropertyDescriptors())
                .map(FeatureDescriptor::getName)
                .filter(not(properties::contains))
                .toArray(String[]::new);
    }

    // copy properties but ignore null value
    // so that default value in desc will persist
    public static void copyPropertiesIgnoreNull(Object desc, Object source) {
        String[] nullProperties = getNullPropertyNames(source);
        BeanUtils.copyProperties(source, desc, nullProperties);
    }

    public static void copyPropertiesInBound(Object desc, Object source, List<String> properties) {
        String[] ignoreProperties = getIgnorePropertyNames(desc, properties);
        BeanUtils.copyProperties(source, desc, ignoreProperties);
    }

    public static <T> List<T> paging(List<T> dataList, Integer pageNum, Integer pageSize) {
        if (pageNum == null || pageSize == null || pageSize <= 0 || pageNum <= 0) {
            return dataList;
        }
        int lowerBound = Math.min((pageNum - 1) * pageSize, dataList.size());
        int upperBound = Math.min(lowerBound + pageSize, dataList.size());
        return dataList.subList(lowerBound, upperBound);
    }

}
