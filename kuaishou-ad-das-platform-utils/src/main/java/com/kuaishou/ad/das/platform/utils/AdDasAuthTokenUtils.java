package com.kuaishou.ad.das.platform.utils;

import static com.kuaishou.ad.das.platform.utils.DateTimeUtils.getCurHourTimestamp;

import java.security.Key;
import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import javax.crypto.spec.IvParameterSpec;

import com.kuaishou.framework.util.HostInfo;
import com.kuaishou.old.exception.ServiceException;

import lombok.extern.slf4j.Slf4j;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-12-15
 */
@Slf4j
public class AdDasAuthTokenUtils {

    /**
     * 偏移变量，固定占8位字节
     */
    private static final String IV_PARAMETER = "12345678";
    /**
     * 密钥算法
     */
    private static final String ALGORITHM = "DES";
    /**
     * 加密/解密算法-工作模式-填充模式
     */
    private static final String CIPHER_ALGORITHM = "DES/CBC/PKCS5Padding";
    /**
     * 默认编码
     */
    private static final String CHARSET = "utf-8";


    private static final String AD_DAS_PASSWORD = "AD-DAS-KEY-PASSWORD";

    /**
     * 生成key
     */
    private static Key generateKey(String password) throws Exception {
        DESKeySpec dks = new DESKeySpec(password.getBytes(CHARSET));
        SecretKeyFactory keyFactory = SecretKeyFactory.getInstance(ALGORITHM);
        return keyFactory.generateSecret(dks);
    }


    /**
     * DES加密字符串
     *
     * @param password 加密密码，长度不能够小于8位
     * @param data 待加密字符串
     * @return 加密后内容
     */
    public static String encrypt(String password, String data) {
        if (password == null || password.length() < 8) {
            throw new RuntimeException("加密失败，key不能小于8位");
        }

        if (data == null) {
            return null;
        }
        try {
            Key secretKey = generateKey(password);
            Cipher cipher = Cipher.getInstance(CIPHER_ALGORITHM);
            IvParameterSpec iv = new IvParameterSpec(IV_PARAMETER.getBytes(CHARSET));
            cipher.init(Cipher.ENCRYPT_MODE, secretKey, iv);
            byte[] bytes = cipher.doFinal(data.getBytes(CHARSET));

            return new String(Base64.getEncoder().encode(bytes));

        } catch (Exception e) {
            e.printStackTrace();
            return data;
        }
    }

    /**
     * DES解密字符串
     *
     * @param password 解密密码，长度不能够小于8位
     * @param data 待解密字符串
     * @return 解密后内容
     */
    public static String decrypt(String password, String data) {
        if (password == null || password.length() < 8) {
            throw new RuntimeException("加密失败，key不能小于8位");
        }
        if (data == null) {
            return null;
        }
        try {
            Key secretKey = generateKey(password);
            Cipher cipher = Cipher.getInstance(CIPHER_ALGORITHM);
            IvParameterSpec iv = new IvParameterSpec(IV_PARAMETER.getBytes(CHARSET));
            cipher.init(Cipher.DECRYPT_MODE, secretKey, iv);
            return new String(cipher.doFinal(Base64.getDecoder().decode(data.getBytes(CHARSET))), CHARSET);
        } catch (Exception e) {
            e.printStackTrace();
            return data;
        }
    }

    /**
     * 生成当前token
     */
    public static String generateCurToken(String token) {

        String tokenContent = token + "-" + getCurHourTimestamp();
        if (HostInfo.debugHost()) {
            log.info("generateCurToken() tokenContent={}", tokenContent);
        }

        String encrypts = encrypt(AD_DAS_PASSWORD, tokenContent);

        if (HostInfo.debugHost()) {
            log.info("generateCurToken() encrypts={}", encrypts);
        }

        return encrypts;
    }

    /**
     * 校验token
     */
    public static String validateToken(String dasToken) throws Exception {

        String dasTokenContent = decrypt(AD_DAS_PASSWORD, dasToken);

        log.info("validateToken() dasToken={}, dasTokenContent={}",
                dasToken, dasTokenContent);

        int index = dasTokenContent.lastIndexOf("-");

        String dasTokenStr = dasTokenContent.substring(0, index);
        String curTimeStr = dasTokenContent.substring(index + 1);

        if (HostInfo.debugHost()) {
            log.info("validateToken() dasTokenStr={}, curTimeStr={}",
                    dasTokenStr, curTimeStr);
        }

        // check time
        Long curHourTime = getCurHourTimestamp();

        if (curHourTime.longValue() != Long.valueOf(curTimeStr).longValue()) {
            throw ServiceException.ofMessage(2, "dasToken validate timeout");
        }

        return dasTokenStr;
    }


    public static void main(String[] args) {
        String tokenStr = "das_libra";
        String curToken = generateCurToken(tokenStr);

        System.out.println("curToken=" + curToken);

        try {
            String curDecrToken  = validateToken(curToken);

            System.out.println("curDecrToken=" + curDecrToken);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
