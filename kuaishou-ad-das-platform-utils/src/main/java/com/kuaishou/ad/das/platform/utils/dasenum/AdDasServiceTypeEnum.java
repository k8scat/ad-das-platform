package com.kuaishou.ad.das.platform.utils.dasenum;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 数据服务类型配置枚举
 *
 * @author liutao <liutao06@kuaishou.com>
 * Created on 2021-01-04
 */
@Getter
@AllArgsConstructor
public enum AdDasServiceTypeEnum {
    Benchmark(1, "基准变更"),
    Increment(2, "增量变更");
    private final int status;
    private final String name;
}
