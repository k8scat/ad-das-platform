package com.kuaishou.ad.das.platform.utils;

import java.lang.reflect.Method;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Types;

import org.apache.commons.beanutils.MethodUtils;

import com.google.common.base.CaseFormat;

import lombok.extern.slf4j.Slf4j;

/**
 * @author liutao <liutao06@kuaishou.com>
 * Created on 2021-01-05
 */
@Slf4j
public class ModelConvertUtils {

    /**
     * 一个比较捡漏的转换工具类
     *
     * @param rs
     * @param dto
     * @param <T>
     * @throws SQLException
     */
    public static <T> void bindDataToDTO(ResultSet rs, T dto) throws SQLException {
        //取得ResultSet的列名
        ResultSetMetaData rsmd = rs.getMetaData();
        int columnsCount = rsmd.getColumnCount();
        String[] columnNames = new String[columnsCount];
        String[] columnDBNames = new String[columnsCount];
        Integer[] mysqlTypes = new Integer[columnsCount];
        for (int i = 0; i < columnsCount; i++) {
            //添加转换
            columnDBNames[i] = rsmd.getColumnLabel(i + 1);
            columnNames[i] = CaseFormat.LOWER_UNDERSCORE.to(CaseFormat.UPPER_CAMEL, rsmd.getColumnLabel(i + 1));
            mysqlTypes[i] = rsmd.getColumnType(i + 1);
        }
        //遍历ResultSet
        //反射, 从ResultSet绑定到JavaBean
        for (int i = 0; i < columnNames.length; i++) {
            Object value = getDbValue(columnDBNames[i], mysqlTypes[i], rs);
            if (value == null) {
                continue;
            }
            //取得Set方法
            String setMethodName = "set" + columnNames[i];
            try {
                Method method = MethodUtils.getAccessibleMethod(dto.getClass(), setMethodName, value.getClass());
                if (method == null) {
                    continue;
                }
                method.invoke(dto, value);
            } catch (Exception e) {
                log.error("bindDataToDTO error", e);
                throw new RuntimeException("bindDataToDTO error");
            }
        }
        //        log.info("转换后的数据{}", JSONUtil.toJsonStr(dto));
    }

    private static Object getDbValue(String columnName, Integer type, ResultSet rs) throws SQLException {
        switch (type) {
            case Types.CLOB:
            case Types.BLOB:
            case Types.VARCHAR:
                return rs.getString(columnName);
            case Types.TINYINT:
            case Types.INTEGER:
                return rs.getInt(columnName);
            case Types.BIGINT:
                return rs.getLong(columnName);
            default:
                return rs.getObject(columnName);
        }
    }
}
