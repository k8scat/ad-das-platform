package com.kuaishou.ad.das.platform.utils.model.kconf;

import java.io.Serializable;
import java.util.Map;
import java.util.Set;

import com.google.common.collect.ImmutableMap;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-03-26
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AdDasIncrementFilterTableMapModel implements Serializable {

    private static final long serialVersionUID = 8070615364274730982L;

    private Map<String, Map<String, Set<String>>> filterTableMapData;

    private static final AdDasIncrementFilterTableMapModel DEFAULT = AdDasIncrementFilterTableMapModel
            .builder().filterTableMapData(ImmutableMap.of()).build();

    public static AdDasIncrementFilterTableMapModel getDefault() {
        return DEFAULT;
    }

    public Map<String, Set<String>> getFilterTableMap(String serviceEnv) {
        return filterTableMapData.getOrDefault(serviceEnv, null);
    }

    public boolean judgeServiceEnvConfig(String serviceEnv) {
        return filterTableMapData.containsKey(serviceEnv);
    }

    public boolean judgeServiceTypeConfig(String serviceEnv, String serviceType) {

        if (filterTableMapData.containsKey(serviceEnv)) {
            if (filterTableMapData.get(serviceEnv).containsKey(serviceType)) {
                return true;
            }
        }

        return false;
    }

    public boolean judgeTableConfig(String serviceEnv, String serviceType, String tableName) {

        if (filterTableMapData.containsKey(serviceEnv)) {
            if (filterTableMapData.get(serviceEnv).containsKey(serviceType)) {
                if (filterTableMapData.get(serviceEnv).get(serviceType).contains(tableName)) {
                    return true;
                }
            }
        }

        return false;
    }

}
