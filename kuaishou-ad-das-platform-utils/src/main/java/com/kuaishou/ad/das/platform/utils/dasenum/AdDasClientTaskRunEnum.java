package com.kuaishou.ad.das.platform.utils.dasenum;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-08-09
 */
public enum  AdDasClientTaskRunEnum {

    NOT_INIT(-1, "基准任务未开始"),
    RUNNING(0, "运行中"),
    RUN_SUC(1, "运行成功"),
    RUN_FAIL(2, "运行失败"),
    ;

    private int code;

    private String desc;

    AdDasClientTaskRunEnum(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public int getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }

    public static AdDasClientTaskRunEnum ofCode(int code) {
        switch (code) {
            case -1 : return NOT_INIT;
            case 0 : return RUNNING;
            case 1 : return RUN_SUC;
            case 2 : return RUN_FAIL;
            default :
                return null;
        }
    }
}
