package com.kuaishou.ad.das.platform.utils.dasenum;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author liutao <liutao06@kuaishou.com>
 * Created on 2021-01-07
 */
@Getter
@AllArgsConstructor
public enum AdDasIncrementProcessStatusEnum {
    Processing(1, "处理中"),
    NoProcess(2, "空闲中");
    private final int status;
    private final String name;
}
