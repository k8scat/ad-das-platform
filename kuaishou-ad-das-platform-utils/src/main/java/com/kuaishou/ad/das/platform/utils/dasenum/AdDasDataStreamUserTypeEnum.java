package com.kuaishou.ad.das.platform.utils.dasenum;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-06-22
 */
public enum AdDasDataStreamUserTypeEnum {

    OWNER(0, "数据流组owner"),
    READER(1, "只读用户"),
    EIDTER(2, "编辑用户"),
    ADMIN(3, "管理员"),
    USER(4, "使用方用户"),

    ;

    private int code;

    private String desc;

    AdDasDataStreamUserTypeEnum(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public int getCode() {
        return this.code;
    }
}
