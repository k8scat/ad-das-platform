package com.kuaishou.ad.das.platform.utils.dasenum;

import lombok.Getter;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-12-06
 */
@Getter
public enum AdDasModifyStatusEnum {

    ALL(0, "全部"),
    EDITING(1, "编辑中"),
    DELETE(2, "废弃变更"),
    APPROVING(3, "审批中"),
    APPROVED(4, "审批通过"),
    REJECTED(5, "审批驳回"),
    PUBLISH_ING(6, "发布中"),
    PUBLISH_SUC(7, "发布成功"),
    ENDING(8, "发布失败"),
    ROLLBACK_ING(9, "回滚中"),
    ROLLBACK_SUC(10, "回滚成功"),
    TERMINATION(11, "终止"),

    GRAY_PUBLISH_ING(12, "灰度发布中"),
    GRAY_ROLLBACK_ING(13, "灰度回滚中"),

    ;

    private Integer code;

    private String desc;

    AdDasModifyStatusEnum(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }
}
