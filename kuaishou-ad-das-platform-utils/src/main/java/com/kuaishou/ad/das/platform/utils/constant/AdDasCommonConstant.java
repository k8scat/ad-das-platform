package com.kuaishou.ad.das.platform.utils.constant;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.kuaishou.ad.model.protobuf.tables.All;
import com.kuaishou.env.util.EnvUtils;
import com.kuaishou.framework.util.HostInfo;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-05-26
 */
public class AdDasCommonConstant {

    public static final String AD_DAS_COMMON_INCR_LIFE_CYCLE_SUBTAG = "CommonIncrementLifeCycle";

    public static final String COMMON_BENCHMARK_LIFE_CYCLE_SUBTAG = "CommonBenchmarkLifeCycle";

    public static final String COMMON_PLATFORMLIFE_CYCLE_SUBTAG = "CommonPlatformLifeCycle";

    public static final String AD_DAS_NAMESPACE_NEW = HostInfo.debugHost() ? "ad.das.platform.test" : "ad.das.platform";

    public static final String Ad_DAS_BENCHMARK = "benchmark";

    public static final String Ad_DAS_INCREMENT= "increment";

    public static final String Ad_DAS_BENCHMARK_CLIENT = "benchmark_client";

    public static int defaultAdCoreShardCount = EnvUtils.belowTesting() || EnvUtils.isTesting() ? 1 : 10_000;

    public static final Set<String> AD_DAS_TOKEN_SET = new HashSet<String>() {
        {
            add("das_libra");
        }
    };

    public static final Map<String, Set<String>> AD_DAS_TOKEN_MAP = new HashMap<String, Set<String>>() {
        {
            put("dasFinance", AD_DAS_TOKEN_SET);
        }
    };

    public static final Map<String, String> TABLENAME_2_PBCLASSNAME_MAP = new HashMap<>();

    static {
        TABLENAME_2_PBCLASSNAME_MAP.put("ad_dsp_account", All.Account.class.getName());
        TABLENAME_2_PBCLASSNAME_MAP.put("ad_dsp_campaign", All.Campaign.class.getName());
        TABLENAME_2_PBCLASSNAME_MAP.put("ad_dsp_unit", All.Unit.class.getName());
        TABLENAME_2_PBCLASSNAME_MAP.put("ad_dsp_creative", All.Creative.class.getName());
        TABLENAME_2_PBCLASSNAME_MAP.put("ad_dsp_photo", All.PhotoStatus.class.getName());
        TABLENAME_2_PBCLASSNAME_MAP.put("ad_dsp_target", All.Target.class.getName());
    }

    public static final String PB_ENUM_ADINSTANCE_STR = "com.kuaishou.ad.model.protobuf.AdEnumModel$AdEnum$AdInstanceType";

}
