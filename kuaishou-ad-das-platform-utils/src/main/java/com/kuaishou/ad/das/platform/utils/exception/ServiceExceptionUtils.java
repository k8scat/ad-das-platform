package com.kuaishou.ad.das.platform.utils.exception;

import com.kuaishou.old.exception.ServiceException;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-05-26
 */
public class ServiceExceptionUtils {

    public static ServiceException ofMessage(String message) {
        return ServiceException.ofMessage(-1, message);
    }

}
