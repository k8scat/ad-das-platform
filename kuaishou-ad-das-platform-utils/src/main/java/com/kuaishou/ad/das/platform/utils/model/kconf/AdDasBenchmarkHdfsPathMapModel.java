package com.kuaishou.ad.das.platform.utils.model.kconf;

import java.io.Serializable;
import java.util.Map;

import com.google.common.collect.ImmutableMap;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-03-08
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AdDasBenchmarkHdfsPathMapModel implements Serializable {

    private static final long serialVersionUID = -4677006379426051069L;

    private Map<String, Map<String, String>> hdfsPathMapData;

    private static final AdDasBenchmarkHdfsPathMapModel DEFAULT = AdDasBenchmarkHdfsPathMapModel
            .builder().hdfsPathMapData(ImmutableMap.of()).build();

    public static AdDasBenchmarkHdfsPathMapModel getDefault() {
        return DEFAULT;
    }

    public Map<String, String> getTaskMap(String nodeEnv) {
        return hdfsPathMapData.getOrDefault(nodeEnv, null);
    }

    public boolean judgeHdfsPathConfig(String nodeEnv) {
        return hdfsPathMapData.containsKey(nodeEnv);
    }

}
