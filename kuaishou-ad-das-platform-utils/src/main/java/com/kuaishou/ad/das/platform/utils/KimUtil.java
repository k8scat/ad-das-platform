package com.kuaishou.ad.das.platform.utils;

import com.kuaishou.ad.das.platform.utils.model.KimMarkDownDTO;
import com.kuaishou.framework.util.ObjectMapperUtils;
import com.kuaishou.kconf.client.Kconf;
import com.kuaishou.kconf.client.Kconfs;

import lombok.extern.slf4j.Slf4j;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-12-24
 */
@Slf4j
public class KimUtil {

    private static final String KIM_PUBLIC_NUMBER_URL = "https://is.corp.kuaishou.com/wx/robot/send?robot_id=";

    private static String KIM_ROBOT_TOKEN = "afaf6ab8-d643-4741-9fbb-25dfa5332d2d";

    @SuppressWarnings("checkstyle:StaticVariableName")
    private static Kconf<String> KIM_ROBOT_TOKEN_KCONF =
            Kconfs.ofString("ad.das.adDasKimRobotToken", KIM_ROBOT_TOKEN)
                    .build();

    /**
     * 发送kim消息到指定的kim应用号，应用号区别为access_token，申请wiki：https://docs.corp.kuaishou.com/d/home/fcACakBiib1PQB0GaPDKhb4uV#
     * https://kim-api1.kwaitalk.com/api/robot/send?key=3e47b52e-cec1-4282-9cdf-d1b0747f80cc
     */
    public static boolean sendMessageToKimPublicNumber(String content) {

        try {
            KimMarkDownDTO kimMarkDownDTO = new KimMarkDownDTO();
            kimMarkDownDTO.setMarkdown(content);
            String result = AdDasHttpClientUtils
                    .doPostWithJson(KIM_PUBLIC_NUMBER_URL + KIM_ROBOT_TOKEN_KCONF.get(),
                            ObjectMapperUtils.toJSON(kimMarkDownDTO));
            System.out.println(result);
        } catch (Exception e) {
            log.error("sendMessageToKimPublicNumber() ocurr error! content={}", content, e);
            return false;
        }
        return true;
    }

    /**
     *
     * @param modifyData
     * @param clickUrl
     * @param modifyStatus  FIXME
     * @param operator
     * @param approver
     */
    public static void sendAdDasModifyMsg(String modifyData, String clickUrl,
                                          String modifyStatus,
                                          String operator, String approver) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("## AdDas 开放平台变更通知 \n");
        stringBuilder.append("变更内容： ").append(modifyData).append("\n");
        stringBuilder.append("变更状态： <font color=green>").append(modifyStatus).append("</font>\n");
        stringBuilder.append("操作人： ").append(operator).append("\n");
        stringBuilder.append("审批人： ").append(approver).append("\n");
        stringBuilder.append("变更链接：<font color=blue> [变更地址](").append(clickUrl).append(")</font>\n");

        log.info("stringBuilder.toString={}", stringBuilder.toString());
        sendMessageToKimPublicNumber(stringBuilder.toString());
    }
}
