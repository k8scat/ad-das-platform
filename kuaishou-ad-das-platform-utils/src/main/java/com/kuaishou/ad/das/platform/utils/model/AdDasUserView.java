package com.kuaishou.ad.das.platform.utils.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-09-29
 */
@Data
public class AdDasUserView {

    @ApiModelProperty(value = "用户名称, 英文")
    private String userName;

    @ApiModelProperty(value = "用户名称, 中文")
    private String displayName;

    @ApiModelProperty(value = "用户email")
    private String email;

    @ApiModelProperty(value = "用户角色： 0-无权限 1- 超级管理员 2-RD用户 3-普通查询人员 ")
    private Integer roleType;
}
