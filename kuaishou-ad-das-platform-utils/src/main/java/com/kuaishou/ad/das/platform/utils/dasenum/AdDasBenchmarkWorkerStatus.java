package com.kuaishou.ad.das.platform.utils.dasenum;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-07-25
 */
public enum AdDasBenchmarkWorkerStatus {

    WORKING(1, "服务中"),
    UPDATING(2, "升级中"),

    ;

    private int code;

    private String desc;

    AdDasBenchmarkWorkerStatus(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public int getCode() {
        return code;
    }
}
