package com.kuaishou.ad.das.platform.utils.model;

/**
 * @author Zhang Jialin <zhangjialin@kuaishou.com>
 * Created on 2021-06-16
 */
public class PlatformUtils {

    /**
     * 根据PB类名获取全限定名
     * @param pbClassName PB类名，比如：Unit
     * @return 全限定名：com.kuaishou.ad.model.protobuf.tables.All.{pbClassName}
     */
    public static String getPbFullClassName(String pbClassName) {
        return String.format("com.kuaishou.ad.model.protobuf.tables.All.%s", pbClassName);
    }
}
