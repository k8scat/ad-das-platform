package com.kuaishou.ad.das.platform.utils.dasenum;

import lombok.Getter;

/**
 * @author wangkai <wangkai14@kuaishou.com>
 * Created on 2021-01-04
 */
@Getter
public enum AdDasTaskOutputDirectoryEnum {

    PERSONAL(1, "个人目录");

    private Integer code;

    private String desc;

    AdDasTaskOutputDirectoryEnum(int code,String desc) {
        this.code = code;
        this.desc = desc;
    }

}

