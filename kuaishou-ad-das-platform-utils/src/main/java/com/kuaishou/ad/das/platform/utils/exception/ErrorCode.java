package com.kuaishou.ad.das.platform.utils.exception;

import lombok.Getter;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-09-26
 */
@Getter
public enum ErrorCode {
    SUCCESS(0, "ok"),
    INTERNAL_ERROR(100, "内部错误"),
    APPROVER_OPERATOR_MUST_NOT_SAME(101, "审核人和发起人不能相同"),
    NO_APPROVE_POWER(102, "没有审批权限"),

    NEED_ID_PARAM_ERROR(1001, "需要制定查询的key 和id"),
    DATA_EXISTED_ERROR(1002, "数据已经存在"),
    USER_NOT_LOGIN_ERROR(1003, "用户未登陆"),
    PB_PARAM_ERROR(1004, "PB版本格式错误"),
    PB_CLASS_NOT_FOUND(1005, "PB类未找到"),
    DATA_NOT_FOUND(1006, "数据未找到"),
    SERVICE_TYPE_NOT_FOUND(1007, "服务类型未找到"),
    PARAM_ERROR(1008, "参数错误"),
    UPDATE_FIALED(1009, "更新失败"),
    MODIFY_IS_PROCESSING(1010, "变更流程处理中"),
    MODIFY_OPERATE_INVALID(1011, "无效操作"),
    MODIFY_REJECTED_REASON_EMPTY(1012, "审核拒绝，原因不能为空"),
    PB_VERSION_HAS_BEEN_PUBLISHED(1013, "服务已升级到指定PB版本"),
    ACCOUNTID_OVERSIZE_ERROR(1014, "单次查询accountId数量超过限制"),

    BENCHMARK_MODIFY_NOT_EXIT(2001, "请先保存并生成测试任务"),
    BENCHMARK_MODIFY_STATUS_ERROR(2002, "基准变更状态异常"),
    BENCHMARK_MODIFY_EXIT(2003, "存在未发布成功的基准变更"),
    BENCHMARK_TABLE_TYPE_ERROR(2004, "未找到多个分表，请检查表类型"),

    /**
     * 增量相关返回值
     */
    INCREMENT_MODIFY_STATUS_ERROR(3001, "增量变更工单状态异常"),
    INCREMENT_MODIFY_NO_AUTH(3002, "增量变更登录人无权限"),
    INCREMENT_TABLE_REPEAT(3003, "增量变更单表只能处理一次"),
    INCREMENT_SERVICE_NOT_JOIN(3004, "增量服务暂未接入"),
    INCREMENT_SERVICE_IN_PROCESSING(3005, "改增量服务已在流程中,请先完成该流程"),
    INCREMENT_MAIN_TABLE_EXISTS(3006, "请勿重复操作"),
    INCREMENT_DELET_FAILED(3007, "删除失败，请重新操作"),
    INCREMENT_PARAM_ERROR(3008, "增量服务参数错误"),
    INCREMENT_TABLE_NOT_EXISTS(3009, "表不存在"),
    INCREMENT_UPDATE_ERROR(3010, "增量服务更新失败"),
    INCREMENT_TABLE_SCHEMA_NOT_NULL(3011, "增量表Schema数据必传"),
    INCREMENT_TABLE_SCHEMA_ERROR(3012, "增量表Schema数据错误"),

    INCREMENT_INSERT_ERROR(3013,"入库失败"),
    INCREMENT_TABLE_OR_PB_ENUM_ERROR(3014,"增量表和枚举信息缺失"),

    INCREMENT_DATA_NOT_FOUND(3015, "增量数据未找到"),


    MODIFY_NEED_APPROVER(4001, "变更工单需要审批人"),
    MAIN_TABLE_NEED_PRIMARY_KEY(4002, "主表需要设置主键"),
    CASCADE_TABLE_NEED_PRIMARY_KEY(4003, "级联表需要设置主键"),

    ;



    private Integer code;

    private String msg;

    ErrorCode(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}
