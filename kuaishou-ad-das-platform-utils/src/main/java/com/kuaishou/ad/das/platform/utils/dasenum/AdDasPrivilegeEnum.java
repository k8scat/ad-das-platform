package com.kuaishou.ad.das.platform.utils.dasenum;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-06-19
 */
public enum AdDasPrivilegeEnum {

    ALL(0, "所有权限"),
    READ_ACCESS(1, "读权限"),
    EDIT_ACCESS(2, "编辑权限"),
    NO_ACCESS(3, "无权限"),

    ;

    private int code;
    private String desc;

    AdDasPrivilegeEnum(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public int getCode() {
        return code;
    }
}
