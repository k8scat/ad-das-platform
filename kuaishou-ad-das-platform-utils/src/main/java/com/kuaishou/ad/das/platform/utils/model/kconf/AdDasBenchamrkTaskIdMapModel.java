package com.kuaishou.ad.das.platform.utils.model.kconf;

import java.io.Serializable;
import java.util.Map;
import java.util.Set;

import com.google.common.collect.ImmutableMap;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-03-08
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AdDasBenchamrkTaskIdMapModel implements Serializable {

    private static final long serialVersionUID = 4773714595767829426L;

    private Map<String, Map<String, Set<String>>> taskIdMapData;

    private static final AdDasBenchamrkTaskIdMapModel DEFAULT = AdDasBenchamrkTaskIdMapModel
            .builder().taskIdMapData(ImmutableMap.of()).build();

    public static AdDasBenchamrkTaskIdMapModel getDefault() {
        return DEFAULT;
    }

    public Map<String, Set<String>> getTaskIdMap(String nodeEnv) {
        return taskIdMapData.getOrDefault(nodeEnv, null);
    }

    public boolean judgeNodeEnvConfig(String nodeEnv) {
        return taskIdMapData.containsKey(nodeEnv);
    }
}
