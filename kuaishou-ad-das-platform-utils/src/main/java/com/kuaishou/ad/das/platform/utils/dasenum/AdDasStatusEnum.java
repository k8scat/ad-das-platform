package com.kuaishou.ad.das.platform.utils.dasenum;

import lombok.Getter;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-12-02
 */
@Getter
public enum AdDasStatusEnum {

    ALL(0, "全部"),
    DEFAULT(1, "有效"),
    DELETE(2, "删除"),


    ;

    private Integer code;

    private String desc;

    AdDasStatusEnum(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }
}
