package com.kuaishou.ad.das.platform.utils.model.kconf;

import java.io.Serializable;
import java.util.Map;

import com.google.common.collect.ImmutableMap;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-03-12
 */
@Slf4j
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AdDasTableSchemaModel implements Serializable {

    private static final long serialVersionUID = 8429467906593113614L;

    private Map<String, Map<String, String>> tableSchemaMap;

    private static final AdDasTableSchemaModel DEFAULT = AdDasTableSchemaModel
            .builder().tableSchemaMap(ImmutableMap.of()).build();

    public static AdDasTableSchemaModel getDefault() {
        return DEFAULT;
    }

    public boolean existTable(String tableName) {
        return this.tableSchemaMap.containsKey(tableName);
    }

    public Map<String, String> getTableSchemaMap(String tableName) {

        return this.tableSchemaMap.getOrDefault(tableName, null);
    }

}
