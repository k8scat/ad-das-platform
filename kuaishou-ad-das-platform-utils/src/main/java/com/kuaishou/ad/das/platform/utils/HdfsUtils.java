package com.kuaishou.ad.das.platform.utils;

import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hdfs.DistributedFileSystem;

import com.google.common.collect.Maps;
import com.google.common.io.LittleEndianDataInputStream;
import com.google.protobuf.Descriptors.FieldDescriptor;
import com.google.protobuf.MessageOrBuilder;
import com.google.protobuf.TextFormat;
import com.kuaishou.ad.model.protobuf.tables.All;
import com.kuaishou.ad.model.protobuf.tables.All.AdDspAccountDailySnapshot;
import com.kuaishou.ad.model.protobuf.tables.All.AdDspAgentInfoDaily;
import com.kuaishou.framework.util.HostInfo;

import lombok.extern.slf4j.Slf4j;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-08-09
 */
@Slf4j
public class HdfsUtils {

    private static final int RETRY_SLEEP_TIME = 10000;

    /**
     * 查看文件或者目录是否存在
     * @param path
     * @return
     * @throws IOException
     */
    public static boolean existDir(String path) throws IOException {
        FileSystem fs = FileSystem.get(loadConfig());
        try {
            return fs.exists(new Path(path));
        } finally {
            if (fs != null) {
                fs.close();
            }
        }
    }

    /**
     * 创建文件目录
     * @param path
     * @throws IOException
     */
    public static void mkDir(String path) throws IOException {

        FileSystem fs = FileSystem.get(loadConfig());
        try {
            fs.mkdirs(new Path(path));
        } finally {
            if (fs != null) {
                fs.close();
            }
        }

    }

    /**
     * 删除文件
     * @param filePath
     * @throws IOException
     */
    public static boolean delFile(String filePath) throws IOException {

        FileSystem fs = FileSystem.get(loadConfig());
        try {

            if (!fs.exists(new Path(filePath))) {
                log.info("【HdfsUtils】 filePath={} is not exist!", filePath);
                return true;
            }
            log.info("【HdfsUtils】 filePath={} is delling", filePath);
            return fs.delete(new Path(filePath), true);
        } finally {
            if (fs != null) {
                fs.close();
            }
        }
    }

    public static boolean delFileWithRetry(String filePath) {

        int count = 1;
        boolean flag = false;

        try {
            flag = delFile(filePath);

        } catch (Exception e) {
            log.error("delFileWithRetry() occur error: filePath={}", filePath, e);
        }

        while (count <= 3) {

            try {

                if (flag) {
                    break;
                }

                Thread.sleep(RETRY_SLEEP_TIME);
                log.info("delFileWithRetry() count={}", count);

                flag = delFile(filePath);

            } catch (Exception e) {
                log.error("delFileWithRetry() occur error: filePath={}, count={}", filePath, count, e);
            }
            count++;
        }

        return flag;
    }


    /** 列出目录下的所有文件路径
     * @param path
     * @return
     * @throws IOException
     */
    public static List<String> listPath(String path) throws IOException {
        FileSystem fs = FileSystem.get(loadConfig());
        try {
            return listPath(fs, path);
        } finally {
            if (fs != null) {
                fs.close();
            }
        }
    }

    /** 使用提供的fs列出所有文件路径
     * @param fs
     * @param path
     * @return
     * @throws IOException
     */
    public static List<String> listPath(FileSystem fs, String path) throws IOException {
        FileStatus[] statusList = fs.listStatus(new Path(path));

        List<String> paths = new LinkedList<>();
        if (statusList != null) {
            for (FileStatus status : statusList) {
                paths.add(path + "/" + status.getPath().getName());
            }
        }
        return paths;
    }

    public static Map<String, Long> listPathAndSizeMap(String path) throws IOException {
        FileSystem fs = FileSystem.get(loadConfig());
        try {
            return listPathAndSizeMap(fs, path);
        } finally {
            if (fs != null) {
                fs.close();
            }
        }
    }

    /**
     * 获取所有文件名称和文件大小
     * @param fs
     * @param path
     * @return
     * @throws IOException
     */
    public static Map<String, Long> listPathAndSizeMap(FileSystem fs, String path) throws IOException {
        FileStatus[] statusList = fs.listStatus(new Path(path));
        if (HostInfo.debugHost() || com.kuaishou.env.util.EnvUtils.isStaging() || com.kuaishou.env.util.EnvUtils.isLocal()) {
            log.info("listPathAndSizeMap() statusList={}", statusList);
        }
        Map<String, Long> paths = Maps.newHashMap();
        if (statusList != null) {
            for (FileStatus status : statusList) {
                paths.put(status.getPath().getName(), status.getLen());
            }
        }
        if (HostInfo.debugHost() || com.kuaishou.env.util.EnvUtils.isStaging() || com.kuaishou.env.util.EnvUtils.isLocal()) {
            log.info("listPathAndSizeMap() paths={}", paths);
        }
        return paths;
    }

    /**
     * 处理目录
     * @param dirPath
     * @param processor
     * @throws IOException
     */
    public static void processDir(String dirPath, StreamProcessor processor) throws IOException {
        FileSystem fs = FileSystem.get(loadConfig());
        List<String> paths = listPath(fs, dirPath);
        if (CollectionUtils.isNotEmpty(paths)) {
            int i = 0;
            long start = System.currentTimeMillis();
            for (String path : paths) {
                long curTime = System.currentTimeMillis();
                Path p = new Path(path);

                //目录的话跳过
                if (fs.getFileStatus(p).isDirectory()) {
                    continue;
                }
                FSDataInputStream stream = fs.open(p);
                try {
                    processor.process(stream);
                } catch (Exception e) {
                    log.error("process stream Ex@", e);
                } finally {
                    if (stream != null) {
                        try {
                            stream.close();
                        } catch (Throwable e) {
                            e.printStackTrace();
                        }
                    }
                    log.info("stream-process path={} [{}/{}] finished cost {}"
                            , path
                            , ++i
                            , paths.size()
                            , System.currentTimeMillis() - curTime);
                }
            }
            log.info("stream-process all [{}/{}] over cost {}"
                    , i
                    , paths.size()
                    , System.currentTimeMillis() - start);
        }
        fs.close();
    }

    /**
     * 处理单个文件(从偏移量)
     * @param path
     * @param processor
     * @param offset
     * @throws IOException
     */
    public static void processPath(String path, StreamProcessor processor, Long offset)
            throws IOException {
        if (StringUtils.isNotBlank(path)) {
            long curTime = System.currentTimeMillis();
            FileSystem fs = FileSystem.get(loadConfig());
            FSDataInputStream stream = fs.open(new Path(path));
            try {
                stream.seek(offset);
                processor.process(stream);
            } catch (Exception e) {
                log.error("process stream Ex@", e);
            } finally {
                if (stream != null) {
                    try {
                        stream.close();
                        fs.close();
                    } catch (Throwable e) {
                        e.printStackTrace();
                    }
                }
                log.info("stream-process finished cost {}"
                        , System.currentTimeMillis() - curTime);
            }
        }
    }

    /**
     * 处理单个文件(从偏移量)
     * @param path
     * @param processor
     * @param offset
     * @throws IOException
     */
    public static void processPathWithPbClassName(String path, StreamProcessorForProto processor,
                                                  Class cl, Long offset)
            throws IOException {
        if (StringUtils.isNotBlank(path)) {
            long curTime = System.currentTimeMillis();
            FileSystem fs = FileSystem.get(loadConfig());
            FSDataInputStream stream = fs.open(new Path(path));
            try {
                stream.seek(offset);
                processor.process(stream, cl);
            } catch (Exception e) {
                log.error("process stream Ex@", e);
            } finally {
                if (stream != null) {
                    try {
                        stream.close();
                        fs.close();
                    } catch (Throwable e) {
                        e.printStackTrace();
                    }
                }
                log.info("stream-process finished cost {}"
                        , System.currentTimeMillis() - curTime);
            }
        }
    }

    /**
     * 处理单个文件
     * @param path
     * @param processor
     * @throws IOException
     */
    public static void processPath(String path, StreamProcessor processor) throws IOException {
        processPath(path, processor, 0L);
    }

    /**
     * 处理单个文件
     * @param path
     * @param processor
     * @throws IOException
     */
    public static void processPathWithPoto(String path, StreamProcessorForProto processor,
                                           Class cl) throws IOException {
        processPathWithPbClassName(path, processor, cl, 0L);
    }


    /**
     * 写入数据
     * @param data
     * @param hdfsPath
     * @param subPath
     * @throws IOException
     */
    public static void writeData(byte[] data, String hdfsPath, String... subPath) throws IOException {
        assert hdfsPath != null;
        if (data != null) {
            long curTime = System.currentTimeMillis();
            FileSystem fs = FileSystem.get(loadConfig());
            Path path = new Path(path(hdfsPath, subPath));
            FSDataOutputStream outputStream = fs.create(path, true);
            try {
                outputStream.write(data);
            } finally {
                outputStream.close();
                fs.close();
                log.info("stream-write finished path={} size={} cost {}"
                        , path.toString()
                        , data.length
                        , System.currentTimeMillis() - curTime);
            }
        }
    }

    public static boolean writeDataWithBoolean(byte[] data, String hdfsPath, String... subPath) throws IOException {
        assert hdfsPath != null;
        if (data != null) {
            long curTime = System.currentTimeMillis();
            FileSystem fs = FileSystem.get(loadConfig());
            Path path = new Path(path(hdfsPath, subPath));
            FSDataOutputStream outputStream = fs.create(path, true);
            try {
                outputStream.write(data);
                return true;
            } finally {
                outputStream.close();
                fs.close();
                log.info("stream-write finished path={} size={} cost {}"
                        , path.toString()
                        , data.length
                        , System.currentTimeMillis() - curTime);
            }
        }
        return false;
    }

    public static boolean writeDatasWithBoolean(List<byte[]> datas, String hdfsPath, String... subPath) throws IOException {
        assert hdfsPath != null;
        if (!CollectionUtils.isEmpty(datas)) {
            long curTime = System.currentTimeMillis();
            FileSystem fs = FileSystem.get(loadConfig());
            Path path = new Path(path(hdfsPath, subPath));
            FSDataOutputStream outputStream = fs.create(path, true);
            List<Integer> dataSizeList = datas.stream()
                    .map(data -> data.length)
                    .collect(Collectors.toList());
            try {
                for (byte[] data : datas) {
                    if (HostInfo.debugHost() || com.kuaishou.env.util.EnvUtils.isStaging() || com.kuaishou.env.util.EnvUtils.isLocal()) {
//                        log.info("writeDatasWithBoolean(),data={}, data.length={}, subPath={}", data, data.length, subPath);
                        log.info("writeDatasWithBoolean(), data.length={}, subPath={}", data.length, subPath);
                    }
                    outputStream.write(data);
                }
                return true;
            } finally {
                outputStream.close();

                fs.close();
                log.info("stream-write finished path={} dataSizeList={} cost {}"
                        , path.toString()
                        , dataSizeList
                        , System.currentTimeMillis() - curTime);

            }
        }
        return false;
    }

    public static boolean writeDataWithRetry(byte[] data, String hdfsPath, String... subPath) throws IOException, InterruptedException {
        assert hdfsPath != null;
        if (data != null) {

            boolean flag = false;
            try {
                flag = writeDataWithBoolean(data, hdfsPath, subPath);
            } catch (Exception e) {
                log.error("writeDataWithRetry() first write hdfs occur error!", e);
            } finally {
                // 重试一次
                if (!flag) {
                    log.error("writeDataWithRetry() writeDataWithBoolean flag={}", flag);
                    log.error("writeDataWithRetry() start retry!");
                    Thread.sleep(RETRY_SLEEP_TIME);
                    flag = writeDataWithBoolean(data, hdfsPath, subPath);
                    log.error("writeDataWithRetry() retry end!");
                }
            }

            return flag;
        }
        return false;
    }

    public static boolean writeDatasWithRetry(List<byte[]> datas, String hdfsPath, String... subPath) throws IOException, InterruptedException {
        assert hdfsPath != null;
        if (!CollectionUtils.isEmpty(datas)) {

            boolean flag = false;
            try {
                flag = writeDatasWithBoolean(datas, hdfsPath, subPath);
            } catch (Exception e) {
                log.error("writeDatasWithRetry() first write hdfs occur error!", e);
            } finally {
                // 重试一次
                if (!flag) {
                    log.error("writeDatasWithRetry() writeDataWithBoolean flag={}", flag);
                    log.error("writeDatasWithRetry() start retry!");
                    Thread.sleep(RETRY_SLEEP_TIME);
                    flag = writeDatasWithBoolean(datas, hdfsPath, subPath);
                    log.error("writeDatasWithRetry() retry end!");
                }
            }

            return flag;
        }
        return false;
    }

    /**
     * 新建路径
     * @param dir
     * @param subdir
     * @return
     */
    public static String path(String dir, String... subdir) {
        StringBuilder sb = new StringBuilder();
        sb.append(dir);
        if (!dir.endsWith("/")) {
            sb.append("/");
        }
        if (subdir != null && subdir.length > 0) {
            for (int i = 0; i < subdir.length; ++i) {
                if (i > 0) {
                    sb.append("/");
                }
                sb.append(subdir[i]);
            }
        }
        return sb.toString();
    }
    /**
     * 载入配置，配置在resource里面
     * @return
     */
    public static Configuration loadConfig() {
        // URL coreSite = RpcStubHolder.class.getClassLoader().getResource("core-site.xml");
        // URL hdfsSite = RpcStubHolder.class.getClassLoader().getResource("hdfs-site.xml");
        // URL mountTable = RpcStubHolder.class.getClassLoader().getResource("mountTable.xml");
        Configuration configuration = new Configuration();
        if (com.kuaishou.env.util.EnvUtils.isStaging() || com.kuaishou.env.util.EnvUtils.isLocal()) {
            log.info("current env is staging or local!!!");
            // configuration.set("fs.defaultFS", "hdfs://host2.test.com:9000");
            configuration.set("fs.hdfs.impl", DistributedFileSystem.class.getName());
            configuration.addResource(new Path("core-site.xml"));
            configuration.addResource(new Path("hdfs-site.xml"));
        } else {
            configuration.set("fs.hdfs.impl", DistributedFileSystem.class.getName());
            configuration.addResource(new Path("core-site.xml"));
            configuration.addResource(new Path("hdfs-site.xml"));
            configuration.addResource(new Path("mountTable.xml"));
        }

        return configuration;
    }


    /**
     * 数据处理接口，调用方会处理文件关闭问题
     */
    public interface StreamProcessor {
        /**
         * 处理文件流，可以转化为行进行处理；例如：BufferedReader br = new BufferedReader(new InputStreamReader(stream));
         * @param stream
         */
        void process(FSDataInputStream stream) throws Exception;
    }

    /**
     * 数据处理接口，调用方会处理文件关闭问题
     */
    public interface StreamProcessorForProto {
        /**
         * 处理文件流，可以转化为行进行处理；例如：BufferedReader br = new BufferedReader(new InputStreamReader(stream));
         * @param stream
         */
        void process(FSDataInputStream stream, Class cl) throws Exception;
    }

    /**
     * 使用制表符将PB所有字段值拼接为一个字符串
     * outputSample: 4197488\t353\t"2021-05-31"
     * @param pb PB实例
     * @param <T> PB类型
     * @return outputSample
     */
    public static <T extends MessageOrBuilder> String joinFieldValueFromPb(T pb) {
        StringBuilder sb = new StringBuilder();
        Map<FieldDescriptor, Object> allFields = pb.getAllFields();
        allFields.keySet().forEach(fd -> {
            try {
                TextFormat.printer().printFieldValue(fd, allFields.get(fd), sb);
                sb.append("\t");
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        return sb.toString();
    }

    /**
     * 反序列基准文件 & 转储为\t分隔的文件
     * @param inputPath 本地文件路径
     * @param outputPath 输出的文件路径
     */
    public static void parseAndDumpText(String inputPath, String outputPath) {
        File f = new File(inputPath);
        // 1. 初始化InputStream，将文件作为字节流读取
        int fileSize = (int) f.length();
        List<String> rows = new LinkedList<>();
        int count = 0;
        System.out.println(Arrays.toString(AdDspAgentInfoDaily.getDescriptor().getFields().stream().map(item -> item.getName()).toArray()));
        try (BufferedInputStream in = new BufferedInputStream(new FileInputStream(f));
                LittleEndianDataInputStream dataInputStream = new LittleEndianDataInputStream(in);
                BufferedWriter out = new BufferedWriter(new FileWriter(outputPath));
        ) {
            int i = 0;
            boolean isGetDataSize = true;
            int dataSize = 0;
            while (i < fileSize) {
                if (isGetDataSize) {
                    // 2. 先读4byte并转换成int，作为内容长度
                    dataSize = dataInputStream.readInt();
                    i = i + 4;
                } else {
                    // 3. 将内容长度作为下一次读取的byte长度，然后通过PB提供的实例方法转换成对应的PB Class
                    byte[] buffer = new byte[dataSize];
                    i = i + dataInputStream.read(buffer, 0, dataSize);
                    All.AdDspAgentInfoDaily pb = All.AdDspAgentInfoDaily.parseFrom(buffer);
                    // do something
                    count ++;
                }
                isGetDataSize = !isGetDataSize;
            }
            System.out.println(count);
            System.exit(0);
            List<String> fieldNames = AdDspAccountDailySnapshot.getDescriptor().getFields()
                    .stream().map(FieldDescriptor::getName).collect(Collectors.toList());
            rows.add(0, String.join("\t", fieldNames));
            out.write(String.join("\n", rows));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        String filePath = "/Users/zhangjialin/Desktop/ad_dsp_agent_account_daily_snapshot";
        String outPathPath = "/Users/zhangjialin/Desktop/dsp_account_353_20210531";
        parseAndDumpText(filePath, outPathPath);
    }
}

