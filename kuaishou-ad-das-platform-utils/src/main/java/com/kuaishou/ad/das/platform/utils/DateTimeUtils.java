package com.kuaishou.ad.das.platform.utils;

import static com.kuaishou.constant.ErrorCode.PARAM_INVALID;

import java.sql.Date;
import java.text.ParseException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.GregorianCalendar;

import org.apache.commons.lang3.time.FastDateFormat;

import com.kuaishou.old.exception.ServiceException;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-11-23
 */
public class DateTimeUtils {

    private static FastDateFormat dateFormat = FastDateFormat.getInstance("yyyy-MM-dd");

    /**
     * 例如:2018-12-28 10:00:00
     */
    public static final String DATE_TIME = "yyyy-MM-dd HH:mm:ss";

    /**
     * 日期string 变成 timestamp
     */
    public static long dateStr2Timestamp(String dateStr) {
        try {
            return dateFormat.parse(dateStr).getTime();
        } catch (ParseException e) {
            throw new ServiceException(PARAM_INVALID);
        }
    }

    /**
     * 获取当前小时时间戳
     */
    public static Long getCurHourTimestamp() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        return calendar.getTime().getTime();
    }

    /**
     * 获取指定 delta 小时的时间戳
     */
    public static Long getDeltaHourDate(int delta, long chargDate) {

        Date date = new Date(chargDate);
        Calendar.getInstance();
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        calendar.add(calendar.HOUR, delta); //把日期往后增加delta小时
        return calendar.getTime().getTime();
    }

    /**
     * 获取指定 delta 天的时间戳
     */
    public static Long getDeltaDate(int delta, long chargDate) {

        Date date = new Date(chargDate);
        Calendar.getInstance();
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        calendar.add(calendar.DATE, delta); //把日期往后增加一天,整数  往后推,负数往前移动
        return calendar.getTime().getTime(); //这个时间就是日期往后推一天的结果
    }

    /**
     * 将Long类型的时间戳转换成String 类型的时间格式，时间格式为：yyyy-MM-dd HH:mm:ss
     */
    public static String getNowDateStr() {
        return DateTimeFormatter.ofPattern(DATE_TIME).format(LocalDateTime.now()).toString();
    }

    /**
     * 采用java8 时间api：
     *  1、second * 1_000_000_000L + nanoTime 标识纳秒时间精度
     *  2、方法耗时：20 微秒 左右
     * @return
     */
    public static long getCurNanoTime() {

        Instant dateTime = LocalDateTime.now().toInstant(ZoneOffset.of("+8"));
        Long second = dateTime.getEpochSecond();
        int nanoTime = dateTime.getNano();

        return second * 1_000_000_000L + nanoTime;
    }

}
