package com.kuaishou.ad.das.platform.utils.dasenum;

import lombok.Getter;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-12-06
 */
@Getter
public enum  AdDasModifyTypeEnum {

    ALL(0, "全部"),
    MODIFY_PB(1, "变更PB"),
    MODIFY_BENCHMARK(2, "变更基准"),
    MODIFY_INCREMENT(3, "变更增量"),
    ;

    private Integer code;

    private String desc;

    AdDasModifyTypeEnum(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public static AdDasModifyTypeEnum ofType(Integer code) {
        switch (code) {
            case 0: return ALL;
            case 1: return MODIFY_PB;
            case 2: return MODIFY_BENCHMARK;
            case 3: return MODIFY_INCREMENT;
            default: return null;
        }
    }

}
