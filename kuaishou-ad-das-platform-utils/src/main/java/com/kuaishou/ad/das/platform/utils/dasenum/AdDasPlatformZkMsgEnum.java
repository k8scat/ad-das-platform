package com.kuaishou.ad.das.platform.utils.dasenum;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-07-05
 */
public enum  AdDasPlatformZkMsgEnum {

    WORKER_UPDATED_MSG(1, "worker变动消息"),
    CLIENT_PUBLISH_MSG(2, "client发布任务消息"),
    WORKER_MASTER_ASSIGN_MSG(3, "worker master 指定更新任务消息"),
    TASK_DONE_MSG(4, "任务执行情况消息"),
    MASTER_RETRY_TASK_MSG(5, "worker master 重试报错任务消息"),
    MASTER_CHECK_TASK_MSG(6, "worker master 校验任务消息"),
    ;

    private Integer code;
    private String desc;

    AdDasPlatformZkMsgEnum(Integer code, String desc) {
        this.code = code;
        this.desc  = desc;
    }

    public Integer getCode() {
        return code;
    }
}
