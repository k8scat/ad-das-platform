package com.kuaishou.ad.das.platform.utils.model;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-12-24
 */
public class KimMarkDownDTO {

    private String msgtype = "markdown";
    private MarkdownBean markdown;

    public String getMsgtype() {
        return msgtype;
    }

    public void setMsgtype(String msgtype) {
        this.msgtype = msgtype;
    }

    public MarkdownBean getMarkdown() {
        return markdown;
    }

    public void setMarkdown(String content) {
        MarkdownBean markdownBean = new MarkdownBean();
        markdownBean.setContent(content);
        this.markdown = markdownBean;
    }

    public static class MarkdownBean {
        private String content;

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }
    }
}
