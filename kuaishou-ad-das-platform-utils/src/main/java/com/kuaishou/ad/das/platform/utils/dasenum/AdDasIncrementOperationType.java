package com.kuaishou.ad.das.platform.utils.dasenum;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author liutao <liutao06@kuaishou.com>
 * Created on 2021-01-07
 */
@Getter
@AllArgsConstructor
public enum AdDasIncrementOperationType {
    NO_HANDLE(0, "未处理"),
    INSERT(1, "插入操作"),
    DEL(2, "删除操作");

    private final int type;
    private final String desc;

}
