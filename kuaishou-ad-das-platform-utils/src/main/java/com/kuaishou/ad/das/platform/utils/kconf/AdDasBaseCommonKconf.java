package com.kuaishou.ad.das.platform.utils.kconf;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.kuaishou.kconf.client.Kconf;
import com.kuaishou.kconf.client.Kconfs;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-01-29
 */
public class AdDasBaseCommonKconf {

    private static final int MASTER_PUBLISH_SLEEP_TIME = 300;

    private static final int INCREMENT_BACKTRACK_MAXTIME = 7200;

    protected static final int DATA_FINAL_SHARD_SIZE = 724_288_000;  // 700M 大小

    protected static final int DATA_SHARD_SIZE = 524_288_000;  // 500M 大小

    protected static final int LOWEST_DATA_SZIE = 200_000; // 20w


    public static Kconf<Integer> adBaseBenchmarkMasterPublishResSleepTime = Kconfs
            .ofInteger("ad.adcore.adBaseBenchmarkMasterPublishResSleepTime", MASTER_PUBLISH_SLEEP_TIME)
            .build();

    //配置最大回溯时间
    public static Kconf<Integer> adDasIncrementBacktrackMaxTime = Kconfs
            .ofInteger("ad.adcore.adDasIncrementBacktrackMaxTime", INCREMENT_BACKTRACK_MAXTIME)
            .build();

    public static Kconf<Integer> adBaseBenchmarkMasterRetryTimes = Kconfs
            .ofInteger("ad.adcore.adBaseBenchmarkMasterRetryTimes", 3)
            .build();

    public static Kconf<Integer> adBaseBenchmarkMasterRetryAlarmTimes = Kconfs
            .ofInteger("ad.adcore.adBaseBenchmarkMasterRetryAlarmTimes", 2)
            .build();

    public static Kconf<Double> adBaseBenchmarkMasterCheckDeltaPercent = Kconfs
            .ofDouble("ad.das.adBaseBenchmarkMasterCheckDeltaPercent", 0.2)
            .build();

    public static Kconf<Boolean> adBetaBenchmarkSlaveRunStopSwitch = Kconfs
            .ofBoolean("ad.das.adBetaBenchmarkSlaveRunStopSwitch", false)
            .build();

    public static Kconf<Long> adBaseBenchmarkWriteHdfsTimeout = Kconfs
            .ofLong("ad.das.adBaseBenchmarkWriteHdfsTimeout", 180)
            .build();

    public static Kconf<Integer> adBaseBenchmarkDataFinalShardSize = Kconfs
            .ofInteger("ad.adcore.adBaseBenchmarkDataFinalShardSize", DATA_FINAL_SHARD_SIZE)
            .build();

    public static Kconf<Integer> adBaseBenchmarkDataShardSize = Kconfs
            .ofInteger("ad.adcore.adBaseBenchmarkDataShardSize", DATA_SHARD_SIZE)
            .build();

    public static Kconf<Integer> adBaseBenchmarkLowestDataSize = Kconfs
            .ofInteger("ad.adcore.adBaseBenchmarkLowestDataSize", LOWEST_DATA_SZIE)
            .build();

    public static Kconf<Map<String, Integer>> adDasPlatformBenchmarkServiceNumConfig = Kconfs
            .ofIntegerMap("ad.adcore.adDasPlatformBenchmarkServiceNumConfig",  new HashMap<String, Integer>())
            .build();


    public static Kconf<Integer> adDasPlatformIncrementShardCount = Kconfs
            .ofInteger("ad.das.adDasPlatformIncrementShardCount", 500)
            .build();

    protected static final Set<String> DATA_SIZE_CHECK_TABLE_NAME_SET = new HashSet<String>() {
        {
            add("ad_dsp_creative");
            add("ad_dsp_unit");
            add("ad_dsp_campaign");
            add("ad_dsp_account");
            add("ad_dsp_creative_advanced_programmed");
            add("ad_dsp_target");
            add("ad_dsp_unit_target");
            add("ad_dsp_photo");
            add("ad_dsp_app");
        }
    };

    public static Kconf<Set<String>> adBaseBenchmarkDataSizeCheckTableName = Kconfs
            .ofStringSet("ad.das.adBaseBenchmarkDataSizeCheckTableName", DATA_SIZE_CHECK_TABLE_NAME_SET)
            .build();

    public static final Kconf<Integer> AD_DAS_BASE_RUNNER_THREAD = Kconfs
            .ofInteger("ad.das.adDasBaseRunnerThread", 200)
            .build();

    public static final Kconf<Integer> AD_DAS_COMMON_BENCHMARK_LIFECYCLE_THREAD = Kconfs
            .ofInteger("ad.das.adDasCommonBenchmarkLifeCycleThread", 200).build();

    public static final Kconf<Integer> AD_DAS_COMMON_BENCHMARK_EXPORTER_THREAD = Kconfs
            .ofInteger("ad.das.adDasCommonBenchmarkExporterThread", 200)
            .build();

    public static final Kconf<Integer> AD_DAS_COMMON_BENCHMARK_FETCHER_THREAD = Kconfs
            .ofInteger("ad.das.adDasCommonBenchmarkFetcherThread", 200).build();

    public static final Kconf<Integer> AD_DAS_BENCHMARK_MASTER_CRON_TIME = Kconfs
            .ofInteger("ad.das.adDasBenchmarkMasterCronTime", 2).build();

    public static Kconf<Boolean> adBetaBenchmarkDebugLogSwitch = Kconfs
            .ofBoolean("ad.das.adBetaBenchmarkDebugLogSwitch", false)
            .build();
}
