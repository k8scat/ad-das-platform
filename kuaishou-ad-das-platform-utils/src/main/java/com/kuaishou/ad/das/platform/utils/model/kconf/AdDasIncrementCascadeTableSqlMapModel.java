package com.kuaishou.ad.das.platform.utils.model.kconf;

import java.io.Serializable;
import java.util.Map;

import com.google.common.collect.ImmutableMap;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-03-28
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AdDasIncrementCascadeTableSqlMapModel implements Serializable {

    private static final long serialVersionUID = -4437108919598699516L;

    private Map<String, Map<String, String>> cascadeTableSqlMapData;

    private static final AdDasIncrementCascadeTableSqlMapModel DEFAULT = AdDasIncrementCascadeTableSqlMapModel
            .builder().cascadeTableSqlMapData(ImmutableMap.of()).build();

    public static AdDasIncrementCascadeTableSqlMapModel getDefault() {
        return DEFAULT;
    }

    public Map<String, String> getCascadeTableSqlMap(String serviceType) {
        return cascadeTableSqlMapData.getOrDefault(serviceType, null);
    }

    public boolean judgeServiceTypeConfig(String serviceType) {
        return cascadeTableSqlMapData.containsKey(serviceType);
    }

}
