package com.kuaishou.ad.das.platform.utils.dasenum;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-07-25
 */
public enum AdDasBenchmarkClientPublishType {

    PUBLISH(1, "第一发布"),
    RETRY_PUBLISH(2,"重试发布"),

    ;

    private int code;

    private String desc;

    AdDasBenchmarkClientPublishType(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public int getCode() {
        return code;
    }
}
