package com.kuaishou.ad.das.platform.utils.dasenum;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author liutao <liutao06@kuaishou.com>
 * Created on 2021-01-10
 */
@Getter
@AllArgsConstructor
public enum AdDasIncrementDatasourceEnum {
    AD_DAS_CORE_RESOLVE("AdDasCoreResolve", "adDas"),
    AD_DAS_DSP_BASE_RESOLVE("AdDasDspBaseResolve", "adDas"),
    AD_DAS_DSP_TEST_RESOLVE("AdDasDspTestResolve", "adDas"),
    AD_DAS_DSP_TEST_RESOLVE_ONE("AdDasDspTestResolve1", "adDas"),
    AD_DAS_DSP_TEST_RESOLVE_TWO("AdDasDspTestResolve2", "adDas"),
    AD_DAS_DSP_TEST_RESOLVE_THREE("AdDasDspTestResolve3", "adDas"),
    AD_DAS_DSP_TEST_RESOLVE_FORE("AdDasDspTestResolve4", "adDas"),
    AD_DAS_DSP_TEST_RESOLVE_FIVE("AdDasDspTestResolve5", "adDas"),
    AD_DAS_DSP_TEST_RESOLVE_SIX("AdDasDspTestResolve6", "adDas"),
    ;

    String serviceName;

    String datasource;


}
