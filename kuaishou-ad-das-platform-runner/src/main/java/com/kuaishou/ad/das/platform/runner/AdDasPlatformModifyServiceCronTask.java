package com.kuaishou.ad.das.platform.runner;

import static com.cronutils.model.CronType.UNIX;
import static com.cronutils.model.field.expression.FieldExpressionFactory.every;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import com.cronutils.builder.CronBuilder;
import com.cronutils.model.Cron;
import com.cronutils.model.definition.CronDefinitionBuilder;
import com.kuaishou.ad.das.platform.core.common.AdDasPlatformServiceCommonInit;
import com.kuaishou.ad.das.platform.core.common.service.AdDasModifyCronService;
import com.kuaishou.framework.runner.CronScheduledTask;

import kuaishou.common.BizDef;
import lombok.extern.slf4j.Slf4j;

/**
 * 1、定时巡查工单状态， 异常或者超时工单 转为终止状态
 * 2、监听 zk 上的 suc节点变化， 更新对应的DB 中的工单状态
 * 3、TODO
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-01-03
 */
@Lazy
@Slf4j
@Service
public class AdDasPlatformModifyServiceCronTask implements CronScheduledTask {

    @Autowired
    private AdDasPlatformServiceCommonInit platformServiceCommonInit;

    @Autowired
    private AdDasModifyCronService modifyCronService;

    @Override
    public void cronRun() throws Exception {

        // TODO 定时扫描 正在发布中的工单数据
        log.info("Start scan modify data!");
        modifyCronService.scanModifyData();
        log.info("Scan modify data success!");
    }

    @Override
    public void uniqueRunningLockAcquiredCallback() {

        // client 主备切换实现
        platformServiceCommonInit.initPlatformModifyService();
    }

    @Override
    public Cron cronConfig() {

        // 每2分运行一次
        return CronBuilder.cron(CronDefinitionBuilder.instanceDefinitionFor(UNIX))
                .withMinute(every(1))
                .instance()
                .validate();
    }

    @Override
    public boolean runAtStart() {
        return true;
    }

    @NotNull
    @Override
    public BizDef bizDef() {
        return BizDef.AD_DSP;
    }
}
