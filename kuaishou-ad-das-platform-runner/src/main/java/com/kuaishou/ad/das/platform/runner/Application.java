package com.kuaishou.ad.das.platform.runner;

import org.mybatis.spring.annotation.MapperScan;

import com.kuaishou.infra.boot.KsSpringApplicationBuilder;
import com.kuaishou.infra.boot.autoconfigure.KsBootApplication;

import lombok.extern.slf4j.Slf4j;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-07-04
 */
@Slf4j
@KsBootApplication(scanBasePackages = "com.kuaishou.ad")
@MapperScan("com.kuaishou.ad.das.platform.dal.repository.mapper")
public class Application {

    public static void main(String[] args) throws ClassNotFoundException {

        String clientName = System.getenv("DAS_SERVICE_NAME");
        log.info("clientName={}", clientName);

        Class clientClass = Class.forName("com.kuaishou.ad.das.platform.runner." + clientName);

        KsSpringApplicationBuilder.just()
                .service(clientClass)
                .run(args);

    }
}
