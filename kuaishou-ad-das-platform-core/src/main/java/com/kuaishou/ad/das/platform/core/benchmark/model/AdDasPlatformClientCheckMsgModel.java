package com.kuaishou.ad.das.platform.core.benchmark.model;

import java.util.List;
import java.util.Set;

import lombok.Data;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-06-06
 */
@Data
public class AdDasPlatformClientCheckMsgModel {

    private boolean successFlag = false;

    /**
     * 正在做的 taskNum
     */
    private Set<Integer> doingTaskNums;

    private List<String> doneContents;
}
