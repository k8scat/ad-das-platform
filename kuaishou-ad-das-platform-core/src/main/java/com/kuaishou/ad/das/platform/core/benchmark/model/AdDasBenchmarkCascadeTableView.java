package com.kuaishou.ad.das.platform.core.benchmark.model;

import java.io.Serializable;
import java.util.List;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-03-25
 */
@Data
public class AdDasBenchmarkCascadeTableView implements Serializable {

    private static final long serialVersionUID = 2145745242817353989L;

    @ApiModelProperty(value = "级联表")
    private String cascadeTable;

    @ApiModelProperty(value = "级联PB")
    private String cascadePb;

    @ApiModelProperty(value = "Pb列名")
    private String cloumnMsg;

    @ApiModelProperty(value = "db表跟pb字段映射")
    private List<AdDasBenchmarkDb2PbColumnMsgView> db2PbColumnMsgViews;

}
