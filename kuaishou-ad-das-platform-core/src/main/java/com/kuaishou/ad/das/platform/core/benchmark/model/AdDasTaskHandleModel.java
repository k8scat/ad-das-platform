package com.kuaishou.ad.das.platform.core.benchmark.model;

import java.util.List;
import java.util.Map;

import lombok.Data;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-01-29
 */
@Data
public class AdDasTaskHandleModel {

    /**
     * 任务标号
     */
    private Integer taskNum;

    /**
     * 任务名称
     */
    private String taskName;

    /**
     * 任务执行时间戳
     */
    private Long timestamp;

    private String hdfsPath;

    private String clientName;

    private String assignName;

    /**
     * 大数据表文件shardId
     */
    private Integer shardId = 0;

    /**
     * 表名称
     */
    private String tableName;

    /**
     * 类名称
     */
    private String pbclassFullName;

    private AdDasTaskSchemaModel adDasTaskSchemaModel;

    /**
     * 数据查询类型： 0-db 1-clickHouse
     */
    private Integer fetcherType;

    /**
     * 数据写出类型: 0-hdfs 1-redis
     */
    private Integer exportType;

    /**
     * 原始数据
     */
    private List<Map<String,Object>> dataObjectMaps;

    /**
     * 写入hdfs的数据
     */
    private AdDasPlatformBenchmarkResultModel dtoResult;

}
