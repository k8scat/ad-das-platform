package com.kuaishou.ad.das.platform.core.common;

import static com.github.phantomthief.collection.impl.SimpleBufferTrigger.TriggerResult.trig;
import static com.kuaishou.ad.das.platform.core.utils.AdDasBaseMultiEnvUtils.getDasPlatformNameSpace;
import static com.kuaishou.ad.das.platform.core.utils.AdDasBaseMultiEnvUtils.getDasZk;
import static com.kuaishou.ad.das.platform.utils.constant.AdDasCommonConstant.AD_DAS_NAMESPACE_NEW;
import static com.kuaishou.ad.das.platform.utils.dasenum.AdDasBenchmarkStatusEnum.SUCESS_STATUS;
import static com.kuaishou.ad.das.platform.utils.kconf.AdDasPlatformKconf.AD_DAS_PLATFORM_BENCHMARK_MAX_COUNT;
import static com.kuaishou.ad.das.platform.utils.kconf.AdDasPlatformKconf.AD_DAS_PLATFORM_BENCHMARK_NET_PERIOD;
import static com.kuaishou.ad.das.platform.utils.kconf.AdDasPlatformKconf.AD_DAS_PLATFORM_BENCHMARK_TIME_INTERVAL;
import static com.kuaishou.framework.util.PerfUtils.perf;

import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.TimeUnit;

import org.apache.curator.RetryPolicy;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.retry.RetryForever;
import org.springframework.beans.factory.annotation.Autowired;

import com.github.phantomthief.collection.BufferTrigger;
import com.kuaishou.ad.das.platform.core.benchmark.model.AdDasPlatformTaskDoneModel;
import com.kuaishou.ad.das.platform.core.benchmark.model.AdDasPlatformZkMsgModel;
import com.kuaishou.framework.util.ObjectMapperUtils;

import lombok.extern.slf4j.Slf4j;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-01-29
 */
@Slf4j
public abstract class AdDasBenchmarkAbstractZk extends AdDasBenchmarkZkModel {

    @Autowired
    private AdDasPlatformServiceConfig adDasPlatformServiceConfig;

    /**
     * zk 消息积攒bufferTrigger 转异步单线程处理
     *
     */
    protected final BufferTrigger<AdDasPlatformZkMsgModel> bufferTrigger = BufferTrigger.<AdDasPlatformZkMsgModel, ConcurrentLinkedQueue<AdDasPlatformZkMsgModel>> simple()
            .setContainer(ConcurrentLinkedQueue::new, this::add)
            .maxBufferCount(AD_DAS_PLATFORM_BENCHMARK_MAX_COUNT.get())
            .interval(AD_DAS_PLATFORM_BENCHMARK_TIME_INTERVAL.get(), TimeUnit.SECONDS)
            .enableBackPressure((BaseChangedRow) -> perf(AD_DAS_NAMESPACE_NEW, "back.pressure", "ad.das.platform.benchmark").logstash())
            .consumer(this::consume)
            .name("ad-das-platform-benchmark-buffer-trigger")
            .triggerStrategy((lastConsumeTimestamp, dasCount) -> {
                long interval = AD_DAS_PLATFORM_BENCHMARK_TIME_INTERVAL.get();
                long maxBufferCount = AD_DAS_PLATFORM_BENCHMARK_MAX_COUNT.get();

                long intervalInMs = TimeUnit.SECONDS.toMillis(interval);
                return trig(dasCount >= maxBufferCount
                                || System.currentTimeMillis() - lastConsumeTimestamp >= intervalInMs,
                        AD_DAS_PLATFORM_BENCHMARK_NET_PERIOD.get());
            })
            .build();

    protected boolean add(ConcurrentLinkedQueue<AdDasPlatformZkMsgModel> list, AdDasPlatformZkMsgModel adDasPlatformZkMsgModel) {
        list.add(adDasPlatformZkMsgModel);
        return true;
    }

    /**
     * zk消息 转异步单线程处理
     * @param adDasPlatformZkMsgModels
     */
    protected abstract void consume(ConcurrentLinkedQueue<AdDasPlatformZkMsgModel> adDasPlatformZkMsgModels);

    /**
     * zk 会话链接
     * @throws InterruptedException
     */
    protected void initiZkConnection() throws InterruptedException {
        RetryPolicy retryPolicy = new RetryForever(1000);
        List<String> zkAddress = getDasZk(adDasPlatformServiceConfig.getServiceName(),
                adDasPlatformServiceConfig.getBenchmarkEnv());

        curatorFramework = CuratorFrameworkFactory.builder()
                .connectString(zkAddress.get(0))
                .retryPolicy(retryPolicy)
                .sessionTimeoutMs(SESSION_TIME)  // 120s
                .connectionTimeoutMs(CONNECT_TIMEOUT)  // 100s
                .namespace(getDasPlatformNameSpace(adDasPlatformServiceConfig.getServiceEnv()))
                .build();
        curatorFramework.start();
        curatorFramework.blockUntilConnected(10, TimeUnit.SECONDS);
    }

    protected boolean checkContent(String content) {

        AdDasPlatformTaskDoneModel dasPlatformTaskDoneModel = ObjectMapperUtils
                .fromJSON(content, AdDasPlatformTaskDoneModel.class);

        if (dasPlatformTaskDoneModel.getStatus() == null || (dasPlatformTaskDoneModel.getStatus() != null
                && dasPlatformTaskDoneModel.getStatus().intValue() != SUCESS_STATUS.getStatus())) {
            log.error("【Error】checkContent() tableResult={}", dasPlatformTaskDoneModel);
            return false;
        }
        return true;
    }

}
