package com.kuaishou.ad.das.platform.core.increment.helper;

import static com.kuaishou.ad.das.platform.utils.constant.AdDasCommonConstant.AD_DAS_COMMON_INCR_LIFE_CYCLE_SUBTAG;
import static com.kuaishou.ad.das.platform.utils.constant.AdDasCommonConstant.AD_DAS_NAMESPACE_NEW;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;
import com.kuaishou.ad.das.platform.dal.commonquery.AdDasPlatformCommonQuery;
import com.kuaishou.framework.util.PerfUtils;

import lombok.extern.slf4j.Slf4j;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-01-09
 */
@Slf4j
@Service
public class AdDasIncrementCommonQueryHelper {

    @Autowired
    private AdDasPlatformCommonQuery adDasIncremntCommonQuery;

    /**
     * 查询 db with retry
     * @param jdbcTemplate
     * @param sql
     * @param params
     * @return
     */
    public List<Map<String, Object>> queryWithRetry(JdbcTemplate jdbcTemplate,
                                                    String sql, String tableName,
                                                    Map<String, String> schemaMap,
                                                    List<Object> params) {

        List<Map<String, Object>> result = Lists.newArrayList();

        try {

            result =  adDasIncremntCommonQuery.queryBySqlWithSchema(jdbcTemplate, tableName, schemaMap, sql, params);
        } catch (Exception e) {
            // 增加 perf 打点
            PerfUtils.perf(AD_DAS_NAMESPACE_NEW, AD_DAS_COMMON_INCR_LIFE_CYCLE_SUBTAG,
                    "CommonQueryHelperError", "queryWithRetry", tableName)
                    .logstash();
            log.error("queryWithRetry() ocurr error! sql={}, params={}", sql, params, e);
            result =  adDasIncremntCommonQuery.queryBySqlWithSchema(jdbcTemplate, tableName, schemaMap, sql, params);
        }

        return result;
    }

    /**
     * query count
     * @param jdbcTemplate
     * @param sql
     * @param tableName
     * @param params
     * @return
     */
    public List<Map<String, Object>> queryCountWithRetry(JdbcTemplate jdbcTemplate, String sql, String tableName, List<Object> params) {

        List<Map<String, Object>> result = Lists.newArrayList();

        try {

            result =  adDasIncremntCommonQuery.queryBySql(jdbcTemplate, sql, params);
        } catch (Exception e) {
            // 增加 perf 打点
            PerfUtils.perf(AD_DAS_NAMESPACE_NEW, AD_DAS_COMMON_INCR_LIFE_CYCLE_SUBTAG,
                    "CommonQueryHelper", "queryCountWithRetry", tableName)
                    .logstash();
            log.error("queryCountWithRetry() ocurr error! sql={}, tableName={} params={}", sql, tableName, params, e);
            result =  adDasIncremntCommonQuery.queryBySql(jdbcTemplate, sql, params);
        }

        return result;
    }


    /**
     * 查询 db with retry
     * @param jdbcTemplate
     * @param shardId
     * @param sql
     * @param params
     * @return
     */
    public List<Map<String, Object>> queryByshardWithRetry(JdbcTemplate jdbcTemplate,
                                                           long shardId, String sql,
                                                           List<Object> params) {

        List<Map<String, Object>> result = Lists.newArrayList();

        try {

            result =  adDasIncremntCommonQuery.queryBySql(jdbcTemplate, sql, params);
        } catch (Exception e) {

            PerfUtils.perf(AD_DAS_NAMESPACE_NEW, AD_DAS_COMMON_INCR_LIFE_CYCLE_SUBTAG,
                    "CommonQueryHelper", "queryByshardWithRetry", "error")
                    .logstash();
            log.error("queryWithRetry() ocurr error! sql={}, params={}", sql, params, e);
            result =  adDasIncremntCommonQuery.queryBySql(jdbcTemplate, sql, params);
        }

        return result;
    }

}
