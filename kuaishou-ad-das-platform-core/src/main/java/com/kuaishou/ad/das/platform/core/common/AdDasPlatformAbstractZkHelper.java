package com.kuaishou.ad.das.platform.core.common;

import static com.kuaishou.ad.das.platform.core.utils.AdDasBaseMultiEnvUtils.getDasPlatformNameSpace;
import static com.kuaishou.ad.das.platform.core.utils.AdDasBaseMultiEnvUtils.getDasZk;
import static com.kuaishou.ad.das.platform.utils.constant.AdDasCommonConstant.AD_DAS_NAMESPACE_NEW;
import static com.kuaishou.ad.das.platform.utils.constant.AdDasCommonConstant.COMMON_PLATFORMLIFE_CYCLE_SUBTAG;
import static com.kuaishou.ad.das.platform.utils.dasenum.AdDasModifyPublishStatusEnum.PUBLISH_SUC;
import static com.kuaishou.ad.das.platform.utils.kconf.AdDasPlatformKconf.adDasPlatformProtoEnumVersionConfig;
import static com.kuaishou.framework.util.PerfUtils.perf;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

import org.apache.commons.lang3.StringUtils;
import org.apache.curator.RetryPolicy;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.framework.recipes.cache.TreeCache;
import org.apache.curator.framework.recipes.cache.TreeCacheEvent;
import org.apache.curator.framework.recipes.cache.TreeCacheListener;
import org.apache.curator.retry.RetryForever;
import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.data.Stat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;

import com.google.common.collect.Lists;
import com.kuaishou.ad.das.platform.core.common.daszk.model.AdDasPlatformProtoSchema;
import com.kuaishou.ad.das.platform.core.common.daszk.model.AdDasPublishPbSchema;
import com.kuaishou.ad.das.platform.core.common.daszk.model.AdDasPublishPbSucSchema;
import com.kuaishou.ad.das.platform.core.common.daszk.model.AdDasServiceNodeSchema;
import com.kuaishou.ad.das.platform.core.common.pbloader.AdDasApiPbLoader;
import com.kuaishou.ad.das.platform.core.common.pbloader.AdDasBasePbLoader;
import com.kuaishou.ad.das.platform.core.common.pbloader.AdDasIncrementPbLoader;
import com.kuaishou.ad.das.platform.core.common.service.AdDasModifyPbService;
import com.kuaishou.ad.das.platform.dal.model.AdDasModifyPbMsg;
import com.kuaishou.ad.das.platform.dal.model.AdDasServicePb;
import com.kuaishou.ad.das.platform.dal.repository.AdDasModifyServicePbRecordRepository;
import com.kuaishou.framework.util.ObjectMapperUtils;

import lombok.extern.slf4j.Slf4j;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-12-08
 */
@Slf4j
public abstract class AdDasPlatformAbstractZkHelper extends AdDasBenchmarkZkModel {

    protected static CuratorFramework curatorFramework;
    protected TreeCache commonPbTreeCache;
    protected String myName;  // 本实例的编号
    protected String myServiceNodePath; //本实例的path

    // 所有
    protected TreeCache commonPbTreeCacheService;

    @Autowired
    private AdDasApiPbLoader adDasApiPbLoader;

    @Autowired
    private AdDasBasePbLoader adDasBasePbLoader;

    @Autowired
    private AdDasIncrementPbLoader adDasIncrementPbLoader;

    @Autowired
    private AdDasModifyPbService adDasModifyPbService;

    @Autowired
    protected AdDasPlatformServiceConfig platformServiceConfig;

    @Autowired
    private AdDasModifyServicePbRecordRepository adDasModifyServicePbRecordRepository;

    protected void initZkConnection() throws Exception {
        RetryPolicy retryPolicy = new RetryForever(1000);
        List<String> zkAddress = getDasZk(platformServiceConfig.getServiceName(),
                platformServiceConfig.getBenchmarkEnv());

        log.info("initZkConnection() zkAddress={}", zkAddress);
        this.curatorFramework = CuratorFrameworkFactory.builder()
                .connectString(zkAddress.get(0))
                .retryPolicy(retryPolicy)
                .sessionTimeoutMs(SESSION_TIME)  // 120s
                .connectionTimeoutMs(CONNECT_TIMEOUT)  // 100s
                .namespace(getDasPlatformNameSpace(platformServiceConfig.getServiceEnv()))
                .build();
        this.curatorFramework.start();
        this.curatorFramework.blockUntilConnected(10, TimeUnit.SECONDS);

    }

    protected void initPlatformModifyService() throws Exception {

        initZkConnection();

        // 监听 platform/common_bp/serviceType/serviceName下的 节点变化， 当prepb变化时触发实例 reload kuaishou-ad-new-biz-proto 包
        String commonPbPath = PLATFORM_SERVICE;
        commonPbTreeCacheService = new TreeCache(curatorFramework, commonPbPath);
        TreeCacheListener resourceChildrenCacheListener = (client, event1) -> {
            TreeCacheEvent.Type eventType = event1.getType();

            // 只监听更新操作
            if (eventType == TreeCacheEvent.Type.NODE_UPDATED) {
                String eventData = new String(event1.getData().getData());
                String path = event1.getData().getPath();
                if (StringUtils.isEmpty(eventData)) {
                    log.info("path={}, eventData={}", path, eventData);
                    // TODO 优化发布 Proto 更新代码

                }
            }
        };
        commonPbTreeCacheService.getListenable().addListener(resourceChildrenCacheListener);
        commonPbTreeCacheService.start();
    }

    protected void initCommonPlatformBase() throws Exception {

        initZkConnection();

        // 检查服务父节点是否存在如果不存在就创建
        String servicePath = PLATFORM_SERVICE + "/" + platformServiceConfig.getServiceType() + "/" + platformServiceConfig.getServiceName();
        Stat statService = this.curatorFramework.checkExists()
                .forPath(servicePath);
        if (statService == null) {
            this.curatorFramework.create().creatingParentsIfNeeded().withMode(CreateMode.PERSISTENT)
                    .forPath(servicePath);
        }

        String containerIp = System.getenv(CONTAINERIP);
        String containerId = System.getenv(CONTAINERID);
        AdDasServiceNodeSchema adDasServiceNodeSchema = new AdDasServiceNodeSchema();
        adDasServiceNodeSchema.setContainerId(containerId);
        adDasServiceNodeSchema.setContainerIp(containerIp);
        adDasServiceNodeSchema.setMyName(myName);
        adDasServiceNodeSchema.setServiceType(platformServiceConfig.getServiceType());
        adDasServiceNodeSchema.setServiceName(platformServiceConfig.getServiceName());

        List<AdDasServicePb> servicePbs = adDasModifyPbService.queryServicePb(platformServiceConfig.getServiceName());
        log.info("AdDasCommonPbLoader defaultLoadPb() defaultLoadPb() servicePbs={}", servicePbs);

        AdDasServicePb adDasServicePb;

        if (CollectionUtils.isEmpty(servicePbs)) {
            adDasServicePb = new AdDasServicePb();
        } else {
            adDasServicePb = Optional.ofNullable(servicePbs.get(0))
                    .orElse(new AdDasServicePb());
        }

        String lastestPbVersion = Optional.ofNullable(adDasServicePb.getPbVersion()).orElse("");
        String lastestPbEnumVersion = Optional.ofNullable(adDasServicePb.getPbEnumVersion()).orElse("");
        String lastestPbCommonVersion = Optional.ofNullable(adDasServicePb.getPbCommonVersion()).orElse("");
        AdDasPlatformProtoSchema platformProtoSchema = new AdDasPlatformProtoSchema();
        platformProtoSchema.setPbVersion(lastestPbVersion);
        platformProtoSchema.setPbEnumVersion(lastestPbEnumVersion); // TODO 完善 proto 包版本更新
        adDasServiceNodeSchema.setPlatformProtoSchema(platformProtoSchema);

        String nodeSchemaStr = ObjectMapperUtils.toJSON(adDasServiceNodeSchema);
        log.info("AdDasCommonPbLoader nodeSchemaStr={}", nodeSchemaStr);

        // 为本实例创建服务临时节点
        myServiceNodePath = this.curatorFramework.create().withMode(CreateMode.EPHEMERAL_SEQUENTIAL)
                .forPath(servicePath + "/1", nodeSchemaStr.getBytes());
        myName = myServiceNodePath.replace(servicePath + "/", "");

        // 检查 common/pb 父节点是否存在
        String commonPbPath = PLATFORM_COMMON_PB + "/" + platformServiceConfig.getServiceType() + "/" + platformServiceConfig.getServiceName();
        Stat statCommon = this.curatorFramework.checkExists()
                .forPath(commonPbPath);
        if (statCommon == null) {
            this.curatorFramework.create().creatingParentsIfNeeded().withMode(CreateMode.PERSISTENT)
                    .forPath(commonPbPath);
        }

        String curPbPath = commonPbPath + "/curpb";
        Stat statCurPb =  this.curatorFramework.checkExists().forPath(curPbPath);
        if (statCurPb == null) {
            this.curatorFramework.create().creatingParentsIfNeeded().withMode(CreateMode.PERSISTENT)
                    .forPath(curPbPath);
        }

        String prePbPath = commonPbPath +  "/prepb";
        Stat statPrePb =  this.curatorFramework.checkExists().forPath(prePbPath);
        if (statPrePb == null) {
            this.curatorFramework.create().creatingParentsIfNeeded().withMode(CreateMode.PERSISTENT)
                    .forPath(prePbPath);
        }

        // 监听 /common_bp/serviceType/serviceName下的 节点变化， 当prepb变化时触发实例 reload kuaishou-ad-new-biz-proto 包
        commonPbTreeCache = new TreeCache(curatorFramework, commonPbPath);
        TreeCacheListener resourceChildrenCacheListener = (client, event1) -> {
            TreeCacheEvent.Type eventType = event1.getType();

            // 只监听更新操作
            if (eventType == TreeCacheEvent.Type.NODE_UPDATED) {
                String eventData = new String(event1.getData().getData());
                log.info("/common_pb updated! path={}, eventData={}", event1.getData().getPath(), eventData);

                // prepb 更新触发预加载
                if (event1.getData().getPath().contains(platformServiceConfig.getServiceName())
                        && event1.getData().getPath().endsWith("prepb")
                        && !StringUtils.isEmpty(eventData)) {

                    log.info("/common_pb prepb updated! path={}, eventData={}", event1.getData().getPath(), eventData);
                    preLoadPb(eventData);
                }
            }
        };
        commonPbTreeCache.getListenable().addListener(resourceChildrenCacheListener);
        commonPbTreeCache.start();
    }

    /**
     * 独立任务 更新 suc 结果
     * @param serviceEnv
     * @param path
     */
    private void sucUpdate(String serviceEnv, String path) {

        try {
            //  根据变更的 path 来解析出 serviceType 和 serviceName
            AdDasPublishPbSucSchema adDasPublishPbSucSchema = null;
            log.info("sucUpdate() adDasPublishPbSucSchema={}", adDasPublishPbSucSchema);

            // 当pushlishStatus 状态为 发布异常 或者发布成功时
            Integer pushlishStatus = adDasPublishPbSucSchema.getPushlishStatus();


           /* // 处理DB 工单数据
            adDasModifyPbService.processModifyPb("", "serviceName", adDasPublishPbSucSchema);
*/
            // 更新 curPb 状态
            // String curPbPath = PLATFORM_COMMON_PB + "/" + (serviceType) + "/" + serviceName + "/curpb";
            /*Stat statCurPb =  getCuratorFramework().checkExists().forPath(curPbPath);
            if (statCurPb == null) {
                getCuratorFramework().create().creatingParentsIfNeeded().withMode(CreateMode.PERSISTENT)
                        .forPath(curPbPath);
            }
            // 写入 /curpb 用于重启时直接读取该版本数据
            AdDasPublishPbSchema dasPublishPbSchema = new AdDasPublishPbSchema();
            dasPublishPbSchema.setModifyId(adDasPublishPbSucSchema.getModifyId());
            dasPublishPbSchema.setPbVersion(adDasPublishPbSucSchema.getTargetPbVersion());
            String adDasPublishPbSchemaStr = ObjectMapperUtils.toJSON(dasPublishPbSchema);
            getCuratorFramework().setData().forPath(curPbPath, adDasPublishPbSchemaStr.getBytes());
*/
        } catch (Exception e) {
            log.error("sucUpdate() occur error! serviceEnv={}, path={}", serviceEnv, path, e);
            perf(AD_DAS_NAMESPACE_NEW, COMMON_PLATFORMLIFE_CYCLE_SUBTAG,
                    "reLoadPbError", "sucUpdate", path).logstash();
        }
    }

    /**
     * 汇总检查 发布Pb状态
     * @param serviceType
     * @param serviceName
     * @param adDasModifyPbMsg
     * @return
     */
    public boolean checkPublishStatus(String serviceType, String serviceName, AdDasModifyPbMsg adDasModifyPbMsg) {

        // 获取 服务下 所有实例列表数据
        AtomicReference<Boolean> checkVersion = new AtomicReference<>(true);
        try {
            List<AdDasServiceNodeSchema> adDasServiceNodeSchemas = queryServiceNodeSchemas(serviceType, serviceName);
            log.info("checkPublishStatus() adDasServiceNodeSchemas={}", adDasServiceNodeSchemas);

            for (AdDasServiceNodeSchema adDasServiceNodeSchema : adDasServiceNodeSchemas) {

                AdDasPlatformProtoSchema platformProtoSchema =  adDasServiceNodeSchema.getPlatformProtoSchema();
                String curPbVersion = platformProtoSchema.getPbVersion();

                String targetPbVersion = adDasModifyPbMsg.getTargetPbVersion();

                // 判断整体更新结果
                if (!curPbVersion.equals(targetPbVersion)) {
                    checkVersion.set(false);
                }
            }
        } catch (Exception e) {
            log.error("checkPublishStatus() serviceType={}, serviceName={}, adDasPublishPbSucSchema={}",
                    serviceType, serviceName, adDasModifyPbMsg, e);
            perf(AD_DAS_NAMESPACE_NEW, COMMON_PLATFORMLIFE_CYCLE_SUBTAG,
                    "publishPbError", "checkPublishStatus",
                    "", serviceType + ":" + serviceName).logstash();
            checkVersion.set(false);
        }

        return checkVersion.get();
    }

    /**
     * 查询特定服务下的实例列表
     * @param serviceType
     * @param serviceName
     * @return
     * @throws Exception
     */
    public List<AdDasServiceNodeSchema> queryServiceNodeSchemas(String serviceType, String serviceName) throws Exception {
        List<AdDasServiceNodeSchema> result = Lists.newArrayList();

        String servicePath = PLATFORM_COMMON_PB + "/" + serviceType + "/" + serviceName ;
        List<String> childClients = getCuratorFramework().getChildren().forPath(servicePath);
        log.info("queryServiceNodeSchemas() serviceName={}, childClient={}",
                serviceName, childClients);

        for (String childClient : childClients) {
            String childClientData = new String(getCuratorFramework().getData().forPath(servicePath + "/" + childClient));

            if (org.springframework.util.StringUtils.isEmpty(childClientData)) {
                log.error("queryServiceNodeSchemas() serviceName={}, childClient={} data is empty!",
                        serviceName, childClientData);
                continue;
            }
            AdDasServiceNodeSchema adDasServiceNodeSchema = ObjectMapperUtils.fromJSON(
                    childClientData, AdDasServiceNodeSchema.class);
            result.add(adDasServiceNodeSchema);
        }

        return result;
    }

    /**
     * 查询 服务类型下 的 所有服务名称
     * @param serviceType
     * @return
     * @throws Exception
     */
    public List<String> queryServiceNameByType(String serviceType) {

        try {
            String servicePathIncrement = PLATFORM_COMMON_PB + "/" + serviceType;
            List<String> childClientsIncrement = getCuratorFramework().getChildren().forPath(servicePathIncrement);
            return childClientsIncrement;
        } catch (Exception e) {
            log.error("queryServiceNameByType() serviceType={}", serviceType);
        }

        return Lists.newArrayList();
    }

    /**
     * 根据服务名称 查询 服务下的 实例信息
     * @param serviceName
     * @return
     */
    public List<AdDasServiceNodeSchema> queryServiceNodeSchemas(String serviceName) throws Exception {
        List<AdDasServiceNodeSchema> result = Lists.newArrayList();

        String serviceType = queryServiceTypeByServiceName(serviceName);
        log.info("queryServiceNodeSchemas() serviceName={}, serviceType={}", serviceName, serviceType);

        String servicePath = PLATFORM_COMMON_PB + "/" + platformServiceConfig.getServiceType()
                + "/" + serviceName ;
        List<String> childClients = getCuratorFramework().getChildren().forPath(servicePath);
        log.info("queryServiceNodeSchemas() serviceName={}, childClient={}",
                serviceName, childClients);

        for (String childClient : childClients) {
            String childClientData = new String(getCuratorFramework().getData().forPath(servicePath + "/" + childClient));

            if (org.springframework.util.StringUtils.isEmpty(childClientData)) {
                log.error("queryServiceNodeSchemas() serviceName={}, childClient={} data is empty!",
                        serviceName, childClientData);
                continue;
            }
            AdDasServiceNodeSchema adDasServiceNodeSchema = ObjectMapperUtils.fromJSON(
                    childClientData, AdDasServiceNodeSchema.class);
            result.add(adDasServiceNodeSchema);
        }

        return result;
    }

    /**
     * 批量更新 临时节点 信息
     * @param serviceName
     * @param dasServiceNodeSchemaMap
     */
    public void updateServiceNodeSchemas(String serviceName, Map<String, AdDasServiceNodeSchema> dasServiceNodeSchemaMap) throws Exception {

        String serviceType = queryServiceTypeByServiceName(serviceName);
        log.info("updateServiceNodeSchemas() serviceName={}, serviceType={}", serviceName, serviceType);

        String servicePath = PLATFORM_COMMON_PB + "/" + serviceType + "/" + serviceName ;
        List<String> childClients = getCuratorFramework().getChildren().forPath(servicePath);
        log.info("updateServiceNodeSchemas() serviceName={}, childClient={}",
                serviceName, childClients);

        for (String childClient : childClients) {
            String childClientData = new String(getCuratorFramework().getData().forPath(servicePath + "/" + childClient));

            if (org.springframework.util.StringUtils.isEmpty(childClientData)) {
                log.error("updateServiceNodeSchemas() serviceName={}, childClient={} data is empty!",
                        serviceName, childClientData);
                continue;
            }
            AdDasServiceNodeSchema adDasServiceNodeSchema = ObjectMapperUtils.fromJSON(
                    childClientData, AdDasServiceNodeSchema.class);
            String serviceNodeName = adDasServiceNodeSchema.getContainerId();

            if (dasServiceNodeSchemaMap.containsKey(serviceNodeName)) {
                AdDasServiceNodeSchema dasServiceNodeSchema = dasServiceNodeSchemaMap.get(serviceNodeName);
                String dasServiceNodeSchemaStr = ObjectMapperUtils.toJSON(dasServiceNodeSchema);
                log.info("updateServiceNodeSchemas() serviceName={}, dasServiceNodeSchema={}", serviceName, dasServiceNodeSchema);
                getCuratorFramework().setData().forPath(servicePath + "/" + childClient, dasServiceNodeSchemaStr.getBytes());
            }
        }
    }

    /**
     * 根据 serviceName 获取 serviceType
     * 查询 service/ ad.das.api、ad.das.base、ad.das.increment 节点下的 所有实例名称
     * @param serviceName
     * @return
     * @throws Exception
     */
    public String queryServiceTypeByServiceName(String serviceName) throws Exception {

        List<String> childClientsApi = queryServiceNameByType("ad.das.api");
        log.info("queryServiceTypeByServiceName() serviceName={}, childClientsApi={}",
                serviceName, childClientsApi);
        if (!CollectionUtils.isEmpty(childClientsApi) && childClientsApi.contains(serviceName)) {
            return "ad.das.api";
        }

        List<String> childClientsBase = queryServiceNameByType("ad.das.base");
        log.info("queryServiceTypeByServiceName() serviceName={}, childClientsBase={}",
                serviceName, childClientsBase);
        if (!CollectionUtils.isEmpty(childClientsBase) && childClientsBase.contains(serviceName)) {
            return "ad.das.base";
        }

        List<String> childClientsIncrement = queryServiceNameByType("ad.das.increment");
        log.info("queryServiceTypeByServiceName() serviceName={}, childClientsIncrement={}",
                serviceName, childClientsIncrement);
        if (!CollectionUtils.isEmpty(childClientsIncrement) && childClientsIncrement.contains(serviceName)) {
            return "ad.das.increment";
        }

        return "";
    }

    /**
     * 预加载 pb 变更
     * 1. 预加载 Pb 包, 更新 ad_das_modify_service_pb_record 中的实例记录
     * 2. 反射 Pb class Cache
     * 3. 由preLoadPb 触发 更新 curPb & suc
     * @param eventData
     */
    private void preLoadPb(String eventData) {

        try {

            String commonPbPath = PLATFORM_COMMON_PB + "/" + platformServiceConfig.getServiceType() + "/" + platformServiceConfig.getServiceName();;
            String prePbPath = commonPbPath + "/prepb";
            Stat statPrePb =  getCuratorFramework().checkExists().forPath(prePbPath);
            if (statPrePb == null) {
                getCuratorFramework().create().creatingParentsIfNeeded().withMode(CreateMode.PERSISTENT)
                        .forPath(prePbPath);
            }

            String prePbData = new String(getCuratorFramework().getData().forPath(prePbPath));

            AdDasPublishPbSchema dasPublishPbSchema = ObjectMapperUtils
                    .fromJSON(prePbData, AdDasPublishPbSchema.class);
            log.info("preLoadPb() adDasPublishPbSchema={}", dasPublishPbSchema);

            // 取实例
            String instanceData = new String(getCuratorFramework().getData().forPath(myServiceNodePath));

            AdDasServiceNodeSchema adDasServiceNodeSchema = ObjectMapperUtils
                    .fromJSON(instanceData, AdDasServiceNodeSchema.class);
            log.info("preLoadPb() adDasServiceNodeSchema={}", adDasServiceNodeSchema);

            if (!adDasServiceNodeSchema.getPlatformProtoSchema().getPbVersion().
                    equals(dasPublishPbSchema.getPbVersion())) {

                if (dasPublishPbSchema.getPublishType() == 2 && CollectionUtils.isEmpty(dasPublishPbSchema.getPublishInstances())
                        && !dasPublishPbSchema.getPublishInstances().contains(myName)) {
                    log.info("not in prePublish instances!");
                    return;
                }

                AdDasPlatformProtoSchema adDasPlatformProtoSchema = new AdDasPlatformProtoSchema();
                adDasPlatformProtoSchema.setPbVersion(dasPublishPbSchema.getPbVersion());
                adDasPlatformProtoSchema.setPbEnumVersion(adDasPlatformProtoEnumVersionConfig.get()); // TODO 完善Proto

                // 触发 prePb reload
                adDasApiPbLoader.reloadPrePbCache(adDasPlatformProtoSchema);

                // 完成正式 Pb cache 替换
                adDasApiPbLoader.reloadCurPbCache(adDasPlatformProtoSchema);

                AdDasPlatformProtoSchema platformProtoSchema = adDasServiceNodeSchema.getPlatformProtoSchema();
                platformProtoSchema.setPbVersion(dasPublishPbSchema.getPbVersion());
                platformProtoSchema.setPbEnumVersion(dasPublishPbSchema.getPbEnumVersion());
                platformProtoSchema.setPbCommonVersion(dasPublishPbSchema.getPbCommonVersion());
                adDasServiceNodeSchema.setPlatformProtoSchema(platformProtoSchema);
                String adDasServiceNodeSchemaStr = ObjectMapperUtils.toJSON(adDasServiceNodeSchema);
                getCuratorFramework().setData().forPath(myServiceNodePath, adDasServiceNodeSchemaStr.getBytes());

                log.info("preLoadPb() updatePublishStatus()  dasPublishPbSchema={}", dasPublishPbSchema);
                // 更新 ad_das_modify_service_pb_record表中的 publish_status
                adDasModifyServicePbRecordRepository.updatePublishStatus(dasPublishPbSchema.getModifyId(),
                        PUBLISH_SUC.getCode(), adDasServiceNodeSchema.getServiceName(), adDasServiceNodeSchema.getContainerId());

                log.info("【AdDasAbstractZkHelper】preLoadPb() success!");
            } else {
                log.info("pbVersion had been updated! curPbVersion={}, targetPbVersion",
                        adDasServiceNodeSchema.getPlatformProtoSchema().getPbVersion(), dasPublishPbSchema.getPbVersion());
            }
        } catch (Exception e) {
            log.error("preLoadPb() occur error! serviceType={}, serviceEnv={}, eventData={}",
                    platformServiceConfig.getServiceType(), platformServiceConfig.getServiceEnv(), eventData, e);
            perf(AD_DAS_NAMESPACE_NEW, COMMON_PLATFORMLIFE_CYCLE_SUBTAG,
                    "reLoadPbError", "preLoadPb", platformServiceConfig.getServiceEnv(),
                    platformServiceConfig.getServiceType() + ":" + platformServiceConfig.getServiceName()).logstash();
        }
    }


    public static CuratorFramework getCuratorFramework() {
        return curatorFramework;
    }
}
