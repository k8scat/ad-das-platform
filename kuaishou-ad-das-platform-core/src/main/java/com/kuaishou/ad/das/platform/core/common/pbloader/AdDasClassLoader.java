package com.kuaishou.ad.das.platform.core.common.pbloader;

import static com.kuaishou.ad.das.platform.utils.constant.AdDasCommonConstant.AD_DAS_NAMESPACE_NEW;
import static com.kuaishou.ad.das.platform.utils.constant.AdDasCommonConstant.COMMON_PLATFORMLIFE_CYCLE_SUBTAG;
import static com.kuaishou.framework.util.PerfUtils.perf;

import java.net.JarURLConnection;
import java.net.URL;
import java.net.URLClassLoader;
import java.net.URLConnection;
import java.util.List;

import org.apache.commons.compress.utils.Lists;

import lombok.extern.slf4j.Slf4j;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-11-29
 */
@Slf4j
public class AdDasClassLoader extends URLClassLoader {

    /**
     * 访问 jar包URL的连接
     */
    private List<JarURLConnection> cachedJarFile = Lists.newArrayList();

    /**
     * 破坏 系统默认的 类加载器委托规则， 将类加载器的父类加载器设置为 NULL
     */
    public AdDasClassLoader() {
        super(new URL[] {}, null);
    }

    /**
     * 将指定的文件url添加到类加载器的classpath中去，并缓存jar connection，方便以后卸载jar
     * @param
     */
    public void addURLFile(URL file) {
        try {
            // 打开并缓存文件url连接
            URLConnection uc = file.openConnection();
            if (uc instanceof JarURLConnection) {
                uc.setUseCaches(true);
                ((JarURLConnection) uc).getManifest();
                cachedJarFile.add((JarURLConnection)uc);
            }
        } catch (Exception e) {
            log.error("Failed to cache plugin JAR file: " + file.toExternalForm());
        }
        // 指定 jar 包路径
        addURL(file);
    }

    /**
     * 卸载 Jar,  释放包文件缓存
     */
    public void unloadJarFile(){

        for (JarURLConnection jarURLConnection : cachedJarFile) {
            if(jarURLConnection==null){
                return;
            }

            try {
                log.warn("Unloading plugin JAR file " + jarURLConnection.getJarFile().getName());
                jarURLConnection.getJarFile().close();
                jarURLConnection=null;
                System.gc();
            } catch (Exception e) {
                log.error("Failed to unload JAR file\n"+e);
                perf(AD_DAS_NAMESPACE_NEW, COMMON_PLATFORMLIFE_CYCLE_SUBTAG,
                        "unloadPbError", "unloadJarFile").logstash();
            }
        }
    }

    /**
     * 重写自定义类加载器 入口方法
     * @param name
     * @return
     * @throws ClassNotFoundException
     */
    @Override
    public Class<?> loadClass(String name) throws ClassNotFoundException {
        return loadClass(name,false);
    }

    /**
     * 重写 类加载方法
     * 1、指定包路径的 Class
     * 2、其他包路径下的 交给 系统默认类加载器加载
     * @param name
     * @param resolve
     * @return
     * @throws ClassNotFoundException
     */
    @Override
    public Class<?> loadClass(String name,boolean resolve) throws ClassNotFoundException {

        // FIXME 可定制化 放开 加载更多类数据
        if (name.startsWith("com.kuaishou.ad.model.protobuf.tables.All")
                ||name.startsWith("com.kuaishou.ad.model.protobuf")
                ||name.startsWith("com.kuaishou.protobuf.ad")) {
            return customLoad(name, resolve, this);
        }

        // 其他类 交给 系统默认类加载器加载
        return AdDasClassLoader.class.getClassLoader().loadClass(name);
    }

    /**
     * 自定义加载类
     * @param name
     * @param cl
     * @return
     * @throws ClassNotFoundException
     */
    public Class customLoad(String name, boolean resolve, ClassLoader cl) throws ClassNotFoundException {

        //findClass()调用的是URLClassLoader里面重载了ClassLoader的findClass()方法
        Class clazz = ((AdDasClassLoader) cl).findClass(name);

        // 加载Class 时 不进行类链接
        if (resolve) {
            ((AdDasClassLoader) cl).resolveClass(clazz);
        }

        return clazz;
    }

    /**
     * 重写 findClass 方法
     * 1、去指定 jar 包地址 加载Class
     *
     * @param name
     * @return
     * @throws ClassNotFoundException
     */
    @Override
    protected Class<?> findClass(String name) throws ClassNotFoundException {
        try {
            return super.findClass(name);
        } catch (ClassNotFoundException e) {
            log.error("findClass() ocurr error! className={}", name, e);
            perf(AD_DAS_NAMESPACE_NEW, COMMON_PLATFORMLIFE_CYCLE_SUBTAG,
                    "loadPbError", "findClass", name).logstash();
            return getClass().getClassLoader().loadClass(name);
        }
    }
}
