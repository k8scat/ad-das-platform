package com.kuaishou.ad.das.platform.core.benchmark.client;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;

import com.cronutils.model.Cron;
import com.kuaishou.ad.das.platform.core.benchmark.helper.AdDasPlatformBenchmarkClientHelper;
import com.kuaishou.framework.runner.CronScheduledTask;

import kuaishou.common.BizDef;
import lombok.extern.slf4j.Slf4j;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-06-05
 */
@Slf4j
public abstract class AdDasPlatformBenchmarkAbstractClient implements CronScheduledTask {

    @Autowired
    protected AdDasPlatformBenchmarkClientHelper adDasPlatformBenchmarkClientHelper;

    @Autowired
    protected CommonClientLifeCycle commonClientLifeCycle;

    @Override
    public void cronRun() throws Exception {
        log.info("AdDasPlatformBenchmarkAbstractClient() start!");
        commonClientLifeCycle.run();
        log.info("AdDasPlatformBenchmarkAbstractClient() end!");
    }

    @Override
    public void uniqueRunningLockAcquiredCallback() {

        // client 主备切换实现
        adDasPlatformBenchmarkClientHelper.initClient();
    }

    // CHECKSTYLE:OFF
    @Override
    public boolean runAtStart() {
        return adDasPlatformBenchmarkClientHelper.runAtStart();
    }
    // CHECKSTYLE:ON

    @Override
    public Cron cronConfig() {
        return adDasPlatformBenchmarkClientHelper.getCronData();
    }

    @NotNull
    @Override
    public BizDef bizDef() {
        return BizDef.AD_DSP;
    }
}
