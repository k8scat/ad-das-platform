package com.kuaishou.ad.das.platform.core.increment.helper;

import static com.kuaishou.ad.das.platform.utils.kconf.AdDasPlatformKconf.adDasIncrementDataSourceShardKeyConfig;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import com.google.common.collect.Maps;
import com.google.protobuf.ProtocolMessageEnum;
import com.kuaishou.ad.das.platform.core.common.AdDasPlatformServiceCommonInit;
import com.kuaishou.ad.das.platform.core.common.AdDasPlatformServiceConfig;
import com.kuaishou.ad.das.platform.core.increment.lifecycle.AdDasIncrementServiceModelCacher;
import com.kuaishou.ad.das.platform.core.increment.model.AdDasIncrementAdInstanceClassModel;
import com.kuaishou.ad.das.platform.core.increment.model.AdDasPlatformIncrementTableColumnModel;
import com.kuaishou.ad.das.platform.utils.model.kconf.AdDasIncrementFillingTable;
import com.kuaishou.framework.util.HostInfo;
import com.kuaishou.infra.databus.common.domain.mysql.ChangeEventData;
import com.kuaishou.infra.databus.common.domain.mysql.InsertChangedEventRow;
import com.kuaishou.infra.databus.common.domain.mysql.UpdateChangedEventRow;

import lombok.extern.slf4j.Slf4j;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-06-05
 */
@Lazy
@Slf4j
@Component
public class AdDasPlatformIncrementCommonHelper {

    @Autowired
    private AdDasPlatformServiceConfig platformServiceConfig;

    @Autowired
    private AdDasPlatformServiceCommonInit commonInit;

    @Autowired
    private AdDasIncrementServiceModelCacher serviceModelCacher;

    /**
     * 根据环境变量配置，读取 服务数据源名称
     * @return
     */
    public String getDataSourceName() {

        String dataSource = serviceModelCacher.getServiceModel().getDataSource();

        if (HostInfo.debugHost()) {
            return "adDspTest";
        }
       return dataSource;
    }

    /**
     * 增量流数据 topic
     * @return
     */
    public String getTopic() {
        String topic = serviceModelCacher.getServiceModel().getTopic();
        if (HostInfo.debugHost()) {
            if (!topic.contains("test")) {
               return topic + "_test";
            }
        }

        return topic;
    }

    /**
     * 根据环境变量， 读取 服务消费组
     * @return
     */
    public String getConsumerGroup() {

        String consumerGroup = serviceModelCacher.getServiceModel().getConsumerGroup();
        if (HostInfo.debugHost()) {
            return consumerGroup + "_test";
        }
        return consumerGroup;
    }

    /**
     * 根据环境变量， 读取服务处理的表
     * @param tableName
     * @return
     */
    public boolean filterTable(String tableName) {

        return serviceModelCacher
                .getServiceModel().filterTable(tableName);
    }

    /**
     * 通用过滤增量类型
     * @param changeEventData
     * @return
     */
    public boolean filterEventType(ChangeEventData changeEventData) {
        if (changeEventData instanceof InsertChangedEventRow
                || changeEventData instanceof UpdateChangedEventRow) {
            return true;
        }

        return false;
    }

    /**
     * 获取增量服务
     * @return
     */
    public String getClusterShardId() {

        String dataSource = getDataSourceName();

        if (adDasIncrementDataSourceShardKeyConfig.get().containsKey(dataSource)) {

            return adDasIncrementDataSourceShardKeyConfig.get().get(dataSource);
        }

        return "clusterId";
    }

    /**
     * FIXME 注意增加 warmup 类容
     */
    public void tryWarmup() {

    }

    /**
     *
     * @param tableName
     * @return
     */
    public Set<String> getTableColumnData(String tableName) {
        return serviceModelCacher.getServiceModel()
                .getTableSchema(tableName)
                .getColumnSet();
    }

    /**
     * 主表 和 辅助表 会映射到 一个 level 上
     * @param noShardTableName
     * @return
     */
    public String getLevelByTableName(String noShardTableName) {

       return serviceModelCacher.getServiceModel()
               .getTableLevelMap(noShardTableName);
    }

    /**
     * 获取表对应的 primaryKey
     * @param noShardTableName
     * @return
     */
    public String getTablePrimaryKey(String noShardTableName) {

        AdDasPlatformIncrementTableColumnModel incrementTableColumnModel =  serviceModelCacher.getServiceModel()
                .getTableSchema(noShardTableName);
        return incrementTableColumnModel.getPrimaryIdKey();
    }

    /**
     * 获取 AdInstance 类信息
     * @return
     */
    public AdDasIncrementAdInstanceClassModel getInstanceClassModel() {
        return serviceModelCacher.getLastestClassModel();
    }

    /**
     * 获取表对应的 type
     * @param tableName
     * @return
     */
    public ProtocolMessageEnum getAdInstanceType(String tableName) {

        return serviceModelCacher.getServiceModel().getTableType(tableName);
    }

    /**
     * 获取表对应的 sql
     * @return
     */
    public Map<String, String> buildFillingTable2SqlMap() {
        return serviceModelCacher.getServiceModel().getFillingTableSql();
    }

    /**
     * 表 2 字段映射关系
     * @param tableName
     * @return
     */
    public Map<String, String> getTableSchema(String tableName) {

        AdDasPlatformIncrementTableColumnModel tableColumnModel = serviceModelCacher.getServiceModel()
                .getTableSchema(tableName);
        return tableColumnModel.getColumnTypeMap();
    }

    /**
     * 横级联填充关系
     * @return
     */
    public AdDasIncrementFillingTable getFillingTable() {
        return  serviceModelCacher.getServiceModel().getFillingTable();
    }

    /**
     * level 2 type map
     * @return
     */
    public Map<String, ProtocolMessageEnum> getDasLevelTypeMap() {
        return  serviceModelCacher.getServiceModel().getLevel2TypeMap();
    }

    /**
     * 表关系权重
     * @return
     */
    public Map<String, Integer> getTableWeightMapCascade() {

        return Maps.newConcurrentMap();
    }

    /**
     * 获取数据 主键 key
     * @param row
     * @return
     */
    public Object getHashKeyForKbus(String tableName, Map<String, Serializable> row) {

        // 配置化 获取主键id
        Map<String, AdDasPlatformIncrementTableColumnModel> tableColumnMap = serviceModelCacher
                .getServiceModel().getIncrementTableSchemaModel().getTableColumnMap();
        AdDasPlatformIncrementTableColumnModel incrementTableColumnModel = tableColumnMap.get(tableName);
        String primaryKey = incrementTableColumnModel.getPrimaryIdKey();

        if (!StringUtils.isEmpty(primaryKey)) {

            return row.get(primaryKey);
        } else {
           return row.hashCode();
        }
    }

    /**
     * 获取数据主键 id
     * @param tableName
     * @param row
     * @return
     */
    public Long getDataId(String tableName, Map<String, Serializable> row) {

        String primaryKey = "";
        // 配置化 获取主键id
        Map<String, AdDasPlatformIncrementTableColumnModel> tableColumnMap = serviceModelCacher
                .getServiceModel().getIncrementTableSchemaModel().getTableColumnMap();
        AdDasPlatformIncrementTableColumnModel incrementTableColumnModel = tableColumnMap.get(tableName);
        primaryKey = incrementTableColumnModel.getPrimaryIdKey();

        if (StringUtils.isEmpty(primaryKey)) {
            throw new RuntimeException("Table=" + tableName + " must be setted with id");
        }

        return ((Number) row.get(primaryKey)).longValue();
    }

    /**
     * 转换 横级联填充查询 数据
     * @param queryDbDataMap
     * @return
     */
    public Map<String, Map<Long, Map<String, Object>>> buildId2DataMap(
            Map<String, List<Map<String, Object>>> queryDbDataMap) {
        Map<String, Map<Long, Map<String, Object>>>  result = Maps.newHashMap();

        queryDbDataMap.forEach((key, value) -> {
            Map<Long, Map<String, Object>> dataResult = Maps.newHashMap();

            for (Map<String, Object> data : value) {
                dataResult.put(getPrimaryId(key, data), data);
            }
            result.put(key, dataResult);
        });

        return result;
    }

    /**
     * 获取数据 主键
     * @param tableName
     * @param data
     * @return
     */
    public Long getPrimaryId(String tableName, Map<String, Object> data) {

        String primaryKey = "";

        // 配置化 获取主键id
        Map<String, AdDasPlatformIncrementTableColumnModel> tableColumnMap = serviceModelCacher
                .getServiceModel().getIncrementTableSchemaModel().getTableColumnMap();
        AdDasPlatformIncrementTableColumnModel incrementTableColumnModel = tableColumnMap.get(tableName);
        primaryKey = incrementTableColumnModel.getPrimaryIdKey();

        if (StringUtils.isEmpty(primaryKey)) {
            throw new RuntimeException("Table=" + tableName + " must be setted with id");
        }

        if (HostInfo.debugHost()) {
            log.info("getPrimaryId() tableName={}, primaryKey={}", tableName, primaryKey);
        }

        if (data.containsKey(primaryKey)) {
            return ((Number) data.get(primaryKey)).longValue();
        } else {
            throw new RuntimeException("Table=" + tableName + " id=" + primaryKey + " is empty!");
        }
    }

}
