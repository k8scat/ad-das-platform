package com.kuaishou.ad.das.platform.core.benchmark.server;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kuaishou.ad.das.platform.core.benchmark.server.watcher.AdDasBenchmarkBenchmarkWorkerMasterWatcher;

import lombok.extern.slf4j.Slf4j;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-07-05
 */
@Slf4j
@Service
public class CommonServerWorkerMasterLifeCycle {

    @Autowired
    private AdDasBenchmarkBenchmarkWorkerMasterWatcher workerMasterWatcher;

    /**
     * worker master 启动后，会不断进行巡查
     * 1. 巡查 任务 信息
     * 2. 巡查 worker实例 信息
     * 3. 监控报警
     */
    public void run() {

        checkWorkerStatus();

        checkWorkerStatus();
    }

    /**
     * 检查master 所管理的 worker、以及当前worker执行的 任务信息
     */
    private void checkWorkerStatus() {

    }

    /**
     * 检查集群中所管理的 client 任务运行进度， 对于超时client流，进行监控报警
     */
    private void checkoutClientTask() {


    }
}
