package com.kuaishou.ad.das.platform.core.common.service;

import static com.kuaishou.ad.das.platform.core.common.AdDasBenchmarkZkModel.PLATFORM_COMMON_PB;
import static com.kuaishou.ad.das.platform.utils.constant.AdDasCommonConstant.AD_DAS_NAMESPACE_NEW;
import static com.kuaishou.ad.das.platform.utils.constant.AdDasCommonConstant.COMMON_PLATFORMLIFE_CYCLE_SUBTAG;
import static com.kuaishou.framework.util.PerfUtils.perf;

import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.data.Stat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import com.kuaishou.ad.das.platform.core.common.daszk.Helper.AdDasPlatformCommonZkHelper;
import com.kuaishou.ad.das.platform.core.common.daszk.model.AdDasPublishPbSchema;
import com.kuaishou.ad.das.platform.core.common.daszk.model.AdDasPublishPbSucSchema;
import com.kuaishou.ad.das.platform.dal.model.AdDasModify;
import com.kuaishou.ad.das.platform.dal.model.AdDasModifyPbMsg;
import com.kuaishou.ad.das.platform.dal.repository.AdDasModifyPbMsgRepository;
import com.kuaishou.ad.das.platform.dal.repository.AdDasModifyRepository;
import com.kuaishou.ad.das.platform.dal.repository.AdDasServicePbRepository;
import com.kuaishou.ad.das.platform.utils.dasenum.AdDasModifyStatusEnum;
import com.kuaishou.ad.das.platform.utils.dasenum.AdDasModifyTypeEnum;
import com.kuaishou.framework.util.ObjectMapperUtils;

import lombok.extern.slf4j.Slf4j;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-02-21
 */
@Lazy
@Slf4j
@Service
public class AdDasModifyCronService {

    @Autowired
    private AdDasModifyPbMsgRepository adDasModifyPbMsgRepository;

    @Autowired
    private AdDasServicePbRepository adDasServicePbRepository;

    @Autowired
    private AdDasModifyRepository adDasModifyRepository;

    @Autowired
    private AdDasPlatformCommonZkHelper adDasCommonZkHelper;

    @Autowired
    private AdDasModifyPbService adDasModifyPbService;

    /**
     * 扫描正在发布中的工单数据
     *
     */
    public void scanModifyData() {

        //  查询 正在发布中的 变更Pb工单
        scanModifyPbData();
    }

    public void scanModifyPbData() {
        List<AdDasModify> modifies = adDasModifyRepository.queryByStatus(AdDasModifyTypeEnum.MODIFY_PB.getCode(),
                AdDasModifyStatusEnum.PUBLISH_ING.getCode());

        log.info("scanModifyPbData() publishing data modifies={}", modifies);
        for (AdDasModify adDasModify : modifies) {
            Long modifyId = adDasModify.getId();
            log.info("scanModifyPbData() publishing data modifyId={}", modifyId);
            List<AdDasModifyPbMsg> adDasModifyPbMsgs = adDasModifyPbMsgRepository.queryByModifyId(modifyId);
            if (CollectionUtils.isEmpty(adDasModifyPbMsgs)) {
                log.info("scanModifyPbData() modifyId={} adDasModifyPbMsgs is empty!", modifyId);
                return;
            }

            AdDasModifyPbMsg adDasModifyPbMsg = adDasModifyPbMsgs.get(0);

            String serviceType = adDasModifyPbMsg.getServiceType();
            String serviceName = adDasModifyPbMsg.getServiceName();
            log.info("scanModifyPbData() publishing data adDasModifyPbMsg={}", adDasModifyPbMsg);
            try {
                if (adDasCommonZkHelper.checkPublishStatus(serviceType, serviceName, adDasModifyPbMsg)) {

                    // 处理DB 工单数据
                    adDasModifyPbService.processModifyPb(serviceType, serviceName, adDasModifyPbMsg);

                    // 更新 curPb 状态
                    String curPbPath = PLATFORM_COMMON_PB + "/" + (serviceType) + "/" + serviceName + "/curpb";
                    Stat statCurPb =  adDasCommonZkHelper.getCuratorFramework().checkExists().forPath(curPbPath);
                    if (statCurPb == null) {
                        adDasCommonZkHelper.getCuratorFramework().create().creatingParentsIfNeeded().withMode(CreateMode.PERSISTENT)
                                .forPath(curPbPath);
                    }
                    // 写入 /curpb 用于重启时直接读取该版本数据
                    AdDasPublishPbSchema dasPublishPbSchema = new AdDasPublishPbSchema();
                    dasPublishPbSchema.setModifyId(adDasModifyPbMsg.getModifyId());
                    dasPublishPbSchema.setPbVersion(adDasModifyPbMsg.getTargetPbVersion());
                    dasPublishPbSchema.setPbEnumVersion(adDasModifyPbMsg.getTargetPbEnumVersion());
                    dasPublishPbSchema.setPbCommonVersion(adDasModifyPbMsg.getTargetPbCommonVersion());
                    String adDasPublishPbSchemaStr = ObjectMapperUtils.toJSON(dasPublishPbSchema);
                    adDasCommonZkHelper.getCuratorFramework().setData().forPath(curPbPath, adDasPublishPbSchemaStr.getBytes());
                }
            } catch (Exception e) {
                log.error("scanModifyPbData() adDasModify={}", adDasModify, e);
                perf(AD_DAS_NAMESPACE_NEW, COMMON_PLATFORMLIFE_CYCLE_SUBTAG,
                        "scanModifyDataError", "scanModifyPbData",
                        serviceType + ":" + serviceName + ":" + modifyId).logstash();
            }
        }
    }

}
