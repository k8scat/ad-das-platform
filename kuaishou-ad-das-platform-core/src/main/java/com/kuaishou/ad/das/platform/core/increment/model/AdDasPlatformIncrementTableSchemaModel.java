package com.kuaishou.ad.das.platform.core.increment.model;

import java.util.Map;

import com.google.protobuf.ProtocolMessageEnum;
import com.kuaishou.ad.das.platform.utils.model.kconf.AdDasIncrementFillingTable;

import lombok.Data;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-06-05
 */
@Data
public class AdDasPlatformIncrementTableSchemaModel {

    /**
     * 1.数据表 与 pb 枚举映射关系
     */
    private Map<String, ProtocolMessageEnum> table2TypeMap;

    /**
     * 2.数据表 与Level 映射关系
     */
    private Map<String, String> table2LevelMap;

    /**
     * 3. 数据层 2 pb类枚举关系
     */
    private Map<String, ProtocolMessageEnum> tableLevel2TypeMap;

    /**
     * 4. 数据表 字段映射关系
     */
    private Map<String, AdDasPlatformIncrementTableColumnModel> tableColumnMap;

    /**
     * 5.数据横级联表关系
     */
    private AdDasIncrementFillingTable incrementFillingTable;

    /**
     * 6. 表 2 横级联sql 关系
     */
    private Map<String, String> table2FillingSqlMap;

}
