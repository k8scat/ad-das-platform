package com.kuaishou.ad.das.platform.core.benchmark.client;

import static com.kuaishou.ad.das.platform.core.utils.AdBaseUtils.getNowTimestamp;
import static com.kuaishou.ad.das.platform.utils.constant.AdDasCommonConstant.AD_DAS_NAMESPACE_NEW;
import static com.kuaishou.ad.das.platform.utils.constant.AdDasCommonConstant.Ad_DAS_BENCHMARK_CLIENT;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.time.StopWatch;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.kuaishou.ad.das.platform.core.benchmark.client.checker.AdDasPLatformBenchmarkChecker;
import com.kuaishou.ad.das.platform.core.benchmark.client.manager.AdDasPlatformBenchmarkManager;
import com.kuaishou.ad.das.platform.core.benchmark.client.publisher.AdDasBenchmarkPublisher;
import com.kuaishou.ad.das.platform.core.benchmark.helper.AdDasBenchmarkCommonQueryHelper;
import com.kuaishou.ad.das.platform.core.benchmark.helper.AdDasBenchmarkPlatformClientZkHelper;
import com.kuaishou.ad.das.platform.core.benchmark.model.AdDasPlatformClientCheckMsgModel;
import com.kuaishou.ad.das.platform.core.benchmark.model.AdDasPlatformClientTaskBatchModel;
import com.kuaishou.ad.das.platform.core.common.AdDasPlatformServiceConfig;
import com.kuaishou.ad.model.protobuf.tables.Meta;
import com.kuaishou.framework.util.PerfUtils;

import lombok.extern.slf4j.Slf4j;

/**
 * client声明周期：
 * 1. 检查重跑任务
 * 2. 发布任务
 * 3. 监听任务状态
 * 4. 校验数据
 * 5. 汇总任务
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-02-21
 */
@Slf4j
@Service
public class CommonClientLifeCycle {

    @Autowired
    private AdDasBenchmarkPlatformClientZkHelper commonZkHelper;

    @Autowired
    protected AdDasBenchmarkPublisher adDasBenchmarkPublisher;

    @Autowired
    protected AdDasPLatformBenchmarkChecker adDasPLatformBenchmarkChecker;

    @Autowired
    protected AdDasPlatformBenchmarkManager adDasPlatformBenchmarkManager;

    @Autowired
    protected AdDasBenchmarkCommonQueryHelper commonQueryHelper;

    @Autowired
    protected AdDasPlatformServiceConfig platformServiceConfig;

    /**
     * 1、根据基准名称获取任务列表
     * 2、判断是否是重跑，清理dump_done 和 dump_info文件
     * 3、根据集群实例信息，简版预分配任务
     * 4、发布任务
     * 5、启动定时巡查任务
     * 6、校验任务结果
     * 7、汇总任务结果
     * 8、
     * 9、
     * 10、
     */
    public void run() {

        // 取任务执行的整点时间
        Long taskTimestamp = getNowTimestamp();
        log.info("starts run() taskTimestamp={}", taskTimestamp);

        StopWatch watchRun = StopWatch.createStarted();
        try {

            // 1、根据基准名称获取任务列表
            AdDasPlatformClientTaskBatchModel clientTaskModel = commonQueryHelper
                    .queryClientTaskModel(platformServiceConfig.getServiceName());
            clientTaskModel.setTimestamp(taskTimestamp);
            log.info("starts run() clientTaskModel={}", clientTaskModel);

            if (clientTaskModel == null || (clientTaskModel != null && CollectionUtils.isEmpty(clientTaskModel.getTaskIds()))) {

                log.info("starts run() clientTaskModel is null!");
                return;
            }

            // 2、判断是否是重跑，清理dump_done 和 dump_info文件
            adDasPLatformBenchmarkChecker.checkAndDealDumpInfo(taskTimestamp, clientTaskModel.getHdfsPath());

            // 3、发布任务
            adDasBenchmarkPublisher.publishTask(clientTaskModel);

            // 4、启动定时巡查任务
            List<String> doneContents = runCheckTask(clientTaskModel);

            Meta.DumpInfo dumpInfo = adDasPlatformBenchmarkManager.buildDumpInfo(doneContents, clientTaskModel);

            // 5、校验任务结果
            adDasPLatformBenchmarkChecker.checkFinalData(dumpInfo, clientTaskModel);

            // 6、汇总任务结果
            adDasPlatformBenchmarkManager.managerAllTasks(dumpInfo, clientTaskModel);

            // 7、校验dump 信息
            adDasPLatformBenchmarkChecker.checkDump(clientTaskModel);

            // 8、新增统计数据文件大小
            adDasPLatformBenchmarkChecker.checkDataFileSize(clientTaskModel);
        } catch (Exception e) {
            log.error("run() ocurr error! taskTimestamp={}", taskTimestamp, e);
            PerfUtils.perf(AD_DAS_NAMESPACE_NEW, Ad_DAS_BENCHMARK_CLIENT,
                    platformServiceConfig.getServiceType(), platformServiceConfig.getServiceName(), "error")
                    .logstash();
        }

        watchRun.stop();
        PerfUtils.perf(AD_DAS_NAMESPACE_NEW, Ad_DAS_BENCHMARK_CLIENT,
                platformServiceConfig.getServiceType(), platformServiceConfig.getServiceName(), "success")
                .micros(watchRun.getTime(TimeUnit.MICROSECONDS))
                .logstash();
    }

    /**
     * 定时巡查 & 重试任务
     * @param clientTaskModel
     */
    public List<String> runCheckTask(AdDasPlatformClientTaskBatchModel clientTaskModel) {
        // 定时监听节点进度
        int countIndex = 1;
        AdDasPlatformClientCheckMsgModel clientCheckMsgModel = adDasPLatformBenchmarkChecker.checkAllTask(clientTaskModel);
        while (!clientCheckMsgModel.isSuccessFlag()) {
            try {

                log.info("【NOTICE WARNNING】countIndex={}, taskNum={} is doing !!!",
                        countIndex++, clientCheckMsgModel.getDoingTaskNums());

                clientCheckMsgModel = adDasPLatformBenchmarkChecker.checkAllTask(clientTaskModel);
                Thread.sleep(1000);
            } catch (Exception e) {
                log.error("runCheckTask() ocurr error! clientTaskModel={}", clientTaskModel, e);
            }
        }

        return clientCheckMsgModel.getDoneContents();
    }
}
