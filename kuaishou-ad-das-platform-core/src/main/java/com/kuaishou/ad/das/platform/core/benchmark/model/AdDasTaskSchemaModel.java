package com.kuaishou.ad.das.platform.core.benchmark.model;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.lang.StringUtils;

import com.kuaishou.framework.util.ObjectMapperUtils;

import lombok.Data;

/**
 * 基准任务 元数据
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-01-30
 */
@Data
public class AdDasTaskSchemaModel {

    private Integer taskNum;

    private String taskName;

    private String dataSource;

    private String mainTable;

    private String pbClass;

    private String pbClassFullName;

    private Long timestamp;

    /**
     * 表类型： 0-单表 1-分表
     */
    private Integer tableType;

    /**
     * 公共SQL
     */
    private List<String> commonSqls;

    /**
     * 时间参数 ： -1，-2 逗号分隔
     */
    private List<Integer> timeParams;

    /**
     * 分表 shardId list
     */
    private List<Integer> taskShardList;

    /**
     * 分表执行并发度
     */
    private Integer concurrency;

    /**
     * task 自定义sql flag
     */
    private Integer taskSqlFlag;

    /**
     * 任务自定义sql
     */
    private List<String> taskSqls;

    /**
     * 字段 db 2 pb 映射关系
     */
    private List<AdDasBenchmarkDb2PbColumnMsgView> columnSchema;

    /**
     * 级联表字段 映射关系
     */
    private List<AdDasBenchmarkCascadeTableView> cascadeTableSchemas;

    private String hdfsPath;

    /**
     * 转换 task 表数据
     * @param dataMap
     */
    public void buildTaskData(Map<String, Object> dataMap) {

        this.setDataSource((String) dataMap.get("data_source"));
        this.setMainTable((String) dataMap.get("main_table"));
        this.setPbClass((String) dataMap.get("pb_class"));
        this.setPbClassFullName((String) dataMap.get("pb_class_full_name"));
        this.setTableType((Integer) dataMap.get("table_type"));

        String taskShardListStr = (String) dataMap.get("task_shard_list");
        List<Integer> taskShardList = ObjectMapperUtils
                .fromJSON(taskShardListStr, List.class, Integer.class);
        this.setTaskShardList(taskShardList);

        this.setConcurrency((Integer) dataMap.get("concurrency"));
        this.setTaskSqlFlag((Integer) dataMap.get("task_sql_flag"));

        String taskSqlsStr = (String) dataMap.get("task_sqls");
        String[] taskSqlsStrs = taskSqlsStr.split(";");
        List<String> taskSqls = Arrays.asList(taskSqlsStrs.clone());
        this.setTaskSqls(taskSqls);
    }

    /**
     * 转换 taskCommonDetail 数据
     * @param dataMap
     */
    public void buildTaskCommonDetailData(Map<String, Object> dataMap) {


        String commonSqlsStr = (String) dataMap.get("common_sqls");
        String[] commonSqlsStrs = commonSqlsStr.split(";");
        List<String> commonSqls = Arrays.asList(commonSqlsStrs.clone());
        this.setCommonSqls(commonSqls);

        String columnSchemaStr = (String) dataMap.get("column_schema");
        List<AdDasBenchmarkDb2PbColumnMsgView> columnSchema = ObjectMapperUtils
                .fromJSON(columnSchemaStr, List.class, AdDasBenchmarkDb2PbColumnMsgView.class);
        this.setColumnSchema(columnSchema);

        String timeParamsStr = (String) dataMap.get("time_params");
        if (!StringUtils.isEmpty(timeParamsStr)) {
            List<Integer> timeParam = Arrays.stream(timeParamsStr.split(","))
                    .filter(time -> !StringUtils.isEmpty(time))
                    .map(time -> Integer.valueOf(time))
                    .collect(Collectors.toList());
            this.setTimeParams(timeParam);
        }

        String cascadeTableSchema = (String) dataMap.get("cascade_column_schema");
        List<AdDasBenchmarkCascadeTableView> cascadeTableSchemas = ObjectMapperUtils
                .fromJSON(cascadeTableSchema, List.class, AdDasBenchmarkCascadeTableView.class);
        this.setCascadeTableSchemas(cascadeTableSchemas);
    }

}
