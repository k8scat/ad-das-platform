package com.kuaishou.ad.das.platform.core.common;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


import com.kuaishou.ad.das.platform.core.benchmark.server.converter.AdDasBenchmarkCoverter;
import com.kuaishou.ad.das.platform.core.benchmark.server.exporter.AdDasBenchmarkExporter;
import com.kuaishou.ad.das.platform.core.benchmark.server.fetcher.AdDasBenchmarkFetcher;
import com.kuaishou.ad.das.platform.core.benchmark.server.merger.AdDasBenchmarkMerger;

import lombok.extern.slf4j.Slf4j;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-01-30
 */
@Slf4j
public class AdDasBenchmarkFactory {

    private static final Map<String, AdDasBenchmarkCoverter> DAS_BENCHMARK_COVERTER_MAP =
            new ConcurrentHashMap<String, AdDasBenchmarkCoverter>();

    private static final Map<String, AdDasBenchmarkFetcher> DAS_BENCHMARK_FETCHER_MAP =
            new ConcurrentHashMap<String, AdDasBenchmarkFetcher>();

    private static final Map<String, AdDasBenchmarkMerger> DAS_BENCHMARK_MERGER_MAP =
            new ConcurrentHashMap<String, AdDasBenchmarkMerger>();

    private static final Map<String, AdDasBenchmarkExporter> DAS_BENCHMARK_EXPORTER_MAP =
            new ConcurrentHashMap<String, AdDasBenchmarkExporter>();


    public static void registerConverter(String tableName, AdDasBenchmarkCoverter coverter) {
        DAS_BENCHMARK_COVERTER_MAP.put(tableName, coverter);
    }

    public static void registerFetcher(String tableName, AdDasBenchmarkFetcher fetcher) {
        DAS_BENCHMARK_FETCHER_MAP.put(tableName, fetcher);
    }

    public static void registerMerger(String tableName, AdDasBenchmarkMerger merger) {
        DAS_BENCHMARK_MERGER_MAP.put(tableName, merger);
    }

    public static void registerExporter(String tableName, AdDasBenchmarkExporter exporter) {
        DAS_BENCHMARK_EXPORTER_MAP.put(tableName, exporter);
    }

    public static boolean judgeSpConverter(String tableName) {
        return DAS_BENCHMARK_COVERTER_MAP.containsKey(tableName);
    }

    public static boolean judgeSpFetcher(String tableName) {
        return DAS_BENCHMARK_FETCHER_MAP.containsKey(tableName);
    }

    public static boolean judgeSpMerger(String tableName) {
        return DAS_BENCHMARK_MERGER_MAP.containsKey(tableName);
    }

    public static boolean judgeSpExporter(String tableName) {
        return DAS_BENCHMARK_EXPORTER_MAP.containsKey(tableName);
    }

    public static AdDasBenchmarkCoverter getConverter(String tableName) {
        return DAS_BENCHMARK_COVERTER_MAP.get(tableName);
    }

    public static AdDasBenchmarkFetcher getFetcher(String tableName) {
        return DAS_BENCHMARK_FETCHER_MAP.get(tableName);
    }

    public static AdDasBenchmarkMerger getMerger(String tableName) {
        return DAS_BENCHMARK_MERGER_MAP.get(tableName);
    }

    public static AdDasBenchmarkExporter getExporter(String tableName) {
        return DAS_BENCHMARK_EXPORTER_MAP.get(tableName);
    }

}
