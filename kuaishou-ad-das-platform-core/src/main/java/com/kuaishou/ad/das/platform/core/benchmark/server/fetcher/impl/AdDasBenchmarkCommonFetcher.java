package com.kuaishou.ad.das.platform.core.benchmark.server.fetcher.impl;

import static com.google.common.util.concurrent.MoreExecutors.shutdownAndAwaitTermination;
import static com.kuaishou.ad.das.platform.utils.kconf.AdDasBaseCommonKconf.AD_DAS_COMMON_BENCHMARK_FETCHER_THREAD;
import static com.kuaishou.framework.concurrent.DynamicThreadExecutor.dynamic;
import static com.kuaishou.infra.framework.common.util.TermHelper.addTerm;
import static java.util.concurrent.TimeUnit.MINUTES;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Service;

import com.kuaishou.ad.das.platform.core.benchmark.server.fetcher.AdDasBenchmarkFetcher;
import com.kuaishou.ad.das.platform.core.benchmark.model.AdDasTaskHandleModel;
import com.kuaishou.ad.das.platform.core.benchmark.model.AdDasTaskSchemaModel;

import lombok.extern.slf4j.Slf4j;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-01-29
 */
@Slf4j
@Service
public class AdDasBenchmarkCommonFetcher
        extends AdDasBenchmarkAbstractFetcher
        implements AdDasBenchmarkFetcher {

    @Override
    public AdDasTaskHandleModel fetch(AdDasTaskSchemaModel adDasTaskSchemaModel, AdDasTaskHandleModel taskHandleModel) {

        if (taskHandleModel.getFetcherType() == 0) {
            AdDasTaskHandleModel result = defaultfetchFromDb(adDasTaskSchemaModel, taskHandleModel);
            return result;
        }  else {
            log.error("fetch() taskHandleModel={}, fetcherType must be configed!", taskHandleModel);
            return null;
        }
    }

    @PostConstruct
    public void init() {
        commonBenchmarkFetcherExecutor = dynamic(AD_DAS_COMMON_BENCHMARK_FETCHER_THREAD::get,
                "ad-das-common-benchmark-fetcher-pool");
        // TermHelper.addTerm 关机时注册关闭逻辑
        addTerm(() -> shutdownAndAwaitTermination(commonBenchmarkFetcherExecutor, 10, MINUTES));

    }
}
