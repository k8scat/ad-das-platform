package com.kuaishou.ad.das.platform.core.common.service;

import static com.kuaishou.ad.das.platform.utils.constant.AdDasCommonConstant.AD_DAS_NAMESPACE_NEW;
import static com.kuaishou.ad.das.platform.utils.constant.AdDasCommonConstant.COMMON_PLATFORMLIFE_CYCLE_SUBTAG;
import static com.kuaishou.framework.util.PerfUtils.perf;

import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kuaishou.ad.das.platform.core.common.daszk.model.AdDasPublishPbSucSchema;
import com.kuaishou.ad.das.platform.dal.model.AdDasModifyPbMsg;
import com.kuaishou.ad.das.platform.dal.model.AdDasServicePb;
import com.kuaishou.ad.das.platform.dal.repository.AdDasModifyRepository;
import com.kuaishou.ad.das.platform.dal.repository.AdDasServicePbRepository;
import com.kuaishou.ad.das.platform.utils.dasenum.AdDasModifyStatusEnum;
import com.kuaishou.ad.das.platform.utils.dasenum.AdDasStatusEnum;

import lombok.extern.slf4j.Slf4j;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-01-20
 */
@Slf4j
@Service
public class AdDasModifyPbService {

    @Autowired
    private AdDasServicePbRepository adDasServicePbRepository;

    @Autowired
    private AdDasModifyRepository adDasModifyRepository;

    /**
     * 查询 service pb
     * @param serviceName
     * @return
     */
    public List<AdDasServicePb> queryServicePb(String serviceName) {
        List<AdDasServicePb> servicePbs = adDasServicePbRepository
                .queryByService(serviceName, AdDasStatusEnum.DEFAULT.getCode());

        return servicePbs;
    }

    /**
     * 更新发布Pb状态
     * @param serviceType
     * @param serviceName
     * @param adDasModifyPbMsg
     */
    public void processModifyPb(String serviceType, String serviceName,
                                AdDasModifyPbMsg adDasModifyPbMsg ) {

        try {

            // 更新 变更工单表状态 ad_das_modify
            adDasModifyRepository.updateStatus(adDasModifyPbMsg.getModifyId(),
                    AdDasModifyStatusEnum.PUBLISH_SUC.getCode(),
                    System.currentTimeMillis());

            List<AdDasServicePb> adDasServicePbs = adDasServicePbRepository
                    .queryByService(serviceName, AdDasStatusEnum.DEFAULT.getCode());
            if (CollectionUtils.isEmpty(adDasServicePbs)) {
                // insert
                AdDasServicePb adDasServicePb = new AdDasServicePb();
                adDasServicePb.setServiceType(serviceType);
                adDasServicePb.setServiceName(serviceName);
                adDasServicePb.setServicePbStatus(AdDasStatusEnum.DEFAULT.getCode());
                adDasServicePb.setPbVersion(adDasModifyPbMsg.getTargetPbVersion());
                adDasServicePb.setCreateTime(System.currentTimeMillis());
                adDasServicePb.setOperator("系统");
                adDasServicePb.setUpdateTime(System.currentTimeMillis());
                log.info("insert adDasServicePb={}", adDasServicePb);

                adDasServicePbRepository.insert(adDasServicePb);
            } else {
                // 更新 服务Pb表数据 ad_das_service_pb
                log.info("update serviceName={}, serviceType={}", serviceName, serviceType);
                adDasServicePbRepository.update(serviceName, serviceType,
                        adDasModifyPbMsg.getTargetPbVersion(), System.currentTimeMillis());
            }

        } catch (Exception e) {
            log.error("processModifyPb() serviceType={}, serviceName={}, adDasModifyPbMsg={}",
                    serviceType, serviceName, adDasModifyPbMsg, e);
            perf(AD_DAS_NAMESPACE_NEW, COMMON_PLATFORMLIFE_CYCLE_SUBTAG,
                    "publishPbError", "processModifyPb",
                    "", serviceType + ":" + serviceName).logstash();
        }
    }
}
