package com.kuaishou.ad.das.platform.core.common.daszk.model;

import java.io.Serializable;
import java.util.List;

import lombok.Data;

/**
 * 写入 /curpb  和 /prepb 的数据schema
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-12-09
 */
@Data
public class AdDasPublishPbSchema implements Serializable {

    private static final long serialVersionUID = -3270197672485028718L;

    private Long modifyId;

    private String pbVersion;

    private String pbEnumVersion;

    private String pbCommonVersion;

    private List<String> publishInstances;

    /**
     * 1-全量发布 2-灰度发布 3-全量回滚
     */
    private Integer publishType;
}
