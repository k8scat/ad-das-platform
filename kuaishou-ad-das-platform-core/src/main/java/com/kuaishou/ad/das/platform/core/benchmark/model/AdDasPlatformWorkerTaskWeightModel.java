package com.kuaishou.ad.das.platform.core.benchmark.model;

import java.io.Serializable;

import lombok.Data;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-07-05
 */
@Data
public class AdDasPlatformWorkerTaskWeightModel implements Serializable {

    private static final long serialVersionUID = -4690301384555679947L;

    /**
     * 实例名称
     */
    private String workerName;

    /**
     * 实例运行的任务数
     */
    private Integer taskCount = 0;

}
