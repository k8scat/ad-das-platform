package com.kuaishou.ad.das.platform.core.increment.transfer;

import java.util.List;
import java.util.Map;


import com.kuaishou.ad.das.platform.core.increment.model.AdDasCascadeModel;
import com.kuaishou.ad.das.platform.core.increment.model.AdDasIncrementAdapterModel;

/**
 * 增量数据处理器接口:
 *
 * 1、perf 打点 监控
 * 2、批量级联转换: 主要处理批量查DB、 特殊字段处理
 * 3、发送kafka
 *
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-10-30
 */
public interface AdDasKbusBatchTransfer<D extends AdDasCascadeModel> {


    List<D> doBatchTransfer(
            String tableName, Long accountId, List<AdDasIncrementAdapterModel> adDasIncrementAdapterObjects);

    void convertFillingMore(String fillingTableName, Map<String, Object> data, AdDasCascadeModel adDasCascadeModel);
}
