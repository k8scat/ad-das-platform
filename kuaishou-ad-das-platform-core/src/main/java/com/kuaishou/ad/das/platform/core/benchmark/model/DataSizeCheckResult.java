package com.kuaishou.ad.das.platform.core.benchmark.model;

import java.io.Serializable;
import java.util.Map;

import lombok.Data;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-08-09
 */
@Data
public class DataSizeCheckResult implements Serializable {

    private static final long serialVersionUID = 3542202534444842616L;

    private Long timestamp;

    private String masterType;

    /**
     * 各层级表数据条数
     */
    private Map<String, Long> tableName2DataCountMap;

    /**
     * 各层数据对应的文件大小
     */
    private Map<String, Long> tableName2DataFileSizeMap;

    public DataSizeCheckResult() {

    }

    public DataSizeCheckResult(Long timestamp, String masterType) {
        this.timestamp = timestamp;
        this.masterType = masterType;
    }
}
