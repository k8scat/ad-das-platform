package com.kuaishou.ad.das.platform.core.benchmark.server.fetcher.impl;

import static com.kuaishou.ad.das.platform.core.utils.AdBaseUtils.getDaysBefore;

import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import com.google.common.collect.Lists;
import com.kuaishou.ad.das.dal.datasource.AdCoreDataSource;
import com.kuaishou.ad.das.platform.core.benchmark.helper.AdDasBenchmarkCommonQueryHelper;
import com.kuaishou.ad.das.platform.core.benchmark.model.AdDasTaskHandleModel;
import com.kuaishou.ad.das.platform.core.benchmark.model.AdDasTaskSchemaModel;
import com.kuaishou.framework.concurrent.DynamicThreadExecutor;

import lombok.extern.slf4j.Slf4j;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-01-30
 */
@Slf4j
public abstract class AdDasBenchmarkAbstractFetcher {

    protected static DynamicThreadExecutor commonBenchmarkFetcherExecutor;

    @Autowired
    private AdDasBenchmarkCommonQueryHelper adDasBenchmarkCommonQueryHelper;

    protected AdDasTaskHandleModel defaultfetchFromDb(AdDasTaskSchemaModel adDasTaskSchemaModel,
                                                            AdDasTaskHandleModel taskHandleModel) {

        List<AdDasTaskHandleModel> result = Lists.newArrayList();

        List<Map<String,Object>> dataObjectMaps = Lists.newArrayList();

        Integer tableType = adDasTaskSchemaModel.getTableType();
        List<String> sqls = adDasTaskSchemaModel.getCommonSqls();
        if (adDasTaskSchemaModel.getTaskSqlFlag() == 1) {
            sqls = adDasTaskSchemaModel.getTaskSqls();
        }

        List<Object> params = Lists.newArrayList();
        // 时间参数处理
        if (!CollectionUtils.isEmpty(adDasTaskSchemaModel.getTimeParams())) {
            adDasTaskSchemaModel.getTimeParams().forEach(timeParam -> {
                params.add(getDaysBefore(timeParam, taskHandleModel.getTimestamp()));
            });
        }

        String dataSource  = adDasTaskSchemaModel.getDataSource();
        log.info("dasRun() params={}, sqls={}, dataSource={}, tableType={}", params,
                sqls, dataSource, tableType);

        if (tableType == 0) {
            // 单表处理
            dataObjectMaps.addAll(queryDb(null, sqls, params, dataSource));
        } else {
            //  分表处理， 按照shardIdList 分并发处理
            List<List<Integer>> shardIdListSubs = Lists.partition(adDasTaskSchemaModel.getTaskShardList(),
                    adDasTaskSchemaModel.getConcurrency());

            List<CompletableFuture> futureList = Lists.newArrayList();

            List<String> finalSqls = sqls;
            shardIdListSubs.forEach(subList -> {

                CompletableFuture<Void> futureCreative = CompletableFuture
                        .runAsync(() -> dataObjectMaps.addAll(
                                queryDb(subList, finalSqls, params, dataSource)), commonBenchmarkFetcherExecutor);
                futureList.add(futureCreative);
            });

            CompletableFuture.allOf(futureList.toArray(new CompletableFuture[futureList.size()]))
                    .join();
        }

        taskHandleModel.setDataObjectMaps(dataObjectMaps);

        return taskHandleModel;
    }

    private List<Map<String,Object>> queryDb(List<Integer> shardIdList,
                                             List<String> sqls, List<Object> params,
                                             String dataSource) {

        List<Map<String,Object>> result = Lists.newArrayList();

        log.info("queryDb() shardIdList={}, sqls={}, dataSource={}", shardIdList, sqls, dataSource);
        if (CollectionUtils.isEmpty(shardIdList)) {
            // 单表直接取数 FIXME 后续扩展为 动态数据源
            JdbcTemplate jdbcTemplate = AdCoreDataSource.valueOf(dataSource).read().getJdbcTemplate();

            List<CompletableFuture> futureList = Lists.newArrayList();
            for (String sqlStr : sqls) {
                CompletableFuture<Void> futureCreative = CompletableFuture
                        .runAsync(() -> result.addAll(
                                adDasBenchmarkCommonQueryHelper.queryWithRetry(jdbcTemplate, sqlStr, params)),
                                commonBenchmarkFetcherExecutor);
                futureList.add(futureCreative);
            }

            CompletableFuture.allOf(futureList.toArray(new CompletableFuture[futureList.size()]))
                    .join();
        } else {
            // 分表取数
            for (Integer shardId : shardIdList) {

                // FIXME 后续扩展为 动态数据源
                JdbcTemplate jdbcTemplate = AdCoreDataSource.valueOf(dataSource)
                        .shardRead(shardId)
                        .getJdbcTemplate();

                List<CompletableFuture> futureList = Lists.newArrayList();
                for (String sqlStr : sqls) {
                    CompletableFuture<Void> futureCreative = CompletableFuture
                            .runAsync(() -> result.addAll(
                                    adDasBenchmarkCommonQueryHelper.queryWithRetry(jdbcTemplate, sqlStr, params)),
                                    commonBenchmarkFetcherExecutor);
                    futureList.add(futureCreative);
                }

                CompletableFuture.allOf(futureList.toArray(new CompletableFuture[futureList.size()]))
                        .join();
            }
        }

        return result;
    }

}
