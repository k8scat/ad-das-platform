package com.kuaishou.ad.das.platform.core.benchmark.server.converter;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Map;

import com.kuaishou.ad.das.platform.core.benchmark.model.AdDasTaskHandleModel;
import com.kuaishou.ad.das.platform.core.benchmark.model.AdDasTaskSchemaModel;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-01-29
 */
public interface AdDasBenchmarkCoverter {

    /**
     *
     * 1、查询 db column 2 proto column 映射关系
     * 2、dataMap convert into proto
     * 3、hadCode convert more
     *
     * @param adDasTaskHandleModel
     */
    void convert(AdDasTaskSchemaModel adDasTaskSchemaModel, AdDasTaskHandleModel adDasTaskHandleModel,
                 List<Map<String,Object>> mergerData) throws NoSuchMethodException, IOException, IllegalAccessException, InvocationTargetException;

}
