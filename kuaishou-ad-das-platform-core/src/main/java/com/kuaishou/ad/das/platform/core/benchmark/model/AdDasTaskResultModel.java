package com.kuaishou.ad.das.platform.core.benchmark.model;

import static com.kuaishou.ad.das.platform.utils.dasenum.AdDasBenchmarkStatusEnum.SUCESS_STATUS;

import java.io.Serializable;
import java.util.List;

import com.kuaishou.ad.das.platform.utils.dasenum.AdDasBenchmarkStatusEnum;

import lombok.Data;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-01-29
 */
@Data
public class AdDasTaskResultModel implements Serializable {

    private static final long serialVersionUID = -8857559363873955846L;

    /**
     * 任务标号
     */
    private Integer taskNum;

    /**
     * 任务执行时间戳
     */
    private Long timestamp;

    /**
     * hdfs路径
     */
    private String hdfsPath;

    /**
     * proto名
     */
    private String protoName;

    /**
     * 表名称
     */
    private String tableStr;

    private List<DoneTableResult> resultList;


    private Integer status;

    private String reason;

    public AdDasTaskResultModel() {
        this.status = SUCESS_STATUS.getStatus();
        this.reason = SUCESS_STATUS.getReason();
    }

    public AdDasTaskResultModel(Integer taskNum, Long timestamp, AdDasBenchmarkStatusEnum statusEnum) {
        this.taskNum = taskNum;
        this.timestamp = timestamp;
        this.status = statusEnum.getStatus();
        this.reason = statusEnum.getReason();
    }

    public AdDasTaskResultModel(Integer taskNum, Long timestamp, String tableStr, AdDasBenchmarkStatusEnum statusEnum) {
        this.taskNum = taskNum;
        this.timestamp = timestamp;
        this.tableStr = tableStr;
        this.status = statusEnum.getStatus();
        this.reason = statusEnum.getReason();
    }

    public void setStatusAndReason(AdDasBenchmarkStatusEnum statusEnum) {
        this.status = statusEnum.getStatus();
        this.reason = statusEnum.getReason();
    }
}
