package com.kuaishou.ad.das.platform.core.common.daszk.model;

import java.io.Serializable;

import lombok.Data;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-12-08
 */
@Data
public class AdDasServiceNodeSchema implements Serializable {

    private static final long serialVersionUID = -6117254492384190861L;

    private String containerIp;

    private String containerId;

    private String serviceType;

    private String serviceName;

    private String myName;

    /**
     * 用于灰度发布 标识指定升级的版本
     */
    private AdDasPlatformProtoSchema platformProtoSchema;

}
