package com.kuaishou.ad.das.platform.core.increment.transfer.impl;

import static com.kuaishou.ad.das.platform.core.utils.AdDasPlatformPbReflectUtils.buildFieldMessge;

import java.lang.reflect.Method;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.stereotype.Service;

import com.google.common.collect.Maps;
import com.google.protobuf.Descriptors;
import com.google.protobuf.Message;
import com.kuaishou.ad.das.platform.core.increment.model.AdDasCascadeModel;
import com.kuaishou.ad.das.platform.core.increment.model.AdDasIncrementAdInstanceClassModel;
import com.kuaishou.ad.das.platform.core.increment.model.AdDasIncrementAdapterModel;
import com.kuaishou.ad.das.platform.core.increment.transfer.AdDasKbusBatchTransfer;
import com.kuaishou.framework.util.HostInfo;

import lombok.extern.slf4j.Slf4j;

/**
 * Common Converter 逻辑为 老binglogResolver 平移过来
 *
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-10-30
 */
@Slf4j
@Service
public class AdDasKbusCommonBatchTransfer
        extends AdDasKbusAbstractBatchTransfer<AdDasCascadeModel>
        implements AdDasKbusBatchTransfer<AdDasCascadeModel> {

    @Override
    public List<AdDasCascadeModel> doBatchTransfer(
            String tableName, Long accountId, List<AdDasIncrementAdapterModel> adDasKbusConcreteDatas) {
        return defaultBatchConvert(tableName, accountId, adDasKbusConcreteDatas,
                (row, adInstanceBuilder) -> convertMore(row, adInstanceBuilder));
    }


    @Override
    public void convertFillingMore(String fillingTableName, Map<String, Object> data, AdDasCascadeModel adDasCascadeModel) {

        Set<String> tableColumnSet = adDasPlatformIncrementCommonHelper.getTableColumnData(fillingTableName);

        AdDasIncrementAdInstanceClassModel instanceClassModel = adDasCascadeModel.getInstanceClassModel();
        Method methodField = instanceClassModel.getMethodField();

        try {

            Map<String, Message> instanceFields = adDasCascadeModel.getInstanceFields();
            if (HostInfo.debugHost()) {
                log.info("instanceFields={}", instanceFields);
            }
            for (Map.Entry<String, Object> entryData : data.entrySet()) {
                Object obj = methodField.invoke(null, null);
                Message.Builder objBuilder = (Message.Builder) obj;
                Descriptors.Descriptor descriptor = objBuilder.getDescriptorForType();
                if (tableColumnSet.contains(entryData.getKey()) || tableColumnSet.contains("*")) {
                    instanceFields.put(entryData.getKey(), buildFieldMessge(objBuilder, descriptor, entryData.getKey(), entryData.getValue()));

                }
            }

            if (HostInfo.debugHost()) {
                log.info("instanceFields={}", instanceFields);
            }
            adDasCascadeModel.setInstanceFields(instanceFields);
        } catch (Exception e) {
            log.error("convertFillingMore() ocurr error! adDasCascadeModel={}", adDasCascadeModel, e);
        }
    }

    @Override
    public AdDasCascadeModel buildConverterData(
            String tableName, AdDasIncrementAdapterModel adDasKbusConcreteData,
            Message.Builder adInstanceBuilder, AdDasIncrementAdInstanceClassModel classModel) {
        return defaultBuildConverterData(tableName, adDasKbusConcreteData, adInstanceBuilder, classModel);
    }

    @Override
    protected void convertMore(Object row, AdDasCascadeModel adDasCascadeModel) {

        Map<String, Object> dataRow = (Map<String, Object>) row;

        AdDasIncrementAdInstanceClassModel instanceClassModel = adDasCascadeModel.getInstanceClassModel();
        Method methodField = instanceClassModel.getMethodField();

        try {
            Map<String, Message> instanceFields = Maps.newHashMap();

            for (Map.Entry<String, Object> entryData : dataRow.entrySet()) {
                Object obj = methodField.invoke(null, null);
                Message.Builder objBuilder = (Message.Builder) obj;
                Descriptors.Descriptor descriptor = objBuilder.getDescriptorForType();
                String columnKey = entryData.getKey();
                Object data = entryData.getValue();

                instanceFields.put(columnKey, buildFieldMessge(objBuilder, descriptor, columnKey, data));
            }
            if (HostInfo.debugHost()) {
                log.info("instanceFields={}", instanceFields);
            }
            adDasCascadeModel.setInstanceFields(instanceFields);
        } catch (Exception e) {
            log.error("convertMore() ocurr error! adDasCascadeModel={}", adDasCascadeModel, e);
        }
    }

}
