package com.kuaishou.ad.das.platform.core.utils;

import static com.kuaishou.ad.das.platform.utils.kconf.AdDasPlatformKconf.DAS_BAENCHMARK_CLIENT_PATH;
import static com.kuaishou.ad.das.platform.utils.kconf.AdDasPlatformKconf.DAS_BAENCHMARK_COMMON_PATH;
import static com.kuaishou.ad.das.platform.utils.kconf.AdDasPlatformKconf.DAS_PLATFORM_ZK_NAME_SPACE;
import static com.kuaishou.ad.das.platform.utils.kconf.AdDasPlatformKconf.DAS_ZOOK_KEEPER;

import java.util.List;
import java.util.Map;


import org.apache.commons.lang.StringUtils;

import lombok.extern.slf4j.Slf4j;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-03-08
 */
@Slf4j
public class AdDasBaseMultiEnvUtils {

    public static final List<String> getDasZk(String adDasServiceName, String adDasBenchmarkEnv) {

        List<String> zkLists;
        if (StringUtils.isEmpty(adDasBenchmarkEnv)) {
            zkLists =  DAS_ZOOK_KEEPER.get().get(adDasServiceName);
        } else {
            zkLists =  DAS_ZOOK_KEEPER.get().get(adDasBenchmarkEnv);
        }

        log.info("adDasServiceName={}, adDasBenchmarkEnv={}, DAS_ZOOK_KEEPER.get()={}",
                adDasServiceName, adDasBenchmarkEnv, DAS_ZOOK_KEEPER.get());

        return zkLists;
    }

    public static String getDasPlatformNameSpace(String serviceEnv) {
        return DAS_PLATFORM_ZK_NAME_SPACE.get().get(serviceEnv);
    }

    public static String getDasBenchmarkCommonPath(String adDasBenchmarkEnv) {

        return DAS_BAENCHMARK_COMMON_PATH.get().get(adDasBenchmarkEnv);
    }

    public static Map<String, String> getDasBenchmarkHdfsPath(String clientName) {

        return DAS_BAENCHMARK_CLIENT_PATH.get();
    }

}
