package com.kuaishou.ad.das.platform.core.common.pbloader;

import static com.kuaishou.ad.das.platform.core.common.pbloader.AdDasClassLoaderHelper.setCurPbVersion;
import static com.kuaishou.ad.das.platform.core.common.pbloader.AdDasClassLoaderHelper.updatePbVersion;
import static com.kuaishou.ad.das.platform.utils.kconf.AdDasPlatformKconf.adDasPlatformProtoEnumVersionConfig;

import java.net.MalformedURLException;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.lang3.time.StopWatch;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;

import com.kuaishou.ad.das.platform.core.common.AdDasPlatformServiceConfig;
import com.kuaishou.ad.das.platform.core.common.daszk.model.AdDasPlatformProtoSchema;
import com.kuaishou.ad.das.platform.dal.model.AdDasServicePb;
import com.kuaishou.ad.das.platform.dal.repository.AdDasServicePbRepository;
import com.kuaishou.ad.das.platform.utils.dasenum.AdDasStatusEnum;

import lombok.extern.slf4j.Slf4j;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-12-08
 */
@Slf4j
public abstract class AdDasCommonPbLoader {

    @Autowired
    private AdDasClassLoaderHelper adDasClassLoaderHelper;

    @Autowired
    private AdDasServicePbRepository adDasServicePbRepository;

    @Autowired
    protected AdDasPlatformServiceConfig platformServiceConfig;

    /**
     * PB cache会 存放All.proto下的所有 Class & 及其使用到的 其他的 class
     */
    private static ConcurrentHashMap<String, Class> curPbCache = new ConcurrentHashMap<>();
    private static ConcurrentHashMap<String, Class> prePbCache = new ConcurrentHashMap<>();

    /**
     * 当前pb包版本
     */
    private static AdDasPlatformProtoSchema curProtoSchema;

    /**
     * 初始化Pb
     * @throws MalformedURLException
     * @throws ClassNotFoundException
     */
    protected void defaultLoadPb() throws MalformedURLException, ClassNotFoundException {

        // 根据服务名称查询 DB 拿到最新的 kuaishou-ad-new-biz-proto 版本
        List<AdDasServicePb> servicePbs = adDasServicePbRepository
                .queryByService(platformServiceConfig.getServiceName(), AdDasStatusEnum.DEFAULT.getCode());
        log.info("AdDasCommonPbLoader defaultLoadPb() servicePbs={}", servicePbs);

        if (CollectionUtils.isEmpty(servicePbs)) {
            return;
        }

        AdDasPlatformProtoSchema adDasPlatformProtoSchema = new AdDasPlatformProtoSchema();
        String lastestPbVersion = servicePbs.get(0).getPbVersion();
        String lastestPbEnumVersion = servicePbs.get(0).getPbEnumVersion();
        adDasPlatformProtoSchema.setPbVersion(lastestPbVersion);
        adDasPlatformProtoSchema.setPbEnumVersion(lastestPbEnumVersion);
        log.info("AdDasCommonPbLoader defaultLoadPb() adDasPlatformProtoSchema={}", adDasPlatformProtoSchema);

        setCurPbVersion(adDasPlatformProtoSchema);

        Long curTimestamp = System.currentTimeMillis();
        // 加载对应的Pb class cache
        Class clz = adDasClassLoaderHelper.loadClass(adDasPlatformProtoSchema,
                "com.kuaishou.ad.model.protobuf.tables.All");

        log.info("AdDasCommonPbLoader defaultLoadPb() cost={}", System.currentTimeMillis() - curTimestamp);
        // 查询All类下面的 所有子类
        Class[] subClazs = clz.getClasses();

        for (Class subClaz : subClazs) {
            if (!subClaz.isInterface()) {
                log.info("AdDasCommonPbLoader defaultLoadPb() className={}, subClaz={}",
                        subClaz.getName(), subClaz);
                curPbCache.put(subClaz.getName(), subClaz);
                prePbCache.put(subClaz.getName(), subClaz);
            }
        }
    }

    public ConcurrentHashMap<String, Class> getCurPbCache() {
        return curPbCache;
    }

    public ConcurrentHashMap<String, Class> getPrePbCache() {
        return prePbCache;
    }

    /**
     * 获取指定的 class
     * @param className
     * @return
     */
    public Class loadClassFromCache(String className) {

        StopWatch watch = StopWatch.createStarted();
        Class cl = curPbCache.get(className);

        if (cl == null) {
            try {
                cl = adDasClassLoaderHelper.loadClass(curProtoSchema, className);

                if (cl != null) {
                    log.info("loadClassFromCache() reCache class={}", className);
                    curPbCache.put(className, cl);
                }
            } catch (Exception e) {
                log.error("loadClassFromCache() curProtoSchema={}, className={}", curProtoSchema, className, e);
                // perf 打点
            }
        }

        return cl;
    }

    /**
     * 根据版本切换 prePb Cache
     * @param adDasPlatformProtoSchema
     */
    public void reloadPrePbCache(AdDasPlatformProtoSchema adDasPlatformProtoSchema) throws MalformedURLException, ClassNotFoundException {
        try {

            // 加载对应的Pb class cache
            Class clz = adDasClassLoaderHelper.loadClass(adDasPlatformProtoSchema,
                    "com.kuaishou.ad.model.protobuf.tables.All");

            // 查询All类下面的 所有子类
            Class[] subClazs = clz.getClasses();

            for (Class subClaz : subClazs) {
                if (!subClaz.isInterface()) {
                    log.info("【AdDasCommonPbLoader】 reloadPrePbCache() className={}, subClaz={}",
                            subClaz.getName(), subClaz);
                    prePbCache.put(subClaz.getName(), subClaz);
                }
            }
        } catch (Exception e) {
            log.error("reloadPrePbCache() occur error! adDasPlatformProtoSchema={}", adDasPlatformProtoSchema, e);
            throw e;
        }
    }

    /**
     * 根据版本切换正式 curPb Cache
     */
    public void reloadCurPbCache(AdDasPlatformProtoSchema adDasPlatformProtoSchema) {

        log.info("reloadCurPbCache() prePbCache.keySet={}, curPbCache.keySet={}",
                prePbCache.keySet(), curPbCache.keySet());
        if (prePbCache.size() >= curPbCache.size()) {
            curPbCache = prePbCache;
            setCurPbVersion(adDasPlatformProtoSchema);
        }

        log.info("reloadCurPbCache() prePbCache.keySet={}, curPbCache.keySet={}",
                prePbCache.keySet(), curPbCache.keySet());
    }

}
