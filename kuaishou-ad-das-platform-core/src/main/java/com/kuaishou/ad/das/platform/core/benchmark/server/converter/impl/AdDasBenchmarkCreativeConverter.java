package com.kuaishou.ad.das.platform.core.benchmark.server.converter.impl;

import static com.kuaishou.ad.das.platform.core.common.AdDasBenchmarkFactory.registerConverter;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Service;

import com.google.protobuf.Descriptors;
import com.google.protobuf.Message;
import com.kuaishou.ad.das.platform.core.benchmark.server.converter.AdDasBenchmarkCoverter;
import com.kuaishou.ad.das.platform.core.benchmark.model.AdDasTaskHandleModel;
import com.kuaishou.ad.das.platform.core.benchmark.model.AdDasTaskSchemaModel;

import lombok.extern.slf4j.Slf4j;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-02-21
 */
@Slf4j
@Service
public class AdDasBenchmarkCreativeConverter
        extends AdDasBenchmarkAbstractCoverter
        implements AdDasBenchmarkCoverter {

    @Override
    public void convert(AdDasTaskSchemaModel adDasTaskSchemaModel,
                        AdDasTaskHandleModel adDasTaskHandleModel,
                        List<Map<String, Object>> mergerData)
            throws NoSuchMethodException, IOException, IllegalAccessException, InvocationTargetException {

        defaultConvert(adDasTaskSchemaModel, adDasTaskHandleModel, mergerData,
                (msgBuilder, dataMap) -> convertMore(msgBuilder, dataMap));
    }

    @Override
    protected void convertMore(Message.Builder msgBuilder, Map<String, Object> dataMap) {

        Descriptors.Descriptor descriptor = msgBuilder.getDescriptorForType();

        Descriptors.FieldDescriptor filedDescriptor = descriptor.findFieldByName("first_audit_passtime");
        if (filedDescriptor == null) {
            log.error("buildPbMessage() pbColumn=firstAuditPasstime, is not founded!");
            // TODO perf 打点报警

            throw new RuntimeException("pbColumn=firstAuditPasstime is not founded!");
        }
        msgBuilder.setField(filedDescriptor, Optional.ofNullable(dataMap.get("first_audit_passtime"))
                .orElse(0L));

    }

    @Override
    protected void doMerge(List<Map<String, Object>> dataObjectMaps, List<Map<String, Object>> mergerData) {

    }

    @PostConstruct
    private void init() {
        registerConverter("ad_dsp_creative", this);
    }
}
