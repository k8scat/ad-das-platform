package com.kuaishou.ad.das.platform.core.increment.entrance.impl;

import static com.kuaishou.ad.das.platform.core.utils.AdDasIncrementUtils.getAccountId;
import static com.kuaishou.ad.das.platform.core.utils.AdDasIncrementUtils.getModifyField;
import static com.kuaishou.ad.das.platform.utils.constant.AdDasCommonConstant.AD_DAS_NAMESPACE_NEW;
import static com.kuaishou.ad.das.platform.utils.constant.AdDasCommonConstant.Ad_DAS_INCREMENT;
import static com.kuaishou.framework.util.PerfUtils.perf;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.StopWatch;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.kuaishou.ad.das.platform.core.increment.entrance.AdDasPlatformIncrementEntrance;
import com.kuaishou.ad.das.platform.core.increment.helper.AdDasPlatformIncrementCommonHelper;
import com.kuaishou.ad.das.platform.core.increment.lifecycle.CommonIncrementLifeCycle;
import com.kuaishou.ad.das.platform.core.increment.model.AdDasIncrementAdapterModel;
import com.kuaishou.ad.model.protobuf.AdEnumModel;
import com.kuaishou.framework.binlog.util.BinlogResolver;
import com.kuaishou.framework.util.HostInfo;
import com.kuaishou.framework.util.PerfUtils;
import com.kuaishou.infra.databus.common.domain.mysql.ChangeEventData;
import com.kuaishou.infra.databus.common.domain.mysql.ChangeEventHeader;
import com.kuaishou.infra.databus.common.domain.mysql.DmlChangedEventRow;
import com.kuaishou.infra.databus.common.domain.mysql.MysqlEventColumn;
import com.kuaishou.infra.databus.common.domain.mysql.UpdateChangedEventRow;

import lombok.extern.slf4j.Slf4j;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-06-05
 */
@Slf4j
@Component
public class AdDasPlatformIncrementEntranceImpl implements AdDasPlatformIncrementEntrance {

    @Autowired
    private AdDasPlatformIncrementCommonHelper incrementCommonHelper;

    @Autowired
    private CommonIncrementLifeCycle commonIncrementLifeCycle;

    @Override
    public void process(AdDasIncrementAdapterModel adDasIncrementAdapterObject) {


    }

    /**
     * 批量处理
     * @param rightDataList
     */
    public void processBatch(List<AdDasIncrementAdapterModel> rightDataList) {

        StopWatch created = StopWatch.createStarted();

        // 先按照 table分组
        Map<String, List<AdDasIncrementAdapterModel>> table2DataMap = rightDataList.stream()
                .collect(Collectors.groupingBy(AdDasIncrementAdapterModel::getTableName));

        table2DataMap.keySet().forEach(tableName -> {

            String level = incrementCommonHelper.getLevelByTableName(tableName);

            List<AdDasIncrementAdapterModel> concreteDataList = table2DataMap.get(tableName);

            List<AdDasIncrementAdapterModel> concreteDatas;
            // FIXME 对相同主键id数据做覆盖处理
            concreteDatas = mergeModifyFieldList(concreteDataList);

            // 有去重 打一下diff日志
            if (concreteDatas.size() != concreteDataList.size()) {
                log.info("kbusDispatchDistinct result: tableName={}, concreteDatas.size={}, concreteDataList.size={}",
                        tableName, concreteDatas.size(), concreteDataList.size());
            }

            // 分 table 处理
            commonIncrementLifeCycle.lifeCycle(tableName, level, concreteDatas);

        });

        created.stop();
        perf(AD_DAS_NAMESPACE_NEW, Ad_DAS_INCREMENT, "eachTransactionDispatch")
                .micros(created.getTime(TimeUnit.MICROSECONDS))
                .logstash();
    }

    @Override
    public AdDasIncrementAdapterModel adapterData(ChangeEventHeader eventHeader, ChangeEventData changeEventData) throws Throwable {
        try {

            StopWatch created = StopWatch.createStarted();

            String shardTableName = eventHeader.getTableName();

            String noShardTableName = BinlogResolver.getNoShardTableName(shardTableName);
            if (HostInfo.debugHost()) {
                log.info("【AdDasKbusDspAbstractResolver】 noshardtableName={}", shardTableName);
            }
            long timestamp = eventHeader.getTimestamp();

            Map<String, MysqlEventColumn> rowColumnMaps = ((DmlChangedEventRow) changeEventData).getAffectedRow();

            AdEnumModel.AdEnum.AdDasMsgType msgType = AdEnumModel.AdEnum.AdDasMsgType.INSERT;;
            Map<String, MysqlEventColumn> rowColumnMapsBefore = null;
            // update 事件
            if (changeEventData instanceof UpdateChangedEventRow) {
                msgType = AdEnumModel.AdEnum.AdDasMsgType.UPDATE;
                rowColumnMapsBefore = ((UpdateChangedEventRow) changeEventData).getBeforeAffectedRow();
            }

            if (rowColumnMaps == null) {
                PerfUtils.perf(AD_DAS_NAMESPACE_NEW, Ad_DAS_INCREMENT, "rowError.buildConcreteData")
                        .logstash();
                return null;
            }

            Map<String, Serializable> row = Maps.newHashMapWithExpectedSize(rowColumnMaps.size());
            List<String> modifyFieldList = Lists.newArrayList();

            if (AdEnumModel.AdEnum.AdDasMsgType.UPDATE == msgType && rowColumnMapsBefore != null) {
                // 标记改动过的字段
                Map<String, MysqlEventColumn> finalRowColumnMapsBefore = rowColumnMapsBefore;
                rowColumnMaps.forEach((key, value) -> {
                    row.put(key, value.getData());

                    // check updated column
                    try {

                        String modifyColumn = getModifyField(key, rowColumnMaps, finalRowColumnMapsBefore);

                        if (!StringUtils.isEmpty(modifyColumn)) {
                            modifyFieldList.add(modifyColumn);
                        }
                    } catch (Exception e) {
                        log.error("check modify column ocur error", e);
                    }
                });


            } else {
                rowColumnMaps.forEach((key, value) -> {
                    row.put(key, value.getData());
                });
            }

            Long accountId = getAccountId(rowColumnMaps);

            // 获取唯一id
            Long id = incrementCommonHelper.getDataId(noShardTableName, row);

            AdDasIncrementAdapterModel concreteData = new AdDasIncrementAdapterModel();
            // 唯一id
            concreteData.setId(id);
            concreteData.setAccountId(accountId);
            concreteData.setTableName(noShardTableName);
            concreteData.setTimestamp(timestamp);
            concreteData.setShardTableName(shardTableName);
            // 深拷贝处理
            concreteData.setRow(row);

            // 设置级联表 level
            concreteData.setDataLevel(incrementCommonHelper.getLevelByTableName(noShardTableName));
            concreteData.setMsgType(msgType);
            concreteData.setModifyFieldList(modifyFieldList);

            created.stop();
            perf(AD_DAS_NAMESPACE_NEW, Ad_DAS_INCREMENT, "eachbuildConcreteData", noShardTableName)
                    .micros(created.getTime(TimeUnit.MICROSECONDS))
                    .logstash();

            return concreteData;

        } catch (Throwable throwable) {
            log.error("buildConcreteData() ocurr error!", throwable);
            // perf 打点监控
            PerfUtils.perf(AD_DAS_NAMESPACE_NEW, Ad_DAS_INCREMENT, "error.buildConcreteData")
                    .logstash();
        }

        return null;
    }

    private List<AdDasIncrementAdapterModel> mergeModifyFieldList(List<AdDasIncrementAdapterModel> originDatas) {
        Map<Long, AdDasIncrementAdapterModel> mergedMap = new HashMap<>();
        Map<Long, Set<String>> id2ModifyFieldMap = new HashMap<>();
        for (AdDasIncrementAdapterModel adDasIncrementAdapterObject : originDatas) {
            List<String> modifyFieldList = adDasIncrementAdapterObject.getModifyFieldList();
            if (CollectionUtils.isNotEmpty(modifyFieldList)) {
                Set<String> modifyFieldSet = id2ModifyFieldMap.get(adDasIncrementAdapterObject.getId());
                if (CollectionUtils.isEmpty(modifyFieldSet)) {
                    modifyFieldSet = new HashSet<>();
                }
                modifyFieldSet.addAll(modifyFieldList);
                id2ModifyFieldMap.put(adDasIncrementAdapterObject.getId(), modifyFieldSet);
            }
            mergedMap.put(adDasIncrementAdapterObject.getId(), adDasIncrementAdapterObject);
        }
        mergedMap.forEach((key, value) -> {
            Set<String> modifySet = id2ModifyFieldMap.get(key);
            if (HostInfo.debugHost()) {
                log.info("modifyFieldList id:{},modifyFieldList:{}", key, modifySet);
            }
            if (CollectionUtils.isNotEmpty(modifySet)) {
                value.setModifyFieldList(new ArrayList<>(modifySet));
            }
        });
        return new ArrayList<>(mergedMap.values());
    }
}
