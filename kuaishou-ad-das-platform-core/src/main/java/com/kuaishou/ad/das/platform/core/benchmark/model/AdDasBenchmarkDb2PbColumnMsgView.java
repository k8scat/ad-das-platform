package com.kuaishou.ad.das.platform.core.benchmark.model;

import java.io.Serializable;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-11-22
 */
@Data
public class AdDasBenchmarkDb2PbColumnMsgView implements Serializable {


    private static final long serialVersionUID = -8370669952875910439L;

    @ApiModelProperty(value = "db列名")
    private String dbColumn;

    @ApiModelProperty(value = "db列类型")
    private String dbColumnType;

    @ApiModelProperty(value = "pb列名")
    private String pbColumn;

    @ApiModelProperty(value = "pb列类型")
    private String pbColumnType;

    @ApiModelProperty(value = "是否为主键")
    private Boolean isPrimaryKey;
}
