package com.kuaishou.ad.das.platform.core.common.pbloader;

import static com.kuaishou.ad.das.platform.utils.constant.AdDasCommonConstant.AD_DAS_NAMESPACE_NEW;
import static com.kuaishou.ad.das.platform.utils.constant.AdDasCommonConstant.COMMON_PLATFORMLIFE_CYCLE_SUBTAG;
import static com.kuaishou.framework.util.PerfUtils.perf;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.time.StopWatch;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.kuaishou.ad.das.platform.core.common.daszk.model.AdDasPlatformProtoSchema;
import com.kuaishou.framework.concurrent.ExecutorsEx;
import com.kuaishou.kconf.client.Kconf;
import com.kuaishou.kconf.client.Kconfs;

import lombok.extern.slf4j.Slf4j;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-11-29
 */
@Lazy
@Slf4j
@Service
public class AdDasClassLoaderHelper {

    // 系统当前使用版本
    private static AdDasPlatformProtoSchema curPbVersion;

    private static final String PB_CLASS_PREFIX = "com.kuaishou.ad.model.protobuf.tables.All$";

    private static final String PB_AD_ENUM_CLASS_PREFIX = "com.kuaishou.ad.model.protobuf.AdEnumModel$AdEnum$";

    private static Kconf<String> adDasProtoPbLoaderUrl = Kconfs
            .ofString("ad.das.adDasProtoPbLoaderUrl",
                    "http://nexus.corp.kuaishou.com:88/nexus/content/groups/public/kuaishou/kuaishou-ad-new-biz-proto/%s/kuaishou-ad-new-biz-proto-%s.jar")
            .build();

    private static Kconf<String> adDasCommonProtoPbLoaderUrl = Kconfs
            .ofString("ad.das.adDasCommonProtoPbLoaderUrl",
                    "http://nexus.corp.kuaishou.com:88/nexus/content/groups/public/kuaishou/kuaishou-ad-new-common-proto/%s/kuaishou-ad-new-common-proto-%s.jar")
            .build();

    private static Kconf<String> adDasProtoPbEnumLoaderUrl = Kconfs
            .ofString("ad.das.adDasProtoPbEnumLoaderUrl",
                    "http://nexus.corp.kuaishou.com:88/nexus/content/groups/public/kuaishou/kuaishou-ad-reco-base-proto/%s/kuaishou-ad-reco-base-proto-%s.jar")
            .build();

    private static Kconf<Integer> adDasCleanClassCacheDelayTime = Kconfs
            .ofInteger("ad.das.adDasCleanClassCacheDelayTime", 2)
            .build();

    private static Kconf<Integer> adDasCleanClassCacheServiveTime = Kconfs
            .ofInteger("ad.das.adDasCleanClassCacheServiveTime", 5)
            .build();

    // 缓存ClassLoader
    private final static ConcurrentHashMap<String, AdDasClassLoader> LOADER_CACHE = new ConcurrentHashMap<>();
    // 缓存Class数据
    private final static ConcurrentHashMap<String, Map<String, Class>> CLASS_CACHE = new ConcurrentHashMap<>();

    // 包版本时间记录
    private final static ConcurrentHashMap<String, Long> LOADER_CACHE_KEY_TIME = new ConcurrentHashMap<>();

    private static ScheduledExecutorService scheduledExecutorService;

    public static String getPbClassPrefix() {
        return PB_CLASS_PREFIX;
    }

    public static String getPbEnumClassPrefix() {
        return PB_AD_ENUM_CLASS_PREFIX;
    }

    /**
     * 当前系统使用 Pb 包版本
     * @return
     */
    public static AdDasPlatformProtoSchema getCurPbVersion() {
        return curPbVersion;
    }

    /**
     * 更新 服务当前使用 包版本
     * @param curPbVersion
     */
    public static void setCurPbVersion(AdDasPlatformProtoSchema curPbVersion) {
        log.info("【AdDasClassLoaderHelper】setCurPbVersion() curPbVersion={} is updating", curPbVersion);
        AdDasClassLoaderHelper.curPbVersion = curPbVersion;
    }

    /**
     * 更新 proto包版本
     * @param newPbVersion
     */
    public static void updatePbVersion(String newPbVersion) {
        log.info("【AdDasClassLoaderHelper】setCurPbVersion() curPbVersion={} is updating", curPbVersion);
        AdDasClassLoaderHelper.curPbVersion.setPbVersion(newPbVersion);
    }

    /**
     * 获取 proto 包版本
     * @param jarName
     * @return
     */
    public static String getProtoPbVersion(String jarName) {

        if (jarName.startsWith("kuaishou-ad-new-biz-proto-") && jarName.endsWith(".jar")) {

            int indexBegin = jarName.lastIndexOf("-");
            int indexEnd = jarName.lastIndexOf(".jar");
            String subStr = jarName.substring(indexBegin + 1, indexEnd);

            return subStr;
        }

        return null;
    }

    /**
     * 获取 proto enum 包版本
     * @param jarName
     * @return
     */
    public static String getProtoPbEnumVersion(String jarName) {

        if (jarName.startsWith("kuaishou-ad-reco-base-proto-") && jarName.endsWith(".jar")) {

            int indexBegin = jarName.lastIndexOf("-");
            int indexEnd = jarName.lastIndexOf(".jar");
            String subStr = jarName.substring(indexBegin + 1, indexEnd);

            return subStr;
        }

        return null;
    }

    /**
     * 获取 proto common 包版本
     * @param jarName
     * @return
     */
    public static String getProtoPbCommonVersion(String jarName) {

        if (jarName.startsWith("kuaishou-ad-new-common-proto-") && jarName.endsWith(".jar")) {

            int indexBegin = jarName.lastIndexOf("-");
            int indexEnd = jarName.lastIndexOf(".jar");
            String subStr = jarName.substring(indexBegin + 1, indexEnd);

            return subStr;
        }

        return null;
    }

    /**
     * 加载远程 Jar 包
     * @param adDasPlatformProtoSchema
     * @throws MalformedURLException
     */
    public AdDasClassLoader loadJar(AdDasPlatformProtoSchema adDasPlatformProtoSchema) throws MalformedURLException {

        Long curTimestamp = System.currentTimeMillis();
        StopWatch watch = StopWatch.createStarted();

        if (LOADER_CACHE.containsKey(adDasPlatformProtoSchema.getProtoJarVersion())) {
            return LOADER_CACHE.get(adDasPlatformProtoSchema.getProtoJarVersion());
        }

        String protoPbVersion = getProtoPbVersion(adDasPlatformProtoSchema.getPbVersion());
        log.info("loadJar() LOADER_CACHE={}, protoPbVersion={}", LOADER_CACHE, protoPbVersion);
        if (protoPbVersion == null) {
            throw new RuntimeException("jarName=" + adDasPlatformProtoSchema.getProtoJarVersion() + "is error!");
        }
        String protoEnumVersion = getProtoPbEnumVersion(adDasPlatformProtoSchema.getPbEnumVersion());
        if (protoEnumVersion == null) {
            throw new RuntimeException("jarName=" + adDasPlatformProtoSchema.getProtoJarVersion() + "is error!");
        }
        String protoCommonVersion = getProtoPbCommonVersion(adDasPlatformProtoSchema.getPbCommonVersion());
        if (protoCommonVersion == null) {
            throw new RuntimeException("jarName=" + adDasPlatformProtoSchema.getProtoJarVersion() + "is error!");
        }

        AdDasClassLoader urlClassLoader = new AdDasClassLoader();

        String path = String.format(adDasProtoPbLoaderUrl.get(), protoPbVersion, protoPbVersion);
        URL jarUrl = new URL(path);
        urlClassLoader.addURLFile(jarUrl);

        String pathEnum = String.format(adDasProtoPbEnumLoaderUrl.get(), protoEnumVersion, protoEnumVersion);
        URL jarUrlEnum = new URL(pathEnum);
        urlClassLoader.addURLFile(jarUrlEnum);

        String pathCommon = String.format(adDasCommonProtoPbLoaderUrl.get(), protoCommonVersion, protoCommonVersion);
        URL jarUrlCommon = new URL(pathCommon);
        urlClassLoader.addURLFile(jarUrlCommon);

        LOADER_CACHE.put(adDasPlatformProtoSchema.getProtoJarVersion(),urlClassLoader);
        if (!CLASS_CACHE.contains(adDasPlatformProtoSchema.getPbVersion())) {
            CLASS_CACHE.put(adDasPlatformProtoSchema.getProtoJarVersion(), new HashMap<>());
        }
        log.info("loadJar() LOADER_CACHE={}, jarUrl={}, jarUrlEnum={}",
                LOADER_CACHE, jarUrl, jarUrlEnum);

        // key 最近一次存活时间记录
        LOADER_CACHE_KEY_TIME.put(adDasPlatformProtoSchema.getProtoJarVersion(), System.currentTimeMillis());
        log.info("AdDasClassLoaderHelper loadJar() jarName={}, cost={}",
                adDasPlatformProtoSchema.getProtoJarVersion(), System.currentTimeMillis() - curTimestamp);
        perf(AD_DAS_NAMESPACE_NEW, COMMON_PLATFORMLIFE_CYCLE_SUBTAG,
                "loadPbCost", "loadJar", adDasPlatformProtoSchema.getProtoJarVersion())
                .micros(watch.getTime(TimeUnit.MILLISECONDS))
                .logstash();
        return urlClassLoader;
    }

    /**
     * 根据包版本 加载指定的Class
     * @param name
     * @return
     * @throws ClassNotFoundException
     * @throws MalformedURLException
     */
    public Class loadClass(AdDasPlatformProtoSchema adDasPlatformProtoSchema, String name) throws ClassNotFoundException, MalformedURLException {

        StopWatch watch = StopWatch.createStarted();
        // 判断是否已经 缓存了 Class
        Map<String, Class> classMap = CLASS_CACHE.get(adDasPlatformProtoSchema.getProtoJarVersion());
        log.info("loadEnumClass() classMap={}", classMap);

        if (classMap != null && classMap.containsKey(name)) {
            log.info("loadClass() cacheClass name={}, classMap={}", name, classMap);
            // key 最近一次存活时间记录
            LOADER_CACHE_KEY_TIME.put(adDasPlatformProtoSchema.getProtoJarVersion(), System.currentTimeMillis());
            perf(AD_DAS_NAMESPACE_NEW, COMMON_PLATFORMLIFE_CYCLE_SUBTAG,
                    "loadPbCost", "loadClassNull", adDasPlatformProtoSchema.getProtoJarVersion() + ":" + name)
                    .micros(watch.getTime(TimeUnit.MILLISECONDS))
                    .logstash();
            return classMap.get(name);
        }

        AdDasClassLoader urlClassLoader = loadJar(adDasPlatformProtoSchema);
        Class targetClass = urlClassLoader.loadClass(name);
        log.info("name={},targetClass={}, urlClassLoader={}",
                name,targetClass, urlClassLoader.getURLs());

        if (targetClass == null) {
            log.info("jarName={}, class={} is not found!", adDasPlatformProtoSchema.getProtoJarVersion(), name);
            perf(AD_DAS_NAMESPACE_NEW, COMMON_PLATFORMLIFE_CYCLE_SUBTAG,
                    "loadPbCost", "loadClass", adDasPlatformProtoSchema.getProtoJarVersion() + ":" + name)
                    .micros(watch.getTime(TimeUnit.MILLISECONDS))
                    .logstash();
            return null;
        }
        log.info("name={},targetClass.getClasses()={}", name, Arrays.stream(targetClass.getClasses()).toArray());

        // 缓存 Class 对象
        cacheClass(adDasPlatformProtoSchema, name, targetClass);

        perf(AD_DAS_NAMESPACE_NEW, COMMON_PLATFORMLIFE_CYCLE_SUBTAG,
                "loadPbCost", "loadClass", adDasPlatformProtoSchema.getProtoJarVersion() + ":" + name)
                .micros(watch.getTime(TimeUnit.MILLISECONDS))
                .logstash();
        return targetClass;
    }

    /**
     * 加载 pb 类枚举
     * @param adDasPlatformProtoSchema
     * @param name
     * @return
     */
    public Class loadEnumClass(AdDasPlatformProtoSchema adDasPlatformProtoSchema, String name) throws MalformedURLException, ClassNotFoundException {

        StopWatch watch = StopWatch.createStarted();
        // 判断是否已经 缓存了 Class
        Map<String, Class> classMap = CLASS_CACHE.get(adDasPlatformProtoSchema.getProtoJarVersion());
        log.info("loadEnumClass() classMap={}", classMap);

        if (classMap != null && classMap.containsKey(name)) {
            log.info("loadEnumClass() cacheClass name={}, classMap={}", name, classMap);
            // key 最近一次存活时间记录
            LOADER_CACHE_KEY_TIME.put(adDasPlatformProtoSchema.getProtoJarVersion(), System.currentTimeMillis());
            perf(AD_DAS_NAMESPACE_NEW, COMMON_PLATFORMLIFE_CYCLE_SUBTAG,
                    "loadEnumClassCost", "loadClassNull", adDasPlatformProtoSchema.getProtoJarVersion() + ":" + name)
                    .micros(watch.getTime(TimeUnit.MILLISECONDS))
                    .logstash();
            return classMap.get(name);
        }

        AdDasClassLoader urlClassLoader = loadJar(adDasPlatformProtoSchema);
        Class targetClass = urlClassLoader.loadClass(name);
        log.info("name={},targetClass={}, urlClassLoader={}",
                name,targetClass, urlClassLoader.getURLs());

        if (targetClass == null) {
            log.info("jarName={}, class={} is not found!", adDasPlatformProtoSchema.getProtoJarVersion(), name);
            perf(AD_DAS_NAMESPACE_NEW, COMMON_PLATFORMLIFE_CYCLE_SUBTAG,
                    "loadPbCost", "loadClass", adDasPlatformProtoSchema.getProtoJarVersion() + ":" + name)
                    .micros(watch.getTime(TimeUnit.MILLISECONDS))
                    .logstash();
            return null;
        }
        log.info("name={},targetClass.getClasses()={}", name, Arrays.stream(targetClass.getClasses()).toArray());

        // 缓存 Class 对象
        cacheClass(adDasPlatformProtoSchema, name, targetClass);

        perf(AD_DAS_NAMESPACE_NEW, COMMON_PLATFORMLIFE_CYCLE_SUBTAG,
                "loadPbCost", "loadClass", adDasPlatformProtoSchema.getProtoJarVersion() + ":" + name)
                .micros(watch.getTime(TimeUnit.MILLISECONDS))
                .logstash();
        return targetClass;
    }

    /**
     * 缓存类加载信息
     * @param adDasPlatformProtoSchema
     * @param name
     * @param targetClass
     */
    public void cacheClass(AdDasPlatformProtoSchema adDasPlatformProtoSchema, String name, Class targetClass) {

        if (CLASS_CACHE.containsKey(adDasPlatformProtoSchema.getProtoJarVersion())) {
            Map<String, Class> classMapTmp = CLASS_CACHE.get(adDasPlatformProtoSchema.getProtoJarVersion());
            classMapTmp.put(name, targetClass);
            log.info("cacheClass() classMapTmp={}", classMapTmp);
            CLASS_CACHE.put(adDasPlatformProtoSchema.getProtoJarVersion(), classMapTmp);
        } else {
            Map<String, Class> classMapTmp = new HashMap<>();
            classMapTmp.put(name, targetClass);
            log.info("cacheClass() newClassMapTmp={}", classMapTmp);
            CLASS_CACHE.put(adDasPlatformProtoSchema.getProtoJarVersion(), classMapTmp);
        }

        log.info("cacheClass() CLASS_CACHE={}", CLASS_CACHE);
        // key 最近一次存活时间记录
        LOADER_CACHE_KEY_TIME.put(adDasPlatformProtoSchema.getProtoJarVersion(), System.currentTimeMillis());
    }

    /**
     * 清理缓存 Jar包 和 Class 信息
     * @param jarName
     * @throws MalformedURLException
     */
    public void unloadJarFile(String jarName) throws MalformedURLException {
        AdDasClassLoader urlClassLoader = LOADER_CACHE.get(jarName);
        if(urlClassLoader != null){
            urlClassLoader.unloadJarFile();
            urlClassLoader = null;
            LOADER_CACHE.remove(jarName);
        }

        Map<String, Class> classMap = CLASS_CACHE.get(jarName);
        if (!CollectionUtils.isEmpty(classMap)) {
            classMap.clear();
            CLASS_CACHE.remove(jarName);
        }

        LOADER_CACHE_KEY_TIME.remove(jarName);
    }

    /**
     * 单独线程处理：
     * 定时清理过期 jar包 和 Class 信息 缓存
     */
    public void cleanClassCache() {

        Long timestampNow = System.currentTimeMillis();
/*
        LOADER_CACHE_KEY_TIME.forEach((key, value) -> {
            try {
                long deltaTime = timestampNow - value;
                log.info("cleanClassCache() timestampNow={}, key={}, value={}, deltaTime={}",
                        timestampNow, key, value, deltaTime);
                if (deltaTime >= adDasCleanClassCacheServiveTime.get().intValue() * 1000 * 60 * 60
                        && !curPbVersion.equals(key)) {
                    unloadJarFile(key);
                }
            } catch (Exception e) {
                log.error("cleanClassCache() occur error!", e);
                perf(AD_DAS_NAMESPACE_NEW, COMMON_PLATFORMLIFE_CYCLE_SUBTAG,
                        "loadPbError", "cleanClassCache")
                        .logstash();
            }
        });*/
    }

    @PostConstruct
    public void init() {

        log.info("【AdDasClassLoaderHelper】init() start clean-class-loader-cache-thread!");
        scheduledExecutorService =  Executors.newSingleThreadScheduledExecutor(ExecutorsEx
                .newSingleDaemonThreadFactory("ad-das-platform-class-loader-cache-clean-thread"));
        // 启动 巡查Class Loader Cache线程
        scheduledExecutorService
                .scheduleWithFixedDelay(() -> cleanClassCache(),
                        10, adDasCleanClassCacheDelayTime.get(), TimeUnit.MINUTES);
    }
}
