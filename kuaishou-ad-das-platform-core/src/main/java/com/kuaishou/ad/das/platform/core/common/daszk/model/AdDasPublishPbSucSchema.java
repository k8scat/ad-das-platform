package com.kuaishou.ad.das.platform.core.common.daszk.model;

import java.io.Serializable;

import lombok.Data;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-12-08
 */
@Data
public class AdDasPublishPbSucSchema implements Serializable {

    private static final long serialVersionUID = -7344242362075105582L;

    private Long modifyId;

    private String targetPbVersion;

    private Integer pushlishStatus; // 初始化 0-发布中 1-发布成功 2-发布异常
}
