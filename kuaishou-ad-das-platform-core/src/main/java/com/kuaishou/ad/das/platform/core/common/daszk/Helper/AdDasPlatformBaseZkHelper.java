package com.kuaishou.ad.das.platform.core.common.daszk.Helper;

import org.springframework.stereotype.Component;

import com.kuaishou.ad.das.platform.core.common.AdDasPlatformAbstractZkHelper;

import lombok.extern.slf4j.Slf4j;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-12-08
 */
@Slf4j
@Component
public class AdDasPlatformBaseZkHelper extends AdDasPlatformAbstractZkHelper {

    /**
     * 初始化 zk
     * @throws Exception
     */
    public void dasBaseZkInit() throws Exception {

        initCommonPlatformBase();
    }

}
