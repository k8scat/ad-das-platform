package com.kuaishou.ad.das.platform.core.increment.lifecycle;


import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.kuaishou.ad.das.platform.core.increment.filling.AdDasIncrementBatchFilling;
import com.kuaishou.ad.das.platform.core.increment.producer.AdDasIncrementBatchProducer;
import com.kuaishou.ad.das.platform.core.increment.transfer.AdDasKbusBatchTransfer;

import lombok.extern.slf4j.Slf4j;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-10-31
 */
@Slf4j
public class AdDasKbusIncrementFactory {

    private static String  serviceEnv = "test";
    private static String serviceName = "";
    private static String servicePodName = "";
    private static String servicePodFlag = "";

    static {
        serviceEnv = System.getenv("DAS_SERVICE_ENV");
        log.warn("【NOTICE】DAS_SERVICE_ENV={}", serviceEnv);
        serviceName = System.getenv("KWS_SERVICE_NAME");
        log.warn("【NOTICE】KWS_SERVICE_NAME={}", serviceName);
        servicePodName = System.getenv("MY_POD_NAME");
        log.warn("【NOTICE】Y_POD_NAME={}", servicePodName);
        servicePodFlag = serviceName + ":" + servicePodName;
    }


    private static final Map<String, AdDasKbusBatchTransfer> SPECIAL_KBUS_BATCH_TRANSFER_MAP =
            new ConcurrentHashMap<String, AdDasKbusBatchTransfer>();

    private static final Map<String, AdDasIncrementBatchFilling> DAS_INCREMENT_BATCH_FILLING_MAP =
            new ConcurrentHashMap<String, AdDasIncrementBatchFilling>();

    private static final Map<String, AdDasIncrementBatchProducer> DAS_INCREMENT_BATCH_PRODUCE_MAP =
            new ConcurrentHashMap<String, AdDasIncrementBatchProducer>();

    /**
     * TODO 增加 特定服务注册 逻辑
     * @param tableName
     * @param converter
     */
    public static void registSpTransfer(String tableName, AdDasKbusBatchTransfer converter) {

        SPECIAL_KBUS_BATCH_TRANSFER_MAP.put(tableName, converter);
    }

    /**
     * 注册 filling
     * @param tableName
     * @param filling
     */
    public static void registFilling(String tableName, AdDasIncrementBatchFilling filling) {

        DAS_INCREMENT_BATCH_FILLING_MAP.put(tableName, filling);
    }

    /**
     * 注册 producer
     * @param tableName
     * @param producer
     */
    public static void registProducer(String tableName, AdDasIncrementBatchProducer producer) {

        DAS_INCREMENT_BATCH_PRODUCE_MAP.put(tableName, producer);
    }



    public static boolean judgeSpKbusBatchTable(String tableName) {

        return SPECIAL_KBUS_BATCH_TRANSFER_MAP.containsKey(tableName);
    }

    public static boolean judgeSpFillingTable(String tableName) {

        return DAS_INCREMENT_BATCH_FILLING_MAP.containsKey(tableName);
    }


    public static boolean judgeSpProduceTable(String tableName) {

        return DAS_INCREMENT_BATCH_PRODUCE_MAP.containsKey(tableName);
    }

    public static AdDasKbusBatchTransfer getKbusBatchTransfer(String tableName) {

        return SPECIAL_KBUS_BATCH_TRANSFER_MAP.get(tableName);
    }

    public static AdDasIncrementBatchFilling getBatchFilling(String tableName) {

        return DAS_INCREMENT_BATCH_FILLING_MAP.get(tableName);
    }

    public static AdDasIncrementBatchProducer getBatchProducer(String tableName) {

        return DAS_INCREMENT_BATCH_PRODUCE_MAP.get(tableName);
    }

    public static String getServiceEnv() {
        return serviceEnv;
    }

    public static String getServiceName() {
        return serviceName;
    }

    public static String getServicePodName() {
        return servicePodName;
    }

    public static String getServicePodFlag() {
        return servicePodFlag;
    }

}
