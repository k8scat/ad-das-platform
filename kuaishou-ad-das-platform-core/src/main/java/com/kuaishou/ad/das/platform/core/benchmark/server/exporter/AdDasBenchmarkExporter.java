package com.kuaishou.ad.das.platform.core.benchmark.server.exporter;

import com.kuaishou.ad.das.platform.core.benchmark.model.AdDasTaskHandleModel;
import com.kuaishou.ad.das.platform.core.benchmark.model.AdDasTaskResultModel;
import com.kuaishou.ad.das.platform.core.benchmark.model.AdDasTaskSchemaModel;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-01-29
 */
public interface AdDasBenchmarkExporter {

    /**
     * 根据 exportType 选择写出 数据
     *
     * @param adDasTaskHandleModel
     * @return
     */
    AdDasTaskResultModel export(AdDasTaskSchemaModel adDasTaskSchemaModel, AdDasTaskHandleModel adDasTaskHandleModel);



}
