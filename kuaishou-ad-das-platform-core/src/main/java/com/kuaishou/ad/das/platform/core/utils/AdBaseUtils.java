package com.kuaishou.ad.das.platform.core.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import lombok.extern.slf4j.Slf4j;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-08-09
 */
@Slf4j
public class AdBaseUtils {

    private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd_HHmmSS");
    private static SimpleDateFormat atomFormat = new SimpleDateFormat("yyyy-MM-dd_HHmm");

    public static String buildReaourceErrorPerf(Integer assignmentId, String tableStr,
                                         Integer status, String reason) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("resId=").append(assignmentId);
        stringBuilder.append("tableStr=").append(tableStr).append(" error: status=")
                .append(status).append(",reason=").append(reason);

        return stringBuilder.toString();
    }

    public static String getRealTableName(String tableName) {
        int index = tableName.lastIndexOf(">");
        if (index < 0 || index == tableName.length() - 1) {
            return tableName;
        }

        if (tableName.contains("<")) {
            String nameTmp = new String(tableName.substring(index + 1));
            return nameTmp.replace("<", "-");
        }

        return tableName.substring(index + 1);
    }

    public static String getNoShardTableName(String tableName) {
        int index = tableName.indexOf("-");
        if (index < 0 || index == tableName.length() - 1) {
            return tableName;
        }

        return tableName.substring(0, index);
    }

    public static String timeToString(Long time) {

        try {
            Date dateTime = new Date(time);
            return sdf.format(dateTime);
        } catch (Exception e) {
            log.error("timeToString() occur error: time={}", time, e);
        }
        return String.valueOf(time);
    }

    public static long getDaysBefore(int days, long timestamp) {

        Calendar calendar = Calendar.getInstance(); //得到日历
        calendar.setTime(new Date(timestamp)); //把指定时间赋给日历
        calendar.add(Calendar.DAY_OF_MONTH, days);
        calendar.set(Calendar.HOUR, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime().getTime();
    }

    public static String buildFileName(String tableStr, Integer assignmentId, Integer shardId) {

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(getRealTableName(tableStr)).append("-");
        stringBuilder.append(assignmentId).append("-");
        stringBuilder.append(shardId);

        return stringBuilder.toString();
    }

    public static Long getNowTimestamp() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        return calendar.getTime().getTime();
    }

    public static Long getNowTimestampByMins() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime().getTime();
    }

    public static String atomTimeToString(Long time) {
        try {
            Date dateTime = new Date(time);
            return atomFormat.format(dateTime);
        } catch (Exception e) {
            log.error("timeToString() occur error: time={}", time, e);
        }
        return String.valueOf(time);
    }
}
