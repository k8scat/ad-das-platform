package com.kuaishou.ad.das.platform.core.increment.model;

import java.util.Map;
import java.util.Set;

import lombok.Data;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-06-05
 */
@Data
public class AdDasPlatformIncrementTableColumnModel {

    /**
     * 表名
     */
    private String tableName;

    /**
     * 表主键
     */
    private String primaryIdKey;

    /**
     * 字段类型 映射关系
     */
    private Map<String, String> columnTypeMap;

    /**
     * 表类型： 1-主表 2-级联辅助表
     */
    private Integer tableType;

    /**
     * 级联表外键
     */
    private String foreignKey;

    /**
     * 获取 表 字段 set
     * @return
     */
    public Set<String> getColumnSet() {
        return this.columnTypeMap.keySet();
    }
}
