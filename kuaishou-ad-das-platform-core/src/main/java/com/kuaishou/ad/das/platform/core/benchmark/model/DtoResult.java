package com.kuaishou.ad.das.platform.core.benchmark.model;

import java.util.List;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-08-09
 */
public class DtoResult {

    /**
     * 数据大小
     */
    private Integer recordNum;

    /**
     * 写入数据
     */
    private List<byte[]> recordResults;

    public Integer getRecordNum() {
        return recordNum;
    }

    public void setRecordNum(Integer recordNum) {
        this.recordNum = recordNum;
    }

    public List<byte[]> getRecordResults() {
        return recordResults;
    }

    public void setRecordResults(List<byte[]> recordResults) {
        this.recordResults = recordResults;
    }

}
