package com.kuaishou.ad.das.platform.core.utils;

import static com.kuaishou.ad.das.platform.utils.kconf.AdDasBaseCommonKconf.adDasPlatformBenchmarkServiceNumConfig;
import static com.kuaishou.ad.das.platform.utils.kconf.AdDasPlatformKconf.AD_DAS_BENCHMARK_HDFS_PATH_MAP_MODEL_KCONF;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;

import com.google.common.collect.Maps;
import com.kuaishou.ad.das.platform.utils.model.kconf.AdDasBenchmarkHdfsPathMapModel;
import com.kuaishou.kconf.client.Kconf;
import com.kuaishou.kconf.client.Kconfs;

import lombok.extern.slf4j.Slf4j;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-09-27
 */
@Slf4j
public class AdDasBenchmarkUtils {

    public static final Map<String, String> COMMON_HDFS_PATH_CONFIG_MAP = new HashMap<String, String>() {
        {
            put("ad_base", "/home/ad/benchmark/base/");
        }
    };

    public static Kconf<Map<String, String>> commonHdfsPathConfig = Kconfs.ofStringMap(
            "ad.adcore.benchmarkCommonHdfsPathConfig", COMMON_HDFS_PATH_CONFIG_MAP).build();

    /**
     * 查询基准路径
     * @param benchmarkType
     * @return
     */
    public static String getBenchMarkPath(String benchmarkType) {

        if (benchmarkType.contains(">")) {
            String[] benchmarkStrs = benchmarkType.split(">");
            String serviceEnv = benchmarkStrs[0];
            String benchamrkTypeStr = benchmarkStrs[1];
            AdDasBenchmarkHdfsPathMapModel hdfsPathMapModel = AD_DAS_BENCHMARK_HDFS_PATH_MAP_MODEL_KCONF.get();

            Map<String, String> pathDataMap = hdfsPathMapModel.getTaskMap(serviceEnv);
            log.info("getBenchMarkPath() benchmarkType={}, pathDataMap={}",
                    benchmarkType, pathDataMap);

            return pathDataMap.get(benchamrkTypeStr);
        } else {
            return commonHdfsPathConfig.get().get(benchmarkType);
        }
    }

    /**
     * 查询基准类型列表
     * @return
     */
    public static List<String> getBenchmarkTypeLists() {

        List<String> dataPathList = commonHdfsPathConfig.get().keySet().stream()
                .collect(Collectors.toList());

        AdDasBenchmarkHdfsPathMapModel hdfsPathMapModel = AD_DAS_BENCHMARK_HDFS_PATH_MAP_MODEL_KCONF.get();

        hdfsPathMapModel.getHdfsPathMapData().forEach((key, value) -> {

            String pathMorePrefix = key + ">";

            value.forEach((masterKey, masterValue) -> {
                String pathMore  = pathMorePrefix + masterKey;
                log.info("getBenchmarkTypeLists() pathMore={}, masterValue={}", pathMore, masterValue);
                dataPathList.add(pathMore);
            });
        });

        return dataPathList;
    }

    /**
     * 根据 基准类型 获取 benchmekType
     * @param name
     * @return
     */
    public static Integer getBenchmarkCode(String name) {
        return adDasPlatformBenchmarkServiceNumConfig.get().get(name);
    }

    /**
     * 根据 benchmarkType 获取 基准类型名
     * @param benchmarkType
     * @return
     */
    public static String getBenchmarkTypeName(Integer benchmarkType) {
        Map<String, Integer> benchmarkTypeMap = adDasPlatformBenchmarkServiceNumConfig.get();
        Map<Integer, String> benchmarkDataTypeMap = Maps.newHashMap();
        benchmarkTypeMap.forEach((key, value) -> benchmarkDataTypeMap.put(value, key));
        return benchmarkDataTypeMap.get(benchmarkType);
    }

    /**
     * 查询基准目录路径
     * @param benchmarkType
     * @param timestamp
     * @return
     */
    public static String getBenchmarkDirPath(String benchmarkType, Long timestamp) {
        return getBenchMarkPath(benchmarkType) + AdBaseUtils.timeToString(timestamp);
    }

    /**
     * 查询基准文件路径
     * @param benchmarkType
     * @param timestamp
     * @param fileName
     * @return
     */
    public static String getBenchmarkFilePath(String benchmarkType, Long timestamp, String fileName) {
        return getBenchMarkPath(benchmarkType) + AdBaseUtils.timeToString(timestamp) + "/" + fileName;
    }


    /**
     * 构造 pb 的className
     * @param protoName
     * @return
     */
    public static String buildPbClassName(String protoName) {
        int index = StringUtils.lastIndexOf(protoName, ".");
        String rightPbName = StringUtils.substring(protoName, index + 1);

        return "com.kuaishou.ad.model.protobuf.tables.All$" + rightPbName;
    }
}
