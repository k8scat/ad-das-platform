package com.kuaishou.ad.das.platform.core.common;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.kuaishou.ad.das.platform.core.common.daszk.Helper.AdDasPlatformApiZkHelper;
import com.kuaishou.ad.das.platform.core.common.daszk.Helper.AdDasPlatformBaseZkHelper;
import com.kuaishou.ad.das.platform.core.common.daszk.Helper.AdDasPlatformIncrementZkHelper;
import com.kuaishou.ad.das.platform.core.common.daszk.Helper.AdDasPlatformPlatformModifyServiceZkHelper;
import com.kuaishou.ad.das.platform.core.common.pbloader.AdDasApiPbLoader;
import com.kuaishou.ad.das.platform.core.common.pbloader.AdDasBasePbLoader;
import com.kuaishou.ad.das.platform.core.common.pbloader.AdDasIncrementPbLoader;

import lombok.extern.slf4j.Slf4j;

/**
 * 平台服务启动初始化注册zk节点， 以 ad_das_api举例：
 *  addas_platform_test/platform/common_pb/api/{service_name}/curpb
 *                                                           /prepb
 *                             /common_pb/worker/{service_name}/curpb
 *                                                             /prepb
 *                             /common_pb/client/{service_name}/curpb
 *                                                             /prepb
 * 服务实例注册 zk 数据结构， 服务启动后会进行zk注册
 *               /platform/service/{service_type}/{service_name}/nodes/100001
 *                                                                    /100002
 *                                                                    /100003
 *               /platform/service/{service_type}/{service_name}/nodes/100001
 *                                                                    /100002
 *                                                                    /100003
 *               /platform/service/client/{service_name}/nodes/100001
 *                                                            /100002
 *                                                            /100003
 *                                                      /schema_publish
 *
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-12-08
 */
@Slf4j
@Component
public class AdDasPlatformServiceCommonInit {

    private static volatile Integer isStarted = 0;
    private static volatile Integer isPlatformStarted = 0;

    @Autowired
    private AdDasPlatformServiceConfig adDasPlatformServiceConfig;

    @Autowired
    private AdDasPlatformApiZkHelper adDasApiZkHelper;

    @Autowired
    private AdDasPlatformBaseZkHelper adDasBaseZkHelper;

    @Autowired
    private AdDasPlatformIncrementZkHelper adDasIncrementZkHelper;

    @Autowired
    private AdDasApiPbLoader adDasApiPbLoader;

    @Autowired
    private AdDasBasePbLoader adDasBasePbLoader;

    @Autowired
    private AdDasIncrementPbLoader adDasIncrementPbLoader;

    @Autowired
    private AdDasPlatformPlatformModifyServiceZkHelper adDasPlatformModifyServiceZkHelper;

    @EventListener(value = {ContextRefreshedEvent.class})
    public void init(ContextRefreshedEvent event) {

        String serviceType = adDasPlatformServiceConfig.getServiceType();

        if (StringUtils.isEmpty(serviceType)) {
            log.info("AdDasCommonZkInit init() serviceType is empty!");
            return;
        }

         synchronized (AdDasPlatformServiceCommonInit.class) {
            if (!isStarted.equals(0)) {
                log.info("只能初始化一次");
                return;
            } else {
                isStarted++;
            }
        }

        try {
            // api平台 启动
            if (adDasPlatformServiceConfig.isApi()) {
                log.info("AdDasCommonZkInit init()");
                adDasApiZkHelper.dasApiZkInit();

                // init pb schema
                log.info("AdDasCommonZkInit init() start initpb!");
                 adDasApiPbLoader.loadApiPb();
            }

            // 基准 worker 启动
            if (adDasPlatformServiceConfig.isWorker()) {
                adDasBaseZkHelper.dasBaseZkInit();
                adDasBasePbLoader.loadBasePb();
            }

            // 增量服务 启动
            if (adDasPlatformServiceConfig.isIncre()) {
                adDasIncrementZkHelper.dasIncrementZkInit();
                adDasIncrementPbLoader.loadIncrementPb();
            }

        } catch (Exception e) {
            log.info("AdDasCommonZkInit init() occur error!", e);
        }

        log.info("AdDasCommonZkInit init() zk success!");
    }

    /**
     * Proto 包升级服务 初始化启动
     */
    public void initPlatformModifyService() {

        log.info("initPlatformModifyService() start!");
        synchronized (AdDasPlatformServiceCommonInit.class) {
            if (!isPlatformStarted.equals(0)) {
                log.info("只能初始化一次");
                return;
            } else {
                isPlatformStarted++;
            }
        }

        try {
            // 平台发布服务 启动
            if (adDasPlatformServiceConfig.isPbRunner()) {
                adDasPlatformModifyServiceZkHelper.platformModifyProtoZkInit();
            }
        } catch (Exception e) {
            log.info("AdDasCommonZkInit initPlatformModifyService() occur error!", e);
        }
    }
}
