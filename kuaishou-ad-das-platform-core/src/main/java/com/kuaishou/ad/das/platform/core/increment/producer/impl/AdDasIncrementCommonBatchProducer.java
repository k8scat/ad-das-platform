package com.kuaishou.ad.das.platform.core.increment.producer.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.kuaishou.ad.das.platform.core.increment.model.AdDasCascadeModel;
import com.kuaishou.ad.das.platform.core.increment.producer.AdDasIncrementBatchProducer;

import lombok.extern.slf4j.Slf4j;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-01-05
 */
@Slf4j
@Service
public class AdDasIncrementCommonBatchProducer
        extends AdDasIncrementAbstractBatchProducer
        implements AdDasIncrementBatchProducer {

    @Override
    public void doBatchSendKafka(String tableName, Long accountId, List<AdDasCascadeModel> adDasCascadeModels) {
        defaultBatchSendKafka(tableName, adDasCascadeModels, null);
    }


    @Override
    protected void sendKafkaMore(String tableName, AdDasCascadeModel adDasCascadeModel) {

    }
}
