package com.kuaishou.ad.das.platform.core.benchmark.model;

import java.io.Serializable;
import java.util.List;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/** FIXME 对应 db表接入信息数据 ad_das_benchmark_main_table_msg
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-11-22
 */
@Data
public class AdDasBenchmarkMainTableView implements Serializable {

    private static final long serialVersionUID = 6638848215479503259L;

    @ApiModelProperty(value = "基准id")
    private Long benchmarkId;

    @ApiModelProperty(value = "数据流组id")
    private Long dataStreamGroupId;

    @ApiModelProperty(value = "表id")
    private Long commonDetailId;

    @ApiModelProperty(value = "数据流组名称")
    private String dataStreamGroupName;

    @ApiModelProperty(value = "基准名称")
    private String benchmarkName;

    @ApiModelProperty(value = "主数据源")
    private String dataSource;

    @ApiModelProperty(value = "主数据表")
    private String mainTable;

    @ApiModelProperty(value = "pb类")
    private String pbClass;

    @ApiModelProperty(value = "删除表")
    private Boolean deleteTable;

    @ApiModelProperty(value = "级联表关系列表")
    private List<AdDasBenchmarkCascadeTableView> cascadeTableViews;

    @ApiModelProperty(value = "db表跟pb字段映射")
    private List<AdDasBenchmarkDb2PbColumnMsgView> db2PbColumnMsgViews;

    @ApiModelProperty(value = "主表类型 0-单表，1-分表")
    private Integer tableType;

    @ApiModelProperty(value = "单表数据增长预估")
    private Integer predictGrowth;

    @ApiModelProperty(value = "主表查询sql")
    private String commonSqls;

    @ApiModelProperty(value = "主表查询时间范围")
    private String timeParams;

    @ApiModelProperty(value = "是否启用shard sql 0-不启用，1-启用")
    private Integer shardSqlFlag;

    @ApiModelProperty(value = "shard Id")
    private String shardSqlColumn;

}
