package com.kuaishou.ad.das.platform.core.benchmark.server.converter.impl;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.google.protobuf.Descriptors;
import com.google.protobuf.Message;
import com.kuaishou.ad.das.platform.core.benchmark.server.converter.AdDasBenchmarkCoverter;
import com.kuaishou.ad.das.platform.core.benchmark.model.AdDasTaskHandleModel;
import com.kuaishou.ad.das.platform.core.benchmark.model.AdDasTaskSchemaModel;

import lombok.extern.slf4j.Slf4j;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-01-29
 */
@Slf4j
@Service
public class AdDasBenchmarkCommonCoverter
        extends AdDasBenchmarkAbstractCoverter
        implements AdDasBenchmarkCoverter {

    @Override
    public void convert(AdDasTaskSchemaModel adDasTaskSchemaModel, AdDasTaskHandleModel adDasTaskHandleModel,
                        List<Map<String,Object>> mergerData) throws NoSuchMethodException, IOException, IllegalAccessException, InvocationTargetException {

        defaultConvert(adDasTaskSchemaModel, adDasTaskHandleModel, mergerData,
                (msgBuilder, dataMap) -> convertMore(msgBuilder, dataMap));
    }

    @Override
    protected void convertMore(Message.Builder msgBuilder, Map<String,Object> dataMap) {
        Descriptors.Descriptor descriptor = msgBuilder.getDescriptorForType();

    }

    @Override
    protected void doMerge(List<Map<String, Object>> dataObjectMaps, List<Map<String, Object>> mergerData) {

    }
}
