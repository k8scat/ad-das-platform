package com.kuaishou.ad.das.platform.core.benchmark.server;

import static com.cronutils.model.CronType.UNIX;
import static com.cronutils.model.field.expression.FieldExpressionFactory.every;
import static com.kuaishou.ad.das.platform.utils.kconf.AdDasBaseCommonKconf.AD_DAS_BENCHMARK_MASTER_CRON_TIME;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;

import com.cronutils.builder.CronBuilder;
import com.cronutils.model.Cron;
import com.cronutils.model.definition.CronDefinitionBuilder;
import com.kuaishou.ad.das.platform.core.benchmark.server.watcher.AdDasBenchmarkBenchmarkWorkerMasterWatcher;
import com.kuaishou.framework.runner.CronScheduledTask;

import kuaishou.common.BizDef;
import lombok.extern.slf4j.Slf4j;

/**
 * Server worker master
 *
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-07-04
 */
@Slf4j
public abstract class AdDasBenchmarkAbstractWorkerMaster implements CronScheduledTask {

    @Autowired
    private AdDasBenchmarkBenchmarkWorkerMasterWatcher masterWatcher;

    @Autowired
    private CommonServerWorkerMasterLifeCycle workerMasterLifeCycle;

    @Override
    public void cronRun() throws Exception {
        log.info("AdDasPlatformBenchmarkAbstractClient() start!");
        workerMasterLifeCycle.run();
        log.info("AdDasPlatformBenchmarkAbstractClient() end!");
    }

    @Override
    public void uniqueRunningLockAcquiredCallback() {
        // client 主备切换实现
        masterWatcher.init();
    }

    // CHECKSTYLE:OFF
    @Override
    public boolean runAtStart() {
        return true;
    }
    // CHECKSTYLE:ON

    @Override
    public Cron cronConfig() {
        return CronBuilder.cron(CronDefinitionBuilder.instanceDefinitionFor(UNIX))
                .withMinute(every(AD_DAS_BENCHMARK_MASTER_CRON_TIME.get()))
                .instance()
                .validate();
    }

    @NotNull
    @Override
    public BizDef bizDef() {
        return BizDef.AD_DSP;
    }
}

