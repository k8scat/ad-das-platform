package com.kuaishou.ad.das.platform.core.increment.model;

import java.io.Serializable;
import java.lang.reflect.Method;

import lombok.Data;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-07-15
 */
@Data
public class AdDasIncrementAdInstanceClassModel implements Serializable {

    private static final long serialVersionUID = -596185140079648643L;

    private String protoJarVersion;

    private Class clsBuilder;

    private Method method;

    private Class clsFieldBuilder;

    private Method methodField;

}
