package com.kuaishou.ad.das.platform.core.benchmark.server.exporter.impl;

import static com.google.common.util.concurrent.MoreExecutors.shutdownAndAwaitTermination;
import static com.kuaishou.ad.das.platform.utils.constant.AdDasCommonConstant.AD_DAS_NAMESPACE_NEW;
import static com.kuaishou.ad.das.platform.utils.constant.AdDasCommonConstant.COMMON_BENCHMARK_LIFE_CYCLE_SUBTAG;
import static com.kuaishou.ad.das.platform.utils.kconf.AdDasBaseCommonKconf.AD_DAS_COMMON_BENCHMARK_EXPORTER_THREAD;
import static com.kuaishou.framework.concurrent.DynamicThreadExecutor.dynamic;
import static com.kuaishou.framework.util.PerfUtils.perf;
import static com.kuaishou.infra.framework.common.util.TermHelper.addTerm;
import static java.util.concurrent.TimeUnit.MINUTES;

import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.commons.compress.utils.Lists;
import org.springframework.stereotype.Service;

import com.kuaishou.ad.das.platform.core.benchmark.server.exporter.AdDasBenchmarkExporter;
import com.kuaishou.ad.das.platform.core.benchmark.model.AdDasTaskHandleModel;
import com.kuaishou.ad.das.platform.core.benchmark.model.AdDasTaskResultModel;
import com.kuaishou.ad.das.platform.core.benchmark.model.AdDasTaskSchemaModel;

import lombok.extern.slf4j.Slf4j;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-01-29
 */
@Slf4j
@Service
public class AdDasBenchmarkCommonExporter
        extends AdDasBenchmarkAbstractExporter
        implements AdDasBenchmarkExporter {

    @Override
    public AdDasTaskResultModel export(AdDasTaskSchemaModel adDasTaskSchemaModel,
                                       AdDasTaskHandleModel adDasTaskHandleModel) {

        if (adDasTaskHandleModel == null) {
            // perf 打点报警
            perf(AD_DAS_NAMESPACE_NEW, COMMON_BENCHMARK_LIFE_CYCLE_SUBTAG,
                    "exportError", "modelsEmpty",
                    adDasTaskSchemaModel.getTaskNum() + ":" + adDasTaskSchemaModel.getTaskName())
                    .logstash();
            return null;
        }

        AdDasTaskResultModel result = null;
        if (adDasTaskHandleModel.getExportType() == 0) {

            List<AdDasTaskHandleModel> adDasTaskHandleModels = Lists.newArrayList();
            adDasTaskHandleModels.add(adDasTaskHandleModel);
            result = defaultexport2Hdfs(adDasTaskSchemaModel, adDasTaskHandleModels);
        } else {
            // TODO 暂不实现
        }

        return result;
    }

    @PostConstruct
    public void init() {
        commonBenchmarkExporterExecutor = dynamic(AD_DAS_COMMON_BENCHMARK_EXPORTER_THREAD::get,
                "ad-das-common-benchmark-exporter-pool");
        // TermHelper.addTerm 关机时注册关闭逻辑
        addTerm(() -> shutdownAndAwaitTermination(commonBenchmarkExporterExecutor, 10, MINUTES));

    }
}
