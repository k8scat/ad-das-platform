package com.kuaishou.ad.das.platform.core.benchmark.client.manager;

import static com.kuaishou.ad.das.platform.core.utils.AdBaseUtils.getNoShardTableName;
import static com.kuaishou.ad.das.platform.core.utils.AdBaseUtils.timeToString;
import static com.kuaishou.ad.das.platform.utils.dasenum.AdDasBenchmarkStatusEnum.SUCESS_STATUS;
import static com.kuaishou.framework.util.PerfUtils.perf;

import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import com.ecyrd.speed4j.StopWatch;
import com.google.common.collect.Lists;
import com.google.protobuf.util.JsonFormat;
import com.kuaishou.ad.das.platform.core.benchmark.helper.AdDasBenchmarkPlatformClientZkHelper;
import com.kuaishou.ad.das.platform.core.benchmark.model.AdDasPlatformClientTaskBatchModel;
import com.kuaishou.ad.das.platform.core.benchmark.model.AdDasPlatformTaskDoneModel;
import com.kuaishou.ad.das.platform.core.benchmark.model.DoneTableResult;
import com.kuaishou.ad.das.platform.core.common.AdDasPlatformServiceConfig;
import com.kuaishou.ad.das.platform.utils.HdfsUtils;
import com.kuaishou.ad.model.protobuf.tables.Meta;
import com.kuaishou.framework.util.ObjectMapperUtils;
import com.kuaishou.framework.util.PerfUtils;

import lombok.extern.slf4j.Slf4j;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-06-06
 */
@Lazy
@Slf4j
@Component
public class AdDasPlatformBenchmarkManager {

    @Autowired
    private AdDasBenchmarkPlatformClientZkHelper clientCommonZkHelper;

    @Autowired
    protected AdDasPlatformServiceConfig platformServiceConfig;

    public Meta.DumpInfo buildDumpInfo(List<String> doneContents, AdDasPlatformClientTaskBatchModel clientTaskModel) {
        // 数据量监控对比
        List<AdDasPlatformTaskDoneModel> taskDoneModels = Lists.newArrayList();
        doneContents.stream()
                .filter(resultStr -> !StringUtils.isEmpty(resultStr))
                .forEach(resultStr -> {
                    AdDasPlatformTaskDoneModel dasPlatformTaskDoneModel = ObjectMapperUtils.fromJSON(resultStr, AdDasPlatformTaskDoneModel.class);
                    if (dasPlatformTaskDoneModel != null) {
                        taskDoneModels.add(dasPlatformTaskDoneModel);
                    }
                });

        // 汇总所有任务 构造dumpInfo
        Meta.DumpInfo.Builder builder = Meta.DumpInfo.newBuilder();
        AtomicReference<Boolean> isAllRight = new AtomicReference<>(true);

        // 外层按照table级别遍历， 内层按照table会shard成多个文件来遍历
        taskDoneModels.stream()
                .filter(taskDoneModel -> taskDoneModel != null)
                .forEach(taskDoneModel -> {
                    try {
                        log.info("writeDumpInfo2HDFS() taskDoneModel={}", taskDoneModel);

                        if (taskDoneModel.getStatus() != null
                                && taskDoneModel.getStatus().intValue() == SUCESS_STATUS.getStatus()) {
                            List<DoneTableResult> resultList = taskDoneModel.getResultList();
                            if (resultList == null || CollectionUtils.isEmpty(resultList)) {
                                isAllRight.set(false);
                                return;
                            }

                            resultList.stream()
                                    .filter(result -> result != null)
                                    .forEach(result -> {
                                        Meta.DumpData.Builder dumpData = Meta.DumpData.newBuilder();
                                        dumpData.setFileName(result.getFileName());
                                        String noShardTableStr = getNoShardTableName(result.getTableStr());
                                        dumpData.setTableName(noShardTableStr);
                                        dumpData.setProtoName(taskDoneModel.getProtoName());
                                        dumpData.setRecordNum(result.getRecordNum());
                                        dumpData.build();
                                        builder.addInfo(dumpData);
                                    });
                        } else {
                            log.error("【NOTICE ERROR】writeDumpInfo2HDFS() taskDoneModel={}", taskDoneModel);
                            isAllRight.set(false);
                            return;
                        }
                    } catch (Exception e) {
                        isAllRight.set(false);
                        log.error("writeDumpInfo2HDFS() buildDumpeInfo forEach ocurr error: taskDoneModel={}", taskDoneModel, e);
                    }
                });

        if (!isAllRight.get()) {
            throw new RuntimeException("clientName=" + platformServiceConfig.getServiceName()
                    + "done ocurr error!");
        }

        Meta.DumpInfo dumpInfo = builder.build();

        return dumpInfo;
    }

    /**
     * 管理汇总任务
     */
    public void managerAllTasks(Meta.DumpInfo dumpInfo, AdDasPlatformClientTaskBatchModel clientTaskModel) {

        StopWatch watcher = PerfUtils.getWatcher();
        try {

            String dumpJsonData = JsonFormat.printer().print(dumpInfo);
            log.info("writeDumpInfo2HDFS()  dumpJsonData={}", dumpJsonData);

            String hdfsPath = clientTaskModel.getHdfsPath();

            HdfsUtils.writeDataWithRetry(dumpJsonData.getBytes(), hdfsPath, timeToString(clientTaskModel.getTimestamp()), "dump_info");
            String done = " ";
            HdfsUtils.writeDataWithRetry(done.getBytes(), hdfsPath, timeToString(clientTaskModel.getTimestamp()), "dump_done");

            perf("", "", clientTaskModel.getClientName(),
                    platformServiceConfig.getServiceEnv(), "success")
                    .count(1).micros(watcher.getTimeMicros()).logstash();
            log.info("【NOTICE】writeDumpInfo2HDFS() done success !!!!");

            clientCommonZkHelper.cleanTaskMsg(clientTaskModel);

        } catch (Exception e) {
            perf("", "", clientTaskModel.getClientName(),
                    platformServiceConfig.getServiceEnv(),
                    clientTaskModel.getClientName()+ "-writeDumpInfo2HDFS",
                    "error")
                    .count(1).logstash();
            log.error("writeDumpInfo2HDFS() ocurr error: dumpInfo={}", dumpInfo, e);
        }


    }
}
