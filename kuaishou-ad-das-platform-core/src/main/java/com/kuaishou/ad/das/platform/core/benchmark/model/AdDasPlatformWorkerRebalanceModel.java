package com.kuaishou.ad.das.platform.core.benchmark.model;

import java.io.Serializable;

import org.apache.curator.framework.recipes.cache.PathChildrenCacheEvent;

import lombok.Data;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-07-06
 */
@Data
public class AdDasPlatformWorkerRebalanceModel implements Serializable {

    private static final long serialVersionUID = -2727489594192439978L;

    /**
     * 1. Type.CHILD_ADDED
     * 2. Type.CHILD_REMOVED
     */
    private PathChildrenCacheEvent.Type type;

    /**
     * 实例名称
     */
    private String workerName;

}
