package com.kuaishou.ad.das.platform.core.common.daszk.Helper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.kuaishou.ad.das.platform.core.common.AdDasPlatformAbstractZkHelper;
import com.kuaishou.ad.das.platform.core.common.AdDasPlatformServiceConfig;
import com.kuaishou.ad.das.platform.core.increment.helper.AdDasIncrementCommonQueryHelper;

import lombok.extern.slf4j.Slf4j;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-12-08
 */
@Slf4j
@Component
public class AdDasPlatformIncrementZkHelper extends AdDasPlatformAbstractZkHelper {

    @Autowired
    private AdDasPlatformServiceConfig platformServiceConfig;

    @Autowired
    private AdDasIncrementCommonQueryHelper commonQueryHelper;

    @Autowired
    private AdDasPlatformIncrementZkHelper adDasIncrementZkHelper;

    /**
     * AdDas 平台相关zk注册
     * @throws Exception
     */
    public void dasIncrementZkInit() throws Exception {

        log.info("init() start!");
        // query ad_das_increment 获取增量服务基本信息


        log.info("init() end!");

        initCommonPlatformBase();

        // TODO 初始化 增量变更注册

    }

}
