package com.kuaishou.ad.das.platform.core.increment.lifecycle;

import static com.kuaishou.ad.das.platform.utils.constant.AdDasCommonConstant.AD_DAS_COMMON_INCR_LIFE_CYCLE_SUBTAG;
import static com.kuaishou.ad.das.platform.utils.constant.AdDasCommonConstant.AD_DAS_NAMESPACE_NEW;
import static com.kuaishou.ad.das.platform.utils.kconf.AdDasBaseCommonKconf.adDasPlatformIncrementShardCount;
import static com.kuaishou.framework.util.PerfUtils.perf;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.time.StopWatch;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.google.common.collect.Lists;
import com.kuaishou.ad.das.platform.core.increment.filling.AdDasIncrementBatchFilling;
import com.kuaishou.ad.das.platform.core.increment.model.AdDasCascadeModel;
import com.kuaishou.ad.das.platform.core.increment.model.AdDasIncrementAdapterModel;
import com.kuaishou.ad.das.platform.core.increment.producer.AdDasIncrementBatchProducer;
import com.kuaishou.ad.das.platform.core.increment.transfer.AdDasKbusBatchTransfer;
import com.kuaishou.framework.util.HostInfo;
import com.kuaishou.framework.util.PerfUtils;

import lombok.extern.slf4j.Slf4j;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-01-05
 */
@Slf4j
@Service
public class CommonIncrementLifeCycle extends AbstractIncrementLifeCycle {

    /**
     * 1、适配 Transfer、 Filling、 Cascade、Producer
     * 2、分shard 并发处理
     * @param tableName
     * @param level
     * @param adapterObjects
     */
    public void lifeCycle(String tableName, String level, List<AdDasIncrementAdapterModel> adapterObjects)  {

        // 获取自定义 Transfer
        AdDasKbusBatchTransfer adDasKbusBatchTransfer = choseTransfer(tableName);

        // 获取自定义 Filling
        AdDasIncrementBatchFilling batchFilling = choseFilling(tableName);

        // 获取自定义 Producer
        AdDasIncrementBatchProducer batchProducer = choseProducer(tableName);

        log.info("lifeCycle() tableName={}", tableName);

        // 分类处理
        dasRun(adDasKbusBatchTransfer, batchFilling, batchProducer,
                tableName, level, 0L, adapterObjects);
    }

    private void dasRun(AdDasKbusBatchTransfer adDasKbusBatchTransfer,
                        AdDasIncrementBatchFilling batchFilling,
                        AdDasIncrementBatchProducer batchProducer,
                        String tableName, String level, Long accountId,
                        List<AdDasIncrementAdapterModel> adapterObjects) {

        // 多线程 处理 分 shard批量处理
        List<List<AdDasIncrementAdapterModel>> shardConcreteDats = Lists.partition(adapterObjects, adDasPlatformIncrementShardCount.get());

        log.info("dasRun() shardConcreteDats.size={}", shardConcreteDats.size());

        shardConcreteDats.stream().forEach(shardConcreteData -> {

            this.processCascade(adDasKbusBatchTransfer, batchFilling, batchProducer,
                    tableName, level, accountId, shardConcreteData);
        });
    }

    /**
     * 级联 批量处理
     * @param adDasIncrementAdapterObjects
     */
    public void processCascade(AdDasKbusBatchTransfer adDasKbusBatchTransfer,
                        AdDasIncrementBatchFilling batchFilling,
                        AdDasIncrementBatchProducer batchProducer,
                        String tableName, String level, Long accountId,
                        List<AdDasIncrementAdapterModel> adDasIncrementAdapterObjects) {
        try {

            PerfUtils.perf(AD_DAS_NAMESPACE_NEW, AD_DAS_COMMON_INCR_LIFE_CYCLE_SUBTAG, "lifeCycle", "processCascade")
                    .count(adDasIncrementAdapterObjects.size())
                    .logstash();

            // 监控打点耗时
            StopWatch createdAll = StopWatch.createStarted();

            // 转换成 AdDasCascadeModel
            List<AdDasCascadeModel> adDasCascadeModels = adDasKbusBatchTransfer.doBatchTransfer(tableName,
                    accountId, adDasIncrementAdapterObjects);

            if (HostInfo.debugHost()) {
                log.info("dasRun() adDasCascadeModels={}", adDasCascadeModels);
            }

            if (CollectionUtils.isEmpty(adDasCascadeModels)) {
                return;
            }

            // 横级联处理
            List<AdDasCascadeModel> fillingCascadeModels = batchFilling
                    .doBatchFilling(tableName, level, accountId, adDasCascadeModels);

            // 发送normal 增量消息 & 级联上线消息
            batchProducer.doBatchSendKafka(tableName, accountId, fillingCascadeModels);

            createdAll.stop();
            perf(AD_DAS_NAMESPACE_NEW, AD_DAS_COMMON_INCR_LIFE_CYCLE_SUBTAG, "processCascade", tableName)
                    .micros(createdAll.getTime(TimeUnit.MICROSECONDS))
                    .logstash();
        } catch (Exception e) {

            perf(AD_DAS_NAMESPACE_NEW, AD_DAS_COMMON_INCR_LIFE_CYCLE_SUBTAG, "lifeCycleError", "processCascade", tableName)
                    .logstash();
            log.error("【CommonIncrementLifeCycle】 processCascade() ocurr error! tableName={}, accountId={}",
                    tableName, accountId, e);
        }
    }

    /**
     * 非 级联 处理
     * @param adDasKbusBatchTransfer
     * @param batchProducer
     * @param tableName
     * @param level
     * @param accountId
     * @param adDasIncrementAdapterObjects
     */
    public void process(AdDasKbusBatchTransfer adDasKbusBatchTransfer,
                        AdDasIncrementBatchProducer batchProducer,
                        String tableName, String level, Long accountId,
                        List<AdDasIncrementAdapterModel> adDasIncrementAdapterObjects) {

        try {
            PerfUtils.perf(AD_DAS_NAMESPACE_NEW, AD_DAS_COMMON_INCR_LIFE_CYCLE_SUBTAG, "lifeCycle", "process")
                    .count(adDasIncrementAdapterObjects.size())
                    .logstash();

            if (HostInfo.debugHost()) {
                log.info("【CommonIncrementLifeCycle】process() tableName={}, adDasIncrementAdapterObjects={}",
                        tableName, adDasIncrementAdapterObjects);
            }

            // 监控打点耗时
            StopWatch createdAll = StopWatch.createStarted();

            // 转换成 AdDasCascadeModel
            List<AdDasCascadeModel> adDasCascadeModels = adDasKbusBatchTransfer.doBatchTransfer(tableName,
                    accountId, adDasIncrementAdapterObjects);
            if (HostInfo.debugHost()) {
                log.info("【CommonIncrementLifeCycle】process() tableName={}, adDasCascadeModels.size={}",
                        tableName, adDasCascadeModels.size());
            }

            // 发送增量消息
            batchProducer.doBatchSendKafka(tableName, accountId, adDasCascadeModels);

            createdAll.stop();
            perf(AD_DAS_NAMESPACE_NEW, AD_DAS_COMMON_INCR_LIFE_CYCLE_SUBTAG, "process", tableName)
                    .micros(createdAll.getTime(TimeUnit.MICROSECONDS))
                    .logstash();
        } catch (Exception e) {

            perf(AD_DAS_NAMESPACE_NEW, AD_DAS_COMMON_INCR_LIFE_CYCLE_SUBTAG, "lifeCycleError", "process", tableName)
                    .logstash();
            log.error("【CommonIncrementLifeCycle】 ocurr error! tableName={}, accountId={}",
                    tableName, accountId, e);
        }
    }
}
