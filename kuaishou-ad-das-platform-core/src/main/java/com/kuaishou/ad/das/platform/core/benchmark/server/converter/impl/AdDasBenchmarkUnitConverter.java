package com.kuaishou.ad.das.platform.core.benchmark.server.converter.impl;

import static com.kuaishou.ad.das.platform.core.common.AdDasBenchmarkFactory.registerConverter;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Service;

import com.google.protobuf.Message;
import com.kuaishou.ad.das.platform.core.benchmark.server.converter.AdDasBenchmarkCoverter;
import com.kuaishou.ad.das.platform.core.benchmark.model.AdDasTaskHandleModel;
import com.kuaishou.ad.das.platform.core.benchmark.model.AdDasTaskSchemaModel;

import lombok.extern.slf4j.Slf4j;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-02-15
 */
@Slf4j
@Service
public class AdDasBenchmarkUnitConverter
        extends AdDasBenchmarkAbstractCoverter
        implements AdDasBenchmarkCoverter {

    @Override
    public void convert(AdDasTaskSchemaModel adDasTaskSchemaModel, AdDasTaskHandleModel adDasTaskHandleModel,
                        List<Map<String, Object>> mergerData) throws NoSuchMethodException, IOException, IllegalAccessException, InvocationTargetException {

        defaultConvert(adDasTaskSchemaModel, adDasTaskHandleModel, mergerData,
                (msgBuilder, dataMap) -> convertMore(msgBuilder, dataMap));
    }

    @Override
    protected void convertMore(Message.Builder msgBuilder, Map<String, Object> dataMap) {

        // 转换 转换计算字段

    }

    @Override
    protected void doMerge(List<Map<String, Object>> dataObjectMaps, List<Map<String, Object>> mergerData) {

    }

    @PostConstruct
    private void init() {
        registerConverter("ad_dsp_unit", this);
    }
}
