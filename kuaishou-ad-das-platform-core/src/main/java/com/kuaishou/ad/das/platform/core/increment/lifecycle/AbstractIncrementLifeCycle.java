package com.kuaishou.ad.das.platform.core.increment.lifecycle;

import static com.kuaishou.ad.das.platform.core.increment.lifecycle.AdDasKbusIncrementFactory.getBatchFilling;
import static com.kuaishou.ad.das.platform.core.increment.lifecycle.AdDasKbusIncrementFactory.getBatchProducer;
import static com.kuaishou.ad.das.platform.core.increment.lifecycle.AdDasKbusIncrementFactory.getKbusBatchTransfer;
import static com.kuaishou.ad.das.platform.core.increment.lifecycle.AdDasKbusIncrementFactory.judgeSpFillingTable;
import static com.kuaishou.ad.das.platform.core.increment.lifecycle.AdDasKbusIncrementFactory.judgeSpKbusBatchTable;
import static com.kuaishou.ad.das.platform.core.increment.lifecycle.AdDasKbusIncrementFactory.judgeSpProduceTable;

import org.springframework.beans.factory.annotation.Autowired;

import com.kuaishou.ad.das.platform.core.increment.filling.AdDasIncrementBatchFilling;
import com.kuaishou.ad.das.platform.core.increment.filling.impl.AdDasIncrementCommonBatchFilling;
import com.kuaishou.ad.das.platform.core.increment.producer.AdDasIncrementBatchProducer;
import com.kuaishou.ad.das.platform.core.increment.producer.impl.AdDasIncrementCommonBatchProducer;
import com.kuaishou.ad.das.platform.core.increment.transfer.AdDasKbusBatchTransfer;
import com.kuaishou.ad.das.platform.core.increment.transfer.impl.AdDasKbusCommonBatchTransfer;

import lombok.extern.slf4j.Slf4j;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-01-27
 */
@Slf4j
public abstract class AbstractIncrementLifeCycle {

    @Autowired
    protected AdDasKbusCommonBatchTransfer adDasKbusCommonBatchConverter;

    @Autowired
    protected AdDasIncrementCommonBatchFilling adDasIncrementCommonBatchFilling;

    @Autowired
    protected AdDasIncrementCommonBatchProducer adDasIncrementCommonBatchProducer;

    protected AdDasKbusBatchTransfer choseTransfer(String tableName) {
        AdDasKbusBatchTransfer adDasKbusBatchTransfer;
        if (judgeSpKbusBatchTable(tableName)) {
            adDasKbusBatchTransfer = getKbusBatchTransfer(tableName);
        } else {
            // 默认converter
            adDasKbusBatchTransfer = adDasKbusCommonBatchConverter;
        }

        return adDasKbusBatchTransfer;
    }

    protected AdDasIncrementBatchFilling choseFilling(String tableName) {

        AdDasIncrementBatchFilling batchFilling;

        if (judgeSpFillingTable(tableName)) {
            batchFilling = getBatchFilling(tableName);
        } else {
            // 默认Filling
            batchFilling = adDasIncrementCommonBatchFilling;
        }

        return batchFilling;
    }


    protected AdDasIncrementBatchProducer choseProducer(String tableName) {
        AdDasIncrementBatchProducer batchProducer;

        if (judgeSpProduceTable(tableName)) {
            batchProducer = getBatchProducer(tableName);
        } else {
            // 默认Filling
            batchProducer = adDasIncrementCommonBatchProducer;
        }

        return batchProducer;
    }

}
