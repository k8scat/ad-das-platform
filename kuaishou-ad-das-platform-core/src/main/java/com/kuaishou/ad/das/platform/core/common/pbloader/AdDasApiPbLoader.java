package com.kuaishou.ad.das.platform.core.common.pbloader;

import java.net.MalformedURLException;

import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-12-08
 */
@Slf4j
@Component
public class AdDasApiPbLoader extends AdDasCommonPbLoader {

    /**
     * 平台初始化加载Pb
     * @throws MalformedURLException
     * @throws ClassNotFoundException
     */
    public void loadApiPb() throws MalformedURLException, ClassNotFoundException {
        defaultLoadPb();
    }
}
