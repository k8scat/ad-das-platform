package com.kuaishou.ad.das.platform.core.increment.model;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import com.google.protobuf.Message;
import com.kuaishou.ad.model.protobuf.AdEnumModel;

import lombok.Data;

/**
 * 通用转换后的数据对象， 特定表的数据对象 需要继承 该class
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-10-31
 */
@Data
public class AdDasCascadeModel implements Serializable {

    private static final long serialVersionUID = 2212071397821959185L;

    /**
     * AdInstance 类信息
     */
    private AdDasIncrementAdInstanceClassModel instanceClassModel;

    /**
     * 通用 pb 数据对象
     *  AdIncModel.AdInstance.Builder
     */
    private Message.Builder adInstanceBuilder;

    /**
     * 通用数据结果List
     */
    private Map<String, Message> instanceFields;

    /**
     * 用于kafka 分区选择 的key
     */
    private Object key;


    /**
     * 数据唯一id
     */
    private Long id;

    /**
     * 表名
     */
    private String tableName;

    /**
     * 分表
     */
    private String shardTableName;

    /**
     * 时间戳
     */
    private Long timestamp;

    /**
     * 程序化创意分
     */
    private Integer creativeScore;

    /**
     * 无 accountId 的数据 默认为0L
     */
    private Long accountId;

    /**
     * 数据层级
     */
    private String dataLevel;

    /**
     * 消息类型： insert、 update、delete
     */
    private AdEnumModel.AdEnum.AdDasMsgType msgType;

    /**
     * update 的字段
     */
    private List<String> modifyFieldList;

    /**
     * 级联热点处理时间
     */
    private Long processTime;
    /**
     * 原始创意id
     */
    private Long originCreativeId;
}
