package com.kuaishou.ad.das.platform.core.increment.entrance;

import java.util.List;

import com.kuaishou.ad.das.platform.core.increment.model.AdDasIncrementAdapterModel;
import com.kuaishou.infra.databus.common.domain.mysql.ChangeEventData;
import com.kuaishou.infra.databus.common.domain.mysql.ChangeEventHeader;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-06-05
 */
public interface AdDasPlatformIncrementEntrance {

    /**
     * 数据适配
     * @param eventHeader
     * @param changeEventData
     * @return
     * @throws Throwable
     */
    AdDasIncrementAdapterModel adapterData(ChangeEventHeader eventHeader, ChangeEventData changeEventData) throws Throwable;

    /**
     * 处理
     * @param adDasIncrementAdapterObject
     */
    void process(AdDasIncrementAdapterModel adDasIncrementAdapterObject);

    /**
     * 批量处理
     * @param rightDataList
     */
    void processBatch(List<AdDasIncrementAdapterModel> rightDataList);
}
