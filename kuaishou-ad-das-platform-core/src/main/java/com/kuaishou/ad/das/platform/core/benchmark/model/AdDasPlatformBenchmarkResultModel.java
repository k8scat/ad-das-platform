package com.kuaishou.ad.das.platform.core.benchmark.model;

import java.util.List;

import lombok.Data;

/**
 * DB 数据 处理结果集
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-01-29
 */
@Data
public class AdDasPlatformBenchmarkResultModel {

    /**
     * 数据结果集类型: 0-bytes 1-originData
     */
    private Integer resultType;

    /**
     * 数据大小
     */
    private Integer recordNum;

    /**
     * 写入数据
     */
    private List<byte[]> recordResults;

    /**
     * 原始数据结果： 应用于 不需要 byte[] 数据的场景
     */
    private List<Object> datas = null;
}
