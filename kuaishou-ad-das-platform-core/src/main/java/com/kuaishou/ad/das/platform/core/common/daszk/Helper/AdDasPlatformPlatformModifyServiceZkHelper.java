package com.kuaishou.ad.das.platform.core.common.daszk.Helper;

import org.springframework.stereotype.Component;

import com.kuaishou.ad.das.platform.core.common.AdDasPlatformAbstractZkHelper;

import lombok.extern.slf4j.Slf4j;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-01-03
 */
@Slf4j
@Component
public class AdDasPlatformPlatformModifyServiceZkHelper extends AdDasPlatformAbstractZkHelper {

    /**
     * 初始化 Proto 变更
     * @throws Exception
     */
    public void platformModifyProtoZkInit() throws Exception {
        initPlatformModifyService();
    }
}
