package com.kuaishou.ad.das.platform.core.benchmark.model;

import java.io.Serializable;
import java.util.List;

import com.kuaishou.ad.das.platform.utils.dasenum.AdDasBenchmarkStatusEnum;

import lombok.Data;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-07-06
 */
@Data
public class AdDasPlatformTaskDoneModel implements Serializable {

    private static final long serialVersionUID = -4476594485576631962L;

    /**
     * 基准流名称
     */
    private String clientName;

    /**
     * 运行时间戳
     */
    private Long timestamp;

    /**
     * 任务编号
     */
    private Integer taskNum;

    /**
     * 基准流路径
     */
    private String hdfsPath;

    /**
     * 执行任务的 worker 名称
     */
    private String assignName;

    /**
     * proto名
     */
    private String protoName;

    /**
     * 表名称
     */
    private String tableStr;

    private List<DoneTableResult> resultList;


    private Integer status;

    private String reason;

    public AdDasPlatformTaskDoneModel() {

    }

    public AdDasPlatformTaskDoneModel(AdDasPlatformClientTaskSingleModel taskSingleModel, String myName,
                                      AdDasBenchmarkStatusEnum statusEnum) {
        this.taskNum = taskSingleModel.getTaskNum();
        this.clientName = taskSingleModel.getClientName();
        this.timestamp = taskSingleModel.getTimestamp();
        this.hdfsPath = taskSingleModel.getHdfsPath();
        this.assignName = myName;
        this.status = statusEnum.getStatus();
        this.reason = statusEnum.getReason();
    }
}
