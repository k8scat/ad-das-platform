package com.kuaishou.ad.das.platform.core.benchmark.helper;

import static com.kuaishou.ad.das.platform.utils.constant.AdDasCommonConstant.AD_DAS_NAMESPACE_NEW;
import static com.kuaishou.ad.das.platform.utils.constant.AdDasCommonConstant.COMMON_BENCHMARK_LIFE_CYCLE_SUBTAG;
import static com.kuaishou.framework.util.PerfUtils.perf;

import org.springframework.stereotype.Service;

import com.kuaishou.ad.das.platform.core.benchmark.model.AdDasPlatformClientTaskSingleModel;
import com.kuaishou.ad.das.platform.core.benchmark.model.AdDasTaskHandleModel;
import com.kuaishou.ad.das.platform.core.benchmark.model.AdDasTaskResultModel;
import com.kuaishou.ad.das.platform.core.benchmark.model.AdDasTaskSchemaModel;
import com.kuaishou.ad.das.platform.utils.dasenum.AdDasBenchmarkStatusEnum;

import lombok.extern.slf4j.Slf4j;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-02-15
 */
@Slf4j
@Service
public class AdDasBenchmarkCommonBuildHelper {

    public AdDasTaskResultModel initBuildResultModel(AdDasPlatformClientTaskSingleModel taskSingleModel, AdDasBenchmarkStatusEnum statusEnum) {
        AdDasTaskResultModel adDasTaskResultModel = new AdDasTaskResultModel();
        adDasTaskResultModel.setTaskNum(taskSingleModel.getTaskNum());
        adDasTaskResultModel.setTimestamp(taskSingleModel.getTimestamp());
        adDasTaskResultModel.setStatusAndReason(statusEnum);

        return adDasTaskResultModel;
    }

    public AdDasTaskResultModel initBuildResultModel(AdDasTaskSchemaModel adDasTaskSchemaModel,
                                                     AdDasBenchmarkStatusEnum statusEnum) {

        AdDasTaskResultModel adDasTaskResultModel = new AdDasTaskResultModel();
        adDasTaskResultModel.setTaskNum(adDasTaskSchemaModel.getTaskNum());
        adDasTaskResultModel.setTimestamp(adDasTaskSchemaModel.getTimestamp());

        adDasTaskResultModel.setHdfsPath(adDasTaskSchemaModel.getHdfsPath());
        adDasTaskResultModel.setTableStr(adDasTaskSchemaModel.getMainTable());
        adDasTaskResultModel.setProtoName(adDasTaskSchemaModel.getPbClassFullName());

        adDasTaskResultModel.setStatusAndReason(statusEnum);

        return adDasTaskResultModel;
    }

    public AdDasTaskResultModel runError(AdDasTaskSchemaModel adDasTaskSchemaModel,
                                         AdDasTaskHandleModel taskHandleModel, Exception e,
                                         String extra2, AdDasBenchmarkStatusEnum statusEnum) {
        log.error("dasRun " + extra2 + " occur error! taskHandleModel={}",
                taskHandleModel, e);
        perf(AD_DAS_NAMESPACE_NEW, COMMON_BENCHMARK_LIFE_CYCLE_SUBTAG,
                "dasRunError", extra2,
                taskHandleModel.getTaskNum() + ":" + taskHandleModel.getTaskName())
                .logstash();

        return initBuildResultModel(adDasTaskSchemaModel, statusEnum);
    }

}
