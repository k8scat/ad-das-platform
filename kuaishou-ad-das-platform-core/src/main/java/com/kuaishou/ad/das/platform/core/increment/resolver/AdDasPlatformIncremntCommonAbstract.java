package com.kuaishou.ad.das.platform.core.increment.resolver;

import static com.kuaishou.ad.das.platform.utils.constant.AdDasCommonConstant.AD_DAS_NAMESPACE_NEW;
import static com.kuaishou.ad.das.platform.utils.constant.AdDasCommonConstant.Ad_DAS_INCREMENT;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.google.common.collect.Lists;
import com.kuaishou.ad.das.platform.core.increment.entrance.AdDasPlatformIncrementEntrance;
import com.kuaishou.ad.das.platform.core.increment.helper.AdDasPlatformIncrementCommonHelper;
import com.kuaishou.ad.das.platform.core.increment.model.AdDasIncrementAdapterModel;
import com.kuaishou.framework.binlog.util.BinlogResolver;
import com.kuaishou.framework.util.PerfUtils;
import com.kuaishou.infra.databus.client.MysqlChangeEventElement;
import com.kuaishou.infra.databus.common.domain.mysql.ChangeEventData;
import com.kuaishou.infra.databus.common.domain.mysql.ChangeEventHeader;
import com.kuaishou.infra.databus.common.domain.mysql.MysqlChangeEvent;

import lombok.extern.slf4j.Slf4j;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-06-05
 */
@Slf4j
public abstract class AdDasPlatformIncremntCommonAbstract {

    @Autowired
    protected AdDasPlatformIncrementCommonHelper adDasPlatformIncrementCommon;

    @Autowired
    protected AdDasPlatformIncrementEntrance adDasPlatformIncrementEntrance;

    /**
     * 过滤 服务消费的 table
     * @param tableName
     * @return
     */
    protected  boolean filterTable(String tableName) {
        return adDasPlatformIncrementCommon.filterTable(tableName);
    }

    /**
     * 过滤增量事件
     * @param changeEventData
     * @return
     */
    protected boolean defaultFilterEvenType(ChangeEventData changeEventData) {
        return adDasPlatformIncrementCommon.filterEventType(changeEventData);
    }

    /**
     * 批量数据过滤 + 数据适配
     * @param changeEventList
     * @return
     * @throws Throwable
     */
    public List<AdDasIncrementAdapterModel> adapterDatas(List<MysqlChangeEventElement> changeEventList) throws Throwable {
        List<AdDasIncrementAdapterModel> dataList = Lists.newArrayList();

        for (MysqlChangeEventElement eventElement : changeEventList) {
            MysqlChangeEvent changeEvent = eventElement.getChangeEvent();

            ChangeEventHeader changeEventHeader = changeEvent.getEventHeader();
            ChangeEventData changeEventData = changeEvent.getEventData().get();

            // 先过滤 新增 或 修改 事件
            if (adDasPlatformIncrementCommon.filterEventType(changeEventData)) {
                String tableName = changeEventHeader.getTableName();
                String noShardTableName = BinlogResolver.getNoShardTableName(tableName);

                // 新增 或 修改事件 && 在监听的table内
                if (adDasPlatformIncrementCommon.filterTable(noShardTableName)) {
                    AdDasIncrementAdapterModel adapterObject = adDasPlatformIncrementEntrance
                            .adapterData(changeEventHeader, changeEventData);
                    if (adapterObject == null) {
                        PerfUtils.perf(AD_DAS_NAMESPACE_NEW, Ad_DAS_INCREMENT, "rowError.buildConcreteData")
                                .logstash();
                    } else {
                        dataList.add(adapterObject);
                    }
                }
            }
        }

        return dataList;
    }

    /**
     * 数据过滤 + 数据适配
     * @param changeEventHeader
     * @param changeEventData
     * @return
     * @throws Throwable
     */
    public AdDasIncrementAdapterModel adapterData(ChangeEventHeader changeEventHeader, ChangeEventData changeEventData) throws Throwable {

        // 先过滤 新增 或 修改 事件
        if (adDasPlatformIncrementCommon.filterEventType(changeEventData)) {
            String tableName = changeEventHeader.getTableName();

            tableName = BinlogResolver.getNoShardTableName(tableName);
            // 新增 或 修改事件 && 在监听的table内
            if (adDasPlatformIncrementCommon.filterTable(tableName)) {
                AdDasIncrementAdapterModel adapterObject = adDasPlatformIncrementEntrance
                        .adapterData(changeEventHeader, changeEventData);
                if (adapterObject == null) {
                    PerfUtils.perf(AD_DAS_NAMESPACE_NEW, Ad_DAS_INCREMENT, "rowError.buildConcreteData")
                            .logstash();
                } else {
                    return adapterObject;
                }
            }
        }

        return null;
    }

}
