package com.kuaishou.ad.das.platform.core.utils;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.google.protobuf.Descriptors;
import com.google.protobuf.Message;
import com.kuaishou.ad.das.platform.core.benchmark.model.AdDasBenchmarkDb2PbColumnMsgView;

import lombok.extern.slf4j.Slf4j;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-01-31
 */
@Slf4j
@Service
public class AdDasPlatformPbReflectUtils {

    public static void setBuilderField(Message.Builder msgBuilder, Descriptors.Descriptor descriptor,
                                       String fieldKey, Object value) {
        Descriptors.FieldDescriptor fieldDescriptorType = descriptor.findFieldByName(fieldKey);
        boolean isRepeated = fieldDescriptorType.isRepeated();
        Descriptors.FieldDescriptor.JavaType type = fieldDescriptorType.getJavaType();

        if (isRepeated) {
            List<Object> objects = (List<Object>) value;
            for (Object obj : objects) {
                Object valueObject = getObject(obj, type, fieldDescriptorType); // getObject
                if (valueObject == null) {
                    // TODO perf 打点报警
                    throw new RuntimeException("pbColumn=" + fieldKey + ", type=" + type + "is not supported!");
                }
                msgBuilder.addRepeatedField(fieldDescriptorType, valueObject);
            }
        } else {
            msgBuilder.setField(fieldDescriptorType, getObject(value, type, fieldDescriptorType));
        }
    }

    public static Message buildFieldMessge(Message.Builder msgBuilder, Descriptors.Descriptor descriptor,
                                           String key, Object data) {

        setBuilderField(msgBuilder, descriptor, "name", key);
        Object o = data;
        if (o == null) {
            //不设置值
        } else if (o instanceof Double) {
            setBuilderField(msgBuilder, descriptor, "double_value", data);
        } else if (o instanceof Float) {
            setBuilderField(msgBuilder, descriptor, "double_value", data);
        } else if (o instanceof Number) {
            setBuilderField(msgBuilder, descriptor, "int_value", data);
        } else if (o instanceof byte[]) {
            setBuilderField(msgBuilder, descriptor, "string_value", new String((byte[])data));
        } else {
            setBuilderField(msgBuilder, descriptor, "string_value", data);
        }

        return msgBuilder.build();
    }

    public void buildPbMessage(Message.Builder msgBuilder, Map<String,Object> dataMap,
                                List<AdDasBenchmarkDb2PbColumnMsgView> columnSchemas,
                               Descriptors.Descriptor descriptor) {

        for (AdDasBenchmarkDb2PbColumnMsgView db2PbColumnMsgView : columnSchemas) {

            Object dbValue = dataMap.get(db2PbColumnMsgView.getDbColumn());
            String pbColumn = db2PbColumnMsgView.getPbColumn();

            Descriptors.FieldDescriptor filedDescriptor = descriptor.findFieldByName(pbColumn);
            if (filedDescriptor == null) {
                log.error("buildPbMessage() pbColumn={}, is not founded!", pbColumn);
                // TODO perf 打点报警
                throw new RuntimeException("pbColumn=" + pbColumn + " is not founded!");
            }

            boolean isRepeated = filedDescriptor.isRepeated();
            Descriptors.FieldDescriptor.JavaType type = filedDescriptor.getJavaType();
            if (isRepeated) {
                String value = (String) dbValue;
                String[] strArray = value.split(",");
                for (int i = 0; i < strArray.length; ++i) {
                    Object valueObject = getObject(strArray[i], type, filedDescriptor); // getObject
                    if (dbValue != null && valueObject == null) {
                        // TODO perf 打点报警
                        throw new RuntimeException("pbColumn=" + pbColumn + ", type=" + type + "is not supported!");
                    }
                    msgBuilder.addRepeatedField(filedDescriptor, valueObject);
                }
            } else {
                Object valueObject = getObject(dbValue, type, filedDescriptor);
                if (dbValue != null && valueObject == null) {
                    // TODO perf 打点报警
                    throw new RuntimeException("pbColumn=" + pbColumn + ", type=" + type + "is not supported!");
                }

                msgBuilder.setField(filedDescriptor, valueObject);
            }
        }
    }

    public static Object getObject(Object obj, Descriptors.FieldDescriptor.JavaType type,
                                    Descriptors.FieldDescriptor filedDescriptor) {
        switch (type) {
            case INT:
                return ((Number) obj).intValue();
            case LONG:
                return ((Number) obj).longValue();
            case FLOAT:
                return ((Number) obj).floatValue();
            case DOUBLE:
                return ((Number) obj).doubleValue();
            case BOOLEAN:
                return getBoolean(obj);
            case STRING:
                return getStringDefault(obj);
            case ENUM:
                return getEnum(obj, filedDescriptor);
            default:
                // 其他类型暂不支持
                return obj;
        }
    }

    private static Object getStringDefault(Object obj) {

        if (obj == null) {
            return "";
        }
        return obj;
    }

    private static Object getBoolean(Object obj) {

        if (obj instanceof Number) {
            if ((obj != null) && 1 == ((Number) obj).intValue()) {
                return true;
            }

            if ((obj != null) && 0 == ((Number) obj).intValue()) {
                return false;
            }
        } else {
            log.error("getBoolean() obj={} is not number!", obj);
        }

        return null;
    }

    private static Object getEnum(Object obj,Descriptors.FieldDescriptor filedDescriptor) {
        Descriptors.EnumDescriptor enumDescriptor = filedDescriptor.getEnumType();
        return enumDescriptor.findValueByNumber((Integer) obj);
    }
}
