package com.kuaishou.ad.das.platform.core.common;

import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-06-21
 */
@Slf4j
@Component
public class AdDasPlatformServiceConfig {

    public static String dasServiceType; // 服务类型 api、client、worker、worker_master、increment、platform_runner
    public static String dasServiceEnv ;  // online、beta、test、staging
    public static String dasServiceName ;  // 服务启动类名称
    public static String dasBenchmarkEnv ; // 基准服务环境

    static {
        dasServiceType = System.getenv("DAS_SERVICE_TYPE"); // 区分服务类型
        dasServiceEnv = System.getenv("DAS_SERVICE_ENV");  //online、beta、test、staging
        dasServiceName = System.getenv("DAS_SERVICE_NAME");  //
        dasBenchmarkEnv = System.getenv("DAS_BENCHMARK_ENV"); // 区分服务类型
        log.info("dasServiceType={}, dasServiceEnv={}, dasServiceName={}, dasBenchmarkEnv",
                dasServiceType, dasServiceEnv, dasServiceName, dasBenchmarkEnv);
    }

    public String getServiceType() {
        return dasServiceType;
    }

    public String getServiceEnv() {
        return dasServiceEnv;
    }

    public String getServiceName() {
        return dasServiceName;
    }

    public String getBenchmarkEnv() {
        return dasBenchmarkEnv;
    }

    public boolean isWorker() {
        return "worker".equals(dasServiceType);
    }

    public boolean isWorkerMaster() {
        return "worker_master".equals(dasServiceType);
    }

    public boolean isClient() {
        return "client".equals(dasServiceType);
    }

    public boolean isApi() {
        return "api".equals(dasServiceType);
    }

    public boolean isIncre() {
        return "increment".equals(dasServiceType);
    }

    public boolean isPbRunner() {
        return "platform_runner".equals(dasServiceType);
    }
}
