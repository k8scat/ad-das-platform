package com.kuaishou.ad.das.platform.core.increment.transfer.impl;

import static com.kuaishou.ad.das.platform.core.utils.AdDasPlatformPbReflectUtils.setBuilderField;
import static com.kuaishou.ad.das.platform.utils.DateTimeUtils.getCurNanoTime;
import static com.kuaishou.ad.das.platform.utils.constant.AdDasCommonConstant.AD_DAS_COMMON_INCR_LIFE_CYCLE_SUBTAG;
import static com.kuaishou.ad.das.platform.utils.constant.AdDasCommonConstant.AD_DAS_NAMESPACE_NEW;
import static com.kuaishou.ad.das.platform.utils.constant.AdDasCommonConstant.Ad_DAS_BENCHMARK;
import static com.kuaishou.framework.util.PerfUtils.perf;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.function.BiConsumer;
import java.util.stream.Collectors;

import org.apache.commons.lang3.time.StopWatch;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.protobuf.Descriptors;
import com.google.protobuf.Message;
import com.google.protobuf.ProtocolMessageEnum;
import com.kuaishou.ad.das.platform.core.increment.helper.AdDasPlatformIncrementCommonHelper;
import com.kuaishou.ad.das.platform.core.increment.model.AdDasCascadeModel;
import com.kuaishou.ad.das.platform.core.increment.model.AdDasIncrementAdInstanceClassModel;
import com.kuaishou.ad.das.platform.core.increment.model.AdDasIncrementAdapterModel;
import com.kuaishou.ad.das.platform.utils.AdDasBeanCoreUtils;
import com.kuaishou.ad.model.protobuf.AdEnumModel;
import com.kuaishou.framework.util.HostInfo;
import com.kuaishou.framework.util.PerfUtils;

import lombok.extern.slf4j.Slf4j;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-10-31
 */
@Slf4j
public abstract class AdDasKbusAbstractBatchTransfer<T extends AdDasCascadeModel> {

    @Autowired
    protected AdDasPlatformIncrementCommonHelper adDasPlatformIncrementCommonHelper;

    protected abstract T buildConverterData(String tableName, AdDasIncrementAdapterModel adDasKbusConcreteData,
                                            Message.Builder adInstanceBuilder, AdDasIncrementAdInstanceClassModel classModel);

    protected abstract void convertMore(Object row, T adDasCascadeModel);

    protected List<T> defaultBatchConvert(
            String tableName, Long accountId, List<AdDasIncrementAdapterModel> adDasKbusConcreteDatas,
            BiConsumer<Map<String, Serializable>, T> convertConsumer) {

        StopWatch createdAll = StopWatch.createStarted();

        List<T> commonConvertDataList = new ArrayList<>(
                adDasKbusConcreteDatas.size());

        adDasKbusConcreteDatas.forEach(adDasKbusConcreteData -> {
            try {
                AdDasIncrementAdInstanceClassModel classModel = adDasPlatformIncrementCommonHelper.getInstanceClassModel();
                // init
                Message.Builder adInstanceBuilder = initBuilder(tableName, adDasKbusConcreteData, classModel);

                if (HostInfo.debugHost()) {
                    log.info("【AdDasKbusAbstractBatchTransfer】defaultBatchConvert() adDasKbusConcreteData={}",
                            adDasKbusConcreteData);
                }

                // build final data
                T commonConvertData = buildConverterData(tableName,
                        adDasKbusConcreteData, adInstanceBuilder, classModel);

                // 执行 自定义 convert 逻辑
                if (convertConsumer != null) {
                    convertConsumer.accept(adDasKbusConcreteData.getRow(), commonConvertData);
                }

                commonConvertDataList.add(commonConvertData);
            } catch (Exception e) {
                perf(AD_DAS_NAMESPACE_NEW, AD_DAS_COMMON_INCR_LIFE_CYCLE_SUBTAG, "lifeCycleError","defaultBatchConvert", tableName)
                        .logstash();
                log.error("defaultBatchConvert() ocurr error! adDasKbusConcreteData={}", adDasKbusConcreteData, e);
            }
        });


        createdAll.stop();
        perf(AD_DAS_NAMESPACE_NEW, AD_DAS_COMMON_INCR_LIFE_CYCLE_SUBTAG, "defaultBatchConvert", tableName)
                .micros(createdAll.getTime(TimeUnit.MICROSECONDS))
                .logstash();

        if (HostInfo.debugHost()) {
            log.info("【AdDasKbusAbstractBatchTransfer】defaultBatchConvert() tableName={}, commonConvertDataList.size={}",
                    tableName, commonConvertDataList.size());
        }
        return commonConvertDataList;
    }

    protected Message.Builder initBuilder(
            String tableName, AdDasIncrementAdapterModel adDasKbusConcreteData, AdDasIncrementAdInstanceClassModel classModel) throws InvocationTargetException, IllegalAccessException {

        Long timestamp = adDasKbusConcreteData.getTimestamp();
        Map<String, Serializable> row = adDasKbusConcreteData.getRow();

        if (timestamp == null || timestamp < 0 || row == null) {
            //  perf 打点
            PerfUtils.perf(AD_DAS_NAMESPACE_NEW, Ad_DAS_BENCHMARK, "initBuilderError", tableName)
                    .logstash();
            return null;
        }

        ProtocolMessageEnum adInstanceType = adDasPlatformIncrementCommonHelper.getAdInstanceType(tableName);

        Message.Builder msgBuilder = (Message.Builder) classModel.getMethod().invoke(null, null);
        Descriptors.Descriptor descriptor = msgBuilder.getDescriptorForType();
        setBuilderField(msgBuilder, descriptor, "uuid", UUID.randomUUID().toString());
        setBuilderField(msgBuilder, descriptor, "create_time", System.currentTimeMillis());
        setBuilderField(msgBuilder, descriptor, "process_time", getCurNanoTime());
        setBuilderField(msgBuilder, descriptor, "binlog_time", timestamp);
        setBuilderField(msgBuilder, descriptor, "normal_flag", 1);
        setBuilderField(msgBuilder, descriptor, "msg_type", adDasKbusConcreteData.getMsgType().getNumber());
        setBuilderField(msgBuilder, descriptor, "type", adInstanceType.getNumber());

        if (AdEnumModel.AdEnum.AdDasMsgType.UPDATE == adDasKbusConcreteData.getMsgType()) {

            List<String> modifyFieldList = adDasKbusConcreteData.getModifyFieldList()
                    .stream()
                    .filter(modifyField -> modifyField != null)
                    .collect(Collectors.toList());
            setBuilderField(msgBuilder, descriptor, "modify_field_list", modifyFieldList);

        }

        return msgBuilder;
    }

    protected AdDasCascadeModel defaultBuildConverterData(
            String tableName, AdDasIncrementAdapterModel adDasIncrementAdapterObject,
            Message.Builder adInstanceBuilder, AdDasIncrementAdInstanceClassModel classModel) {

        // 获取key
        Object key = adDasPlatformIncrementCommonHelper.getHashKeyForKbus(tableName, adDasIncrementAdapterObject.getRow());

        AdDasCascadeModel cascadeModel = new AdDasCascadeModel();
        AdDasBeanCoreUtils.copyPropertiesIgnoreNull(cascadeModel, adDasIncrementAdapterObject);
        cascadeModel.setAdInstanceBuilder(adInstanceBuilder);
        cascadeModel.setKey(key);
        cascadeModel.setInstanceClassModel(classModel);

        if (HostInfo.debugHost()) {
            log.info("【AdDasKbusCommonBatchConverter】doBatchConvert() tableName={}, cascadeModel={}",
                    tableName, cascadeModel);
        }

        return cascadeModel;
    }

}
