package com.kuaishou.ad.das.platform.core.benchmark.helper;

import static com.kuaishou.ad.das.platform.utils.constant.AdDasCommonConstant.AD_DAS_NAMESPACE_NEW;
import static com.kuaishou.ad.das.platform.utils.kconf.AdDasBaseCommonKconf.adBaseBenchmarkDataFinalShardSize;
import static com.kuaishou.ad.das.platform.utils.kconf.AdDasBaseCommonKconf.adBaseBenchmarkDataShardSize;
import static com.kuaishou.ad.das.platform.utils.kconf.AdDasBaseCommonKconf.adBaseBenchmarkLowestDataSize;
import static com.kuaishou.framework.util.PerfUtils.perf;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.stream.IntStream;

import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.google.common.collect.Lists;
import com.google.common.io.LittleEndianDataOutputStream;
import com.google.protobuf.Message;

import lombok.extern.slf4j.Slf4j;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-01-31
 */
@Slf4j
@Service
public class AdDasBenchmarkCommonConvertHelper {

    public List<byte[]> convertBytes(List<Message> originDatas, Integer shardSize) throws IOException {
        List<byte[]> datas = Lists.newArrayList();

        // 小于20w 直接处理
        if (originDatas.size() < adBaseBenchmarkLowestDataSize.get()) {
            List<byte[]> data = finalConvertData(originDatas, null);
            datas.addAll(data);
            return datas;
        }

        // 预估数据量大小进行shard 计算 前5个 和后 5个 数据对象的 平均 byteArray 大小来预估整体大小
        Integer avgSize = countAvgLength(originDatas);
        log.info("convertBytes() avgSize={}", avgSize);

        // 未指定 shardSize, 进行采样
        if (shardSize == null) {
            // FIXME : 按照 500M 进行拆分
            shardSize = adBaseBenchmarkDataShardSize.get() / avgSize;
        }
        log.info("convertBytes() shardSize={}, originDatas.size()={}", shardSize, originDatas.size());

        // shardSize 异常 直接处理返回
        if (shardSize == null) {
            List<byte[]> data = finalConvertData(originDatas, avgSize * originDatas.size());
            datas.addAll(data);
            return datas;
        }

        // shardSize 过大 直接处理 返回
        if (originDatas.size() <= shardSize) {
            List<byte[]> data = finalConvertData(originDatas, avgSize * originDatas.size());
            datas.addAll(data);
            return datas;
        }

        // 分 shard 处理
        List<List<Message>> originDataSubList = Lists.partition(originDatas, shardSize);
        for (List<Message> originDataSub : originDataSubList) {
            List<byte[]> data = finalConvertData(originDataSub, avgSize * originDataSub.size());
            datas.addAll(data);
        }

        return datas;
    }

    private List<byte[]> finalConvertData(List<Message> originDatas, Integer initSize) throws IOException {

        List<byte[]> result = Lists.newArrayList();

        ByteArrayOutputStream byteArrayOutputStream;
        if (initSize == null) {
            byteArrayOutputStream = new ByteArrayOutputStream();
        } else {
            byteArrayOutputStream = new ByteArrayOutputStream(initSize);
        }

        LittleEndianDataOutputStream out = new LittleEndianDataOutputStream(byteArrayOutputStream);

        if (CollectionUtils.isEmpty(originDatas)) {
            // 查不到数据写空文件
            byte[] resultSuccess = byteArrayOutputStream.toByteArray();
            result.add(resultSuccess);
            return result;
        }

        try {
            for (Message originData : originDatas) {

                if (originData == null) {
                    perf("", "", "originData is null", "")
                            .count(1).logstash();
                    log.error("finalConvertData() originData is null");
                }

                // 大于 700M 时进行拆分
                if (byteArrayOutputStream.size() > adBaseBenchmarkDataFinalShardSize.get()) {

                    log.warn("finalConvertData() byteArrayOutputStream.size={}, is overMaxShardCount={}",
                            byteArrayOutputStream.size(), adBaseBenchmarkDataShardSize.get());
                    byte[] resultSuccess = byteArrayOutputStream.toByteArray();
                    result.add(resultSuccess);

                    // 切换新的 流对象
                    log.warn("finalConvertData() new byteArrayOutputStream: initSize={}", initSize);
                    if (initSize == null) {
                        byteArrayOutputStream = new ByteArrayOutputStream();
                    } else {
                        byteArrayOutputStream = new ByteArrayOutputStream(initSize);
                    }
                    out = new LittleEndianDataOutputStream(byteArrayOutputStream);
                }

                byte[]  byteData = originData.toByteArray();
                out.writeInt(byteData.length);
                out.write(byteData);
            }

            // final转换
            byte[] resultSuccess = byteArrayOutputStream.toByteArray();
            result.add(resultSuccess);

            log.info("finalConvertData() result.size={}", result.size());

            return result;
        } finally {
            out.close();
            byteArrayOutputStream.close();
        }
    }

    /**
     * 预估 单条数据大小
     * @param originDatas
     * @return
     */
    private Integer countAvgLength(List<Message> originDatas) {

        try {
            int countNum = 5;

            List<Integer> dataIndexList = Lists.newArrayList();
            IntStream.range(0, countNum).forEach(index -> dataIndexList.add(index));
            IntStream.range(originDatas.size() - countNum - 1, originDatas.size() - 1).
                    forEach(index -> dataIndexList.add(index));
            log.info("countAvgLength() originDatas.size={}, dataIndexList={}",
                    originDatas.size(), dataIndexList);

            Integer totalSize = 0;
            for (Integer lowIndex : dataIndexList) {
                byte[] byteData = originDatas.get(lowIndex).toByteArray();
                totalSize = totalSize + 4 + byteData.length;

            }

            if (totalSize == 0) {

                return 1;
            }
            // avg 10
            Integer avgSize = totalSize / dataIndexList.size();

            return avgSize;
        } catch (Exception e) {
            log.error("countAvgLength() occur error: ", e);
        }

        return null;
    }
}
