package com.kuaishou.ad.das.platform.core.benchmark.model;

import java.io.Serializable;

import com.kuaishou.ad.das.platform.utils.dasenum.AdDasPlatformZkMsgEnum;

import lombok.Data;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-06-06
 */
@Data
public class AdDasPlatformZkMsgModel implements Serializable {

    private static final long serialVersionUID = 2565806460272281874L;

    /**
     * 消息类型
     */
    private AdDasPlatformZkMsgEnum msgType;

    /**
     * 消息路径
     */
    private String msgPath;

    /**
     * 详细消息
     * 1.当msgTYpe=CLIENT_PUBLISH_MSG 时， 对应  AdDasPlatformClientTaskBatchModel
     * 2.当msgTYpe=WORKER_MASTER_ASSIGN_MSG || MASTER_RETRY_TASK_MSG 时， 对应  AdDasPlatformClientTaskSingleModel
     * 2.当msgTYpe=MASTER_CHECK_TASK_MSG 时， 对应 AdDasPlatformTaskDoneModel
     */
    private String contentData;

    /**
     * 消息时间
     */
    private Long msgTimestamp;

    /**
     * 消息去重标识 msgType:msgPath
     */
    private String msgId;

}
