package com.kuaishou.ad.das.platform.core.benchmark.model;

import java.io.Serializable;

import lombok.Data;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-06-06
 */
@Data
public class AdDasPlatformBenchmarkNodeTaskModel implements Serializable {

    /**
     * server 实例节点名称
     */
    private String serverNodeName;

    /**
     * 实例节点运行
     */
    private Integer taskId;

    /**
     * 任务重试次数
     */
    private Integer taskRetryTimes = 0;

}
