package com.kuaishou.ad.das.platform.core.increment.resolver;

import static com.kuaishou.ad.das.platform.utils.constant.AdDasCommonConstant.AD_DAS_COMMON_INCR_LIFE_CYCLE_SUBTAG;
import static com.kuaishou.ad.das.platform.utils.constant.AdDasCommonConstant.AD_DAS_NAMESPACE_NEW;
import static com.kuaishou.framework.util.PerfUtils.perf;
import static com.kuaishou.infra.framework.defination.ProductDef.KUAISHOU;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.jetbrains.annotations.NotNull;
import org.springframework.util.CollectionUtils;

import com.google.common.collect.Sets;
import com.kuaishou.ad.das.platform.core.increment.model.AdDasIncrementAdapterModel;
import com.kuaishou.framework.binlog.util.BinlogResolver;
import com.kuaishou.framework.util.HostInfo;
import com.kuaishou.framework.warmup.WarmupAble;
import com.kuaishou.infra.databus.client.MysqlChangeEventElement;
import com.kuaishou.infra.databus.client.resolver.mysql.DatabusMysqlBatchMessageContext;
import com.kuaishou.infra.databus.client.resolver.mysql.DatabusMysqlTransactionalKeyAffinityResolver;
import com.kuaishou.infra.databus.common.domain.mysql.ChangeEventData;
import com.kuaishou.infra.databus.common.domain.mysql.ChangeEventHeader;
import com.kuaishou.infra.databus.common.domain.mysql.DmlChangedEventRow;
import com.kuaishou.infra.databus.common.domain.mysql.Gtid;
import com.kuaishou.infra.databus.common.domain.mysql.MysqlChangeEvent;
import com.kuaishou.infra.databus.common.domain.mysql.MysqlEventColumn;
import com.kuaishou.infra.framework.defination.ProductDef;

import kuaishou.common.BizDef;
import lombok.extern.slf4j.Slf4j;

/**
 * 事务性消费 抽象类
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-06-01
 */
@Slf4j
public abstract class AdDasPlatformIncrementTransactionAbstractResolver
        extends AdDasPlatformIncremntCommonAbstract
        implements DatabusMysqlTransactionalKeyAffinityResolver, WarmupAble {

    @Override
    public void resolveBatch(String clusterId, Gtid gtid, List<MysqlChangeEventElement> changeEventList, DatabusMysqlBatchMessageContext messageContext) throws Throwable {

        List<AdDasIncrementAdapterModel> adDasIncrementAdapterObjects = adapterDatas(changeEventList);

        if (CollectionUtils.isEmpty(adDasIncrementAdapterObjects)) {
            return;
        }
        if (HostInfo.debugHost()) {
            log.info("dasRun() adDasIncrementAdapterObjects={}", adDasIncrementAdapterObjects);
        }
        adDasPlatformIncrementEntrance.processBatch(adDasIncrementAdapterObjects);
    }

    @Override
    public Object calcAffinityKey(String clusterId, Gtid gtid, List<MysqlChangeEventElement> changeEventList, DatabusMysqlBatchMessageContext messageContext) throws Throwable {

        String shardKey = adDasPlatformIncrementCommon.getClusterShardId();
        if ("clusterId".equals(shardKey)) {
            return clusterId;
        }

        Set<Long> shardIdSet = Sets.newHashSet();
        for (MysqlChangeEventElement eventElement : changeEventList) {

            MysqlChangeEvent changeEvent = eventElement.getChangeEvent();
            ChangeEventHeader changeEventHeader = changeEvent.getEventHeader();
            ChangeEventData changeEventData = changeEvent.getEventData().get();

            if (defaultFilterEvenType(changeEventData)) {

                String tableName = changeEventHeader.getTableName();
                tableName = BinlogResolver.getNoShardTableName(tableName);
                // 新增 或 修改事件 && 在监听的table内
                if (filterTable(tableName)) {
                    Map<String, MysqlEventColumn> rowColumnMaps = ((DmlChangedEventRow) changeEventData).getAffectedRow();

                    if (rowColumnMaps.containsKey(shardKey)) {
                        MysqlEventColumn mysqlEventColumn =  rowColumnMaps.get(shardKey);
                        Long shardId = ((Number) mysqlEventColumn.getData()).longValue();
                        shardIdSet.add(shardId);
                    }
                }
            }
        }
        if (!CollectionUtils.isEmpty(shardIdSet) && shardIdSet.size() == 1) {

            perf(AD_DAS_NAMESPACE_NEW, AD_DAS_COMMON_INCR_LIFE_CYCLE_SUBTAG, "shardIdFlag")
                    .logstash();
            return shardIdSet.stream().findFirst();
        } else if (!CollectionUtils.isEmpty(shardIdSet) && shardIdSet.size() > 1) {
            perf(AD_DAS_NAMESPACE_NEW, AD_DAS_COMMON_INCR_LIFE_CYCLE_SUBTAG,
                    "errorShardIdFlag", shardIdSet.toString())
                    .logstash();
            log.error("defaultCalcAffinityKey() shardIdSet is error! clusterId={}, shardIdSet={}", clusterId, shardIdSet);
            return clusterId;
        } else {
            perf(AD_DAS_NAMESPACE_NEW, AD_DAS_COMMON_INCR_LIFE_CYCLE_SUBTAG, "defaultShardIdFlag", shardIdSet.toString())
                    .logstash();
            return clusterId;
        }

    }

    @Override
    public void tryWarmup() {
        adDasPlatformIncrementCommon.tryWarmup();
    }

    @NotNull
    @Override
    public String dataSourceName() {
        return adDasPlatformIncrementCommon.getDataSourceName();
    }

    @NotNull
    @Override
    public String consumerGroup() {
        return adDasPlatformIncrementCommon.getConsumerGroup();
    }

    @NotNull
    @Override
    public ProductDef productDef() {
        return KUAISHOU;
    }

    @NotNull
    @Override
    public BizDef bizDef() {
        return BizDef.AD_DSP;
    }
}
