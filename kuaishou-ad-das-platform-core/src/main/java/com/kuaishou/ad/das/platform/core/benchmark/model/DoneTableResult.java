package com.kuaishou.ad.das.platform.core.benchmark.model;

import java.io.Serializable;

import lombok.Data;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-08-09
 */
@Data
public class DoneTableResult implements Serializable {

    private static final long serialVersionUID = -3832158727098878658L;

    /**
     * 任务标号
     */
    private Integer assignmentId;

    /**
     * 大数据表文件shardId
     */
    private Integer shardId = 0;

    /**
     * 任务执行时间戳
     */
    private Long timestamp;

    /**
     * 表名称
     */
    private String tableStr;

    /**
     * 文件名称
     */
    private String fileName;

    /**
     * 数据大小
     */
    private Integer recordNum;

}
