package com.kuaishou.ad.das.platform.core.benchmark.client.checker;

import static com.kuaishou.ad.das.platform.core.common.AdDasBenchmarkZkModel.DONE;
import static com.kuaishou.ad.das.platform.core.common.AdDasBenchmarkZkModel.MASTER_DATA_RECORD;
import static com.kuaishou.ad.das.platform.core.common.AdDasBenchmarkZkModel.PLATFORM_BENCHMARK;
import static com.kuaishou.ad.das.platform.core.common.AdDasBenchmarkZkModel.TASK;
import static com.kuaishou.ad.das.platform.core.common.AdDasBenchmarkZkModel.TASKCONTENT;
import static com.kuaishou.ad.das.platform.core.common.AdDasBenchmarkZkModel.TASKS;
import static com.kuaishou.ad.das.platform.core.utils.AdBaseUtils.timeToString;
import static com.kuaishou.ad.das.platform.core.utils.AdDasBaseMultiEnvUtils.getDasBenchmarkCommonPath;
import static com.kuaishou.ad.das.platform.utils.constant.AdDasCommonConstant.AD_DAS_NAMESPACE_NEW;
import static com.kuaishou.ad.das.platform.utils.constant.AdDasCommonConstant.Ad_DAS_BENCHMARK;
import static com.kuaishou.ad.das.platform.utils.dasenum.AdDasBenchmarkStatusEnum.SUCESS_STATUS;
import static com.kuaishou.ad.das.platform.utils.kconf.AdDasBaseCommonKconf.adBaseBenchmarkDataSizeCheckTableName;
import static com.kuaishou.ad.das.platform.utils.kconf.AdDasBaseCommonKconf.adBaseBenchmarkMasterCheckDeltaPercent;
import static com.kuaishou.framework.util.PerfUtils.perf;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;

import org.apache.commons.compress.utils.Lists;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.kuaishou.ad.das.platform.core.benchmark.helper.AdDasBenchmarkPlatformClientZkHelper;
import com.kuaishou.ad.das.platform.core.benchmark.model.AdDasPlatformClientCheckMsgModel;
import com.kuaishou.ad.das.platform.core.benchmark.model.AdDasPlatformClientTaskBatchModel;
import com.kuaishou.ad.das.platform.core.benchmark.model.AdDasPlatformTaskDoneModel;
import com.kuaishou.ad.das.platform.core.benchmark.model.DataSizeCheckResult;
import com.kuaishou.ad.das.platform.core.common.AdDasPlatformServiceConfig;
import com.kuaishou.ad.das.platform.utils.HdfsUtils;
import com.kuaishou.ad.model.protobuf.tables.Meta;
import com.kuaishou.framework.util.ObjectMapperUtils;

import lombok.extern.slf4j.Slf4j;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-06-06
 */
@Lazy
@Slf4j
@Component
public class AdDasPLatformBenchmarkChecker {

    @Autowired
    private AdDasBenchmarkPlatformClientZkHelper clientCommonZkHelper;

    @Autowired
    protected AdDasPlatformServiceConfig platformServiceConfig;

    /**
     * 清理重跑 dump_info
     */
    public void checkAndDealDumpInfo(Long timestamp, String hdfsPath) {

        String clientName = platformServiceConfig.getServiceName();

        String dumpDoneFilePath = hdfsPath + timeToString(timestamp) + "/dump_done";

        String dumpInfoFilePath = hdfsPath + timeToString(timestamp) + "/dump_info";

        try {

            if (HdfsUtils.delFileWithRetry(dumpDoneFilePath)) {
                log.info("【NOTICE】del dump_done={} success!", dumpDoneFilePath);
            }

            if (HdfsUtils.delFileWithRetry(dumpInfoFilePath)) {
                log.info("【NOTICE】del dump_info={} success!", dumpInfoFilePath);
            }
        } catch (Exception e) {
            log.error("【NOTICE】checkAndDealDumpInfo() ocurr error: timeStamp={}, clientName={}",
                    timestamp, clientName);
        }
    }

    public AdDasPlatformClientCheckMsgModel checkAllTask(AdDasPlatformClientTaskBatchModel clientTaskModel) {

        AtomicReference<Boolean> isDone = new AtomicReference<>(true);
        AdDasPlatformClientCheckMsgModel clientCheckMsgModel = new AdDasPlatformClientCheckMsgModel();

        Set<Integer> doingTaskNums = Sets.newHashSet();
        List<String> doneContents = Lists.newArrayList();

        clientTaskModel.getTaskIds().stream().forEach(taskId -> {
            try {
                Thread.sleep(100);
                String taskContent = clientCommonZkHelper.getPathData(PLATFORM_BENCHMARK + getDasBenchmarkCommonPath(platformServiceConfig.getBenchmarkEnv()) + TASKS
                        + "/" + clientTaskModel.getClientName() + TASK + "/" + taskId.toString() + TASKCONTENT);

                if (StringUtils.isEmpty(taskContent)) {
                    isDone.set(false);
                    return;
                }

                String doneContent = clientCommonZkHelper.getPathData(PLATFORM_BENCHMARK + getDasBenchmarkCommonPath(platformServiceConfig.getBenchmarkEnv()) + TASKS
                        + "/" + clientTaskModel.getClientName() + TASK + "/"  + taskId.toString() + DONE);

                if (StringUtils.isEmpty(doneContent)) {
                    isDone.set(false);
                    doingTaskNums.add(taskId);
                } else {
                    if (checkContent(doneContent)) {
                        doneContents.add(doneContent);
                    } else {
                        isDone.set(false);
                    }
                }
            } catch (Exception e) {
                log.error("zk error", e);
                perf(AD_DAS_NAMESPACE_NEW, Ad_DAS_BENCHMARK,
                        platformServiceConfig.getServiceName(),
                        platformServiceConfig.getServiceEnv(),
                        "recieveTask-Res=" + taskId + "-" + e.getMessage(),
                        "error").count(1).logstash();
            }
        });

        if (isDone.get()) {
            clientCheckMsgModel.setSuccessFlag(true);
        }
        clientCheckMsgModel.setDoingTaskNums(doingTaskNums);
        clientCheckMsgModel.setDoneContents(doneContents);

        return clientCheckMsgModel;
    }

    public boolean checkContent(String content) {

        AdDasPlatformTaskDoneModel dasPlatformTaskDoneModel = ObjectMapperUtils
                .fromJSON(content, AdDasPlatformTaskDoneModel.class);

        if (dasPlatformTaskDoneModel.getStatus() == null || (dasPlatformTaskDoneModel.getStatus() != null
                && dasPlatformTaskDoneModel.getStatus().intValue() != SUCESS_STATUS.getStatus())) {
            log.error("【NOTICE ERROR】dasPlatformTaskDoneModel={}", dasPlatformTaskDoneModel);

            return false;
        }

        return true;
    }

    /**
     * 校验数据结果
     */
    public boolean checkFinalData(Meta.DumpInfo dumpInfo, AdDasPlatformClientTaskBatchModel clientTaskModel) {

        try {
            String dataFilePath = clientTaskModel.getHdfsPath() + timeToString(clientTaskModel.getTimestamp());

            log.info("checkDataSizeBeforeDone() start!!! clientTaskModel={}", clientTaskModel);
            Map<String, Long> dataFileSizeMap = HdfsUtils.listPathAndSizeMap(dataFilePath);
            Map<String, Long> tableName2DataCountMap = Maps.newHashMap();
            Map<String, Long> tableName2DataFileSizeMap = Maps.newHashMap();

            // 将dump_info 的数据 转换到 dataSizeCheckResultNow.tableName2DataCountMap
            // 获取当前目录下的dataFile的 文件大小
            for (Meta.DumpData dumpData : dumpInfo.getInfoList()) {

                String tableName = dumpData.getTableName();
                String fileName = dumpData.getFileName();
                Long recordNum = Integer.valueOf(dumpData.getRecordNum()).longValue();

                if (tableName2DataCountMap.containsKey(tableName)) {
                    Long countTmp = tableName2DataCountMap.get(tableName);
                    tableName2DataCountMap.put(tableName, countTmp + recordNum);
                } else {
                    tableName2DataCountMap.put(tableName, recordNum);
                }

                Long fileSize = dataFileSizeMap.get(fileName) == null ? 0L : dataFileSizeMap.get(fileName);
                if (tableName2DataFileSizeMap.containsKey(tableName)) {
                    Long sizeTmp = tableName2DataFileSizeMap.get(tableName);
                    tableName2DataFileSizeMap.put(tableName, sizeTmp + fileSize);
                } else {
                    tableName2DataFileSizeMap.put(tableName, fileSize);
                }
            }

            // 打点每个tableName对应的导出数量
            perfTableDataSize(tableName2DataCountMap, clientTaskModel);

            //  zk 记录节点 为永久节点， 不存在则创建
            // 取出上一次的记录数，不存在则不对比，存入这次运行的记录数据
            String recordPath = PLATFORM_BENCHMARK + getDasBenchmarkCommonPath(platformServiceConfig.getBenchmarkEnv()) + TASKS + "/" + clientTaskModel.getClientName() + MASTER_DATA_RECORD;
            clientCommonZkHelper.checkRecordNodeExist(recordPath);
            String recordResultData = clientCommonZkHelper.getPathData(recordPath);

            if (!StringUtils.isEmpty(recordResultData)) {
                log.info("checkDataSizeBeforeDone() get last data: recordResultData={}", recordResultData);
                DataSizeCheckResult dataSizeCheckResultLast = ObjectMapperUtils.fromJSON(
                        recordResultData, DataSizeCheckResult.class);
                if (dataSizeCheckResultLast != null) {
                    Map<String, Long> tableName2DataCountMapLast = dataSizeCheckResultLast.getTableName2DataCountMap();
                    Map<String, Long> tableName2DataFileSizeMapLast = dataSizeCheckResultLast.getTableName2DataFileSizeMap();

                    AtomicReference<Boolean> checkDataSize = new AtomicReference<>(true);

                    for (String tableName : adBaseBenchmarkDataSizeCheckTableName.get()) {
                        Long dataCount = tableName2DataCountMap.get(tableName) == null ? 0L : tableName2DataCountMap.get(tableName);
                        Long dataFileSize = tableName2DataFileSizeMap.get(tableName) == null ? 0L
                                : tableName2DataFileSizeMap.get(tableName);

                        Long dataCountLast = tableName2DataCountMapLast.get(tableName) == null ? 0L
                                : tableName2DataCountMapLast.get(tableName);
                        Long dataFileSizeLast = tableName2DataFileSizeMapLast.get(tableName) == null ? 0L
                                : tableName2DataFileSizeMapLast.get(tableName);

                        log.info("checkDataSizeBeforeDone() tableName={}, dataCount={}, dataFileSize={}, "
                                        + "dataCountLast={}, dataFileSizeLast={}",
                                tableName, dataCount, dataFileSize, dataCountLast, dataFileSizeLast);
                        // 对比数据条数
                        if (dataCount < dataCountLast) {
                            if (checkDeltaPercent(dataCount, dataCountLast)) {
                                checkDataSize.set(false);
                                perf(AD_DAS_NAMESPACE_NEW, Ad_DAS_BENCHMARK,
                                        clientTaskModel.getClientName(),
                                        platformServiceConfig.getServiceEnv(),
                                        clientTaskModel.getClientName()
                                                + "-checkDataSize-tableName=" + tableName
                                                + ",dataCount=" + dataCount + "-dataCountLast=" + dataCountLast,
                                        "error")
                                        .count(1).logstash();
                                continue;
                            }
                        }

                        // 后对比 数据文件大小
                        if (dataFileSize < dataFileSizeLast) {
                            if (checkDeltaPercent(dataFileSize, dataFileSizeLast)) {
                                perf(AD_DAS_NAMESPACE_NEW, Ad_DAS_BENCHMARK,
                                        clientTaskModel.getClientName(),
                                        platformServiceConfig.getServiceEnv(),
                                        clientTaskModel.getClientName()
                                                + "-checkDataSize-tableName=" + tableName
                                                + ",dataFileSize=" + dataFileSize + "-dataFileSizeLast=" + dataFileSizeLast,
                                        "error")
                                        .count(1).logstash();
                                checkDataSize.set(false);
                            }
                        }
                    }

                    if (!checkDataSize.get()) {
                        // 打点报警
                        perf(AD_DAS_NAMESPACE_NEW, "", clientTaskModel.getClientName(), platformServiceConfig.getServiceEnv(),
                                String.format("master=" + clientTaskModel.getClientName() + "-本次存在导出表数据数量较上次下降百分之%s",
                                        adBaseBenchmarkMasterCheckDeltaPercent.get() * 100),
                                "error")
                                .count(1).logstash();
                        return false;
                    }
                } else {
                    log.error("checkDataSizeBeforeDone() dataSizeCheckResultLast is null!");
                }
            }

            DataSizeCheckResult dataSizeCheckResultNow = new DataSizeCheckResult(clientTaskModel.getTimestamp(), clientTaskModel.getClientName());
            dataSizeCheckResultNow.setTableName2DataCountMap(tableName2DataCountMap);
            dataSizeCheckResultNow.setTableName2DataFileSizeMap(tableName2DataFileSizeMap);

            // zk 覆盖写入新的记录信息
            String dataSizeCheckResultNowStr = ObjectMapperUtils.toJSON(dataSizeCheckResultNow);
            log.info("checkDataSizeBeforeDone() update new data: dataSizeCheckResultNowStr={}", dataSizeCheckResultNowStr);
            clientCommonZkHelper.recordDumpData(clientTaskModel, dataSizeCheckResultNowStr);

            log.info("checkDataSizeBeforDone() end !!!");
            return true;
        } catch (Exception e) {
            log.error("【NOTICE】checkDataSizeBeforeDone() ocurr error: timeStamp={}, masterType={}",
                    clientTaskModel.getTimestamp(), clientTaskModel.getClientName(), e);
            perf(AD_DAS_NAMESPACE_NEW, Ad_DAS_BENCHMARK, clientTaskModel.getClientName(),
                    platformServiceConfig.getServiceEnv(),
                    clientTaskModel.getClientName() + "-checkDataSizeBeforeDone" + "-" + e.getMessage(), "error")
                    .count(1).logstash();

            return false;
        }
    }

    private void perfTableDataSize(Map<String, Long> tableName2DataCountMap, AdDasPlatformClientTaskBatchModel clientTaskModel) {
        try {
            tableName2DataCountMap.forEach((key, value) -> {
                if (!StringUtils.isEmpty(key) && value != null && value >= 0) {
                    perf(AD_DAS_NAMESPACE_NEW, "", clientTaskModel.getClientName(), platformServiceConfig.getServiceEnv(),
                            key, "success").count(value).logstash();
                }
            });
        } catch (Exception e) {
            log.error("【NOTICE】checkDataSizeBeforDone() ocurr error: masterType={}", clientTaskModel.getClientName());
            perf(AD_DAS_NAMESPACE_NEW, Ad_DAS_BENCHMARK, clientTaskModel.getClientName(), platformServiceConfig.getServiceEnv(),
                    clientTaskModel.getClientName() + "-perfTableDataSize" + "-" + e.getMessage(),
                    "error")
                    .count(1).logstash();

        }
    }

    public void checkDump(AdDasPlatformClientTaskBatchModel clientTaskModel) {

        boolean flag = false;
        try {

            String dumpDoneFilePath = clientTaskModel.getHdfsPath() + timeToString(clientTaskModel.getTimestamp()) + "/dump_done";
            String dumpInfoFilePath = clientTaskModel.getHdfsPath() + timeToString(clientTaskModel.getTimestamp()) + "/dump_info";

            log.info("checkDump() clientTaskModel={}", clientTaskModel);
            log.info("checkDump() dumpDoneFilePath={}, dumpInfoFilePath={}", dumpDoneFilePath, dumpInfoFilePath);
            if (HdfsUtils.existDir(dumpDoneFilePath) && HdfsUtils.existDir(dumpInfoFilePath)) {
                flag = true;
            }
        } catch (Exception e) {
            log.error("checkDump() ocurr error!", e);
        }

        // 检查目录下的dump_done 和dump_info是否写成功
        if (!flag) {
            throw  new RuntimeException("checkDump dump_done or dump_info not exist!!!");
        }
    }

    public void checkDataFileSize(AdDasPlatformClientTaskBatchModel clientTaskModel) {
        String dataFilePath = clientTaskModel.getHdfsPath() + timeToString(clientTaskModel.getTimestamp());

        try {
            log.info("checkDataFileSize() dataFilePath={}", dataFilePath);
            Map<String, Long> dataFileSizeMap = HdfsUtils.listPathAndSizeMap(dataFilePath);
            dataFileSizeMap.forEach((key, value) -> {
                if (!org.springframework.util.StringUtils.isEmpty(key) && value != null && value >= 0) {
                    perf(AD_DAS_NAMESPACE_NEW, "",
                            clientTaskModel.getClientName(),
                            platformServiceConfig.getServiceEnv(),
                            key)
                            .count(value).logstash();
                } else {
                    log.error("【NOTICE】checkDataFileSize() ocurr error: key={}, value={}",  key, value);
                    perf(AD_DAS_NAMESPACE_NEW, Ad_DAS_BENCHMARK,
                            clientTaskModel.getClientName(),
                            platformServiceConfig.getServiceEnv(),
                            clientTaskModel.getClientName() + "-checkDataFileSize" + "-file=" + key + "size error",
                            "error")
                            .count(1).logstash();
                }
            });
            log.info("checkDataFileSize() dataFilePath={} success!", dataFilePath);
        } catch (Exception e) {
            log.error("【NOTICE】checkDataFileSize() ocurr error: timeStamp={}, masterType={}",
                    clientTaskModel.getTimestamp(), clientTaskModel.getClientName());
            perf(AD_DAS_NAMESPACE_NEW, Ad_DAS_BENCHMARK,
                    clientTaskModel.getClientName(),
                    platformServiceConfig.getServiceEnv(),
                    clientTaskModel.getClientName() + "-checkDataFileSize" + "-" + e.getMessage(),
                    "error")
                    .count(1).logstash();
        }
    }

    /**
     * 校验 数量下降 百分比 暂定 下降超过 20% 触发报警
     * @return
     */
    private boolean checkDeltaPercent(Long dataNum, Long dataNumLast) {

        Long dataDelta = dataNumLast - dataNum;
        Long dataDeltaPercent = Double.valueOf(dataNumLast * adBaseBenchmarkMasterCheckDeltaPercent.get())
                .longValue();
        log.info("dataDelta={}, dataDeltaPercent={}", dataDelta, dataDeltaPercent);

        if (dataDelta > dataDeltaPercent) {
            return true;
        }

        return false;
    }

}
