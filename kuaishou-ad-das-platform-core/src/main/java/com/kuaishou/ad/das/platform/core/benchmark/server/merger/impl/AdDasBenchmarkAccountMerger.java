package com.kuaishou.ad.das.platform.core.benchmark.server.merger.impl;

import static com.kuaishou.ad.das.platform.core.common.AdDasBenchmarkFactory.registerMerger;

import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Service;


import com.kuaishou.ad.das.platform.core.benchmark.server.merger.AdDasBenchmarkMerger;
import com.kuaishou.ad.das.platform.core.benchmark.model.AdDasTaskHandleModel;
import com.kuaishou.ad.das.platform.core.benchmark.model.AdDasTaskSchemaModel;

import lombok.extern.slf4j.Slf4j;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-02-15
 */
@Slf4j
@Service
public class AdDasBenchmarkAccountMerger implements AdDasBenchmarkMerger {

    @Override
    public List<Map<String, Object>> queryMergerData(AdDasTaskSchemaModel adDasTaskSchemaModel, AdDasTaskHandleModel taskHandleModel) {
        return null;
    }


    @PostConstruct
    private void init() {
        registerMerger("ad_dsp_account", this);
    }
}
