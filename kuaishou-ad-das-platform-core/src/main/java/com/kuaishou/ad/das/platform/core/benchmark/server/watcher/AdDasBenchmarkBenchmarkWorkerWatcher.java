package com.kuaishou.ad.das.platform.core.benchmark.server.watcher;

import static com.google.common.util.concurrent.MoreExecutors.shutdownAndAwaitTermination;
import static com.kuaishou.ad.das.platform.core.utils.AdDasBaseMultiEnvUtils.getDasBenchmarkCommonPath;
import static com.kuaishou.ad.das.platform.utils.constant.AdDasCommonConstant.Ad_DAS_BENCHMARK;
import static com.kuaishou.ad.das.platform.utils.dasenum.AdDasBenchmarkStatusEnum.RUNTIME_ERROR;
import static com.kuaishou.ad.das.platform.utils.kconf.AdDasBaseCommonKconf.AD_DAS_BASE_RUNNER_THREAD;
import static com.kuaishou.ad.das.platform.utils.kconf.AdDasBaseCommonKconf.adBetaBenchmarkSlaveRunStopSwitch;
import static com.kuaishou.ad.das.platform.utils.constant.AdDasCommonConstant.AD_DAS_NAMESPACE_NEW;
import static com.kuaishou.framework.concurrent.DynamicThreadExecutor.dynamic;
import static com.kuaishou.framework.util.PerfUtils.perf;
import static com.kuaishou.infra.framework.common.util.TermHelper.addTerm;
import static java.util.concurrent.TimeUnit.MINUTES;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.apache.commons.lang3.StringUtils;
import org.apache.curator.framework.recipes.cache.TreeCache;
import org.apache.curator.framework.recipes.cache.TreeCacheEvent;
import org.apache.curator.framework.recipes.cache.TreeCacheListener;
import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.data.Stat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import com.kuaishou.ad.das.platform.core.benchmark.model.AdDasPlatformClientTaskSingleModel;
import com.kuaishou.ad.das.platform.core.benchmark.model.AdDasPlatformTaskDoneModel;
import com.kuaishou.ad.das.platform.utils.dasenum.AdDasPlatformZkMsgEnum;
import com.kuaishou.ad.das.platform.core.benchmark.model.AdDasPlatformZkMsgModel;
import com.kuaishou.ad.das.platform.core.benchmark.server.CommonServerWorkerLifeCycle;
import com.kuaishou.ad.das.platform.core.common.AdDasBenchmarkAbstractZk;
import com.kuaishou.ad.das.platform.core.benchmark.model.AdDasTaskResultModel;
import com.kuaishou.ad.das.platform.core.common.AdDasPlatformServiceConfig;
import com.kuaishou.framework.util.ObjectMapperUtils;

import io.vavr.control.Option;
import lombok.extern.slf4j.Slf4j;

/**
 * Server worker
 *
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-08-09
 */
@Lazy
@Slf4j
@Component
public class AdDasBenchmarkBenchmarkWorkerWatcher extends AdDasBenchmarkAbstractZk {

    @Autowired
    protected CommonServerWorkerLifeCycle commonServerWorkerLifeCycle;

    @Autowired
    protected AdDasPlatformServiceConfig platformServiceConfig;

    /**
     * server worker 实例启动初始化
     *
     * @param event
     */
    @EventListener(value = {ContextRefreshedEvent.class})
    public void init(ContextRefreshedEvent event) {

        if (StringUtils.isEmpty(platformServiceConfig.getBenchmarkEnv())
                || !platformServiceConfig.isWorker()) {
           log.error("Please check Worker configuration! benchmarkEnv={} must be configured!",
                   platformServiceConfig.getBenchmarkEnv());
            return;
        }

        synchronized (AdDasBenchmarkBenchmarkWorkerWatcher.class) {

            if (isStarted.equals(0)) {
                try {

                    initBenchmarkServerWorker();
                    log.info("Server node init() sucess ! myName={}", myName);

                    adBaseRunnerExecutor = dynamic(AD_DAS_BASE_RUNNER_THREAD::get,
                            "ad-base-runner-pool");
                    addTerm(() -> shutdownAndAwaitTermination(adBaseRunnerExecutor, 20, MINUTES));
                } catch (Exception e) {
                    log.error("init zookeeper error", e);
                }

                isStarted++;
            } else {
                log.info("只能初始化一次");
                return;
            }
        }
    }

    /**
     * 初始化 server woker实例
     * @throws Exception
     */
    public void initBenchmarkServerWorker() throws Exception {

        // 初始化 zk 链接
        this.initiZkConnection();

        // 检查Server父节点是否存在如果不存在就创建
        String serverPath = PLATFORM_BENCHMARK + getDasBenchmarkCommonPath(platformServiceConfig.getBenchmarkEnv()) + SERVERS + WORKER;
        Stat statServer = curatorFramework.checkExists()
                .forPath(serverPath);
        if (statServer == null) {
            curatorFramework.create().creatingParentsIfNeeded().withMode(CreateMode.PERSISTENT)
                    .forPath(serverPath);
        }

        String containerIp = System.getenv(CONTAINERIP);
        String containerId = System.getenv(CONTAINERID);
        String clientValue = Option.of(containerIp).getOrElse("") + "-"
                + Option.of(containerId).getOrElse("");
        // 为本实例创建临时节点
        myServerNodePath = curatorFramework.create().withMode(CreateMode.EPHEMERAL_SEQUENTIAL)
                .forPath(PLATFORM_BENCHMARK + getDasBenchmarkCommonPath(platformServiceConfig.getBenchmarkEnv()) + SERVERS + WORKER + "/1", clientValue.getBytes());
        myName = myServerNodePath.replace(PLATFORM_BENCHMARK + getDasBenchmarkCommonPath(platformServiceConfig.getBenchmarkEnv()) + SERVERS + WORKER +  "/", "");

        String taskPath = PLATFORM_BENCHMARK + getDasBenchmarkCommonPath(platformServiceConfig.getBenchmarkEnv()) + TASKS;
        taskTreeCache = new TreeCache(curatorFramework, taskPath);
        log.info("myName={}, taskPath={}", myName, taskPath);
        serverWorkers = curatorFramework.getChildren().forPath(PLATFORM_BENCHMARK + getDasBenchmarkCommonPath(platformServiceConfig.getBenchmarkEnv()) + SERVERS + WORKER);
        Collections.sort(serverWorkers);

        // worker 节点统一监听 Task 节点的变更
        TreeCacheListener resourceChildrenCacheListener = (client, event1) -> {
            TreeCacheEvent.Type eventType = event1.getType();
            if (eventType == TreeCacheEvent.Type.NODE_ADDED || eventType == TreeCacheEvent.Type.NODE_UPDATED) {
                // 如果有新增&更新且token为空，说明有任务分配。按照伙伴算法确认要消费的资源（任务）后往特定的任务下面写下自己的名字
                String eventData = new String(event1.getData().getData());
                // token被清空，说明是新分配的任务
                if (event1.getData().getPath().endsWith(ASSIGN)
                        && event1.getData().getPath().contains(getDasBenchmarkCommonPath(platformServiceConfig.getBenchmarkEnv()))
                        && !StringUtils.isEmpty(eventData)) {
                    log.info("myName={}, event1.getData().getPath()={}",
                            myName, event1.getData().getPath());
                    String taskAssignName = new String(curatorFramework.getData()
                            .forPath(event1.getData().getPath()));
                    if (myName.equals(taskAssignName)) {
                        AdDasPlatformZkMsgModel adDasPlatformZkMsgModel = new AdDasPlatformZkMsgModel();
                        adDasPlatformZkMsgModel.setMsgType(AdDasPlatformZkMsgEnum.WORKER_MASTER_ASSIGN_MSG);
                        adDasPlatformZkMsgModel.setMsgPath(event1.getData().getPath());

                        int index = event1.getData().getPath().indexOf("/assign");
                        String pathPrefix = event1.getData().getPath().substring(0, index);
                        String taskContent = new String(curatorFramework.getData()
                                .forPath(pathPrefix + TASKCONTENT));
                        log.info("event1.getData().getPath()={}, pathPrefix={}, taskContent={}",
                                event1.getData().getPath(), pathPrefix, taskContent);
                        adDasPlatformZkMsgModel.setContentData(taskContent);
                        adDasPlatformZkMsgModel.setMsgTimestamp(System.currentTimeMillis());
                        // 处理任务
                        proccessTask(adDasPlatformZkMsgModel);
                    }
                }
            }
        };
        taskTreeCache.getListenable().addListener(resourceChildrenCacheListener);
        taskTreeCache.start();
    }

    /**
     * 处理任务
     *
     * 1、积攒zk时间消息
     * 2、异步单线程处理
     *
     * @param adDasPlatformZkMsgModel
     */
    protected void proccessTask(AdDasPlatformZkMsgModel adDasPlatformZkMsgModel) {


        // 停止slave 接受任务开关
        if (adBetaBenchmarkSlaveRunStopSwitch.get()) {
            log.info("slave was shutdown!!");
            return;
        }

        if (myName == null) {
            log.error("myNumber is null!!!");
            return;
        }

        bufferTrigger.enqueue(adDasPlatformZkMsgModel);
    }

    /**
     * 检查当前 taskNum 是否可以执行
     * @param taskSingleModel
     * @return
     */
    private boolean runCheckTask(AdDasPlatformClientTaskSingleModel taskSingleModel) {

        boolean getTaskFlag = false;
        synchronized (AdDasBenchmarkBenchmarkWorkerWatcher.class) {
            try {
                Stat tokenStat = curatorFramework.checkExists()
                        .forPath(PLATFORM_BENCHMARK + getDasBenchmarkCommonPath(platformServiceConfig.getBenchmarkEnv()) + TASKS
                                + "/" + taskSingleModel.getClientName() + TASK + "/" + taskSingleModel.getTaskNum().toString() + TOKEN);

                // token不存在，说明无效
                if (tokenStat == null) {
                    getTaskFlag = false;
                }

                String tokenStr = new String(curatorFramework.getData()
                                .forPath(PLATFORM_BENCHMARK + getDasBenchmarkCommonPath(platformServiceConfig.getBenchmarkEnv()) + TASKS
                                        + "/" + taskSingleModel.getClientName() + TASK + "/" + taskSingleModel.getTaskNum().toString() + TOKEN));

                // 不重复消费，但别的节点写入的token可以覆盖
                if (!StringUtils.isEmpty(tokenStr) && myName.equals(tokenStr)) {
                    getTaskFlag = false;
                } else {
                    // 写入自己的名字，开始消费
                    curatorFramework.setData()
                            .forPath(PLATFORM_BENCHMARK + getDasBenchmarkCommonPath(platformServiceConfig.getBenchmarkEnv()) + TASKS
                                            + "/" + taskSingleModel.getClientName() + TASK + "/" + taskSingleModel.getTaskNum().toString()  + TOKEN, myName.getBytes());
                    log.info("myName={} get my task! taskSingleModel={}", myName, taskSingleModel);
                    getTaskFlag = true;
                }

                String doneContent = new String(curatorFramework.getData()
                        .forPath(PLATFORM_BENCHMARK + getDasBenchmarkCommonPath(platformServiceConfig.getBenchmarkEnv()) + TASKS
                                + "/" + taskSingleModel.getClientName() + TASK + "/" + taskSingleModel.getTaskNum().toString()  + DONE));
                // 存在正确执行的结果， 则不重复执行
                if (StringUtils.isNotEmpty(doneContent) && checkContent(doneContent)) {
                    getTaskFlag = false;
                }
            } catch (Exception e) {
                log.error("slaveRecieveTask() zk error", e);
            }
        }

        return getTaskFlag;
    }

    /**
     * 触发异步执行任务
     * @param taskSingleModel
     */
    private void runAsync(AdDasPlatformClientTaskSingleModel taskSingleModel) {
        CompletableFuture.runAsync(() -> {
            try {

                List<AdDasTaskResultModel> taskResultModels = commonServerWorkerLifeCycle.lifeCycle(myName, taskSingleModel);
                log.info("runAsync() taskResultModels={}", taskResultModels);
                AdDasPlatformTaskDoneModel dasPlatformTaskDoneModel = convertResultWrapper(taskSingleModel, taskResultModels);
                // step 3 写入DONE done()
                done(taskSingleModel, dasPlatformTaskDoneModel);

            } catch (Exception e) {
                // step 3 写入DONE done()
                AdDasPlatformTaskDoneModel dasPlatformTaskDoneModel = new AdDasPlatformTaskDoneModel(taskSingleModel, myName, RUNTIME_ERROR);

                done(taskSingleModel, dasPlatformTaskDoneModel);
                log.error("【AdBaseRunner】dispatcher() ocurr error: taskSingleModel={}", taskSingleModel, e);
            }
        }, adBaseRunnerExecutor);
    }

    private AdDasPlatformTaskDoneModel convertResultWrapper(AdDasPlatformClientTaskSingleModel taskSingleModel, List<AdDasTaskResultModel> taskResultModels) {

        AdDasPlatformTaskDoneModel dasPlatformTaskDoneModel = new AdDasPlatformTaskDoneModel();
        dasPlatformTaskDoneModel.setClientName(taskSingleModel.getClientName());
        dasPlatformTaskDoneModel.setHdfsPath(taskSingleModel.getHdfsPath());
        dasPlatformTaskDoneModel.setTimestamp(taskSingleModel.getTimestamp());
        dasPlatformTaskDoneModel.setTaskNum(taskSingleModel.getTaskNum());
        dasPlatformTaskDoneModel.setAssignName(myName);

        if (!CollectionUtils.isEmpty(taskResultModels)) {
            dasPlatformTaskDoneModel.setStatus(taskResultModels.get(0).getStatus());
            dasPlatformTaskDoneModel.setReason(taskResultModels.get(0).getReason());
            dasPlatformTaskDoneModel.setProtoName(taskResultModels.get(0).getProtoName());
            dasPlatformTaskDoneModel.setTableStr(taskResultModels.get(0).getTableStr());
            dasPlatformTaskDoneModel.setResultList(taskResultModels.get(0).getResultList());
        }

        return dasPlatformTaskDoneModel;
    }

    /**
     * slave 跑完任务更新状态
     */
    public Boolean done(AdDasPlatformClientTaskSingleModel taskSingleModel, AdDasPlatformTaskDoneModel dasPlatformTaskDoneModel) {
        try {

            String doneStr = ObjectMapperUtils.toJSON(dasPlatformTaskDoneModel);
            // 更新Done 表示做完了
            curatorFramework.setData()
                    .forPath(PLATFORM_BENCHMARK + getDasBenchmarkCommonPath(platformServiceConfig.getBenchmarkEnv()) + TASKS
                                    + "/" + taskSingleModel.getClientName() + TASK + "/" + taskSingleModel.getTaskNum().toString()  + DONE,
                            doneStr.getBytes());

            return true;
        } catch (Exception e) {
            log.error("zk error", e);
        }
        return false;
    }

    /**
     * 单线程同步处理
     *
     * @param adDasPlatformZkMsgModels
     */
    @Override
    protected void consume(ConcurrentLinkedQueue<AdDasPlatformZkMsgModel> adDasPlatformZkMsgModels) {

        adDasPlatformZkMsgModels.stream()
                .forEach(adDasPlatformZkMsgModel -> {
                    try {
                        // 只处理 worker master 指定的任务
                        if (AdDasPlatformZkMsgEnum.WORKER_MASTER_ASSIGN_MSG == adDasPlatformZkMsgModel.getMsgType()) {

                            String contentData =  adDasPlatformZkMsgModel.getContentData();
                            AdDasPlatformClientTaskSingleModel  taskSingleModel = ObjectMapperUtils
                                    .fromJSON(contentData, AdDasPlatformClientTaskSingleModel.class);
                            log.info("taskSingleModel={}", taskSingleModel);
                            runTask(taskSingleModel);
                        } else {
                            log.info("unKnown msgType! adDasPlatformZkMsgModel={}", adDasPlatformZkMsgModel);
                        }
                    } catch (Exception e) {
                        log.error("【AdBaseRunner】assign() ocurr error: myNumber={}", myName, e);
                    }
        });
    }

    /**
     * 拿到分配的任务写入TOKEN开始消费，，消费完成后写DONE
     */
    public void runTask(AdDasPlatformClientTaskSingleModel taskSingleModel) {

        try {
            //  step2 开始消费任务 HDFS的时间戳从taskNum后面的timestemp获取
            String taskContent = new String(curatorFramework.getData()
                    .forPath(PLATFORM_BENCHMARK + getDasBenchmarkCommonPath(platformServiceConfig.getBenchmarkEnv()) + TASKS
                            + "/" + taskSingleModel.getClientName() + TASK + "/" + taskSingleModel.getTaskNum().toString() + TASKCONTENT));

            // 非master引起的不执行任务
            if (StringUtils.isEmpty(taskContent)) {
                return;
            }

            // 检查当前 taskNum 是否可以执行
            if (runCheckTask(taskSingleModel)) {
                // 异步执行
                runAsync(taskSingleModel);
            }
        } catch (Exception e) {
            // 写入DONE done()
            AdDasPlatformTaskDoneModel dasPlatformTaskDoneModel = new AdDasPlatformTaskDoneModel(taskSingleModel, myName, RUNTIME_ERROR);
            done(taskSingleModel, dasPlatformTaskDoneModel);
            log.error("【AdBaseRunner】dispatcher() ocurr error: taskSingleModel={}", taskSingleModel, e);
        }
    }


}