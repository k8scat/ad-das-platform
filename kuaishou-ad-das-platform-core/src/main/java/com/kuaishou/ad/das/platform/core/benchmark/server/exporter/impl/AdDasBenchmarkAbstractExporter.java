package com.kuaishou.ad.das.platform.core.benchmark.server.exporter.impl;

import static com.kuaishou.ad.das.platform.core.utils.AdBaseUtils.buildFileName;
import static com.kuaishou.ad.das.platform.core.utils.AdBaseUtils.timeToString;
import static com.kuaishou.ad.das.platform.utils.constant.AdDasCommonConstant.Ad_DAS_BENCHMARK;
import static com.kuaishou.ad.das.platform.utils.kconf.AdDasBaseCommonKconf.adBaseBenchmarkWriteHdfsTimeout;
import static com.kuaishou.ad.das.platform.utils.constant.AdDasCommonConstant.AD_DAS_NAMESPACE_NEW;
import static com.kuaishou.framework.util.PerfUtils.perf;
import static java.util.concurrent.TimeUnit.MINUTES;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicReference;

import org.apache.commons.beanutils.BeanUtils;
import org.springframework.util.CollectionUtils;

import com.ecyrd.speed4j.StopWatch;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.kuaishou.ad.das.platform.core.benchmark.model.AdDasPlatformBenchmarkResultModel;
import com.kuaishou.ad.das.platform.core.benchmark.model.AdDasTaskHandleModel;
import com.kuaishou.ad.das.platform.core.benchmark.model.AdDasTaskResultModel;
import com.kuaishou.ad.das.platform.core.benchmark.model.AdDasTaskSchemaModel;
import com.kuaishou.ad.das.platform.core.benchmark.model.DoneTableResult;
import com.kuaishou.ad.das.platform.utils.HdfsUtils;
import com.kuaishou.ad.das.platform.utils.dasenum.AdDasBenchmarkStatusEnum;
import com.kuaishou.framework.concurrent.DynamicThreadExecutor;
import com.kuaishou.framework.util.PerfUtils;

import lombok.extern.slf4j.Slf4j;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-01-30
 */
@Slf4j
public abstract class AdDasBenchmarkAbstractExporter {

    protected static DynamicThreadExecutor commonBenchmarkExporterExecutor;

    protected AdDasTaskResultModel defaultexport2Hdfs(AdDasTaskSchemaModel adDasTaskSchemaModel,
                                                      List<AdDasTaskHandleModel> adDasTaskHandleModels) {

        if (CollectionUtils.isEmpty(adDasTaskHandleModels)) {
            return null;
        }

        log.info("defaultexport2Hdfs() adDasTaskSchemaModel={}, adDasTaskHandleModels={}",
                adDasTaskSchemaModel, adDasTaskHandleModels);

        StopWatch watcher = PerfUtils.getWatcher();
        AdDasTaskResultModel result = new AdDasTaskResultModel();
        result.setTaskNum(adDasTaskSchemaModel.getTaskNum());
        result.setTimestamp(adDasTaskSchemaModel.getTimestamp());
        result.setTableStr(adDasTaskSchemaModel.getMainTable());
        result.setProtoName(adDasTaskSchemaModel.getPbClassFullName());
        log.info("defaultexport2Hdfs() result={}", result);

        try {

            List<DoneTableResult> resultList = Collections.synchronizedList(Lists.newArrayList());
            AtomicReference<Boolean> isAllRight = new AtomicReference<>(true);

            Map<Integer, FutureTask<List<DoneTableResult>>> futureTaskHashMap = Maps.newHashMap();
            Map<Integer, List<DoneTableResult>> doneResultMap = new ConcurrentHashMap<>();
            Map<Integer, FutureTask<List<DoneTableResult>>> futureTaskHashRetryMap = Maps.newHashMap();

            adDasTaskHandleModels.stream()
                    .filter(adDasTaskHandleModel -> adDasTaskHandleModel != null && adDasTaskHandleModel.getDtoResult() != null)
                    .forEach(adDasTaskHandleModel -> {
                        FutureTask<List<DoneTableResult>> futureTask = new FutureTask<List<DoneTableResult>>(() -> {
                            List<DoneTableResult> doneTableResults = writeHDFSData(adDasTaskHandleModel);
                            if (CollectionUtils.isEmpty(doneTableResults)) {
                                return null;
                            } else {
                                return doneTableResults;
                            }
                        });
                        // 提交
                        commonBenchmarkExporterExecutor.submit(futureTask);
                        futureTaskHashMap.put(adDasTaskHandleModels.indexOf(adDasTaskHandleModel), futureTask);
                    });

            // 第一次超时检查
            long curTimestamp = System.currentTimeMillis();
            AtomicReference<Boolean> checkTaskOne = new AtomicReference<>(true);
            while (checkTaskOne.get() && doneResultMap.size() < adDasTaskHandleModels.size()) {
                futureTaskHashMap.forEach((index, futureTask) -> {
                    try {
                        if (!doneResultMap.containsKey(index)) {
                            if (futureTask.isDone()) {
                                List<DoneTableResult> doneResults = futureTask.get(1, MINUTES);
                                if (!CollectionUtils.isEmpty(doneResults)) {
                                    doneResultMap.put(index, doneResults);
                                    return;
                                } else {
                                    // 返回空则跳出
                                    checkTaskOne.set(false);
                                }
                            } else {
                                long timeCost = (System.currentTimeMillis() - curTimestamp) / 1000;
                                if (timeCost >= adBaseBenchmarkWriteHdfsTimeout.get()) {
                                    futureTask.cancel(true);
                                    // 有超时则跳出
                                    checkTaskOne.set(false);
                                    Thread.sleep(10000); // 10s后进行
                                    // 超时重试一次
                                    log.info("【Notice】writeHDFS() retry index={}, resultWrapper={}", index, adDasTaskHandleModels.get(index));
                                    FutureTask<List<DoneTableResult>> futureTaskRetry = new FutureTask<List<DoneTableResult>>(() -> {
                                        List<DoneTableResult> doneTableResults = writeHDFSData(adDasTaskHandleModels.get(index));
                                        if (CollectionUtils.isEmpty(doneTableResults)) {
                                            return null;
                                        } else {
                                            return doneTableResults;
                                        }
                                    });
                                    // 提交
                                    commonBenchmarkExporterExecutor.submit(futureTaskRetry);
                                    futureTaskHashRetryMap.put(index, futureTaskRetry);
                                }
                            }
                        }
                    } catch (InterruptedException | ExecutionException e) {
                        log.error("writeHDFS() first check occur error: assignmentId={}, timeStamp={}, index={}",
                                adDasTaskSchemaModel.getTaskNum(), adDasTaskSchemaModel.getTimestamp(), index, e);
                        checkTaskOne.set(false);
                    } catch (TimeoutException e) {
                        log.error("writeHDFS() first check occur error: assignmentId={}, timeStamp={}, index={}",
                                adDasTaskSchemaModel.getTaskNum(), adDasTaskSchemaModel.getTimestamp(), index, e);
                        checkTaskOne.set(false);
                        futureTask.cancel(true);
                    }
                });
                // while 循环 sleep 100ms
                Thread.sleep(100);
            }

            // 清理 future
            if (!checkTaskOne.get()) {
                futureTaskHashMap.values().forEach(futureTask -> {
                    futureTask.cancel(true);
                });
            }

            // 第二次重试特定任务
            if (!CollectionUtils.isEmpty(futureTaskHashRetryMap)) {
                log.info("【Notice】writeHDFS() start retry taskIndexs={}", futureTaskHashRetryMap.keySet());
                AtomicReference<Boolean> checkTaskTwo = new AtomicReference<>(true);
                long curTimestampTwo = System.currentTimeMillis();
                while (checkTaskTwo.get() && doneResultMap.size() < adDasTaskHandleModels.size()) {
                    futureTaskHashRetryMap.forEach((index, futureTask) -> {
                        try {
                            if (!doneResultMap.containsKey(index)) {
                                if (futureTask.isDone()) {
                                    List<DoneTableResult> doneResults = futureTask.get(1, MINUTES);
                                    if (!CollectionUtils.isEmpty(doneResults)) {
                                        doneResultMap.put(index, doneResults);
                                        return;
                                    } else {
                                        checkTaskTwo.set(false);
                                        isAllRight.set(false);
                                    }
                                } else {
                                    long timeCost = (System.currentTimeMillis() - curTimestampTwo) / 1000;
                                    if (timeCost >= adBaseBenchmarkWriteHdfsTimeout.get()) {
                                        futureTask.cancel(true);
                                        // 有超时则跳出
                                        checkTaskTwo.set(false);
                                    }
                                }
                            }
                        } catch (InterruptedException | ExecutionException e) {
                            log.error("writeHDFS() check retry occur error: assignmentId={}, timeStamp={}, index={}",
                                    adDasTaskSchemaModel.getTaskNum(), adDasTaskSchemaModel.getTimestamp(), index, e);
                            checkTaskTwo.set(false);
                            isAllRight.set(false);
                        } catch (TimeoutException e) {
                            log.error("writeHDFS() check retry occur error: assignmentId={}, timeStamp={}, index={}",
                                    adDasTaskSchemaModel.getTaskNum(), adDasTaskSchemaModel.getTimestamp(), index, e);
                            checkTaskTwo.set(false);
                            isAllRight.set(false);
                            futureTask.cancel(true);
                        }
                    });
                    // while 循环 sleep 100ms
                    Thread.sleep(100);
                }
                // 清理 future
                if (!checkTaskTwo.get()) {
                    futureTaskHashRetryMap.values().forEach(futureTask -> {
                        futureTask.cancel(true);
                    });
                }
            }

            // 合并最后结果
            if (doneResultMap.size() == adDasTaskHandleModels.size()) {
                doneResultMap.forEach((key, value) -> {
                    log.info("writeDataToHDFS() index={}, value={}", key, value);
                    if (!CollectionUtils.isEmpty(value)) {
                        resultList.addAll(value);
                    } else {
                        isAllRight.set(false);
                    }
                });
            } else {
                result.setStatusAndReason(AdDasBenchmarkStatusEnum.WRITE_DATA_2_HDFS_ERROR);
            }

            if (!CollectionUtils.isEmpty(resultList)) {
                result.setResultList(resultList);
            }

            if (!isAllRight.get()) {
                result.setStatusAndReason(AdDasBenchmarkStatusEnum.WRITE_DATA_2_HDFS_ERROR);
            }

            adDasTaskHandleModels.clear();

        } catch (Exception e) {
            log.error("defaultexport2Hdfs() ocurr error! adDasTaskSchemaModel={}", adDasTaskSchemaModel, e);
        }

        return result;
    }

    protected List<DoneTableResult> writeHDFSData(AdDasTaskHandleModel adDasTaskHandleModel) {

        List<DoneTableResult> resultList = Lists.newArrayList();
        String fileName = buildFileName(adDasTaskHandleModel.getTaskName(),
                adDasTaskHandleModel.getTaskNum(), adDasTaskHandleModel.getShardId());
        try {
            AdDasPlatformBenchmarkResultModel dtoResult = adDasTaskHandleModel.getDtoResult();
            log.info("start writeHDFSData ! adDasTaskHandleModel={}", adDasTaskHandleModel);
            if (dtoResult.getRecordNum() < 100) {
                Thread.sleep(1000);
            }

            // 分
            List<byte[]> recordResults = dtoResult.getRecordResults();
            checkDataLists(recordResults);

            boolean writeHDFSFlag = HdfsUtils.writeDatasWithRetry(recordResults, adDasTaskHandleModel.getHdfsPath(),
                    timeToString(adDasTaskHandleModel.getTimestamp()), fileName);

            log.info("writeHDFSData ! fileName={}, writeHDFSFlag={}", fileName, writeHDFSFlag);

            if (writeHDFSFlag) {
                dtoResult.setRecordResults(null);
                DoneTableResult tableResult = new DoneTableResult();
                BeanUtils.copyProperties(tableResult, adDasTaskHandleModel);
                tableResult.setAssignmentId(adDasTaskHandleModel.getTaskNum());
                tableResult.setTableStr(adDasTaskHandleModel.getTableName());
                tableResult.setShardId(adDasTaskHandleModel.getShardId());
                tableResult.setFileName(fileName);
                tableResult.setRecordNum(dtoResult.getRecordNum());
                log.info("writeDataToHDFS() tableResult={}", tableResult);
                perf(AD_DAS_NAMESPACE_NEW, "",
                        "res" + adDasTaskHandleModel.getTaskNum(), fileName, "", "")
                        .count(dtoResult.getRecordNum()).logstash();
                resultList.add(tableResult);
            } else {
                throw new RuntimeException("writeDataToHDFS() failed!");
            }
        } catch (Exception e) {
            log.error("writeHDFS() occur error: assignmentId={}, timeStamp={}",
                    adDasTaskHandleModel.getTaskNum(), adDasTaskHandleModel.getTimestamp(), e);

            perf(AD_DAS_NAMESPACE_NEW, Ad_DAS_BENCHMARK,
                    "slave", "", fileName, "")
                    .count(1).logstash();
            return Lists.newArrayList();
        }

        return resultList;
    }

    public void checkDataLists(List<byte[]> recordResults) {
        if (CollectionUtils.isEmpty(recordResults)) {
            throw new RuntimeException("checkDataLists() recordResults is empty!");
        }

        for (byte[] data : recordResults) {
            if (data == null) {
                throw new RuntimeException("checkDataLists() recordResults has null data");
            }
        }
    }

}
