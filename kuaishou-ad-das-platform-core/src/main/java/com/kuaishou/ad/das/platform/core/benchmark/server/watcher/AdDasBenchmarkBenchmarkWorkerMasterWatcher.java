package com.kuaishou.ad.das.platform.core.benchmark.server.watcher;

import static com.kuaishou.ad.das.platform.core.utils.AdDasBaseMultiEnvUtils.getDasBenchmarkCommonPath;
import static com.kuaishou.ad.das.platform.utils.kconf.AdDasBaseCommonKconf.adBetaBenchmarkDebugLogSwitch;
import static com.kuaishou.ad.das.platform.utils.kconf.AdDasPlatformKconf.adDasPlatformWorkerStatusConfig;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.stream.Collectors;

import org.apache.commons.compress.utils.Lists;
import org.apache.commons.lang3.StringUtils;
import org.apache.curator.framework.recipes.cache.PathChildrenCache;
import org.apache.curator.framework.recipes.cache.PathChildrenCacheEvent;
import org.apache.curator.framework.recipes.cache.PathChildrenCacheListener;
import org.apache.curator.framework.recipes.cache.TreeCache;
import org.apache.curator.framework.recipes.cache.TreeCacheEvent;
import org.apache.curator.framework.recipes.cache.TreeCacheListener;
import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.data.Stat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import com.google.common.collect.Maps;
import com.kuaishou.ad.das.platform.core.benchmark.model.AdDasPlatformClientTaskBatchModel;
import com.kuaishou.ad.das.platform.core.benchmark.model.AdDasPlatformClientTaskSingleModel;
import com.kuaishou.ad.das.platform.core.benchmark.model.AdDasPlatformTaskDoneModel;
import com.kuaishou.ad.das.platform.core.benchmark.model.AdDasPlatformWorkMasterZkModel;
import com.kuaishou.ad.das.platform.core.benchmark.model.AdDasPlatformWorkerRebalanceModel;
import com.kuaishou.ad.das.platform.core.benchmark.model.AdDasPlatformWorkerTaskWeightModel;
import com.kuaishou.ad.das.platform.utils.dasenum.AdDasPlatformZkMsgEnum;
import com.kuaishou.ad.das.platform.core.benchmark.model.AdDasPlatformZkMsgModel;
import com.kuaishou.ad.das.platform.core.common.AdDasBenchmarkAbstractZk;
import com.kuaishou.ad.das.platform.core.common.AdDasPlatformServiceConfig;
import com.kuaishou.ad.das.platform.utils.dasenum.AdDasBenchmarkClientPublishType;
import com.kuaishou.ad.das.platform.utils.dasenum.AdDasBenchmarkWorkerStatus;
import com.kuaishou.framework.util.ObjectMapperUtils;

import lombok.extern.slf4j.Slf4j;

/**
 * Server worker_master
 *
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-08-09
 */
@Lazy
@Slf4j
@Component
public class AdDasBenchmarkBenchmarkWorkerMasterWatcher extends AdDasBenchmarkAbstractZk {

    /**
     * worker 对应执行任务 本地记录
     */
    protected Map<String, List<AdDasPlatformClientTaskSingleModel>> worker2TaskModelsMap = Maps.newConcurrentMap();

    /**
     * worker 任务权重 列表
     */
    protected List<AdDasPlatformWorkerTaskWeightModel> workerTaskWeightModels = Lists.newArrayList();

    @Autowired
    protected AdDasPlatformServiceConfig platformServiceConfig;

    /**
     * server 实例启动初始化
     *
     * @param
     */
    public void init() {

        if (StringUtils.isEmpty(platformServiceConfig.getBenchmarkEnv())
                || !platformServiceConfig.isWorkerMaster()) {
            log.error("Please check WorkerMaster configuration! benchmarkEnv={} must be configured!",
                    platformServiceConfig.getBenchmarkEnv());
            return;
        }

        synchronized (AdDasBenchmarkBenchmarkWorkerMasterWatcher.class) {

            if (isStarted.equals(0)) {
                try {
                    log.info("starts init()! serviceName={}", platformServiceConfig.getServiceName());
                    initBenchmarkServerWorkerMaster();

                } catch (Exception e) {
                    log.error("init zookeeper error", e);
                }

                isStarted++;
            } else {
                log.info("只能初始化一次");
                return;
            }
        }
    }

    /**
     * 初始化 server实例
     * @throws Exception
     */
    public void initBenchmarkServerWorkerMaster() throws Exception {

        // 初始化 zk 链接
        this.initiZkConnection();

        // 检查Server父节点是否存在如果不存在就创建
        String serverPath = PLATFORM_BENCHMARK + getDasBenchmarkCommonPath(platformServiceConfig.getBenchmarkEnv()) + SERVERS;
        Stat statServer = curatorFramework.checkExists()
                .forPath(serverPath);
        if (statServer == null) {
            curatorFramework.create().creatingParentsIfNeeded().withMode(CreateMode.PERSISTENT)
                    .forPath(serverPath);
        }

        // 检查Server Worker_Master 节点是否存在如果不存在就创建
        String workerMasterPath = PLATFORM_BENCHMARK + getDasBenchmarkCommonPath(platformServiceConfig.getBenchmarkEnv())
                + SERVERS + WORKER_MASTER;
        Stat statServerWorkerMaster = curatorFramework.checkExists()
                .forPath(workerMasterPath);
        if (statServerWorkerMaster == null) {
            curatorFramework.create().creatingParentsIfNeeded().withMode(CreateMode.PERSISTENT)
                    .forPath(workerMasterPath);
        }
        AdDasPlatformWorkMasterZkModel workMasterModel = new AdDasPlatformWorkMasterZkModel();
        workMasterModel.setBenchmarkEnv(platformServiceConfig.getBenchmarkEnv());
        workMasterModel.setServiceEnv(platformServiceConfig.getServiceEnv());
        workMasterModel.setServiceName(platformServiceConfig.getServiceName());

        String workerMasterModelStr = ObjectMapperUtils.toJSON(workMasterModel);
        log.info("workerMasterModelStr={}", workerMasterModelStr);
        curatorFramework.setData().forPath(PLATFORM_BENCHMARK + getDasBenchmarkCommonPath(platformServiceConfig.getBenchmarkEnv()) + SERVERS + WORKER_MASTER,
                workerMasterModelStr.getBytes());

        // 检查worker节点是否存在如果不存在就创建
        String workerPath = PLATFORM_BENCHMARK + getDasBenchmarkCommonPath(platformServiceConfig.getBenchmarkEnv()) + SERVERS + WORKER;
        Stat statServerWorker = curatorFramework.checkExists()
                .forPath(workerPath);
        if (statServerWorker == null) {
            curatorFramework.create().creatingParentsIfNeeded().withMode(CreateMode.PERSISTENT)
                    .forPath(workerPath);
        }

        serverPathChildrenCache = new PathChildrenCache(curatorFramework, workerPath, true);
        // 统一监听Server worker节点的变动
        PathChildrenCacheListener clientChildrenCacheListener = (client, event1) -> {
            PathChildrenCacheEvent.Type eventType = event1.getType();

            //  只监听本集群的 worker 实例变化
            if (!event1.getData().getPath().contains(getDasBenchmarkCommonPath(platformServiceConfig.getBenchmarkEnv()))) {
                log.info("event1.getData().getPath()={}", event1.getData().getPath());
                return;
            }

            if (eventType == PathChildrenCacheEvent.Type.CHILD_ADDED
                    || eventType == PathChildrenCacheEvent.Type.CHILD_REMOVED) {
                // 如果有新增，说明有新的worker加入。
                serverWorkers = curatorFramework.getChildren()
                        .forPath(workerPath);
                Collections.sort(serverWorkers);
                log.info("event1.dataPath()={}, serverWorkers={}", event1.getData().getPath(), serverWorkers);

                // 判断基准集群是否在 运行期， 升级期的变更不触发 rebalance操作
               if (AdDasBenchmarkWorkerStatus.WORKING.getCode() == adDasPlatformWorkerStatusConfig.get()
                       .getOrDefault(platformServiceConfig.getBenchmarkEnv(), AdDasBenchmarkWorkerStatus.WORKING.getCode())) {

                   AdDasPlatformZkMsgModel adDasPlatformZkMsgModel = new AdDasPlatformZkMsgModel();
                   adDasPlatformZkMsgModel.setMsgType(AdDasPlatformZkMsgEnum.WORKER_UPDATED_MSG);
                   String dataPath = event1.getData().getPath();
                   adDasPlatformZkMsgModel.setMsgPath(dataPath);
                   AdDasPlatformWorkerRebalanceModel workerRebalanceModel = new AdDasPlatformWorkerRebalanceModel();
                   workerRebalanceModel.setType(eventType);
                   String[] dataPaths = dataPath.split("/");
                   log.info("dataPaths={}", Arrays.stream(dataPaths).toArray());
                   workerRebalanceModel.setWorkerName(dataPaths[dataPaths.length - 1]);
                   String workerRebalanceModelStr = ObjectMapperUtils.toJSON(workerRebalanceModel);
                   log.info("workerRebalanceModelStr={}", workerRebalanceModelStr);
                   adDasPlatformZkMsgModel.setContentData(workerRebalanceModelStr);
                   adDasPlatformZkMsgModel.setMsgTimestamp(System.currentTimeMillis());
                   // 触发 worker master 管理 worker
                   bufferTrigger.enqueue(adDasPlatformZkMsgModel);
               }
            }
        };

        serverPathChildrenCache.getListenable().addListener(clientChildrenCacheListener);
        serverPathChildrenCache.start(PathChildrenCache.StartMode.NORMAL);

        // 监听 Task 节点下 client触发数据流任务发布
        taskTreeCache = new TreeCache(curatorFramework, PLATFORM_BENCHMARK + getDasBenchmarkCommonPath(platformServiceConfig.getBenchmarkEnv()) + TASKS);
        // worker master节点统一监听Task节点的变动
        TreeCacheListener resourceChildrenCacheListener = (client, event1) -> {
            TreeCacheEvent.Type eventType = event1.getType();

            //  只监听本集群的变化
            if (!event1.getData().getPath().contains(getDasBenchmarkCommonPath(platformServiceConfig.getBenchmarkEnv()))) {
                log.info("event1.getData().getPath()={}", event1.getData().getPath());
                return;
            }

            if (eventType == TreeCacheEvent.Type.NODE_UPDATED) {
                String eventData = new String(event1.getData().getData());
                // clients_publish不被清空，说明是新分配的任务
                if (event1.getData().getPath().endsWith(CLIENTS_PUB)) {

                    String clientPublish = new String(curatorFramework.getData().forPath(event1.getData().getPath()));
                    log.info("event1.dataPath()={}, clientPublish={}", event1.getData().getPath(), clientPublish);

                    AdDasPlatformZkMsgModel adDasPlatformZkMsgModel = new AdDasPlatformZkMsgModel();
                    adDasPlatformZkMsgModel.setMsgType(AdDasPlatformZkMsgEnum.CLIENT_PUBLISH_MSG);
                    adDasPlatformZkMsgModel.setMsgPath(event1.getData().getPath());
                    // FIXME 数据对象 AdDasPlatformClientTaskModel
                    adDasPlatformZkMsgModel.setContentData(clientPublish);
                    adDasPlatformZkMsgModel.setMsgTimestamp(System.currentTimeMillis());
                    // 处理 client 发布的任务数据
                    bufferTrigger.enqueue(adDasPlatformZkMsgModel);
                }

                // 监听 done 路径下 成功、或者失败后， 重新计算 worker 负载
                if (event1.getData().getPath().endsWith(DONE) && !StringUtils.isEmpty(eventData)) {

                    String done = new String(curatorFramework.getData().forPath(event1.getData().getPath()));
                    log.info("event1.dataPath()={}, done={}", event1.getData().getPath(), done);
                    AdDasPlatformZkMsgModel adDasPlatformZkMsgModel = new AdDasPlatformZkMsgModel();
                    adDasPlatformZkMsgModel.setMsgType(AdDasPlatformZkMsgEnum.MASTER_CHECK_TASK_MSG);
                    adDasPlatformZkMsgModel.setMsgPath(event1.getData().getPath());
                    // FIXME 数据对象 AdDasPlatformTaskDoneModel
                    adDasPlatformZkMsgModel.setContentData(done);
                    adDasPlatformZkMsgModel.setMsgTimestamp(System.currentTimeMillis());
                    // 处理 client 发布的任务数据
                    bufferTrigger.enqueue(adDasPlatformZkMsgModel);
                }
            }
        };
        taskTreeCache.getListenable().addListener(resourceChildrenCacheListener);
        taskTreeCache.start();
    }

    @Override
    protected void consume(ConcurrentLinkedQueue<AdDasPlatformZkMsgModel> adDasPlatformZkMsgModels) {

        adDasPlatformZkMsgModels.stream().forEach(adDasPlatformZkMsgModel -> {
            if (adBetaBenchmarkDebugLogSwitch.get()) {
                log.info("adDasPlatformZkMsgModel={}", adDasPlatformZkMsgModel);
            }

            if (AdDasPlatformZkMsgEnum.WORKER_UPDATED_MSG == adDasPlatformZkMsgModel.getMsgType()) {
                // FIXME worker rebalance 处理
                AdDasPlatformWorkerRebalanceModel workerRebalanceModel = ObjectMapperUtils
                        .fromJSON(adDasPlatformZkMsgModel.getContentData(), AdDasPlatformWorkerRebalanceModel.class);

                if (PathChildrenCacheEvent.Type.CHILD_ADDED == workerRebalanceModel.getType()) {
                    // 初始化加入当前缓存
                    AdDasPlatformWorkerTaskWeightModel workerTaskWeightModel = new AdDasPlatformWorkerTaskWeightModel();
                    workerTaskWeightModel.setWorkerName(workerRebalanceModel.getWorkerName());
                    workerTaskWeightModels.add(workerTaskWeightModel);
                    // FIXME 重新计算 worker 负载
                    reloadWorkerRecord();
                } else if (PathChildrenCacheEvent.Type.CHILD_REMOVED == workerRebalanceModel.getType()) {

                    if (worker2TaskModelsMap.containsKey(workerRebalanceModel.getWorkerName())) {
                        // check 所有正在运行的任务
                        List<AdDasPlatformClientTaskSingleModel> clientTaskSingleModels = worker2TaskModelsMap
                                .get(workerRebalanceModel.getWorkerName());

                        if (adBetaBenchmarkDebugLogSwitch.get()) {
                            log.info("remove workerName={}", workerRebalanceModel.getWorkerName());
                        }
                        // FIXME 移除 worker2TaskModelsMap 数据
                        worker2TaskModelsMap.remove(workerRebalanceModel.getWorkerName());
                        // FIXME 重新计算 worker 负载
                        reloadWorkerRecord();
                        // 重新将任务 分配给其他实例 执行
                        for (AdDasPlatformClientTaskSingleModel clientTaskSingleModel : clientTaskSingleModels) {
                            // 每次找 serverWorkers 中任务数 最小的来分配
                            String rightWorker = findRightWorker();
                            assignTask(rightWorker, clientTaskSingleModel);
                        }
                    }
                } else {
                    log.error("workerRebalanceModel.getType()={} is error!", workerRebalanceModel.getType());
                }
            } else if (AdDasPlatformZkMsgEnum.CLIENT_PUBLISH_MSG == adDasPlatformZkMsgModel.getMsgType()) {
                // 基准流client 任务分配
                String contentData = adDasPlatformZkMsgModel.getContentData();

                if (!StringUtils.isEmpty(contentData)) {
                    AdDasPlatformClientTaskBatchModel adDasPlatformClientTaskModel = ObjectMapperUtils
                            .fromJSON(contentData, AdDasPlatformClientTaskBatchModel.class);

                    if (adDasPlatformClientTaskModel.getPublishType() == AdDasBenchmarkClientPublishType.PUBLISH.getCode()) {
                        List<Integer> taskNums = adDasPlatformClientTaskModel.getTaskIds();
                        for (Integer taskNum : taskNums) {
                            // 每次找 serverWorkers 中任务数 最小的来分配
                            String rightWorker = findRightWorker();
                            // FIXME master 来分配 worker 执行对应任务
                            AdDasPlatformClientTaskSingleModel taskSingleModel = new AdDasPlatformClientTaskSingleModel();
                            taskSingleModel.setClientName(adDasPlatformClientTaskModel.getClientName());
                            taskSingleModel.setTimestamp(adDasPlatformClientTaskModel.getTimestamp());
                            taskSingleModel.setHdfsPath(adDasPlatformClientTaskModel.getHdfsPath());
                            taskSingleModel.setTaskNum(taskNum);

                            assignTask(rightWorker, taskSingleModel);
                        }
                    } else if(adDasPlatformClientTaskModel.getPublishType() == AdDasBenchmarkClientPublishType.RETRY_PUBLISH.getCode()) {
                        List<Integer> retryTaskNums = adDasPlatformClientTaskModel.getTaskIds();
                        for (Integer taskNum : retryTaskNums) {
                            // 每次找 serverWorkers 中任务数 最小的来分配
                            String rightWorker = findRightWorker();
                            // FIXME master 来分配 worker 执行对应任务
                            AdDasPlatformClientTaskSingleModel taskSingleModel = new AdDasPlatformClientTaskSingleModel();
                            taskSingleModel.setClientName(adDasPlatformClientTaskModel.getClientName());
                            taskSingleModel.setTimestamp(adDasPlatformClientTaskModel.getTimestamp());
                            taskSingleModel.setHdfsPath(adDasPlatformClientTaskModel.getHdfsPath());
                            taskSingleModel.setTaskNum(taskNum);

                            assignTask(rightWorker, taskSingleModel);
                        }
                    }
                } else {
                    log.info("adDasPlatformZkMsgModel={} client success!", adDasPlatformZkMsgModel);
                }

                // FIXME 重新计算 worker 负载
                reloadWorkerRecord();
            } else if (AdDasPlatformZkMsgEnum.MASTER_CHECK_TASK_MSG == adDasPlatformZkMsgModel.getMsgType()) {
                // FIXME 校验任务done 情况， 已完成的任务， 清理出本地缓存
                String contentData = adDasPlatformZkMsgModel.getContentData();
                if (!StringUtils.isEmpty(contentData)) {
                    AdDasPlatformTaskDoneModel platformTaskDoneModel = ObjectMapperUtils
                            .fromJSON(contentData, AdDasPlatformTaskDoneModel.class);

                    // FIXME 移除 worker2TaskModelsMap 任务负载数据
                    log.info("remove workerName={}", platformTaskDoneModel);
                    if (worker2TaskModelsMap.containsKey(platformTaskDoneModel.getAssignName())) {
                        List<AdDasPlatformClientTaskSingleModel> clientTaskSingleModels = worker2TaskModelsMap
                                .get(platformTaskDoneModel.getAssignName());
                        if (!CollectionUtils.isEmpty(clientTaskSingleModels)) {
                            List<AdDasPlatformClientTaskSingleModel> clientTaskSingleModelNew = clientTaskSingleModels.stream()
                                    .filter(clientTaskSingleModel -> clientTaskSingleModel.getTaskNum() != platformTaskDoneModel.getTaskNum())
                                    .collect(Collectors.toList());

                            if (!CollectionUtils.isEmpty(clientTaskSingleModelNew)) {
                                log.info("clientTaskSingleModels.size={}, clientTaskSingleModelNew.size={},", clientTaskSingleModels.size(), clientTaskSingleModelNew);
                                worker2TaskModelsMap.put(platformTaskDoneModel.getAssignName(), clientTaskSingleModelNew);
                            } else {
                                worker2TaskModelsMap.remove(platformTaskDoneModel.getAssignName());
                            }
                        } else {
                            worker2TaskModelsMap.remove(platformTaskDoneModel.getAssignName());
                        }

                        // FIXME 重新计算  workerTaskWeightModels
                        List<AdDasPlatformWorkerTaskWeightModel> workerTaskWeightModelsNew = calculateWorkerWeight(worker2TaskModelsMap);
                        workerTaskWeightModels = workerTaskWeightModelsNew;
                        updateWorkerMaster();
                    }
                }
            }
        });
    }

    /**
     * 重新计算 worker 负载
     */
    private void reloadWorkerRecord() {

        Map<String, List<AdDasPlatformClientTaskSingleModel>> worker2TaskModelsMapNew = Maps.newConcurrentMap();

        worker2TaskModelsMap.forEach((key, value) -> {
            if (serverWorkers.contains(key)) {
                worker2TaskModelsMapNew.put(key, value);
            }
        });

        worker2TaskModelsMap = worker2TaskModelsMapNew;

        List<AdDasPlatformWorkerTaskWeightModel> workerTaskWeightModelsNew = calculateWorkerWeight(worker2TaskModelsMapNew);
        workerTaskWeightModels = workerTaskWeightModelsNew;
        log.info("workerTaskWeightModels={}", workerTaskWeightModels);
        updateWorkerMaster();
    }

    /**
     * 计算 worker 负载
     * @return
     */
    private List<AdDasPlatformWorkerTaskWeightModel> calculateWorkerWeight(Map<String, List<AdDasPlatformClientTaskSingleModel>> worker2TaskModelsMapTmp){

        // 遍历 server worker实例
        List<AdDasPlatformWorkerTaskWeightModel> workerTaskWeightModelsTmp = Lists.newArrayList();
        for (String worker : serverWorkers) {
            AdDasPlatformWorkerTaskWeightModel adDasPlatformWorkerTaskWeightModel = new AdDasPlatformWorkerTaskWeightModel();
            adDasPlatformWorkerTaskWeightModel.setWorkerName(worker);
            if (worker2TaskModelsMapTmp.containsKey(worker)) {
                adDasPlatformWorkerTaskWeightModel.setTaskCount(worker2TaskModelsMapTmp.get(worker).size());
            }

            workerTaskWeightModelsTmp.add(adDasPlatformWorkerTaskWeightModel);
        }

        return workerTaskWeightModelsTmp;
    }

    /**
     * 更新 worker_master 信息
     */
    private void updateWorkerMaster() {

        try {
            AdDasPlatformWorkMasterZkModel workMasterModel = new AdDasPlatformWorkMasterZkModel();
            workMasterModel.setBenchmarkEnv(platformServiceConfig.getBenchmarkEnv());
            workMasterModel.setServiceEnv(platformServiceConfig.getServiceEnv());
            workMasterModel.setServiceName(platformServiceConfig.getServiceName());

            Map<String, List<Integer>> workerMsg = Maps.newHashMap();
            worker2TaskModelsMap.forEach((key, value) -> {

                List<Integer> taskNums = value
                        .stream()
                        .map(AdDasPlatformClientTaskSingleModel::getTaskNum)
                        .collect(Collectors.toList());
                workerMsg.put(key, taskNums);
            });

            workMasterModel.setWorkerMsg(workerMsg);

            String workerMasterModelStr = ObjectMapperUtils.toJSON(workMasterModel);
            log.info("workerMasterModelStr={}", workerMasterModelStr);
            curatorFramework.setData().forPath(PLATFORM_BENCHMARK + getDasBenchmarkCommonPath(platformServiceConfig.getBenchmarkEnv()) + SERVERS + WORKER_MASTER,
                    workerMasterModelStr.getBytes());
        } catch (Exception e) {
            log.error("updateWorkerMaster() ocurr error! ", e);
        }
    }

    /**
     * FIXME 找寻第一个任务数最小的 worker 实例
     * @return
     */
    public synchronized String findRightWorker() {

        String rightWorker = "";
        // 初始化
        if (CollectionUtils.isEmpty(workerTaskWeightModels)) {
            initWorkerMasterCache();
        } else {
            // 清理 worker
            reloadWorkerRecord();
        }

        workerTaskWeightModels.sort(Comparator.comparing(AdDasPlatformWorkerTaskWeightModel::getTaskCount));
        log.info("findRightWorker() workerTaskWeightModels={}", workerTaskWeightModels);

        if (CollectionUtils.isEmpty(workerTaskWeightModels)) {
            rightWorker = serverWorkers.stream()
                    .filter(worker -> !worker2TaskModelsMap.containsKey(worker))
                    .findFirst().orElse("");
        } else {
            AdDasPlatformWorkerTaskWeightModel workerTaskWeightModel =  workerTaskWeightModels
                    .stream()
                    .findFirst()
                    .get();
            log.info("findRightWorker() workerTaskWeightModel={}", workerTaskWeightModel);
            rightWorker =  workerTaskWeightModel.getWorkerName();
        }

        log.info("rightWorker={}", rightWorker);

        if (StringUtils.isEmpty(rightWorker)) {
            log.error("can not find right worker!!!");
            // TODO perf打点报警

        }

        return rightWorker;
    }

    /**
     * 初始化 worker master 缓存
     * 1. 扫描 TASK 节点下所有的 任务
     */
    public void initWorkerMasterCache() {
        try {
            // 先查询task下的所有client子节点
            List<String> taskChilds = curatorFramework.getChildren()
                    .forPath(PLATFORM_BENCHMARK + getDasBenchmarkCommonPath(platformServiceConfig.getBenchmarkEnv()) + TASKS);

            Map<String, List<AdDasPlatformClientTaskSingleModel>> worker2TaskModelsMapTmp = Maps.newConcurrentMap();
            // 遍历client子节点， 扫描client节点下的所有任务数据
           for (String client : taskChilds) {
               List<String> clientChilds = curatorFramework.getChildren()
                       .forPath(PLATFORM_BENCHMARK + getDasBenchmarkCommonPath(platformServiceConfig.getBenchmarkEnv()) + TASKS + "/" + client + TASK);

               for (String task : clientChilds) {

                   log.info("task={}", task);

                   String taskContent = new String(curatorFramework.getData()
                           .forPath(PLATFORM_BENCHMARK + getDasBenchmarkCommonPath(platformServiceConfig.getBenchmarkEnv()) + TASKS
                                   + "/" +client + TASK  + "/" + task + TASKCONTENT));
                   String done = new String(curatorFramework.getData()
                           .forPath(PLATFORM_BENCHMARK + getDasBenchmarkCommonPath(platformServiceConfig.getBenchmarkEnv()) + TASKS
                                   + "/" +client + TASK + "/" + task + DONE));
                   String assign = new String(curatorFramework.getData()
                           .forPath(PLATFORM_BENCHMARK + getDasBenchmarkCommonPath(platformServiceConfig.getBenchmarkEnv()) + TASKS
                                   + "/" +client + TASK   + "/" + task + ASSIGN));
                   if (StringUtils.isEmpty(done)
                           && !StringUtils.isEmpty(taskContent)
                           && !StringUtils.isEmpty(assign)) {

                       AdDasPlatformClientTaskSingleModel taskSingleModel = ObjectMapperUtils
                               .fromJSON(taskContent, AdDasPlatformClientTaskSingleModel.class);

                       if (worker2TaskModelsMapTmp.containsKey(assign)) {
                           List<AdDasPlatformClientTaskSingleModel> clientTaskSingleModels = worker2TaskModelsMapTmp.get(assign);
                           clientTaskSingleModels.add(taskSingleModel);
                           worker2TaskModelsMapTmp.put(assign, clientTaskSingleModels);
                       } else {
                           List<AdDasPlatformClientTaskSingleModel> clientTaskSingleModels = Lists.newArrayList();
                           clientTaskSingleModels.add(taskSingleModel);
                           worker2TaskModelsMapTmp.put(assign, clientTaskSingleModels);
                       }
                   }
               }
           }

            worker2TaskModelsMap = worker2TaskModelsMapTmp;

           log.info("worker2TaskModelsMap={}", worker2TaskModelsMap);

           // 遍历 server worker实例
            List<AdDasPlatformWorkerTaskWeightModel> workerTaskWeightModelsTmp = calculateWorkerWeight(worker2TaskModelsMapTmp);
            workerTaskWeightModels = workerTaskWeightModelsTmp;
            log.info("workerTaskWeightModels={}", workerTaskWeightModels);
            updateWorkerMaster();

        } catch (Exception e) {
            log.error("initWorkerMasterCache() ocurr error!", e);
        }
    }

    /**
     * 分配任务
     * 1. 更新 对应 client 节点下的 assign 信息
     * 2. 更新 worker master 内的 缓存信息
     * @param rightWorker
     * @param taskSingleModel
     */
    public void assignTask(String rightWorker, AdDasPlatformClientTaskSingleModel taskSingleModel) {

        try {
            curatorFramework.setData()
                    .forPath(PLATFORM_BENCHMARK + getDasBenchmarkCommonPath(platformServiceConfig.getBenchmarkEnv()) + TASKS
                            + "/" + taskSingleModel.getClientName() + TASK + "/" + taskSingleModel.getTaskNum().toString()  + ASSIGN, rightWorker.getBytes());
            if (worker2TaskModelsMap.containsKey(rightWorker)) {
                List<AdDasPlatformClientTaskSingleModel> taskSingleModels = worker2TaskModelsMap.get(rightWorker);
                taskSingleModels.add(taskSingleModel);
                log.info("rightWorker={}, taskSingleModels={}", rightWorker, taskSingleModels);
                worker2TaskModelsMap.put(rightWorker, taskSingleModels);
                workerTaskWeightModels.stream()
                        .filter(workerTaskWeightModel -> rightWorker.equals(workerTaskWeightModel.getWorkerName()))
                        .forEach(workerTaskWeightModel -> workerTaskWeightModel.setTaskCount(taskSingleModels.size()));
            } else {
                List<AdDasPlatformClientTaskSingleModel> taskSingleModels = Lists.newArrayList();
                taskSingleModels.add(taskSingleModel);
                log.info("rightWorker={}, taskSingleModels={}", rightWorker, taskSingleModels);
                worker2TaskModelsMap.put(rightWorker, taskSingleModels);
                AdDasPlatformWorkerTaskWeightModel workerTaskWeightModel = new AdDasPlatformWorkerTaskWeightModel();
                workerTaskWeightModel.setWorkerName(rightWorker);
                workerTaskWeightModel.setTaskCount(taskSingleModels.size());
                workerTaskWeightModels.add(workerTaskWeightModel);
            }
        } catch (Exception e) {
            log.error("AssignTask() rightWorker={}, taskSingleModel={}",
                    rightWorker, taskSingleModel, e);
        }
    }
}