package com.kuaishou.ad.das.platform.core.common;

import java.util.List;

import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.recipes.cache.PathChildrenCache;
import org.apache.curator.framework.recipes.cache.TreeCache;

import com.google.common.collect.Lists;
import com.kuaishou.framework.concurrent.DynamicThreadExecutor;

/**
 * zk data model
 *
 *
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-01-29
 */
public class AdDasBenchmarkZkModel {

    protected static volatile Integer isStarted = 0;
    protected static final int SESSION_TIME = 120000;
    protected static final int CONNECT_TIMEOUT = 100000;

    protected static DynamicThreadExecutor adBaseRunnerExecutor;

    /**
     * 通用节点定义，可写为常量
     */
    public static final String SERVERS = "/servers";
    public static final String WORKER = "/worker";
    public static final String WORKER_MASTER = "/worker_master";

    public static final String TASKS = "/tasks";
    public static final String TASK = "/task";
    public static final String CLIENTS_PUB = "/client_publish";

    public static final String TOKEN = "/token";
    public static final String TASKCONTENT = "/taskContent";
    public static final String DONE = "/done";
    public static final String ASSIGN = "/assign";

    public static final String MASTER_DATA_RECORD = "/records";

    public static final String PLATFORM_COMMON_PB = "/platform/common_pb";
    public static final String PLATFORM_SERVICE = "/platform/service";
    public static final String PLATFORM_BENCHMARK = "/platform/benchmark";

    /**
     * 节点上报信息
     */
    protected static final String CONTAINERIP = "MY_POD_IP";
    protected static final String CONTAINERID = "MY_POD_NAME";

    protected CuratorFramework curatorFramework;
    protected PathChildrenCache serverPathChildrenCache;
    protected TreeCache taskTreeCache;
    protected String myName;  // 本实例的编号 外号

    protected String myServerNodePath; //本实例的path
    protected List<String> serverWorkers= Lists.newArrayList(); // 所有活着的实例

}
