package com.kuaishou.ad.das.platform.core.benchmark.server;

import static com.google.common.util.concurrent.MoreExecutors.shutdownAndAwaitTermination;
import static com.kuaishou.ad.das.platform.core.common.AdDasBenchmarkFactory.getConverter;
import static com.kuaishou.ad.das.platform.core.common.AdDasBenchmarkFactory.getExporter;
import static com.kuaishou.ad.das.platform.core.common.AdDasBenchmarkFactory.getFetcher;
import static com.kuaishou.ad.das.platform.core.common.AdDasBenchmarkFactory.getMerger;
import static com.kuaishou.ad.das.platform.core.common.AdDasBenchmarkFactory.judgeSpExporter;
import static com.kuaishou.ad.das.platform.core.common.AdDasBenchmarkFactory.judgeSpFetcher;
import static com.kuaishou.ad.das.platform.core.common.AdDasBenchmarkFactory.judgeSpMerger;
import static com.kuaishou.ad.das.platform.utils.constant.AdDasCommonConstant.AD_DAS_NAMESPACE_NEW;
import static com.kuaishou.ad.das.platform.utils.constant.AdDasCommonConstant.COMMON_BENCHMARK_LIFE_CYCLE_SUBTAG;
import static com.kuaishou.ad.das.platform.utils.kconf.AdDasBaseCommonKconf.AD_DAS_COMMON_BENCHMARK_LIFECYCLE_THREAD;
import static com.kuaishou.framework.concurrent.DynamicThreadExecutor.dynamic;
import static com.kuaishou.framework.util.PerfUtils.perf;
import static com.kuaishou.infra.framework.common.util.TermHelper.addTerm;
import static java.util.concurrent.TimeUnit.MINUTES;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.time.StopWatch;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;
import com.kuaishou.ad.das.platform.core.benchmark.model.AdDasPlatformClientTaskSingleModel;
import com.kuaishou.ad.das.platform.core.benchmark.server.converter.AdDasBenchmarkCoverter;
import com.kuaishou.ad.das.platform.core.benchmark.server.converter.impl.AdDasBenchmarkCommonCoverter;
import com.kuaishou.ad.das.platform.core.benchmark.server.exporter.AdDasBenchmarkExporter;
import com.kuaishou.ad.das.platform.core.benchmark.server.exporter.impl.AdDasBenchmarkCommonExporter;
import com.kuaishou.ad.das.platform.core.benchmark.server.fetcher.AdDasBenchmarkFetcher;
import com.kuaishou.ad.das.platform.core.benchmark.server.fetcher.impl.AdDasBenchmarkCommonFetcher;
import com.kuaishou.ad.das.platform.core.benchmark.helper.AdDasBenchmarkCommonBuildHelper;
import com.kuaishou.ad.das.platform.core.benchmark.helper.AdDasBenchmarkCommonQueryHelper;
import com.kuaishou.ad.das.platform.core.benchmark.server.merger.AdDasBenchmarkMerger;
import com.kuaishou.ad.das.platform.core.benchmark.server.merger.impl.AdDasBenchmarkCommonMerger;
import com.kuaishou.ad.das.platform.core.benchmark.model.AdDasTaskHandleModel;
import com.kuaishou.ad.das.platform.core.benchmark.model.AdDasTaskResultModel;
import com.kuaishou.ad.das.platform.core.benchmark.model.AdDasTaskSchemaModel;
import com.kuaishou.ad.das.platform.utils.dasenum.AdDasBenchmarkStatusEnum;
import com.kuaishou.framework.concurrent.DynamicThreadExecutor;

import lombok.extern.slf4j.Slf4j;

/**
 * 基准任务执行 生命周期
 *
 * AdDasTaskModel ——> AdDasTaskHandleModel ——> AdDasTaskResultModel ——>  zk das基准 协议
 *
 * 1、根据任务ID 获取任务列表
 * 2、多线并发执行任务
 * 3、根据任务名称 获取详细任务元数据信息
 * 4、fetcher 执行 查询DB
 * 5、convert 执行 数据转换
 * 6、exporter 写出结果： 可选写入hdfs、redis
 *
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-01-29
 */
@Slf4j
@Service
public class CommonServerWorkerLifeCycle {

    protected static DynamicThreadExecutor commonBenchmarkLifeCycleExecutor;

    @Autowired
    private AdDasBenchmarkCommonFetcher commonFetcher;

    @Autowired
    private AdDasBenchmarkCommonCoverter commonCoverter;

    @Autowired
    private AdDasBenchmarkCommonExporter commonExporter;

    @Autowired
    private AdDasBenchmarkCommonMerger commonMerger;

    @Autowired
    private AdDasBenchmarkCommonQueryHelper commonQueryHelper;

    @Autowired
    private AdDasBenchmarkCommonBuildHelper commonBuildHelper;

    public List<AdDasTaskResultModel> lifeCycle(String myName, AdDasPlatformClientTaskSingleModel taskSingleModel) {

        List<AdDasTaskResultModel> result = Collections.synchronizedList(new ArrayList<>());

        try {
            // 根据 taskNum 查询 任务列表
            List<AdDasTaskHandleModel> adDasTaskHandleModels = commonQueryHelper.queryTaskSchemaList(taskSingleModel.getTaskNum());
            log.info("lifeCycle() adDasTaskHandleModels={}", adDasTaskHandleModels);

            List<CompletableFuture> futureList = Lists.newArrayList();
            // 按照子任务粒度去执行
            for (AdDasTaskHandleModel taskHandleModel : adDasTaskHandleModels) {

                taskHandleModel.setTaskNum(taskSingleModel.getTaskNum());
                taskHandleModel.setTimestamp(taskSingleModel.getTimestamp());
                taskHandleModel.setHdfsPath(taskSingleModel.getHdfsPath());
                taskHandleModel.setClientName(taskSingleModel.getHdfsPath());
                taskHandleModel.setAssignName(myName);
                taskHandleModel.setExportType(0);  // FIXME 一期默认值
                taskHandleModel.setFetcherType(0); // FIXME 一期默认值

                log.info("lifeCycle() taskHandleModel={}", taskHandleModel);
                CompletableFuture<Void> futureProcess = CompletableFuture.runAsync(
                        () -> result.addAll(dasRun(taskHandleModel.getAdDasTaskSchemaModel(), taskHandleModel)),
                        commonBenchmarkLifeCycleExecutor);
                futureList.add(futureProcess);
            }

            CompletableFuture.allOf(futureList.toArray(new CompletableFuture[futureList.size()]))
                    .join();

        } catch (Exception e) {
            log.error("lifeCycle() occur error! taskSingleModel={}", taskSingleModel, e);

            AdDasTaskResultModel adDasTaskResultModel = commonBuildHelper
                    .initBuildResultModel(taskSingleModel, AdDasBenchmarkStatusEnum.RUNTIME_ERROR);
            result.add(adDasTaskResultModel);
            return result;
        }

        return result;
    }

    /**
     * 按照任务 处理数据
     *
     * 1、fetch
     * 2、convert
     * 3、export
     *
     * @param taskHandleModel
     * @return
     */
    private List<AdDasTaskResultModel> dasRun(AdDasTaskSchemaModel adDasTaskSchemaModel, AdDasTaskHandleModel taskHandleModel) {

        StopWatch created = StopWatch.createStarted();
        List<AdDasTaskResultModel> result = Lists.newArrayList();

        // 根据TableName 选择 fetcher
        AdDasBenchmarkFetcher fetcher = choseFetcher(taskHandleModel.getTableName());

        // 根据TableName 选择 merger
        AdDasBenchmarkMerger merger = choseMerger(taskHandleModel.getTableName());

        // 根据TableName 选择 converter
        AdDasBenchmarkCoverter converter = choseConverter(taskHandleModel.getTableName());

        // 根据TableName 选择 exporter
        AdDasBenchmarkExporter exporter = choseExporter(taskHandleModel.getTableName());

        adDasTaskSchemaModel.setHdfsPath(taskHandleModel.getHdfsPath());

        AdDasTaskHandleModel adDasTaskHandleModels = null;
        try {
            // fetch data
            adDasTaskHandleModels = fetcher.fetch(adDasTaskSchemaModel, taskHandleModel);
        } catch (Exception e) {
            // 异常处理
            AdDasTaskResultModel adDasTaskResultModel = commonBuildHelper.runError(adDasTaskSchemaModel, taskHandleModel, e,
                    "fetcher.fetch", AdDasBenchmarkStatusEnum.QUERY_DATA_ERROR);
            result.add(adDasTaskResultModel);
            return result;
        }
        // merge data 并发 跨库取数
        List<Map<String,Object>> mergerData = null;
        try {
            mergerData = merger.queryMergerData(adDasTaskSchemaModel, taskHandleModel);
        } catch (Exception e) {
            // 异常处理
            AdDasTaskResultModel adDasTaskResultModel = commonBuildHelper.runError(adDasTaskSchemaModel, taskHandleModel, e,
                    "merger.queryMergerData", AdDasBenchmarkStatusEnum.QUERY_MERGE_DATA_ERROR);
            result.add(adDasTaskResultModel);
            return result;
        }

        try {
            // covert data
            converter.convert(adDasTaskSchemaModel, adDasTaskHandleModels, mergerData);
        } catch (Exception e) {
            // 异常处理
            AdDasTaskResultModel adDasTaskResultModel =  commonBuildHelper.runError(adDasTaskSchemaModel, taskHandleModel, e,
                    "converter.convert", AdDasBenchmarkStatusEnum.CONVERT_DATA_ERROR);
            result.add(adDasTaskResultModel);
            return result;
        }

        try {
            // export data
            AdDasTaskResultModel adDasTaskResultModel = exporter.export(adDasTaskSchemaModel, adDasTaskHandleModels);
            result.add(adDasTaskResultModel);
        } catch (Exception e) {
            // 异常处理
            AdDasTaskResultModel adDasTaskResultModel = commonBuildHelper.runError(adDasTaskSchemaModel, taskHandleModel, e,
                    "exporter.export", AdDasBenchmarkStatusEnum.WRITE_DATA_2_HDFS_ERROR);
            result.add(adDasTaskResultModel);
            return result;
        }

        AdDasTaskResultModel adDasTaskResultModel = commonBuildHelper
                .initBuildResultModel(adDasTaskSchemaModel, AdDasBenchmarkStatusEnum.SUCESS_STATUS);
        result.add(adDasTaskResultModel);
        created.stop();
        perf(AD_DAS_NAMESPACE_NEW, COMMON_BENCHMARK_LIFE_CYCLE_SUBTAG,
                "dasRun", taskHandleModel.getTaskNum() + ":" + taskHandleModel.getTaskName())
                .micros(created.getTime(TimeUnit.MILLISECONDS))
                .logstash();

        return result;
    }

    private AdDasBenchmarkFetcher choseFetcher(String tableName) {
        AdDasBenchmarkFetcher adDasBenchmarkFetcher;

        if (judgeSpFetcher(tableName)) {
            adDasBenchmarkFetcher = getFetcher(tableName);
        } else {
            // 默认fetcher
            adDasBenchmarkFetcher = commonFetcher;
        }

        return adDasBenchmarkFetcher;
    }

    private AdDasBenchmarkCoverter choseConverter(String tableName) {
        AdDasBenchmarkCoverter adDasBenchmarkCoverter;

        if (judgeSpFetcher(tableName)) {
            adDasBenchmarkCoverter = getConverter(tableName);
        } else {
            // 默认converter
            adDasBenchmarkCoverter = commonCoverter;
        }

        return adDasBenchmarkCoverter;
    }

    private AdDasBenchmarkExporter choseExporter(String tableName) {
        AdDasBenchmarkExporter adDasBenchmarkExporter;

        if (judgeSpExporter(tableName)) {
            adDasBenchmarkExporter = getExporter(tableName);
        } else {
            // 默认exporter
            adDasBenchmarkExporter = commonExporter;
        }

        return adDasBenchmarkExporter;
    }

    private AdDasBenchmarkMerger choseMerger(String tableName) {

        AdDasBenchmarkMerger adDasBenchmarkMerger;

        if (judgeSpMerger(tableName)) {
            adDasBenchmarkMerger = getMerger(tableName);
        } else {
            adDasBenchmarkMerger = commonMerger;
        }

        return adDasBenchmarkMerger;
    }

    @PostConstruct
    private void init() {

        commonBenchmarkLifeCycleExecutor = dynamic(AD_DAS_COMMON_BENCHMARK_LIFECYCLE_THREAD::get,
                "ad-das-common-benchmark-lifecycle-pool");
        // TermHelper.addTerm 关机时注册关闭逻辑
        addTerm(() -> shutdownAndAwaitTermination(commonBenchmarkLifeCycleExecutor, 10, MINUTES));
    }

}
