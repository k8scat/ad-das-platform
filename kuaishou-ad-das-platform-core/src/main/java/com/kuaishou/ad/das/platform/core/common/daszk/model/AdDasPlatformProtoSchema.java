package com.kuaishou.ad.das.platform.core.common.daszk.model;

import static com.kuaishou.ad.das.platform.utils.kconf.AdDasPlatformKconf.adDasPlatformProtoCommonVersionConfig;

import java.io.Serializable;

import lombok.Data;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-07-11
 */
@Data
public class AdDasPlatformProtoSchema implements Serializable {

    private static final long serialVersionUID = -3439499026602931355L;
    /**
     * proto 包版本
     */
    private String pbVersion;

    /**
     * proto enum包版本
     */
    private String pbEnumVersion;

    /**
     * proto common 包版本
     */
    private String pbCommonVersion = adDasPlatformProtoCommonVersionConfig.get();

    /**
     * 获取唯一的 proto Jar 包版本
     * @return
     */
    public String getProtoJarVersion() {
        return pbVersion + "_" + pbEnumVersion + "_" + pbCommonVersion;
    }
}
