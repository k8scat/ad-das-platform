package com.kuaishou.ad.das.platform.core.benchmark.client.publisher;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import com.kuaishou.ad.das.platform.core.benchmark.helper.AdDasBenchmarkPlatformClientZkHelper;
import com.kuaishou.ad.das.platform.core.benchmark.model.AdDasPlatformClientTaskBatchModel;
import com.kuaishou.ad.das.platform.core.benchmark.model.AdDasPlatformClientTaskSingleModel;

import lombok.extern.slf4j.Slf4j;

/**
 * master zk publisher
 *
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-01-29
 */
@Lazy
@Slf4j
@Component
public class AdDasBenchmarkPublisher {

    @Autowired
    private AdDasBenchmarkPlatformClientZkHelper commonZkHelper;

    /**
     * 发布任务
     * @param clientTaskModel
     */
    public void publishTask(AdDasPlatformClientTaskBatchModel clientTaskModel) throws Exception {

        List<Integer> taskNums = clientTaskModel.getTaskIds();

        for (Integer taskNum : taskNums)  {
            AdDasPlatformClientTaskSingleModel clientTaskSingleModelModel = new AdDasPlatformClientTaskSingleModel();
            clientTaskSingleModelModel.setClientName(clientTaskModel.getClientName());
            clientTaskSingleModelModel.setHdfsPath(clientTaskModel.getHdfsPath());
            clientTaskSingleModelModel.setTimestamp(clientTaskModel.getTimestamp());
            clientTaskSingleModelModel.setTaskNum(taskNum);
            log.info("publishTask() clientTaskSingleModelModel={}", clientTaskSingleModelModel);
            // 更新发布 /task/clients/下的 具体任务节点
            commonZkHelper.clientPublishTask(clientTaskSingleModelModel);
        }

        // 更新 /task/clients/client_pushlish节点, 触发 worker_master 执行
        clientTaskModel.setCreateTime(System.currentTimeMillis());
        commonZkHelper.clientPublish(clientTaskModel);
    }

}
