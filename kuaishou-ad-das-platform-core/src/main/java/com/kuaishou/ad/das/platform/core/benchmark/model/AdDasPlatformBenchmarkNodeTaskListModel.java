package com.kuaishou.ad.das.platform.core.benchmark.model;

import java.io.Serializable;
import java.util.List;

import lombok.Data;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-06-06
 */
@Data
public class AdDasPlatformBenchmarkNodeTaskListModel implements Serializable {

    /**
     * client 名称
     */
    private String adDasClientName;

    /**
     *
     */
    private String hdfsPath;

    /**
     * 当前任务执行时间戳
     */
    private Long timestamp;

    /**
     * 当前任务列表
     */
    private List<Integer> clientTasks;

    /**
     * client下任务执行信息汇总
     */
    private List<AdDasPlatformBenchmarkNodeTaskModel> nodeTaskModels;
}
