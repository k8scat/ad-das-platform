package com.kuaishou.ad.das.platform.core.increment.filling;

import java.util.List;

import com.kuaishou.ad.das.platform.core.increment.model.AdDasCascadeModel;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-01-05
 */
public interface AdDasIncrementBatchFilling<D extends AdDasCascadeModel> {

    /**
     * 0、配置JSON数据：
     *  级联信息配置：
     *   {
     *     "CREATIVE": {
     *         "ad_dsp_creative": ["ad_dsp_creative_support_info"],
     *         "ad_dsp_creative_support_info": ["ad_dsp_creative", "ad_dsp_creative_advanced_program"],
     *         "ad_dsp_creative_advanced_program": ["ad_dsp_creative_support_info"]
     *     },
     *     "UNIT": {
     *         "ad_dsp_unit": ["ad_dsp_unit_support_info"]
     *     }
     *   }
     *
     *  级联取数SQL配置：
     *   {
     *     "ad_dsp_creative_support_info" : "select reative_id,creative_category,creative_tag from ad_dsp_creative_support_info_%s where account_id=? and creative_id in (#ids#) "
     *     "ad_dsp_unit_support_info" : "sql2"
     *   }
     *
     *   级联表数据字段配置：
     *   {
     *       "ad_dsp_creative_support_info": ["creative_id","creative_category","creative_tag"],
     *       "ad_dsp_creative": ["*"],
     *       "ad_dsp_unit_support_info" : ["",""]
     *       "ad_dsp_unit": ["*"]
     *   }
     *
     * 1、根据 level 去取横级联配置 需要级联的表列表、排除 tableName 本身
     * 2、并发查询DB， 根据表名 过滤需要的字段
     * 3、填充到 AdDasCascadeModel数据中
     * @param tableName
     * @param level
     * @param accountId
     * @param cascadeModels
     */
    List<AdDasCascadeModel> doBatchFilling(String tableName, String level, Long accountId, List<D> cascadeModels);
}
