package com.kuaishou.ad.das.platform.core.increment.lifecycle;

import static com.kuaishou.ad.das.platform.core.common.pbloader.AdDasClassLoaderHelper.setCurPbVersion;
import static com.kuaishou.ad.das.platform.utils.constant.AdDasCommonConstant.PB_ENUM_ADINSTANCE_STR;

import java.lang.reflect.Method;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.google.protobuf.Descriptors;
import com.google.protobuf.Message;
import com.google.protobuf.ProtocolMessageEnum;
import com.kuaishou.ad.das.platform.core.common.AdDasPlatformServiceConfig;
import com.kuaishou.ad.das.platform.core.common.daszk.model.AdDasPlatformProtoSchema;
import com.kuaishou.ad.das.platform.core.common.pbloader.AdDasClassLoaderHelper;
import com.kuaishou.ad.das.platform.core.increment.model.AdDasIncrementAdInstanceClassModel;
import com.kuaishou.ad.das.platform.core.increment.model.AdDasPlatformIncrementServiceModel;
import com.kuaishou.ad.das.platform.core.increment.model.AdDasPlatformIncrementTableColumnModel;
import com.kuaishou.ad.das.platform.core.increment.model.AdDasPlatformIncrementTableSchemaModel;
import com.kuaishou.ad.das.platform.core.increment.model.AdDasPlatformTableColumnModel;
import com.kuaishou.ad.das.platform.dal.model.AdDasIncrement;
import com.kuaishou.ad.das.platform.dal.model.AdDasModifyIncrementTableDetail;
import com.kuaishou.ad.das.platform.dal.model.AdDasServicePb;
import com.kuaishou.ad.das.platform.dal.repository.AdDasIncrementRepository;
import com.kuaishou.ad.das.platform.dal.repository.AdDasModifyIncrementTableDetailRepository;
import com.kuaishou.ad.das.platform.dal.repository.AdDasServicePbRepository;
import com.kuaishou.ad.das.platform.utils.dasenum.AdDasStatusEnum;
import com.kuaishou.ad.das.platform.utils.model.kconf.AdDasIncrementFillingTable;
import com.kuaishou.framework.util.ObjectMapperUtils;

import lombok.extern.slf4j.Slf4j;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-07-06
 */
@Lazy
@Slf4j
@Component
public class AdDasIncrementServiceModelCacher {

    @Autowired
    private AdDasPlatformServiceConfig platformServiceConfig;

    @Autowired
    private AdDasIncrementRepository incrementRepository;

    @Autowired
    private AdDasModifyIncrementTableDetailRepository incrementTableDetailRepository;

    @Autowired
    private AdDasClassLoaderHelper adDasClassLoaderHelper;

    @Autowired
    private AdDasServicePbRepository adDasServicePbRepository;

    /**
     * adInstance 类 path
     */
    private static String adInstanceBuilder = "com.kuaishou.ad.model.protobuf.AdIncModel";

    private static String adInstanceClassStr = "AdInstance";

    private static String adInstanceFieldClassStr = "AdInstanceField";

    /**
     * 增量服务信息模型
     */
    private static AdDasPlatformIncrementServiceModel adDasIncrementServiceModel = new AdDasPlatformIncrementServiceModel();

    /**
     * 增量服务 pb 类枚举 关系
     */
    private static Map<String, ProtocolMessageEnum> pbTypeEnumMap = Maps.newConcurrentMap();

    /**
     * 增量服务 AdInstance 类缓存
     */
    private static Map<String, AdDasIncrementAdInstanceClassModel> instanceClassModelMap = Maps.newConcurrentMap();

    public AdDasPlatformIncrementServiceModel getServiceModel() {
        return adDasIncrementServiceModel;
    }

    public synchronized void setServiceModel(AdDasPlatformIncrementServiceModel incrementServiceModel) {
        adDasIncrementServiceModel = incrementServiceModel;
    }

    /**
     * 获取最新的 AdInstance 类信息
     * @return
     */
    public synchronized AdDasIncrementAdInstanceClassModel getLastestClassModel() {
        AdDasPlatformProtoSchema adDasPlatformProtoSchema = AdDasClassLoaderHelper.getCurPbVersion();
        log.info("getLastestClassModel() adDasPlatformProtoSchema={}", adDasPlatformProtoSchema);
        if (!instanceClassModelMap.containsKey(adDasPlatformProtoSchema.getProtoJarVersion())) {
            try {

                //  初始化加载 AdInstance 类信息数据
                Class cls = adDasClassLoaderHelper.loadClass(adDasPlatformProtoSchema, adInstanceBuilder);
                Class[] clses = cls.getClasses();

                Class clsAdInstance = null;
                Class clsAdInstanceField = null;
                for (Class clsData : clses) {

                    if (clsData.getName().endsWith(adInstanceClassStr)) {
                        clsAdInstance = clsData;
                    }

                    if (clsData.getName().endsWith(adInstanceFieldClassStr)) {
                        clsAdInstanceField = clsData;
                    }
                }
                Method method = clsAdInstance.getMethod("newBuilder");
                Object obj = method.invoke(null, null);
                Message.Builder msgBuilder = (Message.Builder) obj;       // 得到 builder

                Method methodField = clsAdInstanceField.getMethod("newBuilder");
                Object objField = methodField.invoke(null, null);
                Message.Builder msgBuilderField = (Message.Builder) objField;       // 得到 builder

                AdDasIncrementAdInstanceClassModel instanceClassModel = new AdDasIncrementAdInstanceClassModel();
                instanceClassModel.setProtoJarVersion(adDasPlatformProtoSchema.getProtoJarVersion());
                instanceClassModel.setClsBuilder(clsAdInstance);
                instanceClassModel.setMethod(method);
                instanceClassModel.setClsFieldBuilder(clsAdInstanceField);
                instanceClassModel.setMethodField(methodField);
                log.info("getLastestClassModel() instanceClassModel={}", instanceClassModel);
                instanceClassModelMap.put(adDasPlatformProtoSchema.getProtoJarVersion(), instanceClassModel);
            } catch (Exception e) {
                log.error("getLastestClassModel() ocurr error! adDasPlatformProtoSchema={}",
                        adDasPlatformProtoSchema, e);
            }
        }

        log.info("getLastestClassModel() instanceClassModelMap={}", instanceClassModelMap);

        return instanceClassModelMap.get(adDasPlatformProtoSchema.getProtoJarVersion());
    }

    /**
     * 组装 增量服务 模型
     * @param adDasIncrement
     * @param incrementTableDetails
     * @return
     */
    public synchronized AdDasPlatformIncrementServiceModel buildServiceModel(AdDasIncrement adDasIncrement, List<AdDasModifyIncrementTableDetail> incrementTableDetails) {

        AdDasPlatformIncrementServiceModel incrementServiceModel = new AdDasPlatformIncrementServiceModel();

        // 服务基本信息
        incrementServiceModel.setServiceName(adDasIncrement.getIncrementName());
        incrementServiceModel.setDataSource(adDasIncrement.getDataSource());
        incrementServiceModel.setTopic(adDasIncrement.getTopic());
        incrementServiceModel.setConsumerGroup(adDasIncrement.getConsumerGroup());
        incrementServiceModel.setCreateTime(adDasIncrement.getCreateTime());
        incrementServiceModel.setUpdateTime(adDasIncrement.getUpdateTime());

        // 增量服务表详情
        AdDasPlatformIncrementTableSchemaModel incrementTableSchemaModel = new AdDasPlatformIncrementTableSchemaModel();

        Set<String> pbEnumSet = incrementTableDetails.stream()
                .map(AdDasModifyIncrementTableDetail::getPbClassEnum)
                .collect(Collectors.toSet());
        // level 对应Type 关系
        Map<String, ProtocolMessageEnum> tableLevel2TypeMap = getPbEnum(pbEnumSet);
        log.info("pbEnumSet={}, tableLevel2TypeMap={}", pbEnumSet, tableLevel2TypeMap);

        // table 2 level 映射关系
        Map<String, String> table2LevelMap = Maps.newHashMap();

        // 表横级联关系
        Map<String, Map<String, Set<String>>> fillingTable = Maps.newHashMap();

        // 表字段填充
        Map<String, AdDasPlatformIncrementTableColumnModel> tableColumnMap = Maps.newHashMap();

        // 表对应 type 关系
        Map<String, ProtocolMessageEnum> tableSchemaMap = Maps.newHashMap();
        for (AdDasModifyIncrementTableDetail incrementTableDetail : incrementTableDetails) {
            String increTable = incrementTableDetail.getIncreTable();
            String pbClassEnum = incrementTableDetail.getPbClassEnum();
            ProtocolMessageEnum instanceType = tableLevel2TypeMap.get(pbClassEnum);
            log.info("increTable={}, pbClassEnum={}, instanceType={}", increTable, pbClassEnum, instanceType);
            tableSchemaMap.put(increTable, instanceType);
            table2LevelMap.put(increTable, pbClassEnum);

            Integer tableType = incrementTableDetail.getTableType();
            String cascadedTable = incrementTableDetail.getCascadedTable();

            // 只需要处理主表
            if (tableType == 1 && StringUtils.isEmpty(cascadedTable)) {
                Set<String> cascadeTableSet = ObjectMapperUtils
                        .fromJSON(cascadedTable, Set.class, String.class);
                if (fillingTable.containsKey(pbClassEnum)) {

                    Map<String, Set<String>> levelMap = fillingTable.get(pbClassEnum);
                    log.info("levelMap={}, incrementTableDetail={}", levelMap, incrementTableDetail);
                } else {
                    Map<String, Set<String>> levelMap = Maps.newHashMap();
                    levelMap.put(increTable, cascadeTableSet);
                    for (String cascadeTable: cascadeTableSet) {
                        levelMap.put(increTable, Sets.newHashSet(cascadeTable));
                    }
                    log.info("levelMap={}, incrementTableDetail={}", levelMap, incrementTableDetail);
                    fillingTable.put(pbClassEnum, levelMap);
                }
            }

            AdDasPlatformIncrementTableColumnModel tableColumnModel = new AdDasPlatformIncrementTableColumnModel();
            tableColumnModel.setTableName(incrementTableDetail.getIncreTable());
            tableColumnModel.setTableType(incrementTableDetail.getTableType());
            tableColumnModel.setPrimaryIdKey(incrementTableDetail.getPrimaryIdKey());
            // 非主表才有外键
            tableColumnModel.setForeignKey("");

            String tableColumn = incrementTableDetail.getTableColumn();
            List<AdDasPlatformTableColumnModel> tableColumnModels = ObjectMapperUtils
            .fromJSON(tableColumn, List.class, AdDasPlatformTableColumnModel.class);

            Map<String, String> columnTypeMap = Maps.newHashMap();

            for (AdDasPlatformTableColumnModel tableColumnModelData : tableColumnModels) {
                columnTypeMap.put(tableColumnModelData.getColumnName(), tableColumnModelData.getColumnType());
            }
            tableColumnModel.setColumnTypeMap(columnTypeMap);
            tableColumnMap.put(increTable, tableColumnModel);
        }
        // 1. 表 与 类枚举 关系
        log.info("tableSchemaMap={}", tableSchemaMap);
        incrementTableSchemaModel.setTable2TypeMap(tableSchemaMap);
        // 2. 表 与 level 关系
        log.info("table2LevelMap={}", table2LevelMap);
        incrementTableSchemaModel.setTable2LevelMap(table2LevelMap);
        // 3. 数据层 2 pb类枚举关系
        log.info("tableLevel2TypeMap={}", tableLevel2TypeMap);
        incrementTableSchemaModel.setTableLevel2TypeMap(tableLevel2TypeMap);
        // 4. 表 与 字段 关系
        log.info("tableColumnMap={}", tableColumnMap);
        incrementTableSchemaModel.setTableColumnMap(tableColumnMap);
        // 5.横向填充关系
        AdDasIncrementFillingTable incrementFillingTable = new AdDasIncrementFillingTable();
        incrementFillingTable.setFillingTable(fillingTable);
        log.info("incrementFillingTable={}", incrementFillingTable);
        incrementTableSchemaModel.setIncrementFillingTable(incrementFillingTable);
        // 6.表横向填充 sql
        Map<String, String> table2FillingSqlMap = Maps.newHashMap();
        log.info("table2FillingSqlMap={}", table2FillingSqlMap);
        incrementTableSchemaModel.setTable2FillingSqlMap(table2FillingSqlMap);

        incrementServiceModel.setIncrementTableSchemaModel(incrementTableSchemaModel);

        return incrementServiceModel;
    }


    public Map<String, ProtocolMessageEnum> getPbEnum(Set<String> pbEnumSet) {

        Map<String, ProtocolMessageEnum> pbEnumMap = Maps.newHashMap();

        // 判断是否存在新增的 pbTypeEnum
        Set<String> newPbEnumSet = pbEnumSet.stream()
                .filter(pbEnumName -> !pbTypeEnumMap.containsKey(pbEnumName))
                .collect(Collectors.toSet());
        log.info("newPbEnumSet={}, pbTypeEnumMap={}", newPbEnumSet, pbTypeEnumMap);

        if (!CollectionUtils.isEmpty(newPbEnumSet)) {
            updatePbTypeEnumMap();
        }

        log.info("pbTypeEnumMap={}", pbTypeEnumMap);

        for (String pbEnumName : pbEnumSet) {

            log.info("pbEnumSet={}", pbEnumSet);
            if (pbTypeEnumMap.containsKey(pbEnumName)) {
                pbEnumMap.put(pbEnumName, pbTypeEnumMap.get(pbEnumName));
            } else {
                // TODO perf 打点报警
            }
        }

        return pbEnumMap;
    }

    /**
     * 反射更新当前 pb类枚举 关系
     */
    public void updatePbTypeEnumMap() {

        try {
            AdDasPlatformProtoSchema curPbVersion = AdDasClassLoaderHelper.getCurPbVersion();

            if (curPbVersion == null) {
                // 根据服务名称查询 DB 拿到最新的 kuaishou-ad-new-biz-proto 版本
                List<AdDasServicePb> servicePbs = adDasServicePbRepository
                        .queryByService(platformServiceConfig.getServiceName(), AdDasStatusEnum.DEFAULT.getCode());
                log.info("AdDasIncrementServiceModelCacher defaultLoadPb() servicePbs={}", servicePbs);

                if (CollectionUtils.isEmpty(servicePbs)) {
                    return;
                }

                AdDasPlatformProtoSchema adDasPlatformProtoSchema = new AdDasPlatformProtoSchema();
                String lastestPbVersion = servicePbs.get(0).getPbVersion();
                String lastestPbEnumVersion = servicePbs.get(0).getPbEnumVersion();
                adDasPlatformProtoSchema.setPbVersion(lastestPbVersion);
                adDasPlatformProtoSchema.setPbEnumVersion(lastestPbEnumVersion);
                log.info("AdDasIncrementServiceModelCacher defaultLoadPb() adDasPlatformProtoSchema={}", adDasPlatformProtoSchema);

                setCurPbVersion(adDasPlatformProtoSchema);
                curPbVersion = adDasPlatformProtoSchema;
            }
            log.info("curPbVersion={}", curPbVersion);
            Class enumCls = adDasClassLoaderHelper.loadEnumClass(curPbVersion, PB_ENUM_ADINSTANCE_STR);

            Object[] enumClassList = enumCls.getEnumConstants();
            for (Object enumClass : enumClassList) {
                ProtocolMessageEnum messageEnum = (ProtocolMessageEnum)enumClass;
                log.info("messageEnum={}, messageEnum.getNumber()={}", messageEnum);
                pbTypeEnumMap.put(enumClass.toString(), messageEnum);
            }

        } catch (Exception e) {
            log.error("updatePbTypeEnumMap() ocurr error!", e);
        }
    }

    @PostConstruct
    public void init() {

        if (platformServiceConfig.isIncre()) {
            log.info("start init()!");
            AdDasIncrement adDasIncrement = queryIncrementService();
            log.info("adDasIncrement={}", adDasIncrement);
            // 组装增量服务基本信息
            // query ad_das_modify_increment_table_detail 获取增量表信息
            List<AdDasModifyIncrementTableDetail> incrementTableDetails = queryIncrementTableDetail(adDasIncrement);
            log.info("incrementTableDetails={}", incrementTableDetails);
            // 组装增量服务表信息
            AdDasPlatformIncrementServiceModel incrementServiceModel = this.buildServiceModel(adDasIncrement, incrementTableDetails);
            log.info("incrementServiceModel={}", incrementServiceModel);
            this.setServiceModel(incrementServiceModel);
            log.info("init() sucess!");

            AdDasIncrementAdInstanceClassModel adInstanceClassModel = this.getLastestClassModel();
            log.info("adInstanceClassModel={}", adInstanceClassModel);
        }
    }

    /**
     * query ad_das_increment 获取增量服务基本信息
     * @return
     */
    private AdDasIncrement queryIncrementService() {

        AdDasIncrement adDasIncrement = incrementRepository
                .queryByIncrementName(platformServiceConfig.getServiceName());
        log.info("queryIncrementService() serviceName={}, adDasIncrement={}",
                platformServiceConfig.getServiceName(), adDasIncrement);
        return adDasIncrement;
    }

    /**
     * query ad_das_modify_increment_table_detail 获取增量表信息
     * @return
     */
    private List<AdDasModifyIncrementTableDetail> queryIncrementTableDetail(AdDasIncrement adDasIncrement) {
        List<AdDasModifyIncrementTableDetail> incrementTableDetails = incrementTableDetailRepository
                .queryByIncreId(adDasIncrement.getId());
        log.info("queryIncrementTableDetail() increId={}, incrementTableDetails.size()={}",
                adDasIncrement.getId(), incrementTableDetails.size());
        return incrementTableDetails;
    }
}
