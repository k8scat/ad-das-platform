package com.kuaishou.ad.das.platform.core.increment.producer.impl;

import static com.kuaishou.ad.das.platform.core.utils.AdDasPlatformPbReflectUtils.setBuilderField;
import static com.kuaishou.ad.das.platform.utils.constant.AdDasCommonConstant.AD_DAS_COMMON_INCR_LIFE_CYCLE_SUBTAG;
import static com.kuaishou.ad.das.platform.utils.constant.AdDasCommonConstant.AD_DAS_NAMESPACE_NEW;
import static com.kuaishou.framework.util.PerfUtils.perf;

import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.function.BiConsumer;

import org.apache.commons.compress.utils.Lists;
import org.apache.commons.lang3.time.StopWatch;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;

import com.google.protobuf.Message;
import com.kuaishou.ad.das.platform.core.increment.helper.AdDasPlatformIncrementCommonHelper;
import com.kuaishou.ad.das.platform.core.increment.model.AdDasCascadeModel;
import com.kuaishou.framework.kafka.PartitionSelector;
import com.kuaishou.framework.util.HostInfo;
import com.kuaishou.framework.util.PerfUtils;
import com.kuaishou.infra.framework.kafka.KafkaProducers;

import lombok.extern.slf4j.Slf4j;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-01-05
 */
@Slf4j
public abstract class AdDasIncrementAbstractBatchProducer {

    @Autowired
    protected AdDasPlatformIncrementCommonHelper incrementCommonHelper;

    protected abstract void sendKafkaMore(String tableName, AdDasCascadeModel adDasCascadeModel);

    protected void defaultBatchSendKafka(
            String tableName, List<AdDasCascadeModel> adDasCascadeModels,
            BiConsumer<String, AdDasCascadeModel> convertConsumer) {

        PerfUtils.perf(AD_DAS_NAMESPACE_NEW, AD_DAS_COMMON_INCR_LIFE_CYCLE_SUBTAG, "kbusCountTrace", "defaultBatchSendKafka")
                .count(adDasCascadeModels.size())
                .logstash();

        StopWatch createdAll = StopWatch.createStarted();

        adDasCascadeModels.forEach(adDasCascadeModel -> {

            this.defaultSendKafka(tableName, adDasCascadeModel, convertConsumer);
        });
        createdAll.stop();
        perf(AD_DAS_NAMESPACE_NEW, AD_DAS_COMMON_INCR_LIFE_CYCLE_SUBTAG, "defaultBatchSendKafka", tableName)
                .micros(createdAll.getTime(TimeUnit.MICROSECONDS))
                .logstash();
    }

    protected void defaultSendKafka(String tableName, AdDasCascadeModel adDasCascadeModel,
                                    BiConsumer<String, AdDasCascadeModel> convertConsumer) {

        Message.Builder adInstanceBuilder = adDasCascadeModel.getAdInstanceBuilder();
        Map<String, Message> instanceFields = adDasCascadeModel.getInstanceFields();

        if (!CollectionUtils.isEmpty(instanceFields)) {
            List<Message> instanceFieldList = Lists.newArrayList();
            instanceFields.forEach((key, value) -> {
                instanceFieldList.add(value);
            });
            setBuilderField(adInstanceBuilder, adInstanceBuilder.getDescriptorForType(),
                    "fields", instanceFieldList);

            if (HostInfo.debugHost()) {
                log.info("defaultSendKafka() topic={}, tableName={}, adInstanceBuilder={}",
                        incrementCommonHelper.getTopic(), tableName, adInstanceBuilder);
            }

            KafkaProducers.sendProto(incrementCommonHelper.getTopic(), adInstanceBuilder.build(),
                    PartitionSelector.hashEx(adDasCascadeModel.getId()));
        } else {
            log.error("defaultSendKafka() topic={}, tableName={}, adDasCascadeModel={}",
                    incrementCommonHelper.getTopic(), tableName,  adDasCascadeModel);
            log.error("instanceFields is empty!!!!");
        }
    }
}
