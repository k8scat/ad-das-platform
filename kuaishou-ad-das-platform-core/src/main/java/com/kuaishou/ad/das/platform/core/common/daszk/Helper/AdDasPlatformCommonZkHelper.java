package com.kuaishou.ad.das.platform.core.common.daszk.Helper;

import static com.kuaishou.ad.das.platform.core.utils.AdDasBaseMultiEnvUtils.getDasBenchmarkCommonPath;

import java.util.List;

import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.data.Stat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import com.kuaishou.ad.das.platform.core.benchmark.model.AdDasPlatformClientTaskBatchModel;
import com.kuaishou.ad.das.platform.core.common.AdDasPlatformAbstractZkHelper;
import com.kuaishou.ad.das.platform.core.common.daszk.model.AdDasPublishPbSchema;
import com.kuaishou.ad.das.platform.dal.model.AdDasBenchmark;
import com.kuaishou.ad.das.platform.dal.model.AdDasModifyPbMsg;
import com.kuaishou.framework.util.ObjectMapperUtils;

import lombok.extern.slf4j.Slf4j;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-12-09
 */
@Slf4j
@Component
public class AdDasPlatformCommonZkHelper extends AdDasPlatformAbstractZkHelper {

    @Autowired
    private AdDasPlatformApiZkHelper adDasApiZkHelper;

    @Autowired
    private AdDasPlatformBaseZkHelper adDasBaseZkHelper;

    @Autowired
    private AdDasPlatformIncrementZkHelper adDasIncrementZkHelper;

    /**
     * 触发发布 proto 升级
     * @param adDasModifyPbMsg
     * @param publishInstances
     * @throws Exception
     */
    public void publishPbUpdateZkMsg(AdDasModifyPbMsg adDasModifyPbMsg,
                                     List<String> publishInstances) throws Exception {

        String prePbPath = PLATFORM_COMMON_PB + "/" + platformServiceConfig.getServiceType()
                + "/" + platformServiceConfig.getServiceName() + "/prepb";
        Stat statPrePb =  getCuratorFramework().checkExists().forPath(prePbPath);
        if (statPrePb == null) {
            getCuratorFramework().create().creatingParentsIfNeeded().withMode(CreateMode.PERSISTENT)
                    .forPath(prePbPath);
        }

        // 写入 /prepb 发布数据
        AdDasPublishPbSchema adDasPublishPbSchema = new AdDasPublishPbSchema();
        adDasPublishPbSchema.setModifyId(adDasModifyPbMsg.getModifyId());
        adDasPublishPbSchema.setPbVersion(adDasModifyPbMsg.getTargetPbVersion());
        adDasPublishPbSchema.setPbEnumVersion(adDasModifyPbMsg.getTargetPbEnumVersion());
        adDasPublishPbSchema.setPbCommonVersion(adDasModifyPbMsg.getTargetPbCommonVersion());

        if (!CollectionUtils.isEmpty(publishInstances)) {
            adDasPublishPbSchema.setPublishInstances(publishInstances);
            adDasPublishPbSchema.setPublishType(2);
        } else {
            adDasPublishPbSchema.setPublishType(1);
        }

        String adDasPublishPbSchemaStr = ObjectMapperUtils.toJSON(adDasPublishPbSchema);
        log.info("publishPbUpdateZkMsg() adDasPublishPbSchemaStr={}", adDasPublishPbSchemaStr);
        getCuratorFramework().setData().forPath(prePbPath, adDasPublishPbSchemaStr.getBytes());
    }

    /**
     * 回滚发布zk
     * @param adDasModifyPbMsg
     * @throws Exception
     */
    public void rollbecckPbUpdateZkMsg(AdDasModifyPbMsg adDasModifyPbMsg) throws Exception {

        String prePbPath = PLATFORM_COMMON_PB + "/" + platformServiceConfig.getServiceType()
                + "/" + platformServiceConfig.getServiceName() + "/prepb";
        Stat statPrePb =  getCuratorFramework().checkExists().forPath(prePbPath);
        if (statPrePb == null) {
            getCuratorFramework().create().creatingParentsIfNeeded().withMode(CreateMode.PERSISTENT)
                    .forPath(prePbPath);
        }

        // 写入 /prepb 发布数据
        AdDasPublishPbSchema adDasPublishPbSchema = new AdDasPublishPbSchema();
        adDasPublishPbSchema.setModifyId(adDasModifyPbMsg.getModifyId());
        adDasPublishPbSchema.setPbVersion(adDasModifyPbMsg.getTargetPbVersion());
        adDasPublishPbSchema.setPbEnumVersion(adDasModifyPbMsg.getTargetPbEnumVersion());
        adDasPublishPbSchema.setPbCommonVersion(adDasModifyPbMsg.getTargetPbCommonVersion());
        adDasPublishPbSchema.setPublishType(3);

        String adDasPublishPbSchemaStr = ObjectMapperUtils.toJSON(adDasPublishPbSchema);
        log.info("publishPbUpdateZkMsg() adDasPublishPbSchemaStr={}", adDasPublishPbSchemaStr);
        getCuratorFramework().setData().forPath(prePbPath, adDasPublishPbSchemaStr.getBytes());
    }

    /**
     * 查询当前任务正在运行信息
     * @param adDasBenchmark
     * @return
     * @throws Exception
     */
    public AdDasPlatformClientTaskBatchModel getClientTaskData(AdDasBenchmark adDasBenchmark) throws Exception {

        String clientPublishPath = PLATFORM_BENCHMARK + getDasBenchmarkCommonPath("client") + TASKS
                + "/" + adDasBenchmark.getBenchmarkName() + CLIENTS_PUB;
        log.info("clientPublishPath={}", clientPublishPath);

        String clientTaskModelStr = new String(getCuratorFramework().getData().forPath(clientPublishPath));
        log.info("clientPublishPath={}", clientPublishPath);

        if (StringUtils.isEmpty(clientTaskModelStr)) {
            return null;
        }

        AdDasPlatformClientTaskBatchModel clientTaskBatchModel = ObjectMapperUtils
                .fromJSON(clientTaskModelStr, AdDasPlatformClientTaskBatchModel.class);

        return clientTaskBatchModel;
    }

}

