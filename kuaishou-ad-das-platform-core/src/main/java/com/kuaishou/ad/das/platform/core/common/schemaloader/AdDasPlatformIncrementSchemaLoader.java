package com.kuaishou.ad.das.platform.core.common.schemaloader;

import java.util.concurrent.ConcurrentHashMap;


import org.springframework.context.annotation.Lazy;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import com.kuaishou.ad.das.platform.core.increment.model.AdDasPlatformIncrementServiceModel;

import lombok.extern.slf4j.Slf4j;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-06-05
 */
@Lazy
@Slf4j
@Component
public class AdDasPlatformIncrementSchemaLoader {

    private static ConcurrentHashMap<String, AdDasPlatformIncrementServiceModel> schemaModelCache = new ConcurrentHashMap<>();

    public AdDasPlatformIncrementServiceModel getIncrementSchemaModel() {
        return schemaModelCache.get("increment_schema");
    }


    /**
     * 服务启动时，执行 schema 数据的初始化
     */

    @EventListener(value = {ContextRefreshedEvent.class})
    public void init(ContextRefreshedEvent event) {
        // 查询DB

        // 转换数据
        AdDasPlatformIncrementServiceModel adDasPlatformIncrementSchemaModel = new AdDasPlatformIncrementServiceModel();

        // 更新缓存
        schemaModelCache.put("increment_schema", adDasPlatformIncrementSchemaModel);

        // 初始化监听zk

    }
}
