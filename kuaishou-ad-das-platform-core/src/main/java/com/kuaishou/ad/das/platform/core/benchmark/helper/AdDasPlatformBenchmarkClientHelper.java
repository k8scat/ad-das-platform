package com.kuaishou.ad.das.platform.core.benchmark.helper;

import static com.cronutils.model.CronType.UNIX;
import static com.cronutils.model.field.expression.FieldExpressionFactory.every;
import static com.cronutils.model.field.expression.FieldExpressionFactory.on;
import static com.kuaishou.ad.das.platform.utils.kconf.AdDasPlatformKconf.adDasPlatformClientCronConfig;
import static com.kuaishou.ad.das.platform.utils.kconf.AdDasPlatformKconf.adDasPlatformClientRunStartConfig;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cronutils.builder.CronBuilder;
import com.cronutils.model.Cron;
import com.cronutils.model.definition.CronDefinitionBuilder;
import com.kuaishou.ad.das.platform.core.common.AdDasPlatformServiceConfig;

import lombok.extern.slf4j.Slf4j;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-06-05
 */
@Slf4j
@Component
public class AdDasPlatformBenchmarkClientHelper {

    @Autowired
    protected AdDasBenchmarkPlatformClientZkHelper clientCommonZkHelper;

    @Autowired
    protected AdDasPlatformServiceConfig adDasPlatformServiceConfig;

    /**
     * 根据服务配置，返回定时规则
     *
     * @return
     */
    public Cron getCronData() {
        // 每两个小时整点运行一次
        String serviceName = adDasPlatformServiceConfig.getServiceName();

        Map<String, String> runStartConfigMap = adDasPlatformClientCronConfig.get();

        if (runStartConfigMap.containsKey(serviceName)) {

            String cronStr = runStartConfigMap.get(serviceName);
            String[] cronStrs = cronStr.split("-");
            Integer cronEvery = Integer.valueOf(cronStrs[0]);
            Integer cronOn = Integer.valueOf(cronStrs[1]);
            return CronBuilder.cron(CronDefinitionBuilder.instanceDefinitionFor(UNIX))
                    .withHour(every(cronEvery))
                    .withMinute(on(cronOn))
                    .instance()
                    .validate();
        } else {
            return CronBuilder.cron(CronDefinitionBuilder.instanceDefinitionFor(UNIX))
                    .withHour(every(2))
                    //.withMinute(on(1))
                    .instance()
                    .validate();
        }
    }

    /**
     * 根据服务配置，判断是否立即重跑
     * 默认不立即重跑
     * @return
     */
    public boolean runAtStart() {

        String serviceName = adDasPlatformServiceConfig.getServiceName();

        Map<String, Integer> runStartConfigMap = adDasPlatformClientRunStartConfig.get();

        if (runStartConfigMap.containsKey(serviceName)) {
            return runStartConfigMap.get(serviceName) == 1;
        }

        return false;
    }

    /**
     * 初始化client服务
     */
    public void initClient() {

        try {
            clientCommonZkHelper.initBenchmarkClient();
        } catch (Exception e) {
            log.error("initClient ocurr error!", e);
        }
    }
}
