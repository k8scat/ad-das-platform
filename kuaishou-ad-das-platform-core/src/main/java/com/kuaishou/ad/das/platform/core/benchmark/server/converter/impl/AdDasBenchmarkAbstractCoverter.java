package com.kuaishou.ad.das.platform.core.benchmark.server.converter.impl;

import static com.kuaishou.ad.das.platform.core.utils.AdDasBenchmarkUtils.buildPbClassName;
import static com.kuaishou.ad.das.platform.utils.constant.AdDasCommonConstant.AD_DAS_NAMESPACE_NEW;
import static com.kuaishou.ad.das.platform.utils.constant.AdDasCommonConstant.COMMON_BENCHMARK_LIFE_CYCLE_SUBTAG;
import static com.kuaishou.framework.util.PerfUtils.perf;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.protobuf.Descriptors;
import com.google.protobuf.Message;
import com.kuaishou.ad.das.platform.core.benchmark.helper.AdDasBenchmarkCommonConvertHelper;
import com.kuaishou.ad.das.platform.core.utils.AdDasPlatformPbReflectUtils;
import com.kuaishou.ad.das.platform.core.benchmark.model.AdDasBenchmarkCascadeTableView;
import com.kuaishou.ad.das.platform.core.benchmark.model.AdDasBenchmarkDb2PbColumnMsgView;
import com.kuaishou.ad.das.platform.core.benchmark.model.AdDasPbClassReflectModel;
import com.kuaishou.ad.das.platform.core.benchmark.model.AdDasPlatformBenchmarkResultModel;
import com.kuaishou.ad.das.platform.core.benchmark.model.AdDasTaskHandleModel;
import com.kuaishou.ad.das.platform.core.benchmark.model.AdDasTaskSchemaModel;
import com.kuaishou.ad.das.platform.core.common.pbloader.AdDasBasePbLoader;

import lombok.extern.slf4j.Slf4j;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-01-30
 */
@Slf4j
public abstract class AdDasBenchmarkAbstractCoverter {

    @Autowired
    private AdDasBasePbLoader adDasBasePbLoader;

    @Autowired
    private AdDasBenchmarkCommonConvertHelper commonConvertHelper;

    @Autowired
    private AdDasPlatformPbReflectUtils pbReflectHelper;

    /**
     * 对常规转换后，进行自定义二次处理
     * @param msgBuilder
     * @param dataMap
     */
    protected abstract void convertMore(Message.Builder msgBuilder, Map<String,Object> dataMap);

    protected void defaultConvert(AdDasTaskSchemaModel adDasTaskSchemaModel, AdDasTaskHandleModel adDasTaskHandleModel,
                                  List<Map<String,Object>> mergerData, BiConsumer<Message.Builder, Map<String,Object>> adDasTaskHandleModelConsumer)
            throws NoSuchMethodException, InvocationTargetException, IllegalAccessException, IOException {

        // 字段映射关系
        List<AdDasBenchmarkDb2PbColumnMsgView> columnSchemas = adDasTaskSchemaModel.getColumnSchema();

        // 解析 横级联 反射对象
        List<AdDasBenchmarkCascadeTableView> cascadeTableSchemas = adDasTaskSchemaModel.getCascadeTableSchemas();

        // pb column名 和 级联 schema的映射关系
        Map<String, AdDasBenchmarkCascadeTableView> pbColumn2CascadeTableMap = cascadeTableSchemas
                .stream()
                .collect(Collectors.toMap(AdDasBenchmarkCascadeTableView::getCloumnMsg, cascadeTable -> cascadeTable));

        // pb column名 和 pb类 反射对象
        Map<String, AdDasPbClassReflectModel> dasPbClassReflectModelMap = buildReflectModelMap(cascadeTableSchemas);

        log.info("defaultConvert() dasPbClassReflectModelMap={}", dasPbClassReflectModelMap);
        // pb column名 和 newBuilder 反射方法对象
        Map<String, Method> dasMethodMap = buildReflectMethodMap(dasPbClassReflectModelMap);
        log.info("defaultConvert() dasMethodMap={}", dasMethodMap);

        // 发射生成 Proto 对象
        AdDasPbClassReflectModel reflectModel = reflectModel(adDasTaskSchemaModel.getPbClass());

        Method method = reflectModel.getMethod("newBuilder");

        List<Map<String,Object>> dataObjectMaps = adDasTaskHandleModel.getDataObjectMaps();

        // 内存 merge 扩展点 按照主键Id 进行
        if (!CollectionUtils.isEmpty(mergerData)) {
            doMerge(dataObjectMaps, mergerData);
        }

        AdDasPlatformBenchmarkResultModel dtoResult = doConvert(dataObjectMaps, method,
                adDasTaskHandleModelConsumer, columnSchemas, cascadeTableSchemas,
                pbColumn2CascadeTableMap, dasPbClassReflectModelMap,
                dasMethodMap);
        adDasTaskHandleModel.setDtoResult(dtoResult);
        adDasTaskHandleModel.setPbclassFullName(reflectModel.getPbClassFullName());

        // clear
        adDasTaskHandleModel.getDataObjectMaps().clear();
        adDasTaskHandleModel.setDataObjectMaps(null);
    }

    private Map<String, AdDasPbClassReflectModel> buildReflectModelMap(List<AdDasBenchmarkCascadeTableView> cascadeTableSchemas) {

        Map<String, AdDasPbClassReflectModel> result = Maps.newHashMap();

        for (AdDasBenchmarkCascadeTableView dasBenchmarkCascadeTableView : cascadeTableSchemas)  {

            try {
                AdDasPbClassReflectModel reflectModel = reflectModel(dasBenchmarkCascadeTableView.getCascadePb());
                result.put(dasBenchmarkCascadeTableView.getCloumnMsg(), reflectModel);

            } catch (Exception e) {
                log.error("buildReflectModelMap() occur error! dasBenchmarkCascadeTableView={}", dasBenchmarkCascadeTableView);
                // TODO perf 打点

            }
        }

        return result;
    }

    private Map<String, Method> buildReflectMethodMap(Map<String, AdDasPbClassReflectModel> reflectModelMap) {

        Map<String, Method> result = Maps.newHashMap();

        reflectModelMap.forEach((key, value) -> {

            try {
                Method method = value.getMethod("newBuilder");
                result.put(key, method);
            } catch (Exception e) {
                log.error("buildReflectMethodMap() occur error! key={}, value={}", key, value);
                // TODO perf 打点

            }
        });

        return result;
    }

    private AdDasPbClassReflectModel reflectModel(String pbClassName)
            throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {

        AdDasPbClassReflectModel reflectModel = new AdDasPbClassReflectModel();

        try {
            Class cl = adDasBasePbLoader.loadClassFromCache(buildPbClassName(pbClassName));
            log.info("reflectModel() pbClassName={}, cl={}", pbClassName, cl);
            reflectModel.setCl(cl);

            Method methodgetDes = cl.getMethod("getDescriptor");
            Descriptors.Descriptor descriptor = (Descriptors.Descriptor) methodgetDes.invoke(null, new Object[] {});
            String pbClassFullName = descriptor.getFullName();

            reflectModel.setPbClassFullName(pbClassFullName);
            reflectModel.setPbClassName(pbClassName);
        } catch (Exception e) {
            log.error("reflectModel() occur error!!!, pbClassName={}", pbClassName, e);
            perf(AD_DAS_NAMESPACE_NEW, COMMON_BENCHMARK_LIFE_CYCLE_SUBTAG,
                    "reflectModelError", pbClassName + ":" + e.getMessage())
                    .logstash();
            throw e;
        }

        log.info("reflectModel() pbClassName={}, reflectModel={}", pbClassName, reflectModel);
        return reflectModel;
    }

    protected abstract void doMerge(List<Map<String,Object>> dataObjectMaps, List<Map<String,Object>> mergerData);

    private AdDasPlatformBenchmarkResultModel doConvert(List<Map<String,Object>> dataObjectMaps, Method method,
                                                        BiConsumer<Message.Builder, Map<String,Object>> adDasTaskHandleModelConsumer,
                                                        List<AdDasBenchmarkDb2PbColumnMsgView> columnSchemas,
                                                        List<AdDasBenchmarkCascadeTableView> cascadeTableSchemas,
                                                        Map<String, AdDasBenchmarkCascadeTableView> pbColumn2CascadeTableMap,
                                                        Map<String, AdDasPbClassReflectModel> dasPbClassReflectModelMap,
                                                        Map<String, Method> dasMethodMap) throws IOException {

        AdDasPlatformBenchmarkResultModel result = new AdDasPlatformBenchmarkResultModel();

        List<Message> messageList = Lists.newArrayList();
        for (Map<String,Object> dataMap : dataObjectMaps) {
            try {
                Object obj = method.invoke(null, new Object[] {});
                Message.Builder msgBuilder = (Message.Builder) obj;
                Descriptors.Descriptor descriptor = msgBuilder.getDescriptorForType();
                pbReflectHelper.buildPbMessage(msgBuilder, dataMap, columnSchemas, descriptor);

                // 增加 横级联填充 反射数据
                if (!CollectionUtils.isEmpty(cascadeTableSchemas)) {
                    for (AdDasBenchmarkCascadeTableView cascadeTable : cascadeTableSchemas) {

                        Method methodCascade = dasMethodMap.get(cascadeTable.getCloumnMsg());
                        Object objCascade = methodCascade.invoke(null, new Object[] {});

                        Message.Builder msgBuilderCascade = (Message.Builder) objCascade;

                        AdDasBenchmarkCascadeTableView cascadeTableView = pbColumn2CascadeTableMap.get(cascadeTable.getCloumnMsg());
                        List<AdDasBenchmarkDb2PbColumnMsgView> db2PbColumnMsgCascade = cascadeTableView.getDb2PbColumnMsgViews();
                        // 反射填充 级联类 数据
                        Descriptors.Descriptor descriptorCascade = msgBuilderCascade.getDescriptorForType();

                        pbReflectHelper.buildPbMessage(msgBuilderCascade, dataMap, db2PbColumnMsgCascade, descriptorCascade);

                        // 填充级联类 子数据
                        Descriptors.FieldDescriptor filedDescriptor = descriptor.findFieldByName(cascadeTable.getCloumnMsg());
                        log.info("cascadeTable.getCloumnMsg()={}, filedDescriptor={}, msgBuilderCascade={}",
                                cascadeTable.getCloumnMsg(), filedDescriptor, msgBuilderCascade);
                        msgBuilder.setField(filedDescriptor, msgBuilderCascade.build());
                    }
                }

                if (adDasTaskHandleModelConsumer != null) {
                    adDasTaskHandleModelConsumer.accept(msgBuilder, dataMap);
                }

                messageList.add(msgBuilder.build());
            } catch (Exception e) {
                log.error("doConvert() class={}, buildPbMessage occur error!!!", method, e);
                perf(AD_DAS_NAMESPACE_NEW, COMMON_BENCHMARK_LIFE_CYCLE_SUBTAG,
                        "doConvertError", "eachRun", method.getName() + ":" + e.getMessage())
                        .logstash();
            }
        }

        result.setRecordNum(messageList.size());
        try {
            // message to bytes
            List<byte[]> resultSuccess = commonConvertHelper.convertBytes(messageList, null);
            result.setRecordResults(resultSuccess);
            result.setResultType(0);

        } catch (Exception e) {
            log.error("doConvert() class={}, convertBytes occur error!!!", method, e);
            perf(AD_DAS_NAMESPACE_NEW, COMMON_BENCHMARK_LIFE_CYCLE_SUBTAG,
                    "doConvertError", method.getName() + ":" + e.getMessage())
                    .logstash();
            throw e;
        }

        return result;
    }

}
