package com.kuaishou.ad.das.platform.core.increment.model;


import java.io.Serializable;
import java.util.List;
import java.util.Map;

import com.kuaishou.ad.model.protobuf.AdEnumModel;

import lombok.Data;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-10-30
 */
@Data
public class AdDasIncrementAdapterModel implements Serializable {

    private static final long serialVersionUID = 1235161834564791763L;

    /**
     * 数据唯一id
     */
    private Long id;

    /**
     * 表名
     */
    private String tableName;

    /**
     * 分表
     */
    private String shardTableName;

    /**
     * 时间戳
     */
    private Long timestamp;

    /**
     * 数据 map
     */
    private Map<String, Serializable> row;

    /**
     * 无 accountId 的数据 默认为0L
     */
    private Long accountId;

    /**
     * 数据层级 CAMPAIGN、 UNIT、 CREATIVE
     */
    private String dataLevel;

    /**
     * 消息类型： insert、 update、delete
     */
    private AdEnumModel.AdEnum.AdDasMsgType msgType;

    /**
     * update 的字段
     */
    private List<String> modifyFieldList;

    /**
     * 热点转发时的processTime
     */
    private Long processTime;


    public String getPrimaryId() {
        return shardTableName + ":" + id;
    }
}
