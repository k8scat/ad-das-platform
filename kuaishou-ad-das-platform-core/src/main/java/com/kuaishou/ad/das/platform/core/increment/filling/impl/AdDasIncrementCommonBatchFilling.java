package com.kuaishou.ad.das.platform.core.increment.filling.impl;

import static com.kuaishou.ad.das.platform.utils.constant.AdDasCommonConstant.AD_DAS_COMMON_INCR_LIFE_CYCLE_SUBTAG;
import static com.kuaishou.ad.das.platform.utils.constant.AdDasCommonConstant.AD_DAS_NAMESPACE_NEW;
import static com.kuaishou.framework.util.PerfUtils.perf;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.time.StopWatch;
import org.springframework.stereotype.Service;

import com.kuaishou.ad.das.platform.core.increment.filling.AdDasIncrementBatchFilling;
import com.kuaishou.ad.das.platform.core.increment.model.AdDasCascadeModel;

import lombok.extern.slf4j.Slf4j;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-01-06
 */
@Slf4j
@Service
public class AdDasIncrementCommonBatchFilling
        extends AdDasIncrementAbstractBatchFilling
        implements AdDasIncrementBatchFilling<AdDasCascadeModel> {

    @Override
    public List<AdDasCascadeModel> doBatchFilling(String tableName, String level, Long accountId, List<AdDasCascadeModel> cascadeModels) {

        StopWatch createdAll = StopWatch.createStarted();

        List<AdDasCascadeModel> result =  defaultBatchFilling(tableName, level, accountId, cascadeModels);

        perf(AD_DAS_NAMESPACE_NEW, AD_DAS_COMMON_INCR_LIFE_CYCLE_SUBTAG, "doBatchFilling", tableName, level)
                .count(result.size())
                .micros(createdAll.getTime(TimeUnit.MICROSECONDS))
                .logstash();

        return result;
    }
}
