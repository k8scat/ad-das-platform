package com.kuaishou.ad.das.platform.core.benchmark.helper;

import static com.kuaishou.ad.das.platform.core.utils.AdDasBaseMultiEnvUtils.getDasBenchmarkCommonPath;
import static com.kuaishou.ad.das.platform.utils.dasenum.AdDasBenchmarkStatusEnum.QUERY_DATA_ERROR;
import static com.kuaishou.ad.das.platform.utils.dasenum.AdDasBenchmarkStatusEnum.RUNTIME_SLAVE_ERROR;
import static com.kuaishou.ad.das.platform.utils.dasenum.AdDasBenchmarkStatusEnum.SUCESS_STATUS;
import static com.kuaishou.ad.das.platform.utils.dasenum.AdDasBenchmarkStatusEnum.WRITE_DATA_2_HDFS_ERROR;
import static com.kuaishou.ad.das.platform.utils.kconf.AdDasBaseCommonKconf.adBaseBenchmarkMasterPublishResSleepTime;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.commons.lang3.StringUtils;
import org.apache.curator.framework.recipes.cache.TreeCache;
import org.apache.curator.framework.recipes.cache.TreeCacheEvent;
import org.apache.curator.framework.recipes.cache.TreeCacheListener;
import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.data.Stat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import com.google.common.collect.Maps;
import com.kuaishou.ad.das.platform.core.benchmark.model.AdDasPlatformClientTaskBatchModel;
import com.kuaishou.ad.das.platform.core.benchmark.model.AdDasPlatformClientTaskSingleModel;
import com.kuaishou.ad.das.platform.core.benchmark.model.AdDasPlatformTaskDoneModel;
import com.kuaishou.ad.das.platform.utils.dasenum.AdDasPlatformZkMsgEnum;
import com.kuaishou.ad.das.platform.core.benchmark.model.AdDasPlatformZkMsgModel;
import com.kuaishou.ad.das.platform.core.common.AdDasBenchmarkAbstractZk;
import com.kuaishou.ad.das.platform.core.common.AdDasPlatformServiceConfig;
import com.kuaishou.ad.das.platform.utils.dasenum.AdDasBenchmarkStatusEnum;
import com.kuaishou.framework.util.ObjectMapperUtils;

import lombok.extern.slf4j.Slf4j;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-06-06
 */
@Lazy
@Slf4j
@Component
public class AdDasBenchmarkPlatformClientZkHelper extends AdDasBenchmarkAbstractZk {

    /**
     * 自动重试任务记录， 每次运行 最多重试3次
     */
    protected Map<Integer, AtomicInteger> autoRetryTaskMap = Maps.newConcurrentMap();

    public static final Map<Integer, AdDasBenchmarkStatusEnum> REETRY_ERROR_ENUM = new HashMap() {
        {
            put(QUERY_DATA_ERROR.getStatus(), QUERY_DATA_ERROR);
            put(RUNTIME_SLAVE_ERROR.getStatus(), RUNTIME_SLAVE_ERROR);
            put(WRITE_DATA_2_HDFS_ERROR.getStatus(), WRITE_DATA_2_HDFS_ERROR);
        }
    };

    @Autowired
    protected AdDasPlatformServiceConfig platformServiceConfig;

    /**
     * 初始化 client实例
     * @throws Exception
     */
    public void initBenchmarkClient() throws Exception {

        if (platformServiceConfig.isClient()
                && (StringUtils.isEmpty(platformServiceConfig.getServiceName()) || StringUtils.isEmpty(platformServiceConfig.getBenchmarkEnv()) )) {
            log.info("Client must be configured!!!");
            return;
        }

        this.initiZkConnection();

        // 检查Server父节点是否存在如果不存在就创建
        String serverPath = PLATFORM_BENCHMARK + getDasBenchmarkCommonPath(platformServiceConfig.getBenchmarkEnv()) + SERVERS;
        Stat statServer = curatorFramework.checkExists()
                .forPath(serverPath);
        if (statServer == null) {
            curatorFramework.create().creatingParentsIfNeeded().withMode(CreateMode.PERSISTENT)
                    .forPath(serverPath);
        }
        // 检查task父节点是否存在如果不存在就创建
        String taskPath = PLATFORM_BENCHMARK + getDasBenchmarkCommonPath(platformServiceConfig.getBenchmarkEnv())+ TASKS;
        Stat statTask = curatorFramework.checkExists()
                .forPath(taskPath);
        if (statTask == null) {
            curatorFramework.create().creatingParentsIfNeeded().withMode(CreateMode.PERSISTENT)
                    .forPath(taskPath);
        }
        // 检查client节点时候存在
        String clientPath = PLATFORM_BENCHMARK + getDasBenchmarkCommonPath(platformServiceConfig.getBenchmarkEnv()) + TASKS
                + "/" + platformServiceConfig.getServiceName();
        Stat statClient = curatorFramework.checkExists()
                .forPath(clientPath);
        if (statClient == null) {
            curatorFramework.create().creatingParentsIfNeeded().withMode(CreateMode.PERSISTENT)
                    .forPath(clientPath);
        }

        // 检查client publish节点时候存在
        String clientPublishPath = PLATFORM_BENCHMARK + getDasBenchmarkCommonPath(platformServiceConfig.getBenchmarkEnv()) + TASKS
                + "/" + platformServiceConfig.getServiceName() + CLIENTS_PUB;
        Stat statClientPub = curatorFramework.checkExists()
                .forPath(clientPublishPath);
        if (statClientPub == null) {
            curatorFramework.create().creatingParentsIfNeeded().withMode(CreateMode.PERSISTENT)
                    .forPath(clientPublishPath);
        }

        // 监听自己 管理下的任务数据
        taskTreeCache = new TreeCache(curatorFramework, PLATFORM_BENCHMARK + getDasBenchmarkCommonPath(platformServiceConfig.getBenchmarkEnv()) + TASKS
                + "/" + platformServiceConfig.getServiceName());

        // 统一监听Resource节点的变动
        TreeCacheListener resourceChildrenCacheListener = (client, event1) -> {
            TreeCacheEvent.Type eventType = event1.getType();

            // 只监听更新操作，client不会新增节点
            if (eventType == TreeCacheEvent.Type.NODE_UPDATED) {
                String eventData = new String(event1.getData().getData());
                // DONE不为空，说明有子任务完成了
                if (event1.getData().getPath().endsWith(DONE)
                        && event1.getData().getPath().contains(getDasBenchmarkCommonPath(platformServiceConfig.getBenchmarkEnv()))
                        && !StringUtils.isEmpty(eventData)) {
                    // 如果有新增&更新。得检查下是否有节点完成了任务
                    AdDasPlatformZkMsgModel adDasPlatformZkMsgModel = new AdDasPlatformZkMsgModel();
                    adDasPlatformZkMsgModel.setMsgType(AdDasPlatformZkMsgEnum.TASK_DONE_MSG);
                    adDasPlatformZkMsgModel.setMsgPath(event1.getData().getPath());
                    log.info("adDasPlatformZkMsgModel={}", adDasPlatformZkMsgModel);
                    // 处理 client 发布的任务数据
                    bufferTrigger.enqueue(adDasPlatformZkMsgModel);
                }
            }
        };
        taskTreeCache.getListenable().addListener(resourceChildrenCacheListener);
        taskTreeCache.start();
    }

    /**
     * 初始化发布任务
     * @param clientTaskModel
     * @return
     */
    public void clientPublishTask(AdDasPlatformClientTaskSingleModel clientTaskModel) {

        Integer taskNum = clientTaskModel.getTaskNum();
        String taskContent = ObjectMapperUtils.toJSON(clientTaskModel);
        try {

            String taskNumPath = PLATFORM_BENCHMARK + getDasBenchmarkCommonPath(platformServiceConfig.getBenchmarkEnv()) + TASKS
                    + "/" + clientTaskModel.getClientName() + TASK + "/" + taskNum.toString();
            Stat stat = curatorFramework.checkExists()
                    .forPath(taskNumPath);
            if (stat == null) {
                // 没有那就创建一个
                curatorFramework.create().creatingParentsIfNeeded()
                        .forPath(taskNumPath);
            }
            String taskContentPath = PLATFORM_BENCHMARK + getDasBenchmarkCommonPath(platformServiceConfig.getBenchmarkEnv()) + TASKS
                    + "/" + clientTaskModel.getClientName() + TASK + "/" + taskNum.toString() + TASKCONTENT;
            Stat taskContentStat = curatorFramework.checkExists()
                    .forPath(taskContentPath);
            if (taskContentStat == null) {
                curatorFramework.create()
                        .forPath(taskContentPath, taskContent.getBytes());
            } else {
                curatorFramework.setData()
                        .forPath(taskContentPath, taskContent.getBytes());
            }
            String donePath = PLATFORM_BENCHMARK + getDasBenchmarkCommonPath(platformServiceConfig.getBenchmarkEnv()) + TASKS
                    + "/" + clientTaskModel.getClientName() + TASK + "/" + taskNum.toString() + DONE;
            Stat donestat = curatorFramework.checkExists().forPath(donePath);
            if (donestat == null) {
                curatorFramework.create()
                        .forPath(donePath, "".getBytes());
            } else {
                curatorFramework.setData()
                        .forPath(donePath, "".getBytes());
            }

            String tokenPath = PLATFORM_BENCHMARK + getDasBenchmarkCommonPath(platformServiceConfig.getBenchmarkEnv()) + TASKS
                    + "/" + clientTaskModel.getClientName() + TASK + "/" + taskNum.toString() + TOKEN;
            Stat tokenStat = curatorFramework.checkExists()
                    .forPath(tokenPath);
            if (tokenStat == null) {
                curatorFramework.create()
                        .forPath(tokenPath, "".getBytes());
            }

            String assignPath = PLATFORM_BENCHMARK + getDasBenchmarkCommonPath(platformServiceConfig.getBenchmarkEnv()) + TASKS
                    + "/" + clientTaskModel.getClientName() + TASK + "/" + taskNum.toString() + ASSIGN;
            Stat assignStat = curatorFramework.checkExists()
                    .forPath(assignPath);
            if (assignStat == null) {
                curatorFramework.create()
                        .forPath(assignPath, "".getBytes());
            }

            // 控制发布任务速度
            Thread.sleep(adBaseBenchmarkMasterPublishResSleepTime.get());
        } catch (Exception e) {
            // TODO perf 打点

            log.error("clientPublishTask() ocurr error! clientTaskModel={}", clientTaskModel, e);
        }
    }

    /**
     * 发布任务
     * @param clientTaskModel
     * @return
     */
    public boolean clientPublish(AdDasPlatformClientTaskBatchModel clientTaskModel) throws Exception {

        String clientTaskModelStr = ObjectMapperUtils.toJSON(clientTaskModel);
        log.info("clientPublish() clientTaskModelStr={}", clientTaskModelStr);
        curatorFramework.setData().forPath(PLATFORM_BENCHMARK + getDasBenchmarkCommonPath(platformServiceConfig.getBenchmarkEnv()) + TASKS
                + "/" + platformServiceConfig.getServiceName() + CLIENTS_PUB, clientTaskModelStr.getBytes());

        return true;
    }

    /**
     * 清理结束后的信息
     * @param clientTaskModel
     */
    public void cleanTaskMsg(AdDasPlatformClientTaskBatchModel clientTaskModel) {

        clientTaskModel.getTaskIds().stream()
                .filter(taskNum -> taskNum != null && taskNum >= 0)
                .forEach(taskNum -> {

                    try {
                        // 先清空 taskContent
                        curatorFramework.setData()
                                .forPath(PLATFORM_BENCHMARK + getDasBenchmarkCommonPath(platformServiceConfig.getBenchmarkEnv()) + TASKS
                                                + "/" + clientTaskModel.getClientName() + TASK + "/" + taskNum.toString()  + TASKCONTENT,
                                        "".getBytes());
                        log.info("【Notice】 cleanTaskMsg() resourceId={}", taskNum);
                        curatorFramework.setData()
                                .forPath(PLATFORM_BENCHMARK +getDasBenchmarkCommonPath(platformServiceConfig.getBenchmarkEnv()) + TASKS
                                                + "/" + clientTaskModel.getClientName() + TASK + "/" + taskNum.toString()  + DONE,
                                        "".getBytes());

                        curatorFramework.setData()
                                .forPath(PLATFORM_BENCHMARK +getDasBenchmarkCommonPath(platformServiceConfig.getBenchmarkEnv()) + TASKS
                                                + "/" + clientTaskModel.getClientName() + TASK + "/" + taskNum.toString()  + ASSIGN,
                                        "".getBytes());

                        curatorFramework.setData().forPath(PLATFORM_BENCHMARK + getDasBenchmarkCommonPath(platformServiceConfig.getBenchmarkEnv()) + TASKS
                                + "/" + platformServiceConfig.getServiceName() + CLIENTS_PUB, "".getBytes());

                    } catch (Exception e) {
                        log.error("cleanTaskMsg() ocurr error: resourceId={}", taskNum);
                    }
                });

        // 清理任务重试记录
        autoRetryTaskMap.clear();
    }

    /**
     * 检查并创建节点
     * @param path
     * @throws Exception
     */
    public void checkRecordNodeExist(String path) throws Exception {
        Stat stat = curatorFramework.checkExists()
                .forPath(path);
        if (stat == null) {
            // 没有那就创建一个
            curatorFramework.create().creatingParentsIfNeeded()
                    .forPath(path);
        }

        curatorFramework.setData()
                .forPath(path, "".getBytes());
    }

    /**
     * 获取指定节点的数据
     * @param path
     * @return
     * @throws Exception
     */
    public String getPathData(String path) throws Exception {

        String dataStr =
                new String(curatorFramework.getData().forPath(path));

        return dataStr;
    }

    public void recordDumpData(AdDasPlatformClientTaskBatchModel clientTaskModel, String dataSizeCheckResultNowStr) throws Exception {
        //  zk 记录节点 为永久节点， 不存在则创建
        String recordPath = PLATFORM_BENCHMARK + getDasBenchmarkCommonPath(platformServiceConfig.getBenchmarkEnv()) + TASKS + "/" + clientTaskModel.getClientName() + MASTER_DATA_RECORD;
        Stat stat = curatorFramework.checkExists()
                .forPath(recordPath);
        if (stat == null) {
            // 没有那就创建一个
            curatorFramework.create().creatingParentsIfNeeded()
                    .forPath(recordPath);
        }
        curatorFramework.setData()
                .forPath(recordPath, dataSizeCheckResultNowStr.getBytes());
    }

    @Override
    protected void consume(ConcurrentLinkedQueue<AdDasPlatformZkMsgModel> adDasPlatformZkMsgModels) {

        adDasPlatformZkMsgModels.stream().forEach(adDasPlatformZkMsgModel -> {

            if (AdDasPlatformZkMsgEnum.TASK_DONE_MSG == adDasPlatformZkMsgModel.getMsgType()) {
                // FIXME 校验任务done 情况， 已完成的任务， 清理出本地缓存
                String contentData = adDasPlatformZkMsgModel.getContentData();
                if (!StringUtils.isEmpty(contentData)) {
                    AdDasPlatformTaskDoneModel platformTaskDoneModel = ObjectMapperUtils
                            .fromJSON(contentData, AdDasPlatformTaskDoneModel.class);
                    log.info("platformTaskDoneModel={}");

                    // 判断任务是否需要重试
                    if (SUCESS_STATUS.getStatus() != platformTaskDoneModel.getStatus()
                            && REETRY_ERROR_ENUM.containsKey(platformTaskDoneModel.getStatus())) {

                    }
                }
            }
        });
    }
}
