package com.kuaishou.ad.das.platform.core.benchmark.model;

import java.lang.reflect.Method;

import lombok.Data;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-01-31
 */
@Data
public class AdDasPbClassReflectModel {

    private String pbClassName;

    private String pbClassFullName;

    private Class cl;


    public Method getMethod(String methodName) throws NoSuchMethodException {

        return  cl.getMethod(methodName);
    }
}
