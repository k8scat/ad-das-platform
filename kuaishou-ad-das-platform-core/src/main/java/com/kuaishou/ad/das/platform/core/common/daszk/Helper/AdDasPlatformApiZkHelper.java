package com.kuaishou.ad.das.platform.core.common.daszk.Helper;

import static com.kuaishou.ad.das.platform.core.utils.AdDasBaseMultiEnvUtils.getDasBenchmarkCommonPath;

import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.data.Stat;
import org.springframework.stereotype.Component;

import com.kuaishou.ad.das.platform.core.common.AdDasPlatformAbstractZkHelper;

import lombok.extern.slf4j.Slf4j;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-12-08
 */
@Slf4j
@Component
public class AdDasPlatformApiZkHelper extends AdDasPlatformAbstractZkHelper {

    /**
     * 初始化平台 zk
     * @throws Exception
     */
    public void dasApiZkInit() throws Exception {

        initCommonPlatformBase();
        initApiClient();
    }

    /**
     * 初始化注册 api client
     * @throws Exception
     */
    public void initApiClient() throws Exception {

        // 检查Server父节点是否存在如果不存在就创建
        String serverPath = PLATFORM_BENCHMARK + getDasBenchmarkCommonPath(platformServiceConfig.getBenchmarkEnv()) + SERVERS;
        Stat statServer = curatorFramework.checkExists()
                .forPath(serverPath);
        if (statServer == null) {
            curatorFramework.create().creatingParentsIfNeeded().withMode(CreateMode.PERSISTENT)
                    .forPath(serverPath);
        }
        // 检查task父节点是否存在如果不存在就创建
        String taskPath = PLATFORM_BENCHMARK + getDasBenchmarkCommonPath(platformServiceConfig.getBenchmarkEnv())+ TASKS;
        Stat statTask = curatorFramework.checkExists()
                .forPath(taskPath);
        if (statTask == null) {
            curatorFramework.create().creatingParentsIfNeeded().withMode(CreateMode.PERSISTENT)
                    .forPath(taskPath);
        }
        // 检查client节点时候存在
        String clientPath = PLATFORM_BENCHMARK + getDasBenchmarkCommonPath(platformServiceConfig.getBenchmarkEnv()) + TASKS
                + "/" + platformServiceConfig.getServiceName();
        Stat statClient = curatorFramework.checkExists()
                .forPath(clientPath);
        if (statClient == null) {
            curatorFramework.create().creatingParentsIfNeeded().withMode(CreateMode.PERSISTENT)
                    .forPath(clientPath);
        }

        // 检查client publish节点时候存在
        String clientPublishPath = PLATFORM_BENCHMARK + getDasBenchmarkCommonPath(platformServiceConfig.getBenchmarkEnv()) + TASKS
                + "/" + platformServiceConfig.getServiceName() + CLIENTS_PUB;
        Stat statClientPub = curatorFramework.checkExists()
                .forPath(clientPublishPath);
        if (statClientPub == null) {
            curatorFramework.create().creatingParentsIfNeeded().withMode(CreateMode.PERSISTENT)
                    .forPath(clientPublishPath);
        }
    }

}
