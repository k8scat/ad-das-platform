package com.kuaishou.ad.das.platform.core.benchmark.server.merger.impl;

import java.util.List;
import java.util.Map;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import com.kuaishou.ad.das.platform.core.benchmark.server.merger.AdDasBenchmarkMerger;
import com.kuaishou.ad.das.platform.core.benchmark.model.AdDasTaskHandleModel;
import com.kuaishou.ad.das.platform.core.benchmark.model.AdDasTaskSchemaModel;

import lombok.extern.slf4j.Slf4j;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-02-15
 */
@Lazy
@Slf4j
@Service
public class AdDasBenchmarkCommonMerger implements AdDasBenchmarkMerger {

    @Override
    public List<Map<String, Object>> queryMergerData(AdDasTaskSchemaModel adDasTaskSchemaModel, AdDasTaskHandleModel taskHandleModel) {
        return null;
    }
}
