package com.kuaishou.ad.das.platform.core.utils;

import static java.lang.Math.abs;
import static org.apache.commons.collections.MapUtils.getString;

import java.util.Map;

import com.google.common.base.Charsets;
import com.google.common.hash.Hashing;
import com.kuaishou.ad.model.protobuf.AdIncModel;
import com.kuaishou.infra.databus.common.domain.mysql.MysqlEventColumn;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-07-07
 */
public class AdDasIncrementUtils {

    /**
     * 识别 变化的 字段
     * @param key
     * @param rowColumnMaps
     * @param rowColumnMapsBefore
     * @return
     */
    public static String getModifyField(String key, Map<String, MysqlEventColumn> rowColumnMaps,
                                        Map<String, MysqlEventColumn> rowColumnMapsBefore) {

        long afterDataVersion = getDataValueVersion(getString(rowColumnMaps, key));
        long beforeDataVersion = getDataValueVersion(getString(rowColumnMapsBefore, key));

        if (afterDataVersion != beforeDataVersion) {
            return key;
        }

        return null;
    }

    public static long getDataValueVersion(String dataValue) {
        return abs(Hashing.md5()
                .newHasher()
                .putString(noneToEmpty(dataValue), Charsets.UTF_8)
                .hash()
                .asLong());
    }

    public static String noneToEmpty(String str) {
        return org.springframework.util.StringUtils.isEmpty(str) ? "" : str;
    }

    public static Long getAccountId(Map<String, MysqlEventColumn> rowColumnMaps) {

        if (rowColumnMaps.containsKey("account_id")) {
            MysqlEventColumn mysqlEventColumn = rowColumnMaps.get("account_id");
            return ((Number) mysqlEventColumn.getData()).longValue();
        }
        return 0L;
    }

    public static AdIncModel.AdInstanceField getAdInstanceFieldForKbus(String key, Object value) {
        AdIncModel.AdInstanceField.Builder builder = AdIncModel.AdInstanceField.newBuilder().setName(key);
        Object o = value;
        if (o == null) {
            //不设置值
        } else if (o instanceof Double) {
            builder.setDoubleValue((Double) o);
        } else if (o instanceof Float) {
            builder.setDoubleValue((Float) o);
        } else if (o instanceof Number) {
            builder.setIntValue(((Number) o).longValue());
        } else if (o instanceof byte[]) {
            builder.setStringValue(new String((byte[]) o));
        } else {
            builder.setStringValue(o.toString());
        }
        return builder.build();
    }


}
