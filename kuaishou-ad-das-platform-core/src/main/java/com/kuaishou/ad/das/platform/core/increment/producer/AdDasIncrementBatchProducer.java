package com.kuaishou.ad.das.platform.core.increment.producer;

import java.util.List;

import com.kuaishou.ad.das.platform.core.increment.model.AdDasCascadeModel;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-01-05
 */
public interface AdDasIncrementBatchProducer {

    /**
     * 最终数据发送Kafka
     * @param tableName
     * @param adDasCascadeModels
     */
    void doBatchSendKafka(String tableName, Long accountId, List<AdDasCascadeModel> adDasCascadeModels);
}
