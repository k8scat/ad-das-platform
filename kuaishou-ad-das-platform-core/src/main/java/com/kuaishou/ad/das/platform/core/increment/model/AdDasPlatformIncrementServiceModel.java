package com.kuaishou.ad.das.platform.core.increment.model;

import java.io.Serializable;
import java.util.Map;

import com.google.protobuf.ProtocolMessageEnum;
import com.kuaishou.ad.das.platform.utils.model.kconf.AdDasIncrementFillingTable;
import com.kuaishou.ad.model.protobuf.AdEnumModel;

import lombok.Data;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-06-05
 */
@Data
public class AdDasPlatformIncrementServiceModel implements Serializable {

    private static final long serialVersionUID = 4560563331337408463L;

    /**
     * 服务名称
     */
    private String serviceName;

    /**
     * 数据源
     */
    private String dataSource;

    /**
     * 消费组
     */
    private String consumerGroup;

    /**
     * 数据 topic
     */
    private String topic;

    /**
     * 消费表schema数据
     */
    private AdDasPlatformIncrementTableSchemaModel incrementTableSchemaModel;

    /**
     * 数据创建时间
     */
    private Long createTime;

    /**
     * 数据更新时间
     */
    private Long updateTime;


    /**
     * 增量入口过滤 table
     * @param tableName
     * @return
     */
    public boolean filterTable(String tableName) {
        return incrementTableSchemaModel.getTable2TypeMap().containsKey(tableName);
    }

    /**
     * 获取表对应的level
     * @param tableName
     * @return
     */
    public String getTableLevelMap(String tableName) {
        return incrementTableSchemaModel.getTable2LevelMap().get(tableName);
    }

    /**
     * 获取增量横级联关系
     * @return
     */
    public AdDasIncrementFillingTable getFillingTable() {
        return incrementTableSchemaModel.getIncrementFillingTable();
    }

    /**
     * 获取 level 2 type map
     * @return
     */
    public Map<String, ProtocolMessageEnum> getLevel2TypeMap() {
        return incrementTableSchemaModel.getTableLevel2TypeMap();
    }

    /**
     * 获取表对应的 type
     * @param tableName
     * @return
     */
    public ProtocolMessageEnum getTableType(String tableName) {
        return incrementTableSchemaModel.getTable2TypeMap().get(tableName);
    }

    /**
     * 获取 表 schema
     * @param tableName
     * @return
     */
    public AdDasPlatformIncrementTableColumnModel getTableSchema(String tableName) {
        return incrementTableSchemaModel.getTableColumnMap().get(tableName);
    }

    /**
     * 获取 横级联填充 表
     * @return
     */
    public Map<String, String> getFillingTableSql(){
        return incrementTableSchemaModel.getTable2FillingSqlMap();
    }
}
