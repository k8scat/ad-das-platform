package com.kuaishou.ad.das.platform.core.benchmark.model;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import lombok.Data;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-07-05
 */
@Data
public class AdDasPlatformWorkMasterZkModel implements Serializable {

    private static final long serialVersionUID = -388614375173162488L;

    /**
     * 服务名称
     */
    private String serviceName;

    /**
     * 基准集群环境
     */
    private String benchmarkEnv;

    /**
     * 服务环境
     */
    private String serviceEnv;

    /**
     * 管理的 worker 的信息
     */
    private Map<String, List<Integer>> workerMsg;

}
