package com.kuaishou.ad.das.platform.core.benchmark.model;

import java.io.Serializable;

import lombok.Data;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-07-05
 */
@Data
public class AdDasPlatformClientTaskSingleModel implements Serializable {

    private static final long serialVersionUID = 2898343032918803937L;

    private String clientName;

    private Long timestamp;

    private String hdfsPath;

    private Integer taskNum;
}

