package com.kuaishou.ad.das.platform.core.benchmark.helper;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import com.google.common.collect.Lists;
import com.kuaishou.ad.das.dal.datasource.AdCoreDataSource;
import com.kuaishou.ad.das.platform.core.benchmark.model.AdDasPlatformClientTaskBatchModel;
import com.kuaishou.ad.das.platform.core.benchmark.model.AdDasTaskHandleModel;
import com.kuaishou.ad.das.platform.core.benchmark.model.AdDasTaskSchemaModel;
import com.kuaishou.ad.das.platform.dal.commonquery.AdDasPlatformCommonQuery;

import lombok.extern.slf4j.Slf4j;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-01-30
 */
@Slf4j
@Service
public class AdDasBenchmarkCommonQueryHelper {

    protected JdbcTemplate dasJdbcTemplate = AdCoreDataSource.adDas.read().getJdbcTemplate();

    @Autowired
    private AdDasPlatformCommonQuery adDasBenchmarkCommonQuery;

    /**
     * 查询  Client 管理的任务信息
     * @param clientName
     * @return
     */
    public AdDasPlatformClientTaskBatchModel queryClientTaskModel(String clientName) {

        // 先根据基准服务名称 获取 benchmakr_id
        String benchmarkSql = "select id,hdfs_path from ad_das_benchmark where benchmark_name=?";
        List<Object> benchmarkParams = Lists.newArrayList();
        benchmarkParams.add(clientName);
        Map<String, Object> benchmarkResult = queryMapWithRetry(dasJdbcTemplate, benchmarkSql, benchmarkParams);
        log.info("queryClientTaskModel() benchmarkSql={}, benchmarkResult={}", benchmarkSql, benchmarkResult);
        Long benchmark_id = (Long) benchmarkResult.getOrDefault("id", 0L);
        String hdfsPath = (String)  benchmarkResult.getOrDefault("hdfs_path", "");

        AdDasPlatformClientTaskBatchModel adDasPlatformClientTaskModel = new AdDasPlatformClientTaskBatchModel();
        adDasPlatformClientTaskModel.setClientName(clientName);
        String sql = "select task_number from ad_das_benchmark_task where benchmark_id=? ";

        List<Object> params = Lists.newArrayList();
        params.add(benchmark_id);

        List<Map<String, Object>> result = queryWithRetry(dasJdbcTemplate, sql, params);
        log.info("queryClientTaskModel() sql={}, params={}, result={}",
                sql, params, result);
        adDasPlatformClientTaskModel.buildClientTask(result);
        adDasPlatformClientTaskModel.setPublishType(1);

        adDasPlatformClientTaskModel.setHdfsPath(hdfsPath);
        return adDasPlatformClientTaskModel;
    }

    /**
     * 查询 任务 元数据 信息
     * @param taskDataMap
     * @return
     */
    public AdDasTaskSchemaModel queryTaskSchemaModel(Map<String, Object> taskDataMap) {

        // query ad_das_benchmark_common_detail 表
        Map<String, Object> taskCommondetailMap = queryTaskCommonDetail((Long) taskDataMap.get("benchmark_id"),
                (String) taskDataMap.get("main_table"));

        if (CollectionUtils.isEmpty(taskDataMap) || CollectionUtils.isEmpty(taskDataMap)) {
            throw new RuntimeException("queryTaskSchemaModel() data is empty!");
        }

        AdDasTaskSchemaModel adDasTaskSchemaModel = new AdDasTaskSchemaModel();

        adDasTaskSchemaModel.buildTaskData(taskDataMap);
        adDasTaskSchemaModel.buildTaskCommonDetailData(taskCommondetailMap);

        return adDasTaskSchemaModel;
    }

    /**
     * 查询任务列表
     *
     * @param taskNum
     * @return
     */
    public List<AdDasTaskHandleModel> queryTaskSchemaList(Integer taskNum) {

        String sql = "select * from ad_das_benchmark_task where task_number=? ";

        List<Object> params = Lists.newArrayList();
        params.add(taskNum);

        List<Map<String, Object>> dataResult = queryWithRetry(dasJdbcTemplate, sql, params);

        List<AdDasTaskHandleModel> result = dataResult.stream()
                .filter(dataMap -> dataMap.containsKey("task_name")
                        && dataMap.containsKey("main_table"))
                .map(dataMap -> {
                    AdDasTaskHandleModel adDasTaskHandleModel = new AdDasTaskHandleModel();
                    String taskName = (String) dataMap.get("task_name");
                    String tableName = (String) dataMap.get("main_table");

                    // 查询 taskSchema
                    AdDasTaskSchemaModel adDasTaskSchemaModel = queryTaskSchemaModel(dataMap);
                    adDasTaskSchemaModel.setTaskNum(taskNum);
                    adDasTaskSchemaModel.setTaskName(taskName);

                    adDasTaskHandleModel.setTaskNum(taskNum);
                    adDasTaskHandleModel.setTaskName(taskName);
                    adDasTaskHandleModel.setTableName(tableName);

                    adDasTaskHandleModel.setAdDasTaskSchemaModel(adDasTaskSchemaModel);
                    log.info("queryTaskSchemaList() dataMap={}, adDasTaskHandleModel={}", dataMap, adDasTaskHandleModel);
                    return adDasTaskHandleModel;
                })
                .filter(adDasTaskHandleModel -> adDasTaskHandleModel != null
                        && !StringUtils.isEmpty(adDasTaskHandleModel.getTaskName()))
                .collect(Collectors.toList());

        return result;
    }

    /**
     * 查询任务信息
     * @param taskId
     * @param taskName
     * @return
     */
    public Map<String, Object> queryTask(Integer taskId, String taskName) {

        String sql = "select * from ad_das_benchmark_task "
                + "where task_number=? and task_name=? ";

        List<Object> params = Lists.newArrayList();
        params.add(taskId);
        params.add(taskName);

        Map<String, Object> result = queryMapWithRetry(dasJdbcTemplate, sql, params);

        return result;
    }

    /**
     * 查询任务公共详情信息
     * @param benchmarkId
     * @param mainTable
     * @return
     */
    public Map<String, Object> queryTaskCommonDetail(Long benchmarkId, String mainTable) {

        String sql = "select * from ad_das_benchmark_task_common_detail "
                + "where benchmark_id=? and main_table=? ";

        List<Object> params = Lists.newArrayList();
        params.add(benchmarkId);
        params.add(mainTable);

        Map<String, Object> result = queryMapWithRetry(dasJdbcTemplate, sql, params);

        return result;
    }


    /**
     * 查询 db with retry
     * @param jdbcTemplate
     * @param sql
     * @param params
     * @return
     */
    public List<Map<String, Object>> queryWithRetry(JdbcTemplate jdbcTemplate,
                                                    String sql,
                                                    List<Object> params) {
        List<Map<String, Object>> result;

        try {

            result =  adDasBenchmarkCommonQuery.queryBySql(jdbcTemplate, sql, params);
        } catch (Exception e) {

            log.error("queryWithRetry() occur error! sql={}, params={}", sql, params, e);
            result =  adDasBenchmarkCommonQuery.queryBySql(jdbcTemplate, sql, params);
        }

        return result;
    }

    /**
     * 查询单个数据 with retry
     * @param jdbcTemplate
     * @param sql
     * @param params
     * @return
     */
    public Map<String, Object> queryMapWithRetry(JdbcTemplate jdbcTemplate,
                                                 String sql,
                                                 List<Object> params) {

        Map<String, Object> result;

        try {

            result =  adDasBenchmarkCommonQuery.queryMapBySql(jdbcTemplate, sql, params);
        } catch (Exception e) {

            log.error("queryMapWithRetry() occur error! sql={}, params={}", sql, params, e);
            result =  adDasBenchmarkCommonQuery.queryMapBySql(jdbcTemplate, sql, params);
        }

        return result;
    }

}
