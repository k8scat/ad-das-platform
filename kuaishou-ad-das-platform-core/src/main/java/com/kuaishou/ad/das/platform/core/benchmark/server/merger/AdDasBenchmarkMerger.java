package com.kuaishou.ad.das.platform.core.benchmark.server.merger;

import java.util.List;
import java.util.Map;

import com.kuaishou.ad.das.platform.core.benchmark.model.AdDasTaskHandleModel;
import com.kuaishou.ad.das.platform.core.benchmark.model.AdDasTaskSchemaModel;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-02-14
 */
public interface AdDasBenchmarkMerger {

    /**
     * 跨库查询 merge 数据
     * @param adDasTaskSchemaModel
     * @param taskHandleModel
     * @return
     */
    List<Map<String,Object>> queryMergerData(AdDasTaskSchemaModel adDasTaskSchemaModel, AdDasTaskHandleModel taskHandleModel);
}
