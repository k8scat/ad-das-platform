package com.kuaishou.ad.das.platform.core.benchmark.server.fetcher;

import com.kuaishou.ad.das.platform.core.benchmark.model.AdDasTaskHandleModel;
import com.kuaishou.ad.das.platform.core.benchmark.model.AdDasTaskSchemaModel;


/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-01-29
 */
public interface AdDasBenchmarkFetcher {

    /**
     *
     * 1、根据 fetchType 选择取数方式
     * 2、 fetch data set to dataObjectMaps
     *
     * @param taskHandleModel
     * @return
     */
    AdDasTaskHandleModel fetch(AdDasTaskSchemaModel adDasTaskSchemaModel, AdDasTaskHandleModel taskHandleModel);

}
