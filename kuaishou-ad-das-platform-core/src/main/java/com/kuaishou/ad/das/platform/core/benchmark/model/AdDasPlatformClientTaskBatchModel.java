package com.kuaishou.ad.das.platform.core.benchmark.model;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import org.apache.commons.compress.utils.Lists;

import lombok.Data;

/**
 * 任务model
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-06-06
 */
@Data
public class AdDasPlatformClientTaskBatchModel implements Serializable {

    private static final long serialVersionUID = 7687017575150022294L;

    private String clientName;

    private Long timestamp;

    private String hdfsPath;

    /**
     * 正常一次运行发布的 taskId
     */
    private List<Integer> taskIds = Lists.newArrayList();

    /**
     * client检测， 需要进行重试的 taskIds
     */
    private List<Integer> retryTaskIds = Lists.newArrayList();

    /**
     * 任务创建时间
     */
    private Long createTime ;

    /**
     * 基准流任务状态 -1-初始化 0-运行中 1-运行成功 2-运行失败
     *  FIXME : AdDasClientTaskRunEnum
     */
    private Integer clientStatus;

    /**
     * 发布事件类型： 1-初始化发布 2-重试任务发布
     */
    private Integer publishType = 1;

    /**
     * 重试次数
     */
    private Integer retryTimes=0;

    public void buildClientTask(List<Map<String, Object>> result) {

        for (Map<String, Object> data : result) {
            if (data != null) {
                Object taskData = data.get("task_number");

                if (taskData != null) {
                    Integer taskId = ((Number) taskData).intValue();
                    taskIds.add(taskId);
                }
            }
        }
    }
}
