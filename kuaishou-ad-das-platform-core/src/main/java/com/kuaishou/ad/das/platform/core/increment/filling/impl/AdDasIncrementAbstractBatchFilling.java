package com.kuaishou.ad.das.platform.core.increment.filling.impl;

import static com.google.common.util.concurrent.MoreExecutors.shutdownAndAwaitTermination;
import static com.kuaishou.ad.das.platform.core.increment.lifecycle.AdDasKbusIncrementFactory.getKbusBatchTransfer;
import static com.kuaishou.ad.das.platform.core.increment.lifecycle.AdDasKbusIncrementFactory.judgeSpKbusBatchTable;
import static com.kuaishou.ad.das.platform.core.utils.AdDasIncrementUtils.getAdInstanceFieldForKbus;
import static com.kuaishou.ad.das.platform.dal.datasource.AdDataSourceUtil.getDasIncremnetJdbcTemplate;
import static com.kuaishou.ad.das.platform.utils.constant.AdDasCommonConstant.AD_DAS_COMMON_INCR_LIFE_CYCLE_SUBTAG;
import static com.kuaishou.ad.das.platform.utils.constant.AdDasCommonConstant.AD_DAS_NAMESPACE_NEW;
import static com.kuaishou.ad.das.platform.utils.constant.AdDasCommonConstant.defaultAdCoreShardCount;
import static com.kuaishou.ad.das.platform.utils.kconf.AdDasPlatformKconf.AD_DAS_INCREMENT_BATCH_FILLING_THREAD_COUNT;
import static com.kuaishou.framework.util.PerfUtils.perf;
import static java.util.concurrent.TimeUnit.DAYS;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.time.StopWatch;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;

import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.protobuf.Message;
import com.kuaishou.ad.das.platform.core.increment.helper.AdDasIncrementCommonQueryHelper;
import com.kuaishou.ad.das.platform.core.increment.helper.AdDasPlatformIncrementCommonHelper;
import com.kuaishou.ad.das.platform.core.increment.model.AdDasCascadeModel;
import com.kuaishou.ad.das.platform.core.increment.transfer.AdDasKbusBatchTransfer;
import com.kuaishou.ad.das.platform.core.increment.transfer.impl.AdDasKbusCommonBatchTransfer;
import com.kuaishou.ad.das.platform.utils.model.kconf.AdDasIncrementFillingTable;
import com.kuaishou.framework.concurrent.DynamicThreadExecutor;
import com.kuaishou.framework.util.HostInfo;
import com.kuaishou.framework.util.PerfUtils;
import com.kuaishou.infra.framework.common.util.TermHelper;

import lombok.extern.slf4j.Slf4j;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-01-05
 */
@Slf4j
public abstract class AdDasIncrementAbstractBatchFilling {

    protected static DynamicThreadExecutor incrementBatchFillingExecutor;

    private static volatile Integer incrementBatchFillingStarted = 0;

    @Autowired
    private AdDasIncrementCommonQueryHelper adDasIncrementCommonQueryHelper;

    @Autowired
    private AdDasKbusCommonBatchTransfer commonBatchTransfer;

    @Autowired
    private AdDasPlatformIncrementCommonHelper adDasPlatformIncrementCommonHelper;

    private AdDasKbusBatchTransfer choseTransfer(String tableName) {
        AdDasKbusBatchTransfer adDasKbusBatchTransfer;
        if (judgeSpKbusBatchTable(tableName)) {
            adDasKbusBatchTransfer = getKbusBatchTransfer(tableName);
        } else {
            // 默认transfer
            adDasKbusBatchTransfer = commonBatchTransfer;
        }

        return adDasKbusBatchTransfer;
    }

    protected List<AdDasCascadeModel> defaultBatchFilling(String tableName, String level,
                                                          Long accountId, List<AdDasCascadeModel> cascadeModels) {

        // 从内存缓存中 获取 table 对应的级联关系
        AdDasIncrementFillingTable incrementFillingTable = adDasPlatformIncrementCommonHelper.getFillingTable();

        if (HostInfo.debugHost()) {
            log.info("defaultBatchFilling() incrementFillingTable={}, tableName={}, level={}",
                    incrementFillingTable, tableName, level);
        }

        // 根据 Level 查 横级联配置
        if (!incrementFillingTable.judgeLevel(level)) {
            return cascadeModels;
        }

        // 根据Table 判断是否需要 查 横级联配置
        if (!incrementFillingTable.judgeLevelTable(level, tableName)) {
            return cascadeModels;
        }

        // 根据 tableName 去 取需要横级联的 表
        Set<String> fillingTableSet = incrementFillingTable.getLevelTableNameSet(level, tableName);

        if (CollectionUtils.isEmpty(fillingTableSet)) {
            PerfUtils.perf(AD_DAS_NAMESPACE_NEW, AD_DAS_COMMON_INCR_LIFE_CYCLE_SUBTAG, "lifeCycleError",
                    "batchFillingTableSetEmpty", level, tableName)
                    .logstash();
            log.error("defaultBatchFilling() fillingTableSet is empty!!! tableName={}, level={}, fillingTableSet={}",
                    tableName, level, fillingTableSet);

            return cascadeModels;
        }

        if (HostInfo.debugHost()) {
            log.info("doBatchFilling() fillingTableSet={}", fillingTableSet);
        }

        // 根据 横级联的 表 去找表对应的 sql
        Map<String, String> fillingTable2SqlMap = adDasPlatformIncrementCommonHelper.buildFillingTable2SqlMap();

        if (CollectionUtils.isEmpty(fillingTable2SqlMap)) {

            PerfUtils.perf(AD_DAS_NAMESPACE_NEW, AD_DAS_COMMON_INCR_LIFE_CYCLE_SUBTAG, "lifeCycleError",
                    "batchFillingTable2SqlEmpty", level, tableName)
                    .logstash();
            log.error("defaultBatchFilling() fillingTable2SqlMap is empty!!! tableName={}, level={}, fillingTable2SqlMap={}",
                    tableName, level, fillingTable2SqlMap);
            return cascadeModels;
        }

        if (HostInfo.debugHost()) {
            log.info("doBatchFilling() fillingTable2SqlMap={}", fillingTable2SqlMap);
        }

        // FIXME 并发取数
        Map<String, List<Map<String, Object>>> queryDbDataMap = queryDataAndFilling(accountId,
                fillingTableSet, fillingTable2SqlMap, cascadeModels);

        StopWatch createdBuildId = StopWatch.createStarted();
        // 转换主键
        Map<String, Map<Long, Map<String, Object>>> dbDataMap = adDasPlatformIncrementCommonHelper.buildId2DataMap(queryDbDataMap);
        perf(AD_DAS_NAMESPACE_NEW, AD_DAS_COMMON_INCR_LIFE_CYCLE_SUBTAG, "filling","buildId2DataMap")
                .micros(createdBuildId.getTime(TimeUnit.MICROSECONDS))
                .logstash();

        // 转换填充  adInstanceBuilder
        return doFillingFinal(tableName, level, fillingTableSet, cascadeModels, dbDataMap);
    }

    protected Map<String, List<Map<String, Object>>> queryDataAndFilling(
            Long accountId, Set<String> fillingTableSet,
            Map<String, String> fillingTable2SqlMap, List<AdDasCascadeModel> cascadeModels) {
        List<Long> primaryIdList = new ArrayList<>();

        StopWatch createdAll = StopWatch.createStarted();
        for (AdDasCascadeModel adDasCascadeModel : cascadeModels) {

            primaryIdList.add(adDasCascadeModel.getId());
        }
        if (HostInfo.debugHost()) {
            log.info("queryDataAndFilling() primaryIdList={}", primaryIdList);
        }
        Map<String, List<Map<String, Object>>> queryDataMap = new ConcurrentHashMap<>();

        if (CollectionUtils.isEmpty(primaryIdList)) {
            return queryDataMap;
        }

        try {

            List<CompletableFuture> futureList = Lists.newArrayList();

            for (String tableName : fillingTableSet) {

                String sql = fillingTable2SqlMap.get(tableName);

                Map<String, String> schemaMap = adDasPlatformIncrementCommonHelper.getTableSchema(tableName);

                if (CollectionUtils.isEmpty(schemaMap)) {
                    throw new RuntimeException("tableName=" + tableName + "must be configured with schemaMap!");
                }

                CompletableFuture<Void> futureCreative = CompletableFuture
                        .runAsync(() -> queryDataMap.putAll(this.doQueryDb(accountId, tableName, schemaMap,
                                sql, primaryIdList)), incrementBatchFillingExecutor);

                futureList.add(futureCreative);
            }

            CompletableFuture.allOf(futureList.toArray(new CompletableFuture[futureList.size()]))
                    .join();

        } catch (Exception e) {
            PerfUtils.perf(AD_DAS_NAMESPACE_NEW, AD_DAS_COMMON_INCR_LIFE_CYCLE_SUBTAG,
                    "lifeCycleError", "batchFilling", "queryDataAndFilling", fillingTableSet)
                    .logstash();
            log.error("queryDataAndFilling() ocurr error!!! accountId={}, fillingTableSet={}", accountId, fillingTableSet, e);
        }

        perf(AD_DAS_NAMESPACE_NEW, AD_DAS_COMMON_INCR_LIFE_CYCLE_SUBTAG, "filling", "queryDataAndFilling")
                .micros(createdAll.getTime(TimeUnit.MICROSECONDS))
                .logstash();

        return queryDataMap;
    }

    /**
     * 构造sql & 查询db
     * @param accountId
     * @param tableName
     * @param sql
     * @param primaryIdList
     * @return
     */
    protected Map<String, List<Map<String, Object>>> doQueryDb(Long accountId, String tableName, Map<String, String> schemaMap,
                                                              String sql, List<Long> primaryIdList) {

        Map<String, List<Map<String, Object>>> result = Maps.newHashMap();

        long shardId = accountId % defaultAdCoreShardCount;

        PerfUtils.perf(AD_DAS_NAMESPACE_NEW, AD_DAS_COMMON_INCR_LIFE_CYCLE_SUBTAG,
                "batchFilling", "queryDataAndFilling", sql)
                .logstash();

        String finalSql = sql.replace("#ids#",  Joiner.on(",").join(primaryIdList));

        List<Object> params = Lists.newArrayList();
        params.add(accountId);

        if (HostInfo.debugHost()) {
            log.info("doQueryDb() finalSql={}, params={}", finalSql, params);
        }

        List<Map<String, Object>> dbResult = adDasIncrementCommonQueryHelper
                .queryWithRetry(getDasIncremnetJdbcTemplate(shardId), String.format(finalSql, shardId),
                        tableName, schemaMap, params);

        result.put(tableName, dbResult);

        return result;
    }

    /**
     * 最终 filling
     * @param tableName
     * @param cascadeModels
     * @param dbDataMap
     */
    protected List<AdDasCascadeModel> doFillingFinal(String tableName, String level, Set<String> fillingTableSet,
                                     List<AdDasCascadeModel> cascadeModels,
                                     Map<String, Map<Long, Map<String, Object>>> dbDataMap) {

        List<AdDasCascadeModel> cascadeModelsResult = Lists.newArrayList();

        StopWatch createdFinal = StopWatch.createStarted();

        if (CollectionUtils.isEmpty(fillingTableSet)) {

            PerfUtils.perf(AD_DAS_NAMESPACE_NEW, AD_DAS_COMMON_INCR_LIFE_CYCLE_SUBTAG, "lifeCycleError",
                    "batchFillingTableEmpty", level, tableName)
                    .logstash();
            log.error("doFillingFinal() data is empty! tableName={}, fillingTableSet={}",
                    tableName, fillingTableSet);
            return cascadeModels;
        }

        if (CollectionUtils.isEmpty(dbDataMap)) {

            PerfUtils.perf(AD_DAS_NAMESPACE_NEW, AD_DAS_COMMON_INCR_LIFE_CYCLE_SUBTAG, "lifeCycleError",
                    "batchFillingDBDataEmpty", level, tableName)
                    .logstash();
            log.error("doFillingFinal() data is empty! tableName={}, queryDbDataMap={}",
                    tableName, dbDataMap);
            return cascadeModels;
        }

        //  查询本表字段配置
        Set<String> tableColumnSet = adDasPlatformIncrementCommonHelper.getTableColumnData(tableName);

        try {
            for (AdDasCascadeModel cascadeModel : cascadeModels) {

                if (!CollectionUtils.isEmpty(tableColumnSet)) {
                    // 先过滤本表字段
                    filterCurTableColumn(cascadeModel, tableColumnSet);
                }
                Long id;
                if (cascadeModel.getTableName().equals("creative_adapter")) {
                    id = cascadeModel.getOriginCreativeId();
                } else {
                     id = cascadeModel.getId();
                }
                boolean dataFlag = false;
                // 假如是主表
                Integer tableLevelWeight = Optional.ofNullable(adDasPlatformIncrementCommonHelper.getTableWeightMapCascade().get(tableName))
                        .orElse(10);
                if (tableLevelWeight == 0) {
                    dataFlag = true;
                }

                for (String fillingTableName : fillingTableSet) {
                    Map<Long, Map<String, Object>> dbData = dbDataMap.get(fillingTableName);

                    // 逐个填充
                    if (!CollectionUtils.isEmpty(dbData.get(id))) {
                        convertFillingData(fillingTableName, cascadeModel, dbData.get(id));
                        dataFlag = true;
                    } else {
                        // 只要有一个表没有横级联上， 就报警
                        PerfUtils.perf(AD_DAS_NAMESPACE_NEW, AD_DAS_COMMON_INCR_LIFE_CYCLE_SUBTAG, "lifeCycleError",
                                "fillingTableEmpty", level, tableName, fillingTableName)
                                .logstash();
                    }
                }

                // 覆盖消息对应 type by level
                if (!dataFlag) {
                    // perf 打点 报警
                    PerfUtils.perf(AD_DAS_NAMESPACE_NEW, AD_DAS_COMMON_INCR_LIFE_CYCLE_SUBTAG, "lifeCycleError",
                            "batchFillingEmpty", level, tableName)
                            .logstash();
                }
            }
        } catch (Exception e) {
            PerfUtils.perf(AD_DAS_NAMESPACE_NEW, AD_DAS_COMMON_INCR_LIFE_CYCLE_SUBTAG, "lifeCycleError",
                    "batchFilling", "doFillingFinal", tableName)
                    .logstash();
            log.error("doFillingFinal() ocurr error! tableName={}, level={}, fillingTableSet={}",
                    tableName, level, fillingTableSet, e);
            return cascadeModels;
        }

        if (CollectionUtils.isEmpty(cascadeModelsResult)) {
            PerfUtils.perf(AD_DAS_NAMESPACE_NEW, AD_DAS_COMMON_INCR_LIFE_CYCLE_SUBTAG, "lifeCycleError",
                    "batchFilling", "doFillingFinalFillingResultEmpty")
                    .count(cascadeModels.size())
                    .logstash();
            log.error("doFillingFinal() cascadeModelsResult is empty!!");
        }

        perf(AD_DAS_NAMESPACE_NEW, AD_DAS_COMMON_INCR_LIFE_CYCLE_SUBTAG, "filling","doFillingFinal")
                .micros(createdFinal.getTime(TimeUnit.MICROSECONDS))
                .logstash();

        return cascadeModelsResult;
    }

    /**
     * 根据配置字段 过滤本表字段
     * @param cascadeModel
     * @param tableColumnSet
     */
    protected void filterCurTableColumn(AdDasCascadeModel cascadeModel, Set<String> tableColumnSet) {

        if (tableColumnSet.contains("*")) {
            return;
        }

        Map<String, Message>instanceFieldsNew = Maps.newHashMap();
        Map<String, Message> instanceFields = cascadeModel.getInstanceFields();

        instanceFields.forEach((key, value) -> {

            if (tableColumnSet.contains(key)) {

                instanceFieldsNew.put(key, value);
            }
        });

        if (!CollectionUtils.isEmpty(instanceFieldsNew)) {
            cascadeModel.setInstanceFields(instanceFieldsNew);
        } else {
            log.error("filterCurTableColumn() instanceFieldsNew is empty! tableColumnSet={}", tableColumnSet);
        }
    }

    /**
     * 根据表 字段 配置 过滤 Filling表 需要填充的字段
     *
     * @param fillingTableName
     * @param adDasCascadeModel
     * @param data
     */
    protected void convertFillingData(String fillingTableName,
                                      AdDasCascadeModel adDasCascadeModel, Map<String, Object> data) {

        // 被级联表增加 convertMore() 逻辑
        AdDasKbusBatchTransfer adDasKbusBatchTransfer = choseTransfer(fillingTableName);
        adDasKbusBatchTransfer.convertFillingMore(fillingTableName, data, adDasCascadeModel);
    }

    @PostConstruct
    private void init() {

        synchronized (AdDasIncrementAbstractBatchFilling.class) {
            if (!incrementBatchFillingStarted.equals(0)) {
                log.warn("init() 已执行过");
                return;
            }

            incrementBatchFillingExecutor = DynamicThreadExecutor.dynamic(AD_DAS_INCREMENT_BATCH_FILLING_THREAD_COUNT::get,
                    "dynamic-ad-das-increment-batch-filling-thread-%d");
            // 退出时清理线程池数据
            TermHelper.addTerm(() -> shutdownAndAwaitTermination(incrementBatchFillingExecutor, 1, DAYS));
            incrementBatchFillingStarted++;

            log.info("【AdDasIncrementAbstractBatchFilling】init() executor addTerm success!");
        }
    }
}
