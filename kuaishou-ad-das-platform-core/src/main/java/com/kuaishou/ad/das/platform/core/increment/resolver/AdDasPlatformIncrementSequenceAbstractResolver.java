package com.kuaishou.ad.das.platform.core.increment.resolver;

import static com.kuaishou.infra.framework.defination.ProductDef.KUAISHOU;

import java.util.Map;

import org.jetbrains.annotations.NotNull;

import com.github.phantomthief.util.ThrowableSupplier;
import com.kuaishou.ad.das.platform.core.increment.model.AdDasIncrementAdapterModel;
import com.kuaishou.framework.binlog.util.BinlogResolver;
import com.kuaishou.framework.util.HostInfo;
import com.kuaishou.framework.warmup.WarmupAble;
import com.kuaishou.infra.databus.client.resolver.mysql.DatabusMysqlKeyAffinityResolver;
import com.kuaishou.infra.databus.client.resolver.mysql.DatabusMysqlMessageContext;
import com.kuaishou.infra.databus.common.domain.mysql.ChangeEventData;
import com.kuaishou.infra.databus.common.domain.mysql.ChangeEventHeader;
import com.kuaishou.infra.databus.common.domain.mysql.DmlChangedEventRow;
import com.kuaishou.infra.databus.common.domain.mysql.MysqlEventColumn;
import com.kuaishou.infra.framework.defination.ProductDef;

import kuaishou.common.BizDef;
import lombok.extern.slf4j.Slf4j;

/**
 * 顺序性消费 抽象类
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-06-01
 */
@Slf4j
public abstract class AdDasPlatformIncrementSequenceAbstractResolver
        extends AdDasPlatformIncremntCommonAbstract
        implements DatabusMysqlKeyAffinityResolver, WarmupAble {

    @Override
    public void resolve(ChangeEventHeader eventHeader, ThrowableSupplier<ChangeEventData, Throwable> message, DatabusMysqlMessageContext context) throws Throwable {

        AdDasIncrementAdapterModel adDasIncrementAdapterObject = adapterData(eventHeader, message.get());

        if (adDasIncrementAdapterObject == null) {
            return;
        }

        if (HostInfo.debugHost()) {
            log.info("dasRun() adDasIncrementAdapterObject={}", adDasIncrementAdapterObject);
        }

        adDasPlatformIncrementEntrance.process(adDasIncrementAdapterObject);
    }

    @Override
    public Object calcAffinityKey(ChangeEventHeader eventHeader, ThrowableSupplier<ChangeEventData, Throwable> message, DatabusMysqlMessageContext context) throws Throwable {

        ChangeEventData changeEventData = message.get();

        String tableName = eventHeader.getTableName();
        String noShardTableName = BinlogResolver.getNoShardTableName(tableName);

        // 新增 或 修改事件 && 在监听的table内
        if (defaultFilterEvenType(changeEventData)) {

            if (filterTable(noShardTableName)) {
                Map<String, MysqlEventColumn> rowColumnMaps = ((DmlChangedEventRow) changeEventData).getAffectedRow();

                // 根据不同的tableName的主键id 来分发处理
                String primaryKey = adDasPlatformIncrementCommon.getTablePrimaryKey(noShardTableName);
                if (rowColumnMaps.containsKey(primaryKey)) {
                    MysqlEventColumn mysqlEventColumn =  rowColumnMaps.get(primaryKey);
                    Long shardId = ((Number) mysqlEventColumn.getData()).longValue();
                    return shardId;
                }
            }
        }

        return tableName;
    }

    @Override
    public void tryWarmup() {
        adDasPlatformIncrementCommon.tryWarmup();
    }

    @NotNull
    @Override
    public String dataSourceName() {
        return adDasPlatformIncrementCommon.getDataSourceName();
    }

    @NotNull
    @Override
    public String consumerGroup() {
        return adDasPlatformIncrementCommon.getConsumerGroup();
    }

    @NotNull
    @Override
    public ProductDef productDef() {
        return KUAISHOU;
    }

    @NotNull
    @Override
    public BizDef bizDef() {
        return BizDef.AD_DSP;
    }
}
