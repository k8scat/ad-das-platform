package com.kuaishou.ad.das.platform.core.benchmark.model;

import java.io.Serializable;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author wangkai <wangkai14@kuaishou.com>
 * Created on 2020-12-23
 */
@Data
public class AdDasBenchmarkBaseTaskInfoView implements Serializable {

    private static final long serialVersionUID = 8968993866288862432L;

    @ApiModelProperty(value = "任务Id")
    private long taskId;

    @ApiModelProperty(value = "任务名称")
    private String taskName;

    @ApiModelProperty(value = "任务编号")
    private int taskNumber;

    @ApiModelProperty(value = "分表shard号list")
    private String taskShardList;

    @ApiModelProperty(value = "任务自定义sql标志")
    private int taskSqlFlag;

    @ApiModelProperty(value = "任务自定义sqlList")
    private String taskSqls;

    @ApiModelProperty(value = "任务分表ids")
    private String shardIds;

    @ApiModelProperty(value = "使用shardSql")
    private int shardSqlFlag;

    @ApiModelProperty(value = "任务状态")
    private int taskStatus;

    @ApiModelProperty(value = "并发度")
    private int concurrency;

}
