#!/bin/sh -e

# 脚本执行 pipeline 触发： https://kdev.corp.kuaishou.com/web/workbench/pipelineFlow?kGid=210&pipelineId=594075

echo "Start create stream code!!!"

streamType=${streamType}

echo "streamType="$streamType

if [ "$streamType" = "client" ]; then
  echo "create client code!"
  cd "./kuaishou-ad-das-platform-benchmark-client/"
  ls -al
  sh -x ad-das-client-create-code.sh
  exit 0
fi

if [ "$streamType" = "increment" ]; then
  echo "create increment code!"
  cd "./kuaishou-ad-das-platform-increment/"
  ls -al
  sh -x ad-das-incre-create-code.sh
  exit 0
fi