package com.kuaishou.ad.das.platform.dal.repository;

import java.util.List;

import com.kuaishou.ad.das.platform.dal.model.AdDasTableColumn;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-11-30
 */
public interface AdDasTableColumnRepository {

    List<AdDasTableColumn> queryTableColumnAll(String dataSource, String mainTable);
}
