package com.kuaishou.ad.das.platform.dal.model;

import lombok.Data;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-06-09
 */
@Data
public class AdDasIncrementTableDetail {

    private Long id;

    /**
     * 增量id
     */
    private Long increId;

    private Long modifyId;

    private String dataSource;

    private String increTable;

    /**
     * 表类型： 1-主表 2-级联表
     */
    private Integer tableType;

    /**
     * 主键id
     */
    private String primaryIdKey;

    private String tableColumn;

    private String pbClassEnum;

    private String operator;

    private String approver;

    private Long createTime;

    private Long updateTime;

    /**
     * 主表级联关系
     */
    private String cascadeSchema;

    /**
     * 被级联表
     */
    private String cascadedTable;

}
