package com.kuaishou.ad.das.platform.dal.model;

import lombok.Data;

/**
 * @author wangkai <wangkai14@kuaishou.com>
 * Created on 2020-12-22
 */
@Data
public class AdDasBenchmarkTaskCommonDetailEdit {

    private Long id;

    private long benchmarkId;

    private String dataSource;

    private String pbClass;

    private String mainTable;

    private int tableType;

    private String columnSchema;

    private String cascadeColumnSchema;

    private String commonSqls;

    private String timeParams;

    private int predictGrowth;

    private int shardSqlFlag;

    private String shardSqlColumn;

    private String operator;

    private Long createTime;

    private Long updateTime;

    private Long modifyId;

}
