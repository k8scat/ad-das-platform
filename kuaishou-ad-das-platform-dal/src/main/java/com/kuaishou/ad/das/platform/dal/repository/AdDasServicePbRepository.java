package com.kuaishou.ad.das.platform.dal.repository;

import java.util.List;

import com.kuaishou.ad.das.platform.dal.model.AdDasServicePb;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-11-29
 */
public interface AdDasServicePbRepository {

    /**
     * 用于服务 第一次 发布变更写入 记录
     * @param adDasServicePb
     * @return
     */
    int insert(AdDasServicePb adDasServicePb);

    List<AdDasServicePb> queryCurrentByVersion(String pbVersion, Integer servicePbStatus);

    List<AdDasServicePb> queryCurService(Integer servicePbStatus);

    List<AdDasServicePb> queryByService(String serviceName, Integer servicePbStatus);

    List<AdDasServicePb> queryByServiceList(List<String> serviceNameList);

    List<AdDasServicePb> queryByServiceType(String serviceType);

    int update(String serviceName, String serviceType, String pbVersion, Long updateTime);
}
