package com.kuaishou.ad.das.platform.dal.repository;

import com.kuaishou.ad.das.platform.dal.model.AdDasBenchmarkTaskCommonDetailEdit;

/**
 * @author wangkai <wangkai14@kuaishou.com>
 * Created on 2021-01-05
 */
public interface AdDasBenchmarkTaskCommonDetailEditRepository {

    int insert(AdDasBenchmarkTaskCommonDetailEdit adDasBenchMarkTaskCommonDetailEdit);


    AdDasBenchmarkTaskCommonDetailEdit getByBenchmarkIdAndMainTable(Long benchmarkId, String mainTable);

    void update(AdDasBenchmarkTaskCommonDetailEdit adDasBenchmarkTaskCommonDetailEdit);

    void deleteByBenchmarkTypeAndMainTable(Long benchmarkId, String mainTable);
}
