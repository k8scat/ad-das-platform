package com.kuaishou.ad.das.platform.dal.repository.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Repository;

import com.kuaishou.ad.das.platform.dal.model.AdDasModifyIncrementTableRecord;
import com.kuaishou.ad.das.platform.dal.repository.AdDasModifyIncrementTableRecordRepository;
import com.kuaishou.ad.das.platform.dal.repository.mapper.AdDasModifyIncrementTableRecordMapper;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-06-15
 */
@Lazy
@Repository
public class AdDasModifyIncrementTableRecordRepositoryImpl implements AdDasModifyIncrementTableRecordRepository {

    @Autowired
    private AdDasModifyIncrementTableRecordMapper adDasModifyIncrementTableRecordMapper;

    @Override
    public List<AdDasModifyIncrementTableRecord> listByModifyId(Long modifyId) {
        return adDasModifyIncrementTableRecordMapper.listByModifyId(modifyId);
    }

    @Override
    public Long insert(AdDasModifyIncrementTableRecord adDasModifyIncrementTableRecord) {
        return adDasModifyIncrementTableRecordMapper.insert(adDasModifyIncrementTableRecord);
    }

    @Override
    public void delByModifyId(Long modifyId) {
        adDasModifyIncrementTableRecordMapper.delByModifyId(modifyId);
    }
}
