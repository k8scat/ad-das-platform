package com.kuaishou.ad.das.platform.dal.repository.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import com.kuaishou.ad.das.platform.dal.model.AdDasBenchmarkTaskHistory;
import com.kuaishou.ad.das.platform.dal.repository.AdDasBenchmarkTaskHistoryRepository;
import com.kuaishou.ad.das.platform.dal.repository.mapper.AdDasBenchmarkTaskHistoryMapper;

import lombok.extern.slf4j.Slf4j;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-07-13
 */
@Lazy
@Slf4j
@Service
public class AdDasBenchmarkTaskHistoryRepositoryImpl implements AdDasBenchmarkTaskHistoryRepository {

    @Autowired
    private AdDasBenchmarkTaskHistoryMapper benchmarkTaskHistoryMapper;

    @Override
    public int insert(AdDasBenchmarkTaskHistory adDasBenchmarkTaskHistory) {
        return benchmarkTaskHistoryMapper.insert(adDasBenchmarkTaskHistory);
    }

    @Override
    public List<AdDasBenchmarkTaskHistory> listByModifyId(Long modifyId) {
        return benchmarkTaskHistoryMapper.listByModifyId(modifyId);
    }
}
