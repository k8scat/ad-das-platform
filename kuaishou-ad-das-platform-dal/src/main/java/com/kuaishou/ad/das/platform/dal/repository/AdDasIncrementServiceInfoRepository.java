package com.kuaishou.ad.das.platform.dal.repository;

import java.util.Collection;
import java.util.List;

import com.kuaishou.ad.das.platform.dal.model.AdDasIncrementBacktrackServiceDetail;
import com.kuaishou.ad.das.platform.dal.model.AdDasIncrementServiceInfo;

/**
 * @author caoyinhang <caoyinhang@kuaishou.com>
 * Created on 2021-07-06
 */

public interface AdDasIncrementServiceInfoRepository {

    int insert(AdDasIncrementServiceInfo adDasIncrementServiceInfo);

    void update(AdDasIncrementServiceInfo adDasIncrementServiceInfo);

    void deleteById(Long dataId);

    List<AdDasIncrementBacktrackServiceDetail> queryByCondition(String incrementName, String dataSource, String consumerGroup, String topic);

    List<AdDasIncrementServiceInfo> queryAll();

    List<AdDasIncrementServiceInfo> queryByIncreId(Collection<Long> increId);
}
