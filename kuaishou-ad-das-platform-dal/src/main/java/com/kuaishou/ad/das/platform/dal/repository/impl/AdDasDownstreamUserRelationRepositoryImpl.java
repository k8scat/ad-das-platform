package com.kuaishou.ad.das.platform.dal.repository.impl;

import java.util.List;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import com.kuaishou.ad.das.dal.datasource.AdCoreDataSource;
import com.kuaishou.ad.das.platform.dal.dao.AdDasUserGroupBindCondition;
import com.kuaishou.ad.das.platform.dal.model.AdDasDownstreamUserRelationBind;
import com.kuaishou.ad.das.platform.dal.repository.AdDasDownstreamUserRelationRepository;

/**
 * @author Zhang Jialin <zhangjialin@kuaishou.com>
 * Created on 2021-06-14
 */
@Repository
public class AdDasDownstreamUserRelationRepositoryImpl implements AdDasDownstreamUserRelationRepository {



    @Override
    public void insert(AdDasDownstreamUserRelationBind param) {
        param.setCreateTime(System.currentTimeMillis());
        param.setUpdateTime(System.currentTimeMillis());
        String sql = "INSERT INTO " + TABLE_NAME + " "
                + "(data_stream_id, data_stream_type, user_group_id, create_time, update_time)"
                + "values (:dataStreamId, :dataStreamType, :userGroupId, :createTime, :updateTime)";
        AdCoreDataSource.adDas.write().update(sql, new BeanPropertySqlParameterSource(param));
    }

    @Override
    public void insert(List<AdDasDownstreamUserRelationBind> param) {
        String sql = "INSERT INTO " + TABLE_NAME + " "
                + "(data_stream_id, data_stream_type, user_group_id, create_time, update_time)"
                + "values (:dataStreamId, :dataStreamType, :userGroupId, :createTime, :updateTime)";
        BeanPropertySqlParameterSource[] parameterSourcesArr =
                param.stream().peek(item -> {
                    item.setCreateTime(System.currentTimeMillis());
                    item.setUpdateTime(System.currentTimeMillis());
                }).map(BeanPropertySqlParameterSource::new).toArray(BeanPropertySqlParameterSource[]::new);
        AdCoreDataSource.adDas.write().batchUpdate(sql, parameterSourcesArr);
    }

    @Override
    public void update(AdDasDownstreamUserRelationBind param) {
        param.setUpdateTime(System.currentTimeMillis());
        String sql = "UPDATE " + TABLE_NAME + " "
                + "SET data_stream_id = :dataStreamId, data_stream_type = :dataStreamType, "
                + "user_group_id = :userGroupId, update_time = :updateTime "
                + "WHERE id = :id";
        AdCoreDataSource.adDas.write().update(sql, new BeanPropertySqlParameterSource(param));
    }

    @Override
    public List<AdDasDownstreamUserRelationBind> query(AdDasUserGroupBindCondition condition) {
        StringBuilder sqlBuilder = new StringBuilder("SELECT * FROM " + TABLE_NAME + " ");
        MapSqlParameterSource paramSource = new MapSqlParameterSource();
        buildPredicate(condition, sqlBuilder, paramSource);
        return AdCoreDataSource.adDas.read().query(sqlBuilder.toString(), paramSource, new BeanPropertyRowMapper<>(AdDasDownstreamUserRelationBind.class));
    }

    @Override
    public void delete(AdDasUserGroupBindCondition condition) {
        StringBuilder sqlBuilder = new StringBuilder("UPDATE " + TABLE_NAME + " SET is_delete = 1");
        MapSqlParameterSource paramSource = new MapSqlParameterSource();
        buildPredicate(condition, sqlBuilder, paramSource);
        AdCoreDataSource.adDas.write().update(sqlBuilder.toString(), paramSource);
    }

    private void buildPredicate(AdDasUserGroupBindCondition condition, StringBuilder sqlBuilder,
            MapSqlParameterSource paramSource) {
        sqlBuilder.append(" WHERE is_delete = :isDelete ");
        paramSource.addValue("isDelete", 0);
        if (!CollectionUtils.isEmpty(condition.getUserGroupIds())) {
            sqlBuilder.append("AND user_group_id in (:userGroupIds)");
            paramSource.addValue("userGroupIds", condition.getUserGroupIds());
        }
        if (!CollectionUtils.isEmpty(condition.getDataStreamIds())) {
            sqlBuilder.append("AND data_stream_id in (:dataStreamIds)");
            paramSource.addValue("dataStreamIds", condition.getDataStreamIds());
        }
        if (!CollectionUtils.isEmpty(condition.getDataStreamTypes())) {
            sqlBuilder.append("AND data_stream_type in (:dataStreamTypes)");
            paramSource.addValue("dataStreamTypes", condition.getDataStreamTypes());
        }

    }
}
