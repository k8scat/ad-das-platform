package com.kuaishou.ad.das.platform.dal.model;

import lombok.Data;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-07-13
 */
@Data
public class AdDasBenchmarkTaskHistory extends AdDasBenchmarkTask {
}
