package com.kuaishou.ad.das.platform.dal.repository.mapper;

import java.util.Collection;
import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.kuaishou.ad.das.platform.dal.model.AdDasModifyIncrementTableRecord;
import com.kuaishou.infra.boot.jdbc.datasource.DataSourceRouting;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-06-15
 */
@Mapper
@DataSourceRouting("adDas")
public interface AdDasModifyIncrementTableRecordMapper {

    @Select("SELECT * from ad_das_modify_increment_table_record "
            + " where modify_id = #{modifyId} ")
    List<AdDasModifyIncrementTableRecord> listByModifyId(@Param("modifyId") Long modifyId);

    @Insert("insert into ad_das_modify_increment_table_record(incre_id,modify_id,data_source,incre_table,table_type,table_column,pb_class_enum,operator,approver," +
            "create_time,update_time,cascade_schema,cascaded_table)" +
            " values(#{increId},#{modifyId},#{dataSource},#{increTable},#{tableType}" +
            ",#{tableColumn},#{pbClassEnum},#{operator},#{approver},#{createTime},#{updateTime},#{cascadeSchema},#{cascadedTable})")
    long insert(AdDasModifyIncrementTableRecord adDasModifyIncrementTableRecord);

    @Delete("delete from ad_das_modify_increment_table_record where modify_id=#{modifyId}")
    void delByModifyId(Long modifyId);

}
