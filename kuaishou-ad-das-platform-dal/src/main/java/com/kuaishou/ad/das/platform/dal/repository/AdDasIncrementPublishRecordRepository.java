package com.kuaishou.ad.das.platform.dal.repository;

import java.util.List;

import com.kuaishou.ad.das.platform.dal.model.AdDasIncrementPublishRecord;

/**
 * @author liutao <liutao06@kuaishou.com>
 * Created on 2021-01-10
 */
public interface AdDasIncrementPublishRecordRepository {


    List<AdDasIncrementPublishRecord> listAdDasIncrementPublishRecordByModifyId(Long modifyId);

    Integer insert(AdDasIncrementPublishRecord adDasIncrementPublishRecord);

}
