package com.kuaishou.ad.das.platform.dal.repository.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.kuaishou.ad.das.platform.dal.model.AdDasBenchmarkTaskEdit;
import com.kuaishou.infra.boot.jdbc.datasource.DataSourceRouting;

/**
 * @author wangkai <wangkai14@kuaishou.com>
 * Created on 2020-12-22
 */

@Mapper
@DataSourceRouting("adDas")
public interface AdDasBenchmarkTaskEditMapper {

    @Insert("insert into ad_das_benchmark_task_edit (task_name,task_number,modify_id,benchmark_id," +
            "data_source,main_table,table_type,task_shard_list,pb_class,pb_class_full_name," +
            "task_sql_flag,task_sqls,shard_sql_flag,shard_ids,task_status,concurrency,operator,create_time,update_time)" +
            "values(#{taskName},#{taskNumber},#{modifyId},#{benchmarkId},#{dataSource}," +
            "#{mainTable},#{tableType},#{taskShardList},#{pbClass},#{pbClassFullName},#{taskSqlFlag}," +
            "#{taskSqls},#{shardSqlFlag},#{shardIds},#{taskStatus},#{concurrency},#{operator},#{creatTime},#{updateTime})")
    int insert(AdDasBenchmarkTaskEdit adDasBenchMarkTaskEdit);

    @Delete("delete from ad_das_benchmark_task_edit where id=#{taskId}")
    void deleteByTaskId(@Param("taskId") Long taskId);

    @Select("select * from ad_das_benchmark_task_edit where id>0 order by task_number desc limit 1")
    AdDasBenchmarkTaskEdit getLastTask();

    @Select("select * from ad_das_benchmark_task_edit where benchmark_id=#{benchmarkId} and main_table=#{mainTable}")
    List<AdDasBenchmarkTaskEdit> getByBenchmarkTypeAndMainTable(@Param("benchmarkId") Long benchmarkId,
                                                                @Param("mainTable") String mainTable);

    @Update("update ad_das_benchmark_task_edit set task_name=#{taskName},task_number=#{taskNumber}," +
            "benchmark_id=#{benchmarkId}," +
            "data_source=#{dataSource},main_table=#{mainTable},table_type=#{tableType},task_shard_list=#{taskShardList}," +
            "pb_class=#{pbClass},task_sql_flag=#{taskSqlFlag},task_sqls=#{taskSqls}," +
            "shard_sql_flag=#{shardSqlFlag},shard_ids=#{shardIds}," +
            "task_status=#{taskStatus},concurrency=#{concurrency},operator=#{operator},update_time=#{updateTime} " +
            "where id=#{id}")
    void update(AdDasBenchmarkTaskEdit adDasBenchmarkTaskEdit);

    @Select("select * from ad_das_benchmark_task_edit where id=#{id}")
    AdDasBenchmarkTaskEdit getById(@Param("id") long id);

    @Select("select * from ad_das_benchmark_task_edit where task_name=#{taskName} and benchmark_id=#{benchmarkId}")
    AdDasBenchmarkTaskEdit getByBenchmarkAndName(@Param("taskName") String taskName, @Param("benchmarkId") Long benchmarkId);

    @Select("select * from ad_das_benchmark_task_edit where main_table=#{main_table} and benchmark_id=#{benchmarkId}")
    List<AdDasBenchmarkTaskEdit> queryListByBenchmarkAndTable(@Param("mainTable") String tableName, @Param("benchmarkId") Long benchmarkId);

    @Select("select * from ad_das_benchmark_task_edit where benchmark_id=#{benchmarkId} " +
            "and data_source=#{dataSource} and main_table = #{mainTable} and pb_class=#{pbClass}")
    List<AdDasBenchmarkTaskEdit> getByBenchmarkId(@Param("benchmarkId") Long benchmarkId,
                                                    @Param("dataSource") String dataSource,
                                                    @Param("pbClass") String pbClass,
                                                    @Param("mainTable") String mainTable);

    @Select("select * from ad_das_benchmark_task_edit where id > 0")
    List<AdDasBenchmarkTaskEdit> queryAll();


    @Select({"<script>"
            +"SELECT * FROM ad_das_benchmark_task_edit "
            +"WHERE 1=1"
            +"<if test='benchmarkId != null'> AND benchmark_id=#{benchmarkId}</if>"
            +"<if test='dataSource != null and dataSource !=\"\"'> AND data_source=#{dataSource}</if>"
            +"<if test='pbClass != null and pbClass !=\"\"'> AND pb_class=#{pbClass}</if>"
            +"<if test='mainTable != null and mainTable !=\"\"'> AND main_table=#{mainTable}</if>"
            +"<if test='taskName != null and taskName !=\"\"'> AND task_name like CONCAT(#{taskName},'%')</if>"
            +"<if test='taskStatus != null and taskStatus !=0'> AND task_status=#{taskStatus}</if>"
            +"limit #{offset},#{limit}"
            +"</script>"})
    List<AdDasBenchmarkTaskEdit> getByParams(@Param("benchmarkId") Long benchmarkId,
                                             @Param("dataSource") String dataSource,
                                             @Param("pbClass") String pbClass,
                                             @Param("mainTable") String mainTable,
                                             @Param("taskName") String taskName,
                                             @Param("taskStatus") Integer taskStatus,
                                             @Param("offset") Integer offset,
                                             @Param("limit") Integer limit);

    @Select({"<script>"
            +"SELECT count(*) FROM ad_das_benchmark_task_edit "
            +"WHERE 1=1"
            +"<if test='benchmarkId != null'> AND benchmark_id=#{benchmarkId}</if>"
            +"<if test='dataSource != null and dataSource !=\"\"'> AND data_source=#{dataSource}</if>"
            +"<if test='pbClass != null and pbClass !=\"\"'> AND pb_class=#{pbClass}</if>"
            +"<if test='mainTable != null and mainTable !=\"\"'> AND main_table=#{mainTable}</if>"
            +"<if test='taskName != null and taskName !=\"\"'> AND task_name like CONCAT(#{taskName},'%')</if>"
            +"<if test='taskStatus != null and taskStatus !=0'> AND task_status=#{taskStatus}</if>"
            +"</script>"})
    long getTotalByParams(@Param("benchmarkId") Long benchmarkId,
                          @Param("dataSource") String dataSource,
                          @Param("pbClass") String pbClass,
                          @Param("mainTable") String mainTable,
                          @Param("taskName") String taskName,
                          @Param("taskStatus") Integer taskStatus);

    @Delete("delete from ad_das_benchmark_task_edit where benchmark_id=#{benchmarkId} and main_table=#{mainTable}")
    void deleteByBenchmarkIdAndMainTable(@Param("benchmarkId") Long benchmarkId,
                                           @Param("mainTable") String mainTable);

    @Update("update ad_das_benchmark_task_edit set task_status=#{taskStatus},update_time=#{updateTime},operator=#{operator} " +
            "where benchmark_id=#{benchmarkId} and main_table=#{mainTable}")
    void updateStatusByBenchmarkIdAndMainTable(@Param("benchmarkId") Long benchmarkId,
                                                 @Param("mainTable") String mainTable,
                                                 @Param("taskStatus") Integer taskStatus,
                                                 @Param("operator") String operator,
                                                 @Param("updateTime") Long updateTime);

    @Select("select * from ad_das_benchmark_task_edit where benchmark_id=#{benchmarkId} and main_table=#{mainTable}")
    List<AdDasBenchmarkTaskEdit> getByBenchmarkTypeAndMainTableAndDataSource(@Param("benchmarkId") Long benchmarkId,
                                                                @Param("mainTable") String mainTable,
                                                                             @Param("dataSource") String dataSource);
}
