package com.kuaishou.ad.das.platform.dal.repository.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.kuaishou.ad.das.platform.dal.model.AdDasBenchmarkTask;
import com.kuaishou.infra.boot.jdbc.datasource.DataSourceRouting;

/**
 * @author wangkai <wangkai14@kuaishou.com>
 * Created on 2020-12-22
 */
@Mapper
@DataSourceRouting("adDas")
public interface AdDasBenchmarkTaskMapper {

    @Insert("insert into ad_das_benchmark_task(task_name,task_number,modify_id,benchmark_id," +
            "data_source,main_table,table_type,task_shard_list,pb_class,pb_class_full_name," +
            "task_sql_flag,task_sqls,shard_sql_flag,shard_ids,task_status,concurrency,operator,create_time,update_time)" +
            "values(#{taskName},#{taskNumber},#{modifyId},#{benchmarkId},#{dataSource}," +
            "#{mainTable},#{tableType},#{taskShardList},#{pbClass},#{pbClassFullName},#{taskSqlFlag}," +
            "#{taskSqls},#{shardSqlFlag},#{shardIds},#{taskStatus},#{concurrency},#{operator},#{creatTime},#{updateTime})")
    int insert(AdDasBenchmarkTask adDasBenchMarkTask);

    @Select({"<script>"
            +"SELECT * FROM ad_das_benchmark_task "
            +"WHERE 1=1"
            +"<if test='benchmarkId != null'> AND benchmark_id=#{benchmarkId}</if>"
            +"<if test='dataSource != null and dataSource !=\"\"'> AND data_source=#{dataSource}</if>"
            +"<if test='pbClass != null and pbClass !=\"\"'> AND pb_class=#{pbClass}</if>"
            +"<if test='mainTable != null and mainTable !=\"\"'> AND main_table=#{mainTable}</if>"
            +"<if test='taskName != null and taskName !=\"\"'> AND task_name like CONCAT(#{taskName},'%')</if>"
            +" limit #{offset},#{limit}"
            +"</script>"})
    List<AdDasBenchmarkTask> getByParams(@Param("benchmarkId") Long benchmarkId,
                                         @Param("dataSource") String dataSource,
                                         @Param("pbClass") String pbClass,
                                         @Param("mainTable") String mainTable,
                                         @Param("taskName") String taskName,
                                         @Param("offset") Integer offset,
                                         @Param("limit") Integer limit);



    @Select("select * from ad_das_benchmark_task where id>0")
    List<AdDasBenchmarkTask> getAll();

    @Select({"<script>"
            +"SELECT count(*) FROM ad_das_benchmark_task "
            +"WHERE 1=1"
            +"<if test='benchmarkId != null'> AND benchmark_id=#{benchmarkId}</if>"
            +"<if test='dataSource != null and dataSource !=\"\"'> AND data_source=#{dataSource}</if>"
            +"<if test='pbClass != null and pbClass !=\"\"'> AND pb_class=#{pbClass}</if>"
            +"<if test='mainTable != null and mainTable !=\"\"'> AND main_table=#{mainTable}</if>"
            +"<if test='taskName != null and taskName !=\"\"'> AND task_name like CONCAT(#{taskName},'%')</if>"
            +"</script>"})
    long getTotalByParams(@Param("benchmarkId") Long benchmarkId,
                         @Param("dataSource") String dataSource,
                         @Param("pbClass") String pbClass,
                         @Param("mainTable") String mainTable,
                         @Param("taskName") String taskName);

    @Delete("delete from ad_das_benchmark_task where id=#{taskId}")
    void deleteByTaskId(@Param("taskId") Long taskId);

    @Select("select * from ad_das_benchmark_task where id=#{taskId}")
    AdDasBenchmarkTask getById(@Param("taskId") Long taskId);

    @Select("select * from ad_das_benchmark_task where task_name=#{task_name} and benchmark_id=#{benchmarkId}")
    AdDasBenchmarkTask getByBenchmarkAndName(@Param("task_name") String taskName, @Param("benchmark_id") Long benchmarkId);

    @Select("select * from ad_das_benchmark_task where main_table=#{main_table} and benchmark_id=#{benchmarkId}")
    List<AdDasBenchmarkTask> queryListByBenchmarkAndTable(@Param("main_table") String tableName, @Param("benchmarkId") Long benchmarkId);

    @Select("select * from ad_das_benchmark_task where task_name=#{task_name} and task_number=#{task_number}")
    AdDasBenchmarkTask getByNumAndName(@Param("task_name") String taskName, @Param("task_number") Integer taskNumber);


    @Select("select * from ad_das_benchmark_task where benchmark_id=#{benchmarkId} and data_source=#{dataSource} " +
            "and pb_class=#{pbClass} and main_table=#{mainTable}")
    List<AdDasBenchmarkTask> getByBenchmarkId(@Param("benchmarkId") Long benchmarkId,
                                                @Param("dataSource") String dataSource,
                                                @Param("pbClass") String pbClass,
                                                @Param("mainTable") String mainTable);


    @Select("select * from ad_das_benchmark_task where benchmark_id=#{benchmarkId} ")
    List<AdDasBenchmarkTask> listByBenchmarkId(@Param("benchmarkId") Long benchmarkId);

    @Update("update ad_das_benchmark_task set task_name=#{taskName},task_number=#{taskNumber}," +
            "task_shard_list=#{taskShardList},task_sql_flag=#{taskSqlFlag}," +
            "task_sqls=#{taskSqls},shard_sql_flag=#{shardSqlFlag},shard_ids=#{shardIds}," +
            "concurrency=#{concurrency},operator=#{operator},update_time=#{updateTime} where id=#{id}")
    void update(AdDasBenchmarkTask adDasBenchmarkTask);

    @Delete("delete from ad_das_benchmark_task where benchmark_id=#{benchmarkId} and main_table=#{mainTable}")
    void deleteByBenchmarkIdAndMainTable(@Param("benchmarkId") Long benchmarkId,
                                         @Param("mainTable") String mainTable);
}




