package com.kuaishou.ad.das.platform.dal.model;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-11-29
 */
public abstract class AdDasAbstarctModel<T> {

    public abstract T parseResult(ResultSet rs) throws SQLException;
}
