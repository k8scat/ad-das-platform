package com.kuaishou.ad.das.platform.dal.repository.impl;

import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Repository;

import com.kuaishou.ad.das.platform.dal.model.AdDasIncrementBacktrackRecond;
import com.kuaishou.ad.das.platform.dal.model.AdDasIncrementBacktrackRecondDetail;
import com.kuaishou.ad.das.platform.dal.repository.AdDasIncrementBacktrackRecondRepository;
import com.kuaishou.ad.das.platform.dal.repository.mapper.AdDasIncrementBacktackRecondMapper;

/**
 * @author caoyinhang <caoyinhang@kuaishou.com>
 * Created on 2021-07-06
 */

@Lazy
@Repository
public class AdDasIncrementBacktrackRecondRepositoryImpl implements AdDasIncrementBacktrackRecondRepository {

    @Autowired
    private AdDasIncrementBacktackRecondMapper adDasIncrementBacktackRecondMapper;

    @Override
    public int insert(AdDasIncrementBacktrackRecond adDasIncrementBacktrackRecond) {
        return adDasIncrementBacktackRecondMapper.insert(adDasIncrementBacktrackRecond);
    }

    @Override
    public List<AdDasIncrementBacktrackRecondDetail> queryByCondition(String incrementName, String dataSource,
            String consumerGroup, String topic,
            String operator, Integer status, String beginTime, String endTime, Integer limit,
            Integer offset) {
        return adDasIncrementBacktackRecondMapper.queryByCondition(incrementName,dataSource,consumerGroup,topic,operator,
                status,beginTime,endTime,limit,offset);
    }

    @Override
    public AdDasIncrementBacktrackRecond queryById(Long id) {
        return null;
    }

    @Override
    public List<AdDasIncrementBacktrackRecond> queryAll() {
        return adDasIncrementBacktackRecondMapper.listAll();
    }

    @Override
    public List<AdDasIncrementBacktrackRecond> queryByIncreId(Collection<Long> increId) {
        return adDasIncrementBacktackRecondMapper.listByIncreId(increId);
    }

    @Override
    public AdDasIncrementBacktrackRecond queryOperatorByIncreId(Long increId) {
        return adDasIncrementBacktackRecondMapper.queryOperatorByIncreId(increId);
    }

    @Override
    public AdDasIncrementBacktrackRecond queryByOperator(String operator) {
        return adDasIncrementBacktackRecondMapper.queryByOperator(operator);
    }

    @Override
    public Integer queryCountByCondition(String incrementName, String dataSource,
            String consumerGroup, String topic, String operator, Integer status, String beginTime, String endTime) {
        return adDasIncrementBacktackRecondMapper.queryCountByCondition(incrementName,dataSource, consumerGroup,topic,operator,status,beginTime, endTime);
    }


}
