package com.kuaishou.ad.das.platform.dal.repository;

import java.util.Collection;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.kuaishou.ad.das.platform.dal.dao.AdDasBenchmarkCondition;
import com.kuaishou.ad.das.platform.dal.model.AdDasBenchmark;

/**
 * @author Zhang Jialin <zhangjialin@kuaishou.com>
 * Created on 2021-06-03
 */
public interface AdDasBenchmarkRepository {

    AdDasBenchmark queryById(Long id);

    List<AdDasBenchmark> listAll();

    /**
     * 查询
     * @param condition 查询条件
     * @return 列表
     */
    List<AdDasBenchmark> query(AdDasBenchmarkCondition condition);

    AdDasBenchmark listByName(@Param("benchmarkName") String benchmarkName);

    /**
     * 根据id 查询
     * @param benchmakrIds
     * @return
     */
    List<AdDasBenchmark> queryById(Collection<Long> benchmakrIds);

    /**
     * 查询条数
     * @param condition 查询条件
     * @return 列表
     */
    Long count(AdDasBenchmarkCondition condition);

    void insert(AdDasBenchmark adDasBenchmark);

    void update(AdDasBenchmark adDasBenchmark);

    void deleteById(Long dataId);
}
