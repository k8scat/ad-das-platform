package com.kuaishou.ad.das.platform.dal.repository.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.kuaishou.ad.das.platform.dal.model.AdDasPb;
import com.kuaishou.infra.boot.jdbc.datasource.DataSourceRouting;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-06-21
 */
@Mapper
@DataSourceRouting("adDas")
public interface AdDasPbMapper {

    @Insert("insert into ad_das_pb(pb_version,pb_enum_version,operator,create_time,update_time) "
            + "values(#{pbVersion}, #{pbEnumVersion}, #{operator}, #{createTime}, #{updateTime}) ")
    long insert(AdDasPb adDasPb);

    @Select("select * from ad_das_pb order by id desc limit 1")
    AdDasPb queryLatest();

    @Select("select * from ad_das_pb order by id desc")
    List<AdDasPb> queryAll();

    @Select("select * from ad_das_pb where pb_version = #{pbVersion}")
    AdDasPb queryByVersion(@Param("pbVersion") String pbVersion);
}
