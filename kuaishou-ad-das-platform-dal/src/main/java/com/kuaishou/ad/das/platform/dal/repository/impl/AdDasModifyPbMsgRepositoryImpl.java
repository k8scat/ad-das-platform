package com.kuaishou.ad.das.platform.dal.repository.impl;

import java.util.List;

import org.springframework.context.annotation.Lazy;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import com.google.common.collect.Lists;
import com.kuaishou.ad.das.platform.dal.model.AdDasModifyPbMsg;
import com.kuaishou.ad.das.platform.dal.repository.AdDasModifyPbMsgRepository;
import com.kuaishou.ad.das.platform.dal.repository.common.AdDasAbstractRepository;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-11-29
 */
@Lazy
@Repository
public class AdDasModifyPbMsgRepositoryImpl
        extends AdDasAbstractRepository<AdDasModifyPbMsg>
        implements AdDasModifyPbMsgRepository {

    @Override
    protected RowMapper<AdDasModifyPbMsg> getRowMapper() {
        return (rs, row) -> {
            AdDasModifyPbMsg adDasModifyPbMsg = new AdDasModifyPbMsg();
            adDasModifyPbMsg.parseResult(rs);
            return adDasModifyPbMsg;
        };
    }

    @Override
    protected JdbcTemplate getJdbcTemplate(String dataSource) {
        return jdbcTemplate;
    }

    @Override
    public int insert(AdDasModifyPbMsg adDasModifyPbMsg) {

        String insertSql = "insert into ad_das_modify_pb_msg("
                + "modify_id,cur_lastest_pb_version,service_name,service_type,cur_pb_version,"
                + "target_pb_version,pre_version,cr_links,pipline_links,operator,create_time,update_time) values(?,?,?, ?,?,?, ?,?,?, ?,?,?)";

        return jdbcTemplateWrite.update(insertSql, adDasModifyPbMsg.getModifyId(), adDasModifyPbMsg.getCurLastestPbVersion(),
                adDasModifyPbMsg.getServiceName(), adDasModifyPbMsg.getServiceType(), adDasModifyPbMsg.getCurPbVersion(),
                adDasModifyPbMsg.getTargetPbVersion(), adDasModifyPbMsg.getPreVersion(),
                adDasModifyPbMsg.getCrLinks(), adDasModifyPbMsg.getPiplineLinks(),
                adDasModifyPbMsg.getOperator(), adDasModifyPbMsg.getCreateTime(), adDasModifyPbMsg.getUpdateTime());
    }

    @Override
    public List<AdDasModifyPbMsg> queryByModifyId(Long modifyId) {

        String querySql = "select * from ad_das_modify_pb_msg where modify_id=?";

        return jdbcTemplate.query(querySql, getRowMapper(), modifyId);
    }

    @Override
    public List<AdDasModifyPbMsg> queryByModifyIds(List<Long> modifyIds) {

        List<Object> param = Lists.newArrayList();

        if (CollectionUtils.isEmpty(modifyIds)) {
            return Lists.newArrayList();
        }

        String querySql = "select * from ad_das_modify_pb_msg "
                + "where modify_id in (";

        for (Long id : modifyIds) {

            if (querySql.endsWith("?")) {
                querySql = querySql + ",";
            }

            querySql = querySql + "?";
            param.add(id);
        }

        querySql = querySql + ")";

        return jdbcTemplate.query(querySql, getRowMapper(), param.toArray());
    }
}
