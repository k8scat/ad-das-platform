package com.kuaishou.ad.das.platform.dal.model;

import lombok.Data;

/**
 * @author wangkai <wangkai14@kuaishou.com>
 * Created on 2020-12-22
 */
@Data
public class AdDasBenchmarkTaskCommonDetail extends AdDasBenchmarkTaskCommonDetailEdit {

    @Override
    public String toString() {
        return "AdDasBenchmarkTaskCommonDetail{} " + super.toString();
    }
}
