package com.kuaishou.ad.das.platform.dal.repository.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.kuaishou.ad.das.platform.dal.model.AdDasDataStreamGroup;
import com.kuaishou.infra.boot.jdbc.datasource.DataSourceRouting;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-06-10
 */
@Mapper
@DataSourceRouting("adDas")
public interface AdDasDataStreamGroupMapper {

    @Insert("insert into ad_das_data_stream_group(data_stream_group_name,data_stream_group_desc,"
            + "owner_email_prefix,owner_name,create_time,update_time,is_delete)" +
            " values(#{dataStreamGroupName},#{dataStreamGroupDesc},#{ownerEmailPrefix},#{ownerName},#{createTime}" +
            ",#{updateTime},#{isDelete})")
    Integer insert(AdDasDataStreamGroup adDasDataStreamGroup);

    @Update("update ad_das_data_stream_group ")
    void update(AdDasDataStreamGroup adDasDataStreamGroup);

    @Delete("delete from ad_das_data_stream_group where id=#{dataId} ")
    void deleteById(@Param("dataId") Long dataId);

    @Select("SELECT * from ad_das_data_stream_group "
            + " where id = #{id} ")
    AdDasDataStreamGroup listById(@Param("id") Long id);

    @Select({"<script> "
            + "SELECT * from ad_das_data_stream_group datastream "
            + " left join ad_das_benchmark benchmark on datastream.id=benchmark.data_stream_group_id "
            + " left join ad_das_increment increment on datastream.id=increment.data_stream_group_id "
            + " where 1=1"
            + " <if test = \" ids != null and ids.size() != 0 \"> "
            +     " and datastream.id in "
            + " <foreach item = 'item' index = 'index' collection = 'ids' open = '(' separator = ',' close = ')' > "
            + "     #{item} "
            + "    </foreach> "
            + " </if> "
            + " <if test = \" incrementTopics != null and incrementTopics.size() != 0\"> "
            +     " and increment.topic in "
            + " <foreach item = 'item' index = 'index' collection = 'incrementTopics' open = '(' separator = ',' close = ')' > "
            + "     #{item} "
            + "    </foreach> "
            + " </if> "
            + " <if test = \" hdfsPath != null and hdfsPath.size() != 0 \"> "
            +     " and benchmark.hdfs_path in "
            + " <foreach item = 'item' index = 'index' collection = 'hdfsPath' open = '(' separator = ',' close = ')' > "
            + "     #{item} "
            + "    </foreach> "
            + " </if> "
            + " <if test = \" dataStreamGroupName != null and dataStreamGroupName != '' \"> "
            +     " and datastream.data_stream_group_name=#{dataStreamGroupName} "
            + " </if> "
            + " <if test = \" dataSourceNames != null and dataSourceNames.size() != 0 \"> "
            +     " and increment.data_source in "
            + " <foreach item = 'item' index = 'index' collection = 'dataSourceNames' open = '(' separator = ',' close = ')' > "
            + "     #{item} "
            + "    </foreach> "
            + " </if> "
            + " </script>"})
    List<AdDasDataStreamGroup> queryByCondition(@Param("ids") List<Long> ids, @Param("dataStreamGroupName") String dataStreamGroupName,
                                                @Param("dataSourceNames") List<String> dataSourceNames, @Param("incrementTopics") List<String> incrementTopics,
                                                @Param("hdfsPath") List<String> hdfsPath);


    @Select({"<script> "
            + "SELECT count(*) from ad_das_data_stream_group datastream "
            + " left join ad_das_benchmark benchmark on datastream.id=benchmark.data_stream_group_id "
            + " left join ad_das_increment increment on datastream.id=increment.data_stream_group_id "
            + " where 1=1"
            + " <if test = \" ids != null and ids.size() != 0\"> "
            +     " and datastream.id in "
            + " <foreach item = 'item' index = 'index' collection = 'ids' open = '(' separator = ',' close = ')' > "
            + "     #{item} "
            + "    </foreach> "
            + " </if> "
            + " <if test = \" incrementTopics != null and incrementTopics.size() != 0\"> "
            +     " and increment.topic in "
            + " <foreach item = 'item' index = 'index' collection = 'incrementTopics' open = '(' separator = ',' close = ')' > "
            + "     #{item} "
            + "    </foreach> "
            + " </if> "
            + " <if test = \" hdfsPath != null and hdfsPath.size() != 0\"> "
            +     " and benchmark.hdfs_path in "
            + " <foreach item = 'item' index = 'index' collection = 'hdfsPath' open = '(' separator = ',' close = ')' > "
            + "     #{item} "
            + "    </foreach> "
            + " </if> "
            + " <if test = \" dataStreamGroupName != null and dataStreamGroupName != '' \"> "
            +     " and datastream.data_stream_group_name=#{dataStreamGroupName} "
            + " </if> "
            + " <if test = \" dataSourceNames != null and dataSourceNames.size() != 0 \"> "
            +     " and increment.data_source in "
            + " <foreach item = 'item' index = 'index' collection = 'dataSourceNames' open = '(' separator = ',' close = ')' > "
            + "     #{item} "
            + "    </foreach> "
            + " </if> "
            + " </script>"})
    long countByCondition(@Param("ids") List<Long> ids, @Param("dataStreamGroupName") String dataStreamGroupName,
                                                @Param("dataSourceNames") List<String> dataSourceNames, @Param("incrementTopics") List<String> incrementTopics,
                                                @Param("hdfsPath") List<String> hdfsPath);


}
