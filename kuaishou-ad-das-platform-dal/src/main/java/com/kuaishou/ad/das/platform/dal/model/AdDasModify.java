package com.kuaishou.ad.das.platform.dal.model;

import java.sql.ResultSet;
import java.sql.SQLException;

import lombok.Data;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-11-29
 */
@Data
public class AdDasModify extends AdDasAbstarctModel<AdDasModify> {

    private Long id;

    private String modifyName;

    private Integer modifyType;

    private String operator;

    private Integer modifyStatus;

    private String approver;

    private String reason;

    private Long createTime;

    private Long updateTime;

    @Override
    public AdDasModify parseResult(ResultSet rs) throws SQLException {

        this.setId(rs.getLong("id"));
        this.setModifyName(rs.getString("modify_name"));
        this.setModifyType(rs.getInt("modify_type"));
        this.setOperator(rs.getString("operator"));
        this.setModifyStatus(rs.getInt("modify_status"));
        this.setApprover(rs.getString("approver"));
        this.setReason(rs.getString("reason"));
        this.setCreateTime(rs.getLong("create_time"));
        this.setUpdateTime(rs.getLong("update_time"));
        return this;
    }
}
