package com.kuaishou.ad.das.platform.dal.repository.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Repository;

import com.kuaishou.ad.das.platform.dal.model.AdDasPb;
import com.kuaishou.ad.das.platform.dal.repository.AdDasPbRepository;
import com.kuaishou.ad.das.platform.dal.repository.mapper.AdDasPbMapper;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-11-29
 */
@Lazy
@Repository
public class AdDasPbRepositoryImpl implements AdDasPbRepository {

    @Autowired
    private AdDasPbMapper adDasPbMapper;

    @Override
    public long insert(AdDasPb adDasPb) {
        return adDasPbMapper.insert(adDasPb);
    }

    @Override
    public AdDasPb queryLastPb() {
       return adDasPbMapper.queryLatest();
    }

    @Override
    public List<AdDasPb> queryAll() {
        return adDasPbMapper.queryAll();
    }

    @Override
    public AdDasPb queryByVersion(String pbVersion) {
        return adDasPbMapper.queryByVersion(pbVersion);
    }
}
