package com.kuaishou.ad.das.platform.dal.model;

import java.sql.ResultSet;
import java.sql.SQLException;

import lombok.Data;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-11-29
 */
@Data
public class AdDasPbClassColumn extends AdDasAbstarctModel<AdDasPbClassColumn>{

    private Long id;

    private String pbVersion;

    private String pbClass;

    private String pbColumn;

    private String pbFullColumn;

    private String pbJavaType;

    private String pbType;

    private String enumType;

    private String defaultValue;

    private String operator;

    private Long createTime;

    private Long updateTime;

    @Override
    public AdDasPbClassColumn parseResult(ResultSet rs) throws SQLException {

        this.setId(rs.getLong("id"));
        this.setPbVersion(rs.getString("pb_version"));
        this.setPbClass(rs.getString("pb_class"));
        this.setPbColumn(rs.getString("pb_column"));
        this.setPbFullColumn(rs.getString("pb_full_column"));
        this.setPbJavaType(rs.getString("pb_java_type"));
        this.setPbType(rs.getString("pb_type"));
        this.setEnumType(rs.getString("enum_type"));
        this.setDefaultValue(rs.getString("default_value"));
        this.setOperator(rs.getString("operator"));
        this.setCreateTime(rs.getLong("create_time"));
        this.setUpdateTime(rs.getLong("update_time"));

        return this;
    }
}
