package com.kuaishou.ad.das.platform.dal.datasource;

import java.util.concurrent.ConcurrentHashMap;

import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import com.kuaishou.infra.framework.datasource.KsDataSourceFactory;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-06-19
 */
public class AdDasDataSourceHolder {

    private static ConcurrentHashMap<String, NamedParameterJdbcTemplate> namedParameterJdbcTemplateMap = new ConcurrentHashMap<>();

    public static NamedParameterJdbcTemplate getByDataSourceName(String dataSourceName) {

        if (namedParameterJdbcTemplateMap.containsKey(dataSourceName)) {
            return namedParameterJdbcTemplateMap.get(dataSourceName);
        } else {
            NamedParameterJdbcTemplate namedParameterJdbcTemplate =
                    new NamedParameterJdbcTemplate(KsDataSourceFactory.getDataSource(dataSourceName));

            namedParameterJdbcTemplateMap.put(dataSourceName, namedParameterJdbcTemplate);

            return namedParameterJdbcTemplate;
        }
    }
}
