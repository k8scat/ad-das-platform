package com.kuaishou.ad.das.platform.dal.repository.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import com.kuaishou.ad.das.platform.dal.model.AdDasIncrementDetailOnline;
import com.kuaishou.infra.boot.jdbc.datasource.DataSourceRouting;

/**
 * @author liutao <liutao06@kuaishou.com>
 * Created on 2021-01-06
 */
@Mapper
@DataSourceRouting("adDas")
@Repository
public interface AdDasIncrementDetailOnlineMapper {

    @Select("SELECT "
            + "id,modify_id modifyId,increment_service incrementService,data_source dataSource,"
            + " cur_pb_version curPbVersion,main_table mainTable,pb_class pbClass,operator,approver,create_time createTime,update_time updateTime"
            + " from ad_das_increment_detail_online "
            + " where increment_service = #{serviceName} ")
    List<AdDasIncrementDetailOnline> listIncrementDetailOnlineInfoByServiceName(@Param("serviceName") String serviceName);


    @Insert("insert into ad_das_increment_detail_online(modify_id,increment_service,data_source,cur_pb_version,main_table,pb_class,operator,approver,create_time,update_time)" +
            " values(#{modifyId},#{incrementService},#{dataSource},#{curPbVersion},#{mainTable}" +
            ",#{pbClass},#{operator},#{approver},#{createTime},#{updateTime})")
    Integer insert(AdDasIncrementDetailOnline adDasIncrementDetailOnline);

    @Insert("DELETE from ad_das_increment_detail_online" +
            " where increment_service = #{incrementService} ")
    Integer delByServiceName(String incrementService);
}
