package com.kuaishou.ad.das.platform.dal.repository.impl;

import java.util.List;
import java.util.StringJoiner;

import org.apache.commons.collections.CollectionUtils;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Service;

import com.kuaishou.ad.das.dal.datasource.AdCoreDataSource;
import com.kuaishou.ad.das.platform.dal.dao.AdDasCommonDetailCondition;
import com.kuaishou.ad.das.platform.dal.model.AdDasBenchmarkTaskCommonDetail;
import com.kuaishou.ad.das.platform.dal.repository.AdDasBenchMarkTaskCommonDetailRepository;
import com.kuaishou.ad.das.platform.dal.repository.mapper.AdDasBenchmarkTaskCommonDetailMapper;

/**
 * @author wangkai <wangkai14@kuaishou.com>
 * Created on 2020-12-22
 */
@Lazy
@Service
public class AdDasBenchMarkTaskCommonDetailRepositoryImpl implements AdDasBenchMarkTaskCommonDetailRepository {

    @Autowired
    private AdDasBenchmarkTaskCommonDetailMapper taskCommonDetailMapper;

    @Override
    public int insert(AdDasBenchmarkTaskCommonDetail adDasBenchMarkTaskCommonDetail) {
        return taskCommonDetailMapper.insert(adDasBenchMarkTaskCommonDetail);
    }

    @Override
    public AdDasBenchmarkTaskCommonDetail getByBenchmarkTypeAndMainTable(Long benchmarkId, String mainTable) {
        return taskCommonDetailMapper.getByBenchmarkIdAndMainTable(benchmarkId, mainTable);
    }

    @Override
    public AdDasBenchmarkTaskCommonDetail getByCommonDetailId(Long commonDetailId) {
        return taskCommonDetailMapper.getByCommonDetailId(commonDetailId);
    }

    @Override
    public void update(AdDasBenchmarkTaskCommonDetail adDasBenchmarkTaskCommonDetail) {
        taskCommonDetailMapper.update(adDasBenchmarkTaskCommonDetail);
    }

    @Override
    public void deleteByBenchmarkIdAndMainTable(Long benchmarkId, String mainTable) {
        taskCommonDetailMapper.deleteByBenchmarkIdAndMainTable(benchmarkId,mainTable);
    }

    @Override
    public List<AdDasBenchmarkTaskCommonDetail> query(AdDasCommonDetailCondition condition) {
        StringBuilder sqlBuilder = new StringBuilder("SELECT * FROM " + TABLE_NAME);
        MapSqlParameterSource paramSource = new MapSqlParameterSource();
        buildPredicate(condition, sqlBuilder, paramSource);
        return AdCoreDataSource.adDas.read().query(sqlBuilder.toString(), paramSource,
                new BeanPropertyRowMapper<>(AdDasBenchmarkTaskCommonDetail.class));
    }

    private void buildPredicate(AdDasCommonDetailCondition condition, StringBuilder sb,
            MapSqlParameterSource param) {
        StringJoiner predicateBuilder = new StringJoiner(" AND ");
        if (CollectionUtils.isNotEmpty(condition.getCommonDetailIds())) {
            predicateBuilder.add("id in (:commonDetailIds)");
            param.addValue("commonDetailIds", condition.getCommonDetailIds());
        }
        if (CollectionUtils.isNotEmpty(condition.getBenchmarkIds())) {
            predicateBuilder.add("benchmark_id in (:benchmarkIds)");
            param.addValue("benchmarkIds", condition.getBenchmarkIds());
        }
        if (Strings.isNotEmpty(condition.getMainTableName())) {
            predicateBuilder.add("main_table like '%:mainTableName%'");
            param.addValue("mainTableName", condition.getMainTableName());
        }
        if (predicateBuilder.length() != 0) {
            sb.append(" WHERE ").append(predicateBuilder);
        }

    }
}
