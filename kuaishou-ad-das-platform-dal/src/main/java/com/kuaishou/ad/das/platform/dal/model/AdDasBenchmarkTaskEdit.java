package com.kuaishou.ad.das.platform.dal.model;

import lombok.Data;

/**
 * @author wangkai <wangkai14@kuaishou.com>
 * Created on 2020-12-22
 */
@Data
public class AdDasBenchmarkTaskEdit {

    private Long id;

    private long modifyId;

    private long benchmarkId;

    private String taskName;

    private int taskNumber;

    private String dataSource;

    private String mainTable;

    private Integer tableType;

    private String taskShardList;

    private String pbClass;

    private String pbClassFullName;

    private int taskSqlFlag;

    private String taskSqls;

    private String shardIds;

    /**
     * 0-默认编辑中 1-有效 2-删除
     */
    private int taskStatus;

    private int concurrency;

    private int shardSqlFlag;

    private String operator;

    private long creatTime;

    private long updateTime;
}
