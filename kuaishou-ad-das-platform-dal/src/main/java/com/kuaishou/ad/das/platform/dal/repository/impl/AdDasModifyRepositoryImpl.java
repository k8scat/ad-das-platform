package com.kuaishou.ad.das.platform.dal.repository.impl;

import java.util.List;
import java.util.Map;
import java.util.StringJoiner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import com.google.common.collect.Lists;
import com.kuaishou.ad.das.dal.datasource.AdCoreDataSource;
import com.kuaishou.ad.das.platform.dal.dao.ModifyCondition;
import com.kuaishou.ad.das.platform.dal.model.AdDasModify;
import com.kuaishou.ad.das.platform.dal.repository.AdDasModifyRepository;
import com.kuaishou.ad.das.platform.dal.repository.common.AdDasAbstractRepository;
import com.kuaishou.ad.das.platform.dal.repository.mapper.AdDasModifyMapper;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-11-29
 */
@Lazy
@Repository
public class AdDasModifyRepositoryImpl extends AdDasAbstractRepository<AdDasModify>
        implements AdDasModifyRepository {

    @Autowired
    private AdDasModifyMapper adDasModifyMapper;

    @Override
    public long insert(AdDasModify adDasModify) {
        return adDasModifyMapper.insert(adDasModify);
    }

    @Override
    public Long insertWithIdReturn(AdDasModify adDasModify) {
        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        sqlParameterSource.addValue("modifyName", adDasModify.getModifyName());
        sqlParameterSource.addValue("modifyType", adDasModify.getModifyType());
        sqlParameterSource.addValue("operator", adDasModify.getOperator());
        sqlParameterSource.addValue("modifyStatus", adDasModify.getModifyStatus());
        sqlParameterSource.addValue("approver", adDasModify.getApprover());
        sqlParameterSource.addValue("reason", adDasModify.getReason());
        sqlParameterSource.addValue("createTime", adDasModify.getCreateTime());
        sqlParameterSource.addValue("updateTime", adDasModify.getUpdateTime());
        String insertSql = "insert into ad_das_modify(modify_name,modify_type,operator,modify_status,"
                + "approver,reason,create_time,update_time) "
                + "values(:modifyName,:modifyType,:operator,:modifyStatus,:approver,:reason,:createTime,:updateTime) ";
        KeyHolder keyHolder = new GeneratedKeyHolder();
        NamedParameterJdbcTemplate namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(jdbcTemplate);
        Integer result = namedParameterJdbcTemplate.update(insertSql, sqlParameterSource, keyHolder);
        if (result > 0) {
            return keyHolder.getKey().longValue();
        }
        return -1L;
    }

    @Override
    public List<AdDasModify> queryByNameAndCreateTime(
            String modifyName, Integer modifyType, String operator, Long createTime) {
        String querySql = "select * from ad_das_modify "
                + "where modify_name=? and modify_type=? and operator=? and create_time=? order by id desc";

        return jdbcTemplate.query(querySql, getRowMapper(), modifyName, modifyType, operator, createTime);
    }

    @Override
    public Long countByCondition(String modifyName, Integer modifyType, Integer modifyStatus,
            Long startTime, Long endTime) {

        String querySql = "select count(*) as dataCount "
                + "from ad_das_modify where update_time>=? and update_time<=? ";

        List<Object> params = Lists.newArrayList();
        params.add(startTime);
        params.add(endTime);

        Map<String, Object> dataMap = null;

        if (!StringUtils.isEmpty(modifyName)) {
            querySql = querySql + "and modify_name like '" + modifyName + "%'";
        }

        if (modifyType != null) {
            querySql = querySql + "and modify_type = ? ";
            params.add(modifyType);
        }

        if (modifyStatus != null) {
            querySql = querySql + "and modify_status = ? ";
            params.add(modifyStatus);
        }

        dataMap = jdbcTemplate.queryForMap(querySql, params.toArray());

        if (CollectionUtils.isEmpty(dataMap)) {
            return 0L;
        }

        return Long.parseLong(dataMap.get("dataCount").toString());
    }

    @Override
    public List<AdDasModify> queryByCondition(String modifyName, Integer modifyType,
            Integer modifyStatus, Long startTime, Long endTime,
            Integer limit, Integer offset) {

        String querySql = "select * from ad_das_modify where update_time>=? and update_time<=? ";
        List<Object> params = Lists.newArrayList();
        params.add(startTime);
        params.add(endTime);

        if (!StringUtils.isEmpty(modifyName)) {
            querySql = querySql + "and modify_name like '" + modifyName + "%'";
        }

        if (modifyType != null) {
            querySql = querySql + "and modify_type = ? ";
            params.add(modifyType);
        }

        if (modifyStatus != null) {
            querySql = querySql + "and modify_status = ? ";
            params.add(modifyStatus);
        }

        querySql = querySql + "order by update_time desc ";

        querySql = querySql + "limit ?,?";
        params.add(offset);
        params.add(limit);

        return jdbcTemplate.query(querySql, params.toArray(), getRowMapper());
    }

    @Override
    public List<AdDasModify> queryByIdAndType(Long modifyId, Integer modifyType) {

        String querySql = "select * from ad_das_modify where id=? ";

        if (modifyType != null) {
            querySql = querySql + "and modify_type=? ";
            return jdbcTemplate.query(querySql, getRowMapper(), modifyId, modifyType);
        } else {
            return jdbcTemplate.query(querySql, getRowMapper(), modifyId);
        }
    }

    @Override
    public int updateApprove(Long modifyId, String approver, Integer approveType,
            String reason, Long updateTime) {

        Integer modifyStatus = approveType; // TODO 映射转换

        return adDasModifyMapper.updateApprove(modifyId, approver, modifyStatus, reason, updateTime);
    }

    @Override
    public int updateApprove(Long modifyId, String approver, Integer approveType,
            String reason, Long updateTime, Integer beforeStatus) {

        Integer modifyStatus = approveType; // TODO 映射转换

      return adDasModifyMapper.updateApprove(modifyId, approver, modifyStatus, reason, updateTime);
    }

    @Override
    public int updateStatus(Long modifyId, Integer modifyStatus, String operator, Long updateTime) {

        return adDasModifyMapper.updateStatusWithOperator(modifyId, modifyStatus, operator, updateTime);
    }

    @Override
    public int updateStatus(Long modifyId, Integer modifyStatus, Long updateTime) {

        return adDasModifyMapper.updateStatus(modifyId, modifyStatus, updateTime);
    }

    @Override
    public AdDasModify queryById(Long modifyId) {
        return adDasModifyMapper.queryById(modifyId);
    }

    @Override
    public List<AdDasModify> queryByIds(List<Long> modifyIds) {
        return adDasModifyMapper.queryByIds(modifyIds);
    }

    @Override
    public List<AdDasModify> queryByConditions(List<Long> modifyIds,String modifyName,
                                               Integer modifyType, Integer modifyStatus,
                                               Long startTime, Long endTime,
                                               Integer limit, Integer offset) {
        return adDasModifyMapper.queryByCondition(modifyIds, modifyName, modifyType, modifyStatus,
                startTime, endTime, limit, offset);
    }

    @Override
    public List<AdDasModify> queryByStatus(Integer modifyType, Integer modifyStatus) {
        String querySql = "select * from ad_das_modify where modify_type=? and modify_status=? ";
        return jdbcTemplate.query(querySql, getRowMapper(), modifyType, modifyStatus);
    }

    @Override
    public int updateAdDasModify(AdDasModify adDasModify) {
        String updateSql =
                "update ad_das_modify set approver=?, update_time=? , modify_status=? , modify_name=?"
                        + "where id=?";
        return jdbcTemplateWrite.update(updateSql,
                adDasModify.getApprover(), adDasModify.getUpdateTime(), adDasModify.getModifyStatus(),
                adDasModify.getModifyName(), adDasModify.getId());
    }

    @Override
    public int submitApprove(Long modifyId, String operator, Integer approveType,
            Long updateTime, String approver) {
        String updateSql = "update ad_das_modify set modify_status=?, operator=?, update_time=?, approver=? "
                + "where id=?";
        return jdbcTemplateWrite.update(updateSql, approveType, operator, updateTime, approver, modifyId);
    }

    @Override
    protected RowMapper<AdDasModify> getRowMapper() {
        return (rs, row) -> {
            AdDasModify adDasModify = new AdDasModify();
            adDasModify.parseResult(rs);
            return adDasModify;
        };
    }

    @Override
    protected JdbcTemplate getJdbcTemplate(String dataSource) {
        return jdbcTemplate;
    }

    @Override
    public List<AdDasModify> query(ModifyCondition condition) {
        StringBuilder sqlBuilder = new StringBuilder("SELECT * FROM " + TABLE_NAME);
        MapSqlParameterSource paramSource = new MapSqlParameterSource();
        buildPredicate(condition, sqlBuilder, paramSource);
        return AdCoreDataSource.adDas.read().query(sqlBuilder.toString(), paramSource,
                new BeanPropertyRowMapper<>(AdDasModify.class));
    }

    private void buildPredicate(ModifyCondition condition, StringBuilder sb, MapSqlParameterSource paramSource) {
        StringJoiner sj = new StringJoiner(" AND ");
        if (!CollectionUtils.isEmpty(condition.getModifyIds())) {
            sj.add("id in (:modifyIds)");
            paramSource.addValue("modifyIds", condition.getModifyIds());
        }
        if (!CollectionUtils.isEmpty(condition.getModifyType())) {
            sj.add("modify_type in (:modifyType)");
            paramSource.addValue("modifyType", condition.getModifyType());
        }
        if (!CollectionUtils.isEmpty(condition.getModifyStatus())) {
            sj.add("modify_status in (:modifyStatus)");
            paramSource.addValue("modifyStatus", condition.getModifyStatus());
        }
        if (!StringUtils.isEmpty(condition.getModifyName())) {
            sj.add("modify_name like '%:modifyName%'");
            paramSource.addValue("modifyName", condition.getModifyName());
        }
        if (!StringUtils.isEmpty(condition.getOperator())) {
            sj.add("operator like '%:operator%'");
            paramSource.addValue("operator", condition.getOperator());
        }
        if (condition.getStartTime2EndTime() != null) {
            sj.add(String.format("update_time >= %s", condition.getStartTime2EndTime().getLeft()));
            sj.add(String.format("update_time <= %s", condition.getStartTime2EndTime().getRight()));
        }
        if (sj.length() != 0) {
            sb.append(" WHERE ").append(sj);
        }
        if (condition.getPageNum() > 0 && condition.getPageSize() > 0) {
            int upperBound = (condition.getPageNum() - 1) * condition.getPageSize();
            int lowerBound = upperBound + condition.getPageSize();
            sb.append(String.format(" limit %s, %s", upperBound, lowerBound));
        }
    }

}
