package com.kuaishou.ad.das.platform.dal.repository;

import java.util.List;

import com.kuaishou.ad.das.platform.dal.model.AdDasModifyServicePbRecord;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-12-09
 */
public interface AdDasModifyServicePbRecordRepository {

    int insert(AdDasModifyServicePbRecord adDasModifyServicePbRecord);

    List<AdDasModifyServicePbRecord> queryByCondition(Long modifyId, String serviceName, String serviceNodeName);

    int updatePublishStatus(Long modifyId, Integer publishStatus, String serviceName, String serviceNodeName);
}
