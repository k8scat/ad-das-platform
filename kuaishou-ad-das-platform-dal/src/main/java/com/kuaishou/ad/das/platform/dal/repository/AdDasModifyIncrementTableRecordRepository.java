package com.kuaishou.ad.das.platform.dal.repository;

import java.util.List;

import com.kuaishou.ad.das.platform.dal.model.AdDasModifyIncrementTableRecord;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-06-15
 */
public interface AdDasModifyIncrementTableRecordRepository {

    List<AdDasModifyIncrementTableRecord> listByModifyId(Long modifyId);

    Long insert(AdDasModifyIncrementTableRecord adDasModifyIncrementTableRecord);

    void delByModifyId(Long modifyId);
}
