package com.kuaishou.ad.das.dal.datasource

import com.kuaishou.framework.datasource.n.util.DataSourceConfig
import com.kuaishou.framework.util.HostInfo
import kuaishou.common.BizDef

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-08-08
 */
@Suppress("EnumEntryName")
enum class AdCoreDataSource(private val forTest: AdCoreDataSource? = null,
                            private val usingNewZk: Boolean = false,
                            private val bizDef: BizDef = BizDef.AD) : DataSourceConfig {

    adDspTest(),
    adDspReadOnlyTest(),
    adChargeReadOnlyTest(),
    adDspChargeReadOnly(forTest = adDspReadOnlyTest, usingNewZk = true),
    adChargeBaseReadonly(forTest = adDspTest, usingNewZk = true),
    adDspEsReadOnly(forTest = adDspReadOnlyTest, usingNewZk = true),
    adDspDataReadOnly(forTest = adDspReadOnlyTest, usingNewZk = true),
    adDsp(forTest = adDspTest),
    adDspReadOnly(forTest = adDspReadOnlyTest, usingNewZk = true),
    adCoreRecordTest(),
    adCoreRecord(forTest = adCoreRecordTest, usingNewZk = true),
    adCoreRecordReadOnlyTest(),
    adCoreRecordReadOnly(forTest = adCoreRecordReadOnlyTest, usingNewZk = true),
    adDspAuditReadOnly(forTest = adDspReadOnlyTest, usingNewZk = true),
    adUcTest(),
    adUcDasReadOnly(forTest = adUcTest, usingNewZk = true),
    adBi(),
    adTest(),
    adDpaDataReadOnly(forTest = adChargeReadOnlyTest, usingNewZk = true),
    adDpaTest(),
    adDpa(forTest = adDspTest, usingNewZk = true),

    AdDspKbusReadOnly(forTest = adDspTest, usingNewZk = true),
    adCrmTest(),
    adCrm(forTest = adCrmTest, usingNewZk = true), // CRM平台业务相关库
    adNewAgent(forTest = adDspTest, usingNewZk = true),         // 代理商线上库
    adNewAgentReadOnly(forTest = adDspTest, usingNewZk = true), // 理商线上库
    matrix(forTest = adDspReadOnlyTest, usingNewZk = true),
    adDasTest(),
    adDas(forTest = adDasTest, usingNewZk = true),
    adAsset(forTest = adDspTest, usingNewZk = true),  // 素材库
    adLpTest(),
    adLp(forTest = adLpTest, usingNewZk = true), //魔力建站线上库
    adFinanceTest(),
    adFinanceReader(forTest = adFinanceTest, usingNewZk = true), // 天平
    adCore(forTest = adDspTest, usingNewZk = true),  // 投放平新shard库
    adDasBaseReadOnly(forTest = adDspTest, usingNewZk = true),  // 投放平新shard库 for Das benchmark
    adDasIncrementReadOnly(forTest = adDspTest, usingNewZk = true), // 投放平新shard库 for Das increment
    adCreativeCentorTest_adcoreimage(),
    adCreativeCentorTest_adapter(),
    adCreativeAdapter(forTest = adCreativeCentorTest_adapter,usingNewZk = true),
    adChargeUnify(forTest = adDspTest,usingNewZk = true),
    adChargeUnifyReadOnly(forTest = adDspTest,usingNewZk = true), //新计费库
    adCorePlus(forTest = adCreativeCentorTest_adapter,usingNewZk = true),
    ;

    override fun bizName(): String {
        if (forTest != null && HostInfo.debugHost()) {
            return forTest.name
        } else {
            return name
        }
    }

    override fun usingNewZk(): Boolean {
        return usingNewZk
    }

    override fun bizDef(): BizDef {
        return bizDef
    }

    companion object {

        private val map = values()
                .groupBy { it.name }
                .mapValues { it.value[0] }

        fun fromBizName(bizName: String): AdCoreDataSource? {
            return map[bizName]
        }
    }
}