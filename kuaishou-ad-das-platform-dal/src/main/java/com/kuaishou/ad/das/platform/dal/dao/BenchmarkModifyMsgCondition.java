package com.kuaishou.ad.das.platform.dal.dao;

import java.util.List;

import lombok.Data;

/**
 * @author Zhang Jialin <zhangjialin@kuaishou.com>
 * Created on 2021-06-17
 */
@Data
public class BenchmarkModifyMsgCondition {
    private String mainTable;
    private String dataSource;
    private List<Long> modifyIds;
    private List<Long> benchmarkIds;
}
