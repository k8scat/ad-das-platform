package com.kuaishou.ad.das.platform.dal.model;

import lombok.Data;

/**
 * @author Zhang Jialin <zhangjialin@kuaishou.com>
 * Created on 2021-06-14
 */
@Data
public class AdDasDownstreamUserInfo {
    /**
     * 用户组id
     */
    private Long id;
    /**
     * 用户组名称
     */
    private String userGroupName;
    /**
     * 负责人邮箱前缀
     */
    private String ownerEmailPrefix;
    /**
     * 负责人中文名
     */
    private String ownerName;
    /**
     * 是否删除
     */
    private Integer isDelete;
    /**
     * 创建时间
     */
    private Long createTime;
    /**
     * 更新时间
     */
    private Long updateTime;
}
