package com.kuaishou.ad.das.platform.dal.repository;

import java.util.List;

import com.kuaishou.ad.das.platform.dal.dao.BenchmarkModifyMsgCondition;
import com.kuaishou.ad.das.platform.dal.model.AdDasBenchmarkMsg;

/**
 * @author wangkai <wangkai14@kuaishou.com>
 * Created on 2021-01-05
 */
public interface AdDasBenchmarkMsgRepository {

    String TABLE_NAME = "ad_das_modify_benchmark_msg";

    int insert(AdDasBenchmarkMsg adDasBenchMarkMsg);

    AdDasBenchmarkMsg getByModifyId(Long modifyId);

    AdDasBenchmarkMsg getByTableAndDataSource(Long benchmarkId, String mainTable, String dataSource);

    List<AdDasBenchmarkMsg> getByModifyIds(List<Long> modifyId);

    AdDasBenchmarkMsg getById(Long id);

    AdDasBenchmarkMsg update(AdDasBenchmarkMsg adDasBenchmarkMsg);

    List<AdDasBenchmarkMsg> getByBenchmarkTypeAndMainTable(Long benchmarkId, String mainTable);

    List<AdDasBenchmarkMsg> query(BenchmarkModifyMsgCondition condition);

}
