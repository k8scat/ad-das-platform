package com.kuaishou.ad.das.platform.dal.repository.mapper;

import java.util.Collection;
import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.kuaishou.ad.das.platform.dal.model.AdDasIncrementBacktrackRecond;
import com.kuaishou.ad.das.platform.dal.model.AdDasIncrementBacktrackRecondDetail;
import com.kuaishou.infra.boot.jdbc.datasource.DataSourceRouting;

/**
 * @author caoyinhang <caoyinhang@kuaishou.com>
 * Created on 2021-07-06
 */

@Mapper
@DataSourceRouting("adDas")
public interface AdDasIncrementBacktackRecondMapper {

    @Select("SELECT * from ad_das_increment_backtrack_record "
            + " where id = #{id} ")
    AdDasIncrementBacktrackRecond listById(@Param("id") Long id);

    @Select("SELECT * from ad_das_increment_backtrack_record ")
    List<AdDasIncrementBacktrackRecond> listAll();

    @Insert("insert into ad_das_increment_backtrack_record(incre_id,operator,operation_time,durationSecond,status," +
            "application_rea)" +
            " values(#{increId},#{operator},#{operationTime},#{durationSecond}" +
            ",#{status},#{applicationRea})")
    Integer insert(AdDasIncrementBacktrackRecond adDasIncrementBacktrackRecond);

    @Select({"<script> " +
            " select * "+
            " from ad_das_increment_backtrack_record recond " +
            " left join ad_das_increment increment on increment.id = recond.incre_id" +
            " left join ad_das_increment_service_info service on recond.incre_id = service.incre_id" +
            " where 1=1 "+
            " <if test = ' incrementName != null and incrementName !=\"\" '> " +
            "and increment.increment_name=#{incrementName}" +
            " </if> " +
            " <if test = ' dataSource != null and dataSource !=\"\" '> " +
            " and increment.data_source = #{dataSource} " +
            " </if> " +
            " <if test = ' consumerGroup != null and consumerGroup !=\"\" '> " +
            " and increment.consumer_group = #{consumerGroup} " +
            " </if> " +
            " <if test = ' topic != null and topic !=\"\" '> " +
            " and increment.topic = #{topic} " +
            " </if> " +
            " <if test = ' status != 2 '> " +
            " and recond.status = #{status} " +
            " </if> " +
            " <if test = ' operator != null and operator !=\"\" '> " +
            " and recond.operator = #{operator} " +
            " </if> " +
            " <if test = ' beginTime != null and beginTime !=\"\" and endTime != null and endTime !=\"\" '> " +
            " and recond.operation_time between #{beginTime} and  #{endTime} " +
            " </if> " +
            " order by operation_time desc limit #{offset} , #{limit} " +
            "</script>"})
    List<AdDasIncrementBacktrackRecondDetail> queryByCondition(@Param("incrementName") String incrementName,
            @Param("dataSource") String dataSource,
            @Param("consumerGroup") String consumerGroup, @Param("topic") String topic,
            @Param("operator") String operator,
            @Param("status") Integer status, @Param("beginTime") String beginTime, @Param("endTime") String endTime,
            @Param("limit") Integer limit, @Param("offset") Integer offset);

    @Select({"<script> " +
            " select * "+
            " from ad_das_increment_backtrack_record \n" +
            " where 1=1 " +
            " <if test = \" increIds != null and increIds.size() != 0 \"> " +
            " and incre_id in " +
            " <foreach item = 'item' index = 'index' collection = 'increIds' open = '(' separator = ',' close = ')' > " +
            " #{item} " +
            " </foreach> " +
            " </if> " +
            "</script>"})
    List<AdDasIncrementBacktrackRecond> listByIncreId(@Param("increIds") Collection<Long> increIds);

    @Select("SELECT * from ad_das_increment_backtrack_record "
            + " where incre_id = #{increId} order by operation_time desc LIMIT 1")
    AdDasIncrementBacktrackRecond queryOperatorByIncreId(Long increId);

    @Select("SELECT * from ad_das_increment_backtrack_record "
            + " where status = 1 and operator = #{operator} order by operation_time desc LIMIT 1")
    AdDasIncrementBacktrackRecond queryByOperator(String operator);



    @Select({"<script> " +
            " select count(*) as dataCount "+
            " from ad_das_increment_backtrack_record recond " +
            " left join ad_das_increment increment on increment.id = recond.incre_id" +
            " left join ad_das_increment_service_info service on recond.incre_id = service.incre_id" +
            " where 1=1 "+
            " <if test = ' incrementName != null and incrementName !=\"\" '> " +
            "and increment.increment_name=#{incrementName}" +
            " </if> " +
            " <if test = ' dataSource != null and dataSource !=\"\" '> " +
            " and increment.data_source = #{dataSource} " +
            " </if> " +
            " <if test = ' consumerGroup != null and consumerGroup !=\"\" '> " +
            " and increment.consumer_group = #{consumerGroup} " +
            " </if> " +
            " <if test = ' topic != null and topic !=\"\" '> " +
            " and increment.topic = #{topic} " +
            " </if> " +
            " <if test = ' status != 2 '> " +
            " and recond.status = #{status} " +
            " </if> " +
            " <if test = ' operator != null and operator !=\"\" '> " +
            " and recond.operator = #{operator} " +
            " </if> " +
            " <if test = ' beginTime != null and beginTime !=\"\" and endTime != null and endTime !=\"\" '> " +
            " and recond.operation_time between #{beginTime} and  #{endTime} " +
            " </if> " +
            "</script>"})
    Integer queryCountByCondition(String incrementName, String dataSource, String consumerGroup, String topic, String operator, Integer status, String beginTime, String endTime);
}
