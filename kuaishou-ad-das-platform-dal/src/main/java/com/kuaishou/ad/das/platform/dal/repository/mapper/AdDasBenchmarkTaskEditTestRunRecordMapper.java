package com.kuaishou.ad.das.platform.dal.repository.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.kuaishou.ad.das.platform.dal.model.AdDasBenchmarkTaskEditTestRunRecord;
import com.kuaishou.infra.boot.jdbc.datasource.DataSourceRouting;

/**
 * @author wangkai <wangkai14@kuaishou.com>
 * Created on 2021-01-04
 */

@Mapper
@DataSourceRouting("adDas")
public interface AdDasBenchmarkTaskEditTestRunRecordMapper {

    @Insert("insert into ad_das_benchmark_task_edit_test_run_record(task_run_name,result_path,run_timestamp,task_num_list," +
            "task_id_list,task_run_status,task_run_detail,operator,create_time,update_time) " +
            "values(#{taskRunName},#{resultPath},#{runTimestamp},#{taskNumList},#{taskIdList}," +
            "#{taskRunStatus},#{taskRunDetail},#{operator},#{createTime},#{updateTime})")
    void insert(AdDasBenchmarkTaskEditTestRunRecord adDasBenchmarkTaskEditTestRunRecord);

    @Select("select * from ad_das_benchmark_task_edit_test_run_record where id>0")
    List<AdDasBenchmarkTaskEditTestRunRecord> getAll();

    @Update("update ad_das_benchmark_task_edit_test_run_record set task_run_status=#{taskRunStatus},operator=#{operator}," +
            " update_time=#{updateTime} where id=#{taskRecordId}")
    void update(@Param("taskRunStatus") Integer taskRunStatus, @Param("taskRecordId") Long taskRecordId,
                @Param("operator")String operator, @Param("updateTime") Long updateTime);
}
