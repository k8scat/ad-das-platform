package com.kuaishou.ad.das.platform.dal.repository.impl;

import static com.kuaishou.ad.das.platform.dal.datasource.AdDataSourceUtil.getBaseChargeRead;
import static com.kuaishou.ad.das.platform.dal.datasource.AdDataSourceUtil.getChargeShardId;

import java.util.List;

import org.springframework.context.annotation.Lazy;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import com.google.common.collect.Lists;
import com.kuaishou.ad.das.platform.dal.model.AdDasBenchmark;
import com.kuaishou.ad.das.platform.dal.model.AdDspAccountBalance;
import com.kuaishou.ad.das.platform.dal.repository.AdDasChargeAccountBalanceRepository;
import com.kuaishou.framework.util.HostInfo;

import lombok.extern.slf4j.Slf4j;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-08-06
 */
@Lazy
@Slf4j
@Repository
public class AdDasChargeAccountBalanceRepositoryImpl implements AdDasChargeAccountBalanceRepository {

    @Override
    public AdDspAccountBalance queryById(Long accountId) {

        String tableName = "ad_dsp_account_balance";

        if (!HostInfo.debugHost()) {
            tableName = "ad_dsp_account_balance_" + getChargeShardId(accountId);
        }

        String sql = "select * from " + tableName + " where account_id = " + accountId ;

        MapSqlParameterSource param = new MapSqlParameterSource();
        StringBuilder sb = new StringBuilder("SELECT * FROM %s WHERE is_delete = 0");

        List<AdDspAccountBalance> dspAccountBalances = getBaseChargeRead(getChargeShardId(accountId))
                .query(sql, param, new BeanPropertyRowMapper<>(AdDspAccountBalance.class));
        log.info("sql={}", sql);
        log.info("dspAccountBalances={}", dspAccountBalances);

        if (CollectionUtils.isEmpty(dspAccountBalances)) {
            return null;
        } else {
            return dspAccountBalances.get(0);
        }
    }

    @Override
    public List<AdDspAccountBalance> batchQueryData(Long shardId, List<Long> accountIds) {

        if (CollectionUtils.isEmpty(accountIds)) {
            return Lists.newArrayList();
        }

        String tableName = "ad_dsp_account_balance";

        if (!HostInfo.debugHost()) {
            tableName = "ad_dsp_account_balance_" + shardId;
        }

        String sql = "select * from " + tableName +" where account_id in (";

        for (Long accountId : accountIds) {
            if (sql.endsWith("(")) {
                sql = sql + accountId;
            } else {
                sql = sql + ",";
                sql = sql + accountId;
            }
        }
        sql = sql + ")";
        List<AdDspAccountBalance> dspAccountBalances = getBaseChargeRead(shardId).getJdbcTemplate()
                .queryForList(sql, AdDspAccountBalance.class);

        log.info("sql={}, dspAccountBalance.size", sql, dspAccountBalances.size());

        return dspAccountBalances;
    }
}
