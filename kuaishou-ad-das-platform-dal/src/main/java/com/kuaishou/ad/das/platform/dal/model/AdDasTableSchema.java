package com.kuaishou.ad.das.platform.dal.model;

import java.sql.ResultSet;
import java.sql.SQLException;

import lombok.Data;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-11-30
 */
@Data
public class AdDasTableSchema extends AdDasAbstarctModel<AdDasTableSchema> {

    private String tableName;

    private String tableComment;

    private String createTime;

    @Override
    public AdDasTableSchema parseResult(ResultSet rs) throws SQLException {

        this.setTableName(rs.getString("table_name"));
        this.setTableComment(rs.getString("table_comment"));
        this.setCreateTime(rs.getString("create_time"));

        return this;
    }
}
