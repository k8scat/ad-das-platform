package com.kuaishou.ad.das.platform.dal.repository.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.kuaishou.ad.das.platform.dal.model.AdDasBenchmarkMsg;
import com.kuaishou.infra.boot.jdbc.datasource.DataSourceRouting;

/**
 * @author wangkai <wangkai14@kuaishou.com>
 * Created on 2020-12-31
 */
@Mapper
@DataSourceRouting("adDas")
public interface AdDasBenchmarkMsgMapper {

    @Insert("insert into ad_das_modify_benchmark_msg(modify_id,data_source,main_table," +
            "pb_class,column_schema,cascade_column_schema,table_type,predict_growth," +
            "common_sqls,time_params,shard_sql_flag,shard_sql_column,create_time,delete_table,update_time,benchmark_id) " +
            "values(#{modifyId},#{dataSource},#{mainTable}," +
            "#{pbClass},#{columnSchema},#{cascadeColumnSchema},#{tableType},#{predictGrowth},#{commonSqls},#{timeParams}," +
            "#{shardSqlFlag},#{shardSqlColumn},#{createTime},#{deleteTable},#{updateTime},#{benchmarkId})")
    int insert(AdDasBenchmarkMsg adDasBenchMarkMsg);

    @Select("select * from ad_das_modify_benchmark_msg where modify_id=#{modifyId}")
    AdDasBenchmarkMsg getByModifyId(@Param("modifyId") Long modifyId);

    @Select({"<script> " +
            " select * "+
            " from ad_das_modify_benchmark_msg \n" +
            " where 1=1 " +
            " <if test = \" modifyId != null and modifyId.size() != 0\"> " +
            " and modify_id in " +
            " <foreach item = 'item' index = 'index' collection = 'modifyId' open = '(' separator = ',' close = ')' > " +
            " #{item} " +
            " </foreach> " +
            " </if> " +
            "</script>"})
    List<AdDasBenchmarkMsg> getByModifyIds(@Param("modifyId") List<Long> modifyId);

    @Select("select * from ad_das_modify_benchmark_msg where id=#{id}")
    AdDasBenchmarkMsg getById(@Param("id") Long id);

    @Select("select * from ad_das_modify_benchmark_msg " +
            " where benchmark_id=#{benchmarkId} and main_table=#{mainTable} and data_source=#{dataSource} order by modify_id desc limit 1")
    AdDasBenchmarkMsg getByTableAndDataSource(@Param("benchmarkId") Long benchmarkId,
                                              @Param("mainTable") String mainTable,
                                              @Param("dataSource") String dataSource);

    @Update("update ad_das_modify_benchmark_msg set modify_id=#{modifyId},benchmark_id=#{benchmarkId}," +
            "data_source=#{dataSource},main_table=#{mainTable}," +
            "pb_class=#{pbClass},column_schema=#{columnSchema}," +
            "cascade_column_schema=#{cascadeColumnSchema}," +
            "table_type=#{tableType},predict_growth=#{predictGrowth}," +
            "common_sqls=#{commonSqls},time_params=#{timeParams}," +
            "shard_sql_flag=#{shardSqlFlag},shard_sql_column=#{shardSqlColumn}," +
            "create_time=#{createTime},update_time=#{updateTime},benchmark_id=#{benchmarkId} " +
            "where id=#{id}")
    void update(AdDasBenchmarkMsg adDasBenchmarkMsg);

    @Select("select * from ad_das_modify_benchmark_msg where benchmark_id=#{benchmarkId} and main_table=#{mainTable}")
    List<AdDasBenchmarkMsg> getByBenchmarkTypeAndMainTable(@Param("benchmarkId") Long benchmarkId, @Param("mainTable") String mainTable);
}