package com.kuaishou.ad.das.platform.dal.repository.common;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import com.kuaishou.ad.das.dal.datasource.AdCoreDataSource;


/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-11-29
 */
public abstract class AdDasAbstractRepository<BO> {

    protected static final JdbcTemplate jdbcTemplate = AdCoreDataSource.adDas.read().getJdbcTemplate();

    protected static final JdbcTemplate jdbcTemplateWrite = AdCoreDataSource.adDas.write().getJdbcTemplate();

    protected abstract RowMapper<BO> getRowMapper();

    protected abstract JdbcTemplate getJdbcTemplate(String dataSource);
}
