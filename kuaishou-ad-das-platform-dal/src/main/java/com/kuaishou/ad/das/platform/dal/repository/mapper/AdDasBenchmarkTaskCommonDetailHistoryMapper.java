package com.kuaishou.ad.das.platform.dal.repository.mapper;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.kuaishou.ad.das.platform.dal.model.AdDasBenchmarkTaskCommonDetailHistory;
import com.kuaishou.infra.boot.jdbc.datasource.DataSourceRouting;

/**
 * @author wangkai <wangkai14@kuaishou.com>
 * Created on 2021-01-10
 */

@Mapper
@DataSourceRouting("adDas")
public interface AdDasBenchmarkTaskCommonDetailHistoryMapper {

    @Insert("insert into ad_das_benchmark_task_common_detail_history(modify_id,main_table,table_type," +
            "column_schema,cascade_column_schema,common_sqls,time_params,predict_growth,shard_sql_column," +
            "shard_sql_flag,operator,create_time,update_time,benchmark_id,data_source,pb_class) values(#{modifyId},#{mainTable},#{tableType}," +
            "#{columnSchema},#{cascadeColumnSchema},#{commonSqls},#{timeParams},#{predictGrowth},#{shardSqlColumn}," +
            "#{shardSqlFlag},#{operator},#{createTime},#{updateTime},#{benchmarkId},#{dataSource},#{pbClass})")
    int insert(AdDasBenchmarkTaskCommonDetailHistory adDasBenchmarkTaskCommonDetailHistory);

    @Select("select * from ad_das_benchmark_task_common_detail_history where modify_id=#{modifyId}")
    AdDasBenchmarkTaskCommonDetailHistory getByModifyId(@Param("modifyId") long modifyId);
}
