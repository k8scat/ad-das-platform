package com.kuaishou.ad.das.platform.dal.repository;

import java.util.List;

import com.kuaishou.ad.das.platform.dal.dao.AdDasUserGroupBindCondition;
import com.kuaishou.ad.das.platform.dal.model.AdDasDownstreamUserRelationBind;

/**
 * @author Zhang Jialin <zhangjialin@kuaishou.com>
 * Created on 2021-06-11
 */
public interface AdDasDownstreamUserRelationRepository {
    String TABLE_NAME = "ad_das_downstream_user_relation";

    /**
     * insert
     * 创建/修改时间方法内会用系统当前毫秒时间戳覆盖
     * @param param 实体
     */
    void insert(AdDasDownstreamUserRelationBind param);

    /**
     * insert
     * 创建/修改时间方法内会用系统当前毫秒时间戳覆盖
     * @param param 实体
     */
    void insert(List<AdDasDownstreamUserRelationBind> param);

    /**
     * update
     * 主键字段作为where查询条件用于指定更新的记录
     * 更新时间字段在方法内会使用系统当前毫秒时间戳覆盖
     * @param param 实体
     */
    void update(AdDasDownstreamUserRelationBind param);

    /**
     * 查询列表
     * @param condition 查询条件
     * @return 数据列表
     */
    List<AdDasDownstreamUserRelationBind> query(AdDasUserGroupBindCondition condition);

    void delete(AdDasUserGroupBindCondition condition);

}
