package com.kuaishou.ad.das.platform.dal.repository.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import com.kuaishou.ad.das.platform.dal.model.AdDasBenchmarkTaskCommonDetailHistory;
import com.kuaishou.ad.das.platform.dal.repository.AdDasBenchmarkTaskCommonDetailHistoryRepository;
import com.kuaishou.ad.das.platform.dal.repository.mapper.AdDasBenchmarkTaskCommonDetailHistoryMapper;

/**
 * @author wangkai <wangkai14@kuaishou.com>
 * Created on 2021-01-10
 */
@Lazy
@Service
public class AdDasBenchmarkTaskCommonDetailHistoryRepositoryImpl implements AdDasBenchmarkTaskCommonDetailHistoryRepository {

    @Autowired
    private AdDasBenchmarkTaskCommonDetailHistoryMapper adDasBenchmarkTaskCommonDetailHistoryMapper;
    @Override
    public int insert(AdDasBenchmarkTaskCommonDetailHistory adDasBenchmarkTaskCommonDetailHistory) {
        return adDasBenchmarkTaskCommonDetailHistoryMapper.insert(adDasBenchmarkTaskCommonDetailHistory);
    }

    @Override
    public AdDasBenchmarkTaskCommonDetailHistory getByModifyId(long modifyId) {
        return adDasBenchmarkTaskCommonDetailHistoryMapper.getByModifyId(modifyId);
    }
}
