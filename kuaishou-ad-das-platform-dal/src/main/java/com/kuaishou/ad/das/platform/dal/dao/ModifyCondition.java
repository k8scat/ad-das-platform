package com.kuaishou.ad.das.platform.dal.dao;

import java.util.List;

import org.apache.commons.lang3.tuple.Pair;

import lombok.Data;

/**
 * @author Zhang Jialin <zhangjialin@kuaishou.com>
 * Created on 2021-06-16
 */
@Data
public class ModifyCondition {
    private List<Long> modifyIds;
    private List<Integer> modifyType;
    private List<Integer> modifyStatus;
    private String operator;
    private String modifyName;
    private Pair<Long, Long> startTime2EndTime;
    private Integer pageSize = 0;
    private Integer pageNum = 0;
}
