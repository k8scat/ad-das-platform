package com.kuaishou.ad.das.platform.dal.repository;

import com.kuaishou.ad.das.platform.dal.model.AdDasBenchmarkTaskCommonDetailHistory;

/**
 * @author wangkai <wangkai14@kuaishou.com>
 * Created on 2021-01-10
 */
public interface AdDasBenchmarkTaskCommonDetailHistoryRepository {

    int insert(AdDasBenchmarkTaskCommonDetailHistory adDasBenchmarkTaskCommonDetailHistory);

    AdDasBenchmarkTaskCommonDetailHistory getByModifyId(long modifyId);
}
