package com.kuaishou.ad.das.platform.dal.repository.mapper;

import java.util.Collection;
import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.kuaishou.ad.das.platform.dal.model.AdDasIncrementTableDetail;
import com.kuaishou.infra.boot.jdbc.datasource.DataSourceRouting;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-06-09
 */
@Mapper
@DataSourceRouting("adDas")
public interface AdDasIncrementTableDetailMapper {

    @Select("SELECT * from ad_das_increment_table_detail "
            + " where incre_id = #{increId} ")
    List<AdDasIncrementTableDetail> listInfoCondition(@Param("increId") String increId);

    @Select("SELECT * from ad_das_increment_table_detail "
            + " where incre_table = #{tableName} ")
    List<AdDasIncrementTableDetail> queryByTable(@Param("tableName") String tableName);

    @Select({"<script> " +
            " select * "+
            " from ad_das_increment_table_detail \n" +
            " where 1=1 " +
            " <if test = \" increIds != null and increIds.size() != 0 \"> " +
            " and incre_id in " +
            " <foreach item = 'item' index = 'index' collection = 'increIds' open = '(' separator = ',' close = ')' > " +
            " #{item} " +
            " </foreach> " +
            " </if> " +
            "</script>"})
    List<AdDasIncrementTableDetail> listByIncreId(@Param("increIds") Collection<Long> increIds);

    @Select("SELECT * from ad_das_increment_table_detail "
            + " where incre_id = #{increId} and incre_table = #{increTable}")
    AdDasIncrementTableDetail listByTable(@Param("increId") Long increId, @Param("increTable") String increTable);

    @Select({"<script> " +
            " select * "+
            " from ad_das_increment_table_detail \n" +
            " where 1=1 and incre_id = #{increId} " +
            " <if test = \" increTables != null and increTables.size() != 0 \"> " +
            " and incre_table in " +
            " <foreach item = 'item' index = 'index' collection = 'increTables' open = '(' separator = ',' close = ')' > " +
            " #{item} " +
            " </foreach> " +
            " </if> " +
            "</script>"})
    List<AdDasIncrementTableDetail> listByTables(@Param("increId") Long increId, @Param("increTables") Collection<String> increTables);

    @Insert("insert into ad_das_increment_table_detail(incre_id,modify_id,data_source,incre_table,table_type,primary_id_key,table_column,pb_class_enum,operator,approver," +
            "create_time,update_time,cascade_schema,cascaded_table)" +
            " values(#{increId},#{modifyId},#{dataSource},#{increTable},#{tableType},#{primaryIdKey}" +
            ",#{tableColumn},#{pbClassEnum},#{operator},#{approver},#{createTime},#{updateTime},#{cascadeSchema},#{cascadedTable})")
    Integer insert(AdDasIncrementTableDetail adDasIncrementTableDetail);

    @Delete("delete from ad_das_increment_table_detail where incre_id=#{increId}")
    void deleteByIncreId(@Param("increId") Long increId);

    @Delete("<script> " +
            " delete from ad_das_increment_table_detail where incre_id=#{increId} " +
            " <if test = \" tableNames != null and tableNames.size() != 0 \" >" +
            " and incre_table in " +
            " <foreach item = 'item' index = 'index' collection = 'tableNames' open = '(' separator = ',' close = ')'> " +
            " #{item}" +
            " </foreach> " +
            " </if>" +
            "</script>")
    void deleteByIncreIdAndTables(@Param("increId")Long increId,
                                  @Param("tableNames")Collection<String> tableNames);
}
