package com.kuaishou.ad.das.platform.dal.repository.impl;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Repository;

import com.kuaishou.ad.das.platform.dal.model.AdDasIncrement;
import com.kuaishou.ad.das.platform.dal.repository.AdDasIncrementRepository;
import com.kuaishou.ad.das.platform.dal.repository.mapper.AdDasIncrementMapper;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-06-08
 */
@Lazy
@Repository
public class AdDasIncrementRepositoryImpl implements AdDasIncrementRepository {

    @Autowired
    private AdDasIncrementMapper adDasIncrementMapper;

    @Override
    public int insert(AdDasIncrement adDasIncrement) {
        return adDasIncrementMapper.insert(adDasIncrement);
    }

    @Override
    public void update(AdDasIncrement adDasIncrement) {
        adDasIncrementMapper.update(adDasIncrement);
    }

    @Override
    public void deleteById(Long dataId) {
        adDasIncrementMapper.deletById(dataId);
    }

    @Override
    public List<AdDasIncrement> queryByCondition(Set<Long> increIds, Long dataStreamGroupId, String dataSource,
                                                 String dataTopic) {
       return adDasIncrementMapper.queryByCondition(increIds, dataStreamGroupId, dataSource, dataTopic);
    }

    @Override
    public List<AdDasIncrement> queryByIncreId(Collection<Long> increId) {
        return adDasIncrementMapper.listByIncreId(increId);
    }

    @Override
    public AdDasIncrement queryById(Long id) {
        return adDasIncrementMapper.listById(id);
    }

    @Override
    public AdDasIncrement queryByIncrementName(String incrementName) {
        return adDasIncrementMapper.queryByIncrementName(incrementName);
    }

    @Override
    public List<AdDasIncrement> queryAll() {
        return adDasIncrementMapper.listAll();
    }

    @Override
    public List<AdDasIncrement> searchByCondition(List<Long> groupIds, List<String> topics, List<String> dataSources) {
        return adDasIncrementMapper.searchByCondition(groupIds, topics, dataSources);
    }

    @Override
    public AdDasIncrement queryByConditionBacktrack(String incrName, String dataSource, String consumerGroup,
            String topic) {
        return adDasIncrementMapper.queryByConditionBacktrack(incrName,dataSource,consumerGroup,topic);
    }
}
