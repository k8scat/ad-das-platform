package com.kuaishou.ad.das.platform.dal.repository.impl;

import java.util.List;

import org.springframework.context.annotation.Lazy;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.kuaishou.ad.das.platform.dal.model.AdDasModifyServicePbRecord;
import com.kuaishou.ad.das.platform.dal.repository.AdDasModifyServicePbRecordRepository;
import com.kuaishou.ad.das.platform.dal.repository.common.AdDasAbstractRepository;


/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-12-09
 */
@Lazy
@Repository
public class AdDasModifyServicePbRecordRepositoryImpl
        extends AdDasAbstractRepository<AdDasModifyServicePbRecord>
        implements AdDasModifyServicePbRecordRepository {

    @Override
    protected RowMapper<AdDasModifyServicePbRecord> getRowMapper() {
        return (rs, row) -> {
            AdDasModifyServicePbRecord adDasModifyServicePbRecord = new AdDasModifyServicePbRecord();
            adDasModifyServicePbRecord.parseResult(rs);
            return adDasModifyServicePbRecord;
        };
    }

    @Override
    protected JdbcTemplate getJdbcTemplate(String dataSource) {
        return jdbcTemplate;
    }


    @Override
    public int insert(AdDasModifyServicePbRecord adDasModifyServicePbRecord) {

        String insertSql  = "insert into ad_das_modify_service_pb_record(modify_id,service_name,service_type,"
                + "service_node_name,"
                + "cur_pb_version,target_pb_version,publish_status,create_time,update_time) "
                + "values(?,?,?,?,?,?,?,?,?)";

        return jdbcTemplateWrite.update(insertSql, adDasModifyServicePbRecord.getModifyId(),
                adDasModifyServicePbRecord.getServiceName(), adDasModifyServicePbRecord.getServiceType(),
                adDasModifyServicePbRecord.getServiceNodeName(), adDasModifyServicePbRecord.getCurPbVersion(),
                adDasModifyServicePbRecord.getTargetPbVersion(), adDasModifyServicePbRecord.getPublishStatus(),
                adDasModifyServicePbRecord.getCreateTime(),adDasModifyServicePbRecord.getUpdateTime());
    }

    @Override
    public List<AdDasModifyServicePbRecord> queryByCondition(Long modifyId, String serviceName,
                                                             String serviceNodeName) {

        String querySql = "select * from ad_das_modify_service_pb_record "
                + "where modify_id=? and service_name=? and service_node_name=?";

        return jdbcTemplate.query(querySql, getRowMapper(), modifyId, serviceName, serviceNodeName);
    }

    @Override
    public int updatePublishStatus(Long modifyId, Integer publishStatus, String serviceName, String serviceNodeName) {

        String updateSql = "update ad_das_modify_service_pb_record set publish_status=? "
                + "where modify_id=? and service_name=? and service_node_name=?";

        return jdbcTemplateWrite.update(updateSql, publishStatus,
                modifyId, serviceName, serviceNodeName);
    }
}
