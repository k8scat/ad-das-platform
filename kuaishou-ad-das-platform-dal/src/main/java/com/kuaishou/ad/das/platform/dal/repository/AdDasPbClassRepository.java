package com.kuaishou.ad.das.platform.dal.repository;

import java.util.List;

import com.kuaishou.ad.das.platform.dal.model.AdDasPbClass;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-11-29
 */
public interface AdDasPbClassRepository {

    long insert(AdDasPbClass adDasPbClass);

    long updateStatus(AdDasPbClass adDasPbClass);

    long updatePbClassColumnCount(Long id, String pbClass,
                                  Integer pbClassColumnCount, String operator);

    long updatePbClassColumnCount(String pbVersion, String pbClass,
                                  Integer pbClassColumnCount, String operator);

    List<AdDasPbClass> query(String pbVersion, Integer pbClassStatus);

    List<AdDasPbClass> queryLikePbClass(String pbVersion,String pbClass, Integer pbClassStatus);

    int count(String pbVersion, String pbClass, Integer pbClassStatus);

    List<AdDasPbClass> queryByPbClass(String pbVersion,String pbClass, Integer pbClassStatus);

}
