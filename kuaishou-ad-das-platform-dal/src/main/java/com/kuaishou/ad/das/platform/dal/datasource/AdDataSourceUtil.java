package com.kuaishou.ad.das.platform.dal.datasource;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import com.google.common.collect.Lists;
import com.kuaishou.ad.das.dal.datasource.AdCoreDataSource;
import com.kuaishou.constant.DataSources;
import com.kuaishou.env.util.EnvUtils;
import com.kuaishou.framework.util.HostInfo;
import com.kuaishou.infra.framework.datasource.util.DataSourceConfig;

import lombok.Synchronized;
import lombok.extern.slf4j.Slf4j;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-08-08
 */
@Slf4j
public class AdDataSourceUtil {

    private static final Map<DataSources, NamedParameterJdbcTemplate> MAP = new HashMap<>();

    protected static int defaultShardCount = EnvUtils.belowCandidate() ? 1 : 100;


    public static long getChargeShardId(Long accountId) {

        return Math.abs(accountId % defaultShardCount);
    }
    /**
     * 查询计费库
     * @param shardId
     * @return
     */
    public static NamedParameterJdbcTemplate getBaseChargeRead(Long shardId) {

        if (EnvUtils.belowCandidate()) {
            return AdCoreDataSource.adChargeBaseReadonly.read();
        }

        return AdCoreDataSource.adChargeBaseReadonly.shardRead(shardId);
    }

    /**
     * 保证获取的和JdbcTemplate是同一个DataSource
     *
     * @param name
     * @return
     */
    public static DataSource getDataSourceWrite(DataSources name) {
        return getJdbcTemplateWrite(name).getJdbcTemplate().getDataSource();
    }

    /**
     * 缓存使用相同jdbcTemplate
     *
     * @param name
     * @return
     */
    @Synchronized
    public static NamedParameterJdbcTemplate getJdbcTemplateWrite(DataSources name) {
        MAP.computeIfAbsent(name, DataSourceConfig::write);
        return MAP.get(name);
    }

    /**
     * 风控DataSource
     * @return
     */
    public static NamedParameterJdbcTemplate getRiskDataSourceRead() {

        return null;
    }

    /**
     * Uc DataSource
     * @return
     */
    public static NamedParameterJdbcTemplate getUcDataSourceRead() {
        return AdCoreDataSource.adUcDasReadOnly.read();
    }

    /**
     * crm DataSource
     * @return
     */
    public static NamedParameterJdbcTemplate getCrmDataSourceRead() {
        return AdCoreDataSource.adCrm.read();
    }

    /**
     * agentNew dataSource read
     * @return
     */
    public static NamedParameterJdbcTemplate getAgentNewDataSourceRead() {

        return AdCoreDataSource.adNewAgentReadOnly.read();
    }

    /**
     * agentNew dataSource write
     * @return
     */
    public static NamedParameterJdbcTemplate getAgentNewDataSourceWrite() {

        return AdCoreDataSource.adNewAgentReadOnly.write();
    }

    /**
     * finance dataSource read
     * @return
     */
    public static NamedParameterJdbcTemplate getFinanceDataSourceRead() {
        return AdCoreDataSource.adFinanceReader.read();
    }

    public static JdbcTemplate getDasIncremnetJdbcTemplate(long shardId) {

        if (HostInfo.debugHost()) {
            return AdCoreDataSource.adDasIncrementReadOnly.read().getJdbcTemplate();
        }

        return AdCoreDataSource.adDasIncrementReadOnly.shardRead(shardId).getJdbcTemplate();
    }

    /**
     * 3344 拆库数据源切换
     * @param dataBase
     * @param tableName
     * @param shardId
     * @return
     */
    public static NamedParameterJdbcTemplate getAdCoreDataSourceSwitch(
            String dataBase, String tableName, long shardId) {

        // 开启切换表 到新库
        if ("adDasBaseReadOnly".equals(dataBase)) {
            return AdCoreDataSource.adDasBaseReadOnly.shardRead(shardId);
        }
        if("adCorePlus".equals(dataBase)) {
            return AdCoreDataSource.adCorePlus.shardRead(shardId);
        }
        if (DATABASE_TEMPLATE_SET.contains(dataBase)) {
            return getDataBase(dataBase);
        }

        return AdCoreDataSource.adDspDataReadOnly.read();
    }

    /**
     * 增量数据源
     * @param dataSource
     * @param shardId
     * @return
     */
    public static JdbcTemplate getDasIncremnetJdbcTemplate(String dataSource, long shardId) {

        if (HostInfo.debugHost()) {
            return AdCoreDataSource.valueOf(dataSource).read().getJdbcTemplate();
        }

        return AdCoreDataSource.valueOf(dataSource).shardRead(shardId).getJdbcTemplate();
    }


    public static final Set<String> DATABASE_TEMPLATE_SET = new HashSet() {
        {
            add("adDspReadOnly");
            add("adDspEsReadOnly");
            add("adDspAuditReadOnly");
            add("adDspDataReadOnly");
            add("adData");
            add("adDpa");
            add("adTestStaging");
            add("adCrmStaging");
            add("adBiStaging");
            add("adDspStaging");
            add("adCreativeCentorTest_adcoreimage");
            add("adCreativeCentorTest_adapter");
            add("adCreativeAdapter");
        }
    };

    public static NamedParameterJdbcTemplate getDataBase(String dataBase) {
        
        return null;
    }


    /**
     * 查询当前所有 DataSource
     * @return
     */
    public static List<String> getAllDataSource() {

        List<String> result = Lists.newArrayList();

        AdCoreDataSource[] dataSources = AdCoreDataSource.values();

        for (AdCoreDataSource adCoreDataSource : dataSources) {
            if (!adCoreDataSource.name().endsWith("Test")) {
                result.add(adCoreDataSource.name());
            }
        }

        return result;
    }

}
