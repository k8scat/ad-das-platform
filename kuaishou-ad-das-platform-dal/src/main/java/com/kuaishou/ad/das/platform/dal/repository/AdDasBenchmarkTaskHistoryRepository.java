package com.kuaishou.ad.das.platform.dal.repository;

import java.util.List;

import com.kuaishou.ad.das.platform.dal.model.AdDasBenchmarkTaskHistory;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-07-13
 */
public interface AdDasBenchmarkTaskHistoryRepository {

    int insert(AdDasBenchmarkTaskHistory adDasBenchmarkTaskHistory);

    List<AdDasBenchmarkTaskHistory> listByModifyId(Long modifyId);
}
