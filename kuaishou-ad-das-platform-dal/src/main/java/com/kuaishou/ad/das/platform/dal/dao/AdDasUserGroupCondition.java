package com.kuaishou.ad.das.platform.dal.dao;

import java.util.List;

import lombok.Data;

/**
 * @author Zhang Jialin <zhangjialin@kuaishou.com>
 * Created on 2021-06-14
 */
@Data
public class AdDasUserGroupCondition {
    /**
     * 用户组id
     */
    private List<Long> userGroupIds;
    /**
     * 用户组名称
     */
    private String userGroupName;
    /**
     * 负责人邮箱前缀
     */
    private String ownerEmailPrefix;
    /**
     * 负责人中文名
     */
    private String ownerName;
}
