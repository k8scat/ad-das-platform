package com.kuaishou.ad.das.platform.dal.repository.mapper;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.kuaishou.ad.das.platform.dal.model.AdDasIncrement;
import com.kuaishou.infra.boot.jdbc.datasource.DataSourceRouting;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-06-09
 */
@Mapper
@DataSourceRouting("adDas")
public interface AdDasIncrementMapper {

    @Select("SELECT * from ad_das_increment "
            + " where id = #{id} ")
    AdDasIncrement listById(@Param("id") Long id);

    @Select("SELECT * from ad_das_increment "
            + " where increment_name = #{incrementName} ")
    AdDasIncrement queryByIncrementName(@Param("incrementName") String incrementName);

    @Select("SELECT * from ad_das_increment ")
    List<AdDasIncrement> listAll();

    @Insert("insert into ad_das_increment(increment_code,increment_name,increment_desc,data_source,consumer_group,data_stream_group_id,topic,operator_email_prefix," +
            "is_delete,incre_stream_type,incre_stream_related_id,create_time,update_time)" +
            " values(#{incrementCode},#{incrementName},#{incrementDesc},#{dataSource},#{consumerGroup},#{dataStreamGroupId},#{topic}" +
            ",#{operatorEmailPrefix},#{isDelete},#{increStreamType},#{increStreamRelatedId},#{createTime},#{updateTime})")
    Integer insert(AdDasIncrement adDasIncrement);

    @Update({"<script> " +
            " update ad_das_increment set update_time=#{updateTime},"+
            " <if test = \" incrementCode != null and incrementCode != '' \"> " +
            " increment_code=#{incrementCode}, " +
            " </if> " +
            " <if test = \" incrementName != null and incrementName != '' \"> " +
            " increment_name=#{incrementName}, " +
            " </if> " +
            " <if test = \" incrementDesc != null and incrementDesc != '' \"> " +
            " increment_desc=#{incrementDesc}, " +
            " </if> " +
            " <if test = \" dataSource != null and dataSource != '' \"> " +
            " data_source=#{dataSource}, " +
            " </if> " +
            " <if test = \" consumerGroup != null and consumerGroup != '' \"> " +
            " consumer_group=#{consumerGroup}, " +
            " </if> " +
            " <if test = \" dataStreamGroupId != null and dataStreamGroupId != 0 \"> " +
            " data_stream_group_id=#{dataStreamGroupId}, " +
            " </if> " +
            " <if test = \" topic != null and topic != '' \"> " +
            " topic=#{topic}, " +
            " </if> " +
            " <if test = \" operatorEmailPrefix != null and operatorEmailPrefix != '' \"> " +
            " hdfs_path=#{operatorEmailPrefix}, " +
            " </if> " +
            " <if test = \" isDelete != null \"> " +
            " isDelete=#{isDelete}, " +
            " </if> " +
            " <if test = \" increStreamType != null \"> " +
            " incre_stream_type=#{increStreamType}, " +
            " </if> " +
            " <if test = \" increStreamRelatedId != null and increStreamRelatedId != 0 \"> " +
            " incre_stream_related_id=#{increStreamRelatedId} " +
            " </if> " +
            " where id=#{id}" +
            "</script>"})
    void update(AdDasIncrement adDasIncrement);

    @Delete("delete from ad_das_increment where id=#{dataId}")
    void deletById(@Param("dataId") Long dataId);

    @Select({"<script> " +
            " select * "+
            " from ad_das_increment \n" +
            " where 1=1 and data_stream_group_id=#{dataStreamGroupId}" +
            " <if test = \" dataSource != null \"> " +
            " and data_source = #{dataSource} " +
            " </if> " +
            " <if test = \" dataTopic != null \"> " +
            " and topic = #{dataTopic} " +
            " </if> " +
            " <if test = \" increIds != null and increIds.size() != 0 \"> " +
            " and id in " +
            " <foreach item = 'item' index = 'index' collection = 'increIds' open = '(' separator = ',' close = ')' > " +
            " #{item} " +
            " </foreach> " +
            " </if> " +
            "</script>"})
    List<AdDasIncrement> queryByCondition(@Param("increIds") Set<Long> increIds, @Param("dataStreamGroupId") Long dataStreamGroupId,
                                          @Param("dataSource") String dataSource, @Param("dataTopic") String dataTopic);

    @Select({"<script> " +
            " select * "+
            " from ad_das_increment \n" +
            " where 1=1 " +
            " <if test = \" groupIds != null and groupIds.size() != 0 \"> " +
            " and data_stream_group_id in " +
            " <foreach item = 'item' index = 'index' collection = 'groupIds' open = '(' separator = ',' close = ')' > " +
            " #{item} " +
            " </foreach> " +
            " </if> " +
            " <if test = \" topics != null and topics.size() != 0 \"> " +
            " and topic in " +
            " <foreach item = 'item' index = 'index' collection = 'topics' open = '(' separator = ',' close = ')' > " +
            " #{item} " +
            " </foreach> " +
            " </if> " +
            " <if test = \" dataSources != null and dataSources.size() != 0 \"> " +
            " and data_source in " +
            " <foreach item = 'item' index = 'index' collection = 'dataSources' open = '(' separator = ',' close = ')' > " +
            " #{item} " +
            " </foreach> " +
            " </if> " +
            "</script>"})
    List<AdDasIncrement> searchByCondition(@Param("groupIds") List<Long> groupIds, @Param("topics") List<String> topics,
                                           @Param("dataSources") List<String> dataSources);

    @Select({"<script> " +
            " select * "+
            " from ad_das_increment \n" +
            " where 1=1 " +
            " <if test = \" increIds != null and increIds.size() != 0 \"> " +
            " and id in " +
            " <foreach item = 'item' index = 'index' collection = 'increIds' open = '(' separator = ',' close = ')' > " +
            " #{item} " +
            " </foreach> " +
            " </if> " +
            "</script>"})
    List<AdDasIncrement> listByIncreId(@Param("increIds") Collection<Long> increIds);


    @Select({"<script> " +
            " select * "+
            " from ad_das_increment \n" +
            " where 1=1 "+
            " <if test = ' incrementName != null and incrementName !=\"\" '> " +
            "and increment_name=#{incrementName}" +
            " </if> " +
            " <if test = ' dataSource != null and dataSource !=\"\" '> " +
            " and data_source = #{dataSource} " +
            " </if> " +
            " <if test = ' consumerGroup != null and consumerGroup !=\"\" '> " +
            " and consumer_group = #{consumerGroup} " +
            " </if> " +
            " <if test = ' topic != null and topic !=\"\" '> " +
            " and topic = #{topic} " +
            " </if> " +
            "</script>"})
    AdDasIncrement queryByConditionBacktrack(@Param("incrementName") String incrementName, @Param("dataSource") String dataSource,
            @Param("consumerGroup") String consumerGroup, @Param("topic") String topic);
}
