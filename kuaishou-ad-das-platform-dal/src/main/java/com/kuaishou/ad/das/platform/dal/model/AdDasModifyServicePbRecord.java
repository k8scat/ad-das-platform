package com.kuaishou.ad.das.platform.dal.model;

import java.sql.ResultSet;
import java.sql.SQLException;

import lombok.Data;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-12-09
 */
@Data
public class AdDasModifyServicePbRecord
        extends AdDasAbstarctModel<AdDasModifyServicePbRecord> {

    private Long id;

    private Long modifyId;

    private String serviceName;

    private String serviceType;

    private String serviceNodeName;

    private String curPbVersion;

    private String targetPbVersion;

    private Integer publishStatus;

    private Long createTime;

    private Long updateTime;

    @Override
    public AdDasModifyServicePbRecord parseResult(ResultSet rs) throws SQLException {

        this.setId(rs.getLong("id"));
        this.setModifyId(rs.getLong("modify_id"));
        this.setServiceName(rs.getString("service_name"));
        this.setServiceType(rs.getString("service_type"));
        this.setServiceNodeName(rs.getString("service_node_name"));
        this.setCurPbVersion(rs.getString("cur_pb_version"));
        this.setTargetPbVersion(rs.getString("target_pb_version"));
        this.setPublishStatus(rs.getInt("publish_status"));
        this.setCreateTime(rs.getLong("create_time"));
        this.setUpdateTime(rs.getLong("update_time"));

        return this;
    }
}
