package com.kuaishou.ad.das.platform.dal.repository.impl;

import java.util.Collection;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Component;

import com.kuaishou.ad.das.dal.datasource.AdCoreDataSource;
import com.kuaishou.ad.das.platform.dal.dao.AdDasBenchmarkCondition;
import com.kuaishou.ad.das.platform.dal.model.AdDasBenchmark;
import com.kuaishou.ad.das.platform.dal.repository.AdDasBenchmarkRepository;
import com.kuaishou.ad.das.platform.dal.repository.mapper.AdDasBenchmarkMapper;


/**
 * @author Zhang Jialin <zhangjialin@kuaishou.com>
 * Created on 2021-06-04
 */
@Component
public class AdDasBenchmarkRepositoryImpl implements AdDasBenchmarkRepository {

    private static final String TABLE_NAME = "ad_das_benchmark";

    private static final AdCoreDataSource DATA_SOURCE = AdCoreDataSource.adDas;

    @Autowired
    private AdDasBenchmarkMapper adDasBenchmarkMapper;

    @Override
    public List<AdDasBenchmark> queryById(Collection<Long> benchmakrIds) {

        return adDasBenchmarkMapper.queryById(benchmakrIds);
    }

    @Override
    public AdDasBenchmark queryById(Long id) {
        return adDasBenchmarkMapper.listById(id);
    }

    @Override
    public List<AdDasBenchmark> listAll() {
        return adDasBenchmarkMapper.listAll();
    }

    @Override
    public List<AdDasBenchmark> query(AdDasBenchmarkCondition condition) {
        MapSqlParameterSource param = new MapSqlParameterSource();
        StringBuilder sb = new StringBuilder("SELECT * FROM %s WHERE is_delete = 0");
        buildPredicate(condition, sb, param);
        String sql = String.format(sb.toString(), TABLE_NAME);
        return DATA_SOURCE.read().query(sql, param, new BeanPropertyRowMapper<>(AdDasBenchmark.class));
    }

    @Override
    public AdDasBenchmark listByName(String benchmarkName) {
        return adDasBenchmarkMapper.listByName(benchmarkName);
    }

    @Override
    public Long count(AdDasBenchmarkCondition condition) {
        MapSqlParameterSource param = new MapSqlParameterSource();
        StringBuilder sb = new StringBuilder("SELECT count(*) FROM %s WHERE is_delete = 0");
        buildPredicate(condition, sb, param);
        String sql = String.format(sb.toString(), TABLE_NAME);
        return DATA_SOURCE.read().queryForObject(sql, param, Long.class);
    }

    @Override
    public void insert(AdDasBenchmark adDasBenchmark) {
        adDasBenchmarkMapper.insert(adDasBenchmark);
    }

    @Override
    public void update(AdDasBenchmark adDasBenchmark) {

        adDasBenchmarkMapper.update(adDasBenchmark);
    }

    @Override
    public void deleteById(Long dataId) {
        adDasBenchmarkMapper.deletById(dataId);
    }

    /**
     * 构建谓词（WHERE条件）
     * @param condition 条件参数
     * @param sb SQL string builder
     * @param source 参数源
     */
    private static void buildPredicate(
            AdDasBenchmarkCondition condition, StringBuilder sb, MapSqlParameterSource source) {
        sb.append(" ");
        if (CollectionUtils.isNotEmpty(condition.getDataStreamGroupIds())) {
            sb.append("AND data_stream_group_id in (:dataStreamGroupIds) ");
            source.addValue("dataStreamGroupIds", condition.getDataStreamGroupIds());
        }
        if (CollectionUtils.isNotEmpty(condition.getBenchmarkIds())) {
            sb.append("AND id in (:benchmarkIds) ");
            source.addValue("benchmarkIds", condition.getBenchmarkIds());
        }
        if (CollectionUtils.isNotEmpty(condition.getHdfsPaths())) {
            sb.append("AND hdfs_path in (:hdfsPaths) ");
            source.addValue("hdfsPaths", condition.getHdfsPaths());
        }
        if (condition.getPageNum() != null && condition.getPageNum() != -1 && condition.getPageSize() != null) {
            sb.append("limit :limit, offset :offset");
            source.addValue("limit", condition.getPageSize());
            source.addValue("offset", (condition.getPageNum() - 1) * condition.getPageSize());
        }

    }
}
