package com.kuaishou.ad.das.platform.dal.model;


import lombok.Data;

/**
 * @author liutao <liutao06@kuaishou.com>
 * Created on 2021-01-05
 */
@Data
public class AdDasIncrementDetailOnline {

    private Long id;

    private Long modifyId;

    private String incrementService;

    private String dataSource;

    private String curPbVersion;

    private String mainTable;

    private String pbClass;

    private String operator;

    private String approver;

    private Long createTime;

    private Long updateTime;

}
