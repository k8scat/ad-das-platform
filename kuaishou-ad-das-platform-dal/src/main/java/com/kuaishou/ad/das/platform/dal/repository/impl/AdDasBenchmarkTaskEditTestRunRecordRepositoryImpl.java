package com.kuaishou.ad.das.platform.dal.repository.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import com.kuaishou.ad.das.platform.dal.model.AdDasBenchmarkTaskEditTestRunRecord;
import com.kuaishou.ad.das.platform.dal.repository.AdDasBenchmarkTaskEditTestRunRecordRepository;
import com.kuaishou.ad.das.platform.dal.repository.mapper.AdDasBenchmarkTaskEditTestRunRecordMapper;

/**
 * @author wangkai <wangkai14@kuaishou.com>
 * Created on 2021-01-04
 */
@Lazy
@Service
public class AdDasBenchmarkTaskEditTestRunRecordRepositoryImpl implements AdDasBenchmarkTaskEditTestRunRecordRepository {

    @Autowired
    private AdDasBenchmarkTaskEditTestRunRecordMapper adDasBenchmarkTaskEditTestRunRecordMapper;

    @Override
    public void insert(AdDasBenchmarkTaskEditTestRunRecord adDasBenchmarkTaskEditTestRunRecord) {
        adDasBenchmarkTaskEditTestRunRecordMapper.insert(adDasBenchmarkTaskEditTestRunRecord);
    }

    @Override
    public List<AdDasBenchmarkTaskEditTestRunRecord> getAll() {
        return adDasBenchmarkTaskEditTestRunRecordMapper.getAll();
    }

    @Override
    public void update(Integer taskRunStatus, Long taskRecordId, String operator, Long updateTime) {
        adDasBenchmarkTaskEditTestRunRecordMapper.update(taskRunStatus, taskRecordId, operator, updateTime);
    }
}
