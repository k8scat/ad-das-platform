package com.kuaishou.ad.das.platform.dal.repository.impl;

import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Repository;

import com.kuaishou.ad.das.platform.dal.model.AdDasIncrementTableDetail;
import com.kuaishou.ad.das.platform.dal.repository.AdDasIncrementTableDetailRepository;
import com.kuaishou.ad.das.platform.dal.repository.mapper.AdDasIncrementTableDetailMapper;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-06-10
 */
@Lazy
@Repository
public class AdDasIncrementTableDetailRepositoryImpl implements AdDasIncrementTableDetailRepository {

    @Autowired
    private AdDasIncrementTableDetailMapper adDasIncrementTableDetailMapper;


    @Override
    public int insert(AdDasIncrementTableDetail adDasIncrementTableDetail) {
        return adDasIncrementTableDetailMapper.insert(adDasIncrementTableDetail);
    }

    @Override
    public void deleteByIncreId(Long increId) {
        adDasIncrementTableDetailMapper.deleteByIncreId(increId);
    }

    @Override
    public void deleteByIncreIdAndTables(Long increId, Collection<String> tableNames) {
        adDasIncrementTableDetailMapper.deleteByIncreIdAndTables(increId, tableNames);
    }

    @Override
    public List<AdDasIncrementTableDetail> queryByCondition() {
        return adDasIncrementTableDetailMapper.listInfoCondition("");
    }

    @Override
    public List<AdDasIncrementTableDetail> queryByIncreId(Collection<Long> increId) {
        return adDasIncrementTableDetailMapper.listByIncreId(increId);
    }

    @Override
    public AdDasIncrementTableDetail queryByIncreIdAndTable(Long increId, String mainTable) {
        return adDasIncrementTableDetailMapper.listByTable(increId, mainTable);
    }

    @Override
    public List<AdDasIncrementTableDetail> queryByIncreIdAndTables(Long increId, List<String> mainTable) {
        return adDasIncrementTableDetailMapper.listByTables(increId, mainTable);
    }

    @Override
    public List<AdDasIncrementTableDetail> queryByTableName(String tableName) {
        return adDasIncrementTableDetailMapper.queryByTable(tableName);
    }
}
