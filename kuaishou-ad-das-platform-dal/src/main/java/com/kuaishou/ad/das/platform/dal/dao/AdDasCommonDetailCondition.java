package com.kuaishou.ad.das.platform.dal.dao;

import java.util.List;

import lombok.Data;

/**
 * @author Zhang Jialin <zhangjialin@kuaishou.com>
 * Created on 2021-06-16
 */
@Data
public class AdDasCommonDetailCondition {
    private List<Long> commonDetailIds;

    private List<Long> benchmarkIds;

    private String mainTableName;

    private Integer pageNum;

    private Integer pageSize;
}
