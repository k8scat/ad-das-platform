package com.kuaishou.ad.das.platform.dal.dao;

import java.util.List;

import lombok.Data;

/**
 * @author Zhang Jialin <zhangjialin@kuaishou.com>
 * Created on 2021-06-04
 */
@Data
public class AdDasBenchmarkCondition {
    /**
     * 数据流组id列表
     */
    private List<Long> dataStreamGroupIds;
    /**
     * 基准id列表
     */
    private List<Long> benchmarkIds;
    /**
     * HDFS路径
     */
    private List<String> hdfsPaths;
    /**
     * 单页显示条数
     */
    private Integer pageSize;
    /**
     * 页面大小
     */
    private Integer pageNum;
}
