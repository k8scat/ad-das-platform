package com.kuaishou.ad.das.platform.dal.repository.mapper;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;

import com.kuaishou.ad.das.platform.dal.model.AdDasDownstreamUserInfo;
import com.kuaishou.infra.boot.jdbc.datasource.DataSourceRouting;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-06-22
 */
@Mapper
@DataSourceRouting("adDas")
public interface AdDasDownstreamUserInfoMapper {

    @Insert("insert into ad_das_downstream_user_info(user_group_name,"
            + "owner_email_prefix,owner_name,topic,operator_email_prefix,create_time,update_time,is_delete)" +
            " values(#{userGroupName},#{ownerEmailPrefix},#{ownerName},#{createTime}" +
            ",#{updateTime},#{is_delete})")
    Integer insert(AdDasDownstreamUserInfo adDasDownstreamUserInfo);
}
