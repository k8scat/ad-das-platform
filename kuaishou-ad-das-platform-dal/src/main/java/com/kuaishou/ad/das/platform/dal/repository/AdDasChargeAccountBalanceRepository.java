package com.kuaishou.ad.das.platform.dal.repository;

import java.util.List;

import com.kuaishou.ad.das.platform.dal.model.AdDspAccountBalance;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-08-06
 */
public interface AdDasChargeAccountBalanceRepository {

    AdDspAccountBalance queryById(Long accountId);

    List<AdDspAccountBalance> batchQueryData(Long shardId, List<Long> accountId);
}
