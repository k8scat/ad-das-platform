package com.kuaishou.ad.das.platform.dal.model;

import lombok.Data;

/**
 * @author caoyinhang <caoyinhang@kuaishou.com>
 * Created on 2021-07-06
 */
@Data
public class AdDasIncrementServiceInfo {

    private Long id;

    private Long increId;

    private String ownerName;

    private String region;

    private String bizDef;

    private String tenant;

    private String productDef;

    private String topicType;


}
