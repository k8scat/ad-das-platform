package com.kuaishou.ad.das.platform.dal.util;

import java.lang.reflect.Method;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Types;

import org.apache.commons.beanutils.MethodUtils;

import com.google.common.base.CaseFormat;

import lombok.extern.slf4j.Slf4j;

/**
 * @author liutao <liutao06@kuaishou.com>
 * Created on 2021-01-05
 */
@Slf4j
public class ModelConvertUtils {

    private ModelConvertUtils() {}

    /**
     * 一个比较捡漏的转换工具类
     *
     * @param rs JDBC查询结果集
     * @param dto DO实例
     * @param <T> DO类型参数
     * @throws SQLException SQL执行异常
     */
    public static <T> void bindDataToDTO(ResultSet rs, T dto) throws SQLException {
        //取得ResultSet的列名
        ResultSetMetaData metaData = rs.getMetaData();
        int columnsCount = metaData.getColumnCount();
        String[] columnNames = new String[columnsCount];
        String[] columnDbNames = new String[columnsCount];
        Integer[] mysqlTypes = new Integer[columnsCount];
        for (int i = 0; i < columnsCount; i++) {
            //添加转换
            columnDbNames[i] = metaData.getColumnLabel(i + 1);
            columnNames[i] = CaseFormat.LOWER_UNDERSCORE.to(CaseFormat.UPPER_CAMEL, metaData.getColumnLabel(i + 1));
            mysqlTypes[i] = metaData.getColumnType(i + 1);
        }
        //遍历ResultSet
        //反射, 从ResultSet绑定到JavaBean
        for (int i = 0; i < columnNames.length; i++) {
            Object value = getDbValue(columnDbNames[i], mysqlTypes[i], rs);
            if (value == null) {
                continue;
            }
            //取得Set方法
            String setMethodName = "set" + columnNames[i];
            try {
                Method method = MethodUtils.getAccessibleMethod(dto.getClass(), setMethodName, value.getClass());
                if (method == null) {
                    continue;
                }
                method.invoke(dto, value);
            } catch (Exception e) {
                log.error("bindDataToDTO error", e);
                throw new ModelConvertException("bindDataToDTO error");
            }
        }
    }

    /**
     * 公共RowMapper
     * @param rs JDBC查询结果集
     * @param clazz DO类
     * @param <T> 类型参数
     * @return 类型
     */
    public static <T> T defaultRowMapper(ResultSet rs, Class<T> clazz) {
        try {
            T instance = clazz.getDeclaredConstructor().newInstance();
            bindDataToDTO(rs, instance);
            return instance;
        } catch (Exception e) {
            throw new ModelConvertException("build DO fail");
        }

    }
    private static Object getDbValue(String columnName, Integer type, ResultSet rs) throws SQLException {
        switch (type) {
            case Types.CLOB:
            case Types.BLOB:
            case Types.VARCHAR:
                return rs.getString(columnName);
            case Types.TINYINT:
            case Types.INTEGER:
                return rs.getInt(columnName);
            case Types.BIGINT:
                return rs.getLong(columnName);
            default:
                return rs.getObject(columnName);
        }
    }

    public static class ModelConvertException extends RuntimeException {
        ModelConvertException(String msg) {
            super(msg);
        }
    }
}
