package com.kuaishou.ad.das.platform.dal.model;

import lombok.Data;

/**
 * @author caoyinhang <caoyinhang@kuaishou.com>
 * Created on 2021-07-09
 */
@Data
public class AdDasIncrementBacktrackServiceDetail {

    private String incrementName;
    private String dataSource;
    private String consumerGroup;
    private String topic;
    private String topicType;
    private Long increId;
    private String ownerName;
    private String region;
    private String bizDef;
    private String tenant;
    private String productDef;

}
