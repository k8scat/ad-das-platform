package com.kuaishou.ad.das.platform.dal.repository.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.kuaishou.ad.das.platform.dal.model.AdDasPbClass;
import com.kuaishou.infra.boot.jdbc.datasource.DataSourceRouting;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-07-02
 */
@Mapper
@DataSourceRouting("adDas")
public interface AdDasPbClassMapper {

    @Insert("insert into ad_das_pb_class(pb_version,pb_class,pb_type,pb_class_status,pb_class_column_count,"
            + "operator,create_time,update_time) "
            + "values(#{pbVersion}, #{pbClass}, #{pbType}, #{pbClassStatus}, #{pbClassColumnCount},"
            + "#{operator},#{createTime}, #{updateTime}) ")
    long insert(AdDasPbClass adDasPbClass);

    @Select({"<script> " +
            " select * "+
            " from ad_das_pb_class \n" +
            " where 1=1 and pb_version=#{pbVersion}" +
            " <if test = ' pbClass != null and pbClass !=\"\" '> " +
            " and pb_class = #{pbClass} " +
            " </if> " +
            " <if test = \" pbClassStatus != null \"> " +
            " and pb_class_status = #{pbClassStatus} " +
            " </if> " +
            " order by create_time desc" +
            "</script>"})
    List<AdDasPbClass> queryLikePbClass(@Param("pbVersion") String pbVersion,
                                        @Param("pbClass") String pbClass, @Param("pbClassStatus") Integer pbClassStatus);

    @Select({"<script> " +
            " select count(*) "+
            " from ad_das_pb_class \n" +
            " where 1=1 and pb_version=#{pbVersion}" +
            " <if test = ' pbClass != null and pbClass !=\"\" '> " +
            " and pb_class = #{pbClass} " +
            " </if> " +
            " <if test = \" pbClassStatus != null \"> " +
            " and pb_class_status = #{pbClassStatus} " +
            " </if> " +
            "</script>"})
    int count(@Param("pbVersion")String pbVersion, @Param("pbClass")String pbClass, @Param("pbClassStatus")Integer pbClassStatus);
}
