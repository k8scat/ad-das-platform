package com.kuaishou.ad.das.platform.dal.repository.impl;

import java.util.List;

import org.springframework.context.annotation.Lazy;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.kuaishou.ad.das.platform.dal.model.AdDasIncrementPublishRecord;
import com.kuaishou.ad.das.platform.dal.repository.AdDasIncrementPublishRecordRepository;
import com.kuaishou.ad.das.platform.dal.repository.common.AdDasAbstractRepository;

/**
 * @author liutao <liutao06@kuaishou.com>
 * Created on 2021-01-10
 */
@Lazy
@Repository
public class AdDasIncrementPublishRecordRepositoryImpl extends AdDasAbstractRepository<AdDasIncrementPublishRecord>
        implements AdDasIncrementPublishRecordRepository {

    @Override
    public List<AdDasIncrementPublishRecord> listAdDasIncrementPublishRecordByModifyId(Long modifyId) {
        String querySql = "select * from ad_das_modify_service_increment_record where modify_id=?";
        return jdbcTemplate.query(querySql, getRowMapper(), modifyId);
    }

    @Override
    public Integer insert(AdDasIncrementPublishRecord adDasIncrementPublishRecord) {
        String insertSql = "insert into ad_das_modify_service_increment_record("
                + "modify_id,increment_service,host_name,pre_pb_info,"
                + "cur_pb_info,data_source,publish_status,create_time,update_time) values(?,?,?,?,?,?,?,?,?)";
        return jdbcTemplateWrite.update(insertSql, adDasIncrementPublishRecord.getModifyId(), adDasIncrementPublishRecord.getIncrementService(),
                adDasIncrementPublishRecord.getHostName(), adDasIncrementPublishRecord.getPrePbInfo(), adDasIncrementPublishRecord.getCurPbInfo(),
                adDasIncrementPublishRecord.getDataSource(), adDasIncrementPublishRecord.getPublishStatus(),
                adDasIncrementPublishRecord.getCreateTime(), adDasIncrementPublishRecord.getUpdateTime());
    }

    @Override
    protected RowMapper<AdDasIncrementPublishRecord> getRowMapper() {
        return (rs, row) -> {
            AdDasIncrementPublishRecord adDasIncrementPublishRecord = new AdDasIncrementPublishRecord();
            adDasIncrementPublishRecord.parseResult(rs);
            return adDasIncrementPublishRecord;
        };
    }

    @Override
    protected JdbcTemplate getJdbcTemplate(String dataSource) {
        return jdbcTemplate;
    }
}
