package com.kuaishou.ad.das.platform.dal.model;

import java.sql.ResultSet;
import java.sql.SQLException;

import lombok.Data;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-11-29
 */
@Data
public class AdDasPbClass extends AdDasAbstarctModel<AdDasPbClass> {

    private Long id;

    private String pbVersion;

    private String pbClass;

    /**
     * pb类类型： 1-pb类 2-pb类枚举
     */
    private Integer pbType;

    private Integer pbClassStatus;

    private Integer pbClassColumnCount;

    private String operator;

    private Long createTime;

    private Long updateTime;

    @Override
    public AdDasPbClass parseResult(ResultSet rs) throws SQLException {

        this.setId(rs.getLong("id"));
        this.setPbVersion(rs.getString("pb_version"));
        this.setPbClass(rs.getString("pb_class"));
        this.setPbType(rs.getInt("pb_type"));
        this.setPbClassStatus(rs.getInt("pb_class_status"));
        this.setPbClassColumnCount(rs.getInt("pb_class_column_count"));
        this.setOperator(rs.getString("operator"));
        this.setCreateTime(rs.getLong("create_time"));
        this.setUpdateTime(rs.getLong("update_time"));

        return this;
    }
}
