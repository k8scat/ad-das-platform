package com.kuaishou.ad.das.platform.dal.repository;

import java.util.List;

import com.kuaishou.ad.das.platform.dal.dao.AdDasCommonDetailCondition;
import com.kuaishou.ad.das.platform.dal.model.AdDasBenchmarkTaskCommonDetail;

/**
 * @author wangkai <wangkai14@kuaishou.com>
 * Created on 2020-12-22
 */
public interface AdDasBenchMarkTaskCommonDetailRepository {

    String TABLE_NAME = "ad_das_benchmark_task_common_detail";

    int insert(AdDasBenchmarkTaskCommonDetail adDasBenchMarkTaskCommonDetail);

    AdDasBenchmarkTaskCommonDetail getByBenchmarkTypeAndMainTable(Long benchmarkId, String mainTable);

    AdDasBenchmarkTaskCommonDetail getByCommonDetailId(Long commonDetailId);

    void update(AdDasBenchmarkTaskCommonDetail adDasBenchmarkTaskCommonDetail);

    void deleteByBenchmarkIdAndMainTable(Long benchmarkId, String mainTable);

    List<AdDasBenchmarkTaskCommonDetail> query(AdDasCommonDetailCondition condition);
}
