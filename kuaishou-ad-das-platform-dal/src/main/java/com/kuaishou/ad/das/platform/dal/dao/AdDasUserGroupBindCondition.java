package com.kuaishou.ad.das.platform.dal.dao;

import java.util.List;

import lombok.Data;

/**
 * @author Zhang Jialin <zhangjialin@kuaishou.com>
 * Created on 2021-06-14
 */
@Data
public class AdDasUserGroupBindCondition {
    /**
     * 数据流id
     */
    private List<Long> dataStreamIds;
    /**
     * 数据流类型:0-基准；1-增量
     */
    private List<Integer> dataStreamTypes;
    /**
     * 用户组id
     */
    private List<Long> userGroupIds;
}
