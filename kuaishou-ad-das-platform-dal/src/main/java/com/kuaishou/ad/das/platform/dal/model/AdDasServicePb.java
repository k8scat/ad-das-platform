package com.kuaishou.ad.das.platform.dal.model;

import java.sql.ResultSet;
import java.sql.SQLException;

import lombok.Data;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-11-29
 */
@Data
public class AdDasServicePb extends AdDasAbstarctModel<AdDasServicePb> {

    private Long id;

    private String serviceName;

    private String serviceType;

    private String pbVersion;

    private String pbEnumVersion;

    private String pbCommonVersion;

    private Integer servicePbStatus;

    private String operator;

    private Long createTime;

    private Long updateTime;

    @Override
    public AdDasServicePb parseResult(ResultSet rs) throws SQLException {

        this.setId(rs.getLong("id"));
        this.setServiceName(rs.getString("service_name"));
        this.setServiceType(rs.getString("service_type"));
        this.setPbVersion(rs.getString("pb_version"));
        this.setPbEnumVersion(rs.getString("pb_enum_version"));
        this.setPbCommonVersion(rs.getString("pb_common_version"));
        this.setServicePbStatus(rs.getInt("service_pb_status"));
        this.setOperator(rs.getString("operator"));
        this.setCreateTime(rs.getLong("create_time"));
        this.setUpdateTime(rs.getLong("update_time"));

        return this;
    }
}
