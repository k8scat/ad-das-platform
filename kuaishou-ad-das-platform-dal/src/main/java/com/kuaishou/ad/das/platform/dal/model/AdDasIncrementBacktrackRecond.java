package com.kuaishou.ad.das.platform.dal.model;

import lombok.Data;

/**
 * @author caoyinhang <caoyinhang@kuaishou.com>
 * Created on 2021-07-06
 */
@Data
public class AdDasIncrementBacktrackRecond {

    private Long id;
    private Long increId;
    private String operator;
    private Long operationTime;
    private Long durationSecond;
    private Integer status;
    private String applicationRea;

}
