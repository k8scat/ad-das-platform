package com.kuaishou.ad.das.platform.dal.repository.mapper;

import java.util.Collection;
import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.kuaishou.ad.das.platform.dal.model.AdDasModify;
import com.kuaishou.infra.boot.jdbc.datasource.DataSourceRouting;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-06-15
 */
@Mapper
@DataSourceRouting("adDas")
public interface AdDasModifyMapper {

    @Insert("insert into ad_das_modify(modify_name,modify_type,operator,modify_status,"
            + "approver,reason,create_time,update_time) "
            + "values(#{modifyName}, #{modifyType}, #{operator}, #{modifyStatus},"
            + " #{approver}, #{reason}, #{createTime}, #{updateTime}) ")
    long insert(AdDasModify adDasModify);

    @Select("SELECT * from ad_das_modify where id = #{modifyId} ")
    AdDasModify queryById(@Param("modifyId") Long modifyId);

    @Select({"<script> " +
            " select * "+
            " from ad_das_modify \n" +
            " where 1=1 " +
            " <if test = \" modifyIds != null and modifyIds.size() > 0 \"> " +
            " and id in " +
            " <foreach item = 'item' index = 'index' collection = 'modifyIds' open = '(' separator = ',' close = ')' > " +
            " #{item} " +
            " </foreach> " +
            " </if> " +
            "</script>"})
    List<AdDasModify> queryByIds(@Param("modifyIds") Collection<Long> modifyIds);

    @Select({"<script> " +
            " select * "+
            " from ad_das_modify \n" +
            " where 1=1 " +
            " <if test = \" modifyIds != null and modifyIds.size() > 0 \"> " +
            " and id in " +
            " <foreach item = 'item' index = 'index' collection = 'modifyIds' open = '(' separator = ',' close = ')' > " +
            " #{item} " +
            " </foreach> " +
            " </if> " +
            "</script>"})
    List<AdDasModify> queryByCondition(@Param("modifyIds") Collection<Long> modifyIds,
                                       @Param("modifyName") String modifyName,
                                       @Param("modifyType") Integer modifyType, @Param("modifyStatus") Integer modifyStatus,
                                       @Param("startTime") Long startTime, @Param("endTime") Long endTime,
                                       @Param("limit") Integer limit, @Param("offset") Integer offset);

    @Update("update ad_das_modify set approver=#{approver},modify_status=#{modifyStatus}," +
            "reason=#{reason},update_time=#{updateTime} where id=#{modifyId}")
    int updateApprove(Long modifyId, String approver, Integer modifyStatus, String reason, Long updateTime);

    @Update("update ad_das_modify set operator=#{operator},modify_status=#{modifyStatus}," +
            "update_time=#{updateTime} where id=#{modifyId}")
    int updateStatusWithOperator(Long modifyId, Integer modifyStatus, String operator, Long updateTime);

    @Update("update ad_das_modify set modify_status=#{modifyStatus}," +
            "update_time=#{updateTime} where id=#{modifyId}")
    int updateStatus(Long modifyId, Integer modifyStatus, Long updateTime);

}
