package com.kuaishou.ad.das.platform.dal.repository.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import com.kuaishou.ad.das.platform.dal.model.AdDasPbClass;
import com.kuaishou.ad.das.platform.dal.repository.AdDasPbClassRepository;
import com.kuaishou.ad.das.platform.dal.repository.common.AdDasAbstractRepository;
import com.kuaishou.ad.das.platform.dal.repository.mapper.AdDasPbClassMapper;


/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-11-29
 */
@Lazy
@Repository
public class AdDasPbClassRepositoryImpl extends AdDasAbstractRepository<AdDasPbClass>
        implements AdDasPbClassRepository {

    @Autowired
    private AdDasPbClassMapper adDasPbClassMapper;

    @Override
    protected RowMapper<AdDasPbClass> getRowMapper() {
        return (rs, rowNum) -> {
            AdDasPbClass adDasPbClass = new AdDasPbClass();
            adDasPbClass.parseResult(rs);
            return adDasPbClass;
        };
    }

    @Override
    protected JdbcTemplate getJdbcTemplate(String dataSource) {
        return jdbcTemplate;
    }

    @Override
    public long insert(AdDasPbClass adDasPbClass) {

        return adDasPbClassMapper.insert(adDasPbClass);
    }

    @Override
    public long updateStatus(AdDasPbClass adDasPbClass) {

        String updateSql = "update ad_das_pb_class set pb_class_status=?, update_time=?, operator=? "
                + "where id=? and pb_version=? and pb_class=?";

        return jdbcTemplateWrite.update(updateSql, adDasPbClass.getPbClassStatus(), adDasPbClass.getUpdateTime(), adDasPbClass.getOperator(),
                adDasPbClass.getId(), adDasPbClass.getPbVersion(), adDasPbClass.getPbClass());
    }

    @Override
    public long updatePbClassColumnCount(Long id, String pbClass,
                                         Integer pbClassColumnCount,
                                         String operator) {
        String updateSql = "update ad_das_pb_class set pb_class_column_count=?, update_time=?, operator=? "
                + "where id=? and pb_class=?";

        return jdbcTemplateWrite.update(updateSql, pbClassColumnCount,
                System.currentTimeMillis(), operator, id, pbClass);
    }

    @Override
    public long updatePbClassColumnCount(String pbVersion, String pbClass, Integer pbClassColumnCount, String operator) {

        String updateSql = "update ad_das_pb_class set pb_class_column_count=?, update_time=?, operator=? "
                + "where pb_version=? and pb_class=?";

        return jdbcTemplateWrite.update(updateSql, pbClassColumnCount,
                System.currentTimeMillis(), operator, pbVersion, pbClass);
    }

    @Override
    public List<AdDasPbClass> query(String pbVersion, Integer pbClassStatus) {

        String selectSql = "select * from ad_das_pb_class where pb_version=? ";

        if (pbClassStatus != null) {
            selectSql = selectSql + "and pb_class_status=? ";
            return jdbcTemplate.query(selectSql, getRowMapper(), pbVersion, pbClassStatus);
        }
        return jdbcTemplate.query(selectSql, getRowMapper(), pbVersion);
    }

    @Override
    public List<AdDasPbClass> queryLikePbClass(String pbVersion, String pbClass, Integer pbClassStatus) {


       return adDasPbClassMapper.queryLikePbClass(pbVersion, pbClass, pbClassStatus);
    }

    @Override
    public int count(String pbVersion, String pbClass, Integer pbClassStatus) {

        return adDasPbClassMapper.count(pbVersion, pbClass, pbClassStatus);
    }

    @Override
    public List<AdDasPbClass> queryByPbClass(String pbVersion, String pbClass, Integer pbClassStatus) {

        String selectSql = "select * from ad_das_pb_class where pb_version=? and pb_class=? ";

        if (pbClassStatus != null) {
            selectSql = selectSql + "and pb_class_status=? ";
            return jdbcTemplate.query(selectSql, getRowMapper(), pbVersion, pbClass, pbClassStatus);
        }
        return jdbcTemplate.query(selectSql, getRowMapper(), pbVersion, pbClass);
    }

}
