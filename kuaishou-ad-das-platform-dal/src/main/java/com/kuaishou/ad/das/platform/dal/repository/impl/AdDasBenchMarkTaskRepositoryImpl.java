package com.kuaishou.ad.das.platform.dal.repository.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import com.kuaishou.ad.das.platform.dal.model.AdDasBenchmarkTask;
import com.kuaishou.ad.das.platform.dal.repository.AdDasBenchmarkTaskRepository;
import com.kuaishou.ad.das.platform.dal.repository.mapper.AdDasBenchmarkTaskMapper;

import lombok.extern.slf4j.Slf4j;

/**
 * @author wangkai <wangkai14@kuaishou.com>
 * Created on 2020-12-22
 */
@Lazy
@Slf4j
@Service
public class AdDasBenchMarkTaskRepositoryImpl
        implements AdDasBenchmarkTaskRepository {

    @Autowired
    private AdDasBenchmarkTaskMapper adDasBenchMarkTaskMapper;

    @Override
    public int insert(AdDasBenchmarkTask adDasBenchMarkTask) {
        return adDasBenchMarkTaskMapper.insert(adDasBenchMarkTask);
    }

    @Override
    public List<AdDasBenchmarkTask> queryByParams(Long benchmarkId, String dataSource, String pbClass,
                                                  String mainTable, String taskName, int offset, int limit) {
        return adDasBenchMarkTaskMapper.getByParams(benchmarkId, dataSource, pbClass, mainTable, taskName, offset, limit);
    }

    @Override
    public List<AdDasBenchmarkTask> queryAll() {
        return adDasBenchMarkTaskMapper.getAll();
    }

    @Override
    public long queryCountByParams(Long benchmarkId, String dataSource, String pbClass, String mainTable, String taskName) {
        return adDasBenchMarkTaskMapper.getTotalByParams(benchmarkId, dataSource, pbClass, mainTable, taskName);
    }

    @Override
    public void deleteByTaskId(Long taskId) {
        adDasBenchMarkTaskMapper.deleteByTaskId(taskId);
    }

    @Override
    public AdDasBenchmarkTask queryByTaskId(long taskId) {
        return adDasBenchMarkTaskMapper.getById(taskId);
    }

    @Override
    public List<AdDasBenchmarkTask> queryListByBenchmarkAndTable(String tableName, Long benchmarkId) {
        return adDasBenchMarkTaskMapper.queryListByBenchmarkAndTable(tableName, benchmarkId);
    }

    @Override
    public AdDasBenchmarkTask getByBenchmarkAndName(String taskName, Long benchmarkId) {
        return adDasBenchMarkTaskMapper.getByBenchmarkAndName(taskName, benchmarkId);
    }

    @Override
    public AdDasBenchmarkTask getByNumAndName(String taskName, Integer taskNumber) {
        return adDasBenchMarkTaskMapper.getByNumAndName(taskName, taskNumber);
    }

    @Override
    public List<AdDasBenchmarkTask> listByBenchmarkId(Long benchmarkId) {
        return adDasBenchMarkTaskMapper.listByBenchmarkId(benchmarkId);
    }

    @Override
    public List<AdDasBenchmarkTask> queryByBenchmarkId(Long benchmarkId, String dataSource, String pbClass, String mainTable) {
        return adDasBenchMarkTaskMapper.getByBenchmarkId(benchmarkId, dataSource, pbClass, mainTable);
    }

    @Override
    public void updateAdDasBenchmarkTask(AdDasBenchmarkTask adDasBenchmarkTask) {
        adDasBenchMarkTaskMapper.update(adDasBenchmarkTask);
    }

    @Override
    public void deleteByBenchmarkIdeAndMainTable(Long benchmarkId, String mainTable) {
        adDasBenchMarkTaskMapper.deleteByBenchmarkIdAndMainTable(benchmarkId, mainTable);
    }
}