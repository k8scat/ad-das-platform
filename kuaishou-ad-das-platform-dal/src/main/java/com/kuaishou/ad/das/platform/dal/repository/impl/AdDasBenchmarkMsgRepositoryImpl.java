package com.kuaishou.ad.das.platform.dal.repository.impl;

import java.util.List;
import java.util.StringJoiner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import com.google.common.collect.Lists;
import com.kuaishou.ad.das.dal.datasource.AdCoreDataSource;
import com.kuaishou.ad.das.platform.dal.dao.BenchmarkModifyMsgCondition;
import com.kuaishou.ad.das.platform.dal.model.AdDasBenchmarkMsg;
import com.kuaishou.ad.das.platform.dal.repository.AdDasBenchmarkMsgRepository;
import com.kuaishou.ad.das.platform.dal.repository.mapper.AdDasBenchmarkMsgMapper;

@Lazy
@Service
public class AdDasBenchmarkMsgRepositoryImpl implements AdDasBenchmarkMsgRepository {

    @Autowired
    private AdDasBenchmarkMsgMapper adDasBenchmarkMsgMapper;


    @Override
    public int insert(AdDasBenchmarkMsg adDasBenchMarkMsg) {
        return adDasBenchmarkMsgMapper.insert(adDasBenchMarkMsg);
    }

    @Override
    public AdDasBenchmarkMsg getByModifyId(Long modifyId) {
        return adDasBenchmarkMsgMapper.getByModifyId(modifyId);
    }

    @Override
    public AdDasBenchmarkMsg getByTableAndDataSource(Long benchmarkId, String mainTable, String dataSource) {
        return adDasBenchmarkMsgMapper.getByTableAndDataSource(benchmarkId, mainTable, dataSource);
    }

    @Override
    public List<AdDasBenchmarkMsg> getByModifyIds(List<Long> modifyId) {

        if (org.springframework.util.CollectionUtils.isEmpty(modifyId)) {
            return Lists.newArrayList();
        }
        return adDasBenchmarkMsgMapper.getByModifyIds(modifyId);
    }

    @Override
    public AdDasBenchmarkMsg getById(Long id) {
        return adDasBenchmarkMsgMapper.getById(id);
    }

    @Override
    public AdDasBenchmarkMsg update(AdDasBenchmarkMsg adDasBenchmarkMsg) {
        adDasBenchmarkMsgMapper.update(adDasBenchmarkMsg);
        return adDasBenchmarkMsg;
    }

    @Override
    public List<AdDasBenchmarkMsg> getByBenchmarkTypeAndMainTable(Long benchmarkId, String mainTable) {
        return adDasBenchmarkMsgMapper.getByBenchmarkTypeAndMainTable(benchmarkId, mainTable);
    }

    @Override
    public List<AdDasBenchmarkMsg> query(BenchmarkModifyMsgCondition condition) {
        StringBuilder sqlBuilder = new StringBuilder("SELECT * FROM " + TABLE_NAME);
        MapSqlParameterSource paramSource = new MapSqlParameterSource();
        buildPredicate(condition, sqlBuilder, paramSource);
        return AdCoreDataSource.adDas.read().query(sqlBuilder.toString(), paramSource,
                new BeanPropertyRowMapper<>(AdDasBenchmarkMsg.class));
    }

    private void buildPredicate(BenchmarkModifyMsgCondition condition, StringBuilder sb,
            MapSqlParameterSource paramSource) {
        StringJoiner sj = new StringJoiner(" AND ");
        if (!CollectionUtils.isEmpty(condition.getModifyIds())) {
            sj.add("modify_id in (:modifyIds)");
            paramSource.addValue("modifyIds", condition.getModifyIds());
        }
        if (!CollectionUtils.isEmpty(condition.getBenchmarkIds())) {
            sj.add("benchmark_id in (:benchmarkIds)");
            paramSource.addValue("benchmarkIds", condition.getBenchmarkIds());
        }
        if (!StringUtils.isEmpty(condition.getDataSource())) {
            sj.add("data_source like '%:dataSource%'");
            paramSource.addValue("dataSource", condition.getDataSource());
        }
        if (!StringUtils.isEmpty(condition.getMainTable())) {
            sj.add("main_table like '%:mainTable%'");
            paramSource.addValue("mainTable", condition.getMainTable());
        }
        if (sj.length() != 0) {
            sb.append(" WHERE ").append(sj);
        }
    }

}
