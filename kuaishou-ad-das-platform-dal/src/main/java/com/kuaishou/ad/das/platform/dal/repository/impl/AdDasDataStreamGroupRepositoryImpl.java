package com.kuaishou.ad.das.platform.dal.repository.impl;

import java.util.Collections;
import java.util.List;
import java.util.StringJoiner;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import com.kuaishou.ad.das.dal.datasource.AdCoreDataSource;
import com.kuaishou.ad.das.platform.dal.dao.AdDasDataStreamGroupCondition;
import com.kuaishou.ad.das.platform.dal.model.AdDasDataStreamGroup;
import com.kuaishou.ad.das.platform.dal.repository.AdDasDataStreamGroupRepository;
import com.kuaishou.ad.das.platform.dal.repository.mapper.AdDasDataStreamGroupMapper;

/**
 * @author Zhang Jialin <zhangjialin@kuaishou.com>
 * Created on 2021-06-03
 */
@Lazy
@Repository
public class AdDasDataStreamGroupRepositoryImpl implements AdDasDataStreamGroupRepository {

    private static final AdCoreDataSource DATA_SOURCE = AdCoreDataSource.adDas;

    private static final String TABLE_NAME = "ad_das_data_stream_group";

    @Autowired
    private AdDasDataStreamGroupMapper dasDataStreamGroupMapper;

    @Override
    public List<AdDasDataStreamGroup> query(AdDasDataStreamGroupCondition condition) {
        MapSqlParameterSource param = new MapSqlParameterSource();
        StringBuilder sb = new StringBuilder(String.format("SELECT * FROM %s WHERE is_delete = 0", TABLE_NAME));
        buildPredicate(condition, sb, param);
        return DATA_SOURCE.read().query(sb.toString(), param, new BeanPropertyRowMapper<>(AdDasDataStreamGroup.class));
    }

    @Override
    public List<AdDasDataStreamGroup> queryByCondition(List<Long> dataStreamGroupIds, String dataStreamGroupName, List<String> dataSourceNames,
                                                       List<String> incrementTopics, List<String> hdfsPath) {

        return dasDataStreamGroupMapper.queryByCondition(dataStreamGroupIds, dataStreamGroupName,
                dataSourceNames, incrementTopics, hdfsPath);
    }

    @Override
    public Long count(AdDasDataStreamGroupCondition condition) {
        MapSqlParameterSource param = new MapSqlParameterSource();
        StringBuilder sb = new StringBuilder("SELECT count(*) FROM %s WHERE is_delete = 0 ");
        buildPredicate(condition, sb, param);
        String sql = String.format(sb.toString(), TABLE_NAME);
        return DATA_SOURCE.read().queryForObject(sql, param, Long.class);
    }

    @Override
    public AdDasDataStreamGroup queryById(Long id) {
        AdDasDataStreamGroupCondition condition = new AdDasDataStreamGroupCondition();
        condition.setIds(Collections.singletonList(id));
        return query(condition).get(0);
    }

    @Override
    public void update(AdDasDataStreamGroup param) {
        String sql = "UPDATE " + TABLE_NAME + " SET ";
        StringJoiner joiner = new StringJoiner(", ");
        if (StringUtils.isNotEmpty(param.getDataStreamGroupName())) {
            joiner.add("data_stream_group_name = :dataStreamGroupName");
        }
        if (StringUtils.isNotEmpty(param.getDataStreamGroupDesc())) {
            joiner.add("data_stream_group_desc = :dataStreamGroupDesc");
        }
        if (StringUtils.isNotEmpty(param.getOwnerEmailPrefix())) {
            joiner.add("user_email_prefix = :userEmailPrefix");
        }
        if (StringUtils.isNotEmpty(param.getOwnerName())) {
            joiner.add("user_name = :userName");
        }
        DATA_SOURCE.write().update(sql + joiner, new BeanPropertySqlParameterSource(param));
    }

    @Override
    public void insert(AdDasDataStreamGroup adDasDataStreamGroup) {

        dasDataStreamGroupMapper.insert(adDasDataStreamGroup);
    }

    @Override
    public void deletById(Long dataId) {
        dasDataStreamGroupMapper.deleteById(dataId);
    }

    /**
     * 构建谓词
     * @param condition 条件参数
     * @param sb SQL string builder
     * @param source 参数源
     */
    private static void buildPredicate(AdDasDataStreamGroupCondition condition, StringBuilder sb, MapSqlParameterSource source) {
        sb.append(" ");
        if (Strings.isNotBlank(condition.getDataStreamGroupName())) {
            sb.append("AND data_stream_group_name like '%:dataStreamGroupName%' ");
            source.addValue("dataStreamGroupName", condition.getDataStreamGroupName());
        }
        if (CollectionUtils.isNotEmpty(condition.getIds())) {
            if (condition.getIds().size() == 1) {
                sb.append("AND id = :dataStreamGroupIds ");
                source.addValue("dataStreamGroupIds", condition.getIds().get(0));
            } else {
                sb.append("AND id in (:dataStreamGroupIds) ");
                source.addValue("dataStreamGroupIds", condition.getIds());
            }
        }
        if (condition.getPageNum() != null && condition.getPageNum() != -1 && condition.getPageSize() != null) {
            sb.append("limit :limit, offset :offset");
            source.addValue("limit", condition.getPageSize());
            source.addValue("offset", (condition.getPageNum() - 1) * condition.getPageSize());
        }
    }
}
