package com.kuaishou.ad.das.platform.dal.repository.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import com.kuaishou.ad.das.platform.dal.model.AdDasBenchmarkTaskCommonDetailEdit;
import com.kuaishou.ad.das.platform.dal.repository.AdDasBenchmarkTaskCommonDetailEditRepository;
import com.kuaishou.ad.das.platform.dal.repository.mapper.AdDasBenchmarkTaskCommonDetailEditMapper;

/**
 * @author wangkai <wangkai14@kuaishou.com>
 * Created on 2020-12-22
 */
@Lazy
@Service
public class AdDasBenchmarkTaskCommonDetailEditRepositoryImpl implements AdDasBenchmarkTaskCommonDetailEditRepository {

    @Autowired
    private AdDasBenchmarkTaskCommonDetailEditMapper adDasBenchmarkTaskCommonDetailEditMapper;

    @Override
    public int insert(AdDasBenchmarkTaskCommonDetailEdit adDasBenchMarkTaskCommonDetailEdit) {
        return adDasBenchmarkTaskCommonDetailEditMapper.insert(adDasBenchMarkTaskCommonDetailEdit);
    }

    @Override
    public AdDasBenchmarkTaskCommonDetailEdit getByBenchmarkIdAndMainTable(Long benchmarkId, String mainTable) {
        return adDasBenchmarkTaskCommonDetailEditMapper.getByBenchmarkTypeAndMainTable(benchmarkId, mainTable);
    }

    @Override
    public void update(AdDasBenchmarkTaskCommonDetailEdit adDasBenchmarkTaskCommonDetailEdit) {
        adDasBenchmarkTaskCommonDetailEditMapper.update(adDasBenchmarkTaskCommonDetailEdit);
    }

    @Override
    public void deleteByBenchmarkTypeAndMainTable(Long benchmarkId, String mainTable) {
        adDasBenchmarkTaskCommonDetailEditMapper.deleteByBenchmarkTypeAndMainTable(benchmarkId,mainTable);
    }
}
