package com.kuaishou.ad.das.platform.dal.repository.impl;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.context.annotation.Lazy;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.kuaishou.ad.das.platform.dal.model.AdDasPbClassColumn;
import com.kuaishou.ad.das.platform.dal.repository.AdDasPbClassColumnRepository;
import com.kuaishou.ad.das.platform.dal.repository.common.AdDasAbstractRepository;


/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-11-29
 */
@Lazy
@Repository
public class AdDasPbClassColumnRepositoryImpl extends AdDasAbstractRepository<AdDasPbClassColumn>
        implements AdDasPbClassColumnRepository {

    @Override
    protected RowMapper<AdDasPbClassColumn> getRowMapper() {
        return (rs, row) -> {
            AdDasPbClassColumn adDasPbClassColumn = new AdDasPbClassColumn();
            adDasPbClassColumn.parseResult(rs);
            return adDasPbClassColumn;
        };
    }

    @Override
    protected JdbcTemplate getJdbcTemplate(String dataSource) {
        return jdbcTemplate;
    }

    protected BatchPreparedStatementSetter getBatchPreparedStatementSetter(List<AdDasPbClassColumn> adDasPbClassColumns) {
        return new BatchPreparedStatementSetter() {
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                ps.setString(1, adDasPbClassColumns.get(i).getPbVersion());
                ps.setString(2, adDasPbClassColumns.get(i).getPbClass());
                ps.setString(3, adDasPbClassColumns.get(i).getPbColumn());
                ps.setString(4, adDasPbClassColumns.get(i).getPbFullColumn());
                ps.setString(5, adDasPbClassColumns.get(i).getPbJavaType());
                ps.setString(6, adDasPbClassColumns.get(i).getPbType());
                ps.setString(7, adDasPbClassColumns.get(i).getEnumType());
                ps.setString(8, adDasPbClassColumns.get(i).getDefaultValue());
                ps.setString(9, adDasPbClassColumns.get(i).getOperator());
                ps.setLong(10, adDasPbClassColumns.get(i).getCreateTime());
                ps.setLong(11, adDasPbClassColumns.get(i).getUpdateTime());
            }
            public int getBatchSize() {
                return adDasPbClassColumns.size();
            }
        };
    }

    @Override
    public int insert(AdDasPbClassColumn adDasPbClassColumn) {
        String insertSql = "insert into ad_das_pb_class_column("
                + "pb_version,pb_class,pb_column,pb_full_column,pb_java_type,pb_type,"
                + "enum_type,default_value,operator,create_time,update_time) "
                + "values(?,?,?,?,?,?,?,?,?,?,?)";
        return jdbcTemplateWrite.update(insertSql, adDasPbClassColumn.getPbVersion(),
                adDasPbClassColumn.getPbClass(),adDasPbClassColumn.getPbFullColumn(),
                adDasPbClassColumn.getPbJavaType(), adDasPbClassColumn.getPbType(),
                adDasPbClassColumn.getEnumType(), adDasPbClassColumn.getDefaultValue(),
                adDasPbClassColumn.getOperator(), adDasPbClassColumn.getCreateTime(),
                adDasPbClassColumn.getUpdateTime());
    }

    @Override
    public int[] batchInsert(List<AdDasPbClassColumn> adDasPbClassColumns) {
        String insertSql = "insert into ad_das_pb_class_column("
                + "pb_version,pb_class,pb_column,pb_full_column,pb_java_type,pb_type,"
                + "enum_type,default_value,operator,create_time,update_time) "
                + "values(?,?,?,?,?,?,?,?,?,?,?)";

        return jdbcTemplateWrite.batchUpdate(insertSql, getBatchPreparedStatementSetter(adDasPbClassColumns));
    }

    @Override
    public void batchUpdate(List<AdDasPbClassColumn> adDasPbClassColumns) {
        String updateSql = "update ";

        List<Object[]> param = adDasPbClassColumns.stream()
                .map(adDasPbClassColumn -> new Object[]{adDasPbClassColumn.getId()})
                .collect(Collectors.toList());

        jdbcTemplateWrite.batchUpdate(updateSql, param);
    }

    @Override
    public void batchDelete(List<AdDasPbClassColumn> adDasPbClassColumns) {

        String deletSql = "delete from ad_das_pb_class_column where id=? ";
        List<Object[]> param = adDasPbClassColumns.stream()
                .map(adDasPbClassColumn -> new Object[]{adDasPbClassColumn.getId()})
                .collect(Collectors.toList());

        this.jdbcTemplateWrite.batchUpdate(deletSql, param);
    }


    @Override
    public List<AdDasPbClassColumn> query(String pbVersion, String pbClass) {

        String selectSql = "select * from ad_das_pb_class_column where pb_version=? and pb_class=?";
        return jdbcTemplate.query(selectSql, getRowMapper(), pbVersion, pbClass);
    }

}
