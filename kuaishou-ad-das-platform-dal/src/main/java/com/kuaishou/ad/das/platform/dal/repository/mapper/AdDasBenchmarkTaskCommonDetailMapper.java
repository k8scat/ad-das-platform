package com.kuaishou.ad.das.platform.dal.repository.mapper;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.kuaishou.ad.das.platform.dal.model.AdDasBenchmarkTaskCommonDetail;
import com.kuaishou.infra.boot.jdbc.datasource.DataSourceRouting;

/**
 * @author wangkai <wangkai14@kuaishou.com>
 * Created on 2020-12-22
 */
@Mapper
@DataSourceRouting("adDas")
public interface AdDasBenchmarkTaskCommonDetailMapper {

    @Insert("insert into ad_das_benchmark_task_common_detail(benchmark_id,modify_id,data_source,main_table,table_type," +
            "column_schema,cascade_column_schema,common_sqls,time_params,predict_growth,shard_sql_column," +
            "shard_sql_flag,operator,create_time,update_time,pb_class) values(#{benchmarkId},#{modifyId},#{dataSource},#{mainTable},#{tableType}," +
            "#{columnSchema},#{cascadeColumnSchema},#{commonSqls},#{timeParams},#{predictGrowth},#{shardSqlColumn}," +
            "#{shardSqlFlag},#{operator},#{createTime},#{updateTime},#{pbClass})")
    int insert(AdDasBenchmarkTaskCommonDetail adDasBenchMarkTaskCommonDetail);

    @Select("select * from ad_das_benchmark_task_common_detail where benchmark_id=#{benchmarkId} " +
            "and main_table=#{mainTable}")
    AdDasBenchmarkTaskCommonDetail getByBenchmarkIdAndMainTable(@Param("benchmarkId") Long benchmarkId,
                                                                  @Param("mainTable") String mainTable);

    @Select("select * from ad_das_benchmark_task_common_detail where id=#{commonDetailId}")
    AdDasBenchmarkTaskCommonDetail getByCommonDetailId(@Param("commonDetailId") Long commonDetailId);

    @Update("update ad_das_benchmark_task_common_detail set modify_id=#{modifyId},table_type=#{tableType},column_schema=#{columnSchema},cascade_column_schema=#{cascadeColumnSchema}," +
            "common_sqls=#{commonSqls},time_params=#{timeParams},predict_growth=#{predictGrowth}," +
            "shard_sql_column=#{shardSqlColumn}," +
            "shard_sql_flag=#{shardSqlFlag},operator=#{operator},update_time=#{updateTime} where id=#{id}")
    void update(AdDasBenchmarkTaskCommonDetail adDasBenchmarkTaskCommonDetail);

    @Delete("delete from ad_das_benchmark_task_common_detail where benchmark_id=#{benchmarkId} " +
            "and main_table=#{mainTable}")
    void deleteByBenchmarkIdAndMainTable(@Param("benchmarkId") Long benchmarkId,
                                           @Param("mainTable") String mainTable);
}
