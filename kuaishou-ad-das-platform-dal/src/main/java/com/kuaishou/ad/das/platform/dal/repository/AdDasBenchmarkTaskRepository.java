package com.kuaishou.ad.das.platform.dal.repository;

import java.util.List;

import com.kuaishou.ad.das.platform.dal.model.AdDasBenchmarkTask;

/**
 * @author wangkai <wangkai14@kuaishou.com>
 * Created on 2021-01-05
 */
public interface AdDasBenchmarkTaskRepository {
    int insert(AdDasBenchmarkTask adDasBenchMarkTask);

    List<AdDasBenchmarkTask> queryByParams(Long benchmarkId, String dataSource, String pbClass, String mainTable,
                                           String taskName, int offset, int limit);

    List<AdDasBenchmarkTask> queryAll();

    long queryCountByParams(Long benchmarkId, String dataSource, String pbClass, String mainTable,
                            String taskName);

    void deleteByTaskId(Long taskId);

    AdDasBenchmarkTask queryByTaskId(long taskId);

    List<AdDasBenchmarkTask> queryListByBenchmarkAndTable(String tableName, Long benchmarkId);

    AdDasBenchmarkTask getByBenchmarkAndName(String taskName, Long benchmarkId);

    AdDasBenchmarkTask getByNumAndName(String taskName, Integer taskNumber);

    List<AdDasBenchmarkTask> listByBenchmarkId(Long benchmarkId);

    List<AdDasBenchmarkTask> queryByBenchmarkId(Long benchmarkId, String dataSource, String pbClass, String mainTable);

    void updateAdDasBenchmarkTask(AdDasBenchmarkTask adDasBenchmarkTask);

    void deleteByBenchmarkIdeAndMainTable(Long benchmarkId, String mainTable);
}
