package com.kuaishou.ad.das.platform.dal.repository.mapper;

import java.util.Collection;
import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.kuaishou.ad.das.platform.dal.model.AdDasIncrementBacktrackServiceDetail;
import com.kuaishou.ad.das.platform.dal.model.AdDasIncrementServiceInfo;
import com.kuaishou.infra.boot.jdbc.datasource.DataSourceRouting;

/**
 * @author caoyinhang <caoyinhang@kuaishou.com>
 * Created on 2021-07-06
 */

@Mapper
@DataSourceRouting("adDas")
public interface AdDasIncrementServiceInfoMapper {

    @Select("SELECT * from ad_das_increment_service_info "
            + " where id = #{id} ")
    AdDasIncrementServiceInfo listById(@Param("id") Long id);

    @Select("SELECT * from ad_das_increment_service_info ")
    List<AdDasIncrementServiceInfo> listAll();

    @Insert("insert into ad_das_increment_service_info(incre_id,owner_name," +
            "region,biz_def,tenant,product_def,topic_type)" +
            " values(#{increId},#{ownerName},#{region},#{bizDef},#{tenant},#{productDef},#{topicType})")
    Integer insert(AdDasIncrementServiceInfo adDasIncrementServiceInfo);

    @Update({"<script> " +
            " update ad_das_increment_service_info set incre_id=#{increId},"+
            " <if test = \" ownerName != null and ownerName != '' \"> " +
            " owner_name=#{ownerName}, " +
            " </if> " +
            " <if test = \" region != null and region != '' \"> " +
            " region=#{region}, " +
            " </if> " +
            " <if test = \" bizDef != null and bizDef != '' \"> " +
            " biz_def=#{bizDef}, " +
            " </if> " +
            " <if test = \" tenant != null and tenant != '' \"> " +
            " tenant=#{tenant}, " +
            " </if> " +
            " <if test = \" topicType != null and topicType != '' \"> " +
            " topic_type=#{topicType}" +
            " </if> " +
            " where id=#{id}" +
            "</script>"})
    void update(AdDasIncrementServiceInfo adDasIncrementServiceInfo);

    @Delete("delete from ad_das_increment_service_info where id=#{dataId}")
    void deleteById(@Param("dataId") Long dataId);

    @Select({"<script> " +
            " select * "+
            " from ad_das_increment_service_info service " +
            " left join ad_das_increment increment on increment.id = service.incre_id " +
            " where 1=1 "+
            " <if test = ' incrementName != null and incrementName !=\"\" '> " +
            " and increment.increment_name=#{incrementName}" +
            " </if> " +
            " <if test = ' dataSource != null and dataSource !=\"\" '> " +
            " and increment.data_source = #{dataSource} " +
            " </if> " +
            " <if test = ' consumerGroup != null and consumerGroup !=\"\" '> " +
            " and increment.consumer_group = #{consumerGroup} " +
            " </if> " +
            " <if test = ' topic != null and topic !=\"\" '> " +
            " and increment.topic = #{topic} " +
            " </if> " +
            "</script>"})
    List<AdDasIncrementBacktrackServiceDetail> queryByCondition(@Param("incrementName") String incrementName, @Param("dataSource") String dataSource,
            @Param("consumerGroup") String consumerGroup, @Param("topic") String topic);

    @Select({"<script> " +
            " select * "+
            " from ad_das_increment_service_info \n" +
            " where 1=1 " +
            " <if test = \" increIds != null and increIds.size() != 0 \"> " +
            " and incre_id in " +
            " <foreach item = 'item' index = 'index' collection = 'increIds' open = '(' separator = ',' close = ')' > " +
            " #{item} " +
            " </foreach> " +
            " </if> " +
            "</script>"})
    List<AdDasIncrementServiceInfo> listByIncreId(@Param("increIds") Collection<Long> increIds);

}
