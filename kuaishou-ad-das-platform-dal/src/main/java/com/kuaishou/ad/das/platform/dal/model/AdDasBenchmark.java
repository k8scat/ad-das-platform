package com.kuaishou.ad.das.platform.dal.model;

import lombok.Data;

/**
 * @author Zhang Jialin <zhangjialin@kuaishou.com>
 * Created on 2021-06-04
 */
@Data
public class AdDasBenchmark {
    /**
     * 基准id
     */
    private Long id;

    /**
     * 基准流code, 用于生成基准流名称 和 hdfsPath
     */
    private String benchmarkCode;

    /**
     * 基准流名称
     */
    private String benchmarkName;

    /**
     * 基准流描述
     */
    private String benchmarkDesc;

    /**
     * 集群server名称
     */
    private String clusterName;
    /**
     * 数据流组id
     */
    private Long dataStreamGroupId;
    /**
     * HDFS路径名
     */
    private String hdfsPath;
    /**
     * 操作人邮箱前缀
     */
    private String operatorEmailPrefix;
    /**
     * 是否删除
     */
    private Integer isDelete;
    /**
     * 创建时间
     */
    private Long createTime;
    /**
     * 更新时间
     */
    private Long updateTime;
}
