package com.kuaishou.ad.das.platform.dal.repository;

import java.util.List;

import com.kuaishou.ad.das.platform.dal.dao.AdDasUserGroupCondition;
import com.kuaishou.ad.das.platform.dal.model.AdDasDownstreamUserInfo;

/**
 * @author Zhang Jialin <zhangjialin@kuaishou.com>
 * Created on 2021-06-11
 */
public interface AdDasDownstreamUserInfoRepository {
    String TABLE_NAME = "ad_das_downstream_user_info";

    /**
     * insert
     * 创建/修改时间方法内会用系统当前毫秒时间戳覆盖
     * 忽略id字段，使用DB自增
     * @param param 实体
     * @return PK
     */
    Long insert(AdDasDownstreamUserInfo param);

    /**
     * update
     * 主键字段作为where查询条件用于指定更新的记录
     * 更新时间字段在方法内会使用系统当前毫秒时间戳覆盖
     * @param param 实体
     */
    void update(AdDasDownstreamUserInfo param);

    /**
     * 查询列表
     * @param condition 查询条件
     * @return 数据列表
     */
    List<AdDasDownstreamUserInfo> query(AdDasUserGroupCondition condition);

}
