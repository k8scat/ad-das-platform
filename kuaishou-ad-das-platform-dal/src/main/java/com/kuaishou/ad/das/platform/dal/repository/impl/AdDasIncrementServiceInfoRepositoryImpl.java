package com.kuaishou.ad.das.platform.dal.repository.impl;

import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Repository;

import com.kuaishou.ad.das.platform.dal.model.AdDasIncrementBacktrackServiceDetail;
import com.kuaishou.ad.das.platform.dal.model.AdDasIncrementServiceInfo;
import com.kuaishou.ad.das.platform.dal.repository.AdDasIncrementServiceInfoRepository;
import com.kuaishou.ad.das.platform.dal.repository.mapper.AdDasIncrementServiceInfoMapper;

/**
 * @author caoyinhang <caoyinhang@kuaishou.com>
 * Created on 2021-07-06
 */

@Lazy
@Repository
public class AdDasIncrementServiceInfoRepositoryImpl implements AdDasIncrementServiceInfoRepository {

    @Autowired
    private AdDasIncrementServiceInfoMapper incrementServiceInfoMapper;

    @Override
    public int insert(AdDasIncrementServiceInfo adDasIncrementServiceInfo) {
        return incrementServiceInfoMapper.insert(adDasIncrementServiceInfo);
    }

    @Override
    public void update(AdDasIncrementServiceInfo adDasIncrementServiceInfo) {
         incrementServiceInfoMapper.update(adDasIncrementServiceInfo);
    }

    @Override
    public void deleteById(Long dataId) {
        incrementServiceInfoMapper.deleteById(dataId);
    }

    @Override
    public List<AdDasIncrementBacktrackServiceDetail> queryByCondition(String incrementName, String dataSource, String consumerGroup, String topic) {
        return incrementServiceInfoMapper.queryByCondition(incrementName,dataSource,consumerGroup,topic);
    }

    @Override
    public List<AdDasIncrementServiceInfo> queryAll() {
        return incrementServiceInfoMapper.listAll();
    }

    @Override
    public List<AdDasIncrementServiceInfo> queryByIncreId(Collection<Long> increId) {
        return incrementServiceInfoMapper.listByIncreId(increId);
    }
}
