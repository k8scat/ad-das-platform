package com.kuaishou.ad.das.platform.dal.model;

import lombok.Data;

@Data
public class AdDasBenchmarkMsg {

    private Long id;

    private Long modifyId;

    private Long benchmarkId;

    private String dataSource;

    private String mainTable;

    private String pbClass;

    private Integer tableType;

    private Integer predictGrowth;

    private String commonSqls;

    private String timeParams;

    private Integer shardSqlFlag;

    private String shardSqlColumn;

    private String columnSchema;

    private String cascadeColumnSchema;

    private Boolean deleteTable;

    private Long createTime;

    private Long updateTime;
}
