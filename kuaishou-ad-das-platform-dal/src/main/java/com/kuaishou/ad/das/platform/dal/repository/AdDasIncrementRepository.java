package com.kuaishou.ad.das.platform.dal.repository;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import com.kuaishou.ad.das.platform.dal.model.AdDasIncrement;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-06-08
 */
public interface AdDasIncrementRepository {

    int insert(AdDasIncrement adDasIncrement);

    void update(AdDasIncrement adDasIncrement);

    void deleteById(Long dataId);

    List<AdDasIncrement> queryByCondition(Set<Long> increIds, Long dataStreamGroupId, String dataSource, String dataTopic);

    AdDasIncrement queryById(Long id);

    AdDasIncrement queryByIncrementName(String incrementName);

    List<AdDasIncrement> queryByIncreId(Collection<Long> increId);

    List<AdDasIncrement> queryAll();

    List<AdDasIncrement> searchByCondition(List<Long> groupIds, List<String> topics, List<String> dataSources);

    AdDasIncrement queryByConditionBacktrack(String incrName, String dataSource, String consumerGroup,
            String incrTopic);
}
