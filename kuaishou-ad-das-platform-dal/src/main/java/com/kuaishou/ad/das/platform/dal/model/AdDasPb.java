package com.kuaishou.ad.das.platform.dal.model;

import java.sql.ResultSet;
import java.sql.SQLException;

import lombok.Data;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-11-29
 */
@Data
public class AdDasPb extends AdDasAbstarctModel<AdDasPb>{

    private Long id;

    private String pbVersion;

    private String pbEnumVersion="";

    private String operator;

    private Long createTime;

    private Long updateTime;

    @Override
    public AdDasPb parseResult(ResultSet rs) throws SQLException {
        this.setId(rs.getLong("id"));
        this.setPbVersion(rs.getString("pb_version"));
        this.setPbEnumVersion(rs.getString("pb_enum_version"));
        this.setOperator(rs.getString("operator"));
        this.setCreateTime(rs.getLong("create_time"));
        this.setUpdateTime(rs.getLong("update_time"));
        return this;
    }
}
