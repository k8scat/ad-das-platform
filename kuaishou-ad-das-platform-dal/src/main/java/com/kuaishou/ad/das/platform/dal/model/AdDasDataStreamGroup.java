package com.kuaishou.ad.das.platform.dal.model;

import lombok.Data;

/**
 * @author Zhang Jialin <zhangjialin@kuaishou.com>
 * Created on 2021-06-03
 */
@Data
public class AdDasDataStreamGroup {
    /**
     * 主键
     */
    private Long id;
    /**
     * 数据流组名称
     */
    private String dataStreamGroupName;
    /**
     * 数据流组描述
     */
    private String dataStreamGroupDesc;
    /**
     * 数据流组负责人邮箱前缀
     */
    private String ownerEmailPrefix;
    /**
     * 负责人中文名
     */
    private String ownerName;
    /**
     * 创建时间
     */
    private Long createTime;
    /**
     * 更新时间
     */
    private Long updateTime;
    /**
     * 是否删除
     */
    private Integer isDelete;
}
