package com.kuaishou.ad.das.platform.dal.model;

import lombok.Data;

/**
 * @author wangkai <wangkai14@kuaishou.com>
 * Created on 2021-01-04
 */
@Data
public class AdDasBenchmarkTaskEditTestRunRecord {
    private Long id;
    private String taskRunName;
    private String resultPath;
    private Long runTimestamp;
    private String taskNumList;
    private String taskIdList;
    private Integer taskRunStatus;
    private String taskRunDetail;
    private String operator;
    private Long createTime;
    private Long updateTime;

}
