package com.kuaishou.ad.das.platform.dal.model;

import java.io.Serializable;

import lombok.Data;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-08-06
 */
@Data
public class AdDspAccountBalance implements Serializable {

    private Long accountId;

    private Long balance;

    private Long contract_rebate;

    private Long credit_balance;

    private Long direct_rebate;

    private Long extended_balance;

    private Long pre_rebate;

    private Long push_balance;

    private Long rebate;

    public String getDataString() {
        return accountId + "," + balance + "," + contract_rebate + "," + credit_balance
                + "," + direct_rebate + "," + extended_balance + "," + pre_rebate + "," + push_balance + "," + rebate;
    }
}
