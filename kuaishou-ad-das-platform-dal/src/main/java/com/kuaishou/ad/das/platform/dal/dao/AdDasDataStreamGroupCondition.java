package com.kuaishou.ad.das.platform.dal.dao;

import java.util.List;

import lombok.Data;

/**
 * @author Zhang Jialin <zhangjialin@kuaishou.com>
 * Created on 2021-06-03
 */
@Data
public class AdDasDataStreamGroupCondition {
    /**
     * 数据流组id
     */
    private List<Long> ids;
    /**
     * 数据流组名称
     */
    private String dataStreamGroupName;
    /**
     * 页码
     */
    private Integer pageNum;
    /**
     * 单页条数
     */
    private Integer pageSize;
}
