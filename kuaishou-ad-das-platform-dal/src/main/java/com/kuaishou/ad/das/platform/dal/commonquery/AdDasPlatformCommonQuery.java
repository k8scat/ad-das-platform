package com.kuaishou.ad.das.platform.dal.commonquery;

import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.JdbcTemplate;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-05-26
 */
public interface AdDasPlatformCommonQuery {

    List<Map<String, Object>> queryBySql(JdbcTemplate jdbcTemplate,
                                         String sql,
                                         List<Object> params);

    List<Map<String, Object>> queryBySqlWithSchema(JdbcTemplate jdbcTemplate, String tableName,
                                                   Map<String, String> schemaMap, String sql, List<Object> params);

    Map<String, Object> queryMapBySql(JdbcTemplate jdbcTemplate, String sql, List<Object> params);

}
