package com.kuaishou.ad.das.platform.dal.datasource;

import org.springframework.context.annotation.Configuration;

import lombok.extern.slf4j.Slf4j;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-04-05
 */
@Slf4j
@Configuration
public class AdDasDataSourceConfiguration {

    private static String adDasEnv;

    /*@Bean
    public RoutingDataSourceCustomizer defaultTargetDataSourceCustomizer() {

        if (HostInfo.debugHost()) {
            log.info("adDasEnv={}", adDasEnv);
            return routingDataSource -> routingDataSource.setDefaultTargetDataSource("adDasTest");
        }

        if ("beta".equals(adDasEnv)) {
            return routingDataSource -> routingDataSource.setDefaultTargetDataSource("adDasBeta");
        } else {
            return routingDataSource -> routingDataSource.setDefaultTargetDataSource("adDas");
        }
    }

    @PostConstruct
    public void init() {
        adDasEnv = System.getenv("DAS_SERVICE_ENV");
        log.info("adDasEnv={}", adDasEnv);
    }

    @Bean("adDasTransactionManager")
    public PlatformTransactionManager infraDemoTransactionManager() {
        DataSourceTransactionManager transactionManager = new DataSourceTransactionManager();
        transactionManager.setDataSource(AdCoreDataSource.adDas.writeDataSource());

        return transactionManager;
    }*/
}
