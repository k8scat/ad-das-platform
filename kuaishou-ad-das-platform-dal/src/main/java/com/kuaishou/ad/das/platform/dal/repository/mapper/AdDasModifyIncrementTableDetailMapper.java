package com.kuaishou.ad.das.platform.dal.repository.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.kuaishou.ad.das.platform.dal.model.AdDasModifyIncrementTableDetail;
import com.kuaishou.infra.boot.jdbc.datasource.DataSourceRouting;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-06-14
 */
@Mapper
@DataSourceRouting("adDas")
public interface AdDasModifyIncrementTableDetailMapper {

    @Select("SELECT * from ad_das_modify_increment_table_detail "
            + " where incre_id = #{increId} ")
    List<AdDasModifyIncrementTableDetail> listInfoCondition(@Param("increId") String increId);

    @Select("SELECT * from ad_das_modify_increment_table_detail "
            + " where modify_id = #{modifyId} ")
    List<AdDasModifyIncrementTableDetail> listByModifyId(@Param("modifyId") Long modifyId);

    @Select("SELECT * from ad_das_modify_increment_table_detail "
            + " where incre_id = #{increId} ")
    List<AdDasModifyIncrementTableDetail> listByIncreId(@Param("increId") Long increId);

    @Select({"<script> " +
            " select * "+
            " from ad_das_modify_increment_table_detail \n" +
            " where 1=1 " +
            " <if test = \" modifyIds != null and modifyIds.size() != 0 \"> " +
            " and modify_id in " +
            " <foreach item = 'item' index = 'index' collection = 'modifyIds' open = '(' separator = ',' close = ')' > " +
            " #{item} " +
            " </foreach> " +
            " </if> " +
            "</script>"})
    List<AdDasModifyIncrementTableDetail> queryByModifyIds(@Param("modifyIds") List<Long> modifyIds);

    @Select("SELECT * from ad_das_modify_increment_table_detail "
            + " where incre_id = #{increId} and incre_table = #{mainTable} order by modify_id desc limit 1")
    AdDasModifyIncrementTableDetail queryMainTableLast(@Param("increId")Long increId, @Param("mainTable")String mainTable);


    @Select({"<script> " +
            " select * "+
            " from ad_das_modify_increment_table_detail " +
            " where 1=1 " +
            " <if test = \" tableName != null \"> " +
            " and incre_table = #{tableName}" +
            " </if> " +
            " <if test = \" increId != null \"> " +
            " and incre_id = #{increId}" +
            " </if> " +
            "</script>"})
    List<AdDasModifyIncrementTableDetail> searchTableWithModifyMsg(@Param("increId") Long increId, @Param("tableName") String tableName);

    @Insert("insert into ad_das_modify_increment_table_detail(incre_id,modify_id,data_source,incre_table,table_type,primary_id_key,table_column,pb_class_enum,operat_type," +
            "operator,approver," +
            "create_time,update_time,cascade_schema,cascaded_table)" +
            " values(#{increId},#{modifyId},#{dataSource},#{increTable},#{tableType},#{primaryIdKey}" +
            ",#{tableColumn},#{pbClassEnum},#{operatType},#{operator},#{approver},#{createTime},#{updateTime},#{cascadeSchema},#{cascadedTable})")
    Integer insert(AdDasModifyIncrementTableDetail adDasModifyIncrementTableDetail);

    @Update("update ad_das_modify_increment_table_detail set modify_id=#{modifyId},table_type=#{tableType}," +
            "primary_id_key=#{primaryIdKey},table_column=#{tableColumn},pb_class_enum=#{pbClassEnum}," +
            "operat_type=#{operatType},cascade_schema=#{cascadeSchema},cascaded_table=#{cascadedTable},update_time=#{updateTime},operator=#{operator} " +
            " where id=#{id}")
    void update(AdDasModifyIncrementTableDetail adDasModifyIncrementTableDetail);

    @Delete("delete from ad_das_modify_increment_table_detail where  modify_id=#{modifyId} and incre_table=#{increTable}")
    void deletByModifyIdAndIncreId(@Param("modifyId") Long modifyId, @Param("increTable") String increTable);
}
