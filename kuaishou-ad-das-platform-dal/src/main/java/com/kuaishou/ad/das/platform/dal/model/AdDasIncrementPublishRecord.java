package com.kuaishou.ad.das.platform.dal.model;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.kuaishou.ad.das.platform.utils.ModelConvertUtils;

import lombok.Data;

/**
 * 增量服务发布记录
 *
 * @author liutao <liutao06@kuaishou.com>
 * Created on 2021-01-06
 */
@Data
public class AdDasIncrementPublishRecord extends AdDasAbstarctModel<AdDasIncrementPublishRecord> {

    private Long id;

    private Long modifyId;

    private String incrementService;

    private String hostName;

    private String curPbInfo;

    private String prePbInfo;


    private String dataSource;


    private Integer publishStatus;

    private Long createTime;

    private Long updateTime;

    @Override
    public AdDasIncrementPublishRecord parseResult(ResultSet rs) throws SQLException {
        ModelConvertUtils.bindDataToDTO(rs, this);
        return this;
    }
}
