package com.kuaishou.ad.das.platform.dal.repository;

import java.util.List;

import com.kuaishou.ad.das.platform.dal.model.AdDasPbClassColumn;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-11-29
 */
public interface AdDasPbClassColumnRepository {

    int insert(AdDasPbClassColumn adDasPbClassColumn);

    int[] batchInsert(List<AdDasPbClassColumn> adDasPbClassColumns);

    void batchUpdate(List<AdDasPbClassColumn> adDasPbClassColumns);

    void batchDelete(List<AdDasPbClassColumn> adDasPbClassColumns);

    List<AdDasPbClassColumn> query(String pbVersion, String pbClass);
}
