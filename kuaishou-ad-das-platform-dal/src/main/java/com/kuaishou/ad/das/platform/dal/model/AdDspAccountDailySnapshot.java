package com.kuaishou.ad.das.platform.dal.model;

import java.sql.ResultSet;
import java.sql.SQLException;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-12-09
 */
@Data
public class AdDspAccountDailySnapshot extends AdDasAbstarctModel<AdDspAccountDailySnapshot> {

    private Long id;

    private Long accountId;

    private Long agentId;

    private String date;

    private Long chargeDate;

    @ApiModelProperty("现金实际消耗")
    private Long realCharged;

    @ApiModelProperty("后返实际消耗")
    private Long rebateRealCharged;

    @ApiModelProperty("前返实际消耗")
    private Long preRebateRealCharged;

    @ApiModelProperty("激励账号实际消耗")
    private Long directRebateRealCharged;

    @ApiModelProperty("框返实际消耗")
    private Long contractRebateRealCharged;

    @ApiModelProperty("信用账户实际消耗")
    private Long creditRealCharged;

    @ApiModelProperty("预留账户充值金额")
    private Long extendedRealCharged;

    @ApiModelProperty("现金充值金额")
    private Long recharge;

    @ApiModelProperty("后返充值金额")
    private Long rebateRecharge;

    @ApiModelProperty("前返充值金额")
    private Long preRebateRecharge;

    @ApiModelProperty("激励账号充值金额")
    private Long directRebateRecharge;

    @ApiModelProperty("框返充值金额")
    private Long contractRebateRecharge;

    @ApiModelProperty("信用账户充值金额")
    private Long creditRecharge;

    @ApiModelProperty("预留账户充值金额")
    private Long extendedRecharge;

    @ApiModelProperty("现金")
    private Long balance;

    @ApiModelProperty("后返")
    private Long rebate;

    @ApiModelProperty("前返")
    private Long preRebate;

    @ApiModelProperty("激励账号")
    private Long directRebate;

    @ApiModelProperty("框返")
    private Long contractRebate;

    @ApiModelProperty("信用账户")
    private Long creditBalance;

    @ApiModelProperty("预留账户")
    private Long extendedBalance;

    public Long getTotalBalance() {
        return balance + rebate + preRebate + directRebate + creditBalance + contractRebate
                + extendedBalance;
    }

    public Long getTotalRebate() {
        return rebate + preRebate + contractRebate;
    }

    @Override
    public AdDspAccountDailySnapshot parseResult(ResultSet rs) throws SQLException {

        this.setId(rs.getLong("id"));
        this.setAccountId(rs.getLong("account_id"));
        this.setAgentId(rs.getLong("agent_id"));
        this.setDate(rs.getString("date"));
        this.setChargeDate(rs.getLong("charge_date"));
        this.setRealCharged(rs.getLong("real_charged"));
        this.setRebateRealCharged(rs.getLong("rebate_real_charged"));
        this.setPreRebateRealCharged(rs.getLong("pre_rebate_real_charged"));
        this.setDirectRebateRealCharged(rs.getLong("direct_rebate_real_charged"));
        this.setContractRebateRealCharged(rs.getLong("contract_rebate_real_charged"));
        this.setCreditRealCharged(rs.getLong("credit_real_charged"));
        this.setExtendedRealCharged(rs.getLong("extended_real_charged"));
        this.setRecharge(rs.getLong("recharge"));
        this.setRebateRecharge(rs.getLong("rebate_recharge"));
        this.setPreRebateRecharge(rs.getLong("pre_rebate_recharge"));
        this.setDirectRebateRecharge(rs.getLong("direct_rebate_recharge"));
        this.setContractRebateRecharge(rs.getLong("contract_rebate_recharge"));
        this.setCreditRecharge(rs.getLong("credit_recharge"));
        this.setExtendedRecharge(rs.getLong("extended_recharge"));
        this.setBalance(rs.getLong("balance"));
        this.setRebate(rs.getLong("rebate"));
        this.setPreRebate(rs.getLong("pre_rebate"));
        this.setDirectRebate(rs.getLong("direct_rebate"));
        this.setContractRebate(rs.getLong("contract_rebate"));
        this.setCreditBalance(rs.getLong("credit_balance"));
        this.setExtendedBalance(rs.getLong("extended_balance"));

        return this;
    }
}
