package com.kuaishou.ad.das.platform.dal.dao;

import java.util.List;

import lombok.Data;

/**
 * @author Zhang Jialin <zhangjialin@kuaishou.com>
 * Created on 2021-06-09
 */
@Data
public class BenchmarkCommonDetailCondition {
    private List<Long> benchmarkIds;
    private List<String> mainTables;

}
