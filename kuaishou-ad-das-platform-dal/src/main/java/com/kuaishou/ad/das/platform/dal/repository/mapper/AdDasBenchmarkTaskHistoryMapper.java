package com.kuaishou.ad.das.platform.dal.repository.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.kuaishou.ad.das.platform.dal.model.AdDasBenchmarkTaskHistory;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-07-13
 */
public interface AdDasBenchmarkTaskHistoryMapper {

    @Insert("insert into ad_das_benchmark_task_history(task_name,task_number,modify_id,benchmark_id," +
            "data_source,main_table,table_type,task_shard_list,pb_class,pb_class_full_name," +
            "task_sql_flag,task_sqls,shard_sql_flag,shard_ids,task_status,concurrency,operator,create_time,update_time)" +
            "values(#{taskName},#{taskNumber},#{modifyId},#{benchmarkId},#{dataSource}," +
            "#{mainTable},#{tableType},#{taskShardList},#{pbClass},#{pbClassFullName},#{taskSqlFlag}," +
            "#{taskSqls},#{shardSqlFlag},#{shardIds},#{taskStatus},#{concurrency},#{operator},#{creatTime},#{updateTime})")
    int insert(AdDasBenchmarkTaskHistory adDasBenchmarkTaskHistory);

    @Select("select * from ad_das_benchmark_task_history where modify_id=#{modifyId}")
    List<AdDasBenchmarkTaskHistory> listByModifyId(@Param("modifyId") Long modifyId);
}
