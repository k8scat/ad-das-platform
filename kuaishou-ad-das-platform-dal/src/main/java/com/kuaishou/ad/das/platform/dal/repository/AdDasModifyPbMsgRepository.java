package com.kuaishou.ad.das.platform.dal.repository;

import java.util.List;

import com.kuaishou.ad.das.platform.dal.model.AdDasModifyPbMsg;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-11-29
 */
public interface AdDasModifyPbMsgRepository {

    int insert(AdDasModifyPbMsg adDasModifyPbMsg);

    List<AdDasModifyPbMsg> queryByModifyId(Long modifyId);

    List<AdDasModifyPbMsg> queryByModifyIds(List<Long> modifyIds);
}
