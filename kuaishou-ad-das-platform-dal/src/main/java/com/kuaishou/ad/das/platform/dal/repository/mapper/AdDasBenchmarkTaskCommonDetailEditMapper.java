package com.kuaishou.ad.das.platform.dal.repository.mapper;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.kuaishou.ad.das.platform.dal.model.AdDasBenchmarkTaskCommonDetailEdit;
import com.kuaishou.infra.boot.jdbc.datasource.DataSourceRouting;

/**
 * @author wangkai <wangkai14@kuaishou.com>
 * Created on 2020-12-22
 */

@Mapper
@DataSourceRouting("adDas")
public interface AdDasBenchmarkTaskCommonDetailEditMapper {
    @Insert("insert into ad_das_benchmark_task_common_detail_edit(benchmark_id,main_table,table_type," +
            "column_schema,cascade_column_schema,common_sqls,time_params,predict_growth,shard_sql_column," +
            "shard_sql_flag,operator,create_time,update_time,data_source,pb_class,modify_id) values(#{benchmarkId},#{mainTable},#{tableType}," +
            "#{columnSchema},#{cascadeColumnSchema},#{commonSqls},#{timeParams},#{predictGrowth},#{shardSqlColumn}," +
            "#{shardSqlFlag},#{operator},#{createTime},#{updateTime},#{dataSource},#{pbClass},#{modifyId})")
    int insert(AdDasBenchmarkTaskCommonDetailEdit adDasBenchMarkTaskCommonDetailEdit);

    @Select("select * from ad_das_benchmark_task_common_detail_edit where " +
            "benchmark_id=#{benchmarkId} and main_table=#{mainTable}")
    AdDasBenchmarkTaskCommonDetailEdit getByBenchmarkTypeAndMainTable(@Param("benchmarkId") Long benchmarkId,
                                                                      @Param("mainTable") String mainTable);


    @Update("update ad_das_benchmark_task_common_detail_edit set modify_id=#{modifyId},benchmark_id=#{benchmarkId}," +
            "main_table=#{mainTable},table_type=#{tableType},column_schema=#{columnSchema},cascade_column_schema=#{cascadeColumnSchema},common_sqls=#{commonSqls}," +
            "time_params=#{timeParams},predict_growth=#{predictGrowth},shard_sql_column=#{shardSqlColumn},shard_sql_flag=#{shardSqlFlag},operator=#{operator},update_time=#{updateTime} " +
            "where id=#{id}")
    void update(AdDasBenchmarkTaskCommonDetailEdit adDasBenchmarkTaskCommonDetailEdit);

    @Delete("delete from ad_das_benchmark_task_common_detail_edit where benchmark_id=#{benchmarkId} " +
            "and main_table=#{mainTable}")
    void deleteByBenchmarkTypeAndMainTable(@Param("benchmarkId") Long benchmarkId,
                                           @Param("mainTable") String mainTable);
}
