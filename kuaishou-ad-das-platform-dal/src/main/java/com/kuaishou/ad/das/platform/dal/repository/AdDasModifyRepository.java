package com.kuaishou.ad.das.platform.dal.repository;

import java.util.List;

import com.kuaishou.ad.das.platform.dal.dao.ModifyCondition;
import com.kuaishou.ad.das.platform.dal.model.AdDasModify;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-11-29
 */
public interface AdDasModifyRepository {

    String TABLE_NAME = "ad_das_modify";

    long insert(AdDasModify adDasModify);

    /**
     * 插入后返回id
     *
     * @param adDasModify
     * @return
     */
    Long insertWithIdReturn(AdDasModify adDasModify);

    List<AdDasModify> queryByNameAndCreateTime(String modifyName,
                                               Integer modifyType,
                                               String approver, Long createTime);

    Long countByCondition(String modifyName, Integer modifyType,
                          Integer modifyStatus,
                          Long startTime, Long endTime);

    List<AdDasModify> queryByCondition(String modifyName, Integer modifyType,
                                       Integer modifyStatus,
                                       Long startTime, Long endTime,
                                       Integer limit, Integer offset);

    List<AdDasModify> queryByIdAndType(Long modifyId, Integer modifyType);

    int updateApprove(Long modifyId, String approver, Integer approveType, String reason, Long updateTime);

    int updateStatus(Long modifyId, Integer modifyStatus, String operator, Long updateTime);

    int updateStatus(Long modifyId, Integer modifyStatus, Long updateTime);

    int updateApprove(Long modifyId, String operator, Integer approveType,
                      String reason, Long updateTime, Integer beforeStatus);

    AdDasModify queryById(Long modifyId);

    List<AdDasModify> queryByIds(List<Long> modifyIds);

    List<AdDasModify> queryByConditions(List<Long> modifyIds,String modifyName,
                                        Integer modifyType, Integer modifyStatus,
                                        Long startTime, Long endTime,
                                        Integer limit, Integer offset);

    List<AdDasModify> queryByStatus(Integer modifyType, Integer modifyStatus);

    int updateAdDasModify(AdDasModify adDasModify);

    int submitApprove(Long modifyId, String operator, Integer approveType, Long updateTime, String approver);

    List<AdDasModify> query(ModifyCondition condition);

}
