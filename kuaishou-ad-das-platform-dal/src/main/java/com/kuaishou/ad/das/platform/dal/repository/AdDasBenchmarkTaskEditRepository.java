package com.kuaishou.ad.das.platform.dal.repository;

import java.util.List;

import com.kuaishou.ad.das.platform.dal.model.AdDasBenchmarkTaskEdit;

/**
 * @author wangkai <wangkai14@kuaishou.com>
 * Created on 2020-12-22
 */
public interface AdDasBenchmarkTaskEditRepository {

    int insert(AdDasBenchmarkTaskEdit adDasBenchMarkTaskEdit);

    AdDasBenchmarkTaskEdit getLastTask();

    void deleteByTaskId(Long taskId);

    List<AdDasBenchmarkTaskEdit> getByBenchmarkTypeAndMainTable(Long benchmarkId, String mainTable);

    void updateAdDasBenchmarkTaskEdit(AdDasBenchmarkTaskEdit adDasBenchmarkTaskEdit);

    AdDasBenchmarkTaskEdit getByTaskId(long taskId);

    AdDasBenchmarkTaskEdit getByTypeAndName(String taskName, Long benchmarkId);

    List<AdDasBenchmarkTaskEdit> getByTypeAndTable(String tableName, Long benchmarkId);

    List<AdDasBenchmarkTaskEdit> getByBenchmarkType(Long benchmarkId, String dataSource, String pbClass, String mainTable);

    List<AdDasBenchmarkTaskEdit> getAll();

    List<AdDasBenchmarkTaskEdit> getByParams(Long benchmarkId, String dataSource, String pbClass,
                                             String mainTable, String taskName,
                                             Integer taskStatus, int offset, int limit);

    long getTotalByParams(Long benchmarkId, String dataSource, String pbClass,
                          String mainTable, String taskName,
                          Integer taskStatus);

    void deleteByBenchmarkTypeAndMainTable(Long benchmarkId, String mainTable);

    void updateStatusByBenchmarkTypeAndMainTable(Long benchmarkId, String mainTable, Integer taskStatus, String operator, Long updateTime);

    List<AdDasBenchmarkTaskEdit> getByBenchmarkTypeAndMainTableAndDataSource(Long benchmarkId, String mainTable,String dataSource);

}
