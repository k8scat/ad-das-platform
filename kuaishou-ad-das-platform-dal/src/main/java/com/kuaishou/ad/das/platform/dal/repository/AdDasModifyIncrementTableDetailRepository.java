package com.kuaishou.ad.das.platform.dal.repository;

import java.util.List;

import com.kuaishou.ad.das.platform.dal.model.AdDasModifyIncrementTableDetail;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-06-14
 */
public interface AdDasModifyIncrementTableDetailRepository {

    int insert(AdDasModifyIncrementTableDetail adDasModifyIncrementTableDetail);

    void update(AdDasModifyIncrementTableDetail adDasModifyIncrementTableDetail);

    void deleteByModifyIdAndTable(Long modifyId, String increTable);

    List<AdDasModifyIncrementTableDetail> queryByModifyId(Long modifyId);

    List<AdDasModifyIncrementTableDetail> queryByIncreId(Long increId);

    List<AdDasModifyIncrementTableDetail> searchTableWithModifyMsg(Long increId, String tableName);

    List<AdDasModifyIncrementTableDetail> queryByModifyIds(List<Long> modifyIds);


    AdDasModifyIncrementTableDetail queryMainTableLast(Long increId, String mainTable);
}
