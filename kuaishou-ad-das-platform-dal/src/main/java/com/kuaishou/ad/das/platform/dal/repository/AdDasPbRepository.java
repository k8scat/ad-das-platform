package com.kuaishou.ad.das.platform.dal.repository;

import java.util.List;

import com.kuaishou.ad.das.platform.dal.model.AdDasPb;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-11-29
 */
public interface AdDasPbRepository {

    long insert(AdDasPb adDasPb);

    /**
     * 查询当前最新PB数据
     * @return
     */
    AdDasPb queryLastPb();


    List<AdDasPb> queryAll();

    /**
     * 查询指定 version 的数据
     * @param pbVersion
     * @return
     */
    AdDasPb queryByVersion(String pbVersion);

}
