package com.kuaishou.ad.das.platform.dal.model;

import lombok.Data;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-06-08
 */
@Data
public class AdDasIncrement {

    private Long id;

    /**
     * 增量流 code
     */
    private String incrementCode;

    /**
     * 增量流集群名称
     */
    private String incrementName;

    /**
     * 增量流描述
     */
    private String incrementDesc;

    private String dataSource;

    /**
     * 增量消费组
     */
    private String consumerGroup;

    private Long dataStreamGroupId;

    private String topic;

    private String operatorEmailPrefix;

    private Integer isDelete;

    /**
     * 增量流类型: 1-online 2-preOnline
     */
    private Integer increStreamType;

    /**
     * 增量流关联Id, 当increStream=2时, 代表关联的online流id
     */
    private Long increStreamRelatedId;

    private Long createTime;

    private Long updateTime;
}
