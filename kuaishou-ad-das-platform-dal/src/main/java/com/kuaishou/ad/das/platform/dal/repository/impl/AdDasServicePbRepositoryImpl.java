package com.kuaishou.ad.das.platform.dal.repository.impl;

import java.util.List;
import java.util.Map;

import org.springframework.context.annotation.Lazy;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import com.google.common.collect.Maps;
import com.kuaishou.ad.das.platform.dal.model.AdDasServicePb;
import com.kuaishou.ad.das.platform.dal.repository.AdDasServicePbRepository;
import com.kuaishou.ad.das.platform.dal.repository.common.AdDasAbstractRepository;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-11-29
 */
@Lazy
@Repository
public class AdDasServicePbRepositoryImpl extends AdDasAbstractRepository<AdDasServicePb>
        implements AdDasServicePbRepository {

    @Override
    protected RowMapper<AdDasServicePb> getRowMapper() {
        return (rs, rowNum) -> {
            AdDasServicePb adDasServicePb = new AdDasServicePb();
            adDasServicePb.parseResult(rs);

            return adDasServicePb;
        };
    }

    @Override
    protected JdbcTemplate getJdbcTemplate(String dataSource) {
        return jdbcTemplate;
    }

    @Override
    public int insert(AdDasServicePb adDasServicePb) {
        String insertSql = "insert into ad_das_service_pb"
                + "(service_name, service_type, pb_version, pb_enum_version, pb_common_version, service_pb_status,operator,create_time,update_time)"
                + " values(?,?,?,?,?,?,?,?)";

        return jdbcTemplateWrite.update(
                insertSql, adDasServicePb.getServiceName(), adDasServicePb.getServiceType(),
                adDasServicePb.getPbVersion(), adDasServicePb.getPbEnumVersion(), adDasServicePb.getPbCommonVersion(),
                adDasServicePb.getServicePbStatus(), adDasServicePb.getOperator(),
                adDasServicePb.getCreateTime(), adDasServicePb.getUpdateTime());
    }

    @Override
    public List<AdDasServicePb> queryCurrentByVersion(String pbVersion, Integer servicePbStatus) {

        String selectSql = "select * from ad_das_service_pb where pb_version=? and service_pb_status=?";
        return jdbcTemplate.query(selectSql, getRowMapper(), pbVersion, servicePbStatus);
    }

    @Override
    public List<AdDasServicePb> queryCurService(Integer servicePbStatus) {
        String selectSql = "select * from ad_das_service_pb where service_pb_status=?";
        return jdbcTemplate.query(selectSql, getRowMapper(), servicePbStatus);
    }

    @Override
    public List<AdDasServicePb> queryByService(String serviceName, Integer servicePbStatus) {
        String selectSql = "select * from ad_das_service_pb where service_name=? and service_pb_status=?";
        return jdbcTemplate.query(selectSql, getRowMapper(), serviceName, servicePbStatus);
    }

    @Override
    public List<AdDasServicePb> queryByServiceList(List<String> serviceNameList) {
        Map<String, Object> params = Maps.newHashMap();
        params.put("serviceNameList", serviceNameList);
        String selectSql = "select * from ad_das_service_pb where service_name in(:serviceNameList) and service_pb_status=1";
        NamedParameterJdbcTemplate jdbc = new NamedParameterJdbcTemplate(jdbcTemplate);
        return jdbc.query(selectSql, params, getRowMapper());
    }

    @Override
    public List<AdDasServicePb> queryByServiceType(String serviceType) {
        String selectSql = "select * from ad_das_service_pb where service_type=? and service_pb_status=1";
        return jdbcTemplate.query(selectSql, getRowMapper(), serviceType);
    }

    @Override
    public int update(String serviceName, String serviceType, String pbVersion, Long updateTime) {
        String updateSql = "update ad_das_service_pb set pb_version=?, update_time=? "
                + "where service_type=? and service_name=? ";

        return jdbcTemplateWrite.update(updateSql, pbVersion, updateTime, serviceType, serviceName);
    }
}
