package com.kuaishou.ad.das.platform.dal.repository;

import java.util.List;

import com.kuaishou.ad.das.platform.dal.dao.AdDasDataStreamGroupCondition;
import com.kuaishou.ad.das.platform.dal.model.AdDasDataStreamGroup;

/**
 * @author Zhang Jialin <zhangjialin@kuaishou.com>
 * Created on 2021-06-03
 */
public interface AdDasDataStreamGroupRepository {

    /**
     * 查询
     * @param condition 查询条件
     * @return 列表
     */
    List<AdDasDataStreamGroup> query(AdDasDataStreamGroupCondition condition);

    /**
     * 搜索数据流组
     * @param dataStreamGroupIds
     * @param dataStreamGroupName
     * @param dataSourceNames
     * @param incrementTopics
     * @param hdfsPath
     * @return
     */
    List<AdDasDataStreamGroup> queryByCondition(List<Long> dataStreamGroupIds, String dataStreamGroupName,
                                                List<String> dataSourceNames, List<String> incrementTopics,
                                                List<String> hdfsPath);

    /**
     * 查询条数
     * @param condition 查询条件
     * @return 列表
     */
    Long count(AdDasDataStreamGroupCondition condition);

    /**
     * 根据主键id 查询数据流组
     * @param id 数据流组id
     * @return 实体
     */
    AdDasDataStreamGroup queryById(Long id);

    /**
     * 更新数据流组
     * @param adDasDataStreamGroup 数据流组实体，为null1的不更新
     */
    void update(AdDasDataStreamGroup adDasDataStreamGroup);

    void insert(AdDasDataStreamGroup adDasDataStreamGroup);

    void deletById(Long dataId);
}
