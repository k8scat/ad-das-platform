package com.kuaishou.ad.das.platform.dal.repository.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import com.kuaishou.ad.das.platform.dal.model.AdDasBenchmarkTaskEdit;
import com.kuaishou.ad.das.platform.dal.repository.AdDasBenchmarkTaskEditRepository;
import com.kuaishou.ad.das.platform.dal.repository.mapper.AdDasBenchmarkTaskEditMapper;

/**
 * @author wangkai <wangkai14@kuaishou.com>
 * Created on 2020-12-22
 */
@Lazy
@Service
public class AdDasBenchmarkTaskEditRepositoryImpl
        implements AdDasBenchmarkTaskEditRepository {
    @Autowired
    private AdDasBenchmarkTaskEditMapper adDasBenchMarkTaskEditMapper;

    @Override
    public int insert(AdDasBenchmarkTaskEdit adDasBenchMarkTaskEdit) {
        return adDasBenchMarkTaskEditMapper.insert(adDasBenchMarkTaskEdit);
    }

    @Override
    public AdDasBenchmarkTaskEdit getLastTask() {
        return adDasBenchMarkTaskEditMapper.getLastTask();
    }

    @Override
    public void deleteByTaskId(Long taskId) {
        adDasBenchMarkTaskEditMapper.deleteByTaskId(taskId);
    }

    @Override
    public List<AdDasBenchmarkTaskEdit> getByBenchmarkTypeAndMainTable(Long benchmarkId, String mainTable) {
        return adDasBenchMarkTaskEditMapper.getByBenchmarkTypeAndMainTable(benchmarkId, mainTable);
    }

    @Override
    public void updateAdDasBenchmarkTaskEdit(AdDasBenchmarkTaskEdit adDasBenchmarkTaskEdit) {
        adDasBenchMarkTaskEditMapper.update(adDasBenchmarkTaskEdit);
    }

    @Override
    public AdDasBenchmarkTaskEdit getByTaskId(long taskId) {
        return adDasBenchMarkTaskEditMapper.getById(taskId);
    }

    @Override
    public AdDasBenchmarkTaskEdit getByTypeAndName(String taskName, Long benchmarkId) {
        return adDasBenchMarkTaskEditMapper.getByBenchmarkAndName(taskName, benchmarkId);
    }

    @Override
    public List<AdDasBenchmarkTaskEdit> getByTypeAndTable(String tableName, Long benchmarkId) {
        return adDasBenchMarkTaskEditMapper.queryListByBenchmarkAndTable(tableName, benchmarkId);
    }

    @Override
    public List<AdDasBenchmarkTaskEdit> getByBenchmarkType(Long benchmarkId, String dataSource, String pbClass, String mainTable) {
        return adDasBenchMarkTaskEditMapper.getByBenchmarkId(benchmarkId, dataSource, pbClass, mainTable);
    }

    @Override
    public List<AdDasBenchmarkTaskEdit> getAll() {
        return adDasBenchMarkTaskEditMapper.queryAll();
    }

    @Override
    public List<AdDasBenchmarkTaskEdit> getByParams(Long benchmarkId, String dataSource, String pbClass, String mainTable,
                                                    String taskName, Integer taskStatus, int offset, int limit) {
        return adDasBenchMarkTaskEditMapper.getByParams(benchmarkId, dataSource, pbClass, mainTable, taskName, taskStatus, offset, limit);
    }

    @Override
    public long getTotalByParams(Long benchmarkId, String dataSource, String pbClass, String mainTable, String taskName, Integer taskStatus) {
        return adDasBenchMarkTaskEditMapper.getTotalByParams(benchmarkId, dataSource, pbClass, mainTable, taskName, taskStatus);
    }

    @Override
    public void deleteByBenchmarkTypeAndMainTable(Long benchmarkId, String mainTable) {
        adDasBenchMarkTaskEditMapper.deleteByBenchmarkIdAndMainTable(benchmarkId, mainTable);
    }

    @Override
    public void updateStatusByBenchmarkTypeAndMainTable(Long benchmarkId, String mainTable, Integer taskStatus, String operator, Long updateTime) {
        adDasBenchMarkTaskEditMapper.updateStatusByBenchmarkIdAndMainTable(benchmarkId, mainTable, taskStatus, operator, updateTime);
    }

    @Override
    public List<AdDasBenchmarkTaskEdit> getByBenchmarkTypeAndMainTableAndDataSource(Long benchmarkId, String mainTable, String dataSource) {
        return adDasBenchMarkTaskEditMapper.getByBenchmarkTypeAndMainTableAndDataSource(benchmarkId, mainTable,dataSource);
    }
}
