package com.kuaishou.ad.das.platform.dal.commonquery.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import com.google.common.collect.Maps;
import com.kuaishou.ad.das.platform.dal.commonquery.AdDasPlatformCommonQuery;

import lombok.extern.slf4j.Slf4j;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-05-26
 */
@Slf4j
@Repository
public class AdDasPlatformCommonQueryImpl implements AdDasPlatformCommonQuery {

    @Override
    public List<Map<String, Object>> queryBySql(
            JdbcTemplate jdbcTemplate, String sql, List<Object> params) {

        List<Map<String, Object>> data = jdbcTemplate.queryForList(sql, params.toArray());

        return data;
    }

    @Override
    public List<Map<String, Object>> queryBySqlWithSchema(JdbcTemplate jdbcTemplate, String tableName,
                                                          Map<String, String> schemaMap, String sql,
                                                          List<Object> params) {

        List<Map<String, Object>> data = jdbcTemplate.query(sql, params.toArray(), this.getRowMapper(tableName, schemaMap));

        return data;
    }

    @Override
    public Map<String, Object> queryMapBySql(JdbcTemplate jdbcTemplate, String sql, List<Object> params) {
        Map<String, Object> data = jdbcTemplate.queryForMap(sql, params.toArray());

        return data;
    }

    private RowMapper<Map<String, Object>> getRowMapper(String tableName, Map<String, String> schemaMap) {
        return (rs, row) -> {
            Map<String, Object> dataMap = Maps.newHashMap();

            if (!CollectionUtils.isEmpty(schemaMap)) {
                schemaMap.forEach((key, value) -> {
                    try {
                        dataMap.put(key, getResultObject(rs, key, value));
                    } catch (SQLException throwables) {

                        log.error("AdDasPlatformIncreCommonQueryImpl getRowMapper() occur error! key={}, value={}",
                                key, value, throwables);
                    }
                });
            } else {

                log.error("AdDasPlatformIncreCommonQueryImpl getRowMapper() tableName={} must be configured schemaMap", tableName);
            }
            return dataMap;
        };
    }

    private Object getResultObject(ResultSet rs, String columnName, String columnType) throws SQLException {
        switch (columnType) {
            case "INT" :
                Integer dataInt = rs.getInt(columnName);
                if (dataInt == 0 && rs.wasNull()) {
                    return null;
                }
                return dataInt;
            case "LONG" :
                Long dataLong = rs.getLong(columnName);
                if (dataLong == 0L && rs.wasNull()) {
                    return null;
                }
                return rs.getLong(columnName);
            case "STRING" :
                return rs.getString(columnName);
            case "DOUBLE" :
                Double dataDouble = rs.getDouble(columnName);
                if (dataDouble == 0.0D && rs.wasNull()) {
                    return null;
                }
                return rs.getDouble(columnName);
            case "FLOAT" :
                Float dataFloat = rs.getFloat(columnName);
                if (dataFloat == 0.0D && rs.wasNull()) {
                    return null;
                }
                return rs.getFloat(columnName);
            default: return null;
        }
    }

}
