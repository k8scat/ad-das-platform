package com.kuaishou.ad.das.platform.dal.model;

import java.sql.ResultSet;
import java.sql.SQLException;

import lombok.Data;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-11-29
 */
@Data
public class AdDasModifyPbMsg extends AdDasAbstarctModel<AdDasModifyPbMsg> {

    private Long id;

    private Long modifyId;

    private String curLastestPbVersion;

    private String serviceName;

    private String serviceType;

    private String curPbVersion;

    private String targetPbVersion;

    private String targetPbEnumVersion;

    private String targetPbCommonVersion;

    private String preVersion;

    private String operator;

    private String crLinks;

    private String piplineLinks;

    private Long createTime;

    private Long updateTime;

    @Override
    public AdDasModifyPbMsg parseResult(ResultSet rs) throws SQLException {

        this.setId(rs.getLong("id"));
        this.setModifyId(rs.getLong("modify_id"));
        this.setCurLastestPbVersion(rs.getString("cur_lastest_pb_version"));
        this.setServiceName(rs.getString("service_name"));
        this.setServiceType(rs.getString("service_type"));
        this.setCurPbVersion(rs.getString("cur_pb_version"));
        this.setTargetPbVersion(rs.getString("target_pb_version"));
        this.setTargetPbEnumVersion(rs.getString("target_pb_enum_version"));
        this.setTargetPbCommonVersion(rs.getString("target_pb_common_version"));
        this.setPreVersion(rs.getString("pre_version"));
        this.setCrLinks(rs.getString("cr_links"));
        this.setPiplineLinks(rs.getString("pipline_links"));
        this.setOperator(rs.getString("operator"));
        this.setCreateTime(rs.getLong("create_time"));
        this.setUpdateTime(rs.getLong("update_time"));
        return this;
    }
}
