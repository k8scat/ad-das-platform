package com.kuaishou.ad.das.platform.dal.repository;

import java.util.Collection;
import java.util.List;

import com.kuaishou.ad.das.platform.dal.model.AdDasIncrementTableDetail;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-06-09
 */
public interface AdDasIncrementTableDetailRepository {

    int insert(AdDasIncrementTableDetail adDasIncrementTableDetail);

    void deleteByIncreId(Long increId);

    void deleteByIncreIdAndTables(Long increId, Collection<String> tableNames);

    List<AdDasIncrementTableDetail> queryByCondition();

    List<AdDasIncrementTableDetail> queryByIncreId(Collection<Long> increId);

    AdDasIncrementTableDetail queryByIncreIdAndTable(Long increId, String mainTable);

    List<AdDasIncrementTableDetail> queryByIncreIdAndTables(Long increId, List<String> mainTable);

    List<AdDasIncrementTableDetail> queryByTableName(String tableName);
}
