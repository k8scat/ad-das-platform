package com.kuaishou.ad.das.platform.dal.repository.impl;

import java.util.List;

import org.springframework.context.annotation.Lazy;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.kuaishou.ad.das.dal.datasource.AdCoreDataSource;
import com.kuaishou.ad.das.platform.dal.model.AdDasTableColumn;
import com.kuaishou.ad.das.platform.dal.repository.AdDasTableColumnRepository;
import com.kuaishou.ad.das.platform.dal.repository.common.AdDasAbstractRepository;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-11-30
 */
@Lazy
@Repository
public class AdDasTableColumnRepositoryImpl extends AdDasAbstractRepository<AdDasTableColumn>
        implements AdDasTableColumnRepository {

    @Override
    protected RowMapper<AdDasTableColumn> getRowMapper() {
        return ((rs, rowNum) -> {
            AdDasTableColumn adDasTableColumn = new AdDasTableColumn();
            adDasTableColumn.parseResult(rs);

            return adDasTableColumn;
        });
    }

    @Override
    protected JdbcTemplate getJdbcTemplate(String dataSource) {
        return AdCoreDataSource.valueOf(dataSource).read().getJdbcTemplate();
    }

    @Override
    public List<AdDasTableColumn> queryTableColumnAll(String dataSource, String mainTable) {

        String selectSql = String.format("show full fields from %s",mainTable);

        return getJdbcTemplate(dataSource).query(selectSql, getRowMapper());
    }

}
