package com.kuaishou.ad.das.platform.dal.model;

import lombok.Data;

/**
 * @author Zhang Jialin <zhangjialin@kuaishou.com>
 * Created on 2021-06-14
 */
@Data
public class AdDasDownstreamUserRelationBind {
    /**
     * 基准id
     */
    private Long id;
    /**
     * 数据流组id
     */
    private Long dataStreamGroupId;
    /**
     * 数据流id
     */
    private Long dataStreamId;
    /**
     * 数据流类型:0-基准；1-增量
     */
    private Integer dataStreamType;
    /**
     * 用户组id
     */
    private Long userGroupId;
    /**
     * 是否删除
     */
    private boolean isDelete;
    /**
     * 创建时间
     */
    private Long createTime;
    /**
     * 更新时间
     */
    private Long updateTime;
}
