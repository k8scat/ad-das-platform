package com.kuaishou.ad.das.platform.dal.repository.impl;

import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import com.kuaishou.ad.das.dal.datasource.AdCoreDataSource;
import com.kuaishou.ad.das.platform.dal.dao.AdDasUserGroupCondition;
import com.kuaishou.ad.das.platform.dal.model.AdDasDownstreamUserInfo;
import com.kuaishou.ad.das.platform.dal.repository.AdDasDownstreamUserInfoRepository;

/**
 * @author Zhang Jialin <zhangjialin@kuaishou.com>
 * Created on 2021-06-14
 */
@Repository
public class AdDasDownstreamUserInfoRepositoryImpl implements AdDasDownstreamUserInfoRepository {

    @Override
    public Long insert(AdDasDownstreamUserInfo param) {
        param.setIsDelete(0);
        param.setCreateTime(System.currentTimeMillis());
        param.setUpdateTime(System.currentTimeMillis());
        SimpleJdbcInsert insert = new SimpleJdbcInsert(AdCoreDataSource.adDas.write().getJdbcTemplate())
                .withTableName(TABLE_NAME).usingGeneratedKeyColumns("id");
        return insert.executeAndReturnKey(new BeanPropertySqlParameterSource(param)).longValue();
    }

    @Override
    public void update(AdDasDownstreamUserInfo param) {
        param.setUpdateTime(System.currentTimeMillis());
        String sql = "UPDATE " + TABLE_NAME + " "
                + "SET user_group_name = :userGroupName, owner_email_prefix = :ownerEmailPrefix, "
                + "owner_name = :ownerName, update_time = :updateTime "
                + "WHERE id = :id";
        AdCoreDataSource.adDas.write().update(sql, new BeanPropertySqlParameterSource(param));
    }

    @Override
    public List<AdDasDownstreamUserInfo> query(AdDasUserGroupCondition condition) {
        StringBuilder sqlBuilder = new StringBuilder("SELECT * FROM " + TABLE_NAME + " ");
        MapSqlParameterSource paramSource = new MapSqlParameterSource();
        buildPredicate(condition, sqlBuilder, paramSource);
        return AdCoreDataSource.adDas.read().query(sqlBuilder.toString(), paramSource, new BeanPropertyRowMapper<>(AdDasDownstreamUserInfo.class));
    }

    private void buildPredicate(AdDasUserGroupCondition condition, StringBuilder sqlBuilder, MapSqlParameterSource paramSource) {
        sqlBuilder.append(" WHERE is_delete = :isDelete ");
        paramSource.addValue("isDelete", 0);
        if (CollectionUtils.isNotEmpty(condition.getUserGroupIds())) {
            sqlBuilder.append("AND id in (:userGroupIds)");
            paramSource.addValue("userGroupIds", condition.getUserGroupIds());
        }
        if (StringUtils.isNotEmpty(condition.getOwnerName())) {
            sqlBuilder.append("AND owner_name like %:ownerName%");
            paramSource.addValue("ownerName", condition.getOwnerName());
        }
        if (StringUtils.isNotEmpty(condition.getOwnerEmailPrefix())) {
            sqlBuilder.append("AND owner_email_prefix like %:ownerEmailPrefix%");
            paramSource.addValue("ownerEmailPrefix", condition.getOwnerEmailPrefix());
        }
        if (StringUtils.isNotEmpty(condition.getUserGroupName())) {
            sqlBuilder.append("AND user_group_name like %:userGroupName%");
            paramSource.addValue("userGroupName", condition.getUserGroupName());
        }
    }
}
