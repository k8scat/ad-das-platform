package com.kuaishou.ad.das.platform.dal.repository.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import com.kuaishou.ad.das.platform.dal.model.AdDasDownstreamUserRelationBind;
import com.kuaishou.infra.boot.jdbc.datasource.DataSourceRouting;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-06-22
 */
@Mapper
@DataSourceRouting("adDas")
public interface AdDasUserGroupBindMapper {

    @Select("SELECT * from ad_das_data_stream_user_group_relation ")
    List<AdDasDownstreamUserRelationBind> listAll();

    @Insert("insert into ad_das_data_stream_user_group_relation(data_stream_group_id,data_stream_id,data_stream_type,user_group_id," +
            "create_time,update_time,is_delete)" +
            " values(#{dataStreamGroupId},#{dataStreamId},#{dataStreamType},#{userGroupId},#{createTime},#{updateTime},#{isDelete}")
    Integer insert(AdDasDownstreamUserRelationBind adDasUserGroupBind);

    @Select("SELECT * from ad_das_data_stream_user_group_relation group_relation "
            + "left join ad_das_data_stream_group group_data on group_relation.user_group_id=group_data.id "
            + "where ")
    List<AdDasDownstreamUserRelationBind> listByCondition();

}
