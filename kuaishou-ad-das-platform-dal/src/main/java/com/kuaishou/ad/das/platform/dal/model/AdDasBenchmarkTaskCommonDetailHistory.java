package com.kuaishou.ad.das.platform.dal.model;

import lombok.Data;

/**
 * @author wangkai <wangkai14@kuaishou.com>
 * Created on 2021-01-10
 */
@Data
public class AdDasBenchmarkTaskCommonDetailHistory  extends AdDasBenchmarkTaskCommonDetail {

    private Long modifyId;
}
