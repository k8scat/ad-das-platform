package com.kuaishou.ad.das.platform.dal.model;

import java.sql.ResultSet;
import java.sql.SQLException;

import lombok.Data;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-11-30
 */
@Data
public class AdDasTableColumn extends AdDasAbstarctModel<AdDasTableColumn> {

    private String fieldName;

    private String type;

    @Override
    public AdDasTableColumn parseResult(ResultSet rs) throws SQLException {

        this.setFieldName(rs.getString("Field"));
        this.setType(rs.getString("Type"));
        return this;
    }
}
