package com.kuaishou.ad.das.platform.dal.repository.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Repository;

import com.kuaishou.ad.das.platform.dal.model.AdDasModifyIncrementTableDetail;
import com.kuaishou.ad.das.platform.dal.repository.AdDasModifyIncrementTableDetailRepository;
import com.kuaishou.ad.das.platform.dal.repository.mapper.AdDasModifyIncrementTableDetailMapper;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-06-14
 */
@Lazy
@Repository
public class AdDasModifyIncrementTableDetailRepositoryImpl implements AdDasModifyIncrementTableDetailRepository {

    @Autowired
    private AdDasModifyIncrementTableDetailMapper adDasModifyIncrementTableDetailMapper;

    @Override
    public int insert(AdDasModifyIncrementTableDetail adDasModifyIncrementTableDetail) {
        return adDasModifyIncrementTableDetailMapper.insert(adDasModifyIncrementTableDetail);
    }

    @Override
    public void update(AdDasModifyIncrementTableDetail adDasModifyIncrementTableDetail) {
        adDasModifyIncrementTableDetailMapper.update(adDasModifyIncrementTableDetail);
    }

    @Override
    public void deleteByModifyIdAndTable(Long modifyId, String increTable) {
        adDasModifyIncrementTableDetailMapper.deletByModifyIdAndIncreId(modifyId, increTable);
    }

    @Override
    public List<AdDasModifyIncrementTableDetail> queryByModifyId(Long modifyId) {
        return adDasModifyIncrementTableDetailMapper.listByModifyId(modifyId);
    }

    @Override
    public List<AdDasModifyIncrementTableDetail> queryByIncreId(Long increId) {
        return adDasModifyIncrementTableDetailMapper.listByIncreId(increId);
    }

    @Override
    public List<AdDasModifyIncrementTableDetail> searchTableWithModifyMsg(Long increId, String tableName) {
        return adDasModifyIncrementTableDetailMapper.searchTableWithModifyMsg(increId, tableName);
    }

    @Override
    public List<AdDasModifyIncrementTableDetail> queryByModifyIds(List<Long> modifyIds) {
        return adDasModifyIncrementTableDetailMapper.queryByModifyIds(modifyIds);
    }

    @Override
    public AdDasModifyIncrementTableDetail queryMainTableLast(Long increId, String mainTable) {
        return adDasModifyIncrementTableDetailMapper.queryMainTableLast(increId, mainTable);
    }

}
