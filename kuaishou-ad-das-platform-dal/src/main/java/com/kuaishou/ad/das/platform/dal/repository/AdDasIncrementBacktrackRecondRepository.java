package com.kuaishou.ad.das.platform.dal.repository;

import java.util.Collection;
import java.util.List;

import com.kuaishou.ad.das.platform.dal.model.AdDasIncrementBacktrackRecond;
import com.kuaishou.ad.das.platform.dal.model.AdDasIncrementBacktrackRecondDetail;

/**
 * @author caoyinhang <caoyinhang@kuaishou.com>
 * Created on 2021-07-06
 */
public interface AdDasIncrementBacktrackRecondRepository {

    int insert(AdDasIncrementBacktrackRecond adDasIncrementBacktrackRecond);

    List<AdDasIncrementBacktrackRecondDetail> queryByCondition(String incrementName, String dataSource,
            String consumerGroup, String topic,
            String operator, Integer status, String beginTime, String endTime, Integer limit,
            Integer offset);

    AdDasIncrementBacktrackRecond queryById(Long id);

    List<AdDasIncrementBacktrackRecond> queryAll();

    List<AdDasIncrementBacktrackRecond> queryByIncreId(Collection<Long> increId);

    AdDasIncrementBacktrackRecond queryOperatorByIncreId(Long increId);

    AdDasIncrementBacktrackRecond queryByOperator(String operator);

    Integer queryCountByCondition(String incrementName, String dataSource, String consumerGroup, String topic, String operator, Integer status, String beginTime, String endTime);
}
