package com.kuaishou.ad.das.platform.dal.repository.mapper;

import java.util.Collection;
import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.kuaishou.ad.das.platform.dal.model.AdDasBenchmark;
import com.kuaishou.infra.boot.jdbc.datasource.DataSourceRouting;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-06-21
 */
@Mapper
@DataSourceRouting("adDas")
public interface AdDasBenchmarkMapper {

    @Select("SELECT * from ad_das_benchmark "
            + " where id = #{id} ")
    AdDasBenchmark listById(@Param("id") Long id);

    @Select("SELECT * from ad_das_benchmark "
            + " where id>0 ")
    List<AdDasBenchmark> listAll();

    @Select("SELECT * from ad_das_benchmark where benchmark_name=#{benchmarkName}")
    AdDasBenchmark listByName(@Param("benchmarkName") String benchmarkName);

    @Select({"<script> " +
            " select * "+
            " from ad_das_benchmark \n" +
            " where 1=1 " +
            " <if test = \" benchmakrIds != null and benchmakrIds.size() != 0 \"> " +
            " and id in " +
            " <foreach item = 'item' index = 'index' collection = 'benchmakrIds' open = '(' separator = ',' close = ')' > " +
            " #{item} " +
            " </foreach> " +
            " </if> " +
            "</script>"})
    List<AdDasBenchmark> queryById(@Param("benchmakrIds") Collection<Long> benchmakrIds);

    @Insert("insert into ad_das_benchmark(benchmark_code,benchmark_name,benchmark_desc,cluster_name,data_stream_group_id,hdfs_path,operator_email_prefix," +
            "is_delete,create_time,update_time)" +
            " values(#{benchmarkCode},#{benchmarkName},#{benchmarkDesc},#{clusterName},#{dataStreamGroupId},#{hdfsPath},#{operatorEmailPrefix},#{isDelete}," +
            "#{createTime},#{updateTime})")
    Integer insert(AdDasBenchmark adDasBenchmark);

    @Update({"<script> " +
            " update ad_das_benchmark set update_time=#{updateTime},"+
            " <if test = \" benchmarkCode != null and benchmarkCode != '' \"> " +
            " benchmark_code=#{benchmarkCode}, " +
            " </if> " +
            " <if test = \" benchmarkName != null and benchmarkName != '' \"> " +
            " benchmark_name=#{benchmarkName}, " +
            " </if> " +
            " <if test = \" benchmarkDesc != null and benchmarkDesc != '' \"> " +
            " benchmark_desc=#{benchmarkDesc}, " +
            " </if> " +
            " <if test = \" clusterName != null and clusterName != '' \"> " +
            " cluster_name=#{clusterName}, " +
            " </if> " +
            " <if test = \" dataStreamGroupId != null and dataStreamGroupId != 0 \"> " +
            " data_stream_group_id=#{dataStreamGroupId}, " +
            " </if> " +
            " <if test = \" hdfsPath != null and hdfsPath != '' \"> " +
            " hdfs_path=#{hdfsPath}, " +
            " </if> " +
            " <if test = \" operatorEmailPrefix != null and operatorEmailPrefix != '' \"> " +
            " hdfs_path=#{operatorEmailPrefix}, " +
            " </if> " +
            " <if test = \" isDelete != null \"> " +
            " isDelete=#{isDelete} " +
            " </if> " +
            " where id=#{id}" +
            "</script>"})
    void update(AdDasBenchmark adDasBenchmark);

    @Delete("delete from ad_das_benchmark where id=#{dataId}")
    void deletById(@Param("dataId") Long dataId);

}
