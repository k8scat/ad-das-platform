package com.kuaishou.ad.das.platform.dal.repository.impl;

import java.util.List;

import org.springframework.context.annotation.Lazy;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.kuaishou.ad.das.dal.datasource.AdCoreDataSource;
import com.kuaishou.ad.das.platform.dal.model.AdDasTableSchema;
import com.kuaishou.ad.das.platform.dal.repository.AdDasTableSchemaRepository;
import com.kuaishou.ad.das.platform.dal.repository.common.AdDasAbstractRepository;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-11-30
 */
@Lazy
@Repository
public class AdDasTableSchemaRepositoryImpl extends AdDasAbstractRepository<AdDasTableSchema>
        implements AdDasTableSchemaRepository {

    @Override
    protected RowMapper<AdDasTableSchema> getRowMapper() {
        return ((rs, rowNum) -> {
            AdDasTableSchema adDasTableSchema = new AdDasTableSchema();
            adDasTableSchema.parseResult(rs);

            return adDasTableSchema;
        });
    }

    @Override
    protected JdbcTemplate getJdbcTemplate(String dataSource) {
        return AdCoreDataSource.valueOf(dataSource).read().getJdbcTemplate();
    }

    @Override
    public List<AdDasTableSchema> queryTablesAll(String dataSource, String dbName) {

        String selectSql = "select table_name, create_time, table_comment from information_schema.tables where table_schema=?";
        return getJdbcTemplate(dataSource).query(selectSql, getRowMapper(), dbName);
    }

}
