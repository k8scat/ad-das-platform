package com.kuaishou.ad.das.platform.dal.repository;

import java.util.List;

import com.kuaishou.ad.das.platform.dal.model.AdDasBenchmarkTaskEditTestRunRecord;

/**
 * @author wangkai <wangkai14@kuaishou.com>
 * Created on 2021-01-04
 */
public interface AdDasBenchmarkTaskEditTestRunRecordRepository {

    void insert(AdDasBenchmarkTaskEditTestRunRecord adDasBenchmarkTaskEditTestRunRecord);

    List<AdDasBenchmarkTaskEditTestRunRecord> getAll();

    void update(Integer taskRunStatus, Long taskRecordId, String operator, Long updateTime);
}
