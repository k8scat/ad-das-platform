#!/bin/sh -e

echo "Start create incre code!!!"
increDir="./src/main/java/com/kuaishou/ad/das/platform/increment/resolver/"

if [ -d "$increDir" ];
then
cd $increDir
ls -al
else
echo "增量流目录不存在!"
exit 0
fi

increCode=${streamCode}
increType=${increStreamType}

# 文件名称
increName="AdDasPlatformBench"$increCode${increType}"Client"
increFileData=""

# 文件内容
if [ "${increType}" = "trans" ];
then
  increName="AdDasPlatformBench"$increCode"TransClient"
  increFileData="package com.kuaishou.ad.das.platform.increment.resolver;\n\nimport org.springframework.context.annotation.Lazy;\nimport org.springframework.stereotype.Component;\n\nimport com.kuaishou.ad.das.platform.core.increment.resolver.AdDasPlatformIncrementTransactionAbstractResolver;\n\n@Lazy\n@Component\npublic class "$increName" extends AdDasPlatformIncrementTransactionAbstractResolver {\n\n}"
elif [ "${increType}" = "seque" ];
then
  increName="AdDasPlatformBench"$increCode"SequeClient"
  increFileData="package com.kuaishou.ad.das.platform.increment.resolver;\n\nimport org.springframework.context.annotation.Lazy;\nimport org.springframework.stereotype.Component;\n\nimport com.kuaishou.ad.das.platform.core.increment.resolver.AdDasPlatformIncrementSequenceAbstractResolver;\n\n@Lazy\n@Component\npublic class "$increName" extends AdDasPlatformIncrementSequenceAbstractResolver {\n\n}"
else
  echo "increType is not supported!!!"
  exit 0
fi

increfileName=$increName".java"

if [ ! -f "$increfileName" ];
then
echo $increFileData >> $increfileName
git add *
git commit -m "新增增量流入口类:"$increfileName
git push
else
echo "增量文件："$increfileName" 已存在！"
exit 0
fi
echo "Create incre code success!!!"