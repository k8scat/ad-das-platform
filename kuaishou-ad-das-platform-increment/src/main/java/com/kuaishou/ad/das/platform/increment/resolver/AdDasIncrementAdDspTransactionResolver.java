package com.kuaishou.ad.das.platform.increment.resolver;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import com.kuaishou.ad.das.platform.core.increment.resolver.AdDasPlatformIncrementTransactionAbstractResolver;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-07-07
 */
@Lazy
@Component
public class AdDasIncrementAdDspTransactionResolver extends AdDasPlatformIncrementTransactionAbstractResolver {

}