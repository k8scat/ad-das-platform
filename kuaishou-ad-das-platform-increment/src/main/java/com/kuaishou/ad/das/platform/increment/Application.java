package com.kuaishou.ad.das.platform.increment;

import org.mybatis.spring.annotation.MapperScan;

import com.kuaishou.infra.boot.KsSpringApplication;
import com.kuaishou.infra.boot.autoconfigure.KsBootApplication;

import lombok.extern.slf4j.Slf4j;


/**
 * 服务启动入口类
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-06-01
 */
@Slf4j
@KsBootApplication(scanBasePackages = "com.kuaishou.ad")
@MapperScan("com.kuaishou.ad.das.platform.dal.repository.mapper")
public class Application {


    public static void main(String[] args) throws ClassNotFoundException {
        String clientName = System.getenv("DAS_SERVICE_NAME");
        log.info("clientName={}", clientName);

        Class clientClass = Class.forName("com.kuaishou.ad.das.platform.increment.resolver." + clientName);
        KsSpringApplication application = KsSpringApplication.just();
        application.addService(clientClass);
        application.run(args);
    }
}
