# Kuaishou Ad Das Platform 

## 各模块说明
### kuaishou-ad-das-platform-api
开放平台
 
### kuaishou-ad-das-platform-runner
开放平台辅助runner入口

### kuaishou-ad-das-platform-component
开放平台业务逻辑组件

### kuaishou-ad-das-platform-benchmark
基准服务入口

### kuaishou-ad-das-platform-increment
增量服务入口

### kuaishou-ad-das-platform-core
基准服务和增量服务核心逻辑组件

### kuaishou-ad-das-platform-dal
平台数据库dal层

### kuaishou-ad-das-platform-utils
平台公用工具组件

### kuaishou-ad-das-platform-sdk
平台sdk模块






