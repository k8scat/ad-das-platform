package com.kuaishou.ad.das.platform.benchmark.client;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import com.kuaishou.ad.das.platform.core.benchmark.client.AdDasPlatformBenchmarkAbstractClient;

import lombok.extern.slf4j.Slf4j;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-07-04
 */
@Lazy
@Slf4j
@Service
public class AdDasPlatformBenchmarkCoreClient extends AdDasPlatformBenchmarkAbstractClient {

}
