#!/bin/sh -e

echo "Start create client code!!!"

ls -al

clientDir="./src/main/java/com/kuaishou/ad/das/platform/benchmark/client/"

if [ -d "$clientDir" ];
then
cd $clientDir
ls -al
else
echo "基准流目录不存在!"
exit 0
fi

clientCode=${streamCode}
# 文件名称
clientName="AdDasPlatformBench"$clientCode"Client"
# 文件内容
clientFileData="package com.kuaishou.ad.das.platform.benchmark.client;\n\nimport org.springframework.context.annotation.Lazy;\nimport org.springframework.stereotype.Service;\n\nimport com.kuaishou.ad.das.platform.core.benchmark.client.AdDasPlatformBenchmarkAbstractClient;\n\nimport lombok.extern.slf4j.Slf4j;\n\n@Lazy\n@Slf4j\n@Service\npublic class "$clientName" extends AdDasPlatformBenchmarkAbstractClient {\n\n} "
clientfileName=$clientName".java"

if [ ! -f "$clientfileName" ];
then
echo $clientFileData >> $clientfileName
git add *
git commit -m "新增基准流入口类:"$clientfileName
git push
else
echo "基准文件："$clientfileName" 已存在！"
exit 0
fi

echo "Create client code success!!!"