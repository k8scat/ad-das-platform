package com.kuaishou.ad.das.platform.sal.git;

import static com.kuaishou.ad.das.platform.utils.kconf.AdDasPlatformKconf.adDasPlatformGitPipelineId;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.google.common.collect.Maps;
import com.kuaishou.ad.das.platform.sal.model.AdDasPlatformGitParam;
import com.kuaishou.ad.das.platform.sal.model.AdDasPlatformGitPipelineRecordModel;
import com.kuaishou.ad.das.platform.sal.model.AdDasPlatformGitTriggerModel;
import com.kuaishou.ad.das.platform.utils.AdDasHttpClientUtils;
import com.kuaishou.framework.util.HostInfo;
import com.kuaishou.framework.util.ObjectMapperUtils;

import lombok.extern.slf4j.Slf4j;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-08-09
 */
@Slf4j
@Service
public class AdDasPlatformGitSAO {

    private static final String KDEV_URL = "http://kdev-api.internal";
    private static final String TRIGGER_PIPELINE = "/kdev/internal/pipeline/triggerPipeline";
    private static final String GET_PIPELINE = "/kdev/internal/pipeline/log/get?id=%s";

    private static final  Map<String, String> headers = new HashMap<String, String>(){
        {
            put("Content-type", "application/json;charset=UTF-8");
            put("Authentication", "adDas eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJhZERhcyIsImlhdCI6MTYyNzk4OTU5NywiZXhwIjo0NzgxNTg5NTk3fQ.TSTsO3mjgY2X8YWsgl_0Zrey8OejCxlJCRLdnwizf48");
        }
    };

    /**
     * 触发生成 流入口类代码
     * @param streamType  数据流类型：client、increment
     * @param streamCode
     * @param increStreamType  增量流类型：trans、seque
     * @param userName
     * @return
     */
    public AdDasPlatformGitTriggerModel generateStreamCode(String streamType, String streamCode, String increStreamType, String userName) {

        String url = KDEV_URL + TRIGGER_PIPELINE;
        AdDasPlatformGitParam adDasPlatformGitModel = new AdDasPlatformGitParam();
        adDasPlatformGitModel.setPipelineId(adDasPlatformGitPipelineId.get());
        adDasPlatformGitModel.setUsername(userName);
        adDasPlatformGitModel.setTriggerData(Maps.newHashMap());
        Map<String, Object> envData = Maps.newHashMap();
        envData.put("streamType", streamType);
        envData.put("streamCode", streamCode);
        if ("increment".equals(streamType) && !StringUtils.isEmpty(increStreamType)) {
            envData.put("increStreamType", increStreamType);
        }
        adDasPlatformGitModel.setEnvData(envData);
        String json = ObjectMapperUtils.toJSON(adDasPlatformGitModel);
        String responseData = AdDasHttpClientUtils.doPostWithJson(url, headers, json);
        if (HostInfo.debugHost()) {
            log.info("url={}, responseData={}", url, responseData);
        }
        Map<String, Object> responseDataMap = ObjectMapperUtils.fromJSON(responseData, Map.class);
        Integer status = (Integer) responseDataMap.getOrDefault("status", 0);
        if (status != 200) {
            return null;
        }
        AdDasPlatformGitTriggerModel platformGitTriggerModel = new AdDasPlatformGitTriggerModel();
        platformGitTriggerModel.setTimestamp((Long) responseDataMap.get("timestamp"));
        Map<String, Object> dataMap = (Map<String, Object>) responseDataMap.get("data");
        platformGitTriggerModel.setId((Long) dataMap.get("id"));
        platformGitTriggerModel.setPipelineId((Long) dataMap.get("pipelineId"));
        platformGitTriggerModel.setCreator((String) dataMap.get("creator"));
        platformGitTriggerModel.setUrl((String) dataMap.get("url"));

        return platformGitTriggerModel;
    }

    /**
     * 查询 git pipeline 状态信息
     * @return
     * @throws Exception
     */
    public AdDasPlatformGitPipelineRecordModel getPipelineRecord(AdDasPlatformGitTriggerModel adDasPlatformGitTriggerModel) throws Exception {

        String url = KDEV_URL + GET_PIPELINE;
        Map<String, String> params = Maps.newHashMap();
        String responseData = AdDasHttpClientUtils.doGet(String.format(url, adDasPlatformGitTriggerModel.getId()), headers, params);
        if (HostInfo.debugHost()) {
            log.info("url={}, responseData={}", url, responseData);
        }
        if (StringUtils.isEmpty(responseData)) {
            return null;
        }
        Map<String, Object> responseDataMap = ObjectMapperUtils.fromJSON(responseData, Map.class);
        Integer status = (Integer) responseDataMap.getOrDefault("status", 0);
        if (status != 200) {
            return null;
        }

        AdDasPlatformGitPipelineRecordModel pipelineRecordModel = new AdDasPlatformGitPipelineRecordModel();

        Map<String, Object> dataMap = (Map<String, Object>) responseDataMap.get("data");
        pipelineRecordModel.setId((Long) dataMap.get("id"));
        pipelineRecordModel.setPipelineId((Long) dataMap.get("pipelineId"));
        pipelineRecordModel.setPipelineName((String) dataMap.get("pipelineName"));
        pipelineRecordModel.setStatus((Integer) dataMap.get("status"));
        pipelineRecordModel.setStatusDesc((String) dataMap.get("statusDesc"));
        pipelineRecordModel.setTriggerType((Integer) dataMap.get("triggerType"));
        pipelineRecordModel.setTriggerTypeDesc((String) dataMap.get("triggerTypeDesc"));

        return pipelineRecordModel;
    }
}
