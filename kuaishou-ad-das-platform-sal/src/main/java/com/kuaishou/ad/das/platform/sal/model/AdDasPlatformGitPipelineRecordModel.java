package com.kuaishou.ad.das.platform.sal.model;

import java.io.Serializable;

import lombok.Data;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-08-13
 */
@Data
public class AdDasPlatformGitPipelineRecordModel implements Serializable {

    private static final long serialVersionUID = -2132186358262633771L;

    private Long id;

    private Long pipelineId;

    private String pipelineName;

    private Integer triggerType;

    private String triggerTypeDesc;

    private Integer status;

    private String statusDesc;
}
