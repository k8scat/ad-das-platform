package com.kuaishou.ad.das.platform.sal.model;

import java.io.Serializable;
import java.util.Map;

import lombok.Data;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-08-12
 */
@Data
public class AdDasPlatformGitParam implements Serializable {

    /**
     * 流水线ID
     */
    private Long pipelineId;

    /**
     * 构建人
     */
    private String username;

    /**
     * 其他可选触发参数， 研发云相关参数
     */
    private Map<String, Object> triggerData;

    /**
     * 自定义环境变量-会以环境变量参数形式传递到研发云（value只能是字符串或数字）
     */
    private Map<String, Object> envData;
}
