package com.kuaishou.ad.das.platform.sal.model;

import java.io.Serializable;

import lombok.Data;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-08-13
 */
@Data
public class AdDasPlatformGitTriggerModel implements Serializable {

    private static final long serialVersionUID = -1556013641609126218L;

    private Long id;

    private Long pipelineId;

    private String creator;

    private String url;

    private Long timestamp;
}
