package com.kuaishou.ad.das.platform.sal.kbus;

import java.io.IOException;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.google.common.collect.Maps;
import com.kuaishou.ad.das.platform.utils.AdDasHttpUtils;

import groovy.util.logging.Slf4j;

/**
 * @author caoyinhang <caoyinhang@kuaishou.com>
 * Created on 2021-07-08
 */
@Slf4j
@Service
public class AdDasIncrementBacktrackSAO {

    /**
     * 调用kus回溯接口
     * @param url
     * @param productDef
     * @param dataSource
     * @param tenant
     * @param consumerGroup
     * @param topicType
     * @param region
     * @param durationSecond
     * @return
     * @throws IOException
     */
    public String backtrack(String url, String productDef, String dataSource, String tenant, String consumerGroup,
            String topicType, String region, Long durationSecond) throws IOException {
        StringBuilder SB = new StringBuilder();
        SB.append(url).append("?").append("productDef").append("=").append(productDef)
                .append("&").append("dataSourceName").append("=").append(dataSource)
                .append("&").append("tenant").append("=").append(tenant)
                .append("&").append("consumerGroup").append("=").append(consumerGroup)
                .append("&").append("topicType").append("=").append(topicType)
                .append("&").append("region").append("=").append(region)
                .append("&").append("durationSecond").append("=").append(durationSecond);//拼写post的params
        Map<String, Object> body = Maps.newHashMap();
        Map<String, String> header = Maps.newHashMap();
        header.put("X-KBus-Token","EB0BB455306DA610D8165A846C8E8B4F");
        header.put("content-type","application/json;charset=utf-8");
        String result = AdDasHttpUtils.doPost(SB.toString(), header, body);
        return result;
    }

}
