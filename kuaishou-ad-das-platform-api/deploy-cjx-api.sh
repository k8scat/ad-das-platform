#!/bin/bash
TARGET_DIR="kuaishou-ad-das-platform-api"
HOME=(`pwd`)
TARGET_HOST="td-dz-rs19.yz"
if [ -n "$1" ]; then
        TARGET_HOST="$1"
fi
echo clean mode -U to ${TARGET_HOST}

#mvn spring-boot:run -Dcheckstyle.skip=true -Denforcer.skip=true
#cd ../
#mvn clean install -U -Dcheckstyle.skip=true -Denforcer.skip=true || exit 1
cd $HOME
mvn -DincludeScope=runtime clean package dependency:copy-dependencies -Dcheckstyle.skip=true -Denforcer.skip=true || exit 1

proxychains4 rsync -ztrlvC --delete target/kuaishou-ad-das-platform-api.jar \
      web_server@${TARGET_HOST}::files/chenjiaxiong/kuaishou-ad-das-platform-api || exit 1

echo "部署位置: /home/web_server/files/chenjiaxiong/${TARGET_DIR}"
echo "参考启动方式:nohup sh ~/files/chenjiaxiong/deploy-kuaishou-ad-das-api-cjx.sh &"