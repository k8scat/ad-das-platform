package com.kuaishou.ad.das.platform.api;

import static java.lang.System.currentTimeMillis;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.gson.Gson;
import com.google.protobuf.Descriptors;
import com.google.protobuf.Message;
import com.kuaishou.ad.das.platform.core.common.pbloader.AdDasClassLoader;
import com.kuaishou.ad.das.platform.utils.AdDasHttpUtils;
import com.kuaishou.framework.util.ObjectMapperUtils;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-06-21
 */
@SpringBootTest
public class AdDasClassLoaderTest {

    private static final String pbClassName = "com.kuaishou.ad.model.protobuf.tables.All";

    private static String jsonData = "{\n" +
            "\t\"status\": 200,\n" +
            "\t\"message\": \"success\",\n" +
            "\t\"data\": {\n" +
            "\t\t\"id\": 123,\n" +
            "\t\t\"pipelineId\": 123123,\n" +
            "\t\t\"creator\": \"wangleifeng\",\n" +
            "    \"url\": \"https://kdev.corp.kuaishou.com/xxxx\"\n" +
            "\t},\n" +
            "\t\"traceId\": \"ac120c095fffedc28902343800000186\",\n" +
            "\t\"timestamp\": 1610608067184\n" +
            "}";

    @Test
    public void runMyClassLoader() throws ClassNotFoundException, MalformedURLException {

        ClassLoader parent = getClass().getClassLoader();

        String url =
                "http://nexus.corp.kuaishou.com:88/nexus/content/groups/public/kuaishou/kuaishou-ad-new-biz-proto/1.0.79/kuaishou-ad-new-biz-proto-1.0.79.jar";
        AdDasClassLoader urlClassLoader = new AdDasClassLoader();
        URL jarUrl = new URL(url);
        urlClassLoader.addURLFile(jarUrl);
        Class cls = urlClassLoader.loadClass(pbClassName);

        cls.getName();
        System.out.println(cls.getName());
        cls.getSimpleName();
        System.out.println(cls.getClasses());
        Field[] fields = cls.getDeclaredFields();
        List<String> fieldStrs = Lists.newArrayList();

        for (Field field : fields) {
            fieldStrs.add(field.getName());
        }

        System.out.println(fieldStrs);

    }

    @Test
    public void testFindPath() {

        String path = "/ad/das/cluster/assign";

        int index = path.indexOf("/assign");
        System.out.println("index=" + index);

        String prefixPath = path.substring(0, index);
        System.out.println("prefixPath=" + prefixPath);
    }

    @Test
    public void testEnumClassLoader()
            throws MalformedURLException, ClassNotFoundException, NoSuchMethodException, InvocationTargetException,
            IllegalAccessException {

        String enumClassStr = "com.kuaishou.ad.model.protobuf.AdEnumModel$AdEnum$AdInstanceType";

        AdDasClassLoader urlClassLoader = new AdDasClassLoader();

        String commonProto =
                "http://nexus.corp.kuaishou.com:88/nexus/content/groups/public/kuaishou/kuaishou-ad-new-common-proto/1.0.99/kuaishou-ad-new-common-proto-1.0.99.jar";
        URL jarUrlCommonProto = new URL(commonProto);
        urlClassLoader.addURLFile(jarUrlCommonProto);

        String url =
                "http://nexus.corp.kuaishou.com:88/nexus/content/groups/public/kuaishou/kuaishou-ad-reco-base-proto/1.0.99/kuaishou-ad-reco-base-proto-1.0.99.jar";
        URL jarUrl = new URL(url);
        //urlClassLoader.addURLFile(jarUrl);

        String urlProto =
                "http://nexus.corp.kuaishou.com:88/nexus/content/groups/public/kuaishou/kuaishou-ad-new-biz-proto/1.0.79/kuaishou-ad-new-biz-proto-1.0.79.jar";
        URL jarUrlProto = new URL(urlProto);
        //urlClassLoader.addURLFile(jarUrlProto);

        Class cls = urlClassLoader.loadClass(enumClassStr);

        cls.getName();
        System.out.println(cls.getName());

        Object[] deClassList = cls.getEnumConstants();
/*

        for (Object deClass : deClassList) {
            Class enumClass = deClass.getClass();
            System.out.println(enumClass.getName());
            System.out.println("====================");
            System.out.println(deClass);
            System.out.println("====================");
            System.out.println(enumClass.isEnum());
        }


        Class clsProto = urlClassLoader.loadClass(pbClassName);

        System.out.println(clsProto.getName());*/

        String AdInstanceFieldStr = "com.kuaishou.ad.model.protobuf.AdIncModel";
        Class clsInstanceFieldBuilder = urlClassLoader.loadClass(AdInstanceFieldStr);
        System.out.println(clsInstanceFieldBuilder.getName());
        Class[] classes = clsInstanceFieldBuilder.getClasses();
        Class fieldClass = null;
        for (Class clsData : classes) {

            if (clsData.getName().endsWith("AdInstanceField")) {
                fieldClass = clsData;
            }
            System.out.println(clsData.getName());
        }

        Method method = fieldClass.getMethod("newBuilder");
        Object obj = method.invoke(null, null);
        Message.Builder msgBuilder = (Message.Builder) obj;       // 得到 builder
        Descriptors.Descriptor descriptor = msgBuilder.getDescriptorForType();   // 得到 descript
        System.out.println(descriptor.getName());


        /*String adInstanceBuilder = "com.kuaishou.ad.model.protobuf.AdIncModel$AdInstance";
        Class clsBuilder = urlClassLoader.loadClass(adInstanceBuilder);
        System.out.println(clsBuilder.getName());

        Method method = clsBuilder.getMethod("newBuilder");
        Object obj = method.invoke(null, null);
        Message.Builder msgBuilder = (Message.Builder) obj;       // 得到 builder
        Descriptors.Descriptor descriptor = msgBuilder.getDescriptorForType();   // 得到 descript
        System.out.println(descriptor.getName());

        setBuilderField(msgBuilder, descriptor, "create_time", System.currentTimeMillis());

        System.out.println(msgBuilder);
*/


    }

    public static void setBuilderField(Message.Builder msgBuilder, Descriptors.Descriptor descriptor,
            String fieldKey, Object value) {
        Descriptors.FieldDescriptor fieldDescriptorType = descriptor.findFieldByName(fieldKey);
        Descriptors.FieldDescriptor.JavaType type = fieldDescriptorType.getJavaType();

        msgBuilder.setField(fieldDescriptorType, value);
    }


    @Test
    public void run() {
        long time = currentTimeMillis();
        System.out.println(time);
        //        System.out.println((time-1626937522777l)%(1000 * 24 * 60 * 60)%(1000 * 60 * 60 )%(1000 * 60 )/1000);
        System.out.println((time - 1626937522777l) / 1000);
        SimpleDateFormat sdf = new SimpleDateFormat("YYYY/MM/dd HH:mm:ss");
        String format = sdf.format(time);
        System.out.println(format);


    }

    @Test
    public void jsonDataTest() {
        Map<String, Object> responseDataMap = ObjectMapperUtils.fromJSON(jsonData, Map.class);
        System.out.println("responseDataMap=" + responseDataMap);
        System.out.println("data=" + responseDataMap.get("data").getClass());

    }

    @Test
    public void run1() {
        String url = "https://kbus.corp.kuaishou.com/databus/open/consumer/resetOffset";
        StringBuilder SB = new StringBuilder();
        SB.append(url).append("?").append("productDef").append("=").append("KUAISHOU")
                .append("&").append("dataSourceName").append("=").append("adDspTest")
                .append("&").append("tenant").append("=").append("default")
                .append("&").append("consumerGroup").append("=").append("ad-das-kbus-dsp-noaccountid-resolver-consume-test")
                .append("&").append("topicType").append("=").append("SEQUENCE")
                .append("&").append("region").append("=").append("HB1")
                .append("&").append("durationSecond").append("=").append(100);//拼写post的params
        Map<String, Object> body = Maps.newHashMap();
        Map<String, String> header = Maps.newHashMap();
        header.put("X-KBus-Token", "EB0BB455306DA610D8165A846C8E8B4F");
        header.put("content-type", "application/json;charset=utf-8");
        try {
            String result = AdDasHttpUtils.doPost(SB.toString(), header, body);

            Map<String,Object> resultMap = Maps.newHashMap();
            Gson gson = new Gson();
            Map map = gson.fromJson(result, resultMap.getClass());
            System.out.println(map);
            System.out.println(map.get("status"));
            System.out.println(map.get("message"));
            System.out.println(map.get("status").equals(200.0));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void run2(){

        ExecutorService exec = Executors.newFixedThreadPool(1);
        Callable<String> call = new Callable<String>() {
            public String call() throws Exception {
                run1();
                return null;
            }
        };
        try {
            Future<String> future = exec.submit(call);
            String obj = future.get(1000 * 2, TimeUnit.MILLISECONDS); //任务处理超时时间设为 1 秒
            System.out.println("任务成功返回:" + obj);
        } catch (TimeoutException ex) {
            System.out.println("处理超时啦....");
        } catch (Exception e) {
            System.out.println("处理失败.");
        }
        // 关闭线程池
        exec.shutdown();
    }
}
