package com.kuaishou.ad.das.platform.api.config;

import java.util.Arrays;
import java.util.List;

import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.kuaishou.ad.das.platform.api.common.interceptor.AdDasAuthTokenInterceptor;
import com.kuaishou.ad.das.platform.api.common.resolver.AdDasRoleResolver;
import com.kuaishou.framework.web.resolver.ParamBundleAnnotationResolver;
import com.kuaishou.framework.web.resolver.UuidAnnotationResolver;
import com.kuaishou.framework.web.resolver.VisitorAnnotationResolver;
import com.kuaishou.interceptor.ThreadNamePatternInterceptor;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-09-26
 */
@Configuration
public class WebMvcConfig implements WebMvcConfigurer {


    @Bean
    public ThreadNamePatternInterceptor threadNamePatternInterceptor() {
        return new ThreadNamePatternInterceptor();
    }

    @Bean
    public AdDasAuthTokenInterceptor adDasAuthTokenInterceptor() {
        return new AdDasAuthTokenInterceptor();
    }

    @Bean
    @ConditionalOnMissingBean
    public ByteArrayHttpMessageConverter byteArrayHttpMessageConverter() {
        return new ByteArrayHttpMessageConverter();
    }

    @Bean
    @ConditionalOnMissingBean
    public StringHttpMessageConverter stringHttpMessageConverter() {
        return new StringHttpMessageConverter();
    }

    @Bean
    public VisitorAnnotationResolver visitorAnnotationResolver() {
        return new VisitorAnnotationResolver();
    }

    @Bean
    public ParamBundleAnnotationResolver paramBundleAnnotationResolver() {
        return new ParamBundleAnnotationResolver();
    }

    @Bean
    public UuidAnnotationResolver uuidAnnotationResolver() {
        return new UuidAnnotationResolver();
    }

    @Bean
    public AdDasRoleResolver adDasRoleResolver() {
        return new AdDasRoleResolver();
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        String[] addPathPatterns = { "/**" };
        registry.addInterceptor(threadNamePatternInterceptor()).addPathPatterns(addPathPatterns);

        String[] authPathPatterns = { "/dasOuter/api/**" };
        registry.addInterceptor(adDasAuthTokenInterceptor()).addPathPatterns(authPathPatterns);
    }

    @Override
    public void addArgumentResolvers(List<HandlerMethodArgumentResolver> resolvers) {
        resolvers.addAll(Arrays.asList(
                visitorAnnotationResolver(),
                paramBundleAnnotationResolver(),
                uuidAnnotationResolver(),
                adDasRoleResolver()
        ));
    }

    @Override
    public void extendMessageConverters(List<HttpMessageConverter<?>> converters) {
        converters.clear();
        converters.addAll(Arrays.asList(
                byteArrayHttpMessageConverter(),
                stringHttpMessageConverter()
        ));
    }


    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("swagger-ui.html").addResourceLocations("classpath:/META-INF/resources/");
        registry.addResourceHandler("/webjars/**").addResourceLocations("classpath:/META-INF/resources/webjars/");
    }
}
