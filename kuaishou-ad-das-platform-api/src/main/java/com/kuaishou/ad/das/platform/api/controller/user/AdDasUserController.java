package com.kuaishou.ad.das.platform.api.controller.user;

import static com.kuaishou.ad.das.platform.utils.constant.AdDasApiConstant.DAS_API_PATH;
import static com.kuaishou.ad.das.platform.utils.kconf.AdDasPlatformKconf.adDasPlatformUserMsgs;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.kuaishou.ad.das.platform.component.web.ResponseObject;
import com.kuaishou.ad.das.platform.utils.annotation.AdDasRole;
import com.kuaishou.ad.das.platform.utils.model.AdDasUserView;
import com.kuaishou.framework.web.annotation.ThreadNamePattern;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

/**
 * das用户相关接口
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-09-26
 */
@Api(value = "das用户相关接口")
@Slf4j
@RequestMapping(DAS_API_PATH + "/user")
@RestController
public class AdDasUserController {
    @ApiOperation(value = "用户信息接口，会返回用户的角色，供前端页面权限控制")
    @ThreadNamePattern
    @GetMapping(value = "/queryUserMsg")
    public ResponseObject<AdDasUserView> queryUserMsg(@AdDasRole AdDasUserView dasUserView) {
        ResponseObject<AdDasUserView> result = ResponseObject.ok();
        log.info("queryUserMsg() dasUserView={}", dasUserView);
        // 默认没权限
        Integer roleType = 0;
        if (adDasPlatformUserMsgs.get().containsKey(dasUserView.getUserName())) {
            roleType = adDasPlatformUserMsgs.get().get(dasUserView.getUserName());
        }
        dasUserView.setRoleType(roleType);
        result.setData(dasUserView);
        return result;
    }

}
