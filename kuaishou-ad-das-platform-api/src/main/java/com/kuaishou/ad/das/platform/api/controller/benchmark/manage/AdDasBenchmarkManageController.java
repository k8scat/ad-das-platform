package com.kuaishou.ad.das.platform.api.controller.benchmark.manage;

import static com.kuaishou.ad.das.platform.utils.constant.AdDasApiConstant.DAS_API_BENCHMARK_PATH;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.kuaishou.ad.das.platform.component.biz.benchmark.AdDasBenchmarkManageBiz;
import com.kuaishou.ad.das.platform.component.web.ResponseObject;
import com.kuaishou.ad.das.platform.component.web.param.AdDasBenchmarkManageHomeParam;
import com.kuaishou.ad.das.platform.component.web.param.benchmark.BenchmarkMainTableQueryItemParam;
import com.kuaishou.ad.das.platform.component.web.param.benchmark.BenchmarkQueryParam;
import com.kuaishou.ad.das.platform.component.web.view.AdDasBenchmarkServiceStatusView;
import com.kuaishou.ad.das.platform.component.web.view.AdDasBenchmarkTaskVisualizeView;
import com.kuaishou.ad.das.platform.component.web.view.AdDasCommonDataView;
import com.kuaishou.ad.das.platform.component.web.view.benchmark.manage.AdDasBenchmarkManageBaseInfoView;
import com.kuaishou.ad.das.platform.component.web.view.benchmark.manage.AdDasBenchmarkManageHomeView;
import com.kuaishou.ad.das.platform.component.web.view.benchmark.manage.BenchmarkMainTableQueryItemView;
import com.kuaishou.ad.das.platform.component.web.view.benchmark.manage.BenchmarkQueryView;
import com.kuaishou.framework.web.annotation.ThreadNamePattern;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;

/**
 * @author wangkai <wangkai14@kuaishou.com>
 * Created on 2020-12-28
 */
@Slf4j
@RequestMapping(DAS_API_BENCHMARK_PATH + "/manage")
@RestController
@Api(value = "基准管理首页接口")
public class AdDasBenchmarkManageController {
    @Autowired
    private AdDasBenchmarkManageBiz adDasBenchmarkManageBiz;

    @ApiOperation(value = "基准管理首页接口", notes = "展示当前基准服务正式任务信息")
    @ThreadNamePattern
    @PostMapping(value = "/manageHomePage")
    public ResponseObject<AdDasBenchmarkManageHomeView> manageHomePage(@ApiParam @RequestBody AdDasBenchmarkManageHomeParam param) {
        ResponseObject<AdDasBenchmarkManageHomeView> result = ResponseObject.ok();
        AdDasBenchmarkManageHomeView adDasBenchmarkManageHomeView = adDasBenchmarkManageBiz.queryBenchmarkTaskList(param);
        result.setData(adDasBenchmarkManageHomeView);
        return result;
    }

    @ApiOperation(value = "查询基准元数据信息", notes = "查询基准元数据信息")
    @ThreadNamePattern
    @GetMapping(value = "/queryBenchmarkBaseInfo")
    public ResponseObject<AdDasBenchmarkManageBaseInfoView> queryBenchmarkBaseInfo() {
        ResponseObject<AdDasBenchmarkManageBaseInfoView> result = ResponseObject.ok();
        AdDasBenchmarkManageBaseInfoView adDasBenchmarkManageBaseInfoView = adDasBenchmarkManageBiz.queryBenchmarkBaseInfo();
        result.setData(adDasBenchmarkManageBaseInfoView);
        return result;
    }

    @ApiOperation(value = "批量删除基准任务接口", notes = "批量删除基准任务接口")
    @ThreadNamePattern
    @PostMapping(value = "/deleteTask")
    public ResponseObject<Object> deleteTask(@RequestParam(value = "taskIds") String taskIds) {
        ResponseObject<Object> result = ResponseObject.ok();
        if (StringUtils.isEmpty(taskIds)) {
            log.error("deleteTask()  taskIds={} is empty!", taskIds);
            return result;
        }
        String[] taskIdStrArr = taskIds.split(",");
        List<Long> taskIdList = Arrays.stream(taskIdStrArr)
                .filter(taskIdStr -> !StringUtils.isEmpty(taskIdStr))
                .map(taskIdStr -> Long.valueOf(taskIds))
                .collect(Collectors.toList());

        if (CollectionUtils.isEmpty(taskIdList)) {
            log.error("deleteTask()  taskIdList={} is empty!", taskIdList);
            return result;
        }
        adDasBenchmarkManageBiz.deleteTask(taskIdList);
        return result;
    }

    @ApiOperation(value = "查询基准信息列表", notes = "pageSize=-1时不分页")
    @PostMapping(value = "/query/list")
    public ResponseObject<BenchmarkQueryView> queryList(@ApiParam @RequestBody BenchmarkQueryParam param) {
        BenchmarkQueryView benchmarkQueryView = adDasBenchmarkManageBiz.queryBenchmarkList(param);
        return ResponseObject.ok(benchmarkQueryView);
    }

    @ApiOperation(value = "查询基准主表详情", notes = "包含PB映射等大字段")
    @PostMapping(value = "/mainTable/query/item")
    public ResponseObject<BenchmarkMainTableQueryItemView> queryMainTableItem(
            @ApiParam @RequestBody BenchmarkMainTableQueryItemParam param) {
        BenchmarkMainTableQueryItemView itemView = adDasBenchmarkManageBiz.queryMainTableItem(param);
        return ResponseObject.ok(itemView);
    }

    @ApiOperation(value = "查询基准任务执行状态")
    @PostMapping(value = "/serviceStatus")
    public ResponseObject<AdDasBenchmarkServiceStatusView> serviceStatus(
            @RequestParam(value = "benchmarkId") Long benchmarkId) {
        ResponseObject<AdDasBenchmarkServiceStatusView> result = ResponseObject.ok();
        AdDasBenchmarkServiceStatusView serviceStatusView = adDasBenchmarkManageBiz.getServiceStatus(benchmarkId);
        result.setData(serviceStatusView);

        return result;
    }

    @ApiOperation(value = "查询基准任务执行状态")
    @PostMapping(value = "/benchmarkVisual")
    public ResponseObject<AdDasBenchmarkTaskVisualizeView> benchmarkVisual(
            @RequestParam(value = "benchmarkId") Long benchmarkId) {
        ResponseObject<AdDasBenchmarkTaskVisualizeView> result = ResponseObject.ok();
        AdDasBenchmarkTaskVisualizeView taskVisualizeView = adDasBenchmarkManageBiz.getTaskVisualize(benchmarkId);
        result.setData(taskVisualizeView);

        return result;
    }

    @ApiOperation(value = "查询增量所有基准流列表")
    @ThreadNamePattern
    @PostMapping(value = "/queryBenchmarkEnum")
    public ResponseObject<List<AdDasCommonDataView>> queryBenchmarkEnum() {
        ResponseObject<List<AdDasCommonDataView>> result = ResponseObject.ok();

        return result;
    }

}
