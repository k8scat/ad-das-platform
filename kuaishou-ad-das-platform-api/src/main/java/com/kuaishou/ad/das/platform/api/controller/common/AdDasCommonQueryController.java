package com.kuaishou.ad.das.platform.api.controller.common;

import static com.kuaishou.ad.das.platform.core.utils.AdDasBenchmarkUtils.buildPbClassName;
import static com.kuaishou.ad.das.platform.core.utils.AdDasBenchmarkUtils.getBenchmarkTypeLists;
import static com.kuaishou.ad.das.platform.dal.datasource.AdDataSourceUtil.getAllDataSource;
import static com.kuaishou.ad.das.platform.utils.constant.AdDasApiConstant.DAS_PLATFORM_API_PATH;
import static com.kuaishou.ad.das.platform.utils.kconf.AdDasPlatformKconf.adDasPlatformLoadClassFromCache;
import static com.kuaishou.ad.das.platform.utils.kconf.AdDasPlatformKconf.adDasPlatformUserMsgs;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.protobuf.Descriptors;
import com.google.protobuf.Message;
import com.kuaishou.ad.das.platform.component.biz.benchmark.AdDasBenchmarkManageBiz;
import com.kuaishou.ad.das.platform.component.biz.increment.AdDasIncrementModifyQueryBiz;
import com.kuaishou.ad.das.platform.component.biz.schema.AdDasSchemaQueryBiz;
import com.kuaishou.ad.das.platform.component.biz.usergroup.UserGroupBiz;
import com.kuaishou.ad.das.platform.component.web.ResponseObject;
import com.kuaishou.ad.das.platform.component.web.param.StreamUserGroupEditParam;
import com.kuaishou.ad.das.platform.component.web.view.AdDasCommonDataView;
import com.kuaishou.ad.das.platform.component.web.view.AdDasProtoMsgView;
import com.kuaishou.ad.das.platform.component.web.view.AdDasSchemaPbClassView;
import com.kuaishou.ad.das.platform.component.web.view.AdDasTableMsgView;
import com.kuaishou.ad.das.platform.component.web.view.benchmark.common.AdDasBenchmarkTypeView;
import com.kuaishou.ad.das.platform.core.common.pbloader.AdDasApiPbLoader;
import com.kuaishou.framework.web.annotation.ThreadNamePattern;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;

/**
 * das通用信息相关接口
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-09-26
 */
@Api(value = "das通用信息相关接口")
@Slf4j
@RequestMapping(DAS_PLATFORM_API_PATH + "/common")
@RestController
public class AdDasCommonQueryController {

    @Autowired
    private AdDasSchemaQueryBiz adDasSchemaQueryBiz;

    @Autowired
    private AdDasApiPbLoader adDasApiPbLoader;

    @Autowired
    private UserGroupBiz userGroupBiz;

    @Autowired
    private AdDasBenchmarkManageBiz benchmarkManageBiz;

    @Autowired
    private AdDasIncrementModifyQueryBiz dasIncrementModifyQueryBiz;

    @ApiOperation(value = "基准类型 菜单列表")
    @ThreadNamePattern
    @GetMapping(value = "/queryBenchmarkType")
    public ResponseObject<List<String>> queryBenchmarkType() {
        ResponseObject<List<String>> result = ResponseObject.ok();
        List<String> benchMarkTypes = getBenchmarkTypeLists();
        Set<String> dbPathSet = benchmarkManageBiz.getHdfsPath();

        for (String dbPath : dbPathSet) {
            benchMarkTypes.add("platform<" + dbPath);
        }
        result.setData(benchMarkTypes);
        return result;
    }

    @ApiOperation(value = "基准类型 菜单列表")
    @ThreadNamePattern
    @GetMapping(value = "/queryBenchmarkTypeInfo")
    public ResponseObject<List<AdDasBenchmarkTypeView>> queryBenchmarkTypeInfo() {
        ResponseObject<List<AdDasBenchmarkTypeView>>  result = ResponseObject.ok();
        Map<String, Long> benchmarkTypeMap = Maps.newHashMap();
        List<AdDasBenchmarkTypeView> adDasBenchmarkTypeViews = Lists.newArrayList();
        benchmarkTypeMap.forEach((key, value) -> {
            AdDasBenchmarkTypeView adDasBenchmarkTypeView = new AdDasBenchmarkTypeView();
            adDasBenchmarkTypeView.setBenchmarkName(key);
            adDasBenchmarkTypeView.setBenchmarkId(value);
            adDasBenchmarkTypeViews.add(adDasBenchmarkTypeView);
        });

        result.setData(adDasBenchmarkTypeViews);
        return result;
    }

    @ApiOperation(value = "查询 pb 信息")
    @ThreadNamePattern
    @GetMapping(value = "/queryProtoMsg")
    public ResponseObject<List<AdDasProtoMsgView>> queryProtoMsg(
            @RequestParam String protoFullName) {
        ResponseObject<List<AdDasProtoMsgView>>  result = ResponseObject.ok();
        List<AdDasProtoMsgView> protoMsgViews = Lists.newArrayList();
        // 根据dumpInfo 的数据 解析出 file 对应的 pb 名称
        String className = buildPbClassName(protoFullName);

        try {
            Class<?> cl = Class.forName(className);
            // 切换从 reload Cache 中加载 class
            if (adDasPlatformLoadClassFromCache.get()) {
                cl = adDasApiPbLoader.loadClassFromCache(className);
            }
            // 首先根据 pb message class 名称得到 Class 对象。（坑1：注意内部类连接符为$,如 package_name.class_name$inner_class_name,
            // 另外注意脚本启动的时候要注意对 $ 转义，否则 shell 会以为 $inner_class_name 为变量）
            Method method = cl.getMethod("newBuilder");

            Object obj = method.invoke(null, null);
            Message.Builder msgBuilder = (Message.Builder) obj;       // 得到 builder
            Descriptors.Descriptor descriptor = msgBuilder.getDescriptorForType();   // 得到 descript
            List<Descriptors.FieldDescriptor> fieldDescriptors = descriptor.getFields();

            for (Descriptors.FieldDescriptor fieldDescriptor : fieldDescriptors) {
                AdDasProtoMsgView protoMsgView = new AdDasProtoMsgView();
                protoMsgView.setColumnFullName(fieldDescriptor.getFullName());
                protoMsgView.setColumnName(fieldDescriptor.getName());
                protoMsgView.setColumnJavaType(fieldDescriptor.getJavaType().toString());
                protoMsgView.setColumnPbType(fieldDescriptor.getType().toString());
                if (Descriptors.FieldDescriptor.JavaType.ENUM.equals(fieldDescriptor.getJavaType())) {
                    protoMsgView.setColumnEnumType(fieldDescriptor.getEnumType().getFullName());
                }
                if (fieldDescriptor.getJavaType() == Descriptors.FieldDescriptor.JavaType.MESSAGE) {
                    protoMsgView.setDefaultValue("");
                } else {
                    protoMsgView.setDefaultValue(fieldDescriptor.getDefaultValue().toString());
                }
                protoMsgViews.add(protoMsgView);
            }
        } catch (Exception e) {
            log.error("queryProtoMsg() occur error! protoFullName={}", protoFullName, e);
        }
        result.setData(protoMsgViews);
        return result;
    }

    @ApiOperation(value = "查询基准数据源类型", notes = "查询基准数据源类型")
    @ThreadNamePattern
    @GetMapping(value = "/queryBenchmarkDataSource")
    public ResponseObject<List<String>> queryBenchmarkDataSource() {
        ResponseObject<List<String>> result = ResponseObject.ok();
        List<String> dataSources = getAllDataSource();
        result.setData(dataSources);
        return result;
    }

    @ApiOperation(value = "查询DB下的表数据", notes = "查询DB下的表数据")
    @ThreadNamePattern
    @GetMapping(value = "/queryMainTable")
    public ResponseObject<List<String>> queryMainTable(@RequestParam String dataSource) {
        ResponseObject<List<String>> result = ResponseObject.ok();
        List<String> mainTables = adDasSchemaQueryBiz.queryTableAll(dataSource);
        result.setData(mainTables);
        return result;
    }

    @ApiOperation(value = "查询table信息", notes = "查询table信息")
    @ThreadNamePattern
    @RequestMapping(value = "/queryTableSchema", method = RequestMethod.GET)
    public ResponseObject<List<AdDasTableMsgView>> queryTableMsg(
            @RequestParam String dataSource, @RequestParam String tableName) {
        ResponseObject<List<AdDasTableMsgView>>  result = ResponseObject.ok();
        List<AdDasTableMsgView> tableMsgViews = adDasSchemaQueryBiz.queryTableMsg(dataSource, tableName);
        result.setData(tableMsgViews);
        return result;
    }

    @ApiOperation(value = "查询PbClass列表接口", notes = "查询PbClass列表接口")
    @ThreadNamePattern
    @GetMapping(value = "/queryPbClass")
    public ResponseObject<List<String>> queryPbClass() {
        List<String> pbClassAll = adDasSchemaQueryBiz.queryPbAll();
        return ResponseObject.ok(pbClassAll);
    }

    @ApiOperation(value = "查询Pb类枚举接口", notes = "查询Pb类枚举接口")
    @ThreadNamePattern
    @RequestMapping(value = "/queryPbClassEnum", method = RequestMethod.GET)
    public ResponseObject<List<String>> queryPbClassEnum() {
        ResponseObject result = ResponseObject.ok();
        List<String> pbClassAll = adDasSchemaQueryBiz.queryPbEnum();

        result.setData(pbClassAll);
        return result;
    }

    @ApiOperation(value = "查询PbClassSchema信息", notes = "查询PbClassSchema信息")
    @ThreadNamePattern
    @GetMapping(value = "/queryPbSchema")
    public ResponseObject<List<AdDasSchemaPbClassView>> queryPbSchema(
            @RequestParam String pbClass, @RequestParam String pbVersion) {
        return ResponseObject.ok();
    }

    @ApiOperation(value = "查询基准PbClassSchema信息", notes = "查询基准PbClassSchema信息")
    @ThreadNamePattern
    @GetMapping(value = "/queryBenchmarkPbSchema")
    public ResponseObject<List<AdDasSchemaPbClassView>> queryBenchmarkPbSchema(@RequestParam String pbClass) {


        // 查询db 表中的 字段
        String pbVersion = adDasSchemaQueryBiz.queryLatestSchemaPb();
        List<AdDasSchemaPbClassView> adDasSchemaPbClassViews = adDasSchemaQueryBiz.querySchemaPbClassFromDb(pbVersion,pbClass);
        ResponseObject<List<AdDasSchemaPbClassView>> result = ResponseObject.ok();
        result.setData(adDasSchemaPbClassViews);
        return result;
    }


    @ApiOperation(value = "查询审批管理员列表", notes = "查询审批管理员列表")
    @ThreadNamePattern
    @GetMapping(value = "/queryApprovers")
    public ResponseObject<List<String>> queryApprovers(){
        Map<String, Integer> userTypeMap = adDasPlatformUserMsgs.get();
        List<String> approvers = Lists.newArrayList();
        userTypeMap.forEach((key, value) -> {
            if (value == 1) {
                approvers.add(key);
            }
        });
        return ResponseObject.ok(approvers);
    }

    @ApiOperation(value = "新建/修改用户组")
    @ThreadNamePattern
    @PostMapping(value = "/editUserGroup")
    @ApiModelProperty(value = "用户组id")
    public ResponseObject<Object> editUserGroup(@RequestBody @ApiParam StreamUserGroupEditParam param) {
        userGroupBiz.editUserGroup(param);
        return ResponseObject.ok();
    }

    @ApiOperation(value = "查询HDFS路径列表")
    @ThreadNamePattern
    @PostMapping(value = "/queryHdfsPath")
    public ResponseObject<List<String>> queryHdfsPaths() {
        List<String> hdfsPathList = new ArrayList<>(benchmarkManageBiz.getHdfsPath());
        return ResponseObject.ok(hdfsPathList);
    }

    @ApiOperation(value = "查询增量所有Topic列表")
    @ThreadNamePattern
    @PostMapping(value = "/queryTopics")
    public ResponseObject<List<String>> queryTopics() {
        List<String> hdfsPathList = dasIncrementModifyQueryBiz.getIncreTopics();

        return ResponseObject.ok(hdfsPathList);
    }

    @ApiOperation(value = "查询增量所有数据流组列表")
    @ThreadNamePattern
    @PostMapping(value = "/queryDataStreamEnum")
    public ResponseObject<List<AdDasCommonDataView>> queryDataStreamEnum() {
        ResponseObject<List<AdDasCommonDataView>> result = ResponseObject.ok();

        return result;
    }


}
