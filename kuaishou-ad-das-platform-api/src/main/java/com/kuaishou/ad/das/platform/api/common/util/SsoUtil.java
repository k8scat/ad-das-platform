package com.kuaishou.ad.das.platform.api.common.util;

import java.security.Principal;

import javax.servlet.http.HttpServletRequest;

import org.jasig.cas.client.authentication.AttributePrincipalImpl;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.kuaishou.ad.das.platform.utils.model.AdDasUserView;


/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-10-10
 */
public class SsoUtil {

    public static AdDasUserView currentUser() {
        ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes)
                RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = servletRequestAttributes.getRequest();
        String userId = request.getRemoteUser();
        Principal userPrincipal = request.getUserPrincipal();
        String displayName = (String) ((AttributePrincipalImpl) userPrincipal).getAttributes().get("displayName");
        String mail = (String) ((AttributePrincipalImpl) userPrincipal).getAttributes().get("mail");
        AdDasUserView user = new AdDasUserView();
        user.setUserName(userId);
        user.setDisplayName(displayName);
        user.setEmail(mail);
        return user;
    }
}