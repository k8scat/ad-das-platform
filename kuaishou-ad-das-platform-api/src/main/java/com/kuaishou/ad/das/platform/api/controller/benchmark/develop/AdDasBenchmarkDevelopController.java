package com.kuaishou.ad.das.platform.api.controller.benchmark.develop;

import static com.kuaishou.ad.das.platform.utils.constant.AdDasApiConstant.DAS_API_BENCHMARK_PATH;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.kuaishou.ad.das.platform.api.common.filter.CallContext;
import com.kuaishou.ad.das.platform.component.biz.benchmark.AdDasBenchmarkDevelopBiz;
import com.kuaishou.ad.das.platform.component.web.ResponseObject;
import com.kuaishou.ad.das.platform.component.web.param.AdDasBenchmarkDevelopHomeParam;
import com.kuaishou.ad.das.platform.component.web.param.AdDasBenchmarkDevelopTaskExecuteParam;
import com.kuaishou.ad.das.platform.component.web.view.benchmark.develop.AdDasBenchmarkDevelopBaseInfoView;
import com.kuaishou.ad.das.platform.component.web.view.benchmark.develop.AdDasBenchmarkDevelopHomeView;
import com.kuaishou.ad.das.platform.component.web.view.benchmark.develop.AdDasBenchmarkDevelopOutputDirectoryView;
import com.kuaishou.ad.das.platform.component.web.view.benchmark.develop.AdDasBenchmarkTaskEditRunRecordView;
import com.kuaishou.framework.web.annotation.ThreadNamePattern;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;

/**
 * @author wangkai <wangkai14@kuaishou.com>
 * Created on 2020-12-28
 */
@Slf4j
@RequestMapping(DAS_API_BENCHMARK_PATH + "/develop")
@RestController
@Api(value = "开发工作台接口")
public class AdDasBenchmarkDevelopController {

    @Autowired
    private AdDasBenchmarkDevelopBiz adDasBenchmarkDevelopBiz;

    @ApiOperation(value = "开发工作台首页列表查询接口", notes = "展示当前基准服务开发任务信息")
    @ThreadNamePattern
    @PostMapping(value = "/developHomePage")
    public ResponseObject<AdDasBenchmarkDevelopHomeView> manageHomePage(@ApiParam @RequestBody AdDasBenchmarkDevelopHomeParam param) {
        AdDasBenchmarkDevelopHomeView adDasBenchmarkDevelopHomeView = adDasBenchmarkDevelopBiz.queryAdDasBenchmarkTaskEditList(param);
        ResponseObject<AdDasBenchmarkDevelopHomeView> result = ResponseObject.ok();
        result.setData(adDasBenchmarkDevelopHomeView);
        return result;
    }

    @ApiOperation(value = "开发工作台基础信息查询", notes = "开发工作台基础信息查询")
    @ThreadNamePattern
    @GetMapping(value = "/queryBenchmarkBaseInfo")
    public ResponseObject<AdDasBenchmarkDevelopBaseInfoView> queryBenchmarkBaseInfo() {
        ResponseObject<AdDasBenchmarkDevelopBaseInfoView> result = ResponseObject.ok();
        AdDasBenchmarkDevelopBaseInfoView adDasBenchmarkDevelopBaseInfoView = adDasBenchmarkDevelopBiz.queryBenchmarkBaseInfo();
        result.setData(adDasBenchmarkDevelopBaseInfoView);
        return result;
    }

    @ApiOperation(value = "结果输出目录列表接口", notes = "结果输出目录列表接口")
    @ThreadNamePattern
    @GetMapping(value = "/queryOutputDirectoryType")
    public ResponseObject<List<AdDasBenchmarkDevelopOutputDirectoryView>> queryOutputDirectoryType() {
        List<AdDasBenchmarkDevelopOutputDirectoryView> outputDirectoryViews = adDasBenchmarkDevelopBiz.queryOutputDirectory();
        ResponseObject<List<AdDasBenchmarkDevelopOutputDirectoryView>> result = ResponseObject.ok();
        result.setData(outputDirectoryViews);
        return result;
    }

    @ApiOperation(value = "结果输出路径接口", notes = "结果输出路径接口")
    @ThreadNamePattern
    @GetMapping(value = "/queryOutputDirectoryPath")
    public ResponseObject<String> queryOutputDirectoryPath(
            @RequestParam(value = "outputDirectoryType") Integer outputDirectoryType) {
        String path = adDasBenchmarkDevelopBiz.queryOutputDirectoryPath(CallContext.getOperator(),outputDirectoryType);
        ResponseObject<String> result = ResponseObject.ok();
        result.setData(path);
        return result;
    }


    @ApiOperation(value = "任务执行接口", notes = "任务执行接口")
    @ThreadNamePattern
    @PostMapping(value = "/taskExecute")
    public ResponseObject<Object> taskExecute(@ApiParam @RequestBody AdDasBenchmarkDevelopTaskExecuteParam adDasBenchmarkDevelopTaskExecuteParam) {
        ResponseObject<Object> result = ResponseObject.ok();
        adDasBenchmarkDevelopBiz.taskExecute(CallContext.getOperator(),adDasBenchmarkDevelopTaskExecuteParam);
        return result;
    }

    @ApiOperation(value = "任务取消接口", notes = "任务取消接口")
    @ThreadNamePattern
    @PostMapping(value = "/taskCancel")
    public ResponseObject<Object> taskCancel(@RequestParam(value = "taskRunRecordId") Long taskRunRecordId) {
        adDasBenchmarkDevelopBiz.taskCancel(CallContext.getOperator(),taskRunRecordId);
        return ResponseObject.ok();
    }

    @ApiOperation(value = "任务执行详情", notes = "任务执行详情")
    @ThreadNamePattern
    @PostMapping(value = "/taskExecuteDetail")
    public ResponseObject<List<AdDasBenchmarkTaskEditRunRecordView>> taskExecuteDetail() {
        ResponseObject<List<AdDasBenchmarkTaskEditRunRecordView>> result = ResponseObject.ok();
        List<AdDasBenchmarkTaskEditRunRecordView> views = adDasBenchmarkDevelopBiz.taskExecuteDetail();
        result.setData(views);
        return result;
    }
}
