package com.kuaishou.ad.das.platform.api.controller.increment;

import static com.kuaishou.ad.das.platform.utils.constant.AdDasApiConstant.DAS_PLATFORM_API_PATH;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.kuaishou.ad.das.platform.api.common.filter.CallContext;
import com.kuaishou.ad.das.platform.component.biz.increment.AdDasIncrementModifyModBiz;
import com.kuaishou.ad.das.platform.component.web.ResponseObject;
import com.kuaishou.ad.das.platform.component.web.param.AdDasPlatformIncreModifyParam;
import com.kuaishou.framework.web.annotation.ThreadNamePattern;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-05-24
 */
@Api(value = "ad-das-platform-incre-mod-controller", description = "开放平台增量流mod相关接口")
@Slf4j
@RequestMapping(DAS_PLATFORM_API_PATH + "/increment")
@RestController
public class AdDasPlatformIncreModController {

    @Autowired
    private AdDasIncrementModifyModBiz incrementManageBiz;

    @ApiOperation(value = "增量流变更表接口", notes = "增量流变更表")
    @ThreadNamePattern
    @RequestMapping(value = "/increModify", method = RequestMethod.POST)
    public ResponseObject<Object> increModify(@RequestBody AdDasPlatformIncreModifyParam increModifyParam) {
        ResponseObject<Object> result = ResponseObject.ok();

        incrementManageBiz.increModify(increModifyParam, CallContext.getOperator());

        return result;
    }

    @ApiOperation(value = "审批增量变更接口", notes = "审批增量变更接口")
    @ThreadNamePattern
    @RequestMapping(value = "/approveIncreModify", method = RequestMethod.POST)
    public ResponseObject<Object> approveModifyIncrement(
            @ApiParam(value = "变更id") @RequestParam(value = "modifyId", required = true) Long modifyId,
            @ApiParam(value = "变更类型") @RequestParam(value = "approveType", required = true) Integer approveType,
            @ApiParam(value = "驳回理由", allowEmptyValue = true) @RequestParam(value = "reason", required = false)
                    String reason) {
        ResponseObject<Object> result = ResponseObject.ok();

        incrementManageBiz.approveModifyIncrement(modifyId, approveType, reason, CallContext.getOperator());

        return result;
    }


    @ApiOperation(value = "发布增量变更接口", notes = "发布增量变更接口")
    @ThreadNamePattern
    @RequestMapping(value = "/publishIncreModify", method = RequestMethod.POST)
    public ResponseObject<Object> publishModifyIncrement(
            @ApiParam(value = "变更id") @RequestParam(value = "modifyId", required = true) Long modifyId) {
        ResponseObject<Object> result = ResponseObject.ok();

        incrementManageBiz.publishModifyIncrement(modifyId, CallContext.getOperator());

        return result;
    }

    @ApiOperation(value = "终止增量变更接口", notes = "终止增量变更接口")
    @ThreadNamePattern
    @RequestMapping(value = {"/terminateIncreModify", "/giveUpIncreModify"}, method = RequestMethod.POST)
    public ResponseObject<Object> terminateModifyIncrement(
            @ApiParam(value = "变更id") @RequestParam(value = "modifyId", required = true) Long modifyId) {
        ResponseObject<Object> result = ResponseObject.ok();

        incrementManageBiz.terminateModifyIncrement(modifyId, CallContext.getOperator());

        return result;
    }

    @ApiOperation(value = "回滚增量变更接口", notes = "回滚增量变更接口")
    @ThreadNamePattern
    @RequestMapping(value = "/rollbackIncreModify", method = RequestMethod.POST)
    public ResponseObject<Object> rollbackModifyIncrement(
            @ApiParam(value = "变更id") @RequestParam(value = "modifyId", required = true) Long modifyId) {
        ResponseObject<Object> result = ResponseObject.ok();

        incrementManageBiz.rollbackModifyIncrement(modifyId, CallContext.getOperator());

        return result;
    }

}
