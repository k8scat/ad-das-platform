package com.kuaishou.ad.das.platform.api.controller.modify;

import static com.kuaishou.ad.das.platform.utils.constant.AdDasApiConstant.DAS_PLATFORM_API_PATH;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.kuaishou.ad.das.platform.api.common.filter.CallContext;
import com.kuaishou.ad.das.platform.component.biz.benchmark.AdDasBenchmarkModifyBiz;
import com.kuaishou.ad.das.platform.component.web.ResponseObject;
import com.kuaishou.ad.das.platform.component.web.param.AdDasModifyBenchmarkParam;
import com.kuaishou.ad.das.platform.component.web.view.AdDasModifyBenchmarkDataView;
import com.kuaishou.ad.das.platform.core.benchmark.model.AdDasBenchmarkMainTableView;
import com.kuaishou.framework.web.annotation.ThreadNamePattern;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;

/**
 * das变更基准工单编辑相关接口
 *
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-11-22
 */
@Slf4j
@RequestMapping(DAS_PLATFORM_API_PATH + "/modify/manage")
@RestController
@Api(value = "基准变更操作接口，包括新建、修改，审批、发布基准变更等页面")
public class AdDasModifyBenchmarkModController {

    @Autowired
    private AdDasBenchmarkModifyBiz adDasBenchmarkModifyBiz;

    @ApiOperation(value = "新建基准变更接口", notes = "新建基准变更接口")
    @ThreadNamePattern
    @PostMapping(value = "/addModifyBenchmark")
    public ResponseObject<Long> addModifyBenchmark(@ApiParam @RequestBody AdDasModifyBenchmarkParam param) {
        ResponseObject<Long> result = ResponseObject.ok();
        Long modifyId = adDasBenchmarkModifyBiz.createBenchmark(CallContext.getOperator(), param);
        result.setData(modifyId);
        return result;
    }

    @ApiOperation(value = "审批基准变更接口", notes = "审批基准变更接口")
    @ThreadNamePattern
    @PostMapping(value = "/approveModifyBenchmark")
    public ResponseObject<Object> approveModifyBenchmark(
            @RequestParam Integer approveType,
            @RequestParam(required = false) String reason,
            @RequestParam Long modifyId
    ) {
        ResponseObject<Object> result = ResponseObject.ok();
        adDasBenchmarkModifyBiz.approveModifyBenchmark(CallContext.getOperator(), approveType, reason, modifyId);
        return result;
    }

    @ApiOperation(value = "发布基准变更接口", notes = "发布基准变更接口")
    @ThreadNamePattern
    @PostMapping(value = "/publishModifyBenchmark")
    public ResponseObject<Object> publishModifyBenchmark(@RequestParam Integer approveType, @RequestParam Long modifyId) {
        adDasBenchmarkModifyBiz.publishModifyBenchmark(CallContext.getOperator(), approveType, modifyId);
        return ResponseObject.ok();
    }


    @ApiOperation(value = "生成基准测试任务", notes = "生成基准测试任务")
    @ThreadNamePattern
    @PostMapping(value = "/createBenchmarkTask")
    public ResponseObject<Long> createBenchmarkTask(@ApiParam @RequestBody AdDasModifyBenchmarkParam param) {
        ResponseObject<Long> result = ResponseObject.ok();
        Long modifyId = adDasBenchmarkModifyBiz.createBenchmarkTask(CallContext.getOperator(),param);
        result.setData(modifyId);
        return result;
    }

    @ApiOperation(value = "提交发布审批", notes = "提交发布审批")
    @ThreadNamePattern
    @PostMapping(value = "/submitBenchmark")
    public ResponseObject<Object> submitBenchmark(@ApiParam @RequestBody AdDasModifyBenchmarkParam param) {
        adDasBenchmarkModifyBiz.submitBenchmark(CallContext.getOperator(), param);
        return ResponseObject.ok();
    }

    @ApiOperation(value = "废弃变更", notes = "废弃变更")
    @ThreadNamePattern
    @PostMapping(value = "/discardBenchmark")
    public ResponseObject<Object> discardBenchmark(@RequestParam Long modifyId) {
        ResponseObject<Object> result = ResponseObject.ok();
        adDasBenchmarkModifyBiz.discardBenchmark(CallContext.getOperator(), modifyId);
        return result;
    }

    @ApiOperation(value = "显示变更详情", notes = "显示变更详情")
    @ThreadNamePattern
    @PostMapping(value = "/queryBenchmark")
    public ResponseObject<AdDasModifyBenchmarkDataView> queryBenchmark(@RequestParam Long modifyId) {
        ResponseObject<AdDasModifyBenchmarkDataView> result = ResponseObject.ok();
        AdDasModifyBenchmarkDataView adDasModifyBenchmarkDataView = adDasBenchmarkModifyBiz.queryBenchmark(modifyId);
        result.setData(adDasModifyBenchmarkDataView);
        return result;
    }

    @ApiOperation(value = "查看基准上一版本详情", notes = "查看基准上一版本详情")
    @ThreadNamePattern
    @PostMapping(value = "/queryPreviousBenchmark")
    public ResponseObject<AdDasModifyBenchmarkDataView> queryPreviousBenchmark(@RequestParam Long modifyId) {
        ResponseObject<AdDasModifyBenchmarkDataView> result = ResponseObject.ok();
        AdDasModifyBenchmarkDataView adDasModifyBenchmarkDataView = adDasBenchmarkModifyBiz.queryPreviousBenchmark(modifyId);
        result.setData(adDasModifyBenchmarkDataView);
        return result;
    }

    @ApiOperation(value = "根据mainTableName查询是否已有接入", notes = "查询最新pb对应的table")
    @ThreadNamePattern
    @GetMapping(value = "/queryBenchmarkByTable")
    public ResponseObject<AdDasModifyBenchmarkDataView> queryBenchmarkByTable(
            @RequestParam Long benchmarkId, @RequestParam String mainTable, @RequestParam String dataSource) {
        ResponseObject<AdDasModifyBenchmarkDataView> result = ResponseObject.ok();
        // 根据tableName 查询 现有接入table任务 ad_das_benchmark_detail 数据
        AdDasModifyBenchmarkDataView adDasModifyBenchmarkDataView = adDasBenchmarkModifyBiz
                .queryBenchmarkByTable(benchmarkId,mainTable,dataSource);
        result.setData(adDasModifyBenchmarkDataView);
        return result;
    }

    @ApiOperation(value = "预览diff 接口", notes = "预览diff 接口")
    @ThreadNamePattern
    @GetMapping(value = "/queryCurBenchmark")
    public ResponseObject<AdDasBenchmarkMainTableView> queryCurBenchmark(
            @RequestParam Long benchmarkId, @RequestParam String mainTable, @RequestParam String dataSource) {
        ResponseObject<AdDasBenchmarkMainTableView> result = ResponseObject.ok();
        AdDasBenchmarkMainTableView tableView = adDasBenchmarkModifyBiz
                .queryCurBenchmark(benchmarkId, mainTable, dataSource);
        result.setData(tableView);
        return result;
    }

    @ApiOperation(value = "回滚基准变更接口", notes = "回滚基准变更接口")
    @ThreadNamePattern
    @RequestMapping(value = "/rollbackModify", method = RequestMethod.POST)
    public ResponseObject<Object> rollbackModifyBenchmark(
            @ApiParam(value = "变更id") @RequestParam(value = "modifyId", required = true) Long modifyId) {
        ResponseObject<Object> result = ResponseObject.ok();

        adDasBenchmarkModifyBiz.rollbackBenchmark(CallContext.getOperator(), modifyId);

        return result;
    }
}
