package com.kuaishou.ad.das.platform.api.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.kuaishou.framework.rpc.server.WarmUp;
import com.kuaishou.framework.web.annotation.ThreadNamePattern;

import lombok.extern.slf4j.Slf4j;
import springfox.documentation.annotations.ApiIgnore;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-08-08
 */
@ApiIgnore
@Slf4j
@RestController
public class StatusController implements WarmUp {

    @ResponseBody
    @RequestMapping(value = "/", method = {RequestMethod.GET})
    public Object home(HttpServletResponse response) throws IOException {
        return "ok";
    }

    @RequestMapping("/init")
    @ThreadNamePattern
    public String status(HttpServletRequest request, HttpServletResponse response) {

        return "ok";
    }

    @Override
    public void warmup() throws Exception {
        log.info("ad-das-api start warmup!");
        com.kuaishou.framework.spring.BeanFactory.tryWarmup();
        log.info("ad-das-api warmup success!");
    }
}
