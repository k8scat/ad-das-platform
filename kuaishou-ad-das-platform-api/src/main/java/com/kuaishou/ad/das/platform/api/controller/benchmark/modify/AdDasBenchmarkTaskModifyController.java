package com.kuaishou.ad.das.platform.api.controller.benchmark.modify;

import static com.kuaishou.ad.das.platform.utils.constant.AdDasApiConstant.DAS_API_BENCHMARK_PATH;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.kuaishou.ad.das.platform.api.common.filter.CallContext;
import com.kuaishou.ad.das.platform.component.biz.benchmark.AdDasBenchmarkTaskModifyBiz;
import com.kuaishou.ad.das.platform.component.web.ResponseObject;
import com.kuaishou.ad.das.platform.component.web.view.benchmark.modify.AdDasBenchmarkTaskView;
import com.kuaishou.framework.web.annotation.ThreadNamePattern;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;

/**
 * @author wangkai <wangkai14@kuaishou.com>
 * Created on 2020-12-28
 */

@Slf4j
@RequestMapping(DAS_API_BENCHMARK_PATH + "/task")
@RestController
@Api(value = "基准管理任务变更页面")
public class AdDasBenchmarkTaskModifyController {

    @Autowired
    private AdDasBenchmarkTaskModifyBiz adDasBenchmarkTaskModifyBiz;

    @ApiOperation(value = "基准管理任务详情接口", notes = "基准管理任务详情接口")
    @ThreadNamePattern
    @GetMapping(value = "/taskDetail")
    public ResponseObject<AdDasBenchmarkTaskView> benchmarkTaskDetail(
            @RequestParam(value = "taskId") Long taskId, @RequestParam(value = "taskType") Integer taskType) {
        AdDasBenchmarkTaskView adDasBenchmarkTaskView = adDasBenchmarkTaskModifyBiz.benchmarkTaskDetail(taskId, taskType);
        ResponseObject<AdDasBenchmarkTaskView> result = ResponseObject.ok();
        result.setData(adDasBenchmarkTaskView);
        return result;
    }

    @ApiOperation(value = "基准管理任务编辑接口", notes = "基准管理任务编辑接口")
    @ThreadNamePattern
    @PostMapping(value = "/taskEdit")
    public ResponseObject<Object> benchmarkTaskEdit(@ApiParam @RequestBody AdDasBenchmarkTaskView adDasBenchmarkTaskView) {
        adDasBenchmarkTaskModifyBiz.benchmarkTaskEdit(CallContext.getOperator(),adDasBenchmarkTaskView);
        return ResponseObject.ok();
    }
}
