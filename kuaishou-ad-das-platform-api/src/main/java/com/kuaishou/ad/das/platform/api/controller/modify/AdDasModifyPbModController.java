package com.kuaishou.ad.das.platform.api.controller.modify;

import static com.kuaishou.ad.das.platform.utils.constant.AdDasApiConstant.DAS_PLATFORM_API_PATH;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.kuaishou.ad.das.platform.api.common.filter.CallContext;
import com.kuaishou.ad.das.platform.component.biz.modify.AdDasModifyModBiz;
import com.kuaishou.ad.das.platform.component.web.ResponseObject;
import com.kuaishou.ad.das.platform.component.web.param.AdDasModifyPbParam;
import com.kuaishou.ad.das.platform.component.web.param.AdDasPublishPbParam;
import com.kuaishou.ad.das.platform.component.web.view.AdDasModifyDataView;
import com.kuaishou.framework.web.annotation.ThreadNamePattern;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;

/**
 * das变更PB工单编辑相关接口
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-11-26
 */
@Slf4j
@RequestMapping(DAS_PLATFORM_API_PATH + "/modify/manage")
@RestController
public class AdDasModifyPbModController {

    @Autowired
    private AdDasModifyModBiz adDasModifyModBiz;

    @ApiOperation(value = "新增pb变更接口", notes = "新增pb变更接口")
    @ThreadNamePattern
    @RequestMapping(value = "/addModifyPb", method = RequestMethod.POST)
    public ResponseObject<AdDasModifyDataView> addModifyPb(@ApiParam @RequestBody AdDasModifyPbParam param) {

        ResponseObject<AdDasModifyDataView> result = ResponseObject.ok();

        AdDasModifyDataView adDasModifyDataView = adDasModifyModBiz.addModifyPb(CallContext.getOperator(), param);
        result.setData(adDasModifyDataView);

        return result;
    }

    @ApiOperation(value = "审批pb变更接口", notes = "审批pb变更接口")
    @ThreadNamePattern
    @RequestMapping(value = "/approveModifyPb", method = RequestMethod.POST)
    public ResponseObject<Object> approveModifyPb(
            @RequestParam(value = "modifyId", required = true) Long modifyId,
            @RequestParam(value = "approveType", required = true) Integer approveType,
            @RequestParam(value = "reason", required = false) String reason) {

        ResponseObject<Object> result = ResponseObject.ok();

         adDasModifyModBiz.approveModifyPb(CallContext.getOperator(),
                 modifyId, approveType, reason);

        return result;
    }

    @ApiOperation(value = "发布pb变更接口", notes = "发布pb变更接口")
    @ThreadNamePattern
    @RequestMapping(value = "/publishModifyPb", method = RequestMethod.POST)
    public ResponseObject<Object> publishModifyPb(@ApiParam @RequestBody AdDasPublishPbParam param) {

        ResponseObject<Object> result = ResponseObject.ok();

        adDasModifyModBiz.publishModifyPb(CallContext.getOperator(), param);

        return result;
    }


    @ApiOperation(value = "终止pb变更接口", notes = "终止pb变更接口")
    @ThreadNamePattern
    @RequestMapping(value = "/terminateModifyPb", method = RequestMethod.POST)
    public ResponseObject<Object> terminateModifyPb(@ApiParam @RequestBody AdDasPublishPbParam param) {

        ResponseObject<Object> result = ResponseObject.ok();

        adDasModifyModBiz.terminateModifyPb(CallContext.getOperator(), param);

        return result;
    }

    @ApiOperation(value = "回滚PB变更接口", notes = "回滚PB变更")
    @ThreadNamePattern
    @RequestMapping(value = "/rollbackModifyPb", method = RequestMethod.POST)
    public ResponseObject<Object> rollbackModifyPb(@ApiParam @RequestBody AdDasPublishPbParam param) {

        ResponseObject<Object> result = ResponseObject.ok();

        adDasModifyModBiz.rollBackModifyPb(CallContext.getOperator(), param);

        return result;
    }

}
