package com.kuaishou.ad.das.platform.api.common.resolver;

import javax.servlet.http.HttpServletRequest;

import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;


import com.kuaishou.ad.das.platform.api.common.filter.AdDasCasTokenHelper;
import com.kuaishou.ad.das.platform.utils.annotation.AdDasRole;
import com.kuaishou.ad.das.platform.utils.model.AdDasUserView;

import lombok.extern.slf4j.Slf4j;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-09-26
 */
@Slf4j
public class AdDasRoleResolver implements HandlerMethodArgumentResolver {

    @Override
    public boolean supportsParameter(MethodParameter parameter) {
        return parameter.getParameterAnnotation(AdDasRole.class) != null;
    }

    @Override
    public Object resolveArgument(MethodParameter parameter, ModelAndViewContainer mavContainer,
                                  NativeWebRequest webRequest, WebDataBinderFactory binderFactory) throws Exception {

        AdDasUserView dasUserView  = AdDasCasTokenHelper.getRemoteUser(webRequest.getNativeRequest(HttpServletRequest.class));
        log.info("role user: {}", dasUserView);

        return dasUserView;
    }
}
