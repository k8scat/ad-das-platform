package com.kuaishou.ad.das.platform.api.common.handler;

import static com.kuaishou.ad.das.platform.utils.exception.ErrorCode.INTERNAL_ERROR;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.google.common.collect.Maps;
import com.kuaishou.ad.das.platform.utils.exception.AdDasServiceException;
import com.kuaishou.old.exception.ServiceException;
import com.kuaishou.util.BaseExceptionHandler;

import io.sentry.Sentry;
import lombok.extern.slf4j.Slf4j;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-09-26
 */
@Slf4j
@RestControllerAdvice
public class WebApiExceptionHandler extends BaseExceptionHandler {

    @ExceptionHandler(AdDasServiceException.class)
    public Map<String, Object> handleDasException(AdDasServiceException exception) {
        Sentry.capture(exception);
        log.error("adDasServiceException:", exception);
        return ofDasError(exception);
    }


    @ExceptionHandler(ServiceException.class)
    public Map<String, Object> handleException(ServiceException exception) {
        Sentry.capture(exception);
        log.error("exception:", exception);
        return ofError(exception);
    }

    @ExceptionHandler(Throwable.class)
    public Map<String, Object> handleAllThrowable(HttpServletRequest request, Throwable exception) {
        Sentry.capture(exception);
        log.error("exception:", exception);
        return ofDasError(AdDasServiceException.ofMessage(INTERNAL_ERROR));
    }

    @ExceptionHandler(Exception.class)
    public Map<String, Object> handleAllException(HttpServletRequest request, Exception exception) {
        Sentry.capture(exception);
        log.error("exception:", exception);

        return ofDasError(AdDasServiceException.ofMessage(INTERNAL_ERROR));
    }

    private Map<String, Object> ofError(ServiceException exception) {

        Map<String, Object> result = Maps.newHashMap();
        log.info("exception={}", exception);
        result.put("result", exception.getCode());
        log.info("exception={}", exception);

        String errorMsg = exception.getMessage();
        if (!StringUtils.isEmpty(exception.getOverrideMessage())) {
            errorMsg = exception.getOverrideMessage();
        }

        result.put("errorMsg", errorMsg);
        return result;
    }


    private Map<String, Object> ofDasError(AdDasServiceException exception) {


        Map<String, Object> result = Maps.newHashMap();
        result.put("result", exception.getCode());
        String errorMsg = exception.getErrorMsg();
        result.put("errorMsg", errorMsg);
        log.info("result={}", result);

        return result;
    }
}
