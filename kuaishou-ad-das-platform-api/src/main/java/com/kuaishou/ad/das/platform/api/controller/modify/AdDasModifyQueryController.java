package com.kuaishou.ad.das.platform.api.controller.modify;

import static com.kuaishou.ad.das.platform.utils.constant.AdDasApiConstant.DAS_PLATFORM_API_PATH;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.kuaishou.ad.das.platform.component.biz.modify.AdDasModifyQueryBiz;
import com.kuaishou.ad.das.platform.component.web.ResponseObject;
import com.kuaishou.ad.das.platform.component.web.param.AdDasModifySearchParam;
import com.kuaishou.ad.das.platform.component.web.param.benchmark.BenchmarkModifyQueryParam;
import com.kuaishou.ad.das.platform.component.web.view.AdDasModifyBenchmarkDataView;
import com.kuaishou.ad.das.platform.component.web.view.AdDasModifyDataListView;
import com.kuaishou.ad.das.platform.component.web.view.AdDasModifyIncrementItemView;
import com.kuaishou.ad.das.platform.component.web.view.AdDasModifyIncrementView;
import com.kuaishou.ad.das.platform.component.web.view.AdDasModifyPbView;
import com.kuaishou.ad.das.platform.component.web.view.AdDasModifyTypeView;
import com.kuaishou.ad.das.platform.component.web.view.AdDasServiceInstancesView;
import com.kuaishou.ad.das.platform.component.web.view.AdDasStatusView;
import com.kuaishou.ad.das.platform.component.web.view.benchmark.modify.BenchmarkModifyQueryView;
import com.kuaishou.framework.web.annotation.ThreadNamePattern;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;

/**
 * das变更工单查询相关接口
 *
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-11-22
 */
@Slf4j
@RequestMapping(DAS_PLATFORM_API_PATH + "/modify/manage")
@RestController
public class AdDasModifyQueryController {

    @Autowired
    private AdDasModifyQueryBiz adDasModifyQueryBiz;

    @ApiOperation(value = "变更首页接口", notes = "展示变更工单列表")
    @ThreadNamePattern
    @PostMapping(value = "/manageHomePage")
    public ResponseObject<AdDasModifyDataListView> modifyHome(
            @ApiParam @RequestBody AdDasModifySearchParam param) {
        ResponseObject<AdDasModifyDataListView> result = ResponseObject.ok();
        AdDasModifyDataListView adDasModifyDataListView = adDasModifyQueryBiz.modifyHome(param);
        result.setData(adDasModifyDataListView);
        return result;
    }

    @ApiOperation(value = "查询变更基准详情", notes = "展示基准变更工单详情数据")
    @ThreadNamePattern
    @GetMapping(value = "/benchmarkModifyData")
    public ResponseObject<AdDasModifyBenchmarkDataView> benchmarkModifyData(
            @RequestParam @ApiParam(value = "变更id") Long modifyId) {
        ResponseObject<AdDasModifyBenchmarkDataView> result = ResponseObject.ok();
        // 根据 modifyId查询 ad_das_modify_data表， 然后查询 基准变更详情数据表 ad_das_modify_benchmark_detail
        AdDasModifyBenchmarkDataView adDasModifyBenchmarkDataView = adDasModifyQueryBiz.benchmarkModifyData(modifyId);
        result.setData(adDasModifyBenchmarkDataView);
        return result;
    }


    @ApiOperation(value = "查询变更审批人", notes = "查询变更审批人")
    @ThreadNamePattern
    @GetMapping(value = "/queryApprover")
    public ResponseObject<List<String>> queryApprover() {
        ResponseObject<List<String>> result = ResponseObject.ok();
        // 查询Kconf配置的审批人
        List<String> approvers = adDasModifyQueryBiz.queryApprover();
        result.setData(approvers);
        return result;
    }

    @ApiOperation(value = "查询变更类型", notes = "查询变更类型")
    @ThreadNamePattern
    @GetMapping(value = "/queryModifyType")
    public ResponseObject<List<AdDasModifyTypeView>> queryModifyType() {
        ResponseObject<List<AdDasModifyTypeView>> result = ResponseObject.ok();
        List<AdDasModifyTypeView> adDasModifyTypeViews = adDasModifyQueryBiz.queryModifyTypeEnum();
        result.setData(adDasModifyTypeViews);
        return result;
    }


    @ApiOperation(value = "查询变更pb详情", notes = "查询变更pb详情")
    @ThreadNamePattern
    @GetMapping(value = "/pbModifyData")
    public ResponseObject<AdDasModifyPbView> pbModifyData(
            @RequestParam Long modifyId, @RequestParam Integer modifyType) {
        ResponseObject<AdDasModifyPbView> result = ResponseObject.ok();
        AdDasModifyPbView adDasModifyPbView = adDasModifyQueryBiz.pbModifyData(modifyId, modifyType);
        result.setData(adDasModifyPbView);
        return result;
    }

    @ApiOperation(value = "查询变更增量详情", notes = "查询变更增量详情")
    @ThreadNamePattern
    @GetMapping(value = "/incrementModifyData")
    public ResponseObject<AdDasModifyIncrementView> incrementModifyData(
            @RequestParam Long modifyId, @RequestParam Integer modifyType) {
        ResponseObject<AdDasModifyIncrementView> result = ResponseObject.ok();
        AdDasModifyIncrementView adDasModifyIncrementView = adDasModifyQueryBiz
                .incrementModifyData(modifyId, modifyType);
        result.setData(adDasModifyIncrementView);
        return result;
    }


    @ApiOperation(value = "查询服务注册的机器实例信息", notes = "查询服务注册的机器实例信息")
    @ThreadNamePattern
    @GetMapping(value = "/queryServiceInstance")
    public ResponseObject<List<AdDasServiceInstancesView>> queryServiceInstance(
            @RequestParam Long modifyId, @RequestParam String serviceName) {
        ResponseObject<List<AdDasServiceInstancesView>> result = ResponseObject.ok();
        List<AdDasServiceInstancesView> adDasServiceInstancesViews = adDasModifyQueryBiz
                .queryServiceInstance(modifyId, serviceName);
        result.setData(adDasServiceInstancesViews);
        return result;
    }

    @ApiOperation(value = "查询变更状态枚举", notes = "查询变更状态枚举")
    @ThreadNamePattern
    @GetMapping(value = "/queryModifyStatusEnum")
    public ResponseObject<List<AdDasStatusView>> queryModifyStatusEnum() {
        ResponseObject<List<AdDasStatusView>> result = ResponseObject.ok();
        List<AdDasStatusView> statusViews = adDasModifyQueryBiz.queryModifyStatusEnum();
        result.setData(statusViews);
        return result;
    }

    @ApiOperation(value = "查询增量变更详情")
    @ThreadNamePattern
    @GetMapping(value = "/query/incrementModifyData")
    public ResponseObject<AdDasModifyIncrementItemView> queryIncrementTableDetailModify(
            @RequestParam Long modifyId, @RequestParam Integer modifyType) {
        return ResponseObject.ok();
    }

    @ApiOperation(value = "查询基准变更列表", notes = "不包含大字段")
    @ThreadNamePattern
    @PostMapping(value = "/query/benchmarkModifyData")
    public ResponseObject<BenchmarkModifyQueryView> queryBenchmarkModify(
            @ApiParam @RequestBody BenchmarkModifyQueryParam param) {
        BenchmarkModifyQueryView view = adDasModifyQueryBiz.queryView(param);
        return ResponseObject.ok(view);
    }

}
