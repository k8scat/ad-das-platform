package com.kuaishou.ad.das.platform.api.controller.user;

import static com.kuaishou.ad.das.platform.utils.constant.AdDasApiConstant.DAS_API_PATH;

import java.io.IOException;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.kuaishou.ad.das.platform.api.common.util.AdDasApiEnvUtils;
import com.kuaishou.framework.web.annotation.ThreadNamePattern;
import com.kuaishou.infra.passport.sdk.http.util.InfraCookieUtils;
import com.kuaishou.util.Result;

import lombok.extern.slf4j.Slf4j;

/**
 * das用户登录相关接口
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-09-29
 */
@Slf4j
@RestController
public class AdDasUserLoginController {

    @ResponseBody
    @GetMapping(value = DAS_API_PATH + "/redirect")
    public Object redirect(HttpServletResponse response) throws IOException {
        log.info("User redirecting...");
        response.sendRedirect(AdDasApiEnvUtils.getUrlBase());
        return Result.success();
    }

    @ThreadNamePattern
    @GetMapping(value = DAS_API_PATH + "/logout")
    public Object logout(HttpServletRequest request, HttpServletResponse response) throws IOException {
        //1.获取当前cookie信息
        String host = request.getHeader("Host");
        log.info("---- owner logout host:{} ----", host);
        Cookie[] cookies = request.getCookies();
        for (Cookie cookie : cookies) {
            log.info(
                    "cookieName:{} cookieValue:{} cookieDomain:{} cookiePath:{} cookieMaxAge:{} cookieSecure:{}",
                    cookie.getName(), cookie.getValue(), cookie.getDomain(), cookie.getPath(),
                    cookie.getMaxAge(), cookie.getSecure());
        }
        //2.将当前cookie和session信息设置为失效状态
        request.getSession().invalidate();
        Cookie phCookie = InfraCookieUtils.buildExpiredCookie("LOGINSTATUS", "", "", "/");
        phCookie.setMaxAge(Integer.MAX_VALUE);
        InfraCookieUtils.addCookie(response, phCookie);
        Cookie cookie = InfraCookieUtils.buildExpiredCookie("JSESSIONID", "", "", "/");
        cookie.setMaxAge(Integer.MAX_VALUE);
        InfraCookieUtils.addCookie(response, cookie);
        String redirecting = "https://sso.corp.kuaishou.com/cas/logout?service=" + AdDasApiEnvUtils.getUrlBase();
        log.info("User redirecting:{}...", redirecting);
        response.sendRedirect(redirecting);
        return Result.success();
    }

}
