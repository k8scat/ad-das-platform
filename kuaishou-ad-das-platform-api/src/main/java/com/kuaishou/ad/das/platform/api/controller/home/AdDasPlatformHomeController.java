package com.kuaishou.ad.das.platform.api.controller.home;

import static com.kuaishou.ad.das.platform.utils.constant.AdDasApiConstant.DAS_PLATFORM_API_PATH;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.kuaishou.ad.das.platform.api.common.filter.CallContext;
import com.kuaishou.ad.das.platform.component.biz.home.AdDasPlatformHomeBiz;
import com.kuaishou.ad.das.platform.component.web.ResponseObject;
import com.kuaishou.ad.das.platform.component.web.view.AdDasPlatformHomeView;
import com.kuaishou.framework.web.annotation.ThreadNamePattern;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-08-02
 */
@Api(value = "das平台首页相关接口")
@Slf4j
@RequestMapping(DAS_PLATFORM_API_PATH + "/home")
@RestController
public class AdDasPlatformHomeController {

    @Autowired
    private AdDasPlatformHomeBiz platformHomeBiz;

    @ApiOperation(value = "开放平台首页接口")
    @ThreadNamePattern
    @GetMapping(value = "/platformHome")
    public ResponseObject<AdDasPlatformHomeView> platformHome() {
        ResponseObject<AdDasPlatformHomeView> result = ResponseObject.ok();

        AdDasPlatformHomeView adDasPlatformHomeView = platformHomeBiz.platformHome(CallContext.getOperator());
        result.setData(adDasPlatformHomeView);

        return result;
    }

}
