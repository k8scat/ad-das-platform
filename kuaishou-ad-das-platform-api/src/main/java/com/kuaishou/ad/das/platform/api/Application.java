package com.kuaishou.ad.das.platform.api;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;

import com.kuaishou.infra.boot.KsSpringApplication;
import com.kuaishou.infra.boot.autoconfigure.KsBootApplication;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-05-26
 */
@KsBootApplication(scanBasePackages = {"com.kuaishou.ad.das"}, exclude = {SecurityAutoConfiguration.class})
@MapperScan("com.kuaishou.ad.das.platform.dal.repository.mapper")
public class Application {

    public static void main(String[] args) {
        KsSpringApplication.justRun(args);
    }

}