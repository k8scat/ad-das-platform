package com.kuaishou.ad.das.platform.api.controller.schema;

import static com.kuaishou.ad.das.platform.utils.constant.AdDasApiConstant.DAS_PLATFORM_API_PATH;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.kuaishou.ad.das.platform.api.common.filter.CallContext;
import com.kuaishou.ad.das.platform.component.biz.schema.AdDasSchemaModBiz;
import com.kuaishou.ad.das.platform.component.web.ResponseObject;
import com.kuaishou.ad.das.platform.component.web.param.AdDasPbClassColumnParam;
import com.kuaishou.ad.das.platform.component.web.view.AdDasSchemaPbDetailView;
import com.kuaishou.framework.web.annotation.ThreadNamePattern;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;

/**
 * das元信息编辑相关接口
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-11-22
 */
@Api(value = "ad-das-schema-mod-controller", description = "das元信息编辑相关接口")
@Slf4j
@RequestMapping(DAS_PLATFORM_API_PATH + "/schema/pb")
@RestController
public class AdDasSchemaModController {

    @Autowired
    private AdDasSchemaModBiz adDasSchemaModBiz;

    @ApiOperation(value = "更新pbjar版本信息", notes = "标记当前最新pbJar信息")
    @ThreadNamePattern
    @RequestMapping(value = "/updatePbJar", method = RequestMethod.POST)
    public ResponseObject<Object> updatePbJar(
            @ApiParam(value = "pb包信息") @RequestParam(value = "pbJarVersion", required = true) String pbJarVersion,
            @ApiParam(value = "pbEnum包信息") @RequestParam(value = "pbEnumVersion", required = false) String pbEnumVersion,
            @ApiParam(value = "pbCommon包信息") @RequestParam(value = "pbCommonVersion", required = false) String pbCommonVersion,
            @ApiParam(value = "是否同步上一版本的PBClass: 0-否 1-是") @RequestParam(value = "autoAddPbClass", required = true) Integer autoAddPbClass) {

        ResponseObject<Object> result = ResponseObject.ok();

        // 校验pb版本，不可以回退
        // 写 ad_das_pb 表
        adDasSchemaModBiz.updatePbJar(CallContext.getOperator(), pbJarVersion, pbEnumVersion, pbCommonVersion, 1);
        result.setData(pbJarVersion);

        return result;
    }

    @ApiOperation(value = "新增标记pbClass", notes = "保存指定pbClass")
    @ThreadNamePattern
    @RequestMapping(value = "/addPbClass", method = RequestMethod.POST)
    public ResponseObject<AdDasSchemaPbDetailView> addPbClass(
            @ApiParam(value = "pb包信息") @RequestParam(value = "pbJarVersion", required = true) String pbJarVersion,
            @ApiParam(value = "pbClass名") @RequestParam(value = "pbClassName", required = true) String pbClassName) {

        ResponseObject<AdDasSchemaPbDetailView> result = ResponseObject.ok();

        // 新增标记 pblcass 信息
        AdDasSchemaPbDetailView pbDetailView = adDasSchemaModBiz
                .addPbClass(CallContext.getOperator(), pbJarVersion, pbClassName);
        result.setData(pbDetailView);

        return result;
    }

    @ApiOperation(value = "删除或者指定pbClass", notes = "删除或者指定pbClass")
    @ThreadNamePattern
    @RequestMapping(value = "/updateStatus", method = RequestMethod.POST)
    public ResponseObject<Object> updateStatus(
            @ApiParam(value = "数据唯一id信息") @RequestParam(value = "id", required = true) Long id,
            @ApiParam(value = "pb包信息") @RequestParam(value = "pbJarVersion", required = true) String pbJarVersion,
            @ApiParam(value = "pbClass名") @RequestParam(value = "pbClassName", required = true) String pbClassName,
            @ApiParam(value = "删除或者恢复状态：1-恢复 2-删除") @RequestParam(value = "status", required = true)Integer status) {

        ResponseObject<Object> result = ResponseObject.ok();

        // 删除标记 pblcass 信息
        adDasSchemaModBiz.updateStatus(CallContext.getOperator(), id, pbJarVersion, pbClassName, status);

        return result;
    }

    @ApiOperation(value = "更新pbClass字段信息", notes = "更新pbClass字段信息")
    @ThreadNamePattern
    @RequestMapping(value = "/updatePbClassColumn", method = RequestMethod.POST)
    public ResponseObject<Object> updatePbClassColumn(@RequestBody AdDasPbClassColumnParam param) {

        ResponseObject<Object> result = ResponseObject.ok();

        adDasSchemaModBiz.updatePbClassColumn(CallContext.getOperator(), param);

        return result;
    }
}
