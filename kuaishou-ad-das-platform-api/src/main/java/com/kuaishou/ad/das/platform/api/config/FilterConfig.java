package com.kuaishou.ad.das.platform.api.config;

import java.util.Map;
import java.util.Objects;

import javax.servlet.Filter;

import org.jasig.cas.client.session.SingleSignOutFilter;
import org.jasig.cas.client.util.HttpServletRequestWrapperFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.filter.CharacterEncodingFilter;
import org.springframework.web.filter.RequestContextFilter;

import com.google.common.collect.Maps;
import com.kuaishou.ad.das.platform.api.common.filter.AdDasAuthFilter;
import com.kuaishou.ad.das.platform.api.common.filter.AdDasTicketValidationFilter;
import com.kuaishou.ad.das.platform.api.common.util.AdDasApiEnvUtils;
import com.kuaishou.ad.das.platform.component.web.config.AdDasApiPropertiesConfig;
import com.kuaishou.filter.ScopeFilter;
import com.kuaishou.filter.ServiceExceptionFilter;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-09-26
 */
@Configuration
public class FilterConfig {

    @Autowired
    private AdDasApiPropertiesConfig adDasApiPropertiesConfig;

    private void registerUrl(FilterRegistrationBean<?> bean) {
        // 不配置 /* 的原因是，我们不想把自检的url经过cas过滤器
        // 这里需要根据你自己的项目进行修改
        //  healthCheck|init|v2/api-docs|swagger-ui.html|swagger-resources|webjars
        bean.addUrlPatterns("/das/*");
    }

    @Bean
    public FilterRegistrationBean<SingleSignOutFilter> casSingleSignOutFilter() {
        SingleSignOutFilter filter = new SingleSignOutFilter();
        FilterRegistrationBean bean = new FilterRegistrationBean<>(filter);
        bean.setName("CAS Single Sign Out Filter");
        bean.addInitParameter("casServerUrlPrefix", "https://sso.corp.kuaishou.com/cas");
        registerUrl(bean);
        return bean;
    }

    @Bean
    public FilterRegistrationBean<AdDasAuthFilter> casAuthenticationFilter() {
        AdDasAuthFilter filter = new AdDasAuthFilter();
        FilterRegistrationBean<AdDasAuthFilter> bean = new FilterRegistrationBean<>(filter);
        bean.setName("CAS AdDasAuth Filter");
        bean.addInitParameter("casServerLoginUrl", "https://sso.corp.kuaishou.com/cas/login");
        // 此处配置客户端地址（即项目访问地址，每个下游系统不一样，可以用test.corp.kuaishou.com来本地测试
        bean.addInitParameter("serverName", AdDasApiEnvUtils.getUrlBase());
        registerUrl(bean);
        return bean;
    }

//     ticket过滤器，这里有两个属性无法设置
    @Bean
    public FilterRegistrationBean<AdDasTicketValidationFilter> casValidationFilter() {
        AdDasTicketValidationFilter filter = new AdDasTicketValidationFilter();
        FilterRegistrationBean<AdDasTicketValidationFilter> bean = new FilterRegistrationBean<>(filter);
        bean.setName("CAS Validation Filter");
        bean.addInitParameter("casServerUrlPrefix", "https://sso.corp.kuaishou.com/cas");
        // 此处配置客户端地址（即项目访问地址，每个下游系统不一样，可以用test.corp.kuaishou.com来本地测试
        bean.addInitParameter("serverName", AdDasApiEnvUtils.getUrlBase());
        bean.addInitParameter("redirectAfterValidation", "true");
        bean.addInitParameter("useSession", "true");
        bean.addInitParameter("authn_method", "mfa-duo");
        registerUrl(bean);
        return bean;
    }

    @Bean
    public FilterRegistrationBean<HttpServletRequestWrapperFilter> httpServletRequestWrapperFilter() {
        return buildFilter(new HttpServletRequestWrapperFilter(), 4, "httpServletRequestWrapperFilter");
    }

    /**
     * 用于设置一些Scope全局变量
     * @return FilterRegistrationBean<ScopeFilter>
     */
    @Bean
    public FilterRegistrationBean<ScopeFilter> scopeFilter() {
        ScopeFilter filter = new ScopeFilter();
        return buildFilter(filter, 5, "scopeFilter");
    }

    /**
     * 用于处理ServiceException
     * @return FilterRegistrationBean<ServiceExceptionFilter>
     */
    @Bean
    public FilterRegistrationBean<ServiceExceptionFilter> serviceExceptionFilter() {
        ServiceExceptionFilter filter = new ServiceExceptionFilter();
        return buildFilter(filter, 6, "serviceExceptionFilter");
    }

    /**
     * 设置request的编码
     * @return FilterRegistrationBean<CharacterEncodingFilter>
     */
    @Bean
    @ConditionalOnMissingBean
    public FilterRegistrationBean<CharacterEncodingFilter> characterEncodingFilter() {
        CharacterEncodingFilter filter = new CharacterEncodingFilter();
        filter.setForceEncoding(false);
        filter.setEncoding("utf-8");
        return buildFilter(filter, 7, "characterEncodingFilter");
    }

    /**
     * 初始化一些ContextHolder
     * @return FilterRegistrationBean<RequestContextFilter>
     */
    @Bean
    public FilterRegistrationBean<RequestContextFilter> requestContextFilter() {
        RequestContextFilter filter = new RequestContextFilter();
        filter.setThreadContextInheritable(true);
        return buildFilter(filter, 8, "requestContextFilter");
    }

    private <T extends Filter> FilterRegistrationBean<T> buildFilter(T filterInstance, int order, String name) {
        return buildFilter(filterInstance, order, name, new String[]{"/*"}, Maps.newHashMap());
    }

    private <T extends Filter> FilterRegistrationBean<T> buildFilter(T filterInstance, int order, String name,
                                                                     String[] urlPatterns, Map<String, String> initParams) {
        FilterRegistrationBean<T> filterRegistrationBean = new FilterRegistrationBean<>();
        filterRegistrationBean.setFilter(filterInstance);
        filterRegistrationBean.setOrder(order);
        filterRegistrationBean.setName(name);
        filterRegistrationBean.addUrlPatterns(urlPatterns);
        if (Objects.nonNull(initParams) && !initParams.isEmpty()) {
            initParams.forEach(filterRegistrationBean::addInitParameter);
        }
        return filterRegistrationBean;
    }


}
