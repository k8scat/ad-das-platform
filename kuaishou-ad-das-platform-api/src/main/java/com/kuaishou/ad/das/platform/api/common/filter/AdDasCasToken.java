package com.kuaishou.ad.das.platform.api.common.filter;

import lombok.Data;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-09-26
 */
@Data
public class AdDasCasToken {

    private String serviceId;
    private String loginName;
    private long loginTime;
    private String mail;
    private String displayName;
}
