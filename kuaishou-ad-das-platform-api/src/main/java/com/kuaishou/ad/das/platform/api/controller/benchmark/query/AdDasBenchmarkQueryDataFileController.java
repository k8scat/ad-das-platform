package com.kuaishou.ad.das.platform.api.controller.benchmark.query;

import static com.kuaishou.ad.das.platform.utils.constant.AdDasApiConstant.DAS_PLATFORM_API_PATH;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.kuaishou.ad.das.platform.component.biz.benchmark.AdDasBenchmarkQueryBiz;
import com.kuaishou.ad.das.platform.component.web.ResponseObject;
import com.kuaishou.ad.das.platform.component.web.view.AdDasBenchmarkFileData;
import com.kuaishou.ad.das.platform.component.web.view.AdDasBenchmarkFileDatasView;
import com.kuaishou.framework.web.annotation.ThreadNamePattern;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;

/**
 * 查询基准导出文件相关接口
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-09-26
 */
@Api(value = "查询基准导出文件相关接口")
@Slf4j
@RequestMapping(DAS_PLATFORM_API_PATH + "/benchmark/query")
@RestController
public class AdDasBenchmarkQueryDataFileController {

    @Autowired
    private AdDasBenchmarkQueryBiz adDasQueryDataBiz;

    @ApiOperation(value = "查询基准 文件信息", notes = "文件列表信息，也是文件选择下拉列表数据源")
    @ThreadNamePattern
    @GetMapping(value = "/queryBenchmarkFiles")
    public ResponseObject<AdDasBenchmarkFileDatasView> queryBenchmarkFiles(
            @ApiParam(value = "整点时间戳") @RequestParam(value = "taskTimestamp") Long taskTimestamp,
            @ApiParam(value = "基准类型") @RequestParam(value = "benchmarkType", required = false) String benchmarkType,
            @ApiParam(value = "基准流id") @RequestParam(value = "benchmarkId", required = false) Long benchmarkId) {

        ResponseObject<AdDasBenchmarkFileDatasView> result = ResponseObject.ok();
        AdDasBenchmarkFileDatasView dasBenchmarkFileDataView = new AdDasBenchmarkFileDatasView();
        // 查询路径下
        List<AdDasBenchmarkFileData> adDasBenchmarkFileDtoList = adDasQueryDataBiz
                .queryBenchmarkFiles(taskTimestamp, benchmarkType, benchmarkId);
        dasBenchmarkFileDataView.setDataList(adDasBenchmarkFileDtoList);
        result.setData(dasBenchmarkFileDataView);
        return result;
    }

    @ApiOperation(value = "查询dumpInfo信息", notes = "Data 中是JSON 字符串")
    @ThreadNamePattern
    @GetMapping(value = "/queryDumpInfo")
    public ResponseObject<String> queryDumpInfo(
            @ApiParam(value = "整点时间戳") @RequestParam(value = "taskTimestamp") Long taskTimestamp,
            @ApiParam(value = "基准类型") @RequestParam(value = "benchmarkType", required = false) String benchmarkType,
            @ApiParam(value = "基准流id") @RequestParam(value = "benchmarkId", required = false) Long benchmarkId) {
        ResponseObject<String> result = ResponseObject.ok();
        String dumpInfo = adDasQueryDataBiz.queryDumpInfo(taskTimestamp, benchmarkType, benchmarkId);
        result.setData(dumpInfo);
        return result;
    }

}
