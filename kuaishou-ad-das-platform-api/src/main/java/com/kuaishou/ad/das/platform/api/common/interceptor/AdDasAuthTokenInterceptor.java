package com.kuaishou.ad.das.platform.api.common.interceptor;

import static com.kuaishou.ad.das.platform.utils.constant.AdDasCommonConstant.AD_DAS_TOKEN_MAP;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.util.StringUtils;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;

import com.google.common.collect.Sets;
import com.kuaishou.ad.das.platform.utils.AdDasAuthTokenUtils;
import com.kuaishou.ad.das.platform.utils.annotation.AdDasAuthToken;
import com.kuaishou.ad.das.platform.utils.exception.ServiceExceptionUtils;
import com.kuaishou.framework.util.HostInfo;
import com.kuaishou.kconf.client.Kconf;
import com.kuaishou.kconf.client.Kconfs;

import lombok.extern.slf4j.Slf4j;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-12-14
 */
@Slf4j
public class AdDasAuthTokenInterceptor implements HandlerInterceptor {

    private static final Kconf<Set<String>> IP_EXEMPT_LIST = Kconfs.ofStringSet(
            "ad.adPlatform.ipExemptSet", Sets.newHashSet()).build();

    private static final Kconf<Map<String, Set<String>>> AD_DAS_TOKEN_MAP_KCONF = Kconfs
            .ofSetMap("ad.adcore.adDasTokenMap", AD_DAS_TOKEN_MAP,
                    String.class, String.class).build();

    public boolean preHandle(HttpServletRequest request, HttpServletResponse response,
            Object handler) throws Exception {

        HandlerMethod handlerMethod = (HandlerMethod) handler;
        AdDasAuthToken adDasAuthToken = handlerMethod.getMethodAnnotation(AdDasAuthToken.class);

        if (adDasAuthToken == null) {
            return true;
        }

        String tokenName = adDasAuthToken.tokenName();

        if (StringUtils.isEmpty(tokenName) && AD_DAS_TOKEN_MAP_KCONF.get().containsKey(tokenName)) {
            throw ServiceExceptionUtils.ofMessage("Permission denied");
        }

        Set<String> tokenSet = AD_DAS_TOKEN_MAP_KCONF.get().get(tokenName);

        String tokenValue = request.getParameter("dasToken");

        if (StringUtils.isEmpty(tokenValue)) {
            tokenValue = request.getHeader("dasToken");
        }

        log.info("【AdDasAuthTokenInterceptor】url={}, tokenValue={}", request.getRequestURL(), tokenValue);

        if (StringUtils.isEmpty(tokenValue)) {
            checkIpPermission(request);
            return true;
        } else {
            String dasToken = AdDasAuthTokenUtils.validateToken(tokenValue);
            log.info("【AdDasAuthTokenInterceptor】url={}, dasToken={}", request.getRequestURL(), dasToken);

            if (tokenSet.contains(dasToken)) {
                return true;
            } else {
                throw ServiceExceptionUtils.ofMessage("dasToken validate");
            }
        }

    }

    private void checkIpPermission(HttpServletRequest request) {

        if (HostInfo.debugHost()) {
            printHeaders(request);
        }

        String ipV4 = getIp(request);

        if (StringUtils.isEmpty(ipV4)) {
            throw ServiceExceptionUtils.ofMessage("Permission denied");
        }
        log.info("IPV4:{}", ipV4);
        if (!HostInfo.debugHost() && !IP_EXEMPT_LIST.get().contains(ipV4)) {
            throw ServiceExceptionUtils.ofMessage("Permission denied");
        }
    }

    private String getIp(HttpServletRequest request) {

        String ksClientIp = request.getHeader("x-ksclient-ip");

        if (StringUtils.isEmpty(ksClientIp)) {
            ksClientIp = request.getHeader("x-real-ip");
        }

        return ksClientIp;
    }

    private void printHeaders(HttpServletRequest request) {

        Map<String, String> header = new HashMap<String, String>();

        Enumeration<String> headerNames = request.getHeaderNames();
        log.info("printHeaders() headerNames={}", headerNames);

        while (headerNames.hasMoreElements()) {
            //获取每个请求头名称
            String headerName = headerNames.nextElement();
            //跟距请求头获取请求值
            String value = request.getHeader(headerName);
            header.put(headerName, value);
        }

        log.info("printHeaders() header={}", header);
    }


}
