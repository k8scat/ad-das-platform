package com.kuaishou.ad.das.platform.api.controller.common;

import static com.kuaishou.ad.das.platform.utils.constant.AdDasApiConstant.DAS_PLATFORM_API_PATH;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.kuaishou.ad.das.platform.component.biz.benchmark.AdDasBenchmarkModifyBiz;
import com.kuaishou.ad.das.platform.component.biz.increment.AdDasIncrementModifyModBiz;
import com.kuaishou.ad.das.platform.component.biz.streamgroup.DataStreamGroupBiz;
import com.kuaishou.ad.das.platform.component.web.ResponseObject;
import com.kuaishou.ad.das.platform.component.web.param.AdDasEditBenchmarkStreamParam;
import com.kuaishou.ad.das.platform.component.web.param.AdDasEditIncrementServiceParam;
import com.kuaishou.ad.das.platform.component.web.param.AdDasEditIncrementStreamParam;
import com.kuaishou.ad.das.platform.component.web.param.datastream.DataStreamGroupModifyParam;
import com.kuaishou.framework.web.annotation.ThreadNamePattern;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-08-01
 */
@Api(value = "das通用信息相关接口")
@Slf4j
@RequestMapping(DAS_PLATFORM_API_PATH + "/admin/common")
@RestController
public class AdDasAdminCommonController {

    @Autowired
    private DataStreamGroupBiz dataStreamGroupBiz;

    @Autowired
    private AdDasBenchmarkModifyBiz benchmarkModifyBiz;

    @Autowired
    private AdDasIncrementModifyModBiz incrementModifyModBiz;

    @ApiOperation(value = "添加数据流", notes = "添加数据流")
    @ThreadNamePattern
    @PostMapping(value = "/addDataStream")
    public ResponseObject<Object> addDataStream(@ApiParam @RequestBody DataStreamGroupModifyParam param) {
        ResponseObject<Object> result = ResponseObject.ok();
        dataStreamGroupBiz.mod(param);
        return result;
    }

    @ApiOperation(value = "删除数据流", notes = "删除数据流")
    @ThreadNamePattern
    @PostMapping(value = "/delDataStream")
    public ResponseObject<Object> delDataStream(@RequestParam(value = "dataId", required = true) Long dataId) {
        ResponseObject<Object> result = ResponseObject.ok();
        dataStreamGroupBiz.deleteById(dataId);
        return result;
    }

    @ApiOperation(value = "添加基准流", notes = "添加基准流")
    @ThreadNamePattern
    @PostMapping(value = "/addBenchmarkStream")
    public ResponseObject<Object> addBenchmarkStream(@ApiParam @RequestBody AdDasEditBenchmarkStreamParam param) {
        ResponseObject<Object> result = ResponseObject.ok();
        benchmarkModifyBiz.mod(param);
        return result;
    }

    @ApiOperation(value = "删除基准流", notes = "删除基准流")
    @ThreadNamePattern
    @PostMapping(value = "/delBenchmarkStream")
    public ResponseObject<Object> delBenchmarkStream(@RequestParam(value = "dataId", required = true) Long dataId) {
        ResponseObject<Object> result = ResponseObject.ok();

        benchmarkModifyBiz.deleteById(dataId);
        return result;
    }

    @ApiOperation(value = "添加增量流", notes = "添加增量流")
    @ThreadNamePattern
    @PostMapping(value = "/addIncrementStream")
    public ResponseObject<Object> addIncrementStream(@ApiParam @RequestBody AdDasEditIncrementStreamParam param) {
        ResponseObject<Object> result = ResponseObject.ok();
        incrementModifyModBiz.mod(param);
        return result;
    }

    @ApiOperation(value = "删除增量流", notes = "删除增量流")
    @ThreadNamePattern
    @PostMapping(value = "/delIncrementStream")
    public ResponseObject<Object> delIncrementStream(@RequestParam(value = "dataId", required = true) Long dataId) {
        ResponseObject<Object> result = ResponseObject.ok();
        incrementModifyModBiz.deleteById(dataId);
        return result;
    }

    @ApiOperation(value = "添加增量流服务信息", notes = "添加增量流服务信息")
    @ThreadNamePattern
    @PostMapping(value = "/addIncrementServiceInfo")
    public ResponseObject<Object> addIncrementServiceInfo(@ApiParam @RequestBody AdDasEditIncrementServiceParam param) {
        ResponseObject<Object> result = ResponseObject.ok();
        incrementModifyModBiz.modServiceInfo(param);
        return result;
    }

    @ApiOperation(value = "删除增量流服务信息", notes = "删除增量流服务信息")
    @ThreadNamePattern
    @PostMapping(value = "/delIncrementServiceInfo")
    public ResponseObject<Object> delIncrementServiceInfo(@RequestParam(value = "dataId", required = true) Long dataId) {
        ResponseObject<Object> result = ResponseObject.ok();
        incrementModifyModBiz.deleteServiceInfoById(dataId);
        return result;
    }
}
