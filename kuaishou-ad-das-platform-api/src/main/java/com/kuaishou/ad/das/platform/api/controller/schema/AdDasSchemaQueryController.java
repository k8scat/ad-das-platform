package com.kuaishou.ad.das.platform.api.controller.schema;

import static com.kuaishou.ad.das.platform.utils.constant.AdDasApiConstant.DAS_PLATFORM_API_PATH;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.kuaishou.ad.das.platform.component.biz.schema.AdDasSchemaQueryBiz;
import com.kuaishou.ad.das.platform.component.web.ResponseObject;
import com.kuaishou.ad.das.platform.component.web.view.AdDasSchemaHomePageView;
import com.kuaishou.ad.das.platform.component.web.view.AdDasSchemaPbClassView;
import com.kuaishou.ad.das.platform.component.web.view.AdDasSchemaPbPreviewView;
import com.kuaishou.ad.das.platform.component.web.view.AdDasSchemaPbView;
import com.kuaishou.ad.das.platform.component.web.view.AdDasStatusView;
import com.kuaishou.framework.web.annotation.ThreadNamePattern;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;

/**
 * das元信息查询接口
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-11-22
 */
@Api(value = "ad-das-schema-query-controller", description = "das元信息查询接口")
@Slf4j
@RequestMapping(DAS_PLATFORM_API_PATH + "/schema")
@RestController
public class AdDasSchemaQueryController {

    @Autowired
    private AdDasSchemaQueryBiz adDasSchemaQueryBiz;

    @ApiOperation(value = "元数据首页接口", notes = "展示当前服务使用的最新pb信息")
    @ThreadNamePattern
    @RequestMapping(value = "/schemaHomePage", method = RequestMethod.GET)
    public ResponseObject<AdDasSchemaHomePageView> schemaHomePage() {

        ResponseObject<AdDasSchemaHomePageView> result = ResponseObject.ok();

        AdDasSchemaHomePageView view = adDasSchemaQueryBiz.schemaHomePage();
        result.setData(view);

        return result;
    }

    @ApiOperation(value = "pb信息查询接口", notes = "展示当前最新的pb信息以及标记使用的class数据")
    @ThreadNamePattern
    @RequestMapping(value = "/queryLastestSchemaPb", method = RequestMethod.GET)
    public ResponseObject<String> queryLastestSchemaPb() {

        ResponseObject<String> result = ResponseObject.ok();

        String lastestPbVersion = adDasSchemaQueryBiz.queryLatestSchemaPb();
        result.setData(lastestPbVersion);

        return result;
    }

    @ApiOperation(value = "pb信息查询接口", notes = "展示当前最新的pb信息以及标记使用的class数据")
    @ThreadNamePattern
    @RequestMapping(value = "/querySchemaPb", method = RequestMethod.GET)
    public ResponseObject<AdDasSchemaPbView> querySchemaPb(
            @ApiParam(value = "pb包信息") @RequestParam(value = "pbJarVersion", required = false) String pbJarVersion,
            @ApiParam(value = "搜索Pb类") @RequestParam(value = "pbClass", required = false) String pbClass,
            @ApiParam(value = "pb状态；0-全部") @RequestParam(value = "pbStatus", required = true) Integer pbStatus) {

        ResponseObject<AdDasSchemaPbView> result = ResponseObject.ok();

        AdDasSchemaPbView view = adDasSchemaQueryBiz.querySchemaPb(pbJarVersion, pbClass, pbStatus);
        result.setData(view);

        return result;
    }

    @ApiOperation(value = "pbclass信息查询接口", notes = "展示指定pb class数据")
    @ThreadNamePattern
    @RequestMapping(value = "/querySchemaPbClass", method = RequestMethod.GET)
    public ResponseObject<List<AdDasSchemaPbClassView>> querySchemaPbClass(
            @ApiParam(value = "pb包信息") @RequestParam(value = "pbJarVersion", required = true) String pbJarVersion,
            @ApiParam(value = "pbclass名") @RequestParam(value = "pbClassName", required = true) String pbClassName) {

        ResponseObject<List<AdDasSchemaPbClassView>> result = ResponseObject.ok();

        List<AdDasSchemaPbClassView> views = adDasSchemaQueryBiz.querySchemaPbClass(pbJarVersion, pbClassName);
        result.setData(views);

        return result;
    }

    @ApiOperation(value = "预加载pb信息", notes = "预览指定Pb包信息数据")
    @ThreadNamePattern
    @RequestMapping(value = "/previewPbJar", method = RequestMethod.GET)
    public ResponseObject<List<AdDasSchemaPbPreviewView>> previewPbJar(
            @ApiParam(value = "pb包信息") @RequestParam(value = "pbJarVersion", required = true) String pbJarVersion) {

        ResponseObject<List<AdDasSchemaPbPreviewView>> result = ResponseObject.ok();

        List<AdDasSchemaPbPreviewView> views = adDasSchemaQueryBiz.previewPbJar(pbJarVersion);
        result.setData(views);

        return result;
    }

    @ApiOperation(value = "查询所有pb类列表接口", notes = "查询所有pb类列表接口")
    @ThreadNamePattern
    @RequestMapping(value = "/queryAllPbClass", method = RequestMethod.GET)
    public ResponseObject<List<String>> queryAllPbClass(
            @ApiParam(value = "pb包信息") @RequestParam(value = "pbJarVersion", required = true) String pbJarVersion) {

        ResponseObject<List<String>> result = ResponseObject.ok();

        List<String> view = adDasSchemaQueryBiz.queryClassList(pbJarVersion);
        result.setData(view);

        return result;
    }

    @ApiOperation(value = "查询服务列表接口", notes = "查询服务列表接口")
    @ThreadNamePattern
    @RequestMapping(value = "/queryServiceList", method = RequestMethod.GET)
    public ResponseObject<List<String>> queryServiceList() {

        ResponseObject<List<String>> result = ResponseObject.ok();

        List<String> view = adDasSchemaQueryBiz.queryServiceList(null);
        result.setData(view);

        return result;
    }

    @ApiOperation(value = "查询服务已标记的PB包信息", notes = "查询服务已标记的PB包信息")
    @ThreadNamePattern
    @RequestMapping(value = "/queryServicePb", method = RequestMethod.GET)
    public ResponseObject<String> queryServicePb(@RequestParam(value = "serviceName", required = true) String serviceName) {

        ResponseObject<String> result = ResponseObject.ok();

        String view = adDasSchemaQueryBiz.queryServicePb(serviceName);
        result.setData(view);

        return result;
    }

    @ApiOperation(value = "查询已标记的PB包版本列表", notes = "查询已标记的PB包版本列表")
    @ThreadNamePattern
    @RequestMapping(value = "/queryPbVersions", method = RequestMethod.GET)
    public ResponseObject<List<String>> queryPbVersions() {

        ResponseObject<List<String>> result = ResponseObject.ok();

        List<String> pbVersions = adDasSchemaQueryBiz.queryPbVersions();
        result.setData(pbVersions);

        return result;
    }

    @ApiOperation(value = "查询Pb状态枚举", notes = "查询Pb状态枚举")
    @ThreadNamePattern
    @RequestMapping(value = "/queryPbStatusEnum", method = RequestMethod.GET)
    public ResponseObject<List<AdDasStatusView>> queryPbStatusEnum() {

        ResponseObject<List<AdDasStatusView>> result = ResponseObject.ok();

        List<AdDasStatusView> statusViews = adDasSchemaQueryBiz.queryPbStatus();
        result.setData(statusViews);

        return result;
    }

}
