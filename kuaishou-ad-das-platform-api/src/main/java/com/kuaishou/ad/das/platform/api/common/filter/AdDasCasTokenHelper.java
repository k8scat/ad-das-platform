package com.kuaishou.ad.das.platform.api.common.filter;

import static com.kuaishou.ad.das.platform.utils.kconf.AdDasPlatformKconf.adDasPlatformUserMsgs;
import static com.kuaishou.framework.util.ObjectMapperUtils.toJSON;
import static java.lang.System.currentTimeMillis;
import static java.nio.charset.Charset.forName;
import static org.apache.commons.lang3.StringUtils.isNotEmpty;
import static org.apache.commons.lang3.math.NumberUtils.toLong;
import static org.slf4j.LoggerFactory.getLogger;

import javax.annotation.Nullable;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.jasig.cas.client.validation.Assertion;
import org.slf4j.Logger;

import com.kuaishou.ad.das.platform.utils.constant.AdDasApiConstant;
import com.kuaishou.ad.das.platform.utils.model.AdDasUserView;
import com.kuaishou.env.util.EnvUtils;
import com.kuaishou.framework.util.CookieUtils;
import com.kuaishou.framework.util.ObjectMapperUtils;
import com.kuaishou.keycenter.client.ClientProtectionProvider;
import com.kuaishou.keycenter.security.CoderException;

import lombok.extern.slf4j.Slf4j;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-09-26
 */
@Slf4j
public class AdDasCasTokenHelper {
    private static final Logger LOGGER = getLogger(AdDasCasTokenHelper.class);

    private static String casTokenName = "LOGINSTATUS";
    private static String keyCenterKeyName;

    public static String getTokenName() {
        return casTokenName;
    }

    public static void setTokenName(String tokenName) {
        casTokenName = tokenName;
    }

    public static String getKeyCenterKeyName() {
        return keyCenterKeyName;
    }

    public static void setKeyCenterKeyName(String keyCenterKeyName) {
        AdDasCasTokenHelper.keyCenterKeyName = keyCenterKeyName;
    }

    public static void saveLoginCookie(Assertion assertion, HttpServletResponse response, String domain) throws CoderException {
        String casToken = createToken(assertion);
        CookieUtils.saveCookie(response, AdDasCasTokenHelper.getTokenName(), casToken, AdDasApiConstant.CAS_TOKEN_EXPIRE_TIME,
                null, "/", false);
    }

    public static void saveLogoutCookie(HttpServletResponse response, String domain) throws CoderException {
        CookieUtils.saveCookie(response, getTokenName(), createLogoutToken(),
                AdDasApiConstant.CAS_TOKEN_EXPIRE_TIME, null, "/", false);
    }

    //用户是否登录
    public static boolean isLogined(final HttpServletRequest request) {
        String loginName = getTokenElement(request, TokenElement.LOGIN_NAME);
        if (isNotEmpty(loginName) && !StringUtils.equals(loginName, AdDasApiConstant.CAS_LOGOUT_MARK) && !isTokenExpired(request)) {
            return true;
        }
        return false;
    }

    public static AdDasUserView getRemoteUser(HttpServletRequest request) {

        AdDasUserView adDasUserView = new AdDasUserView();

        String userName = getTokenElement(request, TokenElement.LOGIN_NAME);
        String mail = getTokenElement(request, TokenElement.MAIL);
        String displayName = getTokenElement(request, TokenElement.DISPLAY_NAME);

        adDasUserView.setUserName(userName);
        adDasUserView.setDisplayName(displayName);
        adDasUserView.setEmail(mail);
        adDasUserView.setRoleType(adDasPlatformUserMsgs.get().getOrDefault(userName, 0));

        return adDasUserView;
    }

    /**
     * 前提cookie值正确，检查登录时间是否过期
     * 过期：return true
     * 未过期：return false
     */
    public static boolean isTokenExpired(HttpServletRequest request) {
        String loginName = getTokenElement(request, TokenElement.LOGIN_NAME);
        String loginTime = getTokenElement(request, TokenElement.LOGIN_TIME);

        return !StringUtils.equals(loginName, AdDasApiConstant.CAS_LOGOUT_MARK)
                && currentTimeMillis() - toLong(loginTime) >= AdDasApiConstant.CAS_TOKEN_VALID_TIME;
    }

    private static String createToken(final Assertion assertion) throws CoderException {
        String serviceId = getTokenName();
        String loginName = assertion.getPrincipal().getName();
        AdDasCasToken casToken = new AdDasCasToken();
        casToken.setServiceId(serviceId);
        casToken.setLoginName(loginName);
        casToken.setLoginTime(currentTimeMillis());
        String email = (String) assertion.getPrincipal().getAttributes().get("mail");
        casToken.setMail(email);
        String displayName = (String) assertion.getPrincipal().getAttributes().get("displayName");
        casToken.setDisplayName(displayName);

        log.info("action=CreateToken serviceId={} loginName={}", serviceId, loginName);
        return getEncryptResult(casToken);
    }

    private static String createLogoutToken() throws CoderException {
        String serviceId = getTokenName();
        AdDasCasToken casToken = new AdDasCasToken();
        casToken.setServiceId(serviceId);
        casToken.setLoginName(AdDasApiConstant.CAS_LOGOUT_MARK);
        casToken.setLoginTime(currentTimeMillis());
        return getEncryptResult(casToken);
    }


    private static boolean isVerifiedCookie(String cookieValue) {
        if (StringUtils.isEmpty(cookieValue)) {
            return false;
        }
        try {
            return getDecryptResult(cookieValue) != null;
        } catch (Exception e) {
            LOGGER.info("对cookie值：{}，验证失败。", cookieValue, e);
        }
        return false;
    }

    @Nullable
    public static String getTokenElement(final HttpServletRequest request, TokenElement tokenElement) {
        try {
            String cookieValue = CookieUtils.getCookie(request, getTokenName());
            if (StringUtils.isEmpty(cookieValue) || !isVerifiedCookie(cookieValue)) {
                return null;
            }

            AdDasCasToken casToken = getDecryptResult(cookieValue);
            if (casToken != null) {
                switch (tokenElement) {
                    case SERVICE_ID:
                        return casToken.getServiceId();
                    case LOGIN_NAME:
                        //如果获取的登录用户名称为LOGOUT_NAME，说明用户没有登录
                        log.debug("action=getTokenElement loginName={}", casToken.getLoginName());
                        if (StringUtils.equals(casToken.getLoginName(), AdDasApiConstant.CAS_LOGOUT_MARK)) {
                            return null;
                        }
                        return casToken.getLoginName();
                    case LOGIN_TIME:
                        return String.valueOf(casToken.getLoginTime());
                    case MAIL:
                        return casToken.getMail();
                    case DISPLAY_NAME:
                        return casToken.getDisplayName();
                    default:
                        break;
                }
            }
        } catch (Exception e) {
            LOGGER.info("json to bean error", e);
        }
        return null;
    }

    private static String getEncryptResult(AdDasCasToken token) throws CoderException {

        if (EnvUtils.isLocal()) {
            log.info("getEncryptResult() staging token={}", token);
            byte[] tmpBytes = toJSON(token).getBytes(forName("UTF-8"));
            return Base64.encodeBase64String(tmpBytes);
        }

        byte[] kcEncryptString = ClientProtectionProvider.getProvider(getKeyCenterKeyName())
                .encrypt(toJSON(token).getBytes(forName("UTF-8")));
        return Base64.encodeBase64String(kcEncryptString);
    }

    @Nullable
    private static AdDasCasToken getDecryptResult(String encryptString) {
        try {

            if (EnvUtils.isLocal()) {
                log.info("getDecryptResult() encryptString={}", encryptString);
                byte[] tmpBytes = Base64.decodeBase64(encryptString);

                log.info("getDecryptResult() encryptStr={}", new String(tmpBytes));
                return ObjectMapperUtils.fromJSON(new String(tmpBytes), AdDasCasToken.class);
            }

            byte[] tmpBytes = Base64.decodeBase64(encryptString);
            String rawString = new String(ClientProtectionProvider.getProvider(getKeyCenterKeyName()).decrypt(tmpBytes));
            if (StringUtils.isEmpty(rawString)) {
                return null;
            } else {
                return ObjectMapperUtils.fromJSON(rawString, AdDasCasToken.class);
            }
        } catch (Exception e) {
            LOGGER.info("cookie值解码失败。", e);
            return null;
        }
    }

    /**
     * cookie存储的值名
     * */
    public enum TokenElement {
        SERVICE_ID, LOGIN_NAME, LOGIN_TIME, MAIL, DISPLAY_NAME;
    }
}