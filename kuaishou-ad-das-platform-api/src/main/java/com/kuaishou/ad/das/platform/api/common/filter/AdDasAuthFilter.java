package com.kuaishou.ad.das.platform.api.common.filter;

import java.io.IOException;

import javax.annotation.PostConstruct;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.jasig.cas.client.Protocol;
import org.jasig.cas.client.authentication.AuthenticationRedirectStrategy;
import org.jasig.cas.client.authentication.DefaultAuthenticationRedirectStrategy;
import org.jasig.cas.client.authentication.DefaultGatewayResolverImpl;
import org.jasig.cas.client.authentication.GatewayResolver;
import org.jasig.cas.client.authentication.RegexUrlPatternMatcherStrategy;
import org.jasig.cas.client.authentication.UrlPatternMatcherStrategy;
import org.jasig.cas.client.configuration.ConfigurationKeys;
import org.jasig.cas.client.util.AbstractCasFilter;
import org.jasig.cas.client.util.CommonUtils;
import org.jasig.cas.client.util.ReflectUtils;
import org.springframework.context.annotation.Lazy;

import com.kuaishou.ad.das.platform.api.common.util.AdDasApiEnvUtils;
import com.kuaishou.env.util.EnvUtils;
import com.kuaishou.framework.util.CookieUtils;

import lombok.extern.slf4j.Slf4j;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-09-26
 */
@Slf4j
@Lazy
public class AdDasAuthFilter extends AbstractCasFilter {

    /**
     * The URL to the CAS Server login.
     */
    private String casServerLoginUrl = "https://sso.corp.kuaishou.com/cas/login";

    /**
     * Whether to send the renew request or not.
     */
    private boolean renew = false;

    /**
     * Whether to send the gateway request or not.
     */
    private boolean gateway = false;

    private String ignorePattern = "(healthCheck|init|v2/api-docs|swagger-ui.html|swagger-resources|webjars)";

    private String websiteUrl = AdDasApiEnvUtils.getUrlBase();

    private static final String CAS_TOKEN_NAME = "LOGINSTATUS";

    private static final String KEY_CENTER_KEY_NAME = "ad_das_api.user_cookie";

    @PostConstruct
    public void initParam() {
        log.info("websiteUrl: {} ignorePattern: {}", websiteUrl, ignorePattern);
    }

    private GatewayResolver gatewayStorage = new DefaultGatewayResolverImpl();

    private AuthenticationRedirectStrategy authenticationRedirectStrategy = new DefaultAuthenticationRedirectStrategy();

    private UrlPatternMatcherStrategy ignoreUrlPatternMatcherStrategyClass = new RegexUrlPatternMatcherStrategy();

    public AdDasAuthFilter() {
        this(Protocol.CAS2);
    }

    protected AdDasAuthFilter(final Protocol protocol) {
        super(protocol);
    }

    @Override
    protected void initInternal(final FilterConfig filterConfig) throws ServletException {
        if (!isIgnoreInitConfiguration()) {
            super.initInternal(filterConfig);

            this.ignoreUrlPatternMatcherStrategyClass.setPattern(getIgnorePattern());
            setServerName(getWebsiteUrl());

            logger.info(
                    "action=AuthFilterInit ignorePattern={} casLoginUrl={} homePageUrl={} caseTokenName={}, "
                            + "keyCenterKeyName={}",
                    getIgnorePattern(), casServerLoginUrl, websiteUrl, CAS_TOKEN_NAME,
                    KEY_CENTER_KEY_NAME);

            final Class<? extends GatewayResolver> gatewayStorageClass = getClass(
                    ConfigurationKeys.GATEWAY_STORAGE_CLASS);

            if (gatewayStorageClass != null) {
                setGatewayStorage(ReflectUtils.newInstance(gatewayStorageClass));
            }

            final Class<? extends AuthenticationRedirectStrategy> authenticationRedirectStrategyClass = getClass(
                    ConfigurationKeys.AUTHENTICATION_REDIRECT_STRATEGY_CLASS);

            if (authenticationRedirectStrategyClass != null) {
                this.authenticationRedirectStrategy = ReflectUtils
                        .newInstance(authenticationRedirectStrategyClass);
            }

            AdDasCasTokenHelper.setKeyCenterKeyName(KEY_CENTER_KEY_NAME);
            if (StringUtils.isNotEmpty(CAS_TOKEN_NAME)) {
                AdDasCasTokenHelper.setTokenName(CAS_TOKEN_NAME);
            }
        }
    }

    @Override
    public void init() {
        super.init();
        CommonUtils.assertNotNull(this.casServerLoginUrl, "casServerLoginUrl cannot be null.");
    }

    @Override
    public final void doFilter(final ServletRequest servletRequest,
                               final ServletResponse servletResponse, final FilterChain filterChain)
            throws IOException, ServletException {

        final HttpServletRequest request = (HttpServletRequest) servletRequest;
        final HttpServletResponse response = (HttpServletResponse) servletResponse;

        if (isRequestUrlExcluded(request) || EnvUtils.isLocal()) {
            logger.debug("Request is ignored.");
            filterChain.doFilter(request, response);
            return;
        }

        // 如果已登录，直接跳过
        log.debug("action=AuthFilter request={} cookie={}", request.getRequestURI(),
                CookieUtils.getCookie(request, AdDasCasTokenHelper.getTokenName()));
        if (AdDasCasTokenHelper.isLogined(request)) {
            CallContext.setOperator(AdDasCasTokenHelper.getRemoteUser(request));
            filterChain.doFilter(request, response);
            CallContext.clean();
            return;
        }

        final String serviceUrl = constructServiceUrl(request, response);
        final String ticket = retrieveTicketFromRequest(request);
        final boolean wasGatewayed = this.gateway
                && this.gatewayStorage.hasGatewayedAlready(request, serviceUrl);

        if (CommonUtils.isNotBlank(ticket) || wasGatewayed) {
            filterChain.doFilter(request, response);
            return;
        }
        final String modifiedServiceUrl;

        logger.debug("no ticket and no assertion found");
        if (this.gateway) {
            logger.debug("setting gateway attribute in session");
            modifiedServiceUrl = this.gatewayStorage.storeGatewayInformation(request, serviceUrl);
        } else {
            modifiedServiceUrl = serviceUrl;
        }

        logger.debug("Constructed service url: {}", modifiedServiceUrl);

        final String urlToRedirectTo = CommonUtils.constructRedirectUrl(this.casServerLoginUrl,
                getProtocol().getServiceParameterName(), modifiedServiceUrl, this.renew,
                this.gateway);

        logger.debug("redirecting to \"{}\"", urlToRedirectTo);
        this.authenticationRedirectStrategy.redirect(request, response, urlToRedirectTo);
    }

    public final void setRenew(final boolean renew) {
        this.renew = renew;
    }

    public final void setGateway(final boolean gateway) {
        this.gateway = gateway;
    }

    public final void setCasServerLoginUrl(final String casServerLoginUrl) {
        this.casServerLoginUrl = casServerLoginUrl;
    }

    public final void setGatewayStorage(final GatewayResolver gatewayStorage) {
        this.gatewayStorage = gatewayStorage;
    }

    private boolean isRequestUrlExcluded(final HttpServletRequest request) {
        if (this.ignoreUrlPatternMatcherStrategyClass == null) {
            return false;
        }

        final StringBuffer urlBuffer = request.getRequestURL();
        if (request.getQueryString() != null) {
            urlBuffer.append("?").append(request.getQueryString());
        }
        final String requestUri = urlBuffer.toString();
        return this.ignoreUrlPatternMatcherStrategyClass.matches(requestUri);
    }

    public String getIgnorePattern() {
        return ignorePattern;
    }

    public void setIgnorePattern(String ignorePattern) {
        this.ignorePattern = ignorePattern;
    }

    public String getWebsiteUrl() {
        return websiteUrl;
    }

    public void setWebsiteUrl(String websiteUrl) {
        this.websiteUrl = websiteUrl;
    }
}

