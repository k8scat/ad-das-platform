package com.kuaishou.ad.das.platform.api.common.util;

import org.springframework.util.StringUtils;

import com.kuaishou.kconf.client.Kconf;
import com.kuaishou.kconf.client.Kconfs;

import lombok.extern.slf4j.Slf4j;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-10-13
 */
@Slf4j
public class AdDasApiEnvUtils {

    private static String dasServiceEnv;  //online、beta、test、staging

    static {
        dasServiceEnv = System.getenv("DAS_SERVICE_ENV");  //online、beta、test、staging

    }

    private static final Kconf<String> AD_DAS_API_URL = Kconfs.ofString(
            "ad.adcore.adDasApiUrl", "https://addas.test.gifshow.com/").build();

    public static String getUrlBase() {

        if (com.kuaishou.env.util.EnvUtils.isLocal()) {

            return "http://test.corp.kuaishou.com/";
        }

        if ("beta".equals(dasServiceEnv)) {
            return "http://addas-beta.corp.kuaishou.com/";
        }

        String urlBase = AD_DAS_API_URL.get();

        log.info("CreateUrlByEnv getUrlBase() urlBase={}", urlBase);

        return urlBase;
    }
}
