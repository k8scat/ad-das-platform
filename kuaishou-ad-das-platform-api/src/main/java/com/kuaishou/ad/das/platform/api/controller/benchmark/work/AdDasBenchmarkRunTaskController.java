package com.kuaishou.ad.das.platform.api.controller.benchmark.work;

import static com.kuaishou.ad.das.platform.utils.constant.AdDasApiConstant.DAS_PLATFORM_API_PATH;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.kuaishou.framework.web.annotation.ThreadNamePattern;

import lombok.extern.slf4j.Slf4j;
import springfox.documentation.annotations.ApiIgnore;

/**
 * 触发任务执行相关接口
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-09-26
 */
@ApiIgnore
@Slf4j
@RequestMapping(DAS_PLATFORM_API_PATH + "/run")
@RestController
public class AdDasBenchmarkRunTaskController {

    /**
     * 触发beta任务执行
     * @return
     */
    @ThreadNamePattern
    @PostMapping(value = "/runTask2Beta")
    public Object runTask2Beta(@RequestParam(value = "delLock", required = false, defaultValue = "0") Integer delLock,
                               @RequestParam(value = "resourceIds") String resourceIds,
                               @RequestParam(value = "taskTimestamp") Long taskTimestamp,
                               @RequestParam(value = "masterTypeStr") String masterTypeStr) {

        // 检查执行时间
        List<Integer> resourceIdList = new ArrayList<>();
        if (!StringUtils.isEmpty(resourceIds)) {
            String[] resourceIdStrArr = resourceIds.split(",");
            for (String resourceIdStr : resourceIdStrArr) {
                resourceIdList.add(Integer.valueOf(resourceIdStr));
            }
        } else {
            return "参数错误";
        }
        // TODO: 使用新的基准任务触发的方式
        return null;
//        return adDasCheckDataBiz.runBetaCheckTask(delLock, resourceIdList, taskTimestamp, masterTypeStr);
    }

    /**
     * 获取beta运行任务信息
     * @return
     */
    @ThreadNamePattern
    @GetMapping(value = "/getBetaRunMsg")
    public Object getBetaRunMsg() {

        return "getBetaRunMsg";
    }


    /**
     * 检查beta运行后的执行结果
     * @param delLock
     * @return
     */
    @ThreadNamePattern
    @GetMapping(value = "/checkBetaResult")
    public Object checkBetaResult(@RequestParam(value = "delLock", required = false, defaultValue = "0") Integer delLock) {
        return "checkBetaResult";
    }
}
