package com.kuaishou.ad.das.platform.api.controller.finance;

import static com.kuaishou.ad.das.platform.utils.constant.AdDasApiConstant.CHARACTER_ENCODING;
import static com.kuaishou.ad.das.platform.utils.constant.AdDasApiConstant.DAS_PLATFORM_API_PATH;

import java.util.List;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.kuaishou.ad.das.platform.api.common.filter.CallContext;
import com.kuaishou.ad.das.platform.component.biz.finance.AdDasFinanceManagerBiz;
import com.kuaishou.ad.das.platform.utils.model.AdDasUserView;
import com.kuaishou.framework.web.annotation.ThreadNamePattern;
import com.kuaishou.util.DateTimeUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-08-06
 */
@Api(value = "ad-das-finance-controller", description = "财务管家接口")
@Slf4j
@RequestMapping(DAS_PLATFORM_API_PATH + "/finance")
@RestController
public class AdDasFinanceController {

    @Autowired
    private AdDasFinanceManagerBiz financeManagerBiz;

    @ApiOperation(value = "下载广告主实时余额数据")
    @ThreadNamePattern
    @GetMapping(value = "/downloadAccountBalance")
    public void downloadAccountBalance(@RequestParam(value = "accountIds", required = false) String accountIds,
                                 HttpServletResponse response) {

        AdDasUserView adDasUserView = CallContext.getOperator();
        List<byte[]> dataBytes = financeManagerBiz.downloadAccountBalanceData(accountIds, adDasUserView);

        String resFileName = String.format("AdDas_%s_%s.csv", "data_" + adDasUserView.getUserName(),
                DateTimeUtils.toLocalDateTime(System.currentTimeMillis()));
        response.setHeader("Content-Disposition",
                String.format("attachment;filename=%s", resFileName));
        response.setCharacterEncoding(CHARACTER_ENCODING);
        try {
            ServletOutputStream outputStream = response.getOutputStream();
            for (byte[] data : dataBytes) {
                outputStream.write(data);
            }
            outputStream.flush();
        } catch (Exception e) {
            log.error("downloadAccountBalance() occur error: accountIds={}", accountIds, e);
        }
    }
}
