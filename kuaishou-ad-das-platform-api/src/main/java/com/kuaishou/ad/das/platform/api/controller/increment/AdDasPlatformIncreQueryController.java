package com.kuaishou.ad.das.platform.api.controller.increment;

import static com.kuaishou.ad.das.platform.utils.constant.AdDasApiConstant.DAS_PLATFORM_API_PATH;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.kuaishou.ad.das.platform.component.biz.increment.AdDasIncrementModifyQueryBiz;
import com.kuaishou.ad.das.platform.component.web.ResponseObject;
import com.kuaishou.ad.das.platform.component.web.param.AdDasPlatformModifyParam;
import com.kuaishou.ad.das.platform.component.web.view.AdDasModifyDataListView;
import com.kuaishou.ad.das.platform.component.web.view.AdDasPlatformIncreDetailView;
import com.kuaishou.ad.das.platform.component.web.view.AdDasPlatformIncreSearchView;
import com.kuaishou.ad.das.platform.component.web.view.AdDasPlatformIncreTableDetailView;
import com.kuaishou.ad.das.platform.component.web.view.AdDasPlatformModifyIncrementView;
import com.kuaishou.framework.web.annotation.ThreadNamePattern;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-05-24
 */
@Api(value = "ad-das-platform-incre-query-controller", description = "开放平台增量流query相关接口")
@Slf4j
@RequestMapping(DAS_PLATFORM_API_PATH + "/increment")
@RestController
public class AdDasPlatformIncreQueryController {

    @Autowired
    private AdDasIncrementModifyQueryBiz incrementModifyQueryBiz;

    @ApiOperation(value = "数据流列表页增量查询接口", notes = "展示数据流组中的增量流信息")
    @ThreadNamePattern
    @RequestMapping(value = "/increSearch", method = RequestMethod.GET)
    public ResponseObject<AdDasPlatformIncreSearchView> increSearch(
            @RequestParam(value = "dataStreamGroupId", required = true) Long dataStreamGroupId,
            @RequestParam(value = "dataSource", required = false) String dataSource,
            @RequestParam(value = "table", required = false) String table,
            @RequestParam(value = "dataTopic", required = false) String dataTopic) {
        ResponseObject<AdDasPlatformIncreSearchView> result = ResponseObject.ok();

        AdDasPlatformIncreSearchView increSearchViews = incrementModifyQueryBiz
                .increSearch(dataStreamGroupId, dataSource, table, dataTopic);
        result.setData(increSearchViews);

        return result;
    }

    @ApiOperation(value = "增量流详情页查询接口", notes = "展示增量流详情信息")
    @ThreadNamePattern
    @RequestMapping(value = "/increDetail", method = RequestMethod.GET)
    public ResponseObject<AdDasPlatformIncreDetailView> increDetail(
            @RequestParam(value = "increStreamId", required = true) Long increStreamId,
            @RequestParam(value = "increStreamType", required = true, defaultValue = "1") Integer increStreamType) {
        ResponseObject<AdDasPlatformIncreDetailView> result = ResponseObject.ok();

        AdDasPlatformIncreDetailView increDetailViews = incrementModifyQueryBiz
                .increDetail(increStreamId, increStreamType);
        result.setData(increDetailViews);

        return result;
    }

    @ApiOperation(value = "增量流表详情页查询接口", notes = "展示增量流表详情信息")
    @ThreadNamePattern
    @RequestMapping(value = "/increTableDetail", method = RequestMethod.GET)
    public ResponseObject<AdDasPlatformIncreTableDetailView> increTableDetail(
            @RequestParam(value = "increStreamId", required = true) Long increStreamId,
            @RequestParam(value = "increStreamType", required = true, defaultValue = "1") Integer increStreamType,
            @RequestParam(value = "mainTable", required = true) String mainTable) {
        ResponseObject<AdDasPlatformIncreTableDetailView> result = ResponseObject.ok();

        AdDasPlatformIncreTableDetailView increTableDetailViews = incrementModifyQueryBiz
                .increTableDetail(increStreamId, increStreamType, mainTable);
        result.setData(increTableDetailViews);

        return result;
    }

    @ApiOperation(value = "增量流变更记录接口", notes = "展示增量流变更记录信息")
    @ThreadNamePattern
    @RequestMapping(value = "/increRecord", method = RequestMethod.GET)
    public ResponseObject<AdDasModifyDataListView> increRecord(AdDasPlatformModifyParam modifyParam) {
        ResponseObject<AdDasModifyDataListView> result = ResponseObject.ok();

        AdDasModifyDataListView modifyDataListView = incrementModifyQueryBiz.increRecord(modifyParam);
        result.setData(modifyDataListView);

        return result;
    }

    @ApiOperation(value = "查询变更增量详情", notes = "查询变更增量详情")
    @ThreadNamePattern
    @GetMapping(value = "/incrementModifyData")
    public ResponseObject<AdDasPlatformModifyIncrementView> incrementModifyData(
            @RequestParam Long modifyId, @RequestParam Integer modifyType) {
        ResponseObject<AdDasPlatformModifyIncrementView> result = ResponseObject.ok();

        AdDasPlatformModifyIncrementView incrementView =  incrementModifyQueryBiz
                .incrementModifyData(modifyId, modifyType);

        result.setData(incrementView);
        return result;
    }

}
