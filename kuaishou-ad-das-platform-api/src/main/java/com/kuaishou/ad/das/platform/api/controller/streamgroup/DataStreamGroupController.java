package com.kuaishou.ad.das.platform.api.controller.streamgroup;

import static com.kuaishou.ad.das.platform.utils.constant.AdDasApiConstant.DAS_API_DATA_STREAM_GROUP_PATH;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.kuaishou.ad.das.platform.component.biz.streamgroup.DataStreamGroupBiz;
import com.kuaishou.ad.das.platform.component.web.ResponseObject;
import com.kuaishou.ad.das.platform.component.web.param.datastream.DataStreamGroupModifyParam;
import com.kuaishou.ad.das.platform.component.web.param.datastream.DataStreamGroupQueryListParam;
import com.kuaishou.ad.das.platform.component.web.view.datastream.DataStreamGroupQueryView;
import com.kuaishou.framework.web.annotation.ThreadNamePattern;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;

/**
 * TODO
 * 1. 申请组权限
 * 2. 删除数据流组
 * 3. 创建新数据流组
 * @author Zhang Jialin <zhangjialin@kuaishou.com>
 * Created on 2021-05-17
 */
@Slf4j
@RestController
@RequestMapping(DAS_API_DATA_STREAM_GROUP_PATH)
@Api(value = "数据流组接口")
public class DataStreamGroupController {

    @Autowired
    private DataStreamGroupBiz dataStreamGroupBiz;

    @ApiOperation(value = "数据流组列表查询")
    @ThreadNamePattern
    @PostMapping(value = "/query/list")
    public ResponseObject<DataStreamGroupQueryView> queryList(@ApiParam @RequestBody DataStreamGroupQueryListParam param) {
        DataStreamGroupQueryView vo = dataStreamGroupBiz.queryList(param);
        return ResponseObject.ok(vo);
    }

    @ApiOperation(value = "数据流组更新")
    @PostMapping(value = "/modify/item")
    public ResponseObject<Object> mod(@ApiParam @RequestBody DataStreamGroupModifyParam param) {
        dataStreamGroupBiz.mod(param);
        return ResponseObject.ok();
    }


}
