package com.kuaishou.ad.das.platform.api.common.filter;


import com.kuaishou.ad.das.platform.utils.model.AdDasUserView;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-09-26
 */
public class CallContext {

    private static ThreadLocal<AdDasUserView> operator = new ThreadLocal<>();

    static {
        AdDasUserView adDasUserView = new AdDasUserView();
        adDasUserView.setUserName("wangkai14");
        operator.set(adDasUserView);
    }
    public static AdDasUserView getOperator() {
        return operator.get();
    }

    public static void setOperator(AdDasUserView operator) {
        CallContext.operator.set(operator);
    }

    public static void clean() {
        CallContext.operator.set(null);
    }
}
