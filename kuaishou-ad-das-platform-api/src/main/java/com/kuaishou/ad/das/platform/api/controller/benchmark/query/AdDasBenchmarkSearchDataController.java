package com.kuaishou.ad.das.platform.api.controller.benchmark.query;

import static com.kuaishou.ad.das.platform.utils.constant.AdDasApiConstant.CHARACTER_ENCODING;
import static com.kuaishou.ad.das.platform.utils.constant.AdDasApiConstant.DAS_PLATFORM_API_PATH;
import static com.kuaishou.ad.das.platform.utils.exception.ErrorCode.NEED_ID_PARAM_ERROR;

import java.util.Arrays;
import java.util.List;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.kuaishou.ad.das.platform.component.biz.benchmark.AdDasBenchmarkQueryBiz;
import com.kuaishou.ad.das.platform.component.web.ResponseObject;
import com.kuaishou.ad.das.platform.component.web.param.AdDasBenchmarkSearchDataParam;
import com.kuaishou.ad.das.platform.component.web.view.AdDasBenchmarkCheckDataView;
import com.kuaishou.ad.das.platform.component.web.view.AdDasBenchmarkData;
import com.kuaishou.ad.das.platform.component.web.view.AdDasBenchmarkDatasView;
import com.kuaishou.framework.util.ObjectMapperUtils;
import com.kuaishou.framework.web.annotation.ThreadNamePattern;
import com.kuaishou.util.DateTimeUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;

/**
 * 基准查询相关接口
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-09-26
 */
@Api(value = "基准查询相关接口")
@Slf4j
@RequestMapping(DAS_PLATFORM_API_PATH + "/search")
@RestController
public class AdDasBenchmarkSearchDataController {

    @Autowired
    private AdDasBenchmarkQueryBiz adDasQueryDataBiz;

    /**
     * 将执行结果reload到内存中 checkFile, 会展示数据实例， 成功后，再支持搜索
     */
    @ApiOperation(value = "查询dumpInfo信息", notes = "Data 中是JSON 字符串")
    @ThreadNamePattern
    @PostMapping(value = "/checkDataFile")
    public ResponseObject<AdDasBenchmarkCheckDataView> checkDataFile(@RequestBody AdDasBenchmarkSearchDataParam param) {
        log.info("param={}", ObjectMapperUtils.toJSON(param));

        ResponseObject<AdDasBenchmarkCheckDataView> result = ResponseObject.ok();
        // 获取指定路径下的文件数据
        AdDasBenchmarkCheckDataView dasBenchmarkCheckDataView = adDasQueryDataBiz.checkDataFile(param);
        result.setData(dasBenchmarkCheckDataView);
        return result;
    }

    @ApiOperation(value = "搜索导出的基准的指定的数据", notes = "Data 中是JSON 字符串")
    @ThreadNamePattern
    @PostMapping(value = "/searchData")
    public ResponseObject<AdDasBenchmarkDatasView> searchData(@RequestBody AdDasBenchmarkSearchDataParam param) {
        log.info("param={}", ObjectMapperUtils.toJSON(param));
        if (StringUtils.isEmpty(param.getIdKey()) || CollectionUtils.isEmpty(param.getIds())) {
            return ResponseObject.ofErrorCode(NEED_ID_PARAM_ERROR);
        }
        ResponseObject<AdDasBenchmarkDatasView> result = ResponseObject.ok();
        List<AdDasBenchmarkData> dasBenchmarkDataViews = adDasQueryDataBiz.searchData(param);
        AdDasBenchmarkDatasView dasBenchmarkDatasView = new AdDasBenchmarkDatasView();
        dasBenchmarkDatasView.setDataList(dasBenchmarkDataViews);
        result.setData(dasBenchmarkDatasView);
        return result;
    }

    @ApiOperation(value = "下载导出的基准的指定的数据")
    @ThreadNamePattern
    @GetMapping(value = "/downloadData")
    public void downloadFileData(@RequestParam(value = "delLock", required = false) Integer delLock,
                                 @RequestParam(value = "fileName") String fileName,
                                 @RequestParam(value = "timestamp") Long timestamp,
                                 @RequestParam(value = "benchmarkType") String benchmarkType,
                                 @RequestParam(value = "benchmarkId", required = false) Long benchmarkId,
                                 @ApiParam(value = "1,2,3") @RequestParam(value = "ids", required = false) String ids,
                                 @RequestParam(value = "idKey", required = false) String idKey,
                                 HttpServletResponse response) {
        String[] idStrs = ids.split(",");
        List<String> idList = Arrays.asList(idStrs);
        AdDasBenchmarkSearchDataParam param = new AdDasBenchmarkSearchDataParam();
        param.setBenchmarkType(benchmarkType);
        param.setDelLock(delLock);
        param.setFileName(fileName);
        param.setIdKey(idKey);
        param.setIds(idList);
        param.setTimestamp(timestamp);

        log.info("param={}", ObjectMapperUtils.toJSON(param));
        List<byte[]> dataBytes = adDasQueryDataBiz.downloadData(param);
        String resFileName = String.format("AdDas_%s_%s.csv", param.getFileName(),
                DateTimeUtils.toLocalDateTime(System.currentTimeMillis()));
        response.setHeader("Content-Disposition",
                String.format("attachment;filename=%s", resFileName));
        response.setCharacterEncoding(CHARACTER_ENCODING);
        try {
            ServletOutputStream outputStream = response.getOutputStream();
            for (byte[] data : dataBytes) {
                outputStream.write(data);
            }
            outputStream.flush();
        } catch (Exception e) {
            log.error("downloadFileData() occur error: param={}", param, e);
        }
    }
}
