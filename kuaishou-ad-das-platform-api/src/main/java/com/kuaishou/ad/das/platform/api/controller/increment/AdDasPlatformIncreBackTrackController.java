package com.kuaishou.ad.das.platform.api.controller.increment;

import static com.kuaishou.ad.das.platform.utils.constant.AdDasApiConstant.DAS_PLATFORM_API_PATH;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.kuaishou.ad.das.platform.component.biz.increment.AdDasIncrementBacktrackBiz;
import com.kuaishou.ad.das.platform.component.web.ResponseObject;
import com.kuaishou.ad.das.platform.component.web.param.AdDasIncrementBacktrackDataReSendParam;
import com.kuaishou.ad.das.platform.component.web.param.AdDasIncrementRecondDataReSendParam;
import com.kuaishou.ad.das.platform.component.web.param.AdDasIncrementRecondTableDataReSendParam;
import com.kuaishou.ad.das.platform.component.web.param.AdDasIncrementServiceDataReSendParam;
import com.kuaishou.ad.das.platform.component.web.param.AdDasIncrementServiceTableDataReSendParam;
import com.kuaishou.ad.das.platform.component.web.view.AdDasPlatformIncreBackRecondSearchView;
import com.kuaishou.ad.das.platform.component.web.view.AdDasPlatformIncreBackServiceSearchView;
import com.kuaishou.ad.das.platform.utils.annotation.AdDasRole;
import com.kuaishou.ad.das.platform.utils.model.AdDasUserView;
import com.kuaishou.framework.web.annotation.ThreadNamePattern;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;

/**
 * @author caoyinhang <caoyinhang@kuaishou.com>
 * Created on 2021-07-06
 */
@Api(value = "ad-das-platform-incr-backtrack-controller", description = "开放平台增量流回溯相关接口")
@Slf4j
@RequestMapping(DAS_PLATFORM_API_PATH + "/increment")
@RestController
public class AdDasPlatformIncreBackTrackController {

    @Autowired
    private AdDasIncrementBacktrackBiz incrementBacktrackBiz;

    @ApiOperation(value = "增量回溯服务表头查询", notes = "展示增量回溯服务表头")
    @ThreadNamePattern
    @RequestMapping(value = "/increBacktrackServiceTableSearch", method = RequestMethod.GET)
    public ResponseObject<AdDasIncrementServiceTableDataReSendParam> increBacktrackServiceTableSearch() {

        ResponseObject<AdDasIncrementServiceTableDataReSendParam> result = ResponseObject.ok();

        AdDasIncrementServiceTableDataReSendParam incrementServiceTableDataReSendParam = incrementBacktrackBiz.getServiceTableHead();
        result.setData(incrementServiceTableDataReSendParam);
        return result;
    }

    @ApiOperation(value = "增量回溯服务详情页查询接口", notes = "展示增量回溯服务详情页信息")
    @ThreadNamePattern
    @RequestMapping(value = "/increBacktrackServiceSearch", method = RequestMethod.POST)
    public ResponseObject<AdDasPlatformIncreBackServiceSearchView> increBacktrackServiceSearch(@ApiParam @RequestBody
            AdDasIncrementServiceDataReSendParam serviceDataReSendParam) {
        ResponseObject<AdDasPlatformIncreBackServiceSearchView> result = ResponseObject.ok();

        AdDasPlatformIncreBackServiceSearchView increSearchViews = incrementBacktrackBiz
                .increBacktrackServiceSearch(serviceDataReSendParam);
        result.setData(increSearchViews);
        return result;
    }

    @ApiOperation(value = "增量回溯调用接口", notes = "完成增量回溯功能")
    @ThreadNamePattern
    @RequestMapping(value = "/increBacktrack", method = RequestMethod.POST)
    public ResponseObject<String> increBacktrack(@ApiParam @RequestBody
            AdDasIncrementBacktrackDataReSendParam incrementBacktrackDataReSendParam,@AdDasRole AdDasUserView dasUserView) {
        ResponseObject<String> result = ResponseObject.ok();
        String status = incrementBacktrackBiz.backtrack(incrementBacktrackDataReSendParam,dasUserView);//调用接口实现回溯
        result.setData(status);
        return result;
    }

    @ApiOperation(value = "增量回溯记录表头查询", notes = "展示增量回溯记录表头")
    @ThreadNamePattern
    @RequestMapping(value = "/increBacktrackRecondTableSearch", method = RequestMethod.GET)
    public ResponseObject<AdDasIncrementRecondTableDataReSendParam> increBacktrackRecondTableSearch() {

        ResponseObject<AdDasIncrementRecondTableDataReSendParam> result = ResponseObject.ok();

        AdDasIncrementRecondTableDataReSendParam incrementRecondTableDataReSendParam = incrementBacktrackBiz.getRecondTableHead();
        result.setData(incrementRecondTableDataReSendParam);
        return result;
    }

    @ApiOperation(value = "增量回溯记录详情页查询接口", notes = "展示增量回溯记录详情页信息")
    @ThreadNamePattern
    @RequestMapping(value = "/increBacktrackRecondSearch", method = RequestMethod.POST)
    public ResponseObject<AdDasPlatformIncreBackRecondSearchView> increBacktrackRecondSearch(@ApiParam @RequestBody
            AdDasIncrementRecondDataReSendParam recondDataReSendParam){
        ResponseObject<AdDasPlatformIncreBackRecondSearchView> result = ResponseObject.ok();
        Integer statusR = 2;//初始化为2代表全部或者不选择
        if (recondDataReSendParam.getStatus()!=null){
            if(("成功").equals(recondDataReSendParam.getStatus()))
                statusR=1;
            if (("失败").equals(recondDataReSendParam.getStatus()))
                statusR=0;
        }
        AdDasPlatformIncreBackRecondSearchView increSearchViews = incrementBacktrackBiz
                .increBacktrackRecondSearch(recondDataReSendParam,statusR);
        result.setData(increSearchViews);
        return result;
    }


}
