##数据流表
CREATE TABLE `ad_das_data_stream_group` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '数据流组id',
  `data_stream_group_name` varchar(200) NOT NULL COMMENT '数据流组名称',
  `data_stream_group_desc` varchar(500) NOT NULL COMMENT '数据流组描述',
  `owner_email_prefix` varchar(200) NOT NULL COMMENT '接口人邮箱前缀',
  `owner_name` varchar(100) NOT NULL COMMENT '接口人中文名',
  `create_time` bigint(20) NOT NULL COMMENT '创建时间',
  `update_time` bigint(20) NOT NULL COMMENT '更新时间',
  `is_delete` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COMMENT='数据流组'

##基准流表
CREATE TABLE `ad_das_benchmark` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '基准id',
  `benchmark_name` varchar(200) NOT NULL COMMENT '基准名称',
  `benchmark_desc` varchar(2000) NOT NULL COMMENT '基准流描述',
  `cluster_name` varchar(200) NOT NULL COMMENT '集群名称',
  `data_stream_group_id` bigint(20) NOT NULL COMMENT '数据流组id',
  `hdfs_path` varchar(1000) NOT NULL COMMENT 'HDFS路径名',
  `operator_email_prefix` varchar(200) NOT NULL COMMENT '操作人邮箱前缀',
  `create_time` bigint(20) NOT NULL COMMENT '创建时间',
  `update_time` bigint(20) NOT NULL COMMENT '更新时间',
  `is_delete` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COMMENT='基准'

##增量流表
CREATE TABLE `ad_das_increment` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `increment_code` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '增量流code',
  `increment_name` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '增量流名称',
  `increment_desc` varchar(2000) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '增量流描述',
  `incre_stream_type` tinyint(2) NOT NULL DEFAULT '1' COMMENT '增量流类型: 1-online 2-preOnline',
  `data_stream_group_id` bigint(20) DEFAULT NULL COMMENT '数据流组id',
  `data_source` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '数据源',
  `topic` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '增量流topic',
  `operator_email_prefix` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作人',
  `is_delete` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否删除： 0-未删除 1-删除',
  `incre_stream_related_id` bigint(20) DEFAULT '0' COMMENT '增量流关联Id,当increStream=2时, 代表关联的online流id',
  `create_time` mediumtext COLLATE utf8mb4_unicode_ci COMMENT '创建时间',
  `update_time` mediumtext COLLATE utf8mb4_unicode_ci COMMENT '更新时间',
  `column_12` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='增量流'

##用户组关系表
CREATE TABLE `ad_das_data_stream_user_group_relation` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '基准id',
  `data_stream_group_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '数据流组id',
  `data_stream_id` bigint(20) NOT NULL COMMENT '数据流id',
  `data_stream_type` tinyint(4) NOT NULL COMMENT '数据流类型: 0-基准；1-增量',
  `user_group_id` tinyint(4) NOT NULL COMMENT '用户组id',
  `create_time` bigint(20) NOT NULL COMMENT '创建时间',
  `update_time` bigint(20) NOT NULL COMMENT '更新时间',
  `is_delete` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COMMENT='数据流-用户组关系表'

##用户组表
CREATE TABLE `ad_das_user_group` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '用户组id',
  `user_group_name` varchar(200) NOT NULL COMMENT '用户组名称',
  `owner_email_prefix` varchar(200) NOT NULL COMMENT '负责人邮箱前缀',
  `owner_name` varchar(100) NOT NULL COMMENT '负责人中文名',
  `create_time` bigint(20) NOT NULL COMMENT '创建时间',
  `update_time` bigint(20) NOT NULL COMMENT '更新时间',
  `is_delete` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COMMENT='用户组'

CREATE TABLE `ad_das_modify_benchmark_msg` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增主键',
  `modify_id` bigint(20) NOT NULL COMMENT '变更工单外键',
  `benchmark_type` tinyint(2) NOT NULL COMMENT '基准类型',
  `data_source` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '数据源',
  `main_table` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '主表',
  `pb_class` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'PB类名',
  `column_schema` text COLLATE utf8mb4_unicode_ci COMMENT '数据字段2PB字段映射关系',
  `table_type` tinyint(2) NOT NULL COMMENT '表类型： 0-单表 1-分表',
  `predict_growth` int(11) NOT NULL COMMENT '表预估增长量/d',
  `common_sqls` text COLLATE utf8mb4_unicode_ci COMMENT '取数SQL  List 字符串，会有多个sql',
  `time_params` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '时间参数',
  `shard_sql_flag` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否启用Shard Sql',
  `shard_sql_column` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT 'shard sql 字段名，默认为主键',
  `create_time` bigint(20) NOT NULL COMMENT '创建时间',
  `update_time` bigint(20) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ad_das_modify_benchmark_msg_modify_id_uindex` (`modify_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='基准变更详情表'