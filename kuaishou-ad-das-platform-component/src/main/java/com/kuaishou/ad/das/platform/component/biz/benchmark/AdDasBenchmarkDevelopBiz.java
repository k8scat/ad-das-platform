package com.kuaishou.ad.das.platform.component.biz.benchmark;

import java.util.List;

import com.kuaishou.ad.das.platform.component.web.param.AdDasBenchmarkDevelopHomeParam;
import com.kuaishou.ad.das.platform.component.web.param.AdDasBenchmarkDevelopTaskExecuteParam;
import com.kuaishou.ad.das.platform.component.web.view.benchmark.develop.AdDasBenchmarkDevelopBaseInfoView;
import com.kuaishou.ad.das.platform.component.web.view.benchmark.develop.AdDasBenchmarkDevelopHomeView;
import com.kuaishou.ad.das.platform.component.web.view.benchmark.develop.AdDasBenchmarkDevelopOutputDirectoryView;
import com.kuaishou.ad.das.platform.component.web.view.benchmark.develop.AdDasBenchmarkTaskEditRunRecordView;
import com.kuaishou.ad.das.platform.utils.model.AdDasUserView;

/**
 * @author wangkai <wangkai14@kuaishou.com>
 * Created on 2020-12-31
 */
public interface AdDasBenchmarkDevelopBiz {

    AdDasBenchmarkDevelopBaseInfoView queryBenchmarkBaseInfo();

    AdDasBenchmarkDevelopHomeView queryAdDasBenchmarkTaskEditList(AdDasBenchmarkDevelopHomeParam param);

    List<AdDasBenchmarkDevelopOutputDirectoryView> queryOutputDirectory();

    String queryOutputDirectoryPath(AdDasUserView adDasUserView, Integer outputDirectoryType);

    void taskExecute(AdDasUserView adDasUserView, AdDasBenchmarkDevelopTaskExecuteParam adDasBenchmarkDevelopTaskExecuteParam);

    void taskCancel(AdDasUserView adDasUserView,Long taskRecordId);

    List<AdDasBenchmarkTaskEditRunRecordView> taskExecuteDetail();
}
