package com.kuaishou.ad.das.platform.component.biz.benchmark;

import com.kuaishou.ad.das.platform.component.web.view.benchmark.modify.AdDasBenchmarkTaskView;
import com.kuaishou.ad.das.platform.utils.model.AdDasUserView;

/**
 * @author wangkai <wangkai14@kuaishou.com>
 * Created on 2020-12-30
 */
public interface AdDasBenchmarkTaskModifyBiz {

    AdDasBenchmarkTaskView benchmarkTaskDetail(Long taskId, Integer taskType);

    void benchmarkTaskEdit(AdDasUserView adDasUserView, AdDasBenchmarkTaskView adDasBenchmarkTaskView);
}
