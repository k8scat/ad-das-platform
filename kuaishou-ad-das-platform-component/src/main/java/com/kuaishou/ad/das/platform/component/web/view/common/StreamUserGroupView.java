package com.kuaishou.ad.das.platform.component.web.view.common;

import com.kuaishou.ad.das.platform.component.biz.usergroup.UserGroupWithBindDTO;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author Zhang Jialin <zhangjialin@kuaishou.com>
 * Created on 2021-05-18
 */
@Data
@ApiModel(value = "用户组信息")
public class StreamUserGroupView {
    @ApiModelProperty(value = "用户组id")
    private Long userGroupId;
    @ApiModelProperty(value = "用户组名称")
    private String userGroupName;
    @ApiModelProperty(value = "用户组接口人中文名")
    private String ownerName;
    @ApiModelProperty(value = "用户组接口人邮箱前缀", notes = "用于生成KIM链接：kim://username?username={ownerUniqueName}")
    private String ownerEmailPrefix;

    public static StreamUserGroupView build(UserGroupWithBindDTO dto) {
        StreamUserGroupView vo = new StreamUserGroupView();
        vo.setUserGroupId(dto.getUserGroupId());
        vo.setUserGroupName(dto.getUserGroupName());
        vo.setOwnerName(dto.getOwnerName());
        vo.setOwnerEmailPrefix(dto.getOwnerEmailPrefix());
        return vo;
    }
}
