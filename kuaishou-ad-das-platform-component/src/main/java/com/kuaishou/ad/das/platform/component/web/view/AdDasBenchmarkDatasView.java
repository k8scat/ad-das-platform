package com.kuaishou.ad.das.platform.component.web.view;

import java.io.Serializable;
import java.util.List;

import com.kuaishou.ad.das.platform.component.web.PageData;

import lombok.Data;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-10-09
 */
@Data
public class AdDasBenchmarkDatasView extends PageData implements Serializable {

    private static final long serialVersionUID = -6606296719510833370L;

    private List<AdDasBenchmarkData> dataList;
}
