package com.kuaishou.ad.das.platform.component.web.view;

import lombok.Data;

/**
 * 增量变更明细信息视图
 *
 * @author liutao <liutao06@kuaishou.com>
 * Created on 2021-01-05
 */
@Data
public class AdDasIncrementModifyInfoView {

    /**
     * 表名
     */
    private String mainTable;

    /**
     * pb类名
     */
    private String pbClass;

    /**
     * 操作人
     */
    private String operator;

    /**
     * 审核人
     */
    private String approver;

    /**
     * 操作类型 0 未操作 1 新增 2 删除
     */
    private Integer operationType;

    /**
     * 创建时间
     */
    private Long createTime;

    /**
     * 更新时间
     */
    private Long updateTime;

}
