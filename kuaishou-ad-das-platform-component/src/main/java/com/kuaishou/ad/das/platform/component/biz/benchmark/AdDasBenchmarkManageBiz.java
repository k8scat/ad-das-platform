package com.kuaishou.ad.das.platform.component.biz.benchmark;

import java.util.List;
import java.util.Set;

import com.kuaishou.ad.das.platform.component.web.param.AdDasBenchmarkManageHomeParam;
import com.kuaishou.ad.das.platform.component.web.param.benchmark.BenchmarkMainTableQueryItemParam;
import com.kuaishou.ad.das.platform.component.web.param.benchmark.BenchmarkQueryParam;
import com.kuaishou.ad.das.platform.component.web.view.AdDasBenchmarkServiceStatusView;
import com.kuaishou.ad.das.platform.component.web.view.AdDasBenchmarkTaskVisualizeView;
import com.kuaishou.ad.das.platform.component.web.view.AdDasCommonDataView;
import com.kuaishou.ad.das.platform.component.web.view.benchmark.manage.AdDasBenchmarkManageBaseInfoView;
import com.kuaishou.ad.das.platform.component.web.view.benchmark.manage.AdDasBenchmarkManageHomeView;
import com.kuaishou.ad.das.platform.component.web.view.benchmark.manage.BenchmarkMainTableQueryItemView;
import com.kuaishou.ad.das.platform.component.web.view.benchmark.manage.BenchmarkQueryView;

/**
 * @author wangkai <wangkai14@kuaishou.com>
 * Created on 2020-12-22
 */
public interface AdDasBenchmarkManageBiz {

    AdDasBenchmarkManageHomeView queryBenchmarkTaskList(AdDasBenchmarkManageHomeParam adDasBenchmarkManageHomeParam);


    AdDasBenchmarkManageBaseInfoView queryBenchmarkBaseInfo();

    void deleteTask(List<Long> taskIds);

    BenchmarkQueryView queryBenchmarkList(BenchmarkQueryParam param);

    BenchmarkMainTableQueryItemView queryMainTableItem(BenchmarkMainTableQueryItemParam param);

    /**
     * 获取基准HDFS路径集合
     * @return HDFS路径集合
     */
    Set<String> getHdfsPath();

    /**
     * 获取任务运行状态
     * @param benchmarkId
     * @return
     */
    AdDasBenchmarkServiceStatusView getServiceStatus(Long benchmarkId);

    /**
     * 获取基准可视化信息
     * @param benchmarkId
     * @return
     */
    AdDasBenchmarkTaskVisualizeView getTaskVisualize(Long benchmarkId);

    /**
     * 获取所有基准流枚举
     * @return
     */
    List<AdDasCommonDataView> listAllBenchmarkEnums();

}
