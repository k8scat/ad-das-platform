package com.kuaishou.ad.das.platform.component.web.view.benchmark.develop;

import java.util.List;

import com.kuaishou.ad.das.platform.component.web.PageData;

import lombok.Data;


/**
 * @author wangkai <wangkai14@kuaishou.com>
 * Created on 2020-12-24
 */
@Data
public class AdDasBenchmarkDevelopHomeView extends PageData {

    private List<AdDasBenchmarkDevelopHomeDetailView> homeDetailViewList;
}
