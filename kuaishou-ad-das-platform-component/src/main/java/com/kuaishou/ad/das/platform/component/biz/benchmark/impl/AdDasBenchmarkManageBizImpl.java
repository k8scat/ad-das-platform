package com.kuaishou.ad.das.platform.component.biz.benchmark.impl;

import static com.kuaishou.ad.das.platform.utils.DateTimeUtils.getCurHourTimestamp;
import static com.kuaishou.ad.das.platform.utils.DateTimeUtils.getDeltaHourDate;
import static com.kuaishou.ad.das.platform.utils.exception.ErrorCode.DATA_NOT_FOUND;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.collections.MapUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.google.common.collect.Lists;
import com.kuaishou.ad.das.platform.component.biz.benchmark.AdDasBenchmarkManageBiz;
import com.kuaishou.ad.das.platform.component.biz.usergroup.UserGroupBiz;
import com.kuaishou.ad.das.platform.component.biz.usergroup.UserGroupWithBindDTO;
import com.kuaishou.ad.das.platform.component.common.DataStreamTypeEnum;
import com.kuaishou.ad.das.platform.component.web.param.AdDasBenchmarkManageHomeParam;
import com.kuaishou.ad.das.platform.component.web.param.benchmark.BenchmarkMainTableQueryItemParam;
import com.kuaishou.ad.das.platform.component.web.param.benchmark.BenchmarkQueryParam;
import com.kuaishou.ad.das.platform.component.web.view.AdDasBenchmarkServiceStatusView;
import com.kuaishou.ad.das.platform.component.web.view.AdDasBenchmarkTaskDetailView;
import com.kuaishou.ad.das.platform.component.web.view.AdDasBenchmarkTaskVisualizeView;
import com.kuaishou.ad.das.platform.component.web.view.AdDasCommonDataView;
import com.kuaishou.ad.das.platform.component.web.view.benchmark.manage.AdDasBenchmarkManageBaseInfoView;
import com.kuaishou.ad.das.platform.component.web.view.benchmark.manage.AdDasBenchmarkManageHomeDetailView;
import com.kuaishou.ad.das.platform.component.web.view.benchmark.manage.AdDasBenchmarkManageHomeView;
import com.kuaishou.ad.das.platform.component.web.view.benchmark.manage.BenchmarkMainTableQueryItemView;
import com.kuaishou.ad.das.platform.component.web.view.benchmark.manage.BenchmarkQueryItemView;
import com.kuaishou.ad.das.platform.component.web.view.benchmark.manage.BenchmarkQueryView;
import com.kuaishou.ad.das.platform.component.web.view.common.MainTableSimpleView;
import com.kuaishou.ad.das.platform.core.benchmark.model.AdDasPlatformClientTaskBatchModel;
import com.kuaishou.ad.das.platform.core.common.daszk.Helper.AdDasPlatformCommonZkHelper;
import com.kuaishou.ad.das.platform.dal.dao.AdDasBenchmarkCondition;
import com.kuaishou.ad.das.platform.dal.dao.AdDasCommonDetailCondition;
import com.kuaishou.ad.das.platform.dal.dao.AdDasDataStreamGroupCondition;
import com.kuaishou.ad.das.platform.dal.dao.AdDasUserGroupBindCondition;
import com.kuaishou.ad.das.platform.dal.dao.ModifyCondition;
import com.kuaishou.ad.das.platform.dal.model.AdDasBenchmark;
import com.kuaishou.ad.das.platform.dal.model.AdDasBenchmarkTask;
import com.kuaishou.ad.das.platform.dal.model.AdDasBenchmarkTaskCommonDetail;
import com.kuaishou.ad.das.platform.dal.model.AdDasDataStreamGroup;
import com.kuaishou.ad.das.platform.dal.model.AdDasModify;
import com.kuaishou.ad.das.platform.dal.repository.AdDasBenchMarkTaskCommonDetailRepository;
import com.kuaishou.ad.das.platform.dal.repository.AdDasBenchmarkRepository;
import com.kuaishou.ad.das.platform.dal.repository.AdDasBenchmarkTaskRepository;
import com.kuaishou.ad.das.platform.dal.repository.AdDasDataStreamGroupRepository;
import com.kuaishou.ad.das.platform.dal.repository.AdDasModifyRepository;
import com.kuaishou.ad.das.platform.utils.AdDasBeanCoreUtils;
import com.kuaishou.ad.das.platform.utils.dasenum.AdDasClientTaskRunEnum;
import com.kuaishou.ad.das.platform.utils.exception.AdDasServiceException;

import lombok.extern.slf4j.Slf4j;

/**
 * @author wangkai <wangkai14@kuaishou.com>
 * Created on 2020-12-22
 */
@Lazy
@Slf4j
@Service
public class AdDasBenchmarkManageBizImpl implements AdDasBenchmarkManageBiz {

    @Autowired
    private AdDasBenchmarkTaskRepository adDasBenchmarkTaskRepository;

    @Autowired
    private AdDasBenchmarkRepository benchmarkRepository;

    @Autowired
    private AdDasBenchMarkTaskCommonDetailRepository commonDetailRepository;

    @Autowired
    private AdDasDataStreamGroupRepository dataStreamGroupRepository;

    @Autowired
    private AdDasModifyRepository modifyRepository;

    @Autowired
    private UserGroupBiz userGroupBiz;

    @Autowired
    private AdDasPlatformCommonZkHelper commonZkHelper;

    @Override
    public AdDasBenchmarkManageHomeView queryBenchmarkTaskList(AdDasBenchmarkManageHomeParam param) {
        int limit = param.getPageSize();
        int offset = param.getPageSize() * (param.getPageIndex() - 1);
        long total = adDasBenchmarkTaskRepository.queryCountByParams(param.getBenchmarkId(),
                param.getMainDataSource(), param.getPbClassName(), param.getMainTableName(), param.getTaskName());
        AdDasBenchmarkManageHomeView adDasBenchmarkManageHomeView = new AdDasBenchmarkManageHomeView();
        if (total == 0) {
            return null;
        }
        List<AdDasBenchmarkTask> adDasBenchmarkTasks =
                adDasBenchmarkTaskRepository.queryByParams(param.getBenchmarkId(),
                        param.getMainDataSource(), param.getPbClassName(), param.getMainTableName(), param.getTaskName(), offset, limit);
        if (!CollectionUtils.isEmpty(adDasBenchmarkTasks)) {
            List<AdDasBenchmarkManageHomeDetailView> homeDetailViews = Lists.newArrayList();
            adDasBenchmarkTasks.forEach(adDasBenchMarkTask -> homeDetailViews.add(
                    AdDasBenchmarkManageHomeDetailView.builder()
                            .taskId(adDasBenchMarkTask.getId())
                            .taskName(adDasBenchMarkTask.getTaskName())
                            .taskNum(adDasBenchMarkTask.getTaskNumber())
                            .benchmarkName("")
                            .mainDataSource(adDasBenchMarkTask.getDataSource())
                            .mainTableName(adDasBenchMarkTask.getMainTable())
                            .pbClassName(adDasBenchMarkTask.getPbClass())
                            .build()
            ));
            adDasBenchmarkManageHomeView.setHomeDetailViews(homeDetailViews);
            adDasBenchmarkManageHomeView.setPageSize(adDasBenchmarkTasks.size());
            adDasBenchmarkManageHomeView.setPageNum(param.getPageIndex());
            adDasBenchmarkManageHomeView.setTotalCount(total);
        }
        return adDasBenchmarkManageHomeView;
    }

    @Override
    public AdDasBenchmarkManageBaseInfoView queryBenchmarkBaseInfo() {
        List<AdDasBenchmarkTask> adDasBenchmarkTasks = adDasBenchmarkTaskRepository.queryAll();
        AdDasBenchmarkManageBaseInfoView view = new AdDasBenchmarkManageBaseInfoView();
        Set<String> mainTableNames = new HashSet<>();
        Set<String> pbClassNames = new HashSet<>();
        Set<String> mainDataSources = new HashSet<>();
        adDasBenchmarkTasks.forEach(
                adDasBenchMarkTask -> {
                    mainTableNames.add(adDasBenchMarkTask.getMainTable());
                    pbClassNames.add(adDasBenchMarkTask.getPbClass());
                    mainDataSources.add(adDasBenchMarkTask.getDataSource());
                }
        );
        view.setMainDataSources(new ArrayList<>(mainDataSources));
        view.setMainTableNames(new ArrayList<>(mainTableNames));
        view.setPbClassNames(new ArrayList<>(pbClassNames));
        return view;
    }

    @Override
    public void deleteTask(List<Long> taskIds) {
        if (!CollectionUtils.isEmpty(taskIds)) {
            taskIds.forEach(taskId -> adDasBenchmarkTaskRepository.deleteByTaskId(taskId));
        }
    }

    @Override
    public BenchmarkQueryView queryBenchmarkList(BenchmarkQueryParam param) {
        BenchmarkQueryView vo = new BenchmarkQueryView();
        vo.setPageNum(param.getPageNum());
        vo.setPageSize(param.getPageSize());
        vo.setDataList(Collections.emptyList());
        vo.setTotalCount(0L);
        // 1. 查询基准列表
        List<AdDasBenchmark> benchmarks;
        log.info("param={}", param);
        if (!CollectionUtils.isEmpty(param.getBenchmarkIds())) {
            benchmarks = benchmarkRepository.queryById(param.getBenchmarkIds());
            log.info("benchmarks={}", benchmarks);
        } else {
            AdDasBenchmarkCondition condition = new AdDasBenchmarkCondition();
            AdDasBeanCoreUtils.copyPropertiesIgnoreNull(condition, param);
            condition.setDataStreamGroupIds(Lists.newArrayList(param.getDataStreamGroupId()));
            benchmarks = benchmarkRepository.query(condition);
            log.info("benchmarks={}", benchmarks);
        }

        if (CollectionUtils.isEmpty(benchmarks)) {
            return vo;
        }
        List<Long> benchmarkIds = benchmarks.stream().map(AdDasBenchmark::getId).collect(Collectors.toList());
        // 2. 查询基准表信息
        AdDasCommonDetailCondition commonDetailCondition = new AdDasCommonDetailCondition();
        commonDetailCondition.setBenchmarkIds(benchmarkIds);
        if (!CollectionUtils.isEmpty(param.getTables())) {
            commonDetailCondition.setMainTableName(param.getTables().get(0));
        }
        List<AdDasBenchmarkTaskCommonDetail> details = commonDetailRepository.query(commonDetailCondition);
        log.info("details={}", details);
        Map<Long, List<AdDasBenchmarkTaskCommonDetail>> benchmarkId2Details = details.stream()
                .collect(Collectors.groupingBy(AdDasBenchmarkTaskCommonDetail::getBenchmarkId));
        // 3. 查询modify信息
        ModifyCondition modifyCondition = new ModifyCondition();
        modifyCondition.setModifyIds(details.stream().map(AdDasBenchmarkTaskCommonDetail::getModifyId)
                .collect(Collectors.toList()));
        Map<Long, AdDasModify> modifyId2Modify = modifyRepository.query(modifyCondition).stream()
                .collect(Collectors.toMap(AdDasModify::getId, item -> item));
        log.info("modifyId2Modify={}", modifyId2Modify);
        // 4. 查询数据流组列表
        List<Long> groupIds = benchmarks.stream().map(AdDasBenchmark::getDataStreamGroupId).collect(Collectors.toList());
        AdDasDataStreamGroupCondition groupCondition = new AdDasDataStreamGroupCondition();
        groupCondition.setIds(groupIds);
        Map<Long, AdDasDataStreamGroup> groupId2StreamGroup = dataStreamGroupRepository.query(groupCondition)
                .stream().collect(Collectors.toMap(AdDasDataStreamGroup::getId, item -> item));
        log.info("groupId2StreamGroup={}", groupId2StreamGroup);
        // 5. 查询用户组列表
        AdDasUserGroupBindCondition bindCondition = new AdDasUserGroupBindCondition();
        bindCondition.setDataStreamTypes(Collections.singletonList(DataStreamTypeEnum.BENCHMARK.getValue()));
        bindCondition.setDataStreamIds(benchmarkIds);
        Map<Long, List<UserGroupWithBindDTO>> benchmarkId2UserGroupBinds = userGroupBiz.queryUserGroupAndBind(bindCondition).stream()
                .collect(Collectors.groupingBy(UserGroupWithBindDTO::getDataStreamId));
        log.info("benchmarkId2UserGroupBinds={}", benchmarkId2UserGroupBinds);
        // 6. 构建vo
        List<BenchmarkQueryItemView> itemViews = benchmarks.stream()
                .filter(item -> {
                    if (!CollectionUtils.isEmpty(param.getBenchmarkIds())) {
                        return param.getBenchmarkIds().contains(item.getId());
                    } else {
                        return true;
                    }
                })
            .map(item -> {
                AdDasDataStreamGroup group = groupId2StreamGroup.get(item.getDataStreamGroupId());
                List<MainTableSimpleView> mainTables = Collections.emptyList();
                if (MapUtils.isNotEmpty(benchmarkId2Details)) {
                    log.info("benchmarkId2Details={}", benchmarkId2Details);

                    List<AdDasBenchmarkTaskCommonDetail> adDasBenchmarkTaskCommonDetails = benchmarkId2Details.get(item.getId());
                    if (!CollectionUtils.isEmpty(adDasBenchmarkTaskCommonDetails)) {
                        mainTables = adDasBenchmarkTaskCommonDetails.stream().map(detail ->
                                MainTableSimpleView.build(detail, modifyId2Modify)).collect(Collectors.toList());
                    }
                }
                List<UserGroupWithBindDTO> bindDTOList = new ArrayList<>();
                if (benchmarkId2UserGroupBinds.containsKey(item.getId())) {
                    bindDTOList.addAll(benchmarkId2UserGroupBinds.get(item.getId()));
                }
                return BenchmarkQueryItemView.build(item, bindDTOList, group, mainTables);
            }).collect(Collectors.toList());
        vo.setDataList(itemViews);
        vo.setTotalCount(Long.valueOf(itemViews.size()));
        return vo;
    }

    @Override
    public BenchmarkMainTableQueryItemView queryMainTableItem(BenchmarkMainTableQueryItemParam param) {
        // 1. 查询commonDetail
        BenchmarkMainTableQueryItemView itemView = new BenchmarkMainTableQueryItemView();
        AdDasBenchmarkTaskCommonDetail commonDetail = commonDetailRepository
                .getByCommonDetailId(param.getCommonDetailId());
        AdDasBeanCoreUtils.copyPropertiesIgnoreNull(itemView, commonDetail);
        itemView.setCommonDetailId(commonDetail.getId());
        // 2. 查询数据流组信息
        AdDasBenchmarkCondition benchmarkCondition = new AdDasBenchmarkCondition();
        benchmarkCondition.setBenchmarkIds(Collections.singletonList(commonDetail.getBenchmarkId()));
        List<AdDasBenchmark> benchmarks = benchmarkRepository.query(benchmarkCondition);
        AdDasBenchmark benchmark = benchmarks.get(0);
        AdDasDataStreamGroupCondition groupCondition = new AdDasDataStreamGroupCondition();
        groupCondition.setIds(Collections.singletonList(benchmark.getDataStreamGroupId()));
        List<AdDasDataStreamGroup> groups = dataStreamGroupRepository.query(groupCondition);
        AdDasDataStreamGroup group = groups.get(0);
        itemView.setDataStreamGroupId(group.getId());
        itemView.setDataStreamGroupName(group.getDataStreamGroupName());
        return itemView;
    }

    @Override
    public Set<String> getHdfsPath() {
        AdDasBenchmarkCondition benchmarkCondition = new AdDasBenchmarkCondition();
        List<AdDasBenchmark> benchmarks = benchmarkRepository.query(benchmarkCondition);
        return benchmarks.stream().collect(Collectors.groupingBy(AdDasBenchmark::getBenchmarkName)).keySet();
    }

    @Override
    public AdDasBenchmarkServiceStatusView getServiceStatus(Long benchmarkId) {

        AdDasBenchmark adDasBenchmark = benchmarkRepository.queryById(benchmarkId);
        if (adDasBenchmark == null) {
            throw AdDasServiceException.ofMessage(DATA_NOT_FOUND);
        }

        AdDasBenchmarkServiceStatusView serviceStatusView = new AdDasBenchmarkServiceStatusView();

        try {
            // 查询 对应zk节点信息
            AdDasPlatformClientTaskBatchModel clientTaskData = commonZkHelper.getClientTaskData(adDasBenchmark);

            if (clientTaskData != null) {
                serviceStatusView.setStatus(clientTaskData.getClientStatus());
                serviceStatusView.setStatusDesc(AdDasClientTaskRunEnum
                        .ofCode(clientTaskData.getClientStatus())
                        .getDesc());
            } else {
                serviceStatusView.setStatus(AdDasClientTaskRunEnum.NOT_INIT.getCode());
                serviceStatusView.setStatusDesc(AdDasClientTaskRunEnum.NOT_INIT.getDesc());
            }
            serviceStatusView.setNextRunTime(getDeltaHourDate(2, getCurHourTimestamp()));

        } catch (Exception e) {
            log.error("getServiceStatus() ocurr error! benchmarkId={}", benchmarkId, e);
            serviceStatusView.setStatus(AdDasClientTaskRunEnum.NOT_INIT.getCode());
            serviceStatusView.setStatusDesc(AdDasClientTaskRunEnum.NOT_INIT.getDesc());

            serviceStatusView.setNextRunTime(getDeltaHourDate(-2, getCurHourTimestamp()));
        }

        return serviceStatusView;
    }

    @Override
    public AdDasBenchmarkTaskVisualizeView getTaskVisualize(Long benchmarkId) {

        AdDasBenchmark adDasBenchmark = benchmarkRepository.queryById(benchmarkId);
        if (adDasBenchmark == null) {
            throw AdDasServiceException.ofMessage(DATA_NOT_FOUND);
        }

        AdDasBenchmarkTaskVisualizeView taskVisualizeView = new AdDasBenchmarkTaskVisualizeView();

        try {
            // 查询 对应zk节点信息
            AdDasPlatformClientTaskBatchModel clientTaskData = commonZkHelper.getClientTaskData(adDasBenchmark);
            if (clientTaskData != null) {
                taskVisualizeView.setStatus(AdDasClientTaskRunEnum.RUNNING.getCode());
                taskVisualizeView.setStatusDesc(AdDasClientTaskRunEnum.RUNNING.getDesc());
            }

            // 查询该基准流下的所有 任务列表
            List<AdDasBenchmarkTask> benchmarkTasks = adDasBenchmarkTaskRepository
                    .listByBenchmarkId(benchmarkId);

            taskVisualizeView.setTaskTotal(benchmarkTasks.size());

            List<AdDasBenchmarkTaskDetailView> taskDetailViews = Lists.newArrayList();

            // TODO 查询dump_info 和 数据文件信息


            // 查询对应client下的任务执行信息
            for (AdDasBenchmarkTask adDasBenchmarkTask : benchmarkTasks) {

                AdDasBenchmarkTaskDetailView taskDetailView = new AdDasBenchmarkTaskDetailView();
                taskDetailView.setTaskName(adDasBenchmarkTask.getTaskName());
                taskDetailView.setRunServiceNode("");
                taskDetailView.setDataCount(0L);
                taskDetailView.setDataFileSize(0L);
                taskDetailView.setTaskStatus(AdDasClientTaskRunEnum.RUNNING.getCode());
                taskDetailView.setTaskStatusDesc(AdDasClientTaskRunEnum.RUNNING.getDesc());
                taskDetailView.setCreateTime(System.currentTimeMillis());
                taskDetailView.setUpdateTime(System.currentTimeMillis());
                taskDetailViews.add(taskDetailView);
            }
        } catch (Exception e) {
            log.error("getServiceStatus() ocurr error! benchmarkId={}", benchmarkId, e);
        }

        return taskVisualizeView;
    }

    @Override
    public List<AdDasCommonDataView> listAllBenchmarkEnums() {

        List<AdDasCommonDataView> result = Lists.newArrayList();
        List<AdDasBenchmark> adDasBenchmarks = benchmarkRepository.listAll();

        if (CollectionUtils.isEmpty(adDasBenchmarks)) {
            return result;
        }

        for (AdDasBenchmark adDasBenchmark : adDasBenchmarks) {
            AdDasCommonDataView adDasCommonDataView = new AdDasCommonDataView();
            adDasCommonDataView.setDataDesc(adDasBenchmark.getBenchmarkName());
            adDasCommonDataView.setDataId(adDasBenchmark.getId());
            result.add(adDasCommonDataView);
        }

        return result;
    }
}
