package com.kuaishou.ad.das.platform.component.web.view;

import java.util.List;

import com.kuaishou.ad.das.platform.component.web.PageData;

import lombok.Data;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-11-24
 */
@Data
public class AdDasModifyDataListView extends PageData {

    private List<AdDasModifyDataView>  modifyDataViews;
}
