package com.kuaishou.ad.das.platform.component.web.param;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-11-24
 */
@Data
public class AdDasPageSearchParam {

    @ApiModelProperty(value = "每页大小", allowEmptyValue = true)
    protected Integer pageSize;

    @ApiModelProperty(value = "当前页码", allowEmptyValue = true)
    protected Integer pageIndex;
}
