package com.kuaishou.ad.das.platform.component.web.view;

import java.io.Serializable;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-09-29
 */
@Data
public class AdDasProtoMsgView implements Serializable {

    private static final long serialVersionUID = -5242301016668008340L;

    @ApiModelProperty(value = "列名")
    private String columnName;

    @ApiModelProperty(value = "列全名")
    private String columnFullName;

    @ApiModelProperty(value = "列Java类型")
    private String columnJavaType;

    @ApiModelProperty(value = "列PB类型")
    private String columnPbType;

    @ApiModelProperty(value = "枚举类型")
    private String columnEnumType;

    @ApiModelProperty(value = "默认值")
    private Object defaultValue;
}
