package com.kuaishou.ad.das.platform.component.web.view;

import lombok.Data;

/**
 * @author liutao <liutao06@kuaishou.com>
 * Created on 2021-01-05
 */
@Data
public class AdDasIncrementPublishDetailInfo {

    /**
     * 主机名
     */
    private String hostName;

    /**
     * 当前增量信息
     */
    private String curIncrementInfo;

    /**
     * 上个版本增量信息
     */
    private String preIncrementInfo;

    /**
     * 发布状态
     */
    private Integer publishStatus;
}
