package com.kuaishou.ad.das.platform.component.web.view;

import java.io.Serializable;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-12-06
 */
@Data
public class AdDasModifyPbDetailView implements Serializable {

    private static final long serialVersionUID = 7398653414114604008L;

    @ApiModelProperty(value = "当前最新PB包版本")
    private String curLastestPbVersion;

    @ApiModelProperty(value = "服务名称")
    private String serviceName;

    @ApiModelProperty(value = "当前服务PB包版本")
    private String curServicePbVersion;

    @ApiModelProperty(value = "选择变更PB包版本")
    private String targetPbVersion;

    @ApiModelProperty(value = "git cr链接, 多个cr可 ; 分隔")
    private String crLinks;

    @ApiModelProperty(value = "pipline 打包链接")
    private String piplineLinks;

    @ApiModelProperty(value = "审批人")
    private String approver;

}
