package com.kuaishou.ad.das.platform.component.web.view;

import java.io.Serializable;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 文件列表详情数据
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-09-26
 */

@Data
public class AdDasBenchmarkFileData implements Serializable {

    private static final long serialVersionUID = -7395419211190891792L;

    @ApiModelProperty("文件名称")
    private String fileName;

    @ApiModelProperty(value = "表名")
    private String tableName;

    @ApiModelProperty(value = "数量")
    private Integer recordNum;

    @ApiModelProperty(value = "pb名称")
    private String pbFullName;

    @ApiModelProperty(value = "文件大小")
    private Long fileSize;
}
