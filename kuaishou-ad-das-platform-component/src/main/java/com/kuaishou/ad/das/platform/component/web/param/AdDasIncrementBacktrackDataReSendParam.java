package com.kuaishou.ad.das.platform.component.web.param;

import java.io.Serializable;

import com.kuaishou.ad.das.platform.component.web.view.AdDasPlatformIncreBackServiceGraphView;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author caoyinhang <caoyinhang@kuaishou.com>
 * Created on 2021-07-07
 */

@Data
@ApiModel
public class AdDasIncrementBacktrackDataReSendParam implements Serializable {

    private static final long serialVersionUID = -6916738231062406102L;

    @ApiModelProperty(value = "增量流名称", required = true )
    private String incrementName;

    @ApiModelProperty(value = "productDef", required = true )
    private String productDef;

    @ApiModelProperty(value = "租户", required = true )
    private String tenant;

    @ApiModelProperty(value = "消费组", required = true )
    private String consumerGroup;

    @ApiModelProperty(value = "区域", required = true )
    private String region;

    @ApiModelProperty(value = "回溯时间", required = true )
    private Long durationSecond;

    @ApiModelProperty(value = "申请理由")
    private String applicationRea;

    @ApiModelProperty(value = "增量topic")
    private String topic;

    @ApiModelProperty(value = "消费topic类型", required = true )
    private String topicType;

    @ApiModelProperty(value = "回溯服务信息")
    private AdDasPlatformIncreBackServiceGraphView increBackServiceGraphView;


}
