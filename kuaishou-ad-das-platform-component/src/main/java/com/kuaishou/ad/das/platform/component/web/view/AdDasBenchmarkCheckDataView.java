package com.kuaishou.ad.das.platform.component.web.view;

import java.io.Serializable;
import java.util.List;

import lombok.Data;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-09-27
 */
@Data
public class AdDasBenchmarkCheckDataView implements Serializable {

    private String fileName;

    private String tableName;

    private Integer recordNum;

    private List<AdDasBenchmarkData> firstFiveData;

    private List<AdDasBenchmarkData> lastFiveData;
}
