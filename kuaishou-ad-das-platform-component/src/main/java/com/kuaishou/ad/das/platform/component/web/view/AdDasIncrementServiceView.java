package com.kuaishou.ad.das.platform.component.web.view;

import java.io.Serializable;
import java.util.List;

import lombok.Data;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-12-23
 */
@Data
public class AdDasIncrementServiceView implements Serializable {

    private static final long serialVersionUID = 3086037413422623812L;

    private String serviceName;

    private String dataSource;

    private Integer curTableCount;

    private String curPbVersion;

    private Integer instanceCount;

    private Integer modifyStatus;

    /**
     * 审核驳回原因
     */
    private String reason;

    private List<AdDasIncrementServiceDetailView> serviceDetailViews;
}
