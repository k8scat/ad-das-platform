package com.kuaishou.ad.das.platform.component.web.view.benchmark.manage;

import java.util.List;

import com.kuaishou.ad.das.platform.component.web.PageData;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author Zhang Jialin <zhangjialin@kuaishou.com>
 * Created on 2021-05-18
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class BenchmarkQueryView extends PageData {
    @ApiModelProperty(value = "基准数据条数")
    private List<BenchmarkQueryItemView> dataList;
}
