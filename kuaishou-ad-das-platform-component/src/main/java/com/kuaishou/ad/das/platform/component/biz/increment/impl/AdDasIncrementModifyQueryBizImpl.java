package com.kuaishou.ad.das.platform.component.biz.increment.impl;

import static com.kuaishou.ad.das.platform.utils.exception.ErrorCode.DATA_EXISTED_ERROR;
import static com.kuaishou.ad.das.platform.utils.exception.ErrorCode.INCREMENT_TABLE_NOT_EXISTS;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.kuaishou.ad.das.platform.component.biz.increment.AdDasIncrementModifyQueryBiz;
import com.kuaishou.ad.das.platform.component.biz.schema.AdDasSchemaQueryBiz;
import com.kuaishou.ad.das.platform.component.biz.usergroup.UserGroupBiz;
import com.kuaishou.ad.das.platform.component.biz.usergroup.UserGroupWithBindDTO;
import com.kuaishou.ad.das.platform.component.common.DataStreamTypeEnum;
import com.kuaishou.ad.das.platform.component.web.param.AdDasPlatformModifyParam;
import com.kuaishou.ad.das.platform.component.web.view.AdDasIncrementPublishDetailInfo;
import com.kuaishou.ad.das.platform.component.web.view.AdDasIncrementPublishInfoView;
import com.kuaishou.ad.das.platform.component.web.view.AdDasIncrementServiceView;
import com.kuaishou.ad.das.platform.component.web.view.AdDasModifyDataListView;
import com.kuaishou.ad.das.platform.component.web.view.AdDasModifyDataView;
import com.kuaishou.ad.das.platform.component.web.view.AdDasPlatformIncreDetailView;
import com.kuaishou.ad.das.platform.component.web.view.AdDasPlatformIncreSearchView;
import com.kuaishou.ad.das.platform.component.web.view.AdDasPlatformIncreTableCascadeSchemaView;
import com.kuaishou.ad.das.platform.component.web.view.AdDasPlatformIncreTableDetailView;
import com.kuaishou.ad.das.platform.component.web.view.AdDasPlatformIncreTableSchemaView;
import com.kuaishou.ad.das.platform.component.web.view.AdDasPlatformIncrementGraphView;
import com.kuaishou.ad.das.platform.component.web.view.AdDasPlatformIncrementTableView;
import com.kuaishou.ad.das.platform.component.web.view.AdDasPlatformModifyIncrementView;
import com.kuaishou.ad.das.platform.core.increment.model.AdDasPlatformTableColumnModel;
import com.kuaishou.ad.das.platform.component.web.view.AdDasTableMsgView;
import com.kuaishou.ad.das.platform.component.web.view.common.StreamUserGroupView;
import com.kuaishou.ad.das.platform.dal.dao.AdDasUserGroupBindCondition;
import com.kuaishou.ad.das.platform.dal.model.AdDasDataStreamGroup;
import com.kuaishou.ad.das.platform.dal.model.AdDasIncrement;
import com.kuaishou.ad.das.platform.dal.model.AdDasIncrementPublishRecord;
import com.kuaishou.ad.das.platform.dal.model.AdDasIncrementTableDetail;
import com.kuaishou.ad.das.platform.dal.model.AdDasModify;
import com.kuaishou.ad.das.platform.dal.model.AdDasModifyIncrementTableDetail;
import com.kuaishou.ad.das.platform.dal.repository.AdDasDataStreamGroupRepository;
import com.kuaishou.ad.das.platform.dal.repository.AdDasIncrementPublishRecordRepository;
import com.kuaishou.ad.das.platform.dal.repository.AdDasIncrementRepository;
import com.kuaishou.ad.das.platform.dal.repository.AdDasIncrementTableDetailRepository;
import com.kuaishou.ad.das.platform.dal.repository.AdDasModifyIncrementTableDetailRepository;
import com.kuaishou.ad.das.platform.dal.repository.AdDasModifyRepository;
import com.kuaishou.ad.das.platform.utils.exception.AdDasServiceException;
import com.kuaishou.framework.util.ObjectMapperUtils;

import lombok.extern.slf4j.Slf4j;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-12-23
 */
@Lazy
@Slf4j
@Service
public class AdDasIncrementModifyQueryBizImpl implements AdDasIncrementModifyQueryBiz {

    @Autowired
    private AdDasIncrementPublishRecordRepository adDasIncrementPublishRecordRepository;

    @Autowired
    private AdDasModifyRepository adDasModifyRepository;

    @Autowired
    private AdDasIncrementRepository  adDasIncrementRepository;

    @Autowired
    private AdDasIncrementTableDetailRepository incrementTableDetailRepository;

    @Autowired
    private AdDasDataStreamGroupRepository dasDataStreamGroupRepository;

    @Autowired
    private AdDasModifyIncrementTableDetailRepository modifyIncrementTableDetailRepository;

    @Autowired
    private UserGroupBiz userGroupBiz;

    @Autowired
    private AdDasSchemaQueryBiz adDasSchemaQueryBiz;

    @Override
    public AdDasIncrementServiceView getAdDasIncrementServiceView(String serviceName, Long modifyId) {
        AdDasIncrementServiceView adDasIncrementServiceView = new AdDasIncrementServiceView();
        adDasIncrementServiceView.setServiceName(serviceName);

        return adDasIncrementServiceView;
    }

    @Override
    public AdDasIncrementPublishInfoView incrementPublishInfoDetail(Long modifyId) {
        AdDasIncrementPublishInfoView adDasIncrementPublishInfoView = new AdDasIncrementPublishInfoView();
        List<AdDasIncrementPublishDetailInfo> detailInfoList = Lists.newArrayList();
        //从DB 获取发布信息
        List<AdDasIncrementPublishRecord> adDasIncrementPublishRecords =
                adDasIncrementPublishRecordRepository.listAdDasIncrementPublishRecordByModifyId(modifyId);

        if (!CollectionUtils.isEmpty(adDasIncrementPublishRecords)) {
            for (AdDasIncrementPublishRecord adDasIncrementPublishRecord : adDasIncrementPublishRecords) {
                AdDasIncrementPublishDetailInfo adDasIncrementPublishDetailInfo = new AdDasIncrementPublishDetailInfo();
                adDasIncrementPublishDetailInfo.setHostName(adDasIncrementPublishRecord.getHostName());
                adDasIncrementPublishDetailInfo.setPublishStatus(adDasIncrementPublishDetailInfo.getPublishStatus());
                adDasIncrementPublishDetailInfo.setCurIncrementInfo(adDasIncrementPublishRecord.getCurPbInfo());
                adDasIncrementPublishDetailInfo.setPreIncrementInfo(adDasIncrementPublishRecord.getPrePbInfo());
                detailInfoList.add(adDasIncrementPublishDetailInfo);
            }
        }
        adDasIncrementPublishInfoView.setDetailInfoList(detailInfoList);
        return adDasIncrementPublishInfoView;
    }

    @Override
    public AdDasPlatformIncreSearchView increSearch(Long dataStreamGroupId, String dataSource, String table, String dataTopic) {

        AdDasPlatformIncreSearchView increSearchView = new AdDasPlatformIncreSearchView();

        Set<Long> increIds = Sets.newHashSet();
        if (!StringUtils.isEmpty(table)) {
            List<AdDasIncrementTableDetail> incrementTableDetails =
                    incrementTableDetailRepository.queryByTableName(table);

            if (!CollectionUtils.isEmpty(incrementTableDetails)) {
                Set<Long> increIdsTmp = incrementTableDetails.stream().map(AdDasIncrementTableDetail::getIncreId).collect(Collectors.toSet());
                increIds.addAll(increIdsTmp);
            }
        }

        // 查询 增量流信息
        List<AdDasIncrement> adDasIncrements = adDasIncrementRepository
                .queryByCondition(increIds, dataStreamGroupId, dataSource, dataTopic);

        log.info("convertIncrementGraphView() adDasIncrements={}", adDasIncrements);

        if (CollectionUtils.isEmpty(adDasIncrements)) {
            return increSearchView;
        }

        List<Long> increIdSet = adDasIncrements.stream()
                .map(adDasIncrement -> adDasIncrement.getId())
                .collect(Collectors.toList());

        // 查询增量流中的表数据
        List<AdDasIncrementTableDetail> incrementTableDetails =
                incrementTableDetailRepository.queryByIncreId(increIdSet);
        log.info("convertIncrementGraphView() incrementTableDetails={}, increIdSet={}", incrementTableDetails, increIdSet);

        Map<Long, List<AdDasIncrementTableDetail>>  incrementTableDetailMap = incrementTableDetails
                .stream()
                .filter(adDasIncrementTableDetail -> adDasIncrementTableDetail.getTableType() == 1)
                .collect(Collectors.groupingBy(AdDasIncrementTableDetail::getIncreId));

        // 5. 查询用户组列表
        AdDasUserGroupBindCondition bindCondition = new AdDasUserGroupBindCondition();
        bindCondition.setDataStreamTypes(Collections.singletonList(DataStreamTypeEnum.INCREMENT.getValue()));
        bindCondition.setDataStreamIds(increIdSet);
        Map<Long, List<UserGroupWithBindDTO>> benchmarkId2UserGroupBinds = userGroupBiz.queryUserGroupAndBind(bindCondition).stream()
                .collect(Collectors.groupingBy(UserGroupWithBindDTO::getDataStreamId));
        log.info("benchmarkId2UserGroupBinds={}", benchmarkId2UserGroupBinds);

        List<AdDasPlatformIncrementGraphView> incrementGraphList = adDasIncrements
                .stream().map(adDasIncrement -> convertIncrementGraphView(adDasIncrement, incrementTableDetailMap, benchmarkId2UserGroupBinds))
                .collect(Collectors.toList());

        increSearchView.setIncrementGraphList(incrementGraphList);
        increSearchView.setTotalCount(adDasIncrements.size());

        return increSearchView;
    }

    private AdDasPlatformIncrementGraphView convertIncrementGraphView(AdDasIncrement adDasIncrement,
                                                                      Map<Long, List<AdDasIncrementTableDetail>> incrementTableDetailMap,
                                                                      Map<Long, List<UserGroupWithBindDTO>> benchmarkId2UserGroupBinds) {
        AdDasPlatformIncrementGraphView incrementGraphView = new AdDasPlatformIncrementGraphView();

        incrementGraphView.setIncreId(adDasIncrement.getId());
        incrementGraphView.setIncreName(adDasIncrement.getIncrementName());
        incrementGraphView.setIncreCode(adDasIncrement.getIncrementCode());
        incrementGraphView.setIncreDesc(adDasIncrement.getIncrementDesc());
        incrementGraphView.setDataSource(adDasIncrement.getDataSource());
        incrementGraphView.setOperator(adDasIncrement.getOperatorEmailPrefix());

        incrementGraphView.setCreateTime(adDasIncrement.getCreateTime());
        incrementGraphView.setUpdateTime(adDasIncrement.getUpdateTime());
        incrementGraphView.setDataTopic(adDasIncrement.getTopic());

        List<AdDasIncrementTableDetail> incrementTableDetails = incrementTableDetailMap.get(adDasIncrement.getId());

        log.info("convertIncrementGraphView() incrementTableDetails={}", incrementTableDetails);

        if (!CollectionUtils.isEmpty(incrementTableDetails)) {
            List<String> tableList = incrementTableDetails.stream()
                    .map(incrementTableDetail -> incrementTableDetail.getIncreTable())
                    .collect(Collectors.toList());
            incrementGraphView.setTableList(tableList);
        }

        List<UserGroupWithBindDTO> userGroupWithBindDTOS = benchmarkId2UserGroupBinds.get(adDasIncrement.getId());
        if (!CollectionUtils.isEmpty(userGroupWithBindDTOS)) {
            incrementGraphView.setUserGroups(userGroupWithBindDTOS.stream().map(StreamUserGroupView::build).collect(Collectors.toList()));
        }

        return incrementGraphView;
    }


    @Override
    public AdDasPlatformIncreDetailView increDetail(Long increStreamId, Integer increStreamType) {

        // 查询数据流信息
        AdDasIncrement adDasIncrement = adDasIncrementRepository.queryById(increStreamId);

        // 查询数据流组信息
        AdDasDataStreamGroup adDasDataStreamGroup = dasDataStreamGroupRepository.queryById(adDasIncrement.getDataStreamGroupId());

        // 查询数据流表信息
        List<AdDasIncrementTableDetail> adDasIncrementTableDetails = incrementTableDetailRepository
                .queryByIncreId(Arrays.asList(increStreamId));

        AdDasPlatformIncreDetailView increDetailView = new AdDasPlatformIncreDetailView();
        increDetailView.setDataStreamGroup(adDasDataStreamGroup.getDataStreamGroupName());
        increDetailView.setDataStreamGroupId(adDasDataStreamGroup.getId());
        increDetailView.setIncreStreamName(adDasIncrement.getIncrementName());
        increDetailView.setIncreStreamDesc(adDasIncrement.getIncrementDesc());
        increDetailView.setIncreStreamId(adDasIncrement.getId());
        increDetailView.setIncreStreamType(adDasIncrement.getIncreStreamType());
        increDetailView.setIncreStreamRelatedId(adDasIncrement.getIncreStreamRelatedId());

        AdDasPlatformIncrementGraphView incrementGraphView = new AdDasPlatformIncrementGraphView();
        incrementGraphView.setIncreId(adDasIncrement.getId());
        incrementGraphView.setIncreName(adDasIncrement.getIncrementName());
        incrementGraphView.setIncreCode(adDasIncrement.getIncrementCode());
        incrementGraphView.setIncreDesc(adDasIncrement.getIncrementDesc());
        incrementGraphView.setDataSource(adDasIncrement.getDataSource());
        incrementGraphView.setDataTopic(adDasIncrement.getTopic());

        List<String> tableList = adDasIncrementTableDetails.stream()
                .filter(adDasIncrementTableDetail -> adDasIncrementTableDetail.getTableType() == 1)
                .map(AdDasIncrementTableDetail::getIncreTable).collect(Collectors.toList());
        incrementGraphView.setTableList(tableList);
        incrementGraphView.setOperator(adDasIncrement.getOperatorEmailPrefix());

        // 5. 查询用户组列表
        AdDasUserGroupBindCondition bindCondition = new AdDasUserGroupBindCondition();
        bindCondition.setDataStreamTypes(Collections.singletonList(DataStreamTypeEnum.INCREMENT.getValue()));

        bindCondition.setDataStreamIds(Arrays.asList(increStreamId));
        Map<Long, List<UserGroupWithBindDTO>> benchmarkId2UserGroupBinds = userGroupBiz.queryUserGroupAndBind(bindCondition).stream()
                .collect(Collectors.groupingBy(UserGroupWithBindDTO::getDataStreamId));
        log.info("benchmarkId2UserGroupBinds={}", benchmarkId2UserGroupBinds);

        List<UserGroupWithBindDTO> userGroupWithBindDTOS = benchmarkId2UserGroupBinds.get(adDasIncrement.getId());
        if (!CollectionUtils.isEmpty(userGroupWithBindDTOS)) {
            incrementGraphView.setUserGroups(userGroupWithBindDTOS.stream().map(StreamUserGroupView::build).collect(Collectors.toList()));
        }

        incrementGraphView.setCreateTime(adDasIncrement.getCreateTime());
        incrementGraphView.setUpdateTime(adDasIncrement.getUpdateTime());
        increDetailView.setIncrementGraphView(incrementGraphView);

        List<AdDasPlatformIncrementTableView> incrementTableViews = Lists.newArrayList();
        adDasIncrementTableDetails.stream()
                .forEach(adDasIncrementTableDetail -> {
                    AdDasPlatformIncrementTableView dasPlatformIncrementTableView = new AdDasPlatformIncrementTableView();
                    dasPlatformIncrementTableView.setTable(adDasIncrementTableDetail.getIncreTable());
                    dasPlatformIncrementTableView.setPbEnum(adDasIncrementTableDetail.getPbClassEnum());
                    dasPlatformIncrementTableView.setApprover(adDasIncrementTableDetail.getApprover());
                    dasPlatformIncrementTableView.setOperator(adDasIncrementTableDetail.getOperator());
                    dasPlatformIncrementTableView.setCreateTime(adDasIncrementTableDetail.getCreateTime());
                    dasPlatformIncrementTableView.setUpdateTime(adDasIncrementTableDetail.getUpdateTime());
                    incrementTableViews.add(dasPlatformIncrementTableView);
                });

        increDetailView.setIncrementTableViews(incrementTableViews);

        return increDetailView;
    }

    @Override
    public AdDasPlatformIncreTableDetailView increTableDetail(Long increStreamId,
                                                              Integer increStreamType, String mainTable) {

        AdDasPlatformIncreTableDetailView increTableDetailView = new AdDasPlatformIncreTableDetailView();

        // 查询数据流信息
        AdDasIncrement adDasIncrement = adDasIncrementRepository.queryById(increStreamId);

        // 查询数据流组信息
        AdDasDataStreamGroup adDasDataStreamGroup = dasDataStreamGroupRepository.queryById(adDasIncrement.getDataStreamGroupId());

        // 查询表信息
        AdDasIncrementTableDetail incrementTableDetail = incrementTableDetailRepository.queryByIncreIdAndTable(increStreamId, mainTable);

        if (incrementTableDetail == null) {
            // 提示未接入此表
            return increTableDetailView;
        }

        increTableDetailView.setDataStreamGroupId(adDasDataStreamGroup.getId());
        increTableDetailView.setDataStreamGroup(adDasDataStreamGroup.getDataStreamGroupName());
        increTableDetailView.setDataStreamGroupDesc(adDasDataStreamGroup.getDataStreamGroupDesc());
        increTableDetailView.setIncreStreamId(adDasIncrement.getId());
        increTableDetailView.setIncreStreamName(adDasIncrement.getIncrementName());
        increTableDetailView.setIncreStreamType(adDasIncrement.getIncreStreamType());
        increTableDetailView.setDataSource(adDasIncrement.getDataSource());
        increTableDetailView.setMainTable(incrementTableDetail.getIncreTable());
        increTableDetailView.setPbEnum(incrementTableDetail.getPbClassEnum());

        String cascadeSchema = incrementTableDetail.getCascadeSchema();
        if (!StringUtils.isEmpty(cascadeSchema)) {
            Map<String, AdDasPlatformIncreTableCascadeSchemaView> tableCascadeSchemaViewMa = ObjectMapperUtils
                    .fromJSON(cascadeSchema, Map.class, String.class, AdDasPlatformIncreTableCascadeSchemaView.class);
            increTableDetailView.setTableCascadeSchemaViewMap(tableCascadeSchemaViewMa);
        }

        Map<String, AdDasPlatformIncreTableSchemaView> tableSchemaViewMap = Maps.newHashMap();

        // 查询表 DB schema 字段信息
        List<AdDasTableMsgView> adDasTableMsgViews = adDasSchemaQueryBiz
                .queryTableMsg(incrementTableDetail.getDataSource(), incrementTableDetail.getIncreTable());

        AdDasPlatformIncreTableSchemaView mainTableView = buildIncreTableSchemaView(incrementTableDetail, adDasTableMsgViews);
        tableSchemaViewMap.put(incrementTableDetail.getIncreTable(), mainTableView);

        // 填充级联表关系
        if (!StringUtils.isEmpty(incrementTableDetail.getCascadedTable())) {

            List<String> cascadeTableList = ObjectMapperUtils
                    .fromJSON(incrementTableDetail.getCascadedTable(), List.class, String.class);
            if (!CollectionUtils.isEmpty(cascadeTableList)) {
                List<AdDasIncrementTableDetail> incrementTableDetails = incrementTableDetailRepository.queryByIncreIdAndTables(increStreamId, cascadeTableList);
                for (AdDasIncrementTableDetail cascadeIncreTableDetail : incrementTableDetails) {
                    if (cascadeIncreTableDetail.getTableType() == 2) {
                        List<AdDasTableMsgView> cascadeTableMsgViews = adDasSchemaQueryBiz
                                .queryTableMsg(incrementTableDetail.getDataSource(), incrementTableDetail.getIncreTable());
                        AdDasPlatformIncreTableSchemaView cascadeTableView = buildIncreTableSchemaView(cascadeIncreTableDetail, cascadeTableMsgViews);
                        tableSchemaViewMap.put(cascadeIncreTableDetail.getIncreTable(), cascadeTableView);
                    }
                }
                increTableDetailView.setCascadeTable(cascadeTableList);
            } else {

                increTableDetailView.setCascadeTable(Lists.newArrayList());
            }
        }

        increTableDetailView.setTableSchemaViewMap(tableSchemaViewMap);

        return increTableDetailView;
    }

    /**
     * 填充表字段信息
     * @param incrementTableDetail
     * @return
     */
    private AdDasPlatformIncreTableSchemaView buildIncreTableSchemaView(AdDasIncrementTableDetail incrementTableDetail, List<AdDasTableMsgView> adDasTableMsgViews) {

        AdDasPlatformIncreTableSchemaView increTableSchemaView = new AdDasPlatformIncreTableSchemaView();
        increTableSchemaView.setTableName(incrementTableDetail.getIncreTable());
        increTableSchemaView.setTableType(incrementTableDetail.getTableType());
        increTableSchemaView.setPrimaryIdKey(incrementTableDetail.getPrimaryIdKey());

        String tableColumnData = incrementTableDetail.getTableColumn();

        List<AdDasPlatformTableColumnModel> tableColumnViews = Lists.newArrayList();
        Map<String, AdDasPlatformTableColumnModel> tableColumnViewMap = Maps.newHashMap();
        if (!StringUtils.isEmpty(tableColumnData)) {
            List<AdDasPlatformTableColumnModel> tableColumn = ObjectMapperUtils
                    .fromJSON(tableColumnData, List.class, AdDasPlatformTableColumnModel.class);
            tableColumn.stream().forEach(adDasPlatformTableColumnView -> {
                tableColumnViewMap.put(adDasPlatformTableColumnView.getColumnName(), adDasPlatformTableColumnView);
            });
        }

        for (AdDasTableMsgView adDasTableMsgView : adDasTableMsgViews) {
            AdDasPlatformTableColumnModel tableColumnView = new AdDasPlatformTableColumnModel();
            tableColumnView.setColumnName(adDasTableMsgView.getColumnName());
            tableColumnView.setColumnType(adDasTableMsgView.getColumnType());
            if (tableColumnViewMap.containsKey(adDasTableMsgView.getColumnName())) {
                AdDasPlatformTableColumnModel columnModel = tableColumnViewMap.get(adDasTableMsgView.getColumnName());
                tableColumnView.setFlag(columnModel.isFlag());
            } else {
                tableColumnView.setFlag(false);
            }
            tableColumnViews.add(tableColumnView);
        }

        increTableSchemaView.setTableColumnViews(tableColumnViews);

        increTableSchemaView.setCreateTime(incrementTableDetail.getCreateTime());
        increTableSchemaView.setOperator(incrementTableDetail.getOperator());
        increTableSchemaView.setUpdateTime(incrementTableDetail.getUpdateTime());

        return increTableSchemaView;
    }

    @Override
    public AdDasPlatformModifyIncrementView incrementModifyData(Long modifyId, Integer modifyType) {

        // 查询变更工单
        AdDasModify adDasModify = adDasModifyRepository.queryById(modifyId);

        //  查询数据流表变更详情
        List<AdDasModifyIncrementTableDetail> modifyIncrementTableDetails = modifyIncrementTableDetailRepository
                .queryByModifyId(modifyId);
        if (CollectionUtils.isEmpty(modifyIncrementTableDetails)) {
            throw AdDasServiceException.ofMessage(DATA_EXISTED_ERROR);
        }

        AdDasPlatformModifyIncrementView modifyIncrementView = new AdDasPlatformModifyIncrementView();
        modifyIncrementView.setModifyId(adDasModify.getId());
        modifyIncrementView.setModifyName(adDasModify.getModifyName());
        modifyIncrementView.setModifyStatus(adDasModify.getModifyStatus());
        modifyIncrementView.setReason(adDasModify.getReason());
        modifyIncrementView.setModifyType(adDasModify.getModifyType());
        modifyIncrementView.setApprover(adDasModify.getApprover());
        modifyIncrementView.setOperator(adDasModify.getOperator());

        AdDasPlatformIncreTableDetailView increTableDetailView = new AdDasPlatformIncreTableDetailView();

        // 主表
        AdDasModifyIncrementTableDetail mainTableDetail = modifyIncrementTableDetails.stream()
                .filter(modifyIncrementTableDetail -> modifyIncrementTableDetail.getTableType() == 1)
                .findFirst().get();

        if (mainTableDetail.getOperatType() == 3) {
            modifyIncrementView.setDeleteTable(true);
        } else {
            modifyIncrementView.setDeleteTable(false);
        }

        if (mainTableDetail == null) {
            throw AdDasServiceException.ofMessage(INCREMENT_TABLE_NOT_EXISTS);
        }

        // 查询数据流信息
        AdDasIncrement adDasIncrement = adDasIncrementRepository.queryById(mainTableDetail.getIncreId());

        // 查询数据流组信息
        AdDasDataStreamGroup adDasDataStreamGroup = dasDataStreamGroupRepository.queryById(adDasIncrement.getDataStreamGroupId());

        increTableDetailView.setDataStreamGroupId(adDasDataStreamGroup.getId());
        increTableDetailView.setDataStreamGroup(adDasDataStreamGroup.getDataStreamGroupName());
        increTableDetailView.setIncreStreamName(adDasIncrement.getIncrementName());
        increTableDetailView.setIncreStreamId(adDasIncrement.getId());
        increTableDetailView.setIncreStreamType(adDasIncrement.getIncreStreamType());
        increTableDetailView.setMainTable(mainTableDetail.getIncreTable());
        increTableDetailView.setPbEnum(mainTableDetail.getPbClassEnum());
        increTableDetailView.setDataSource(mainTableDetail.getDataSource());

        String cascadeSchema = mainTableDetail.getCascadeSchema();
        if (!StringUtils.isEmpty(cascadeSchema)) {
            Map<String, AdDasPlatformIncreTableCascadeSchemaView> tableCascadeSchemaViewMa = ObjectMapperUtils
                    .fromJSON(cascadeSchema, Map.class, String.class, AdDasPlatformIncreTableCascadeSchemaView.class);
            increTableDetailView.setTableCascadeSchemaViewMap(tableCascadeSchemaViewMa);
        }

        Map<String, AdDasPlatformIncreTableSchemaView> tableSchemaViewMap = Maps.newHashMap();

        AdDasPlatformIncreTableSchemaView mainTableView = buildModifyIncreTableSchemaView(mainTableDetail);
        tableSchemaViewMap.put(mainTableDetail.getIncreTable(), mainTableView);

        // 填充级联表关系
        if (!StringUtils.isEmpty(mainTableDetail.getCascadedTable())) {

            List<String> cascadeTableList = ObjectMapperUtils
                    .fromJSON(mainTableDetail.getCascadedTable(), List.class, String.class);

            increTableDetailView.setCascadeTable(cascadeTableList);

            // 级联表
            List<AdDasModifyIncrementTableDetail> cascadeTableDetails = modifyIncrementTableDetails.stream()
                    .filter(modifyIncrementTableDetail -> modifyIncrementTableDetail.getTableType() == 2)
                    .collect(Collectors.toList());

            for (AdDasModifyIncrementTableDetail modifyIncrementTableDetail : cascadeTableDetails) {
                AdDasPlatformIncreTableSchemaView cascadeTableView = buildModifyIncreTableSchemaView(modifyIncrementTableDetail);
                tableSchemaViewMap.put(modifyIncrementTableDetail.getIncreTable(), cascadeTableView);
            }
        }

        increTableDetailView.setTableSchemaViewMap(tableSchemaViewMap);
        modifyIncrementView.setIncreTableDetailView(increTableDetailView);

        return modifyIncrementView;
    }

    /**
     * 填充表字段信息
     * @param incrementTableDetail
     * @return
     */
    private AdDasPlatformIncreTableSchemaView buildModifyIncreTableSchemaView(AdDasModifyIncrementTableDetail incrementTableDetail) {

        AdDasPlatformIncreTableSchemaView increTableSchemaView = new AdDasPlatformIncreTableSchemaView();
        increTableSchemaView.setTableName(incrementTableDetail.getIncreTable());
        increTableSchemaView.setTableType(incrementTableDetail.getTableType());
        increTableSchemaView.setPrimaryIdKey(incrementTableDetail.getPrimaryIdKey());

        String tableColumn = incrementTableDetail.getTableColumn();

        if (!StringUtils.isEmpty(tableColumn)) {
            List<AdDasPlatformTableColumnModel> tableColumnViews = ObjectMapperUtils
                    .fromJSON(tableColumn, List.class, AdDasPlatformTableColumnModel.class);

            increTableSchemaView.setTableColumnViews(tableColumnViews);
        } else {
            increTableSchemaView.setTableColumnViews(Lists.newArrayList());
        }

        increTableSchemaView.setCreateTime(incrementTableDetail.getCreateTime());
        increTableSchemaView.setOperator(incrementTableDetail.getOperator());
        increTableSchemaView.setUpdateTime(incrementTableDetail.getUpdateTime());

        return increTableSchemaView;
    }
    @Override
    public AdDasModifyDataListView increRecord(AdDasPlatformModifyParam modifyParam) {

        AdDasModifyDataListView adDasModifyDataListView;

        // 按照表来 join 查 ad_das_modify_increment_table_detail left join ad_das_modify 表数据
        List<AdDasModifyIncrementTableDetail> modifyIncrementTableDetails = modifyIncrementTableDetailRepository
                .searchTableWithModifyMsg(modifyParam.getIncreId(), modifyParam.getTable());

        List<Long> modifyIds = modifyIncrementTableDetails.stream()
                .filter(modifyIncrementTableDetail -> modifyIncrementTableDetail.getTableType() == 1)
                .map(AdDasModifyIncrementTableDetail::getModifyId)
                .collect(Collectors.toList());
        // 再查询出 ad_das_modify 表的数据
        List<AdDasModify> adDasModifies = adDasModifyRepository.queryByConditions(modifyIds, modifyParam.getModifyName(),
                modifyParam.getModifyType(), modifyParam.getModifyStatus(),
                modifyParam.getStartTime(), modifyParam.getEndTime(),
                modifyParam.getPageSize(), (modifyParam.getPageIndex() - 1) * modifyParam.getPageSize());
        adDasModifyDataListView = buildModifyDataView(modifyParam, modifyIncrementTableDetails, adDasModifies);

        return adDasModifyDataListView;
    }

    @Override
    public List<String> getIncreTopics() {

        List<AdDasIncrement> allIncres = adDasIncrementRepository.queryAll();

        return allIncres.stream()
                .map(AdDasIncrement::getTopic)
                .distinct()
                .collect(Collectors.toList());
    }

    private AdDasModifyDataListView buildModifyDataView(AdDasPlatformModifyParam modifyParam, List<AdDasModifyIncrementTableDetail> modifyIncrementTableDetails,
                                                        List<AdDasModify> adDasModifies) {
        AdDasModifyDataListView adDasModifyDataListView = new AdDasModifyDataListView();

        List<AdDasModifyDataView>  modifyDataViews = Lists.newArrayList();

        Map<Long, AdDasModifyIncrementTableDetail> modifyIncrementTableDetailMap = modifyIncrementTableDetails
                .stream()
                .collect(Collectors.toMap(AdDasModifyIncrementTableDetail::getModifyId,
                        modifyIncrementTableDetail -> modifyIncrementTableDetail, (v1,v2)->v1));

        for (AdDasModify adDasModify : adDasModifies) {
            AdDasModifyDataView adDasModifyDataView = new AdDasModifyDataView();

            adDasModifyDataView.setId(adDasModify.getId());
            adDasModifyDataView.setModifyName(adDasModify.getModifyName());
            adDasModifyDataView.setModifyType(adDasModify.getModifyType());
            adDasModifyDataView.setModifyStatus(adDasModify.getModifyStatus());
            adDasModifyDataView.setOperator(adDasModify.getOperator());
            adDasModifyDataView.setApprover(adDasModify.getApprover());
            adDasModifyDataView.setCreateTime(adDasModify.getCreateTime());
            adDasModifyDataView.setUpdateTime(adDasModify.getUpdateTime());

            AdDasModifyIncrementTableDetail adDasModifyIncrementTableDetail = modifyIncrementTableDetailMap.get(adDasModify.getId());
            if (adDasModifyIncrementTableDetail != null) {
                adDasModifyDataView.setTableName(adDasModifyIncrementTableDetail.getIncreTable());
                adDasModifyDataView.setPbClassEnum(adDasModifyIncrementTableDetail.getPbClassEnum());
            }
            modifyDataViews.add(adDasModifyDataView);
        }

        adDasModifyDataListView.setPageNum(modifyParam.getPageIndex());
        adDasModifyDataListView.setPageSize(modifyParam.getPageSize());
        adDasModifyDataListView.setTotalCount((long)adDasModifies.size());
        adDasModifyDataListView.setModifyDataViews(modifyDataViews);

        return adDasModifyDataListView;
    }

}
