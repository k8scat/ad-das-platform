package com.kuaishou.ad.das.platform.component.web.view;

import lombok.Data;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-12-23
 */
@Data
public class AdDasIncrementManageHomeView {

    private String serviceName;

    private Integer instanceCount;

    private String curPbVersion;

    private String operator;

    private Long updateTime;
}
