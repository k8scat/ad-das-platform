package com.kuaishou.ad.das.platform.component.web.view.benchmark.manage;


import com.kuaishou.ad.das.platform.core.benchmark.model.AdDasBenchmarkMainTableView;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author Zhang Jialin <zhangjialin@kuaishou.com>
 * Created on 2021-05-21
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class BenchmarkMainTableQueryItemView extends AdDasBenchmarkMainTableView {
    @ApiModelProperty(value = "数据流组id")
    private Long dataStreamGroupId;
    @ApiModelProperty(value = "数据流组名称")
    private String dataStreamGroupName;
}
