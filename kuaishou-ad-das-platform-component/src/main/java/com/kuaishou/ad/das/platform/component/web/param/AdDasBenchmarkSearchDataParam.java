package com.kuaishou.ad.das.platform.component.web.param;

import java.io.Serializable;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-09-27
 */
@Data
@ApiModel
public class AdDasBenchmarkSearchDataParam implements Serializable {

    private static final long serialVersionUID = 1005780000536540831L;

    @ApiModelProperty(value = "暂不使用")
    private Integer delLock;

    @ApiModelProperty(value = "文件名称， 分为两类： 1-dumpInfo 2-数据文件", required = true)
    private String fileName;

    @ApiModelProperty(value = "整点时间戳", required = true)
    private Long timestamp;

    @ApiModelProperty(value = "基准类型： ad_base、adCharge", required = true)
    private String benchmarkType;

    @ApiModelProperty(value = "基准类型： ad_base、adCharge", required = false, allowEmptyValue = true)
    private Long benchmarkId=null;

    @ApiModelProperty(value = "搜索idList  [2,3,5]")
    private List<String> ids;

    @ApiModelProperty(value = "数据id name")
    private String idKey;

    @ApiModelProperty(value = "环境", allowEmptyValue = true)
    private String nodeEnv;
}
