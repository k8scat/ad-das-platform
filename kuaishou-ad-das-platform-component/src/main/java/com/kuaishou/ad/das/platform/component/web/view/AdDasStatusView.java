package com.kuaishou.ad.das.platform.component.web.view;

import java.io.Serializable;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-12-01
 */
@Data
public class AdDasStatusView implements Serializable {

    private static final long serialVersionUID = -8093812113571863842L;

    @ApiModelProperty(value = "状态编码： 0-全部")
    private Integer statusCode;

    @ApiModelProperty(value = "状态名称")
    private String statusName;
}
