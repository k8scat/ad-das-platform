package com.kuaishou.ad.das.platform.component.web;


import com.kuaishou.ad.das.platform.utils.exception.ErrorCode;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-09-26
 */
@Data
@NoArgsConstructor
public class ResponseObject<T> {

    private Integer result;

    private String errorMsg;

    private T data;


    public static final String RESPONSE_MESSAGE_OK = "ok";


    public ResponseObject(Integer responseCode, String responseMsg) {
        this.result = responseCode;
        this.errorMsg = responseMsg;
    }

    public ResponseObject(Integer responseCode, String responseMsg, T responseData) {
        this.result = responseCode;
        this.errorMsg = responseMsg;
        this.data = responseData;
    }


    public static <T> ResponseObject<T> ofErrorCode(ErrorCode errorCode) {
        return new ResponseObject<>(errorCode.getCode(), errorCode.getMsg());
    }

    public static <T> ResponseObject<T> ok() {
        return new ResponseObject<>(ErrorCode.SUCCESS.getCode(), ErrorCode.SUCCESS.getMsg());
    }

    public static <T> ResponseObject<T> ok(T data) {
        return new ResponseObject<>(ErrorCode.SUCCESS.getCode(), ErrorCode.SUCCESS.getMsg(), data);
    }

    public static <T> ResponseObject<T> ofErrorCode(ErrorCode errorCode, T t) {
        return new ResponseObject<>(errorCode.getCode(), errorCode.getMsg(), t);
    }
}
