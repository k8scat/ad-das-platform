package com.kuaishou.ad.das.platform.component.web.param;

import java.io.Serializable;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-08-01
 */
@Data
public class AdDasEditIncrementServiceParam implements Serializable {

    private static final long serialVersionUID = -6146989622404231108L;

    @ApiModelProperty(value = "id", allowEmptyValue = true)
    private Long id;

    @ApiModelProperty(value = "增量id", allowEmptyValue = true)
    private Long increId;

    @ApiModelProperty(value = "owner名称", allowEmptyValue = true)
    private String ownerName;

    @ApiModelProperty(value = "区域", allowEmptyValue = true)
    private String region;

    @ApiModelProperty(value = "业务线", allowEmptyValue = true)
    private String bizDef;

    @ApiModelProperty(value = "租户", allowEmptyValue = true)
    private String tenant;

    @ApiModelProperty(value = "产品线", allowEmptyValue = true)
    private String productDef;

    @ApiModelProperty(value = "消费类型", allowEmptyValue = true)
    private String topicType;
}
