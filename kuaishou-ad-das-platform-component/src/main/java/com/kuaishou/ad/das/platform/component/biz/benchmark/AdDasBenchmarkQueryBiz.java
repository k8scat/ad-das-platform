package com.kuaishou.ad.das.platform.component.biz.benchmark;

import java.util.List;

import com.kuaishou.ad.das.platform.component.web.param.AdDasBenchmarkSearchDataParam;
import com.kuaishou.ad.das.platform.component.web.view.AdDasBenchmarkCheckDataView;
import com.kuaishou.ad.das.platform.component.web.view.AdDasBenchmarkData;
import com.kuaishou.ad.das.platform.component.web.view.AdDasBenchmarkFileData;


/**
 * adDas 基准查询
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-08-15
 */
public interface AdDasBenchmarkQueryBiz {

    /**
     * 查询 基准文件
     * @param taskTimestamp 时间戳
     * @param benchmarkId 基准流 id
     * @return 文件元数据
     */
    List<AdDasBenchmarkFileData> queryBenchmarkFiles(Long taskTimestamp, String benchmarkType, Long benchmarkId);

    /**
     * 查询基准 dumpInfo
     * @param taskTimestamp 时间戳
     * @param benchmarkId 基准流id
     * @return 文件元数据
     */
    String queryDumpInfo(Long taskTimestamp, String benchmarkType, Long benchmarkId);


    /**
     * 预加载 or 校验数据文件
     * @param param 查询参数
     * @return 文件信息
     */
    AdDasBenchmarkCheckDataView checkDataFile(AdDasBenchmarkSearchDataParam param);


    /**
     * 搜索 基准数据
     * @param param 查询参数
     * @return result
     */
    List<AdDasBenchmarkData> searchData(AdDasBenchmarkSearchDataParam param);

    /**
     * 下载基准数据
     * @param param 查询参数
     * @return ByteArrayList
     */
    List<byte[]> downloadData(AdDasBenchmarkSearchDataParam param);
}
