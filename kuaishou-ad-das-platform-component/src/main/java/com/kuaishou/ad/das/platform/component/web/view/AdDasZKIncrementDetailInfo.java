package com.kuaishou.ad.das.platform.component.web.view;

import lombok.Data;

/**
 * 增量变更ZK 信息
 *
 * @author liutao <liutao06@kuaishou.com>
 * Created on 2021-01-10
 */
@Data
public class AdDasZKIncrementDetailInfo {

    private String mainTable;

    private String pbClass;

}
