package com.kuaishou.ad.das.platform.component.web.view.benchmark.manage;

import lombok.Builder;
import lombok.Data;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-11-22
 */
@Data
@Builder
public class AdDasBenchmarkManageHomeDetailView {

    private long taskId;

    private String taskName;

    private Integer taskNum;

    private String mainDataSource;

    private Long benchmarkId;

    private String benchmarkName;

    private String mainTableName;

    private String pbClassName;

}
