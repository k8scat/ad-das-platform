package com.kuaishou.ad.das.platform.component.handle;

import java.util.Collections;
import java.util.List;

import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import com.kuaishou.ad.das.platform.dal.dao.AdDasBenchmarkCondition;
import com.kuaishou.ad.das.platform.dal.dao.AdDasDataStreamGroupCondition;
import com.kuaishou.ad.das.platform.dal.model.AdDasBenchmark;
import com.kuaishou.ad.das.platform.dal.model.AdDasDataStreamGroup;
import com.kuaishou.ad.das.platform.dal.repository.AdDasBenchmarkRepository;
import com.kuaishou.ad.das.platform.dal.repository.AdDasDataStreamGroupRepository;

/**
 * @author Zhang Jialin <zhangjialin@kuaishou.com>
 * Created on 2021-06-18
 */
@Component
public class BenchmarkModifyHandler {
    @Autowired
    private AdDasBenchmarkRepository benchmarkRepository;

    @Autowired
    private AdDasDataStreamGroupRepository groupRepository;

    public Pair<AdDasBenchmark, AdDasDataStreamGroup> getBenchmarkAndGroup(Long benchmarkId) {
        AdDasBenchmarkCondition benchmarkCondition = new AdDasBenchmarkCondition();
        benchmarkCondition.setBenchmarkIds(Collections.singletonList(benchmarkId));
        AdDasBenchmark benchmark = null;
        AdDasDataStreamGroup group = null;
        List<AdDasBenchmark> benchmarks = benchmarkRepository.query(benchmarkCondition);
        if (!CollectionUtils.isEmpty(benchmarks)) {
            benchmark = benchmarks.get(0);
            AdDasDataStreamGroupCondition groupCondition = new AdDasDataStreamGroupCondition();
            groupCondition.setIds(Collections.singletonList(benchmark.getDataStreamGroupId()));
            List<AdDasDataStreamGroup> dataStreamGroups = groupRepository.query(groupCondition);
            group = dataStreamGroups.get(0);
        }
        return Pair.of(benchmark, group);
    }
}
