package com.kuaishou.ad.das.platform.component.web.view.benchmark.develop;

import lombok.Data;

/**
 * @author wangkai <wangkai14@kuaishou.com>
 * Created on 2020-12-24
 */
@Data
public class AdDasBenchmarkDevelopHomeDetailView {

    private long taskId;

    private String taskName;

    private Integer taskNum;

    private String mainDataSource;

    private String benchmarkTypeName;

    private String mainTableName;

    private String pbClassName;

    private Integer taskStatus;

    private String operator;

    private Long createTime;

    private Long updateTime;
}
