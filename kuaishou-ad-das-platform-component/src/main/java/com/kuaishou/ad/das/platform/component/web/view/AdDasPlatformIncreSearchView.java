package com.kuaishou.ad.das.platform.component.web.view;

import java.util.List;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-05-24
 */
@Data
public class AdDasPlatformIncreSearchView {

    @ApiModelProperty(value = "增量流查询结果列表")
    private List<AdDasPlatformIncrementGraphView> incrementGraphList;

    @ApiModelProperty(value = "总数")
    private Integer totalCount;
}
