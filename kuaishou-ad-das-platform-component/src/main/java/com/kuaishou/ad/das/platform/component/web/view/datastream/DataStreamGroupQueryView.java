package com.kuaishou.ad.das.platform.component.web.view.datastream;

import java.util.Collections;
import java.util.List;

import com.kuaishou.ad.das.platform.component.web.PageData;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author Zhang Jialin <zhangjialin@kuaishou.com>
 * Created on 2021-05-18
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel
public class DataStreamGroupQueryView extends PageData {
    @ApiModelProperty(value = "数据列表")
    private List<DataStreamGroupItem> dataList;

    public static DataStreamGroupQueryView getDefaultInstance() {
        DataStreamGroupQueryView instance = new DataStreamGroupQueryView();
        instance.setDataList(Collections.emptyList());
        instance.setTotalCount(0L);
        return instance;
    }
}
