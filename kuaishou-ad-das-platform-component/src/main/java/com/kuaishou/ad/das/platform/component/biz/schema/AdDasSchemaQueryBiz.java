package com.kuaishou.ad.das.platform.component.biz.schema;

import java.util.List;

import com.kuaishou.ad.das.platform.component.web.view.AdDasSchemaHomePageView;
import com.kuaishou.ad.das.platform.component.web.view.AdDasSchemaPbClassView;
import com.kuaishou.ad.das.platform.component.web.view.AdDasSchemaPbPreviewView;
import com.kuaishou.ad.das.platform.component.web.view.AdDasSchemaPbView;
import com.kuaishou.ad.das.platform.component.web.view.AdDasStatusView;
import com.kuaishou.ad.das.platform.component.web.view.AdDasTableMsgView;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-11-27
 */
public interface AdDasSchemaQueryBiz {

    /**
     * 元数据管理首页
     *
     * @return
     */
    AdDasSchemaHomePageView schemaHomePage();

    /**
     * 根据PB包版本查询
     *
     * @param pbJarVersion
     * @return
     */
    AdDasSchemaPbView querySchemaPb(String pbJarVersion, String pbClass, Integer pbStatus);

    /**
     * 查询系统标记的最新PB包信息
     *
     * @return
     */
    String queryLatestSchemaPb();

    /**
     * 查询PB类对应的 类字段信息
     *
     * @param pbJarVersion
     * @param pbClassName
     * @return
     */
    List<AdDasSchemaPbClassView> querySchemaPbClass(String pbJarVersion, String pbClassName);

    /**
     *
     * @param pbJarVersion
     * @param pbClassName
     * @return
     */
    List<AdDasSchemaPbClassView> querySchemaPbClassFromDb(String pbJarVersion, String pbClassName);

    /**
     * 预加载PB jar 信息
     *
     * @param pbJarVersion
     * @return
     */
    List<AdDasSchemaPbPreviewView> previewPbJar(String pbJarVersion);

    /**
     *
     * @param pbJarVersion
     * @return
     */
    List<String> queryClassList(String pbJarVersion);

    /**
     * 查询服务列表信息
     *
     * @return
     */
    List<String> queryServiceList(String serviceType);

    /**
     * 查询服务对应的 PB 版本信息
     *
     * @param serviceName
     * @return
     */
    String queryServicePb(String serviceName);

    /**
     * 查询已标记的PB包版本信息
     *
     * @return
     */
    List<String> queryPbVersions();

    /**
     * 查询数据 DB 中所有表信息
     *
     * @param dataSource
     * @return
     */
    List<String> queryTableAll(String dataSource);

    /**
     * 查询系统当前 Pb包 标记的 最新PB类列表
     *
     * @return
     */
    List<String> queryPbAll();

    /**
     * 查询 pb 枚举列表
     * @return
     */
    List<String> queryPbEnum();

    /**
     * 查询PB状态枚举
     *
     * @return
     */
    List<AdDasStatusView> queryPbStatus();

    /**
     * 根据表名称查询表所有字段
     *
     * @param tableName
     * @return
     */
    List<AdDasTableMsgView> queryTableMsg(String dataSource, String tableName);

}
