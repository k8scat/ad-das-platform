package com.kuaishou.ad.das.platform.component.web.param.benchmark;

import java.util.List;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author Zhang Jialin <zhangjialin@kuaishou.com>
 * Created on 2021-05-26
 */
@Data
public class BenchmarkModifyQueryParam {
    @ApiModelProperty(value = "基准id", allowEmptyValue = true)
    private List<Long> benchmarkIds;
    @ApiModelProperty(value = "表名", allowEmptyValue = true)
    private String mainTableName;
    @ApiModelProperty(value = "操作人邮箱前缀", allowEmptyValue = true)
    private String operatorEmailPrefix;
    @ApiModelProperty(value = "变更名称", allowEmptyValue = true)
    private String modifyName;
    @ApiModelProperty(value = "开始时间", allowEmptyValue = true)
    private Long startTime;
    @ApiModelProperty(value = "结束时间", allowEmptyValue = true)
    private Long endTime;
    @ApiModelProperty(value = "页码", notes = "传-1时返回全部")
    private Integer pageNum;
    @ApiModelProperty(value = "单页条数", allowEmptyValue = true)
    private Integer pageSize;
}
