package com.kuaishou.ad.das.platform.component.biz.finance;

import java.util.List;

import com.kuaishou.ad.das.platform.utils.model.AdDasUserView;

/**
 * 财务管家
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-08-06
 */
public interface AdDasFinanceManagerBiz {

    /**
     * 下载广告主余额数据
     * @param accountIds
     * @return
     */
    List<byte[]> downloadAccountBalanceData(String accountIds, AdDasUserView adDasUserView);

}
