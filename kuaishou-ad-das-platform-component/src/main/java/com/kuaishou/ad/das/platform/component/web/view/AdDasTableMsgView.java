package com.kuaishou.ad.das.platform.component.web.view;

import java.io.Serializable;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class AdDasTableMsgView implements Serializable {
    private static final long serialVersionUID = 4619877136625323894L;

    @ApiModelProperty(value = "列名")
    private String columnName;

    @ApiModelProperty(value = "列类型")
    private String columnType;
}
