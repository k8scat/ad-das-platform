package com.kuaishou.ad.das.platform.component.web.param;

import java.io.Serializable;
import java.util.List;

import com.kuaishou.ad.das.platform.component.web.view.AdDasSchemaPbClassView;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-11-29
 */
@Data
public class AdDasPbClassColumnParam implements Serializable {

    private static final long serialVersionUID = 1298394032855698306L;

    @ApiModelProperty(value = "PB包版本")
    private String pbVersion;

    @ApiModelProperty(value = "PB类名")
    private String pbClassName;

    @ApiModelProperty(value = "PB类字段信息")
    private List<AdDasSchemaPbClassView>  pbClassColumns;
}
