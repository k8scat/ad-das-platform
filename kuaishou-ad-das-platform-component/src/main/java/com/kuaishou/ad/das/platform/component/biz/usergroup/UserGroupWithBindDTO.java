package com.kuaishou.ad.das.platform.component.biz.usergroup;

import com.kuaishou.ad.das.platform.dal.model.AdDasDownstreamUserInfo;
import com.kuaishou.ad.das.platform.dal.model.AdDasDownstreamUserRelationBind;

import lombok.Data;

/**
 * @author Zhang Jialin <zhangjialin@kuaishou.com>
 * Created on 2021-06-14
 */
@Data
public class UserGroupWithBindDTO {
    /**
     * 基准id
     */
    private Long userGroupBindId;
    /**
     * 数据流id
     */
    private Long dataStreamId;
    /**
     * 数据流类型:0-基准；1-增量
     */
    private Integer dataStreamType;
    /**
     * 用户组id
     */
    private Long userGroupId;
    /**
     * 用户组名称
     */
    private String userGroupName;
    /**
     * 负责人邮箱前缀
     */
    private String ownerEmailPrefix;
    /**
     * 负责人中文名
     */
    private String ownerName;

    public static UserGroupWithBindDTO build(AdDasDownstreamUserRelationBind bind, AdDasDownstreamUserInfo userGroup) {
        UserGroupWithBindDTO dto = new UserGroupWithBindDTO();
        dto.setUserGroupId(bind.getUserGroupId());
        dto.setDataStreamId(bind.getDataStreamId());
        dto.setUserGroupName(userGroup.getUserGroupName());
        dto.setUserGroupBindId(bind.getId());
        dto.setDataStreamType(bind.getDataStreamType());
        dto.setOwnerName(userGroup.getOwnerName());
        dto.setOwnerEmailPrefix(userGroup.getOwnerEmailPrefix());
        return dto;
    }
}
