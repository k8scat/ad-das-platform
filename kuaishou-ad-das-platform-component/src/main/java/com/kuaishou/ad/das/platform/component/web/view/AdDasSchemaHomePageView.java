package com.kuaishou.ad.das.platform.component.web.view;

import java.io.Serializable;
import java.util.List;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-11-22
 */
@Data
public class AdDasSchemaHomePageView implements Serializable {

    @ApiModelProperty(value = "当前最新PB包版本")
    private String pbJarVersion;

    @ApiModelProperty(value = "当前最新PB包版本")
    private List<AdDasSchemaServiceView> schemaServiceViews;
}
