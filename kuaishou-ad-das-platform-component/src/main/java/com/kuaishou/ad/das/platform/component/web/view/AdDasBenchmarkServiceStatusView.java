package com.kuaishou.ad.das.platform.component.web.view;

import java.io.Serializable;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-08-02
 */
@Data
public class AdDasBenchmarkServiceStatusView implements Serializable {

    private static final long serialVersionUID = -4151464053708077287L;

    @ApiModelProperty(value = "任务状态code")
    private Integer status;

    @ApiModelProperty(value = "任务状态描述")
    private String statusDesc;

    @ApiModelProperty(value = "任务下次运行时间")
    private Long nextRunTime;
}
