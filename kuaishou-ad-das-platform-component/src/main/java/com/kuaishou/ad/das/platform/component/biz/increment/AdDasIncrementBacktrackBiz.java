package com.kuaishou.ad.das.platform.component.biz.increment;

import com.kuaishou.ad.das.platform.component.web.param.AdDasIncrementBacktrackDataReSendParam;
import com.kuaishou.ad.das.platform.component.web.param.AdDasIncrementRecondDataReSendParam;
import com.kuaishou.ad.das.platform.component.web.param.AdDasIncrementRecondTableDataReSendParam;
import com.kuaishou.ad.das.platform.component.web.param.AdDasIncrementServiceDataReSendParam;
import com.kuaishou.ad.das.platform.component.web.param.AdDasIncrementServiceTableDataReSendParam;
import com.kuaishou.ad.das.platform.component.web.view.AdDasPlatformIncreBackRecondSearchView;
import com.kuaishou.ad.das.platform.component.web.view.AdDasPlatformIncreBackServiceSearchView;
import com.kuaishou.ad.das.platform.utils.model.AdDasUserView;

/**
 * @author caoyinhang <caoyinhang@kuaishou.com>
 * Created on 2021-07-06
 */
public interface AdDasIncrementBacktrackBiz {

    /**
     * 查询增量流中的 topic 列表
     * @return
     */
    AdDasIncrementServiceTableDataReSendParam getServiceTableHead();

    /**
     * 数据流增量搜索列表
     * @param incrementServiceDataReSendParam
     * @return
     */
    AdDasPlatformIncreBackServiceSearchView increBacktrackServiceSearch(AdDasIncrementServiceDataReSendParam incrementServiceDataReSendParam);

    AdDasIncrementRecondTableDataReSendParam getRecondTableHead();

    AdDasPlatformIncreBackRecondSearchView increBacktrackRecondSearch(AdDasIncrementRecondDataReSendParam incrementRecondDataReSendParam, Integer status);

    String backtrack(AdDasIncrementBacktrackDataReSendParam incrementBacktrackDataReSendParam,
            AdDasUserView dasUserView);


}
