package com.kuaishou.ad.das.platform.component.web.view.common;

import java.util.Map;

import com.kuaishou.ad.das.platform.dal.model.AdDasBenchmarkTaskCommonDetail;
import com.kuaishou.ad.das.platform.dal.model.AdDasModify;
import com.kuaishou.ad.das.platform.utils.model.PlatformUtils;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 数据来源：ad_das_benchmark_task_common_detail(_edit)
 * @author Zhang Jialin <zhangjialin@kuaishou.com>
 * Created on 2021-05-18
 */
@Data
@ApiModel(value = "简易表信息", description = "只包含主表信息，不包含PB映射和级联表信息")
public class MainTableSimpleView {
    @ApiModelProperty(value = "id")
    private Long commonDetailId;
    @ApiModelProperty(value = "表名称")
    private String mainTableName;
    @ApiModelProperty(value = "数据源名称")
    private String dataSourceName;
    @ApiModelProperty(value = "PB类名")
    private String pbClassSimpleName;
    @ApiModelProperty(value = "PB类全限定名名")
    private String pbClassFullyQualifiedName;
    @ApiModelProperty(value = "操作人中文名称")
    private String operationName;
    @ApiModelProperty(value = "操作人邮箱前缀")
    private String operationEmailPrefix;
    @ApiModelProperty(value = "审核人中文名称")
    private String auditorName;
    @ApiModelProperty(value = "审核人邮箱前缀")
    private String auditorEmailPrefix;
    @ApiModelProperty(value = "创建时间")
    private Long createTime;
    @ApiModelProperty(value = "更新时间")
    private Long updateTime;

    public static MainTableSimpleView build(AdDasBenchmarkTaskCommonDetail commonDetail,
            Map<Long, AdDasModify> modifyId2Modify) {
        AdDasModify modify = modifyId2Modify.get(commonDetail.getModifyId());
        MainTableSimpleView vo = new MainTableSimpleView();
        vo.setCommonDetailId(commonDetail.getId());
        vo.setMainTableName(commonDetail.getMainTable());
        vo.setDataSourceName(commonDetail.getDataSource());
        vo.setPbClassSimpleName(commonDetail.getPbClass());
        vo.setPbClassFullyQualifiedName(PlatformUtils.getPbFullClassName(commonDetail.getPbClass()));
        if (modify != null) {
            vo.setAuditorName(modify.getApprover());
            vo.setAuditorEmailPrefix(modify.getApprover());
            vo.setOperationName(modify.getOperator());
            vo.setOperationEmailPrefix(modify.getOperator());
        } else {
            vo.setAuditorName("");
            vo.setAuditorEmailPrefix("");
            vo.setOperationName("");
            vo.setOperationEmailPrefix("");
        }
        vo.setCreateTime(commonDetail.getCreateTime());
        vo.setUpdateTime(commonDetail.getUpdateTime());
        return vo;
    }
}
