package com.kuaishou.ad.das.platform.component.biz.benchmark.impl;

import static com.kuaishou.ad.das.platform.core.utils.AdDasBenchmarkUtils.buildPbClassName;
import static com.kuaishou.ad.das.platform.core.utils.AdDasBenchmarkUtils.getBenchmarkDirPath;
import static com.kuaishou.ad.das.platform.core.utils.AdDasBenchmarkUtils.getBenchmarkFilePath;
import static com.kuaishou.ad.das.platform.utils.exception.ErrorCode.DATA_NOT_FOUND;
import static com.kuaishou.ad.das.platform.utils.exception.ErrorCode.PARAM_ERROR;
import static com.kuaishou.ad.das.platform.utils.kconf.AdDasPlatformKconf.adDasPlatformLoadClassFromCache;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.protobuf.GeneratedMessageV3;
import com.google.protobuf.MessageOrBuilder;
import com.google.protobuf.util.JsonFormat;
import com.kuaishou.ad.das.platform.component.biz.benchmark.AdDasBenchmarkQueryBiz;
import com.kuaishou.ad.das.platform.component.process.AdDasBenchmarkCommonProcess;
import com.kuaishou.ad.das.platform.component.process.AdDasBenchmarkDumpInfoProcess;
import com.kuaishou.ad.das.platform.component.web.param.AdDasBenchmarkSearchDataParam;
import com.kuaishou.ad.das.platform.component.web.view.AdDasBenchmarkCheckDataView;
import com.kuaishou.ad.das.platform.component.web.view.AdDasBenchmarkData;
import com.kuaishou.ad.das.platform.component.web.view.AdDasBenchmarkFileData;
import com.kuaishou.ad.das.platform.core.common.pbloader.AdDasApiPbLoader;
import com.kuaishou.ad.das.platform.core.utils.AdBaseUtils;
import com.kuaishou.ad.das.platform.dal.model.AdDasBenchmark;
import com.kuaishou.ad.das.platform.dal.repository.AdDasBenchmarkRepository;
import com.kuaishou.ad.das.platform.utils.HdfsUtils;
import com.kuaishou.ad.das.platform.utils.exception.AdDasServiceException;
import com.kuaishou.ad.model.protobuf.tables.Meta;
import com.kuaishou.framework.util.ObjectMapperUtils;
import com.kuaishou.kconf.client.Kconf;
import com.kuaishou.kconf.client.Kconfs;

import lombok.extern.slf4j.Slf4j;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-08-15
 */
@Lazy
@Slf4j
@Service
public class AdDasBenchmarkQueryBizImpl implements AdDasBenchmarkQueryBiz {

    private static final Map<String, String> TABLE_PRIMARY_KEY_MAP = Maps.newHashMap();

    static {
        TABLE_PRIMARY_KEY_MAP.put("ad_dsp_account", "id");
        TABLE_PRIMARY_KEY_MAP.put("ad_dsp_account_support_info", "accountId");
        TABLE_PRIMARY_KEY_MAP.put("ad_dsp_agent", "agentId");
        TABLE_PRIMARY_KEY_MAP.put("ad_dsp_agent_account", "accountId");
        TABLE_PRIMARY_KEY_MAP.put("ad_dsp_app", "appId");
        TABLE_PRIMARY_KEY_MAP.put("ad_dsp_campaign", "id");
        TABLE_PRIMARY_KEY_MAP.put("ad_dsp_card_show_data", "id");
        TABLE_PRIMARY_KEY_MAP.put("ad_dsp_cover", "creativeId");
        TABLE_PRIMARY_KEY_MAP.put("ad_dsp_creative", "id");
        TABLE_PRIMARY_KEY_MAP.put("ad_dsp_creative_advanced_programmed", "id");
        TABLE_PRIMARY_KEY_MAP.put("ad_dsp_creative_barrages", "id");
        TABLE_PRIMARY_KEY_MAP.put("ad_dsp_creative_preview", "id");
        TABLE_PRIMARY_KEY_MAP.put("ad_dsp_creative_support_info", "creativeId");
        TABLE_PRIMARY_KEY_MAP.put("ad_dsp_industry_v3", "id");
        TABLE_PRIMARY_KEY_MAP.put("ad_dsp_material", "id");
        TABLE_PRIMARY_KEY_MAP.put("ad_dsp_merchant_app_unit", "id");
        TABLE_PRIMARY_KEY_MAP.put("ad_dsp_package_bg", "bgId");
        TABLE_PRIMARY_KEY_MAP.put("ad_dsp_photo", "photoId");
        TABLE_PRIMARY_KEY_MAP.put("ad_dsp_position", "id");
        TABLE_PRIMARY_KEY_MAP.put("ad_dsp_site_ext_info", "id");
        TABLE_PRIMARY_KEY_MAP.put("ad_dsp_target", "id");
        TABLE_PRIMARY_KEY_MAP.put("ad_dsp_target_paid_audience", "id");
        TABLE_PRIMARY_KEY_MAP.put("ad_dsp_trace_util", "id");
        TABLE_PRIMARY_KEY_MAP.put("ad_dsp_unit_small_shop", "unitId");
        TABLE_PRIMARY_KEY_MAP.put("ad_dsp_unit_support_info", "unitId");
        TABLE_PRIMARY_KEY_MAP.put("ad_dsp_unit_target", "id");
        TABLE_PRIMARY_KEY_MAP.put("ad_dsp_unit_target_base", "id");
        TABLE_PRIMARY_KEY_MAP.put("ad_dsp_upload_population_orientation", "orientationId");
        TABLE_PRIMARY_KEY_MAP.put("ad_risk_account_initiative", "creativeId");
        TABLE_PRIMARY_KEY_MAP.put("ad_risk_industry_initiative", "secondIndustryId");
        TABLE_PRIMARY_KEY_MAP.put("ad_risk_industry_white_account", "id");
        TABLE_PRIMARY_KEY_MAP.put("ad_risk_photo_initiative", "id");
        TABLE_PRIMARY_KEY_MAP.put("ad_risk_target", "id");
        TABLE_PRIMARY_KEY_MAP.put("ad_risk_unit_initiative", "id");
        TABLE_PRIMARY_KEY_MAP.put("ad_dpa_category_target", "id");
        TABLE_PRIMARY_KEY_MAP.put("ad_dpa_creative_advanced", "id");
        TABLE_PRIMARY_KEY_MAP.put("ad_dsp_small_shop_product_spu", "spuId");
    }

    protected static Kconf<Map<String, String>> adDastable2PrimaryKeyMap = Kconfs.ofStringMap(
            "ad.adcore.adDastable2PrimaryKeyMap", TABLE_PRIMARY_KEY_MAP).build();

    @Autowired
    private AdDasBenchmarkDumpInfoProcess adDasBenchmarkDumpInfoProcess;

    @Autowired
    private AdDasApiPbLoader adDasApiPbLoader;

    @Autowired
    private AdDasBenchmarkRepository adDasBenchmarkRepository;

    @Override
    public List<AdDasBenchmarkFileData> queryBenchmarkFiles(Long taskTimestamp, String benchmarkType, Long benchmarkId) {

        // 查询 对应的path数据
        List<AdDasBenchmarkFileData> fileViewList = Lists.newArrayList();

        checkQueryBenchmarkParam(benchmarkType, benchmarkId);

        String dirPath = getFilePath(taskTimestamp, benchmarkType, benchmarkId);
        log.info("dirPath={}", dirPath);
        try {
            List<String> fileLists = HdfsUtils.listPath(dirPath);
            log.info("fileLists={}", fileLists);
            List<String> fileNameList = fileLists.stream()
                    .map(filePath -> {
                        int index = StringUtils.lastIndexOf(filePath, "/");
                        return StringUtils.substring(filePath, index + 1);
                    })
                    .collect(Collectors.toList());

            // 查询dump_info信息
            String dumpInfoPath = dirPath + "/dump_info";
            log.info("dumpInfoPath={}", dumpInfoPath);
            HdfsUtils.processPath(dumpInfoPath, adDasBenchmarkDumpInfoProcess);
            // 生成新的字符串
            Meta.DumpInfo dumpInfo = adDasBenchmarkDumpInfoProcess.getDumpInfo();

            Map<String, Meta.DumpData> fileName2DumpDataMap = dumpInfo.getInfoList().stream()
                    .collect(Collectors.toMap(Meta.DumpData::getFileName, dumpData -> dumpData));

            Map<String, Long> dataFileSizeMap = HdfsUtils.listPathAndSizeMap(dirPath);

            for (String fileName : fileNameList) {
                AdDasBenchmarkFileData fileView = new AdDasBenchmarkFileData();
                fileView.setFileName(fileName);
                Meta.DumpData dumpData = fileName2DumpDataMap.get(fileName);
                if (dumpData == null) {
                    log.error("fileName={} does not have dumpInfoData!", fileName);
                    continue;
                }

                fileView.setPbFullName(dumpData.getProtoName());
                fileView.setRecordNum(dumpData.getRecordNum());
                fileView.setTableName(dumpData.getTableName());

                Long dataSize = Optional.ofNullable(dataFileSizeMap.get(fileName)).orElse(0L);
                fileView.setFileSize(dataSize);

                fileViewList.add(fileView);
            }

        } catch (Exception e) {
            log.error("queryBenchmarkFiles() occur error", e);
        }

        return fileViewList;
    }

    private String getFilePath(Long taskTimestamp, String benchmarkType, Long benchmarkId) {

        String dirPath = "";
        if (benchmarkId != null && benchmarkId>0) {
            // 获取基准流英文名称
            List<AdDasBenchmark> benchmarks = adDasBenchmarkRepository.queryById(Lists.newArrayList(benchmarkId));
            if (CollectionUtils.isEmpty(benchmarks)) {
                throw AdDasServiceException.ofMessage(DATA_NOT_FOUND);
            }
            String benchmarkHdfsPath = benchmarks.get(0).getHdfsPath();
            dirPath = benchmarkHdfsPath + AdBaseUtils.timeToString(taskTimestamp);
        } else {

            if (benchmarkType.startsWith("platform<")) {
                log.info("benchmarkType={}", benchmarkType);
                String[] benchmarkTypes = benchmarkType.split("<");
                log.info("benchmarkTypes={}", Arrays.stream(benchmarkTypes).toArray());
                AdDasBenchmark adDasBenchmark = adDasBenchmarkRepository.listByName(benchmarkTypes[1]);
                dirPath = adDasBenchmark.getHdfsPath() + AdBaseUtils.timeToString(taskTimestamp);
            } else {
                dirPath = getBenchmarkDirPath(benchmarkType, taskTimestamp);
            }
        }

        return dirPath;
    }

    @Override
    public String queryDumpInfo(Long taskTimestamp, String benchmarkType, Long benchmarkId) {

        checkQueryBenchmarkParam(benchmarkType, benchmarkId);

        String result = "";

        String dirPath = getFilePath(taskTimestamp, benchmarkType, benchmarkId);
        log.info("dirPath={}", dirPath);
        String dumpInfoPath = dirPath + "/dump_info";
        try {

            HdfsUtils.processPath(dumpInfoPath, adDasBenchmarkDumpInfoProcess);

            // 生成新的字符串
            Meta.DumpInfo dumpInfo = adDasBenchmarkDumpInfoProcess.getDumpInfo();

            return ObjectMapperUtils.toJSON(dumpInfo.getInfoList());
        } catch (Exception e) {
            log.error("queryDumpInfo() occur error", e);
        }

        return result;
    }

    /**
     * 参数校验
     * @param benchmarkType
     * @param benchmarkId
     */
    private void checkQueryBenchmarkParam(String benchmarkType, Long benchmarkId) {

        if (StringUtils.isEmpty(benchmarkType) && (benchmarkId == null || benchmarkId<=0)) {
            throw AdDasServiceException.ofMessage(PARAM_ERROR);
        }
    }

    @Override
    public AdDasBenchmarkCheckDataView checkDataFile(AdDasBenchmarkSearchDataParam param) {

        AdDasBenchmarkCheckDataView resultView = new AdDasBenchmarkCheckDataView();
        try {
            // 先查 DumpInfo 数据
            String dumpInfoPath = getFilePath(param.getTimestamp(), param.getBenchmarkType(), param.getBenchmarkId()) + "/dump_info";

            // 先查 DumpInfo 数据
            HdfsUtils.processPath(dumpInfoPath, adDasBenchmarkDumpInfoProcess);

            // 生成新的字符串
            Meta.DumpInfo dumpInfo = adDasBenchmarkDumpInfoProcess.getDumpInfo();

            String pbFullName = "";
            String tableName = "";
            Meta.DumpData rightDumpData = null;
            for (Meta.DumpData dumpData : dumpInfo.getInfoList()) {

                if (param.getFileName().equals(dumpData.getFileName())) {
                    pbFullName = dumpData.getProtoName();
                    rightDumpData = dumpData;
                    tableName = dumpData.getTableName();
                }
            }

            // 根据dumpInfo 的数据 解析出 file 对应的 pb 名称
            String className = buildPbClassName(pbFullName);
            // 再查 数据文件
            // 再查 数据文件
            String dataFilePath = getFilePath(param.getTimestamp(), param.getBenchmarkType(), param.getBenchmarkId())
                    + "/" + param.getFileName();

            AdDasBenchmarkCommonProcess process = new AdDasBenchmarkCommonProcess();

            Class<?> cl = Class.forName(className);
            // 切换从 reload Cache 中加载 class
            if (adDasPlatformLoadClassFromCache.get()) {
                cl = adDasApiPbLoader.loadClassFromCache(className);
            }

            HdfsUtils.processPathWithPoto(dataFilePath, process, cl);
            List<Object> datas = process.getDataList();
            if (CollectionUtils.isEmpty(datas)) {
                return resultView;
            }

            resultView.setFileName(rightDumpData.getFileName());
            resultView.setTableName(rightDumpData.getTableName());
            resultView.setRecordNum(rightDumpData.getRecordNum());

            // 填充数据明细
            fillDataDetail(resultView, datas, className, tableName);

            process.clear();
        } catch (Exception e) {
            log.error("checkFile() occur error!", e);
        }

        return resultView;
    }

    @Override
    public List<AdDasBenchmarkData> searchData(AdDasBenchmarkSearchDataParam param) {

        List<AdDasBenchmarkData> result = Lists.newArrayList();
        try {

            Meta.DumpInfo dumpInfo = queryDumpInfo(param);

            String pbFullName = "";
            String tableName = "";
            for (Meta.DumpData dumpData : dumpInfo.getInfoList()) {

                if (param.getFileName().equals(dumpData.getFileName())) {
                    pbFullName = dumpData.getProtoName();
                    tableName = dumpData.getTableName();
                }
            }

            List<Object> datas = queryData(param, pbFullName);

            if (CollectionUtils.isEmpty(datas)) {
                return result;
            }

            String idKey = param.getIdKey();
            Set<String> idSet = new HashSet<>(param.getIds());

            // 反射查询方法
            String name = idKey.substring(0, 1).toUpperCase() + idKey.substring(1, idKey.length());
            String nameString = "get" + name;
            Method method = datas.get(0).getClass().getDeclaredMethod(nameString);

            // 反射查询主键
            String primaryKey = "";
            Method methodPrimaryKey = null;
            if (adDastable2PrimaryKeyMap.get().containsKey(tableName)) {
                primaryKey = adDastable2PrimaryKeyMap.get().get(tableName);
                String primaryIdKey =
                        primaryKey.substring(0, 1).toUpperCase() + primaryKey.substring(1, primaryKey.length());
                String namePrimaryKey = "get" + primaryIdKey;
                methodPrimaryKey = datas.get(0).getClass().getDeclaredMethod(namePrimaryKey);
                log.info("methodPrimaryKey={}", methodPrimaryKey);
            }

            // 反射获取 class  以及对应的 id key 信息
            // 遍历解析出 对应的 ids 数据
            Method finalMethodPrimaryKey = methodPrimaryKey;
            String finalPrimaryKey = primaryKey;
            datas.parallelStream().forEach(data -> {
                try {
                    String idValue = ((Number) method.invoke(data)).toString();

                    if (idSet.contains(idValue)) {
                        log.info("idValue={}", idValue);
                        AdDasBenchmarkData dasBenchmarkDataView = new AdDasBenchmarkData();
                        dasBenchmarkDataView.setDataJson(JsonFormat.printer().print((MessageOrBuilder) data));

                        if (finalMethodPrimaryKey != null) {
                            String primaryIdvalue = finalMethodPrimaryKey.invoke(data).toString();
                            dasBenchmarkDataView.setId(finalPrimaryKey + ": " + primaryIdvalue);
                        } else {
                            dasBenchmarkDataView.setId("未设置主键id");
                        }
                        result.add(dasBenchmarkDataView);
                    }
                } catch (Exception e) {
                    log.error("searchData() reflect data occur error!", e);
                }
            });

        } catch (Exception e) {
            log.error("searchData() occur error!", e);
        }

        return result;
    }

    public Meta.DumpInfo queryDumpInfo(AdDasBenchmarkSearchDataParam param) throws IOException {
        // 先查 DumpInfo 数据
        String dumpInfoPath = getFilePath(param.getTimestamp(), param.getBenchmarkType(), param.getBenchmarkId()) + "/dump_info";
        // 先查 DumpInfo 数据
        HdfsUtils.processPath(dumpInfoPath, adDasBenchmarkDumpInfoProcess);

        // 生成新的字符串
        Meta.DumpInfo dumpInfo = adDasBenchmarkDumpInfoProcess.getDumpInfo();

        return dumpInfo;
    }

    private List<Object> queryData(AdDasBenchmarkSearchDataParam param, String pbFullName) throws IOException, ClassNotFoundException {

        // 根据dumpInfo 的数据 解析出 file 对应的 pb 名称
        String className = buildPbClassName(pbFullName);
        // 再查 数据文件
        String dataFilePath = getFilePath(param.getTimestamp(), param.getBenchmarkType(), param.getBenchmarkId())
                + "/" + param.getFileName();

        AdDasBenchmarkCommonProcess process = new AdDasBenchmarkCommonProcess();

        Class<?> cl = Class.forName(className);
        // 切换从 reload Cache 中加载 class
        if (adDasPlatformLoadClassFromCache.get()) {
            cl = adDasApiPbLoader.loadClassFromCache(className);
        }

        HdfsUtils.processPathWithPoto(dataFilePath, process, cl);
        return process.getDataList();
    }

    @Override
    public List<byte[]> downloadData(AdDasBenchmarkSearchDataParam param) {

        List<byte[]> result = Lists.newArrayList();

        try {
            Meta.DumpInfo dumpInfo = queryDumpInfo(param);

            String pbFullName = "";
            for (Meta.DumpData dumpData : dumpInfo.getInfoList()) {

                if (param.getFileName().equals(dumpData.getFileName())) {
                    pbFullName = dumpData.getProtoName();
                }
            }

            List<Object> datas = queryData(param, pbFullName);

            result = datas.parallelStream().map(data -> {
                try {
                    return JsonFormat.printer().print((MessageOrBuilder) data).getBytes();
                } catch (Exception e) {
                    log.error("downloadData() parseData occur error!", e);
                }
                return null;
            }).filter(Objects::nonNull).collect(Collectors.toList());

        } catch (Exception e) {
            log.error("downloadData() occur error!", e);
        }

        return result;
    }

    private void fillDataDetail(final AdDasBenchmarkCheckDataView resultView,
            List<Object> data, String className, String tableName) {
        if (CollectionUtils.isEmpty(data)) {
            return;
        }

        try {
            // 反射查询主键
            String primaryKey = "";
            Method methodPrimaryKey = null;
            if (adDastable2PrimaryKeyMap.get().containsKey(tableName)) {
                primaryKey = adDastable2PrimaryKeyMap.get().get(tableName);
                String primaryIdKey =
                        primaryKey.substring(0, 1).toUpperCase() + primaryKey.substring(1, primaryKey.length());
                String namePrimaryKey = "get" + primaryIdKey;
                methodPrimaryKey = data.get(0).getClass().getDeclaredMethod(namePrimaryKey);
                log.info("methodPrimaryKey={}", methodPrimaryKey);
            }

            List<AdDasBenchmarkData> newFirstFiveDatas = Lists.newArrayList();
            List<Object> firstFiveData = data.subList(0, 5);

            for (Object item : firstFiveData) {
                GeneratedMessageV3 messageV3 = (GeneratedMessageV3) item;

                AdDasBenchmarkData dasBenchmarkData = new AdDasBenchmarkData();
                dasBenchmarkData.setDataJson(JsonFormat.printer().print(messageV3));
                if (methodPrimaryKey != null) {
                    String primaryIdvalue = methodPrimaryKey.invoke(item).toString();
                    dasBenchmarkData.setId(primaryKey + ": " + primaryIdvalue);
                } else {
                    dasBenchmarkData.setId("未设置主键id");
                }

                newFirstFiveDatas.add(dasBenchmarkData);
            }
            resultView.setFirstFiveData(newFirstFiveDatas);

            List<AdDasBenchmarkData> newLastFiveData = Lists.newArrayList();
            List<Object> lastFiveData = data.subList(Math.max(data.size() - 6, 0), data.size() - 1);
            for (Object item : lastFiveData) {
                GeneratedMessageV3 messageV3 = (GeneratedMessageV3) item;

                AdDasBenchmarkData dasBenchmarkData = new AdDasBenchmarkData();
                dasBenchmarkData.setDataJson(JsonFormat.printer().print(messageV3));
                if (methodPrimaryKey != null) {
                    String primaryIdValue = methodPrimaryKey.invoke(item).toString();
                    dasBenchmarkData.setId(primaryKey + ": " + primaryIdValue);
                } else {
                    dasBenchmarkData.setId("未设置主键id");
                }
                newLastFiveData.add(dasBenchmarkData);
            }
            resultView.setLastFiveData(newLastFiveData);
        } catch (Exception e) {
            log.error("fillDataDetail() resultView={} className={}", resultView, className, e);
        }
    }
}
