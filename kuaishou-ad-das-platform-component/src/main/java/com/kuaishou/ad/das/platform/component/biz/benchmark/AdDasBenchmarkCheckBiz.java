package com.kuaishou.ad.das.platform.component.biz.benchmark;

import java.util.List;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-08-15
 */
public interface AdDasBenchmarkCheckBiz {

    String runBetaCheckTask(Integer delLock,
                            List<Integer> resourceIdList,
                            Long taskTimestamp,
                            String masterTypeStr);
}
