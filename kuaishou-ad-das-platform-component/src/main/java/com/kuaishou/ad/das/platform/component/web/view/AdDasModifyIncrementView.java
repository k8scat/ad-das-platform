package com.kuaishou.ad.das.platform.component.web.view;

import java.io.Serializable;

import lombok.Data;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-12-25
 */
@Data
public class AdDasModifyIncrementView implements Serializable {

    private static final long serialVersionUID = -664656655004987449L;

    private String modifyName;

    private Integer modifyType;

    private Integer modifyStatus;

    private String operator;

    private String approver;

    private AdDasIncrementServiceView incrementServiceView;

}
