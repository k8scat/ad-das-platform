package com.kuaishou.ad.das.platform.component.web.view;

import lombok.Data;

/**
 * @author Zhang Jialin <zhangjialin@kuaishou.com>
 * Created on 2021-05-24
 */
@Data
public class AdDasModifyIncrementItemView {

    private String modifyName;

    private Integer modifyType;

    private Integer modifyStatus;

    private String operator;

    private String approver;

    private AdDasPlatformIncreTableDetailView incrementServiceView;

}
