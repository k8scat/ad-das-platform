package com.kuaishou.ad.das.platform.component.biz.home.impl;

import static com.kuaishou.ad.das.platform.utils.kconf.AdDasPlatformKconf.adDasPlatformChangLogs;
import static com.kuaishou.ad.das.platform.utils.kconf.AdDasPlatformKconf.adDasPlatformOncall;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import com.kuaishou.ad.das.platform.component.biz.home.AdDasPlatformHomeBiz;
import com.kuaishou.ad.das.platform.component.web.view.AdDasPlatformHomeView;
import com.kuaishou.ad.das.platform.utils.model.AdDasUserView;

import lombok.extern.slf4j.Slf4j;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-08-06
 */
@Lazy
@Slf4j
@Service
public class AdDasPlatformHomeBizImpl implements AdDasPlatformHomeBiz {

    @Override
    public AdDasPlatformHomeView platformHome(AdDasUserView adDasUserView) {

        AdDasPlatformHomeView platformHomeView = new AdDasPlatformHomeView();
        platformHomeView.setOncallName(adDasPlatformOncall.get());
        platformHomeView.setChangeLogs(adDasPlatformChangLogs.get());

        return platformHomeView;
    }
}
