package com.kuaishou.ad.das.platform.component.biz.benchmark.impl;

import static com.kuaishou.ad.das.platform.utils.KimUtil.sendAdDasModifyMsg;
import static com.kuaishou.ad.das.platform.utils.constant.AdDasApiConstant.DAS_BENCHMARK_CLICKURL;
import static com.kuaishou.ad.das.platform.utils.exception.ErrorCode.BENCHMARK_TABLE_TYPE_ERROR;
import static com.kuaishou.ad.das.platform.utils.exception.ErrorCode.CASCADE_TABLE_NEED_PRIMARY_KEY;
import static com.kuaishou.ad.das.platform.utils.exception.ErrorCode.MAIN_TABLE_NEED_PRIMARY_KEY;
import static com.kuaishou.ad.das.platform.utils.kconf.AdDasPlatformKconf.AD_DAS_DATASOURCE_DB_MAPPING;
import static com.kuaishou.ad.das.platform.utils.kconf.AdDasPlatformKconf.adDasPlatformDataAdapterMap;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.google.common.collect.Lists;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.kuaishou.ad.das.platform.component.biz.benchmark.AdDasBenchmarkModifyBiz;
import com.kuaishou.ad.das.platform.component.handle.BenchmarkModifyHandler;
import com.kuaishou.ad.das.platform.component.web.config.AdDasApiPropertiesConfig;
import com.kuaishou.ad.das.platform.component.web.param.AdDasEditBenchmarkStreamParam;
import com.kuaishou.ad.das.platform.component.web.param.AdDasModifyBenchmarkParam;
import com.kuaishou.ad.das.platform.component.web.view.AdDasModifyBenchmarkDataView;
import com.kuaishou.ad.das.platform.core.benchmark.model.AdDasBenchmarkCascadeTableView;
import com.kuaishou.ad.das.platform.core.benchmark.model.AdDasBenchmarkDb2PbColumnMsgView;
import com.kuaishou.ad.das.platform.core.benchmark.model.AdDasBenchmarkMainTableView;
import com.kuaishou.ad.das.platform.dal.model.AdDasBenchmark;
import com.kuaishou.ad.das.platform.dal.model.AdDasBenchmarkMsg;
import com.kuaishou.ad.das.platform.dal.model.AdDasBenchmarkTask;
import com.kuaishou.ad.das.platform.dal.model.AdDasBenchmarkTaskCommonDetail;
import com.kuaishou.ad.das.platform.dal.model.AdDasBenchmarkTaskCommonDetailEdit;
import com.kuaishou.ad.das.platform.dal.model.AdDasBenchmarkTaskCommonDetailHistory;
import com.kuaishou.ad.das.platform.dal.model.AdDasBenchmarkTaskEdit;
import com.kuaishou.ad.das.platform.dal.model.AdDasBenchmarkTaskHistory;
import com.kuaishou.ad.das.platform.dal.model.AdDasDataStreamGroup;
import com.kuaishou.ad.das.platform.dal.model.AdDasModify;
import com.kuaishou.ad.das.platform.dal.model.AdDasTableSchema;
import com.kuaishou.ad.das.platform.dal.repository.AdDasBenchMarkTaskCommonDetailRepository;
import com.kuaishou.ad.das.platform.dal.repository.AdDasBenchmarkMsgRepository;
import com.kuaishou.ad.das.platform.dal.repository.AdDasBenchmarkRepository;
import com.kuaishou.ad.das.platform.dal.repository.AdDasBenchmarkTaskCommonDetailEditRepository;
import com.kuaishou.ad.das.platform.dal.repository.AdDasBenchmarkTaskCommonDetailHistoryRepository;
import com.kuaishou.ad.das.platform.dal.repository.AdDasBenchmarkTaskEditRepository;
import com.kuaishou.ad.das.platform.dal.repository.AdDasBenchmarkTaskHistoryRepository;
import com.kuaishou.ad.das.platform.dal.repository.AdDasBenchmarkTaskRepository;
import com.kuaishou.ad.das.platform.dal.repository.AdDasModifyRepository;
import com.kuaishou.ad.das.platform.dal.repository.AdDasTableSchemaRepository;
import com.kuaishou.ad.das.platform.utils.AdDasBeanCoreUtils;
import com.kuaishou.ad.das.platform.utils.dasenum.AdDasModifyStatusEnum;
import com.kuaishou.ad.das.platform.utils.exception.AdDasServiceException;
import com.kuaishou.ad.das.platform.utils.exception.ErrorCode;
import com.kuaishou.ad.das.platform.utils.model.AdDasUserView;
import com.kuaishou.framework.util.ObjectMapperUtils;
import com.kuaishou.infra.framework.binlog.util.BinlogResolver;

import lombok.extern.slf4j.Slf4j;

/**
 * @author wangkai <wangkai14@kuaishou.com>
 * Created on 2020-12-28
 */
@Slf4j
@Lazy
@Service
public class AdDasBenchmarkModifyBizImpl implements AdDasBenchmarkModifyBiz {

    @Autowired
    private AdDasModifyRepository adDasModifyRepository;

    @Autowired
    private AdDasBenchmarkMsgRepository adDasBenchmarkMsgRepository;

    @Autowired
    private AdDasBenchmarkTaskCommonDetailEditRepository adDasBenchmarkTaskCommonDetailEditRepository;

    @Autowired
    private AdDasTableSchemaRepository adDasTableSchemaRepository;

    @Autowired
    private AdDasBenchmarkTaskEditRepository adDasBenchmarkTaskEditRepository;

    @Autowired
    private AdDasBenchMarkTaskCommonDetailRepository adDasBenchMarkTaskCommonDetailRepository;

    @Autowired
    private AdDasBenchmarkTaskRepository adDasBenchmarkTaskRepository;

    @Autowired
    private AdDasBenchmarkTaskCommonDetailHistoryRepository adDasBenchmarkTaskCommonDetailHistoryRepository;

    @Autowired
    private BenchmarkModifyHandler benchmarkModifyHandler;

    @Autowired
    private AdDasBenchmarkTaskHistoryRepository benchmarkTaskHistoryRepository;

    @Autowired
    private AdDasApiPropertiesConfig apiPropertiesConfig;

    @Autowired
    private AdDasBenchmarkRepository benchmarkRepository;

    private static final int SHARD_SIZE = 20;

    private static final List<Integer> MODIFY_STATUS = Arrays.asList(AdDasModifyStatusEnum.EDITING.getCode(),
            AdDasModifyStatusEnum.REJECTED.getCode());

    List<Integer> validStatus = Arrays.asList(AdDasModifyStatusEnum.PUBLISH_SUC.getCode(), AdDasModifyStatusEnum.ROLLBACK_SUC.getCode(),
            AdDasModifyStatusEnum.DELETE.getCode(), AdDasModifyStatusEnum.TERMINATION.getCode(), AdDasModifyStatusEnum.ENDING.getCode());

    @Override
    @Transactional
    public Long createBenchmark(AdDasUserView adDasUserView, AdDasModifyBenchmarkParam param) {
        //  写入 ad_das_modify 表
        AdDasModify adDasModify;
        checkParam(param);

        if (param.getModifyId() == null || param.getModifyId() == 0) {
            validAdDasModify(param);
            adDasModify = saveAdDasModify(adDasUserView.getUserName(), param.getModifyName(),
                    param.getModifyType());
            // 写入ad_das_modify_benchmark_msg 表
            saveAdDasBenchmarkMsg(param, adDasModify.getId());
        } else {
            adDasModify = updateDasModify(param.getModifyId(), param.getModifyType(),
                    adDasUserView.getUserName(), param.getModifyName());
            updateAdDasBenchmarkMsg(param, adDasModify.getId());
        }
        return adDasModify.getId();
    }

    private void checkParam(AdDasModifyBenchmarkParam param) {

        boolean mainTablePrimaryKey = true;

        AdDasBenchmarkMainTableView adDasBenchmarkMainTableData = param.getAdDasBenchmarkMainTableData();
        List<AdDasBenchmarkDb2PbColumnMsgView> db2PbColumnMsgViews = adDasBenchmarkMainTableData.getDb2PbColumnMsgViews();

        for (AdDasBenchmarkDb2PbColumnMsgView db2PbColumnMsgView : db2PbColumnMsgViews) {
            if (db2PbColumnMsgView.getIsPrimaryKey()) {
                mainTablePrimaryKey = false;
            }
        }

        if (mainTablePrimaryKey) {
            throw AdDasServiceException.ofMessage(MAIN_TABLE_NEED_PRIMARY_KEY);
        }

        List<AdDasBenchmarkCascadeTableView> cascadeTableViews = adDasBenchmarkMainTableData.getCascadeTableViews();

        if (!CollectionUtils.isEmpty(cascadeTableViews)) {
            for (AdDasBenchmarkCascadeTableView cascadeTableView : cascadeTableViews) {
                boolean cascadeTablePrimaryKey = true;

                List<AdDasBenchmarkDb2PbColumnMsgView> cascadeDb2PbColumnMsgViews = cascadeTableView.getDb2PbColumnMsgViews();
                for (AdDasBenchmarkDb2PbColumnMsgView db2PbColumnMsgView : cascadeDb2PbColumnMsgViews) {
                    if (db2PbColumnMsgView.getIsPrimaryKey()) {
                        cascadeTablePrimaryKey = false;
                    }
                }

                if (cascadeTablePrimaryKey) {
                    throw AdDasServiceException.ofMessage(CASCADE_TABLE_NEED_PRIMARY_KEY);
                }
            }
        }
    }

    @Override
    @Transactional
    public void approveModifyBenchmark(AdDasUserView adDasUserView, Integer approveType, String reason, Long modifyId) {
        adDasModifyRepository.updateApprove(modifyId, adDasUserView.getUserName(), approveType, reason, System.currentTimeMillis());
        AdDasModify adDasModify = adDasModifyRepository.queryById(modifyId);
        AdDasBenchmarkMsg adDasBenchmarkMsg = adDasBenchmarkMsgRepository.getByModifyId(adDasModify.getId());
        adDasBenchmarkTaskEditRepository.updateStatusByBenchmarkTypeAndMainTable(adDasBenchmarkMsg.getBenchmarkId(), adDasBenchmarkMsg.getMainTable(),
                approveType, adDasUserView.getUserName(),System.currentTimeMillis());

        String clickUrl = String.format(apiPropertiesConfig.getCasServiceName() + DAS_BENCHMARK_CLICKURL, adDasModify.getId());
        String modifyStatus;
        if (approveType == AdDasModifyStatusEnum.APPROVED.getCode()) {
            modifyStatus = AdDasModifyStatusEnum.APPROVED.getDesc();
        } else {
            modifyStatus = AdDasModifyStatusEnum.REJECTED.getDesc();
        }

        sendAdDasModifyMsg(adDasModify.getModifyName(), clickUrl, modifyStatus,
                adDasUserView.getUserName(), adDasModify.getApprover());
    }

    @Override
    @Transactional
    public void discardBenchmark(AdDasUserView adDasUserView, Long modifyId) {

        if (modifyId != null && modifyId > 0) {
            AdDasModify adDasModify = adDasModifyRepository.queryById(modifyId);
            adDasModifyRepository.updateStatus(modifyId, AdDasModifyStatusEnum.DELETE.getCode(),
                    adDasUserView.getUserName(), System.currentTimeMillis());
            AdDasBenchmarkMsg adDasBenchmarkMsg = adDasBenchmarkMsgRepository.getByModifyId(adDasModify.getId());
            adDasBenchmarkTaskEditRepository.updateStatusByBenchmarkTypeAndMainTable(adDasBenchmarkMsg.getBenchmarkId(),
                    adDasBenchmarkMsg.getMainTable(), AdDasModifyStatusEnum.DELETE.getCode(), adDasUserView.getUserName(), System.currentTimeMillis());

            String clickUrl = String.format(apiPropertiesConfig.getCasServiceName() + DAS_BENCHMARK_CLICKURL, adDasModify.getId());
            String modifyStatus = AdDasModifyStatusEnum.DELETE.getDesc();
            sendAdDasModifyMsg(adDasModify.getModifyName(), clickUrl, modifyStatus,
                    adDasUserView.getUserName(), adDasModify.getApprover());
        }
    }

    @Override
    @Transactional
    public void rollbackBenchmark(AdDasUserView adDasUserView, Long modifyId) {

        rollbackBenchmark(modifyId);
        AdDasModify adDasModify = adDasModifyRepository.queryById(modifyId);

        adDasModifyRepository.updateStatus(modifyId, AdDasModifyStatusEnum.ROLLBACK_SUC.getCode(),
                adDasUserView.getUserName(), System.currentTimeMillis());

        String clickUrl = String.format(apiPropertiesConfig.getCasServiceName() + DAS_BENCHMARK_CLICKURL, adDasModify.getId());
        String modifyStatus = AdDasModifyStatusEnum.ROLLBACK_SUC.getDesc();
        sendAdDasModifyMsg(adDasModify.getModifyName(), clickUrl, modifyStatus,
                adDasUserView.getUserName(), adDasModify.getApprover());
    }

    @Override
    @Transactional
    public void submitBenchmark(AdDasUserView adDasUserView, AdDasModifyBenchmarkParam param) {

        checkParam(param);

        AdDasModify adDasModify = adDasModifyRepository.queryById(param.getModifyId());

        AdDasBenchmarkTaskCommonDetailEdit adDasBenchmarkTaskCommonDetailEdit = adDasBenchmarkTaskCommonDetailEditRepository
                .getByBenchmarkIdAndMainTable(param.getAdDasBenchmarkMainTableData().getBenchmarkId(), param.getAdDasBenchmarkMainTableData().getMainTable());
        if (adDasBenchmarkTaskCommonDetailEdit == null) {
            throw new AdDasServiceException(ErrorCode.BENCHMARK_MODIFY_NOT_EXIT.getCode(),
                    ErrorCode.BENCHMARK_MODIFY_NOT_EXIT.getMsg());
        }
        adDasModifyRepository.submitApprove(param.getModifyId(), adDasUserView.getUserName(), AdDasModifyStatusEnum.APPROVING.getCode(),
                System.currentTimeMillis(), param.getApprover());

        Integer taskStatus = AdDasModifyStatusEnum.APPROVING.getCode();
        if (param.getDeleteTable()) {
            taskStatus = AdDasModifyStatusEnum.DELETE.getCode();
        }
        adDasBenchmarkTaskEditRepository.updateStatusByBenchmarkTypeAndMainTable(param.getAdDasBenchmarkMainTableData().getBenchmarkId(),
                param.getAdDasBenchmarkMainTableData().getMainTable(), taskStatus, adDasUserView.getUserName(),System.currentTimeMillis());

        String clickUrl = String.format(apiPropertiesConfig.getCasServiceName() + DAS_BENCHMARK_CLICKURL, adDasModify.getId());
        String modifyStatus = AdDasModifyStatusEnum.APPROVING.getDesc();
        sendAdDasModifyMsg(adDasModify.getModifyName(), clickUrl, modifyStatus,
                adDasUserView.getUserName(), adDasModify.getApprover());
    }

    @Override
    @Transactional
    public void publishModifyBenchmark(AdDasUserView adDasUserView, Integer approveType, Long modifyId) {
        if (approveType.equals(AdDasModifyStatusEnum.PUBLISH_ING.getCode())) {

            AdDasModify adDasModify = adDasModifyRepository.queryById(modifyId);

            AdDasBenchmarkMsg adDasBenchmarkMsg = adDasBenchmarkMsgRepository.getByModifyId(modifyId);
            if (adDasBenchmarkMsg.getDeleteTable()) {
                log.info("publishModifyBenchmark() deleteTable() adDasBenchmarkMsg={}", adDasBenchmarkMsg);
                deleteBenchmarkTable(modifyId, adDasBenchmarkMsg);
                adDasModifyRepository.updateStatus(modifyId, AdDasModifyStatusEnum.PUBLISH_SUC.getCode(),
                        adDasUserView.getUserName(), System.currentTimeMillis());
            } else {
                saveAdDasBenchmarkTaskCommonDetail(modifyId, adDasBenchmarkMsg);
                saveAdDasBenchmarkTask(adDasUserView, modifyId, adDasBenchmarkMsg);
                adDasModifyRepository.updateStatus(modifyId, AdDasModifyStatusEnum.PUBLISH_SUC.getCode(),
                        adDasUserView.getUserName(), System.currentTimeMillis());
            }

            String clickUrl = String.format(apiPropertiesConfig.getCasServiceName() + DAS_BENCHMARK_CLICKURL, adDasModify.getId());
            String modifyStatus = AdDasModifyStatusEnum.PUBLISH_SUC.getDesc();
            sendAdDasModifyMsg(adDasModify.getModifyName(), clickUrl, modifyStatus,
                    adDasUserView.getUserName(), adDasModify.getApprover());
        }
    }

    @Override
    @Transactional
    public Long createBenchmarkTask(AdDasUserView adDasUserView, AdDasModifyBenchmarkParam param) {
        Long modifyId = createBenchmark(adDasUserView, param);
        param.setModifyId(modifyId);
        saveAdDasBenchmarkTaskCommonDetailEdit(modifyId, adDasUserView, param);
        saveAdDasBenchmarkTaskEdit(modifyId, adDasUserView, param);
        return modifyId;
    }

    @Override
    public AdDasModifyBenchmarkDataView queryBenchmark(Long modifyId) {
        AdDasModify adDasModify = adDasModifyRepository.queryById(modifyId);
        AdDasBenchmarkMsg adDasBenchmarkMsg = adDasBenchmarkMsgRepository.getByModifyId(modifyId);
        Pair<AdDasBenchmark, AdDasDataStreamGroup> benchmark2Group =
                benchmarkModifyHandler.getBenchmarkAndGroup(adDasBenchmarkMsg.getBenchmarkId());

        return AdDasModifyBenchmarkDataView.build(adDasModify, adDasBenchmarkMsg, benchmark2Group.getLeft(),
                benchmark2Group.getRight());
    }

    @Override
    public AdDasModifyBenchmarkDataView queryPreviousBenchmark(Long modifyId) {
        AdDasBenchmarkMsg adDasBenchmarkMsg = adDasBenchmarkMsgRepository.getByModifyId(modifyId);
        List<AdDasBenchmarkMsg> adDasBenchmarkMsgs = adDasBenchmarkMsgRepository
                .getByBenchmarkTypeAndMainTable(adDasBenchmarkMsg.getBenchmarkId(), adDasBenchmarkMsg.getMainTable());
        AdDasModifyBenchmarkDataView adDasModifyBenchmarkDataView = new AdDasModifyBenchmarkDataView();
        Map<Long, AdDasBenchmarkMsg> benchmarkMsgMap = adDasBenchmarkMsgs.stream()
                .filter(itme -> itme.getModifyId() != modifyId.longValue())
                .collect(Collectors.toMap(AdDasBenchmarkMsg::getModifyId, msg -> msg));
        if(CollectionUtils.isEmpty(benchmarkMsgMap)) {
            return adDasModifyBenchmarkDataView;
        }
        List<AdDasModify> dasModifies = benchmarkMsgMap.keySet().stream().map(id ->adDasModifyRepository.queryById(id))
        .filter(adDasModify1 -> adDasModify1.getModifyStatus().equals(AdDasModifyStatusEnum.PUBLISH_SUC.getCode())).collect(Collectors.toList());
        if (CollectionUtils.isEmpty(dasModifies)) {
            return adDasModifyBenchmarkDataView;
        }
        Map<Long, AdDasModify> dasModifyMap = dasModifies.stream().collect(Collectors.toMap(AdDasModify::getId, modify -> modify));
        List<Long> modifyIdList = new ArrayList<>(dasModifyMap.keySet());
        modifyIdList.sort(Collections.reverseOrder());
        Pair<AdDasBenchmark, AdDasDataStreamGroup> benchmark2Group = benchmarkModifyHandler.getBenchmarkAndGroup(
                adDasBenchmarkMsg.getBenchmarkId());
        return AdDasModifyBenchmarkDataView.build(dasModifyMap.get(modifyIdList.get(0)),
                benchmarkMsgMap.get(modifyIdList.get(0)), benchmark2Group.getLeft(), benchmark2Group.getRight());
    }

    @Override
    public AdDasBenchmarkMainTableView queryCurBenchmark(Long benchmarkId, String mainTable, String dataSource) {
        AdDasBenchmarkMsg adDasBenchmarkMsg = adDasBenchmarkMsgRepository
                .getByTableAndDataSource(benchmarkId, mainTable, dataSource);

        AdDasBenchmarkMainTableView mainTableView = new AdDasBenchmarkMainTableView();
        mainTableView.setDataSource(adDasBenchmarkMsg.getDataSource());
        mainTableView.setMainTable(adDasBenchmarkMsg.getMainTable());
        mainTableView.setPbClass(adDasBenchmarkMsg.getPbClass());
        mainTableView.setTableType(adDasBenchmarkMsg.getTableType());
        mainTableView.setPredictGrowth(adDasBenchmarkMsg.getPredictGrowth());
        mainTableView.setCommonSqls(adDasBenchmarkMsg.getCommonSqls());
        mainTableView.setTimeParams(adDasBenchmarkMsg.getTimeParams());
        mainTableView.setShardSqlFlag(adDasBenchmarkMsg.getShardSqlFlag());
        mainTableView.setShardSqlColumn(adDasBenchmarkMsg.getShardSqlColumn());
        List<AdDasBenchmarkDb2PbColumnMsgView> views = new Gson().fromJson(adDasBenchmarkMsg.getColumnSchema(),
                new TypeToken<List<AdDasBenchmarkDb2PbColumnMsgView>>() {
                }.getType());
        mainTableView.setDb2PbColumnMsgViews(views);

        return mainTableView;
    }

    @Override
    public AdDasModifyBenchmarkDataView queryBenchmarkByTable(Long benchmarkId, String mainTable, String dataSource) {

        log.info("queryBenchmarkByTable() benchmarkId={}, mainTable={}, dataSource={}", benchmarkId, mainTable, dataSource);

        AdDasModifyBenchmarkDataView adDasModifyBenchmarkDataView = new AdDasModifyBenchmarkDataView();
        if(benchmarkId == null || StringUtils.isEmpty(mainTable) || StringUtils.isEmpty(dataSource)) {
            return adDasModifyBenchmarkDataView;
        }

        List<AdDasBenchmarkTaskEdit> adDasBenchmarkTaskEdits = adDasBenchmarkTaskEditRepository
                .getByBenchmarkTypeAndMainTable(benchmarkId, mainTable);
        if (CollectionUtils.isEmpty(adDasBenchmarkTaskEdits)) {
            return adDasModifyBenchmarkDataView;
        }

        log.info("queryBenchmarkByTable() adDasBenchmarkTaskEdits={}", adDasBenchmarkTaskEdits);

        AdDasBenchmarkTaskCommonDetailEdit edit = adDasBenchmarkTaskCommonDetailEditRepository
                .getByBenchmarkIdAndMainTable(benchmarkId, mainTable);
        log.info("queryBenchmarkByTable() edit={}", edit);

        Pair<AdDasBenchmark, AdDasDataStreamGroup> benchmark2Group = benchmarkModifyHandler
                .getBenchmarkAndGroup(edit.getBenchmarkId());
        AdDasBenchmarkMainTableView adDasBenchmarkMainTableView = new AdDasBenchmarkMainTableView();
        adDasBenchmarkMainTableView.setMainTable(mainTable);
        adDasBenchmarkMainTableView.setDataSource(dataSource);
        adDasBenchmarkMainTableView.setPbClass(adDasBenchmarkTaskEdits.get(0).getPbClass());
        adDasBenchmarkMainTableView.setTableType(edit.getTableType());
        adDasBenchmarkMainTableView.setPredictGrowth(edit.getPredictGrowth());
        adDasBenchmarkMainTableView.setCommonSqls(edit.getCommonSqls());
        adDasBenchmarkMainTableView.setTimeParams(edit.getTimeParams());
        adDasBenchmarkMainTableView.setShardSqlFlag(edit.getShardSqlFlag());
        adDasBenchmarkMainTableView.setShardSqlColumn(edit.getShardSqlColumn());
        List<AdDasBenchmarkDb2PbColumnMsgView> views = new Gson().fromJson(edit.getColumnSchema(),
                new TypeToken<List<AdDasBenchmarkDb2PbColumnMsgView>>() {
        }.getType());
        adDasBenchmarkMainTableView.setDb2PbColumnMsgViews(views);
        if (benchmark2Group.getLeft() != null) {
            adDasBenchmarkMainTableView.setDataStreamGroupId(benchmark2Group.getRight().getId());
            adDasBenchmarkMainTableView.setBenchmarkName(benchmark2Group.getLeft().getBenchmarkName());
            adDasBenchmarkMainTableView.setDataStreamGroupName(benchmark2Group.getRight().getDataStreamGroupName());
        }

        List<AdDasBenchmarkCascadeTableView> adDasBenchmarkCascadeTableViews = ObjectMapperUtils
                .fromJSON(edit.getCascadeColumnSchema(), List.class, AdDasBenchmarkCascadeTableView.class);
        adDasBenchmarkMainTableView.setCascadeTableViews(adDasBenchmarkCascadeTableViews);
        adDasModifyBenchmarkDataView.setMainTableView(adDasBenchmarkMainTableView);
        return adDasModifyBenchmarkDataView;
    }

    @Override
    public void mod(AdDasEditBenchmarkStreamParam param) {

        AdDasBenchmark adDasBenchmark = new AdDasBenchmark();
        AdDasBeanCoreUtils.copyPropertiesIgnoreNull(adDasBenchmark, param);
        adDasBenchmark.setUpdateTime(System.currentTimeMillis());

        if (param.getId() == 0 || param.getId() == null) {
            adDasBenchmark.setCreateTime(System.currentTimeMillis());
            benchmarkRepository.insert(adDasBenchmark);
            return;
        }

        adDasBenchmark.setId(param.getId());
        benchmarkRepository.update(adDasBenchmark);
    }

    @Override
    public void deleteById(Long dataId) {
        benchmarkRepository.deleteById(dataId);
    }


    private AdDasModify saveAdDasModify(String operator, String modifyName, Integer modifyType) {
        Long timeStamp = System.currentTimeMillis();
        AdDasModify adDasModify = new AdDasModify();
        adDasModify.setModifyName(modifyName);
        adDasModify.setModifyType(modifyType);
        adDasModify.setOperator(operator);
        adDasModify.setModifyStatus(AdDasModifyStatusEnum.EDITING.getCode());
        adDasModify.setCreateTime(timeStamp);
        adDasModify.setUpdateTime(timeStamp);
        adDasModifyRepository.insert(adDasModify);
        // 查询
        List<AdDasModify> adDasModifies = adDasModifyRepository.queryByNameAndCreateTime(
                adDasModify.getModifyName(), adDasModify.getModifyType(),
                adDasModify.getOperator(), adDasModify.getCreateTime());

        adDasModify = adDasModifies.get(0);
        return adDasModify;
    }

    private void saveAdDasBenchmarkMsg(AdDasModifyBenchmarkParam param, Long modifyId) {
        AdDasBenchmarkMsg adDasBenchmarkMsg = new AdDasBenchmarkMsg();
        Long timeStamp = System.currentTimeMillis();
        adDasBenchmarkMsg.setModifyId(modifyId);
        adDasBenchmarkMsg.setCreateTime(timeStamp);
        fillAdDasBenchmarkMsgFields(param, adDasBenchmarkMsg);
        adDasBenchmarkMsg.setDeleteTable(param.getDeleteTable());
        adDasBenchmarkMsgRepository.insert(adDasBenchmarkMsg);
    }

    private void validAdDasModify(AdDasModifyBenchmarkParam param) {
        log.info("param={}", param);
        List<AdDasBenchmarkMsg> adDasBenchmarkMsgs = adDasBenchmarkMsgRepository
                .getByBenchmarkTypeAndMainTable(param.getAdDasBenchmarkMainTableData().getBenchmarkId(),param.getAdDasBenchmarkMainTableData().getMainTable());
        if (!CollectionUtils.isEmpty(adDasBenchmarkMsgs)) {
            List<Long> modifyIds = adDasBenchmarkMsgs.stream().map(AdDasBenchmarkMsg::getModifyId)
                    .collect(Collectors.toList());
            log.info("modifyIds={}", modifyIds);

            for (Long modifyId: modifyIds) {
                AdDasModify adDasModify = adDasModifyRepository.queryById(modifyId);
                log.info("modifyIds={}", modifyIds);
                if (adDasModify != null && !validStatus.contains(adDasModify.getModifyStatus())) {
                    throw new AdDasServiceException(ErrorCode.BENCHMARK_MODIFY_EXIT.getCode(),ErrorCode.BENCHMARK_MODIFY_EXIT.getMsg());
                }
            }
        }
    }

    private AdDasModify updateDasModify(Long modifyId, Integer modifyType, String operator, String modifyName) {
        AdDasModify adDasModify = adDasModifyRepository.queryById(modifyId);
        if (!MODIFY_STATUS.contains(adDasModify.getModifyStatus())) {
            throw new AdDasServiceException(ErrorCode.BENCHMARK_MODIFY_STATUS_ERROR.getCode(), ErrorCode.BENCHMARK_MODIFY_STATUS_ERROR.getMsg());
        }
        adDasModify.setModifyName(modifyName);
        adDasModify.setModifyType(modifyType);
        adDasModify.setOperator(operator);
        adDasModify.setUpdateTime(System.currentTimeMillis());
        adDasModifyRepository.updateAdDasModify(adDasModify);
        adDasModifyRepository.updateStatus(modifyId, AdDasModifyStatusEnum.EDITING.getCode(), operator, System.currentTimeMillis());
        return adDasModify;
    }

    private void updateAdDasBenchmarkMsg(AdDasModifyBenchmarkParam param,
                                         Long modifyId) {
        AdDasBenchmarkMsg adDasBenchmarkMsg = adDasBenchmarkMsgRepository.getByModifyId(modifyId);
        if (adDasBenchmarkMsg == null) {
            saveAdDasBenchmarkMsg(param, modifyId);
        } else {
            fillAdDasBenchmarkMsgFields(param, adDasBenchmarkMsg);
            adDasBenchmarkMsgRepository.update(adDasBenchmarkMsg);
        }
    }

    private void fillAdDasBenchmarkMsgFields(AdDasModifyBenchmarkParam param, AdDasBenchmarkMsg adDasBenchmarkMsg) {
        AdDasBenchmarkMainTableView data = param.getAdDasBenchmarkMainTableData();
        Long timeStamp = System.currentTimeMillis();
        adDasBenchmarkMsg.setBenchmarkId(data.getBenchmarkId());
        adDasBenchmarkMsg.setDataSource(data.getDataSource());
        adDasBenchmarkMsg.setMainTable(data.getMainTable());
        adDasBenchmarkMsg.setPbClass(data.getPbClass());
        adDasBenchmarkMsg.setColumnSchema(new Gson().toJson(data.getDb2PbColumnMsgViews()));
        adDasBenchmarkMsg.setCascadeColumnSchema(ObjectMapperUtils.toJSON(data.getCascadeTableViews()));
        adDasBenchmarkMsg.setTableType(data.getTableType());
        adDasBenchmarkMsg.setPredictGrowth(data.getPredictGrowth());
        adDasBenchmarkMsg.setCommonSqls(data.getCommonSqls());
        adDasBenchmarkMsg.setTimeParams(data.getTimeParams());
        adDasBenchmarkMsg.setShardSqlFlag(data.getShardSqlFlag());
        adDasBenchmarkMsg.setShardSqlColumn(data.getShardSqlColumn());
        adDasBenchmarkMsg.setUpdateTime(timeStamp);
    }

    private void saveAdDasBenchmarkTaskCommonDetailEdit(Long modifyId, AdDasUserView adDasUserView, AdDasModifyBenchmarkParam param) {
        AdDasBenchmarkTaskCommonDetailEdit adDasBenchmarkTaskCommonDetailEdit = adDasBenchmarkTaskCommonDetailEditRepository.
                getByBenchmarkIdAndMainTable(param.getAdDasBenchmarkMainTableData().getBenchmarkId(),
                        param.getAdDasBenchmarkMainTableData().getMainTable());
        if (adDasBenchmarkTaskCommonDetailEdit == null) {
            adDasBenchmarkTaskCommonDetailEdit = new AdDasBenchmarkTaskCommonDetailEdit();
            adDasBenchmarkTaskCommonDetailEdit.setCreateTime(System.currentTimeMillis());
            buildAdDasBenchmarkTaskCommonDetailEdit(adDasUserView, param, adDasBenchmarkTaskCommonDetailEdit);
            adDasBenchmarkTaskCommonDetailEdit.setModifyId(modifyId);
            adDasBenchmarkTaskCommonDetailEditRepository.insert(adDasBenchmarkTaskCommonDetailEdit);
        } else {
            buildAdDasBenchmarkTaskCommonDetailEdit(adDasUserView, param, adDasBenchmarkTaskCommonDetailEdit);
            adDasBenchmarkTaskCommonDetailEdit.setModifyId(modifyId);
            adDasBenchmarkTaskCommonDetailEditRepository.update(adDasBenchmarkTaskCommonDetailEdit);
        }
    }

    public void saveAdDasBenchmarkTaskEdit(Long modifyId, AdDasUserView adDasUserView, AdDasModifyBenchmarkParam param) {
        List<AdDasBenchmarkTaskEdit> adDasBenchmarkTaskEdits = adDasBenchmarkTaskEditRepository
                .getByBenchmarkTypeAndMainTable(param.getAdDasBenchmarkMainTableData().getBenchmarkId(), param.getAdDasBenchmarkMainTableData().getMainTable());
        if (CollectionUtils.isEmpty(adDasBenchmarkTaskEdits)) {
            adDasBenchmarkTaskEdits = buildAdDasBenchmarkTaskEdit(modifyId, adDasUserView, param);
            adDasBenchmarkTaskEdits.forEach(adDasBenchmarkTaskEdit -> adDasBenchmarkTaskEditRepository
                    .insert(adDasBenchmarkTaskEdit));
        } else {
            updateAdDasBenchmarkTaskEdit(modifyId, adDasUserView, param.getAdDasBenchmarkMainTableData(), adDasBenchmarkTaskEdits);
        }
    }


    private void buildAdDasBenchmarkTaskCommonDetailEdit(AdDasUserView adDasUserView, AdDasModifyBenchmarkParam param,
                                                         AdDasBenchmarkTaskCommonDetailEdit adDasBenchmarkTaskCommonDetailEdit) {
        Long timestamp = System.currentTimeMillis();
        AdDasBenchmarkMainTableView data = param.getAdDasBenchmarkMainTableData();
        adDasBenchmarkTaskCommonDetailEdit.setMainTable(data.getMainTable());
        adDasBenchmarkTaskCommonDetailEdit.setTableType(data.getTableType());
        adDasBenchmarkTaskCommonDetailEdit.setColumnSchema(new Gson().toJson(data.getDb2PbColumnMsgViews()));
        adDasBenchmarkTaskCommonDetailEdit.setCascadeColumnSchema(ObjectMapperUtils.toJSON(data.getCascadeTableViews()));
        adDasBenchmarkTaskCommonDetailEdit.setCommonSqls(data.getCommonSqls());
        adDasBenchmarkTaskCommonDetailEdit.setTimeParams(data.getTimeParams());
        adDasBenchmarkTaskCommonDetailEdit.setPredictGrowth(data.getPredictGrowth());
        adDasBenchmarkTaskCommonDetailEdit.setShardSqlColumn(data.getShardSqlColumn());
        adDasBenchmarkTaskCommonDetailEdit.setShardSqlFlag(data.getShardSqlFlag());
        adDasBenchmarkTaskCommonDetailEdit.setOperator(adDasUserView.getUserName());
        adDasBenchmarkTaskCommonDetailEdit.setDataSource(data.getDataSource());
        adDasBenchmarkTaskCommonDetailEdit.setPbClass(data.getPbClass());
        adDasBenchmarkTaskCommonDetailEdit.setModifyId(param.getModifyId());
        adDasBenchmarkTaskCommonDetailEdit.setBenchmarkId(data.getBenchmarkId());
        adDasBenchmarkTaskCommonDetailEdit.setUpdateTime(timestamp);
    }

    /**
     * 自动拆分 task
     * @param adDasUserView
     * @param param
     * @return
     */
    private List<AdDasBenchmarkTaskEdit> buildAdDasBenchmarkTaskEdit(Long modifyId, AdDasUserView adDasUserView, AdDasModifyBenchmarkParam param) {
        List<AdDasBenchmarkTaskEdit> adDasBenchmarkTaskEdits = Lists.newArrayList();
        AdDasBenchmarkMainTableView data = param.getAdDasBenchmarkMainTableData();
        String mainTable = data.getMainTable();
        String dataSource = data.getDataSource();
        String dataSourceFinale = adDasPlatformDataAdapterMap.get().getOrDefault(dataSource, dataSource);
        String dbName = AD_DAS_DATASOURCE_DB_MAPPING.get().get(dataSourceFinale);
        AdDasBenchmarkTaskEdit adDasBenchmarkTaskEdit = adDasBenchmarkTaskEditRepository.getLastTask();
        int taskNumber = 0;
        if (adDasBenchmarkTaskEdit != null) {
            taskNumber = adDasBenchmarkTaskEdit.getTaskNumber() + 1;
        }

        // 分表生成任务
        if (param.getAdDasBenchmarkMainTableData().getTableType() == 1) {
            List<AdDasTableSchema> tableSchemas = adDasTableSchemaRepository.queryTablesAll(dataSourceFinale, dbName);
            List<AdDasTableSchema> shardTables = tableSchemas.stream()
                    .filter(tableSchema -> judgeShardTable(mainTable, tableSchema))
                    .collect(Collectors.toList());

            List<Integer> shardIdList = Lists.newArrayList();
            if (shardTables.size() > 1) {
                for (AdDasTableSchema adDasTableSchema : shardTables) {
                    int beginIndex = adDasTableSchema.getTableName().lastIndexOf("_");

                    Integer shardId = Integer.valueOf(adDasTableSchema.getTableName().substring(beginIndex + 1));
                    shardIdList.add(shardId);
                }
                Collections.sort(shardIdList);
                int beginIndex = 0;
                int endIndex;
                List<List<Integer>> shardIdParts = Lists.partition(shardIdList, SHARD_SIZE);
                for (int index = 0; index < shardIdParts.size(); index++) {
                    taskNumber += index;
                    if (index != 0) {
                        beginIndex = index * SHARD_SIZE + 1;
                    }
                    endIndex = (index + 1) * SHARD_SIZE;
                    adDasBenchmarkTaskEdits.add(createAdDasBenchmarkTaskEdit(modifyId,adDasUserView, data,
                            shardIdParts.get(index), taskNumber, beginIndex, endIndex));
                }
            } else {
               throw AdDasServiceException.ofMessage(BENCHMARK_TABLE_TYPE_ERROR);
            }
        } else {
            adDasBenchmarkTaskEdits.add(createAdDasBenchmarkTaskEdit(modifyId, adDasUserView, data,
                    Collections.emptyList(), taskNumber, 0, 0));
        }

        return adDasBenchmarkTaskEdits;
    }

    private boolean judgeShardTable(String mainTable, AdDasTableSchema tableSchema) {
        String targetTableName = tableSchema.getTableName();
        String chosenTable = BinlogResolver.getNoShardTableName(mainTable);
        String targetTable = BinlogResolver.getNoShardTableName(targetTableName);
        int index = tableSchema.getTableName().lastIndexOf("_");
        return index >= 0 && index != targetTableName.length() - 1
                && StringUtils.isNumeric(targetTableName.substring(index + 1))
                && targetTable.equals(chosenTable);
    }

    private AdDasBenchmarkTaskEdit createAdDasBenchmarkTaskEdit(Long modifyId, AdDasUserView adDasUserView,
                                                                AdDasBenchmarkMainTableView data,
                                                                List<Integer> shardIdList,
                                                                Integer taskNumber,
                                                                Integer beginIndex,
                                                                Integer endIndex) {
        AdDasBenchmarkTaskEdit adDasBenchmarkTaskEdit = new AdDasBenchmarkTaskEdit();
        if (CollectionUtils.isEmpty(shardIdList)) {
            adDasBenchmarkTaskEdit.setTaskName(data.getMainTable());
        } else {
            String taskName = data.getMainTable() + "-" + beginIndex + "~" + endIndex;
            adDasBenchmarkTaskEdit.setTaskName(taskName);
        }
        adDasBenchmarkTaskEdit.setBenchmarkId(data.getBenchmarkId());
        adDasBenchmarkTaskEdit.setTaskNumber(taskNumber);
        adDasBenchmarkTaskEdit.setDataSource(data.getDataSource());
        adDasBenchmarkTaskEdit.setMainTable(data.getMainTable());
        adDasBenchmarkTaskEdit.setTableType(data.getTableType());
        adDasBenchmarkTaskEdit.setTaskShardList(shardIdList.stream().map(String::valueOf)
                .collect(Collectors.joining(",")));
        adDasBenchmarkTaskEdit.setPbClass(data.getPbClass());
        // TODO 反射获取 pbClassFullName
        String pbClassFullName = "kuaishou.ad.tables." + (data.getPbClass());
        adDasBenchmarkTaskEdit.setPbClassFullName(pbClassFullName);
        adDasBenchmarkTaskEdit.setShardIds(data.getShardSqlColumn() == null ? "" : data.getShardSqlColumn());
        adDasBenchmarkTaskEdit.setTaskStatus(AdDasModifyStatusEnum.EDITING.getCode());
        adDasBenchmarkTaskEdit.setOperator(adDasUserView.getUserName());
        adDasBenchmarkTaskEdit.setCreatTime(System.currentTimeMillis());
        adDasBenchmarkTaskEdit.setUpdateTime(System.currentTimeMillis());
        adDasBenchmarkTaskEdit.setShardSqlFlag(data.getShardSqlFlag());
        adDasBenchmarkTaskEdit.setTaskSqls("");
        adDasBenchmarkTaskEdit.setTaskSqlFlag(0);
        adDasBenchmarkTaskEdit.setModifyId(modifyId);
        return adDasBenchmarkTaskEdit;
    }

    @Transactional
    public void deleteBenchmarkTable(Long modifyId, AdDasBenchmarkMsg adDasBenchmarkMsg) {

        AdDasBenchmarkTaskCommonDetail adDasBenchmarkTaskCommonDetail = adDasBenchMarkTaskCommonDetailRepository
                .getByBenchmarkTypeAndMainTable(adDasBenchmarkMsg.getBenchmarkId(),adDasBenchmarkMsg.getMainTable());
        log.info("saveAdDasBenchmarkTaskCommonDetail() adDasBenchmarkTaskCommonDetail={}", adDasBenchmarkTaskCommonDetail);
        List<AdDasBenchmarkTask> tasks = adDasBenchmarkTaskRepository.queryByBenchmarkId(adDasBenchmarkMsg.getBenchmarkId(), adDasBenchmarkMsg.getDataSource(),
                adDasBenchmarkMsg.getPbClass(), adDasBenchmarkMsg.getMainTable());
        // 删除 task 表 和 commonDetail 表
        adDasBenchmarkTaskRepository.deleteByBenchmarkIdeAndMainTable(adDasBenchmarkMsg.getBenchmarkId(), adDasBenchmarkMsg.getMainTable());
        adDasBenchMarkTaskCommonDetailRepository.deleteByBenchmarkIdAndMainTable(adDasBenchmarkMsg.getBenchmarkId(), adDasBenchmarkMsg.getMainTable());

        // 删除 taskEdit 表 和 commonDetailEdit 表
        adDasBenchmarkTaskEditRepository.deleteByBenchmarkTypeAndMainTable(adDasBenchmarkMsg.getBenchmarkId(), adDasBenchmarkMsg.getMainTable());
        adDasBenchmarkTaskCommonDetailEditRepository.deleteByBenchmarkTypeAndMainTable(adDasBenchmarkMsg.getBenchmarkId(), adDasBenchmarkMsg.getMainTable());

        // 备份history
        AdDasBenchmarkTaskCommonDetailHistory history = new AdDasBenchmarkTaskCommonDetailHistory();
        AdDasBeanCoreUtils.copyPropertiesIgnoreNull(history, adDasBenchmarkTaskCommonDetail);
        history.setModifyId(modifyId);
        adDasBenchmarkTaskCommonDetailHistoryRepository.insert(history);
    }

    public void updateAdDasBenchmarkTaskEdit(Long modifyId, AdDasUserView adDasUserView,
                                             AdDasBenchmarkMainTableView data, List<AdDasBenchmarkTaskEdit> adDasBenchmarkTaskEdits) {
        for (AdDasBenchmarkTaskEdit adDasBenchmarkTaskEdit : adDasBenchmarkTaskEdits) {
            adDasBenchmarkTaskEdit.setShardIds(data.getShardSqlColumn() == null ? "" : data.getShardSqlColumn());
            adDasBenchmarkTaskEdit.setShardSqlFlag(data.getShardSqlFlag());
            adDasBenchmarkTaskEdit.setTableType(data.getTableType());
            adDasBenchmarkTaskEdit.setPbClass(data.getPbClass());
            // TODO 反射获取 pbClassFullName
            String pbClassFullName = "kuaishou.ad.tables." + (data.getPbClass());
            adDasBenchmarkTaskEdit.setPbClassFullName(pbClassFullName);
            adDasBenchmarkTaskEdit.setOperator(adDasUserView.getUserName());
            adDasBenchmarkTaskEdit.setUpdateTime(System.currentTimeMillis());
            adDasBenchmarkTaskEdit.setModifyId(modifyId);
            adDasBenchmarkTaskEditRepository.updateAdDasBenchmarkTaskEdit(adDasBenchmarkTaskEdit);
        }
    }

    private void saveAdDasBenchmarkTaskCommonDetail(Long modifyId, AdDasBenchmarkMsg adDasBenchmarkMsg) {

        log.info("saveAdDasBenchmarkTaskCommonDetail() modifyId={}", modifyId);

        AdDasBenchmarkTaskCommonDetailEdit edit = adDasBenchmarkTaskCommonDetailEditRepository
                .getByBenchmarkIdAndMainTable(adDasBenchmarkMsg.getBenchmarkId(),adDasBenchmarkMsg.getMainTable());
        log.info("saveAdDasBenchmarkTaskCommonDetail() edit={}", edit);
        AdDasBenchmarkTaskCommonDetail adDasBenchmarkTaskCommonDetail = adDasBenchMarkTaskCommonDetailRepository
                .getByBenchmarkTypeAndMainTable(edit.getBenchmarkId(),edit.getMainTable());
        log.info("saveAdDasBenchmarkTaskCommonDetail() adDasBenchmarkTaskCommonDetail={}", adDasBenchmarkTaskCommonDetail);
        if (adDasBenchmarkTaskCommonDetail == null) {
            adDasBenchmarkTaskCommonDetail = new AdDasBenchmarkTaskCommonDetail();
            AdDasBeanCoreUtils.copyPropertiesIgnoreNull(adDasBenchmarkTaskCommonDetail, edit);
            adDasBenchmarkTaskCommonDetail.setModifyId(modifyId);
            adDasBenchMarkTaskCommonDetailRepository.insert(adDasBenchmarkTaskCommonDetail);
        } else {
            long id = adDasBenchmarkTaskCommonDetail.getId();
            AdDasBenchmarkTaskCommonDetailHistory history = new AdDasBenchmarkTaskCommonDetailHistory();
            AdDasBeanCoreUtils.copyPropertiesIgnoreNull(history, adDasBenchmarkTaskCommonDetail);
            history.setModifyId(modifyId);
            adDasBenchmarkTaskCommonDetailHistoryRepository.insert(history);
            AdDasBeanCoreUtils.copyPropertiesIgnoreNull(adDasBenchmarkTaskCommonDetail, edit);
            adDasBenchmarkTaskCommonDetail.setId(id);
            adDasBenchmarkTaskCommonDetail.setModifyId(modifyId);
            log.info("saveAdDasBenchmarkTaskCommonDetail() adDasBenchmarkTaskCommonDetail2={}", adDasBenchmarkTaskCommonDetail);
            adDasBenchMarkTaskCommonDetailRepository.update(adDasBenchmarkTaskCommonDetail);
        }

    }

    private void saveAdDasBenchmarkTask(AdDasUserView adDasUserView, Long modifyId,  AdDasBenchmarkMsg adDasBenchmarkMsg) {

        adDasBenchmarkTaskEditRepository.updateStatusByBenchmarkTypeAndMainTable(adDasBenchmarkMsg.getBenchmarkId(), adDasBenchmarkMsg.getMainTable(),
                AdDasModifyStatusEnum.PUBLISH_SUC.getCode(), adDasUserView.getUserName(), System.currentTimeMillis());
        List<AdDasBenchmarkTaskEdit> adDasBenchmarkTaskEdits = adDasBenchmarkTaskEditRepository.getByBenchmarkTypeAndMainTable(adDasBenchmarkMsg.getBenchmarkId(),
                adDasBenchmarkMsg.getMainTable());
        List<AdDasBenchmarkTask> tasks = adDasBenchmarkTaskRepository.queryByBenchmarkId(adDasBenchmarkMsg.getBenchmarkId(),adDasBenchmarkMsg.getDataSource(),
                adDasBenchmarkMsg.getPbClass(),adDasBenchmarkMsg.getMainTable());
        if(!CollectionUtils.isEmpty(adDasBenchmarkTaskEdits) && CollectionUtils.isEmpty(tasks)) {
            for (AdDasBenchmarkTaskEdit edit : adDasBenchmarkTaskEdits) {
                AdDasBenchmarkTask adDasBenchmarkTask = new AdDasBenchmarkTask();
                AdDasBeanCoreUtils.copyPropertiesIgnoreNull(adDasBenchmarkTask,edit);
                adDasBenchmarkTaskRepository.insert(adDasBenchmarkTask);
            }
        }
    }
    private void rollbackBenchmark(long modifyId) {

        // 查询 ad_das_modify_benchmark_msg 表
        AdDasBenchmarkMsg adDasBenchmarkMsg = adDasBenchmarkMsgRepository.getByModifyId(modifyId);

        AdDasBenchmarkTaskCommonDetailHistory history = adDasBenchmarkTaskCommonDetailHistoryRepository.getByModifyId(modifyId);
        if (history != null) {

            if (adDasBenchmarkMsg.getDeleteTable()) {
                AdDasBenchmarkTaskCommonDetail adDasBenchmarkTaskCommonDetail = new AdDasBenchmarkTaskCommonDetail();
                AdDasBeanCoreUtils.copyPropertiesIgnoreNull(adDasBenchmarkTaskCommonDetail, history);
                log.info("modifyId={}, adDasBenchmarkTaskCommonDetail={}", modifyId, adDasBenchmarkTaskCommonDetail);
                adDasBenchmarkTaskCommonDetail.setId(null);
                adDasBenchMarkTaskCommonDetailRepository.insert(adDasBenchmarkTaskCommonDetail);

                // 查询 task history 表 还原 task_history
                List<AdDasBenchmarkTaskHistory> adDasBenchmarkTaskHistories = benchmarkTaskHistoryRepository.listByModifyId(modifyId);

                for (AdDasBenchmarkTaskHistory benchmarkTaskHistory : adDasBenchmarkTaskHistories) {
                    AdDasBenchmarkTask adDasBenchmarkTask = new AdDasBenchmarkTask();
                    AdDasBeanCoreUtils.copyPropertiesIgnoreNull(adDasBenchmarkTask,benchmarkTaskHistory);
                    adDasBenchmarkTask.setId(null);
                    log.info("rollback adDasBenchmarkTask={}", adDasBenchmarkTask);
                    adDasBenchmarkTaskRepository.insert(adDasBenchmarkTask);
                }

            } else {
                AdDasBenchmarkTaskCommonDetail adDasBenchmarkTaskCommonDetail = adDasBenchMarkTaskCommonDetailRepository
                        .getByBenchmarkTypeAndMainTable(adDasBenchmarkMsg.getBenchmarkId(), adDasBenchmarkMsg.getMainTable());
                if (adDasBenchmarkTaskCommonDetail != null) {
                    long id = adDasBenchmarkTaskCommonDetail.getId();
                    AdDasBeanCoreUtils.copyPropertiesIgnoreNull(adDasBenchmarkTaskCommonDetail, history);
                    adDasBenchmarkTaskCommonDetail.setId(id);
                    adDasBenchMarkTaskCommonDetailRepository.update(adDasBenchmarkTaskCommonDetail);
                }

                AdDasBenchmarkTaskCommonDetailEdit commonDetailEdit = adDasBenchmarkTaskCommonDetailEditRepository
                        .getByBenchmarkIdAndMainTable(adDasBenchmarkMsg.getBenchmarkId(), adDasBenchmarkMsg.getMainTable());
                if (commonDetailEdit != null) {
                    long id = commonDetailEdit.getId();
                    AdDasBeanCoreUtils.copyPropertiesIgnoreNull(commonDetailEdit, history);
                    commonDetailEdit.setId(id);
                    adDasBenchmarkTaskCommonDetailEditRepository.update(commonDetailEdit);
                }
            }
        } else {

            // 确认是首次接入
            List<AdDasBenchmarkMsg> adDasBenchmarkMsgs = adDasBenchmarkMsgRepository
                    .getByBenchmarkTypeAndMainTable(adDasBenchmarkMsg.getBenchmarkId(), adDasBenchmarkMsg.getMainTable());

            log.info("delete task And commonDetail adDasBenchmarkMsgs={}", adDasBenchmarkMsgs);

            if (!CollectionUtils.isEmpty(adDasBenchmarkMsgs) && adDasBenchmarkMsgs.size() >= 1) {

                // 删除 task表 和 common表
                adDasBenchMarkTaskCommonDetailRepository.deleteByBenchmarkIdAndMainTable(adDasBenchmarkMsg.getBenchmarkId(), adDasBenchmarkMsg.getMainTable());
                adDasBenchmarkTaskRepository.deleteByBenchmarkIdeAndMainTable(adDasBenchmarkMsg.getBenchmarkId(), adDasBenchmarkMsg.getMainTable());

                log.info("delete task And commonDetail adDasBenchmarkMsg={}", adDasBenchmarkMsg);

                adDasBenchmarkTaskCommonDetailEditRepository.deleteByBenchmarkTypeAndMainTable(adDasBenchmarkMsg.getBenchmarkId(), adDasBenchmarkMsg.getMainTable());
                adDasBenchmarkTaskEditRepository.deleteByBenchmarkTypeAndMainTable(adDasBenchmarkMsg.getBenchmarkId(), adDasBenchmarkMsg.getMainTable());

                log.info("delete taskEdit And commonDetailEdit");
            }
        }
    }
}
