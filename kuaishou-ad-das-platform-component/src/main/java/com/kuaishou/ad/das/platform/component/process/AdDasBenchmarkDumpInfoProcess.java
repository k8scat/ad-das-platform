package com.kuaishou.ad.das.platform.component.process;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.hadoop.fs.FSDataInputStream;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import com.google.protobuf.util.JsonFormat;
import com.kuaishou.ad.das.platform.utils.HdfsUtils;
import com.kuaishou.ad.model.protobuf.tables.Meta;

import lombok.extern.slf4j.Slf4j;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-08-15
 */
@Lazy
@Slf4j
@Service
public class AdDasBenchmarkDumpInfoProcess implements HdfsUtils.StreamProcessor {

    private Meta.DumpInfo dumpInfo = null;

    @Override
    public void process(FSDataInputStream stream) throws Exception {

        try {
            List<Integer> list = new ArrayList<>();
            BufferedInputStream bufferedInputStream = new BufferedInputStream(stream);

            int value = 0;
            while ((value = bufferedInputStream.read()) != -1) {
                list.add(value);
            }
            dumpInfo = parseDumpInfo(list);

        } catch (Exception e) {
            log.error("handle file error, info = ", e);
        }
    }

    private Meta.DumpInfo parseDumpInfo(List<Integer> list) throws IOException {

        if (CollectionUtils.isEmpty(list)) {
            return null;
        }

        byte[] dataBytes = new byte[list.size()];
        for (int i = 0; i < list.size(); ++i) {
            dataBytes[i] = (byte) (list.get(i).intValue());
        }
        String dataJson = new String(dataBytes);
        log.info("dataJson={}", dataJson);

        return toProtoBean(Meta.DumpInfo.newBuilder(), dataJson);
    }

    public static Meta.DumpInfo toProtoBean(Meta.DumpInfo.Builder targetBuilder, String json) throws IOException {
        JsonFormat.parser().merge(json, targetBuilder);
        return targetBuilder.build();
    }

    public Meta.DumpInfo getDumpInfo() {
        Meta.DumpInfo.Builder dumpInfoBuilder = Meta.DumpInfo.newBuilder();

        dumpInfoBuilder.addAllInfo(dumpInfo.getInfoList());

        Meta.DumpInfo result = dumpInfoBuilder.build();

        clear();

        return result;
    }

    public void clear() {
        this.dumpInfo = null;
    }
}
