package com.kuaishou.ad.das.platform.component.biz.modify.impl;

import static com.kuaishou.ad.das.platform.utils.kconf.AdDasPlatformKconf.adDasPlatformUserMsgs;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.kuaishou.ad.das.platform.component.biz.increment.AdDasIncrementModifyQueryBiz;
import com.kuaishou.ad.das.platform.component.biz.modify.AdDasModifyQueryBiz;
import com.kuaishou.ad.das.platform.component.handle.BenchmarkModifyHandler;
import com.kuaishou.ad.das.platform.component.web.param.AdDasModifySearchParam;
import com.kuaishou.ad.das.platform.component.web.param.benchmark.BenchmarkModifyQueryParam;
import com.kuaishou.ad.das.platform.component.web.view.AdDasModifyBenchmarkDataView;
import com.kuaishou.ad.das.platform.component.web.view.AdDasModifyDataListView;
import com.kuaishou.ad.das.platform.component.web.view.AdDasModifyDataView;
import com.kuaishou.ad.das.platform.component.web.view.AdDasModifyIncrementView;
import com.kuaishou.ad.das.platform.component.web.view.AdDasModifyPbDetailView;
import com.kuaishou.ad.das.platform.component.web.view.AdDasModifyPbView;
import com.kuaishou.ad.das.platform.component.web.view.AdDasModifyTypeView;
import com.kuaishou.ad.das.platform.component.web.view.AdDasServiceInstancesView;
import com.kuaishou.ad.das.platform.component.web.view.AdDasStatusView;
import com.kuaishou.ad.das.platform.component.web.view.benchmark.modify.BenchmarkModifyQueryItemView;
import com.kuaishou.ad.das.platform.component.web.view.benchmark.modify.BenchmarkModifyQueryView;
import com.kuaishou.ad.das.platform.core.benchmark.model.AdDasBenchmarkMainTableView;
import com.kuaishou.ad.das.platform.core.common.daszk.Helper.AdDasPlatformCommonZkHelper;
import com.kuaishou.ad.das.platform.core.common.daszk.model.AdDasServiceNodeSchema;
import com.kuaishou.ad.das.platform.dal.dao.BenchmarkModifyMsgCondition;
import com.kuaishou.ad.das.platform.dal.dao.ModifyCondition;
import com.kuaishou.ad.das.platform.dal.model.AdDasBenchmark;
import com.kuaishou.ad.das.platform.dal.model.AdDasBenchmarkMsg;
import com.kuaishou.ad.das.platform.dal.model.AdDasDataStreamGroup;
import com.kuaishou.ad.das.platform.dal.model.AdDasIncrement;
import com.kuaishou.ad.das.platform.dal.model.AdDasModify;
import com.kuaishou.ad.das.platform.dal.model.AdDasModifyIncrementTableDetail;
import com.kuaishou.ad.das.platform.dal.model.AdDasModifyPbMsg;
import com.kuaishou.ad.das.platform.dal.model.AdDasModifyServicePbRecord;
import com.kuaishou.ad.das.platform.dal.repository.AdDasBenchmarkMsgRepository;
import com.kuaishou.ad.das.platform.dal.repository.AdDasBenchmarkRepository;
import com.kuaishou.ad.das.platform.dal.repository.AdDasIncrementRepository;
import com.kuaishou.ad.das.platform.dal.repository.AdDasModifyIncrementTableDetailRepository;
import com.kuaishou.ad.das.platform.dal.repository.AdDasModifyPbMsgRepository;
import com.kuaishou.ad.das.platform.dal.repository.AdDasModifyRepository;
import com.kuaishou.ad.das.platform.dal.repository.AdDasModifyServicePbRecordRepository;
import com.kuaishou.ad.das.platform.utils.AdDasBeanCoreUtils;
import com.kuaishou.ad.das.platform.utils.dasenum.AdDasModifyPublishStatusEnum;
import com.kuaishou.ad.das.platform.utils.dasenum.AdDasModifyStatusEnum;
import com.kuaishou.ad.das.platform.utils.dasenum.AdDasModifyTypeEnum;

import lombok.extern.slf4j.Slf4j;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-12-06
 */
@Lazy
@Slf4j
@Service
public class AdDasModifyQueryBizImpl implements AdDasModifyQueryBiz {

    @Autowired
    private AdDasModifyRepository adDasModifyRepository;

    @Autowired
    private AdDasModifyPbMsgRepository adDasModifyPbMsgRepository;

    @Autowired
    private AdDasPlatformCommonZkHelper adDasCommonZkHelper;

    @Autowired
    private AdDasIncrementModifyQueryBiz incrementManageQueryBiz;

    @Autowired
    private AdDasModifyServicePbRecordRepository adDasModifyServicePbRecordRepository;

    @Autowired
    private AdDasBenchmarkMsgRepository adDasBenchmarkMsgRepository;

    @Autowired
    private AdDasModifyIncrementTableDetailRepository modifyIncrementTableDetailRepository;

    @Autowired
    private BenchmarkModifyHandler benchmarkModifyHandler;

    @Autowired
    private AdDasIncrementRepository incrementRepository;

    @Autowired
    private AdDasBenchmarkRepository benchmarkRepository;


    @Override
    public List<AdDasStatusView> queryModifyStatusEnum() {
        return Arrays.stream(AdDasModifyStatusEnum.values())
                .map(this::buildStatusView).collect(Collectors.toList());
    }

    @Override
    public List<AdDasModifyTypeView> queryModifyTypeEnum() {
        return  Arrays.stream(AdDasModifyTypeEnum.values()).map(this::buildTypeView).collect(Collectors.toList());
    }

    @Override
    public AdDasModifyDataListView modifyHome(AdDasModifySearchParam param) {
        AdDasModifyDataListView result = new AdDasModifyDataListView();
        Integer modifyType = param.getModifyType();
        if (modifyType == 0) {
            modifyType = null;
        }

        Integer modifyStatus = param.getModifyStatus();
        if (modifyStatus == 0) {
            modifyStatus = null;
        }

        Integer limit = param.getPageSize();
        Integer offset = param.getPageSize() * (param.getPageIndex() - 1);
        Long count = adDasModifyRepository.countByCondition(param.getModifyName(), modifyType, modifyStatus,
                param.getStartTime(), param.getEndTime());
        List<AdDasModify> adDasModifies = adDasModifyRepository.queryByCondition(
                param.getModifyName(), modifyType, modifyStatus,
                param.getStartTime(), param.getEndTime(), limit, offset);
        // 增加查询服务 信息 和 审批人信息
        List<Long> idLists = adDasModifies.stream()
                .map(AdDasModify::getId).collect(Collectors.toList());

        Map<Long, String> id2ServiceNameMap = getServiceNameMap(idLists);
        List<AdDasModifyDataView> modifyDataViews = adDasModifies.stream()
                .map(adDasModify -> convertModifyDataView(adDasModify, id2ServiceNameMap))
                .collect(Collectors.toList());

        result.setModifyDataViews(modifyDataViews);
        result.setPageSize(param.getPageSize());
        result.setPageNum(param.getPageIndex());
        result.setTotalCount(count);
        return result;
    }

    private Map<Long, String> getServiceNameMap(List<Long> idLists) {
        Map<Long, String> result = Maps.newHashMap();
        if (CollectionUtils.isEmpty(idLists)) {
            return result;
        }
        List<AdDasModifyPbMsg> adDasModifyPbMsgs = adDasModifyPbMsgRepository.queryByModifyIds(idLists);
        if (!CollectionUtils.isEmpty(adDasModifyPbMsgs)) {
            Map<Long, String> pbMsgMap = adDasModifyPbMsgs.stream()
                    .collect(Collectors.toMap(AdDasModifyPbMsg::getModifyId,
                            AdDasModifyPbMsg::getServiceName));
            if (!CollectionUtils.isEmpty(pbMsgMap)) {

                result.putAll(pbMsgMap);
            }
        }

        List<AdDasModifyIncrementTableDetail> modifyIncrementTableDetails = modifyIncrementTableDetailRepository
                .queryByModifyIds(idLists);
        if (!CollectionUtils.isEmpty(modifyIncrementTableDetails)) {

            Set<Long> increIds = modifyIncrementTableDetails.stream()
                    .map(AdDasModifyIncrementTableDetail::getIncreId)
                    .collect(Collectors.toSet());

            // 查询增量服务
            List<AdDasIncrement> adDasIncrements = incrementRepository.queryByIncreId(increIds);
            Map<Long, String> incrementMsgMap = adDasIncrements.stream()
                    .collect(Collectors.toMap(AdDasIncrement::getId,
                            AdDasIncrement::getIncrementName));
            log.info("increIds={}, incrementMsgMap={}", increIds, incrementMsgMap);
            Map<Long, String> incrementModifyMap = Maps.newHashMap();
            modifyIncrementTableDetails.stream()
                    .filter(modifyIncrementTableDetail -> modifyIncrementTableDetail.getTableType() == 1)
                    .forEach(modifyIncrementTableDetail -> {
                        String increName = incrementMsgMap.getOrDefault(modifyIncrementTableDetail.getIncreId(), "增量服务名未找到");
                        incrementModifyMap.put(modifyIncrementTableDetail.getModifyId(), increName);
                    });

            if (!CollectionUtils.isEmpty(incrementModifyMap)) {
                result.putAll(incrementModifyMap);
            }
        }

        List<AdDasBenchmarkMsg> adDasBenchmarkMsgs = adDasBenchmarkMsgRepository.getByModifyIds(idLists);
        if (!CollectionUtils.isEmpty(adDasBenchmarkMsgs)) {
            Map<Long, String> benchmarkModifyMap = Maps.newHashMap();

            Set<Long> benchmarkIds = adDasBenchmarkMsgs.stream()
                    .map(AdDasBenchmarkMsg::getBenchmarkId)
                    .collect(Collectors.toSet());

            List<AdDasBenchmark> adDasBenchmarks = benchmarkRepository.queryById(benchmarkIds);
            Map<Long, String> benchmarkMsgMap = adDasBenchmarks.stream()
                    .collect(Collectors.toMap(AdDasBenchmark::getId,
                            AdDasBenchmark::getBenchmarkName));
            log.info("benchmarkIds={}, benchmarkMsgMap={}", benchmarkIds, benchmarkMsgMap);
            adDasBenchmarkMsgs.forEach(adDasBenchmarkMsg -> {
                String benchmarkName = benchmarkMsgMap.getOrDefault(adDasBenchmarkMsg.getBenchmarkId(), "基准流服务名未找到");
                benchmarkModifyMap.put(adDasBenchmarkMsg.getModifyId(), benchmarkName);
            });
            if (!CollectionUtils.isEmpty(benchmarkModifyMap)) {
                result.putAll(benchmarkModifyMap);
            }
        }
        return result;
    }

    private AdDasModifyDataView convertModifyDataView(AdDasModify adDasModify, Map<Long, String> id2ServiceNameMap) {
        AdDasModifyDataView adDasModifyDataView = new AdDasModifyDataView();
        AdDasBeanCoreUtils.copyPropertiesIgnoreNull(adDasModifyDataView, adDasModify);
        adDasModifyDataView.setModifyType(adDasModify.getModifyType());
        adDasModifyDataView.setApprover(adDasModify.getApprover());
        adDasModifyDataView.setObjectName(id2ServiceNameMap.getOrDefault(adDasModify.getId(), ""));

        return adDasModifyDataView;
    }

    @Override
    public AdDasModifyBenchmarkDataView benchmarkModifyData(Long modifyId) {
        // 1. 查询基准变更表
        BenchmarkModifyMsgCondition benchmarkModifyCondition = new BenchmarkModifyMsgCondition();
        benchmarkModifyCondition.setModifyIds(Collections.singletonList(modifyId));
        List<AdDasBenchmarkMsg> benchmarkMsgs = adDasBenchmarkMsgRepository.query(benchmarkModifyCondition);
        AdDasBenchmarkMsg benchmarkMsg = benchmarkMsgs.get(0);
        // 2. 查变更表
        benchmarkModifyCondition.setModifyIds(Collections.singletonList(modifyId));
        ModifyCondition modifyCondition = new ModifyCondition();
        modifyCondition.setModifyIds(Collections.singletonList(modifyId));
        List<AdDasModify> modifyList = adDasModifyRepository.query(modifyCondition);
        AdDasModify modify = modifyList.get(0);
        // 3. 查基准表和数据流组表
        Pair<AdDasBenchmark, AdDasDataStreamGroup> benchmarkAndGroup =
                benchmarkModifyHandler.getBenchmarkAndGroup(benchmarkMsg.getBenchmarkId());
        // 4. build
        return AdDasModifyBenchmarkDataView.build(modify, benchmarkMsg, benchmarkAndGroup.getLeft(),
                benchmarkAndGroup.getRight());

    }

    @Override
    public List<String> queryApprover() {
        List<String> approvers = Lists.newArrayList();
        // Kconf配置
        Map<String, Integer> userMaps = adDasPlatformUserMsgs.get();
        userMaps.forEach((key, value) -> {
            if (value == 1) {
                approvers.add(key);
            }
        });
        return approvers;
    }

    @Override
    public AdDasModifyPbView pbModifyData(Long modifyId, Integer modifyType) {
        AdDasModifyPbView adDasModifyPbView = new AdDasModifyPbView();
        // 查询 ad_das_modify 工单数据
        List<AdDasModify> adDasModifies = adDasModifyRepository.queryByIdAndType(modifyId, modifyType);
        if (CollectionUtils.isEmpty(adDasModifies)) {
            return adDasModifyPbView;
        }
        log.info("pbModifyData() modifyId={}, modifyType={}", modifyId, modifyType);
        AdDasModify adDasModify = adDasModifies.get(0);
        AdDasBeanCoreUtils.copyPropertiesIgnoreNull(adDasModifyPbView, adDasModify);
        AdDasModifyTypeEnum modifyTypeEnum = AdDasModifyTypeEnum
                .ofType(adDasModify.getModifyType());
        adDasModifyPbView.setModifyType(modifyTypeEnum.getDesc());
        // 查询 ad_das_modify_pb_msg 工单详情
        List<AdDasModifyPbMsg> adDasModifyPbMsgs = adDasModifyPbMsgRepository
                .queryByModifyId(modifyId);
        if (CollectionUtils.isEmpty(adDasModifyPbMsgs)) {
            return adDasModifyPbView;
        }
        AdDasModifyPbDetailView view = convertPbDetailView(adDasModifyPbMsgs.get(0));
        adDasModifyPbView.setView(view);
        return adDasModifyPbView;
    }

    private AdDasModifyPbDetailView convertPbDetailView(AdDasModifyPbMsg adDasModifyPbMsg) {
        AdDasModifyPbDetailView adDasModifyPbDetailView = new AdDasModifyPbDetailView();
        adDasModifyPbDetailView.setCurLastestPbVersion(adDasModifyPbMsg.getCurLastestPbVersion());
        adDasModifyPbDetailView.setCurServicePbVersion(adDasModifyPbMsg.getCurPbVersion());
        adDasModifyPbDetailView.setServiceName(adDasModifyPbMsg.getServiceName());
        adDasModifyPbDetailView.setTargetPbVersion(adDasModifyPbMsg.getTargetPbVersion());
        adDasModifyPbDetailView.setCrLinks(adDasModifyPbMsg.getCrLinks());
        adDasModifyPbDetailView.setPiplineLinks(adDasModifyPbMsg.getPiplineLinks());
        return adDasModifyPbDetailView;
    }

    @Override
    public List<AdDasServiceInstancesView> queryServiceInstance(Long modifyId, String serviceName) {
        List<AdDasServiceInstancesView> result = Lists.newArrayList();
        try {
            // 读取ZK 节点下的数据
            List<AdDasServiceNodeSchema> adDasServiceNodeSchemas = adDasCommonZkHelper
                    .queryServiceNodeSchemas(serviceName);
            log.info("queryServiceInstance() adDasServiceNodeSchemas={}", adDasServiceNodeSchemas);

            if (CollectionUtils.isEmpty(adDasServiceNodeSchemas)) {
                return result;
            }
            // 查询 ad_das_modify_pb_msg 工单详情
            List<AdDasModifyPbMsg> adDasModifyPbMsgs = adDasModifyPbMsgRepository
                    .queryByModifyId(modifyId);
            String targetPbVersion = "";
            if (!CollectionUtils.isEmpty(adDasModifyPbMsgs)) {
                AdDasModifyPbMsg adDasModifyPbMsg = adDasModifyPbMsgs.get(0);
                targetPbVersion = adDasModifyPbMsg.getTargetPbVersion();
            }

            // TODO 批量查询服务状态
            for (AdDasServiceNodeSchema adDasServiceNodeSchema : adDasServiceNodeSchemas) {
                AdDasServiceInstancesView adDasServiceInstancesView = new AdDasServiceInstancesView();
                String serviceNodeName = adDasServiceNodeSchema.getContainerId();
                // 读取 ad_das_modify_service_pb_record 中的数据
                List<AdDasModifyServicePbRecord> adDasModifyServicePbRecords = adDasModifyServicePbRecordRepository
                        .queryByCondition(modifyId, serviceName, serviceNodeName);
                if (!CollectionUtils.isEmpty(adDasModifyServicePbRecords)) {
                    AdDasModifyServicePbRecord adDasModifyServicePbRecord = adDasModifyServicePbRecords.get(0);
                    adDasServiceInstancesView.setServiceName(serviceName);
                    adDasServiceInstancesView.setInstanceName(serviceNodeName);
                    adDasServiceInstancesView.setCurPbVersion(adDasServiceNodeSchema.getPlatformProtoSchema().getPbVersion());
                    adDasServiceInstancesView.setStatus(adDasModifyServicePbRecord.getPublishStatus());
                    adDasServiceInstancesView.setTargetPbVersion(targetPbVersion);
                } else {
                    adDasServiceInstancesView.setServiceName(serviceName);
                    adDasServiceInstancesView.setInstanceName(serviceNodeName);
                    adDasServiceInstancesView.setCurPbVersion(adDasServiceNodeSchema.getPlatformProtoSchema().getPbVersion());
                    adDasServiceInstancesView.setStatus(AdDasModifyPublishStatusEnum.WAITING.getCode());
                    adDasServiceInstancesView.setTargetPbVersion(targetPbVersion);
                }
                result.add(adDasServiceInstancesView);
            }
        } catch (Exception e) {
            log.error("queryServiceInstance() occur error! modifyId={}, serviceName={}",
                    modifyId, serviceName, e);
        }
        return result;
    }

    @Override
    public AdDasBenchmarkMainTableView queryBenchmarkByTable(String mainTableName) {
        return null;
    }

    @Override
    public AdDasModifyIncrementView incrementModifyData(Long modifyId, Integer modifyType) {
        AdDasModifyIncrementView adDasModifyIncrementView = new AdDasModifyIncrementView();

        return adDasModifyIncrementView;
    }

    @Override
    public BenchmarkModifyQueryView queryView(BenchmarkModifyQueryParam param) {
        BenchmarkModifyMsgCondition benchmarkModifyMsgCondition = new BenchmarkModifyMsgCondition();
        if (!CollectionUtils.isEmpty(param.getBenchmarkIds())) {
            benchmarkModifyMsgCondition.setBenchmarkIds(param.getBenchmarkIds());
        }
        if (!StringUtils.isEmpty(param.getMainTableName())) {
            benchmarkModifyMsgCondition.setMainTable(param.getMainTableName());
        }
        List<AdDasBenchmarkMsg> modifyMsgList = adDasBenchmarkMsgRepository.query(benchmarkModifyMsgCondition);
        ModifyCondition modifyCondition = new ModifyCondition();
        if (!StringUtils.isEmpty(param.getModifyName())) {
            modifyCondition.setModifyName(param.getModifyName());
        }
        if (!StringUtils.isEmpty(param.getOperatorEmailPrefix())) {
            modifyCondition.setOperator(param.getOperatorEmailPrefix());
        }
        if (param.getStartTime() != null && param.getEndTime() != null) {
            modifyCondition.setStartTime2EndTime(Pair.of(param.getStartTime(), param.getEndTime()));
        }
        List<AdDasModify> modifyList = adDasModifyRepository.query(modifyCondition);
        Map<Long, AdDasModify> modifyId2Modify = modifyList.stream()
                .collect(Collectors.toMap(AdDasModify::getId, item -> item));
        List<BenchmarkModifyQueryItemView> itemViews = modifyMsgList.stream()
                .map(msg -> {

                    if (modifyId2Modify.get(msg.getModifyId()) == null || msg == null) {
                        return null;
                    } else {
                        return BenchmarkModifyQueryItemView.build(modifyId2Modify.get(msg.getModifyId()), msg);
                    }
                })
                .filter(msg -> msg != null)
                .collect(Collectors.toList());
        BenchmarkModifyQueryView view = new BenchmarkModifyQueryView();
        view.setDataList(itemViews);
        view.setPageNum(param.getPageNum());
        view.setPageSize(param.getPageSize());
        view.setTotalCount(Long.valueOf(itemViews.size()));
        return view;
    }

    private AdDasModifyTypeView buildTypeView(AdDasModifyTypeEnum adDasModifyTypeEnum) {
        AdDasModifyTypeView adDasModifyTypeView = new AdDasModifyTypeView();
        adDasModifyTypeView.setModifyType(adDasModifyTypeEnum.getCode());
        adDasModifyTypeView.setModifyTypeName(adDasModifyTypeEnum.getDesc());
        return adDasModifyTypeView;
    }

    private AdDasStatusView buildStatusView(AdDasModifyStatusEnum adDasStatusEnum) {
        AdDasStatusView adDasStatusView = new AdDasStatusView();
        adDasStatusView.setStatusCode(adDasStatusEnum.getCode());
        adDasStatusView.setStatusName(adDasStatusEnum.getDesc());
        return adDasStatusView;
    }

}
