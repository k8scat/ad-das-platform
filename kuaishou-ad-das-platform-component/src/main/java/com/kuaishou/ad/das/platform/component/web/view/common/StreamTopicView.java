package com.kuaishou.ad.das.platform.component.web.view.common;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author Zhang Jialin <zhangjialin@kuaishou.com>
 * Created on 2021-05-18
 */
@Data
@ApiModel(value = "Topic信息")
public class StreamTopicView {
    @ApiModelProperty(value = "Topic id")
    private Long topicId;
    @ApiModelProperty(value = "Topic名称")
    private String topicName;
}
