package com.kuaishou.ad.das.platform.component.biz.benchmark.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.kuaishou.ad.das.platform.component.biz.benchmark.AdDasBenchmarkTaskModifyBiz;
import com.kuaishou.ad.das.platform.component.biz.common.AdDasBenchmarkTaskType;
import com.kuaishou.ad.das.platform.component.biz.schema.AdDasSchemaQueryBiz;
import com.kuaishou.ad.das.platform.component.handle.BenchmarkModifyHandler;
import com.kuaishou.ad.das.platform.component.web.view.AdDasSchemaPbClassView;
import com.kuaishou.ad.das.platform.component.web.view.benchmark.modify.AdDasBenchmarkTaskView;
import com.kuaishou.ad.das.platform.core.benchmark.model.AdDasBenchmarkBaseTaskInfoView;
import com.kuaishou.ad.das.platform.core.benchmark.model.AdDasBenchmarkCascadeTableView;
import com.kuaishou.ad.das.platform.core.benchmark.model.AdDasBenchmarkDb2PbColumnMsgView;
import com.kuaishou.ad.das.platform.core.benchmark.model.AdDasBenchmarkMainTableView;
import com.kuaishou.ad.das.platform.dal.model.AdDasBenchmark;
import com.kuaishou.ad.das.platform.dal.model.AdDasBenchmarkTask;
import com.kuaishou.ad.das.platform.dal.model.AdDasBenchmarkTaskCommonDetail;
import com.kuaishou.ad.das.platform.dal.model.AdDasBenchmarkTaskCommonDetailEdit;
import com.kuaishou.ad.das.platform.dal.model.AdDasBenchmarkTaskEdit;
import com.kuaishou.ad.das.platform.dal.model.AdDasDataStreamGroup;
import com.kuaishou.ad.das.platform.dal.repository.AdDasBenchMarkTaskCommonDetailRepository;
import com.kuaishou.ad.das.platform.dal.repository.AdDasBenchmarkTaskCommonDetailEditRepository;
import com.kuaishou.ad.das.platform.dal.repository.AdDasBenchmarkTaskEditRepository;
import com.kuaishou.ad.das.platform.dal.repository.AdDasBenchmarkTaskRepository;
import com.kuaishou.ad.das.platform.utils.dasenum.AdDasModifyStatusEnum;
import com.kuaishou.ad.das.platform.utils.dasenum.AdDasServiceEnum;
import com.kuaishou.ad.das.platform.utils.model.AdDasUserView;
import com.kuaishou.framework.util.ObjectMapperUtils;

import lombok.extern.slf4j.Slf4j;

/**
 * @author wangkai <wangkai14@kuaishou.com>
 * Created on 2020-12-30
 */
@Slf4j
@Lazy
@Service
public class AdDasBenchmarkTaskModifyBizImpl implements AdDasBenchmarkTaskModifyBiz {

    @Autowired
    private AdDasBenchmarkTaskEditRepository adDasBenchmarkTaskEditRepository;

    @Autowired
    private AdDasBenchmarkTaskRepository adDasBenchmarkTaskRepository;

    @Autowired
    private AdDasBenchMarkTaskCommonDetailRepository adDasBenchMarkTaskCommonDetailRepository;

    @Autowired
    private AdDasBenchmarkTaskCommonDetailEditRepository adDasBenchmarkTaskCommonDetailEditRepository;

    @Autowired
    private AdDasSchemaQueryBiz adDasSchemaQueryBiz;

    @Autowired
    private BenchmarkModifyHandler benchmarkModifyHandler;

    @Override
    public AdDasBenchmarkTaskView benchmarkTaskDetail(Long taskId, Integer taskType) {
        if (taskType.equals(AdDasBenchmarkTaskType.ONLINE.getType())) {
            return adDasBenchmarkTaskProcess(taskId);
        }
        return adDasBenchmarkTaskEditProcess(taskId);
    }

    @Override
    @Transactional
    public void benchmarkTaskEdit(AdDasUserView adDasUserView, AdDasBenchmarkTaskView adDasBenchmarkTaskView) {
        if (adDasBenchmarkTaskView.getTaskType().equals(AdDasBenchmarkTaskType.ONLINE.getType())) {
            onlineAdDasBenchmarkTaskProcess(adDasUserView, adDasBenchmarkTaskView);
        } else {
            editAdDasBenchmarkTaskProcess(adDasUserView, adDasBenchmarkTaskView);
        }
    }

    private AdDasBenchmarkTaskView adDasBenchmarkTaskProcess(long taskId) {
        AdDasBenchmarkTask adDasBenchMarkTask = adDasBenchmarkTaskRepository.queryByTaskId(taskId);

        if (adDasBenchMarkTask == null) {
            return null;
        }

        AdDasBenchmarkTaskCommonDetail adDasBenchmarkTaskCommonDetail = adDasBenchMarkTaskCommonDetailRepository
                .getByBenchmarkTypeAndMainTable(adDasBenchMarkTask.getBenchmarkId(), adDasBenchMarkTask.getMainTable());
        List<AdDasBenchmarkTask> adDasBenchmarkTasks = adDasBenchmarkTaskRepository.queryByBenchmarkId(adDasBenchMarkTask.getBenchmarkId(), adDasBenchMarkTask.getDataSource(),
                adDasBenchMarkTask.getPbClass(), adDasBenchMarkTask.getMainTable());
        AdDasBenchmarkTaskView view = buildAdDasBenchmarkTaskView(adDasBenchMarkTask, adDasBenchmarkTaskCommonDetail, adDasBenchmarkTasks);
        view.setTaskType(AdDasBenchmarkTaskType.ONLINE.getType());
        return view;
    }

    private AdDasBenchmarkTaskView adDasBenchmarkTaskEditProcess(long taskId) {
        AdDasBenchmarkTaskEdit adDasBenchmarkTaskEdit = adDasBenchmarkTaskEditRepository.getByTaskId(taskId);
        AdDasBenchmarkTaskCommonDetailEdit adDasBenchmarkTaskCommonDetailEdit = adDasBenchmarkTaskCommonDetailEditRepository
                .getByBenchmarkIdAndMainTable(adDasBenchmarkTaskEdit.getBenchmarkId(), adDasBenchmarkTaskEdit.getMainTable());
        List<AdDasBenchmarkTaskEdit> adDasBenchmarkTaskEdits = adDasBenchmarkTaskEditRepository.
                getByBenchmarkType(adDasBenchmarkTaskEdit.getBenchmarkId(), adDasBenchmarkTaskEdit.getDataSource(), adDasBenchmarkTaskEdit.getPbClass(), adDasBenchmarkTaskEdit.getMainTable());
        AdDasBenchmarkTaskView view = buildAdDasBenchmarkTaskView(adDasBenchmarkTaskEdit, adDasBenchmarkTaskCommonDetailEdit, adDasBenchmarkTaskEdits);
        view.setTaskType(AdDasBenchmarkTaskType.EDIT.getType());
        return view;
    }

    private AdDasBenchmarkTaskView buildAdDasBenchmarkTaskView(AdDasBenchmarkTaskEdit adDasBenchmarkTaskEdit,
                                                               AdDasBenchmarkTaskCommonDetailEdit adDasBenchmarkTaskCommonDetailEdit,
                                                               List<? extends AdDasBenchmarkTaskEdit> adDasBenchmarkTaskEdits) {
        AdDasBenchmarkTaskView view = new AdDasBenchmarkTaskView();
        AdDasBenchmarkMainTableView mainTableView = new AdDasBenchmarkMainTableView();

        Pair<AdDasBenchmark, AdDasDataStreamGroup> benchmark2Group =
                benchmarkModifyHandler.getBenchmarkAndGroup(adDasBenchmarkTaskEdit.getBenchmarkId());

        mainTableView.setBenchmarkId(adDasBenchmarkTaskEdit.getBenchmarkId());
        mainTableView.setDataSource(adDasBenchmarkTaskEdit.getDataSource());
        mainTableView.setMainTable(adDasBenchmarkTaskEdit.getMainTable());
        mainTableView.setPbClass(adDasBenchmarkTaskEdit.getPbClass());
        mainTableView.setTableType(adDasBenchmarkTaskEdit.getTableType());
        mainTableView.setPredictGrowth(adDasBenchmarkTaskCommonDetailEdit.getPredictGrowth());
        mainTableView.setCommonSqls(adDasBenchmarkTaskCommonDetailEdit.getCommonSqls());
        mainTableView.setTimeParams(adDasBenchmarkTaskCommonDetailEdit.getTimeParams());
        mainTableView.setShardSqlFlag(adDasBenchmarkTaskEdit.getShardSqlFlag());
        mainTableView.setShardSqlColumn(adDasBenchmarkTaskCommonDetailEdit.getShardSqlColumn());

        AdDasBenchmark benchmark = benchmark2Group.getLeft();
        AdDasDataStreamGroup group = benchmark2Group.getRight();

        if (benchmark != null && group != null) {
            mainTableView.setBenchmarkName(benchmark.getBenchmarkName());
            mainTableView.setDataStreamGroupName(group.getDataStreamGroupName());
            mainTableView.setDataStreamGroupId(group.getId());
        }

        String cascadeColumnSchema = adDasBenchmarkTaskCommonDetailEdit.getCascadeColumnSchema();
        if (!StringUtils.isEmpty(cascadeColumnSchema)) {
            List<AdDasBenchmarkCascadeTableView> cascadeTableViews = ObjectMapperUtils
                    .fromJSON(cascadeColumnSchema, List.class, AdDasBenchmarkCascadeTableView.class);
            mainTableView.setCascadeTableViews(cascadeTableViews);
        }

        List<AdDasBenchmarkDb2PbColumnMsgView> views = new Gson().fromJson(adDasBenchmarkTaskCommonDetailEdit.getColumnSchema(),
                new TypeToken<List<AdDasBenchmarkDb2PbColumnMsgView>>() {
                }.getType());
        mainTableView.setDb2PbColumnMsgViews(views);
        view.setMainTableView(mainTableView);

        List<AdDasBenchmarkBaseTaskInfoView> adDasBenchmarkBaseTaskInfoViews = new ArrayList<>();
        for (AdDasBenchmarkTaskEdit taskEdit : adDasBenchmarkTaskEdits) {
            AdDasBenchmarkBaseTaskInfoView adDasBenchmarkBaseTaskInfoView = new AdDasBenchmarkBaseTaskInfoView();
            adDasBenchmarkBaseTaskInfoView.setTaskId(taskEdit.getId());
            adDasBenchmarkBaseTaskInfoView.setTaskName(taskEdit.getTaskName());
            adDasBenchmarkBaseTaskInfoView.setTaskNumber(taskEdit.getTaskNumber());
            adDasBenchmarkBaseTaskInfoView.setTaskSqlFlag(taskEdit.getTaskSqlFlag());
            adDasBenchmarkBaseTaskInfoView.setTaskShardList(taskEdit.getTaskShardList());
            adDasBenchmarkBaseTaskInfoView.setTaskSqls(taskEdit.getTaskSqls());
            adDasBenchmarkBaseTaskInfoView.setShardIds(taskEdit.getShardIds());
            adDasBenchmarkBaseTaskInfoView.setTaskStatus(taskEdit.getTaskStatus());
            adDasBenchmarkBaseTaskInfoView.setConcurrency(taskEdit.getConcurrency());
            adDasBenchmarkBaseTaskInfoView.setShardSqlFlag(taskEdit.getShardSqlFlag());
            adDasBenchmarkBaseTaskInfoViews.add(adDasBenchmarkBaseTaskInfoView);
        }
        view.setAdDasBenchmarkBaseTaskInfoViews(adDasBenchmarkBaseTaskInfoViews);

        List<String> serviceList = adDasSchemaQueryBiz.queryServiceList(AdDasServiceEnum.AD_DAS_BASE.getName());
        if (!CollectionUtils.isEmpty(serviceList)) {
            String pbVersion = adDasSchemaQueryBiz.queryServicePb(serviceList.get(0));
            List<AdDasSchemaPbClassView> pbClassViews = adDasSchemaQueryBiz.querySchemaPbClass(pbVersion, adDasBenchmarkTaskEdit.getPbClass());
            view.setPbClassViews(pbClassViews);
        } else {
            log.error("adDasSchemaQueryBiz.queryServiceList() data empty!!! serviceType={}", AdDasServiceEnum.AD_DAS_BASE.getName());
        }

        return view;
    }

    public void updateAdDasBenchmarkTaskCommonDetail(AdDasUserView adDasUserView, AdDasBenchmarkTaskView adDasBenchmarkTaskView) {
        AdDasBenchmarkTaskCommonDetail adDasBenchmarkTaskCommonDetail = adDasBenchMarkTaskCommonDetailRepository
                .getByBenchmarkTypeAndMainTable(adDasBenchmarkTaskView.getMainTableView().getBenchmarkId(), adDasBenchmarkTaskView.getMainTableView().getMainTable());
        fillAdDasBenchmarkTaskCommonDetailEdit(adDasUserView, adDasBenchmarkTaskCommonDetail, adDasBenchmarkTaskView);
        adDasBenchMarkTaskCommonDetailRepository.update(adDasBenchmarkTaskCommonDetail);
    }

    public void updateAdDasBenchmarkTaskList(AdDasUserView adDasUserView, AdDasBenchmarkTaskView adDasBenchmarkTaskView) {
        List<AdDasBenchmarkBaseTaskInfoView> adDasBenchmarkBaseTaskInfoViews = adDasBenchmarkTaskView.getAdDasBenchmarkBaseTaskInfoViews();
        if (CollectionUtils.isEmpty(adDasBenchmarkBaseTaskInfoViews)) {
            return;
        }
        //
        List<AdDasBenchmarkTask> adDasBenchmarkTasks = adDasBenchmarkTaskRepository
                .queryListByBenchmarkAndTable(adDasBenchmarkTaskView.getMainTableView().getMainTable(),
                        adDasBenchmarkTaskView.getMainTableView().getBenchmarkId());

        Set<String> taskNameSet = adDasBenchmarkBaseTaskInfoViews.stream()
                .map(AdDasBenchmarkBaseTaskInfoView::getTaskName)
                .collect(Collectors.toSet());

        adDasBenchmarkBaseTaskInfoViews.forEach(item -> updateAdDasBenchmarkTask(adDasUserView.getUserName(), adDasBenchmarkTaskView, item));

        //  删除任务
        adDasBenchmarkTasks.forEach(item -> {
            if (!taskNameSet.contains(item.getTaskName())) {
                adDasBenchmarkTaskRepository.deleteByTaskId(item.getId());
            }
        });

    }

    private void updateAdDasBenchmarkTask(String operator, AdDasBenchmarkTaskView adDasBenchmarkTaskView,
                                          AdDasBenchmarkBaseTaskInfoView taskInfoView) {
        AdDasBenchmarkTask adDasBenchmarkTask = adDasBenchmarkTaskRepository
                .getByBenchmarkAndName(taskInfoView.getTaskName(), adDasBenchmarkTaskView.getMainTableView().getBenchmarkId());

        if (adDasBenchmarkTask == null) {
            adDasBenchmarkTask = createAdDasBenchmarkTask(adDasBenchmarkTaskView);
            fillAdDasBenchmarkTaskEdit(adDasBenchmarkTask, operator, taskInfoView);
            adDasBenchmarkTaskRepository.insert(adDasBenchmarkTask);
        } else {
            fillAdDasBenchmarkTaskEdit(adDasBenchmarkTask, operator, taskInfoView);
            adDasBenchmarkTaskRepository.updateAdDasBenchmarkTask(adDasBenchmarkTask);
        }
    }

    private void updateAdDasBenchmarkTaskEdit(String operator, AdDasBenchmarkTaskView adDasBenchmarkTaskView, AdDasBenchmarkBaseTaskInfoView taskInfoView) {
        AdDasBenchmarkTaskEdit adDasBenchmarkTaskEdit = adDasBenchmarkTaskEditRepository
                .getByTypeAndName(taskInfoView.getTaskName(), adDasBenchmarkTaskView.getMainTableView().getBenchmarkId());
        if (adDasBenchmarkTaskEdit == null) {
            adDasBenchmarkTaskEdit = createAdDasBenchmarkTaskEdit(adDasBenchmarkTaskView);
            fillAdDasBenchmarkTaskEdit(adDasBenchmarkTaskEdit, operator, taskInfoView);
            adDasBenchmarkTaskEditRepository.insert(adDasBenchmarkTaskEdit);
        } else {
            fillAdDasBenchmarkTaskEdit(adDasBenchmarkTaskEdit, operator, taskInfoView);
            adDasBenchmarkTaskEditRepository.updateAdDasBenchmarkTaskEdit(adDasBenchmarkTaskEdit);
        }

    }

    private void fillAdDasBenchmarkTaskCommonDetailEdit(AdDasUserView adDasUserView, AdDasBenchmarkTaskCommonDetailEdit adDasBenchmarkTaskCommonDetailEdit, AdDasBenchmarkTaskView adDasBenchmarkTaskView) {
        adDasBenchmarkTaskCommonDetailEdit.setColumnSchema(new Gson().toJson(adDasBenchmarkTaskView.getMainTableView().getDb2PbColumnMsgViews()));
        adDasBenchmarkTaskCommonDetailEdit.setCommonSqls(adDasBenchmarkTaskView.getMainTableView().getCommonSqls() == null ? "" : adDasBenchmarkTaskView.getMainTableView().getCommonSqls());
        adDasBenchmarkTaskCommonDetailEdit.setTimeParams(adDasBenchmarkTaskView.getMainTableView().getTimeParams() == null ? "" : adDasBenchmarkTaskView.getMainTableView().getTimeParams());
        adDasBenchmarkTaskCommonDetailEdit.setPredictGrowth(adDasBenchmarkTaskView.getMainTableView().getPredictGrowth());
        adDasBenchmarkTaskCommonDetailEdit.setOperator(adDasUserView.getUserName());
        adDasBenchmarkTaskCommonDetailEdit.setUpdateTime(System.currentTimeMillis());
    }

    private void fillAdDasBenchmarkTaskEdit(AdDasBenchmarkTaskEdit adDasBenchmarkTaskEdit, String operator, AdDasBenchmarkBaseTaskInfoView taskInfoView) {
        adDasBenchmarkTaskEdit.setTaskName(taskInfoView.getTaskName());
        adDasBenchmarkTaskEdit.setTaskNumber(taskInfoView.getTaskNumber());
        adDasBenchmarkTaskEdit.setTaskSqlFlag(taskInfoView.getTaskSqlFlag());
        adDasBenchmarkTaskEdit.setTaskSqls(taskInfoView.getTaskSqls() == null ? "" : taskInfoView.getTaskSqls());
        adDasBenchmarkTaskEdit.setTaskShardList(taskInfoView.getTaskShardList() == null ? "" : taskInfoView.getTaskShardList());
        adDasBenchmarkTaskEdit.setConcurrency(taskInfoView.getConcurrency());
        adDasBenchmarkTaskEdit.setShardSqlFlag(taskInfoView.getShardSqlFlag());
        adDasBenchmarkTaskEdit.setShardIds(taskInfoView.getShardIds() == null ? "" : taskInfoView.getShardIds());
        adDasBenchmarkTaskEdit.setOperator(operator);
        adDasBenchmarkTaskEdit.setUpdateTime(System.currentTimeMillis());
    }

    private void updateAdDasBenchmarkTaskEditList(AdDasUserView adDasUserView, AdDasBenchmarkTaskView adDasBenchmarkTaskView) {
        List<AdDasBenchmarkBaseTaskInfoView> adDasBenchmarkBaseTaskInfoViews = adDasBenchmarkTaskView.getAdDasBenchmarkBaseTaskInfoViews();
        if (CollectionUtils.isEmpty(adDasBenchmarkBaseTaskInfoViews)) {
            return;
        }

        // 查询主表对应的数据
        List<AdDasBenchmarkTaskEdit> adDasBenchmarkTaskEdits = adDasBenchmarkTaskEditRepository
                .getByTypeAndTable(adDasBenchmarkTaskView.getMainTableView().getMainTable(), adDasBenchmarkTaskView.getMainTableView().getBenchmarkId());

        Set<String> taskNameSet = adDasBenchmarkBaseTaskInfoViews.stream()
                .map(AdDasBenchmarkBaseTaskInfoView::getTaskName)
                .collect(Collectors.toSet());

        adDasBenchmarkBaseTaskInfoViews.forEach(item -> updateAdDasBenchmarkTaskEdit(adDasUserView.getUserName(),
                adDasBenchmarkTaskView, item));

        //  删除任务
        adDasBenchmarkTaskEdits.forEach(item -> {
            if (!taskNameSet.contains(item.getTaskName())) {
                adDasBenchmarkTaskEditRepository.deleteByTaskId(item.getId());
            }
        });

    }

    private void updateAdDasBenchmarkTaskCommonDetailEdit(AdDasUserView adDasUserView, AdDasBenchmarkTaskView adDasBenchmarkTaskView) {
        AdDasBenchmarkTaskCommonDetailEdit adDasBenchmarkTaskCommonDetailEdit = adDasBenchmarkTaskCommonDetailEditRepository
                .getByBenchmarkIdAndMainTable(adDasBenchmarkTaskView.getMainTableView().getBenchmarkId(), adDasBenchmarkTaskView.getMainTableView().getMainTable());
        fillAdDasBenchmarkTaskCommonDetailEdit(adDasUserView, adDasBenchmarkTaskCommonDetailEdit, adDasBenchmarkTaskView);
        adDasBenchmarkTaskCommonDetailEditRepository.update(adDasBenchmarkTaskCommonDetailEdit);
    }

    private void onlineAdDasBenchmarkTaskProcess(AdDasUserView adDasUserView, AdDasBenchmarkTaskView adDasBenchmarkTaskView) {
        updateAdDasBenchmarkTaskCommonDetail(adDasUserView, adDasBenchmarkTaskView);
        updateAdDasBenchmarkTaskList(adDasUserView, adDasBenchmarkTaskView);
    }

    private void editAdDasBenchmarkTaskProcess(AdDasUserView adDasUserView, AdDasBenchmarkTaskView adDasBenchmarkTaskView) {
        updateAdDasBenchmarkTaskCommonDetailEdit(adDasUserView, adDasBenchmarkTaskView);
        updateAdDasBenchmarkTaskEditList(adDasUserView, adDasBenchmarkTaskView);
    }

    private AdDasBenchmarkTaskEdit createAdDasBenchmarkTaskEdit(AdDasBenchmarkTaskView adDasBenchmarkTaskView) {
        AdDasBenchmarkTaskEdit adDasBenchmarkTaskEdit = new AdDasBenchmarkTaskEdit();
        createAdDasBenchmarkTaskEdit(adDasBenchmarkTaskView, adDasBenchmarkTaskEdit);
        return adDasBenchmarkTaskEdit;
    }

    private void createAdDasBenchmarkTaskEdit(AdDasBenchmarkTaskView adDasBenchmarkTaskView, AdDasBenchmarkTaskEdit adDasBenchmarkTaskEdit) {
        adDasBenchmarkTaskEdit.setBenchmarkId(adDasBenchmarkTaskView.getMainTableView().getBenchmarkId());
        adDasBenchmarkTaskEdit.setDataSource(adDasBenchmarkTaskView.getMainTableView().getDataSource());
        adDasBenchmarkTaskEdit.setMainTable(adDasBenchmarkTaskView.getMainTableView().getMainTable());
        adDasBenchmarkTaskEdit.setTableType(adDasBenchmarkTaskView.getMainTableView().getTableType());
        adDasBenchmarkTaskEdit.setPbClass(adDasBenchmarkTaskView.getMainTableView().getPbClass());
        // TODO 反射获取 pbClassFullName
        String pbClassFullName = "kuaishou.ad.tables." + adDasBenchmarkTaskView.getMainTableView().getPbClass();
        adDasBenchmarkTaskEdit.setPbClassFullName(pbClassFullName);
        adDasBenchmarkTaskEdit.setCreatTime(System.currentTimeMillis());
        adDasBenchmarkTaskEdit.setTaskStatus(AdDasModifyStatusEnum.EDITING.getCode());
    }

    private AdDasBenchmarkTask createAdDasBenchmarkTask(AdDasBenchmarkTaskView adDasBenchmarkTaskView) {
        AdDasBenchmarkTask adDasBenchmarkTask = new AdDasBenchmarkTask();
        createAdDasBenchmarkTaskEdit(adDasBenchmarkTaskView, adDasBenchmarkTask);
        return adDasBenchmarkTask;
    }
}
