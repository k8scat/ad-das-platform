package com.kuaishou.ad.das.platform.component.web.param;

import java.io.Serializable;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author caoyinhang <caoyinhang@kuaishou.com>
 * Created on 2021-07-08
 */
@Data
@ApiModel
public class AdDasIncrementServiceTableDataReSendParam implements Serializable {
    private static final long serialVersionUID = 3553033916492468376L;

    @ApiModelProperty(value = "数据源列表")
    private List<String> dataSourceList;

    @ApiModelProperty(value = "消费组名称列表")
    private List<String> consumerGroupList;

    @ApiModelProperty(value = "增量topic列表")
    private List<String> topicList;

    @ApiModelProperty(value = "消费topicType列表")
    private List<String> topicTypeList;

    @ApiModelProperty(value = "productDef列表")
    private List<String> productDefList;

    @ApiModelProperty(value = "最大回溯时间")
    private Integer maxBacktrackTime;

}
