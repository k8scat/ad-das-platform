package com.kuaishou.ad.das.platform.component.web.view.benchmark.modify;

import com.kuaishou.ad.das.platform.dal.model.AdDasBenchmarkMsg;
import com.kuaishou.ad.das.platform.dal.model.AdDasModify;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author Zhang Jialin <zhangjialin@kuaishou.com>
 * Created on 2021-05-26
 */
@Data
public class BenchmarkModifyQueryItemView {
    @ApiModelProperty(value = "变更id")
    private Long modifyId;
    @ApiModelProperty(value = "变更名称")
    private String modifyName;
    @ApiModelProperty(value = "变更类型")
    private Integer modifyType;
    @ApiModelProperty(value = "数据源名称")
    private String dataSourceName;
    @ApiModelProperty(value = "表名")
    private String mainTableName;
    @ApiModelProperty(value = "PB类名")
    private String pbClassName;
    @ApiModelProperty(value = "操作人邮箱前缀")
    private String operatorEmailPrefix;
    @ApiModelProperty(value = "审核人邮箱前缀")
    private String auditorEmailPrefix;
    @ApiModelProperty(value = "创建名称")
    private Long createTime;
    @ApiModelProperty(value = "更新名称")
    private Long updateTime;

    public static BenchmarkModifyQueryItemView build(AdDasModify modify, AdDasBenchmarkMsg msg) {
        BenchmarkModifyQueryItemView vo = new BenchmarkModifyQueryItemView();
        vo.setModifyId(modify.getId());
        vo.setModifyName(modify.getModifyName());
        vo.setModifyType(modify.getModifyType());
        vo.setCreateTime(modify.getCreateTime());
        vo.setUpdateTime(modify.getUpdateTime());
        vo.setAuditorEmailPrefix(modify.getApprover());
        vo.setDataSourceName(msg.getDataSource());
        vo.setMainTableName(msg.getMainTable());
        vo.setPbClassName(msg.getPbClass());
        vo.setOperatorEmailPrefix(modify.getOperator());
        return vo;
    }
}
