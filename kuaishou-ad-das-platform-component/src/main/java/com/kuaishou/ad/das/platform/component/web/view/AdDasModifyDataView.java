package com.kuaishou.ad.das.platform.component.web.view;

import java.io.Serializable;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-11-24
 */
@Data
public class AdDasModifyDataView implements Serializable {

    @ApiModelProperty(value = "变更id")
    private Long id;

    @ApiModelProperty(value = "变更类型")
    private String modifyName;

    @ApiModelProperty(value = "变更类型")
    private Integer modifyType;

    @ApiModelProperty(value = "操作人")
    private String operator;

    @ApiModelProperty(value = "变更工单状态")
    private Integer modifyStatus;

    @ApiModelProperty(value = "变更对象名称")
    private String objectName;

    @ApiModelProperty(value = "变更表名称")
    private String tableName;

    @ApiModelProperty(value = "变更表pb类")
    private String pbClass;

    @ApiModelProperty(value = "变更表pb类枚举")
    private String pbClassEnum;

    @ApiModelProperty(value = "变更服务审批人")
    private String approver;

    @ApiModelProperty(value = "创建时间")
    private Long createTime;

    @ApiModelProperty(value = "更新时间")
    private Long updateTime;
}
