package com.kuaishou.ad.das.platform.component.biz.increment.impl;

import static com.kuaishou.ad.das.platform.utils.kconf.AdDasBaseCommonKconf.adDasIncrementBacktrackMaxTime;
import static com.kuaishou.ad.das.platform.utils.kconf.AdDasPlatformKconf.adDasPlatformBacktrackUserMsgs;
import static java.lang.System.currentTimeMillis;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import com.google.common.collect.Maps;
import com.google.gson.Gson;
import com.kuaishou.ad.das.platform.component.biz.increment.AdDasIncrementBacktrackBiz;
import com.kuaishou.ad.das.platform.component.common.AdDasIncrementBacktrackStatus;
import com.kuaishou.ad.das.platform.component.web.param.AdDasIncrementBacktrackDataReSendParam;
import com.kuaishou.ad.das.platform.component.web.param.AdDasIncrementRecondDataReSendParam;
import com.kuaishou.ad.das.platform.component.web.param.AdDasIncrementRecondTableDataReSendParam;
import com.kuaishou.ad.das.platform.component.web.param.AdDasIncrementServiceDataReSendParam;
import com.kuaishou.ad.das.platform.component.web.param.AdDasIncrementServiceTableDataReSendParam;
import com.kuaishou.ad.das.platform.component.web.view.AdDasPlatformIncreBackRecondGraphView;
import com.kuaishou.ad.das.platform.component.web.view.AdDasPlatformIncreBackRecondSearchView;
import com.kuaishou.ad.das.platform.component.web.view.AdDasPlatformIncreBackServiceGraphView;
import com.kuaishou.ad.das.platform.component.web.view.AdDasPlatformIncreBackServiceSearchView;
import com.kuaishou.ad.das.platform.dal.model.AdDasIncrement;
import com.kuaishou.ad.das.platform.dal.model.AdDasIncrementBacktrackRecond;
import com.kuaishou.ad.das.platform.dal.model.AdDasIncrementBacktrackRecondDetail;
import com.kuaishou.ad.das.platform.dal.model.AdDasIncrementBacktrackServiceDetail;
import com.kuaishou.ad.das.platform.dal.model.AdDasIncrementServiceInfo;
import com.kuaishou.ad.das.platform.dal.repository.AdDasIncrementBacktrackRecondRepository;
import com.kuaishou.ad.das.platform.dal.repository.AdDasIncrementServiceInfoRepository;
import com.kuaishou.ad.das.platform.dal.repository.AdDasIncrementRepository;
import com.kuaishou.ad.das.platform.sal.kbus.AdDasIncrementBacktrackSAO;
import com.kuaishou.ad.das.platform.utils.model.AdDasUserView;

import lombok.extern.slf4j.Slf4j;

/**
 * @author caoyinhang <caoyinhang@kuaishou.com>
 * Created on 2021-07-06
 */

@Lazy
@Slf4j
@Service
public class AdDasIncrementBacktrackBizImpl implements AdDasIncrementBacktrackBiz {

    @Autowired
    private AdDasIncrementServiceInfoRepository adDasIncrementBacktrackServiceRepository;
    @Autowired
    private AdDasIncrementBacktrackRecondRepository adDasIncrementBacktrackRecondRepository;
    @Autowired
    private AdDasIncrementRepository adDasIncrementRepository;

    @Autowired
    private AdDasIncrementBacktrackSAO adDasIncrementBacktrackSAO;

    @Override
    public AdDasIncrementServiceTableDataReSendParam getServiceTableHead() {

        List<AdDasIncrementServiceInfo> adDasIncrementBacktrackServiceInfos = adDasIncrementBacktrackServiceRepository.queryAll();


        List<Long> increIdSet = adDasIncrementBacktrackServiceInfos.stream()
                .map(adDasIncrementBacktrackServiceInfo -> adDasIncrementBacktrackServiceInfo.getIncreId())
                .collect(Collectors.toList());
        log.info("获取服务表头",increIdSet);
        List<AdDasIncrement> adDasIncrements =
                adDasIncrementRepository.queryByIncreId(increIdSet);

        AdDasIncrementServiceTableDataReSendParam incrementServiceTableDataReSendParam = new AdDasIncrementServiceTableDataReSendParam();
        incrementServiceTableDataReSendParam.setDataSourceList(adDasIncrements.stream()
                .map(AdDasIncrement::getDataSource).distinct().filter(dataSource->!StringUtils.isEmpty(dataSource)).collect(Collectors.toList()));
        incrementServiceTableDataReSendParam.setConsumerGroupList(adDasIncrements.stream()
                .map(AdDasIncrement::getConsumerGroup).distinct().filter(consmerGroup->!StringUtils.isEmpty(consmerGroup)).collect(Collectors.toList()));
        incrementServiceTableDataReSendParam.setTopicList(adDasIncrements.stream()
                .map(AdDasIncrement::getTopic).distinct().filter(topic->!StringUtils.isEmpty(topic)).collect(Collectors.toList()));
        incrementServiceTableDataReSendParam.setTopicTypeList(adDasIncrementBacktrackServiceInfos.stream()
                .map(AdDasIncrementServiceInfo::getTopicType).distinct().filter(topicType->!StringUtils.isEmpty(topicType)).collect(Collectors.toList()));
        incrementServiceTableDataReSendParam.setProductDefList(adDasIncrementBacktrackServiceInfos.stream()
                .map(AdDasIncrementServiceInfo::getProductDef).distinct().filter(productDef->!StringUtils.isEmpty(productDef)).collect(Collectors.toList()));

        incrementServiceTableDataReSendParam.setMaxBacktrackTime(adDasIncrementBacktrackMaxTime.get());
        return incrementServiceTableDataReSendParam;
    }

    @Override
    public AdDasPlatformIncreBackServiceSearchView increBacktrackServiceSearch(AdDasIncrementServiceDataReSendParam incrementServiceDataReSendParam) {

        AdDasPlatformIncreBackServiceSearchView increBackServiceSearchView = new AdDasPlatformIncreBackServiceSearchView();
        // 查询 增量流信息
        List<AdDasIncrementBacktrackServiceDetail> adDasIncrementBacktrackServiceDetails =
                adDasIncrementBacktrackServiceRepository.queryByCondition(incrementServiceDataReSendParam.getIncrementName(),
                        incrementServiceDataReSendParam.getDataSource(),
                        incrementServiceDataReSendParam.getConsumerGroup(), incrementServiceDataReSendParam.getTopic());
        log.info("adDasIncrementBacktrackServiceDetails={}", adDasIncrementBacktrackServiceDetails);

        if (CollectionUtils.isEmpty(adDasIncrementBacktrackServiceDetails)) {
            return increBackServiceSearchView;
        }
        List<AdDasPlatformIncreBackServiceGraphView> increBackServiceGraphViews = adDasIncrementBacktrackServiceDetails
                .stream().map(adDasIncrementBacktrackServiceDetail -> convertIncrementServiceGraphView(adDasIncrementBacktrackServiceDetail))
                .collect(Collectors.toList());

        increBackServiceSearchView.setIncreSearchViews(increBackServiceGraphViews);
        increBackServiceSearchView.setTotalCount(increBackServiceGraphViews.size());
        return increBackServiceSearchView;
    }

    @Override
    public AdDasIncrementRecondTableDataReSendParam getRecondTableHead() {

        List<AdDasIncrementBacktrackRecond> adDasIncrementBacktrackReconds = adDasIncrementBacktrackRecondRepository.queryAll();

        List<Long> increIdSet = adDasIncrementBacktrackReconds.stream()
                .map(adDasIncrementBacktrackServiceInfo -> adDasIncrementBacktrackServiceInfo.getIncreId())
                .collect(Collectors.toList());

        List<AdDasIncrement> adDasIncrements =
                adDasIncrementRepository.queryByIncreId(increIdSet);

        AdDasIncrementRecondTableDataReSendParam incrementRecondTableDataReSendParam = new AdDasIncrementRecondTableDataReSendParam();

        incrementRecondTableDataReSendParam.setDataSourceList(adDasIncrements.stream()
                .map(AdDasIncrement::getDataSource).distinct().filter(dataSource->!StringUtils.isEmpty(dataSource)).collect(Collectors.toList()));
        incrementRecondTableDataReSendParam.setConsumerGroupList(adDasIncrements.stream()
                .map(AdDasIncrement::getConsumerGroup).distinct().filter(consmerGroup->!StringUtils.isEmpty(consmerGroup)).collect(Collectors.toList()));
        incrementRecondTableDataReSendParam.setTopicList(adDasIncrements.stream()
                .map(AdDasIncrement::getTopic).distinct().filter(topic->!StringUtils.isEmpty(topic)).collect(Collectors.toList()));
        incrementRecondTableDataReSendParam.setOperatorList(adDasIncrementBacktrackReconds.stream()
                .map(AdDasIncrementBacktrackRecond::getOperator).distinct().filter(operator->!StringUtils.isEmpty(operator)).collect(Collectors.toList()));
        incrementRecondTableDataReSendParam.setStatusList(new ArrayList<String>(){
            {
                add(AdDasIncrementBacktrackStatus.OPERATION_ALL.getDesc());
                add(AdDasIncrementBacktrackStatus.OPERATION_SUCCESS.getDesc());
                add(AdDasIncrementBacktrackStatus.OPERATION_FAILED.getDesc());
            }
        });
        return incrementRecondTableDataReSendParam;
    }

    @Override
    public AdDasPlatformIncreBackRecondSearchView increBacktrackRecondSearch(
            AdDasIncrementRecondDataReSendParam incrementRecondDataReSendParam, Integer status) {
        AdDasPlatformIncreBackRecondSearchView increBackRecondSearchView = new AdDasPlatformIncreBackRecondSearchView();

        String incrementName = incrementRecondDataReSendParam.getIncrementName();
        String dataSource = incrementRecondDataReSendParam.getDataSource();
        String consumerGroup = incrementRecondDataReSendParam.getConsumerGroup();
        String topic = incrementRecondDataReSendParam.getTopic();
        String operator = incrementRecondDataReSendParam.getOperator();
        String beginTime = incrementRecondDataReSendParam.getBeginTime();
        String endTime = incrementRecondDataReSendParam.getEndTime();

        Integer limit = incrementRecondDataReSendParam.getPageSize();
        Integer offset = incrementRecondDataReSendParam.getPageSize() * (incrementRecondDataReSendParam.getPageIndex() - 1);

        Integer count = adDasIncrementBacktrackRecondRepository
                .queryCountByCondition(incrementName,dataSource, consumerGroup,topic,operator,status,beginTime, endTime);

        List<AdDasIncrementBacktrackRecondDetail> adDasIncrementBacktrackReconds = adDasIncrementBacktrackRecondRepository
                .queryByCondition(incrementName,dataSource, consumerGroup,topic,operator,status,beginTime, endTime,limit,offset);

        if(CollectionUtils.isEmpty(adDasIncrementBacktrackReconds))
            return increBackRecondSearchView;

        List<AdDasPlatformIncreBackRecondGraphView> increBackRecondGraphViews =  adDasIncrementBacktrackReconds.stream()
                .map(adDasIncrementBacktrackRecond -> convertIncrementRecondGraphView(adDasIncrementBacktrackRecond)).collect(Collectors.toList());
        increBackRecondSearchView.setIncreBackRecondGraphViews(increBackRecondGraphViews);
        increBackRecondSearchView.setTotalCount(count);

        return increBackRecondSearchView;
    }

    @Override
    public String backtrack(AdDasIncrementBacktrackDataReSendParam incrementBacktrackDataReSendParam,
            AdDasUserView dasUserView){
        if (!adDasPlatformBacktrackUserMsgs.get().containsKey(dasUserView.getUserName()))
            return AdDasIncrementBacktrackStatus.BACKTRACK_FAILED_WITHOUT_RIGHTS.getDesc();
        if (incrementBacktrackDataReSendParam.getDurationSecond() > adDasIncrementBacktrackMaxTime.get())
            return AdDasIncrementBacktrackStatus.BACKTRACK_FAILED_TIME_TOOLONG.getDesc();

        AdDasIncrementBacktrackRecond adDasIncrementBacktrackRecond = adDasIncrementBacktrackRecondRepository.queryByOperator(dasUserView.getUserName());
        if (adDasIncrementBacktrackRecond != null){
            Long operationTime = adDasIncrementBacktrackRecond.getOperationTime();
            //限刷，60s之内只能操作一次
            if ((getTime()-operationTime)/1000 < 60){
                return AdDasIncrementBacktrackStatus.OPERATION_BUSY.getDesc();
            }
        }
        if (incrementBacktrackDataReSendParam.getProductDef()==null||incrementBacktrackDataReSendParam.getProductDef().equals("")||
                incrementBacktrackDataReSendParam.getIncreBackServiceGraphView().getDataSource()==null||incrementBacktrackDataReSendParam.getIncreBackServiceGraphView().getDataSource().equals("")||
                incrementBacktrackDataReSendParam.getTenant()==null||incrementBacktrackDataReSendParam.getTenant().equals("")||
                incrementBacktrackDataReSendParam.getConsumerGroup()==null||incrementBacktrackDataReSendParam.getConsumerGroup().equals("")||
                incrementBacktrackDataReSendParam.getTopicType()==null||incrementBacktrackDataReSendParam.getTopicType().equals("")||
                incrementBacktrackDataReSendParam.getRegion()==null||incrementBacktrackDataReSendParam.getRegion().equals("")||
                incrementBacktrackDataReSendParam.getDurationSecond()==null)
            return AdDasIncrementBacktrackStatus.BACKTRACK_FAILED_WITHOUT_PARAMS.getDesc();
        //使用ExecutorService将回溯响应时长限制在20s内
        ExecutorService exec = Executors.newFixedThreadPool(1);
        Callable<String> call = new Callable<String>() {
            public String call() throws Exception {
                String url = "https://kbus.corp.kuaishou.com/databus/open/consumer/resetOffset";
                String result = adDasIncrementBacktrackSAO.backtrack(url,incrementBacktrackDataReSendParam.getProductDef(),   //完成回溯
                        incrementBacktrackDataReSendParam.getIncreBackServiceGraphView().getDataSource(),
                        incrementBacktrackDataReSendParam.getTenant(),incrementBacktrackDataReSendParam.getConsumerGroup(),
                        incrementBacktrackDataReSendParam.getTopicType(),incrementBacktrackDataReSendParam.getRegion(),
                        incrementBacktrackDataReSendParam.getDurationSecond());
                return result;
            }
        };
        try {
            Future<String> future = exec.submit(call);
            String resultBacktrack = future.get(1000 * 20, TimeUnit.MILLISECONDS); //任务处理超时时间设为 20 秒

            AdDasIncrement adDasIncrement =
                    adDasIncrementRepository.queryByConditionBacktrack(incrementBacktrackDataReSendParam.getIncreBackServiceGraphView().getIncrementName(),
                            incrementBacktrackDataReSendParam.getIncreBackServiceGraphView().getDataSource(),
                            incrementBacktrackDataReSendParam.getIncreBackServiceGraphView().getConsumerGroup(),
                            incrementBacktrackDataReSendParam.getTopic());
            if(adDasIncrement==null)
                return AdDasIncrementBacktrackStatus.BACKTRACK_NULL.getDesc();
            AdDasIncrementBacktrackRecond incrementBacktrackRecond = new AdDasIncrementBacktrackRecond();

            Map<String,Object> map = Maps.newHashMap();
            Gson gson = new Gson();
            Map resultMap = gson.fromJson(resultBacktrack, map.getClass());

            if (!resultMap.get("status").equals(200.0)){

                incrementBacktrackRecond.setIncreId(adDasIncrement.getId());
                incrementBacktrackRecond.setOperator(incrementBacktrackDataReSendParam.getIncreBackServiceGraphView().getOperator());
                incrementBacktrackRecond.setOperationTime(getTime());
                incrementBacktrackRecond.setDurationSecond(incrementBacktrackDataReSendParam.getDurationSecond());
                incrementBacktrackRecond.setStatus(0);
                incrementBacktrackRecond.setApplicationRea(incrementBacktrackDataReSendParam.getApplicationRea());
                int insertMark = adDasIncrementBacktrackRecondRepository.insert(incrementBacktrackRecond);
                if (insertMark==0)
                    return AdDasIncrementBacktrackStatus.BACKTRACK_FAILED_INSERT_FAILED.getDesc()+" "+resultMap.get("status")+" "+resultMap.get("message");
                return AdDasIncrementBacktrackStatus.BACKTRACK_FAILED_INSERT_SUCCESS.getDesc()+" "+resultMap.get("status")+" "+resultMap.get("message");
            }

            incrementBacktrackRecond.setIncreId(adDasIncrement.getId());
            incrementBacktrackRecond.setOperator(incrementBacktrackDataReSendParam.getIncreBackServiceGraphView().getOperator());
            incrementBacktrackRecond.setOperationTime(getTime());
            incrementBacktrackRecond.setDurationSecond(incrementBacktrackDataReSendParam.getDurationSecond());
            incrementBacktrackRecond.setStatus(1);
            incrementBacktrackRecond.setApplicationRea(incrementBacktrackDataReSendParam.getApplicationRea());

            int insertMark = adDasIncrementBacktrackRecondRepository.insert(incrementBacktrackRecond);
            if (insertMark==0)
                return AdDasIncrementBacktrackStatus.BACKTRACK_SUCCESS_INSERT_FAILED.getDesc();
            return AdDasIncrementBacktrackStatus.BACKTRACK_SUCCESS.getDesc();

        } catch (TimeoutException ex) {
            log.info("回溯等待超过20s");
            AdDasIncrement adDasIncrement =
                    adDasIncrementRepository.queryByConditionBacktrack(incrementBacktrackDataReSendParam.getIncreBackServiceGraphView().getIncrementName(),
                            incrementBacktrackDataReSendParam.getIncreBackServiceGraphView().getDataSource(),
                            incrementBacktrackDataReSendParam.getIncreBackServiceGraphView().getConsumerGroup(),
                            incrementBacktrackDataReSendParam.getTopic());
            if(adDasIncrement==null)
                return AdDasIncrementBacktrackStatus.BACKTRACK_NULL.getDesc();
            AdDasIncrementBacktrackRecond incrementBacktrackRecond = new AdDasIncrementBacktrackRecond();
            incrementBacktrackRecond.setIncreId(adDasIncrement.getId());
            incrementBacktrackRecond.setOperator(incrementBacktrackDataReSendParam.getIncreBackServiceGraphView().getOperator());
            incrementBacktrackRecond.setOperationTime(getTime());
            incrementBacktrackRecond.setDurationSecond(incrementBacktrackDataReSendParam.getDurationSecond());
            incrementBacktrackRecond.setStatus(0);
            incrementBacktrackRecond.setApplicationRea(incrementBacktrackDataReSendParam.getApplicationRea());
            int insertMark = adDasIncrementBacktrackRecondRepository.insert(incrementBacktrackRecond);
            if (insertMark==0)
                return AdDasIncrementBacktrackStatus.BACKTRACK_FAILED_INSERT_FAILED.getDesc()+" "+AdDasIncrementBacktrackStatus.BACKTRACK_FAILED_WITHOUT_RESPONSE.getDesc();
            return AdDasIncrementBacktrackStatus.BACKTRACK_FAILED_INSERT_SUCCESS.getDesc()+" "+AdDasIncrementBacktrackStatus.BACKTRACK_FAILED_WITHOUT_RESPONSE.getDesc();
        } catch (Exception e) {
            log.info("调用回溯接口出现异常");
            e.printStackTrace();
        }
        exec.shutdown();
        return AdDasIncrementBacktrackStatus.BACKTRACK_FAILED.getDesc();
    }

    private AdDasPlatformIncreBackRecondGraphView convertIncrementRecondGraphView(AdDasIncrementBacktrackRecondDetail dasIncrementBacktrackRecondDetail) {
        AdDasPlatformIncreBackRecondGraphView increBackRecondGraphView = new AdDasPlatformIncreBackRecondGraphView();

        increBackRecondGraphView.setIncrementName(dasIncrementBacktrackRecondDetail.getIncrementName());
        increBackRecondGraphView.setDataSource(dasIncrementBacktrackRecondDetail.getDataSource());
        increBackRecondGraphView.setConsumerGroup(dasIncrementBacktrackRecondDetail.getConsumerGroup());
        increBackRecondGraphView.setTopicType(topicType2Chinese(dasIncrementBacktrackRecondDetail.getTopicType()));
        increBackRecondGraphView.setDurationSecond(dasIncrementBacktrackRecondDetail.getDurationSecond());
        increBackRecondGraphView.setOperator(dasIncrementBacktrackRecondDetail.getOperator());
        increBackRecondGraphView.setStatus(status2String(dasIncrementBacktrackRecondDetail.getStatus()));
        increBackRecondGraphView.setOperationTime(timeStamp2Date(dasIncrementBacktrackRecondDetail.getOperationTime()));
        return increBackRecondGraphView;
    }

    private AdDasPlatformIncreBackServiceGraphView convertIncrementServiceGraphView(
            AdDasIncrementBacktrackServiceDetail adDasIncrementBacktrackServiceDetail) {

        AdDasPlatformIncreBackServiceGraphView increBackServiceGraphView = new AdDasPlatformIncreBackServiceGraphView();

        Long increId = adDasIncrementBacktrackServiceDetail.getIncreId();

        AdDasIncrementBacktrackRecond adDasIncrementBacktrackRecond =
                adDasIncrementBacktrackRecondRepository.queryOperatorByIncreId(increId);
        if (adDasIncrementBacktrackRecond!=null){
            increBackServiceGraphView.setOperationTime(timeStamp2Date(adDasIncrementBacktrackRecond.getOperationTime()));
            increBackServiceGraphView.setOperator(adDasIncrementBacktrackRecond.getOperator());
        }
        increBackServiceGraphView.setIncrementName(adDasIncrementBacktrackServiceDetail.getIncrementName());
        increBackServiceGraphView.setDataSource(adDasIncrementBacktrackServiceDetail.getDataSource());
        increBackServiceGraphView.setConsumerGroup(adDasIncrementBacktrackServiceDetail.getConsumerGroup());
        increBackServiceGraphView.setTopicType(topicType2Chinese(adDasIncrementBacktrackServiceDetail.getTopicType()));
        increBackServiceGraphView.setOwnerName(adDasIncrementBacktrackServiceDetail.getOwnerName());
        return increBackServiceGraphView;

    }


    private long getTime(){
        return currentTimeMillis();
    }

    private String timeStamp2Date(long timeStamp) {
        SimpleDateFormat sdf = new SimpleDateFormat("YYYY/MM/dd HH:mm:ss");
        String date = sdf.format(timeStamp);
        return date;
    }

    private String status2String(Integer status){
        if (status==0)
            return "失败";
        return "成功";
    }

    private String topicType2Chinese(String topicType){
        if ("SEQUENCE".equals(topicType))
            return "事务消费";
        if ("RANDOM".equals(topicType))
            return "无序消费";
        return null;
    }

}
