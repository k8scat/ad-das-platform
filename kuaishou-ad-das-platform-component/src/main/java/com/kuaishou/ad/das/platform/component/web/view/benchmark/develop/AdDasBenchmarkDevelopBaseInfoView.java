package com.kuaishou.ad.das.platform.component.web.view.benchmark.develop;

import java.io.Serializable;
import java.util.List;

import com.kuaishou.ad.das.platform.component.web.view.benchmark.common.AdDasBenchmarkStatusView;

import lombok.Data;

/**
 * @author wangkai <wangkai14@kuaishou.com>
 * Created on 2020-12-24
 */
@Data
public class AdDasBenchmarkDevelopBaseInfoView implements Serializable {

    private static final long serialVersionUID = 8886497261621461770L;

    private List<AdDasBenchmarkStatusView> adDasBenchmarkStatusViews;

    List<String> mainTableNames;

    List<String> pbClassNames;

    List<String> mainDataSources;
}
