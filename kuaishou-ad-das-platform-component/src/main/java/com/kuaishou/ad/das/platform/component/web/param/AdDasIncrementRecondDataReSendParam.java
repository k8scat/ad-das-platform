package com.kuaishou.ad.das.platform.component.web.param;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author caoyinhang <caoyinhang@kuaishou.com>
 * Created on 2021-07-07
 */
@Data
@ApiModel
public class AdDasIncrementRecondDataReSendParam implements Serializable {


    private static final long serialVersionUID = 7450524132492002723L;

    @ApiModelProperty(value = "增量流名称")
    private String incrementName;

    @ApiModelProperty(value = "使用数据源")
    private String dataSource;

    @ApiModelProperty(value = "消费组名称")
    private String consumerGroup;

    @ApiModelProperty(value = "增量topic")
    private String topic;

    @ApiModelProperty(value = "操作人")
    private String operator;

    @ApiModelProperty(value = "操作状态")
    private String status;

    @ApiModelProperty(value = "开始时间")
    private String beginTime;

    @ApiModelProperty(value = "结束时间")
    private String endTime;

    @ApiModelProperty(value = "每页大小", required = true )
    protected Integer pageSize;

    @ApiModelProperty(value = "当前页码", required = true )
    protected Integer pageIndex;


}
