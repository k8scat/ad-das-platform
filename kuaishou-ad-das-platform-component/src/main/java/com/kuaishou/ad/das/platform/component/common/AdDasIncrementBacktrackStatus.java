package com.kuaishou.ad.das.platform.component.common;


import lombok.Getter;

/**
 * @author wangkai <wangkai14@kuaishou.com>
 * Created on 2021-01-04
 */
@Getter
public enum AdDasIncrementBacktrackStatus {
    /**
     * 回溯状态
     */
    BACKTRACK_SUCCESS(200,"回溯成功"),
    BACKTRACK_NULL(201,"查询增量回溯服务为空"),
    BACKTRACK_SUCCESS_INSERT_FAILED(202,"回溯成功入库失败"),

    BACKTRACK_FAILED( 400,"回溯失败"),
    BACKTRACK_FAILED_INSERT_SUCCESS( 401,"回溯失败入库成功"),
    BACKTRACK_FAILED_INSERT_FAILED(402,"回溯失败入库失败"),
    BACKTRACK_FAILED_WITHOUT_PARAMS(403,"回溯失败,参数不足"),


    OPERATION_BUSY(502,"一分钟内只能操作一次"),
    BACKTRACK_FAILED_WITHOUT_RIGHTS( 503,"回溯失败,您没有权限进行操作"),
    BACKTRACK_FAILED_TIME_TOOLONG( 504,"回溯失败，回溯时长超出限制"),

    BACKTRACK_FAILED_WITHOUT_RESPONSE( 504,"回溯失败，回溯操作20s内未响应"),

    OPERATION_ALL(1,"全部"),
    OPERATION_SUCCESS(2,"成功"),
    OPERATION_FAILED(3,"失败");

    private Integer status;
    private String desc;

    AdDasIncrementBacktrackStatus(Integer status , String desc) {
        this.status = status;
        this.desc = desc;
    }

}