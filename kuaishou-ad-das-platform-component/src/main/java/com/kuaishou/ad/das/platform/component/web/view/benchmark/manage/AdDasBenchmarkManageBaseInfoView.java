package com.kuaishou.ad.das.platform.component.web.view.benchmark.manage;

import java.util.List;

import lombok.Data;

/**
 * @author wangkai <wangkai14@kuaishou.com>
 * Created on 2020-12-24
 */
@Data
public class AdDasBenchmarkManageBaseInfoView {

    List<String> mainTableNames;

    List<String> pbClassNames;

    List<String> mainDataSources;
}


