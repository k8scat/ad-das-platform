package com.kuaishou.ad.das.platform.component.web.param;

import java.util.List;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author Zhang Jialin <zhangjialin@kuaishou.com>
 * Created on 2021-05-26
 */
@Data
public class StreamUserGroupEditParam {
    @ApiModelProperty(value = "用户组id", allowEmptyValue = true)
    private Long userGroupId;
    @ApiModelProperty(value = "用户组名称", allowEmptyValue = true)
    private String userGroupName;
    @ApiModelProperty(value = "用户组接口人中文名", allowEmptyValue = true)
    private String ownerName;
    @ApiModelProperty(value = "用户组接口人邮箱前缀",
            notes = "用于生成KIM链接：kim://username?username={emailPrefix}",
            allowEmptyValue = true)
    private String ownerEmailPrefix;
    @ApiModelProperty(value = "绑定列表", allowEmptyValue = true)
    private List<Long> bindIds;
    @ApiModelProperty(value = "绑定类型", allowEmptyValue = true)
    private Integer bindType;
}
