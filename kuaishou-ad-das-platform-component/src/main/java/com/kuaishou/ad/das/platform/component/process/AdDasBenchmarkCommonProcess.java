package com.kuaishou.ad.das.platform.component.process;

import java.io.BufferedInputStream;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.hadoop.fs.FSDataInputStream;
import org.slf4j.Logger;

import com.kuaishou.ad.das.platform.utils.HdfsUtils;
import com.kuaishou.framework.sample.RateLogger;

import lombok.extern.slf4j.Slf4j;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-08-23
 */
@Slf4j
public class AdDasBenchmarkCommonProcess implements HdfsUtils.StreamProcessorForProto {

    private static List<Integer> bytesMap = Arrays.asList(1, 256, 256 * 256, 256 * 256 * 256);
    private static final Logger rateLogger = RateLogger.rateLogger(log, 1.0);
    private List<Object> dataList = new ArrayList<>();
    private static final int LENGTH_BATCH_SIZE = 4;

    private enum BytesFlag {
        IsLength, IsProto
    }

    /*
     * 逻辑比较复杂，每4个字节表示一个int_num，后面int_num个字节是proto序列化结果，这样一直循环
     * */
    @Override
    public void process(FSDataInputStream stream, Class cl) {
        int batchSize = LENGTH_BATCH_SIZE;
        List<Integer> list = new ArrayList<>();
        BytesFlag bytesFlag = BytesFlag.IsLength;
        try {
            BufferedInputStream bufferedInputStream = new BufferedInputStream(stream);
            int value = 0;
            while ((value = bufferedInputStream.read()) != -1) {
                list.add(value);
                if (list.size() == batchSize) {
                    // 4字节长度
                    if (bytesFlag == BytesFlag.IsLength) {
                        batchSize = processBatchLength(list);
                        bytesFlag = BytesFlag.IsProto;
                        // pb序列化结构
                    } else if (bytesFlag == BytesFlag.IsProto) {
                        processBatchProto(list, cl);
                        batchSize = LENGTH_BATCH_SIZE;
                        bytesFlag = BytesFlag.IsLength;
                    }
                    list.clear();
                }
            }
            if (!list.isEmpty()) {
                log.error("left list not empty");
            }
        } catch (Exception e) {
            log.error("handle file error, info = ", e);
        }
    }

    private int processBatchLength(List<Integer> list) {
        int result = 0;
        for (int i = 0; i < list.size(); ++i) {
            result += list.get(i) * bytesMap.get(i);
        }
        return result;
    }

    /**
     * 将二进制字节转换成 结构化的proto
     * @param list
     */
    private void processBatchProto(List<Integer> list, Class cl) {
        byte[] protoBytes = new byte[list.size()];
        for (int i = 0; i < list.size(); ++i) {
            protoBytes[i] = (byte) (list.get(i).intValue());
        }
        try {

            Method method = cl.getMethod("parseFrom", new Class[]{byte[].class});
            Object obj = method.invoke(null, protoBytes);

            dataList.add(obj);
        } catch (Exception e) {
            log.error("processBatchProto() AdApp occur error", e);
        }
    }

    public List<Object> getDataList() {
        return dataList;
    }

    public void clear() {
        dataList.clear();
    }
}
