package com.kuaishou.ad.das.platform.component.web.view;

import java.util.List;

import com.kuaishou.ad.das.platform.component.web.view.common.StreamUserGroupView;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-05-24
 */
@Data
public class AdDasPlatformIncrementGraphView {

    @ApiModelProperty(value = "增量流id")
    private Long increId;
    @ApiModelProperty(value = "增量流英文code, 用于生成集群名称和topic")
    private String increCode;
    @ApiModelProperty(value = "增量流集群英文名称")
    private String increName;
    @ApiModelProperty(value = "增量流描述")
    private String increDesc;
    @ApiModelProperty(value = "增量数据源")
    private String dataSource;
    @ApiModelProperty(value = "增量表列表")
    private List<String> tableList;
    @ApiModelProperty(value = "操作人")
    private String operator;
    @ApiModelProperty(value = "创建时间")
    private Long createTime;
    @ApiModelProperty(value = "更新时间")
    private Long updateTime;
    @ApiModelProperty(value = "增量topic")
    private String dataTopic;

    private List<StreamUserGroupView> userGroups;

}
