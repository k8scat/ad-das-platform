package com.kuaishou.ad.das.platform.component.web.param.benchmark;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author Zhang Jialin <zhangjialin@kuaishou.com>
 * Created on 2021-05-21
 */
@Data
public class BenchmarkMainTableQueryItemParam {
    /**
     * 正式版本基准common_detail的id
     * ad_das_benchmark_task_common_detail.id
     */
    @ApiModelProperty(value = "id")
    private Long commonDetailId;
}
