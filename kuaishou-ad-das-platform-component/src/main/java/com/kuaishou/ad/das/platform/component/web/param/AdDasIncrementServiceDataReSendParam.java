package com.kuaishou.ad.das.platform.component.web.param;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author caoyinhang <caoyinhang@kuaishou.com>
 * Created on 2021-07-07
 */
@Data
@ApiModel
public class AdDasIncrementServiceDataReSendParam implements Serializable {

    private static final long serialVersionUID = 1067900509299185508L;

    @ApiModelProperty(value = "增量流名称")
    private String incrementName;

    @ApiModelProperty(value = "使用数据源")
    private String dataSource;

    @ApiModelProperty(value = "消费组名称")
    private String  consumerGroup;

    @ApiModelProperty(value = "增量topic")
    private String  topic;


}
