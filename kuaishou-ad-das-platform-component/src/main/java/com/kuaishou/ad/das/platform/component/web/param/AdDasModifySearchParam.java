package com.kuaishou.ad.das.platform.component.web.param;

import java.io.Serializable;

import lombok.Data;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-11-24
 */
@Data
public class AdDasModifySearchParam extends AdDasPageSearchParam implements Serializable {

    private static final long serialVersionUID = 6547801613680343450L;

    private Long startTime;

    private Long endTime;

    private String modifyName;

    private Integer modifyType;

    private Integer modifyStatus;

}
