package com.kuaishou.ad.das.platform.component.biz.usergroup.impl;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.kuaishou.ad.das.platform.component.biz.usergroup.UserGroupBiz;
import com.kuaishou.ad.das.platform.component.biz.usergroup.UserGroupWithBindDTO;
import com.kuaishou.ad.das.platform.component.web.param.StreamUserGroupEditParam;
import com.kuaishou.ad.das.platform.dal.dao.AdDasUserGroupBindCondition;
import com.kuaishou.ad.das.platform.dal.dao.AdDasUserGroupCondition;
import com.kuaishou.ad.das.platform.dal.model.AdDasDownstreamUserInfo;
import com.kuaishou.ad.das.platform.dal.model.AdDasDownstreamUserRelationBind;
import com.kuaishou.ad.das.platform.dal.repository.AdDasDownstreamUserRelationRepository;
import com.kuaishou.ad.das.platform.dal.repository.AdDasDownstreamUserInfoRepository;
import com.kuaishou.ad.das.platform.utils.AdDasBeanCoreUtils;

/**
 * @author Zhang Jialin <zhangjialin@kuaishou.com>
 * Created on 2021-06-11
 */
@Component
public class UserGroupBizImpl implements UserGroupBiz {
    @Autowired
    AdDasDownstreamUserRelationRepository userGroupRelationRepository;

    @Autowired
    AdDasDownstreamUserInfoRepository userGroupRepository;

    @Override
    @Transactional(value = "adDasTransactionManager", rollbackFor = Exception.class)
    public void editUserGroup(StreamUserGroupEditParam param) {
        if (param.getUserGroupId() == null || param.getUserGroupId() == 0) {
            // 新增用户组
            AdDasDownstreamUserInfo userGroup = new AdDasDownstreamUserInfo();
            userGroup.setUserGroupName(param.getUserGroupName());
            userGroup.setOwnerEmailPrefix(param.getOwnerEmailPrefix());
            userGroup.setOwnerName(param.getOwnerName());
            Long userGroupId = userGroupRepository.insert(userGroup);
            // 同时新增用户绑定关系
            if (!CollectionUtils.isEmpty(param.getBindIds())) {
                param.setUserGroupId(userGroupId);
                List<AdDasDownstreamUserRelationBind> binds = param.getBindIds().stream()
                        .map(id -> {
                            AdDasDownstreamUserRelationBind bind = buildUserGroupBindsByParam(id, param);
                            bind.setUserGroupId(userGroupId);
                            return bind;
                        }).collect(Collectors.toList());
                userGroupRelationRepository.insert(binds);
            }
        } else {
            // 修改用户组
            AdDasUserGroupCondition condition = new AdDasUserGroupCondition();
            condition.setUserGroupIds(Collections.singletonList(param.getUserGroupId()));
            List<AdDasDownstreamUserInfo> query = userGroupRepository.query(condition);
            AdDasDownstreamUserInfo userGroup = query.get(0);
            AdDasBeanCoreUtils.copyPropertiesIgnoreNull(userGroup, param);
            userGroupRepository.update(userGroup);
            // 同时修改用户组绑定关系：先删除当前类型的全部绑定关系，后对参数传入值进行insert
            if (!CollectionUtils.isEmpty(param.getBindIds())) {
                AdDasUserGroupBindCondition bindCondition = new AdDasUserGroupBindCondition();
                bindCondition.setUserGroupIds(Collections.singletonList(param.getUserGroupId()));
                bindCondition.setDataStreamTypes(Collections.singletonList(param.getBindType()));
                userGroupRelationRepository.delete(bindCondition);
                List<AdDasDownstreamUserRelationBind> binds = param.getBindIds().stream()
                        .map(id -> buildUserGroupBindsByParam(id, param)).collect(Collectors.toList());
                userGroupRelationRepository.insert(binds);
            }
        }
    }

    @Override
    public List<UserGroupWithBindDTO> queryUserGroupAndBind(AdDasUserGroupBindCondition bindCondition) {
        List<AdDasDownstreamUserRelationBind> binds = userGroupRelationRepository.query(bindCondition);
        List<Long> groupIds = binds.stream().map(AdDasDownstreamUserRelationBind::getUserGroupId).collect(Collectors.toList());
        AdDasUserGroupCondition groupCondition = new AdDasUserGroupCondition();
        groupCondition.setUserGroupIds(groupIds);
        Map<Long, AdDasDownstreamUserInfo> userGroupId2Item = userGroupRepository.query(groupCondition).stream()
                .collect(Collectors.toMap(AdDasDownstreamUserInfo::getId, item -> item));
        return binds.stream().map(bind ->
                UserGroupWithBindDTO.build(bind, userGroupId2Item.get(bind.getUserGroupId()))).collect(Collectors.toList());


    }

    private AdDasDownstreamUserRelationBind buildUserGroupBindsByParam(Long dataStreamId, StreamUserGroupEditParam param) {
        AdDasDownstreamUserRelationBind bind = new AdDasDownstreamUserRelationBind();
        bind.setUserGroupId(param.getUserGroupId());
        bind.setDataStreamId(dataStreamId);
        bind.setDataStreamType(param.getBindType());
        return bind;
    }

}
