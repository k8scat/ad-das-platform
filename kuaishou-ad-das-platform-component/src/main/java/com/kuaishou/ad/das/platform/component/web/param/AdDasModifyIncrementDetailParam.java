package com.kuaishou.ad.das.platform.component.web.param;

import java.io.Serializable;

import lombok.Data;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-12-24
 */
@Data
public class AdDasModifyIncrementDetailParam implements Serializable {

    private Long id;

    private String pbClass;

    private String mainTable;

}
