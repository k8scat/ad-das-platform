package com.kuaishou.ad.das.platform.component.web.view;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-11-23
 */
@Data
public class AdDasFinanceDailySnapshotView {

    private Long id;

    private long accountId;

    private long agentId;

    private String date;

    private long chargeDate;

    @ApiModelProperty("现金实际消耗")
    private long realCharged;

    @ApiModelProperty("后返实际消耗")
    private long rebateRealCharged;

    @ApiModelProperty("前返实际消耗")
    private long preRebateRealCharged;

    @ApiModelProperty("激励账号实际消耗")
    private long directRebateRealCharged;

    @ApiModelProperty("框返实际消耗")
    private long contractRebateRealCharged;

    @ApiModelProperty("信用账户实际消耗")
    private long creditRealCharged;

    @ApiModelProperty("预留账户充值金额")
    private long extendedRealCharged;

    @ApiModelProperty("现金充值金额")
    private long recharge;

    @ApiModelProperty("后返充值金额")
    private long rebateRecharge;

    @ApiModelProperty("前返充值金额")
    private long preRebateRecharge;

    @ApiModelProperty("激励账号充值金额")
    private long directRebateRecharge;

    @ApiModelProperty("框返充值金额")
    private long contractRebateRecharge;

    @ApiModelProperty("信用账户充值金额")
    private long creditRecharge;

    @ApiModelProperty("预留账户充值金额")
    private long extendedRecharge;

    @ApiModelProperty("现金")
    private long balance;

    @ApiModelProperty("后返")
    private long rebate;

    @ApiModelProperty("前返")
    private long preRebate;

    @ApiModelProperty("激励账号")
    private long directRebate;

    @ApiModelProperty("框返")
    private long contractRebate;

    @ApiModelProperty("信用账户")
    private long creditBalance;

    @ApiModelProperty("预留账户")
    private long extendedBalance;
}
