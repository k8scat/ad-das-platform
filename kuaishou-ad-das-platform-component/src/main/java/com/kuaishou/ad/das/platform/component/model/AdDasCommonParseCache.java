package com.kuaishou.ad.das.platform.component.model;

import static com.kuaishou.ad.das.platform.utils.constant.AdDasCommonConstant.TABLENAME_2_PBCLASSNAME_MAP;
import static com.kuaishou.ad.das.platform.utils.kconf.AdDasPlatformKconf.adDasPlatformLoadClassFromCache;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import com.google.common.cache.LoadingCache;
import com.kuaishou.ad.das.platform.component.process.AdDasBenchmarkCommonProcess;
import com.kuaishou.ad.das.platform.core.common.pbloader.AdDasApiPbLoader;
import com.kuaishou.ad.das.platform.utils.HdfsUtils;
import com.kuaishou.framework.concurrent.AsyncReloadCacheLoader;
import com.kuaishou.infra.framework.common.util.KsCacheBuilder;

import lombok.extern.slf4j.Slf4j;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-08-23
 */
@Lazy
@Slf4j
@Service
public class AdDasCommonParseCache {

    @Autowired
    private AdDasApiPbLoader adDasApiPbLoader;

    private final LoadingCache<AdDasParseParam, List<Object>> commonDataCache = KsCacheBuilder.newBuilder()
            .enableAutoCleanup()
            .maximumSize(100)
            .expireAfterAccess(20, TimeUnit.MINUTES)
            .refreshAfterWrite(20, TimeUnit.MINUTES)
            .build(new AsyncReloadCacheLoader<AdDasParseParam, List<Object>>() {

                @Override
                public List<Object> load(AdDasParseParam adDasParseParam) throws Exception {
                    return reloadData(adDasParseParam);
                }
            });

    /**
     * 查询所有数据
     * @param adDasParseParam
     * @return
     * @throws Exception
     */
    public List<Object> getLists(AdDasParseParam adDasParseParam) throws Exception {
        return commonDataCache.get(adDasParseParam);
    }

    /**
     * 绕过本地缓存查询
     * @param adDasParseParam
     * @return
     * @throws Exception
     */
    public List<Object> reloadData(AdDasParseParam adDasParseParam) throws Exception {

        String className = TABLENAME_2_PBCLASSNAME_MAP.get(adDasParseParam.getTable());
        AdDasBenchmarkCommonProcess process = new AdDasBenchmarkCommonProcess();

        Class cl = Class.forName(className);
        // 切换从 reload Cache 中加载 class
        if (adDasPlatformLoadClassFromCache.get()) {
            cl = adDasApiPbLoader.loadClassFromCache(className);
        }

        HdfsUtils.processPathWithPoto(adDasParseParam.getHdfsPath(), process, cl);

        List<Object> datas = process.getDataList();
        return datas;
    }
}
