package com.kuaishou.ad.das.platform.component.biz.increment;

import com.kuaishou.ad.das.platform.component.web.param.AdDasEditIncrementServiceParam;
import com.kuaishou.ad.das.platform.component.web.param.AdDasEditIncrementStreamParam;
import com.kuaishou.ad.das.platform.component.web.param.AdDasPlatformIncreModifyParam;

import com.kuaishou.ad.das.platform.utils.model.AdDasUserView;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-12-23
 */
public interface AdDasIncrementModifyModBiz {

    /**
     * 增量流变更
     *
     */
    void increModify(AdDasPlatformIncreModifyParam increModifyParam, AdDasUserView adDasUserView);

    /**
     * 审批增量变更
     */
    void approveModifyIncrement(Long modifyId, Integer approveType, String reason, AdDasUserView adDasUserView);

    /**
     * 发布增量变更
     */
    void publishModifyIncrement(Long modifyId, AdDasUserView adDasUserView);

    /**
     * 终止增量变更
     */
    void terminateModifyIncrement(Long modifyId, AdDasUserView adDasUserView);

    /**
     * 回滚增量变更
     */
    void rollbackModifyIncrement(Long modifyId, AdDasUserView adDasUserView);

    void mod(AdDasEditIncrementStreamParam param);

    void deleteById(Long dataId);

    void modServiceInfo(AdDasEditIncrementServiceParam param);

    void deleteServiceInfoById(Long dataId);
}
