package com.kuaishou.ad.das.platform.component.web.param;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author wangkai <wangkai14@kuaishou.com>
 * Created on 2020-12-24
 */
@ApiModel
@Data
public class AdDasBenchmarkDevelopTaskExecuteParam implements Serializable {

    private static final long serialVersionUID = -600867087659307617L;

    @ApiModelProperty(value = "任务运行名称")
    private String taskRunName;

    @ApiModelProperty(value = "结果输出目录类型")
    private Integer outputDirectoryType;

    @ApiModelProperty(value = "结果输出路径")
    private String resultOutputPath;

    @ApiModelProperty(value = "任务运行时间戳")
    private Long taskRunTimeStamp;

    @ApiModelProperty(value = "任务id列表")
    private String taskIdList;

}
