package com.kuaishou.ad.das.platform.component.web.param;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author wangkai <wangkai14@kuaishou.com>
 * Created on 2020-12-24
 */
@ApiModel
@Data
public class AdDasBenchmarkDevelopHomeParam extends AdDasBenchmarkManageHomeParam implements Serializable {

    private static final long serialVersionUID = 3265228120809116493L;

    @ApiModelProperty(value = "任务状态")
    private Integer taskStatus;

}
