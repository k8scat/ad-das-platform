package com.kuaishou.ad.das.platform.component.web.param;

import java.io.Serializable;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-08-01
 */
@Data
public class AdDasEditIncrementStreamParam implements Serializable {

    private static final long serialVersionUID = 4595686586332018557L;

    @ApiModelProperty(value = "增量id", allowEmptyValue = true)
    private Long id;

    @ApiModelProperty(value = "增量流 code", allowEmptyValue = true)
    private String incrementCode;

    @ApiModelProperty(value = "增量流集群名称", allowEmptyValue = true)
    private String incrementName;

    @ApiModelProperty(value = "增量流描述", allowEmptyValue = true)
    private String incrementDesc;

    @ApiModelProperty(value = "数据源", allowEmptyValue = true)
    private String dataSource;

    @ApiModelProperty(value = "增量消费组", allowEmptyValue = true)
    private String consumerGroup;

    @ApiModelProperty(value = "数据流组id", allowEmptyValue = true)
    private Long dataStreamGroupId;

    @ApiModelProperty(value = "增量流topic", allowEmptyValue = true)
    private String topic;

    @ApiModelProperty(value = "操作人", allowEmptyValue = true)
    private String operatorEmailPrefix;

    @ApiModelProperty(value = "是否删除", allowEmptyValue = true)
    private Integer isDelete;

    @ApiModelProperty(value = "增量流类型: 1-online 2-preOnline", allowEmptyValue = true)
    private Integer increStreamType;

    @ApiModelProperty(value = "增量流关联Id, 当increStream=2时, 代表关联的online流id", allowEmptyValue = true)
    private Long increStreamRelatedId;
}
