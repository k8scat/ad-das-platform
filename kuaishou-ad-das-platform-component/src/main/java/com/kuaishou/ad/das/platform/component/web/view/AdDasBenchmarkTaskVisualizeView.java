package com.kuaishou.ad.das.platform.component.web.view;

import java.io.Serializable;
import java.util.List;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-08-02
 */
@Data
public class AdDasBenchmarkTaskVisualizeView extends AdDasBenchmarkServiceStatusView implements Serializable {

    private static final long serialVersionUID = -1766666382306640225L;

    @ApiModelProperty(value = "任务总数")
    private Integer taskTotal;

    @ApiModelProperty(value = "任务运行中数量")
    private Integer taskRunningCount;

    @ApiModelProperty(value = "任务运行成功数量")
    private Integer taskRunSucCount;

    @ApiModelProperty(value = "任务运行失败数量")
    private Integer taskRunFailedCount;

    @ApiModelProperty(value = "任务状态列表")
    private List<AdDasBenchmarkTaskDetailView> taskDetailViews;
}
