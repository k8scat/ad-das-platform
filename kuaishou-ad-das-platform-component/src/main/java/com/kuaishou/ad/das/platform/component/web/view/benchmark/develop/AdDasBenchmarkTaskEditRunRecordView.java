package com.kuaishou.ad.das.platform.component.web.view.benchmark.develop;

import java.io.Serializable;

import lombok.Data;

/**
 * @author wangkai <wangkai14@kuaishou.com>
 * Created on 2020-12-24
 */
@Data
public class AdDasBenchmarkTaskEditRunRecordView implements Serializable {

    private static final long serialVersionUID = 2495385163564800663L;

    private String taskRunName;

    private Long taskRunRecordId;

    private String resultOutputPath;

    private String taskRunStatus;

    private String operator;

    private Long createTime;

    private Long updateTime;
}
