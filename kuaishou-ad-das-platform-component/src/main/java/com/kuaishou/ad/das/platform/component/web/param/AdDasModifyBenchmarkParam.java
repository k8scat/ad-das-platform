package com.kuaishou.ad.das.platform.component.web.param;

import java.io.Serializable;

import com.kuaishou.ad.das.platform.core.benchmark.model.AdDasBenchmarkMainTableView;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-11-24
 */
@Data
@ApiModel
public class AdDasModifyBenchmarkParam implements Serializable {

    private static final long serialVersionUID = -6568036801933104909L;

    @ApiModelProperty(value = "变更id")
    private Long modifyId;

    @ApiModelProperty(value = "变更名称")
    private String modifyName;

    @ApiModelProperty(value = "变更类型")
    private Integer modifyType;

    @ApiModelProperty(value = "变更状态")
    private Integer modifyStatus;

    private AdDasBenchmarkMainTableView adDasBenchmarkMainTableData;

    @ApiModelProperty(value = "是否为表下线操作", notes = "当该字段为tru时，不再读取adDasBenchmarkMainTableData字段")
    private Boolean deleteTable;

    @ApiModelProperty(value = "驳回原因")
    private String reason;

    @ApiModelProperty(value = "审批人")
    private String approver;
}
