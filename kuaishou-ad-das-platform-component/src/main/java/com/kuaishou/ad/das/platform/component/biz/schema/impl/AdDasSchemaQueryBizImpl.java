package com.kuaishou.ad.das.platform.component.biz.schema.impl;

import static com.kuaishou.ad.das.platform.core.common.pbloader.AdDasClassLoaderHelper.getPbClassPrefix;
import static com.kuaishou.ad.das.platform.utils.constant.AdDasCommonConstant.PB_ENUM_ADINSTANCE_STR;
import static com.kuaishou.ad.das.platform.utils.exception.ErrorCode.DATA_NOT_FOUND;
import static com.kuaishou.ad.das.platform.utils.kconf.AdDasPlatformKconf.AD_DAS_DATASOURCE_DB_MAPPING;
import static com.kuaishou.ad.das.platform.utils.kconf.AdDasPlatformKconf.adDasPlatformDataAdapterMap;
import static com.kuaishou.ad.das.platform.utils.kconf.AdDasPlatformKconf.adDasPlatformProtoEnumVersionConfig;

import java.net.MalformedURLException;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.compress.utils.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import com.google.common.collect.Sets;
import com.kuaishou.ad.das.platform.component.biz.common.AdDasCommonBiz;
import com.kuaishou.ad.das.platform.component.biz.schema.AdDasSchemaQueryBiz;
import com.kuaishou.ad.das.platform.component.web.view.AdDasSchemaHomePageView;
import com.kuaishou.ad.das.platform.component.web.view.AdDasSchemaPbClassView;
import com.kuaishou.ad.das.platform.component.web.view.AdDasSchemaPbDetailView;
import com.kuaishou.ad.das.platform.component.web.view.AdDasSchemaPbPreviewView;
import com.kuaishou.ad.das.platform.component.web.view.AdDasSchemaPbView;
import com.kuaishou.ad.das.platform.component.web.view.AdDasSchemaServiceView;
import com.kuaishou.ad.das.platform.component.web.view.AdDasStatusView;
import com.kuaishou.ad.das.platform.component.web.view.AdDasTableMsgView;
import com.kuaishou.ad.das.platform.core.common.daszk.Helper.AdDasPlatformCommonZkHelper;
import com.kuaishou.ad.das.platform.core.common.daszk.model.AdDasPlatformProtoSchema;
import com.kuaishou.ad.das.platform.core.common.pbloader.AdDasClassLoaderHelper;
import com.kuaishou.ad.das.platform.dal.model.AdDasPb;
import com.kuaishou.ad.das.platform.dal.model.AdDasPbClass;
import com.kuaishou.ad.das.platform.dal.model.AdDasPbClassColumn;
import com.kuaishou.ad.das.platform.dal.model.AdDasServicePb;
import com.kuaishou.ad.das.platform.dal.model.AdDasTableColumn;
import com.kuaishou.ad.das.platform.dal.model.AdDasTableSchema;
import com.kuaishou.ad.das.platform.dal.repository.AdDasPbClassColumnRepository;
import com.kuaishou.ad.das.platform.dal.repository.AdDasPbClassRepository;
import com.kuaishou.ad.das.platform.dal.repository.AdDasPbRepository;
import com.kuaishou.ad.das.platform.dal.repository.AdDasServicePbRepository;
import com.kuaishou.ad.das.platform.dal.repository.AdDasTableColumnRepository;
import com.kuaishou.ad.das.platform.dal.repository.AdDasTableSchemaRepository;
import com.kuaishou.ad.das.platform.utils.AdDasBeanCoreUtils;
import com.kuaishou.ad.das.platform.utils.dasenum.AdDasStatusEnum;
import com.kuaishou.ad.das.platform.utils.exception.AdDasServiceException;

import lombok.extern.slf4j.Slf4j;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-11-27
 */
@Lazy
@Slf4j
@Service
public class AdDasSchemaQueryBizImpl extends AdDasCommonBiz
        implements AdDasSchemaQueryBiz {

    private static final String pbClassName = "com.kuaishou.ad.model.protobuf.tables.All";

    @Autowired
    private AdDasPbRepository adDasPbRepository;

    @Autowired
    private AdDasPbClassRepository adDasPbClassRepository;

    @Autowired
    private AdDasServicePbRepository adDasServicePbRepository;

    @Autowired
    private AdDasPbClassColumnRepository adDasPbClassColumnRepository;

    @Autowired
    private AdDasTableSchemaRepository adDasTableSchemaRepository;

    @Autowired
    private AdDasTableColumnRepository adDasTableColumnRepository;

    @Autowired
    private AdDasPlatformCommonZkHelper adDasCommonZkHelper;

    @Autowired
    private AdDasClassLoaderHelper adDasClassLoaderHelper;

    @Override
    public AdDasSchemaHomePageView schemaHomePage() {
        AdDasSchemaHomePageView result = new AdDasSchemaHomePageView();
        // 查询 ad_das_pb 获取最新 pb包信息
        AdDasPb adDasPb = adDasPbRepository.queryLastPb();
        log.info("schemaHomePage() adDasPb={}", adDasPb);

        if (adDasPb == null) {
            return result;
        }
        String pbVersion = adDasPb.getPbVersion();
        result.setPbJarVersion(pbVersion);
        // 查询 ad_das_service_pb 获取服务&pb包信息
        List<AdDasServicePb> adDasServicePbs = adDasServicePbRepository
                .queryCurService(AdDasStatusEnum.DEFAULT.getCode());
        if (CollectionUtils.isEmpty(adDasServicePbs)) {
            return result;
        }
        List<AdDasSchemaServiceView> views = adDasServicePbs.stream()
                .map(this::convertServiceView).collect(Collectors.toList());

        result.setSchemaServiceViews(views);
        return result;
    }

    private AdDasSchemaServiceView convertServiceView(AdDasServicePb adDasServicePb) {
        AdDasSchemaServiceView adDasSchemaServiceView = new AdDasSchemaServiceView();
        AdDasBeanCoreUtils.copyPropertiesIgnoreNull(adDasSchemaServiceView, adDasServicePb);
        adDasSchemaServiceView.setPbJarVersion(adDasServicePb.getPbVersion());
        return adDasSchemaServiceView;
    }

    @Override
    public AdDasSchemaPbView querySchemaPb(String pbJarVersion, String pbClass, Integer pbStatus) {
        AdDasSchemaPbView result = new AdDasSchemaPbView();
        if (pbStatus == 0) {
            pbStatus = null;
        }
        // 查询count
        int count = adDasPbClassRepository.count(pbJarVersion, pbClass, pbStatus);
        result.setPbClassCount(count);
        if (count == 0) {
            return result;
        }
        // 查询 ad_das_pb_class 表获取指定版本标记的 class 数据
        List<AdDasPbClass> adDasPbClassList = adDasPbClassRepository
                .queryLikePbClass(pbJarVersion, pbClass, pbStatus);
        List<AdDasSchemaPbDetailView> schemaPbDetailViews = adDasPbClassList.stream()
                .map(this::convertSchemaPbView).collect(Collectors.toList());
        result.setSchemaPbDetailViews(schemaPbDetailViews);
        result.setPbJarVersion(pbJarVersion);
        return result;
    }

    @Override
    public String queryLatestSchemaPb() {
        // 查询 ad_das_pb 获取最新 pb包信息
        AdDasPb adDasPb = adDasPbRepository.queryLastPb();
        if (adDasPb != null ) {
            return adDasPb.getPbVersion();
        }
        return "";
    }

    private AdDasSchemaPbDetailView convertSchemaPbView(AdDasPbClass adDasPbClass) {
        AdDasSchemaPbDetailView adDasSchemaPbDetailView = new AdDasSchemaPbDetailView();
        AdDasBeanCoreUtils.copyPropertiesIgnoreNull(adDasSchemaPbDetailView, adDasPbClass);
        adDasSchemaPbDetailView.setPbClassName(adDasPbClass.getPbClass());
        adDasSchemaPbDetailView.setPbStatus(adDasPbClass.getPbClassStatus());
        adDasSchemaPbDetailView.setPbClassColumnCount(adDasPbClass.getPbClassColumnCount());

        return adDasSchemaPbDetailView;
    }

    @Override
    public List<AdDasSchemaPbClassView> querySchemaPbClass(String pbJarVersion, String pbClassName) {
        List<AdDasSchemaPbClassView> result = Lists.newArrayList();
        try {

            AdDasPlatformProtoSchema adDasPlatformProtoSchema = getProtoJarVersion(pbJarVersion);

            // 反射解析对应的 PbClass 字段信息
            String pbClass = getPbClassPrefix() + pbClassName;
            Class<?> clz = adDasClassLoaderHelper.loadClass(adDasPlatformProtoSchema, pbClass);
            // 反射解析指定PB版本的class 数据
            result = reflectPbClassView(clz);

        } catch (Exception e) {
            log.error("querySchemaPbClass() occur error! pbJarVersion={}", pbJarVersion, e);
        }

        if (CollectionUtils.isEmpty(result)) {
            return result;
        }

        // 对比DB 中标记的字段数据, 处理 marked 字段值
        List<AdDasPbClassColumn> adDasPbClassColumns = adDasPbClassColumnRepository.query(pbJarVersion, pbClassName);
        if (CollectionUtils.isEmpty(result)) {
            result.forEach(adDasSchemaPbClassView -> adDasSchemaPbClassView.setMarked(0));
        } else {
            Set<String> pbColumnSets = adDasPbClassColumns.stream()
                    .map(AdDasPbClassColumn::getPbColumn).collect(Collectors.toSet());
            result.forEach(adDasSchemaPbClassView -> {
                if (pbColumnSets.contains(adDasSchemaPbClassView.getColumnName())) {
                    adDasSchemaPbClassView.setMarked(1);
                } else {
                    adDasSchemaPbClassView.setMarked(0);
                }
            });
        }
        return result;
    }

    @Override
    public List<AdDasSchemaPbClassView> querySchemaPbClassFromDb(String pbJarVersion, String pbClassName) {
        List<AdDasSchemaPbClassView> result = Lists.newArrayList();

        List<AdDasPbClassColumn>  dasPbClassColumns = adDasPbClassColumnRepository
                .query(pbJarVersion, pbClassName);

        if (!CollectionUtils.isEmpty(dasPbClassColumns)) {
            dasPbClassColumns.stream()
                    .forEach(adDasPbClassColumn -> {
                        AdDasSchemaPbClassView adDasSchemaPbClassView = new AdDasSchemaPbClassView();
                        adDasSchemaPbClassView.setColumnName(adDasPbClassColumn.getPbColumn());
                        adDasSchemaPbClassView.setColumnFullName(adDasPbClassColumn.getPbFullColumn());
                        adDasSchemaPbClassView.setColumnEnumType(adDasPbClassColumn.getEnumType());
                        adDasSchemaPbClassView.setColumnJavaType(adDasPbClassColumn.getPbJavaType());
                        adDasSchemaPbClassView.setColumnPbType(adDasPbClassColumn.getPbType());
                        adDasSchemaPbClassView.setDefaultValue(adDasPbClassColumn.getDefaultValue());
                        adDasSchemaPbClassView.setMarked(1);

                        result.add(adDasSchemaPbClassView);
                    });
        }

        return result;
    }

    @Override
    public List<AdDasSchemaPbPreviewView> previewPbJar(String pbJarVersion) {

        List<AdDasSchemaPbPreviewView> result = Lists.newArrayList();

        try {
            AdDasPlatformProtoSchema adDasPlatformProtoSchema = getProtoJarVersion(pbJarVersion);
            log.info("queryClassList() adDasPlatformProtoSchema={}", adDasPlatformProtoSchema);
            // 查询All类下面的 所有子类
            Class<?>[] subClazzArr = findAllClass(adDasPlatformProtoSchema);
            log.info("previewPbJar() subClazzArr={}", subClazzArr);

            // 查询DB中当前最新版本 标记接入的PB类
            Set<String> latestPbClassSet = Sets.newHashSet();
            String latestPbVersion = queryLatestSchemaPb();
            log.info("previewPbJar() latestPbVersion={}", latestPbVersion);
            if (!StringUtils.isEmpty(latestPbVersion)) {
                List<AdDasPbClass> adDasPbClasses = adDasPbClassRepository
                        .query(latestPbVersion, AdDasStatusEnum.DEFAULT.getCode());

                if (!CollectionUtils.isEmpty(adDasPbClasses)) {
                    adDasPbClasses.forEach(adDasPbClass -> latestPbClassSet.add(adDasPbClass.getPbClass()));
                }
            }

            for (Class<?> subClazz : subClazzArr) {
                try {
                    if (!subClazz.isInterface()) {
                        AdDasSchemaPbPreviewView adDasSchemaPbPreviewView = new AdDasSchemaPbPreviewView();

                        String classSimpleName = subClazz.getSimpleName();
                        adDasSchemaPbPreviewView.setPbClassName(classSimpleName);
                        adDasSchemaPbPreviewView.setPbClassFullName(subClazz.toString());
                        // 反射解析指定PB版本的class 数据
                        List<AdDasSchemaPbClassView> adDasSchemaPbClassViews = reflectPbClassView(subClazz);
                        adDasSchemaPbPreviewView.setAdDasSchemaPbClassViews(adDasSchemaPbClassViews);
                        adDasSchemaPbPreviewView.setColumnCount(adDasSchemaPbClassViews.size());

                        if (latestPbClassSet.contains(classSimpleName)) {
                            adDasSchemaPbPreviewView.setMarked(1);
                        }
                        result.add(adDasSchemaPbPreviewView);
                    }
                } catch (Exception e) {
                    log.error("previewPbJar() occur error! subClazz={}", subClazz, e);
                }
            }
        } catch (Exception e) {
            log.error("previewPbJar() occur error! pbJarVersion={}", pbJarVersion, e);
        }

        return result;
    }

    private Class[] findAllClass(AdDasPlatformProtoSchema adDasPlatformProtoSchema) throws MalformedURLException, ClassNotFoundException {

        Class clz = adDasClassLoaderHelper.loadClass(adDasPlatformProtoSchema, "com.kuaishou.ad.model.protobuf.tables.All");
        log.info("findAllClass() clz={}", clz);

        Class[] result = clz.getClasses();
        log.info("findAllClass() result={}", Arrays.stream(result).toArray());

        // 查询All类下面的 所有子类
        return result;
    }

    @Override
    public List<String> queryClassList(String pbJarVersion) {

        List<String> result = Lists.newArrayList();
        try {
            AdDasPlatformProtoSchema adDasPlatformProtoSchema = getProtoJarVersion(pbJarVersion);
            log.info("queryClassList() adDasPlatformProtoSchema={}", adDasPlatformProtoSchema);
            // 查询All类下面的 所有子类
             Class<?>[] subClazzArr = findAllClass(adDasPlatformProtoSchema);

            for (Class<?> subClazz : subClazzArr) {
                if (!subClazz.isInterface()) {
                    log.info("queryClassList() subClazz={}", subClazz);
                    String classSimpleName = subClazz.getSimpleName();
                    result.add(classSimpleName);
                }
            }
        } catch (Exception e) {
            log.error("queryClassList() occur error! pbJarVersion={}", pbJarVersion, e);
        }
        return result;
    }

    @Override
    public List<String> queryServiceList(String serviceType) {
        List<String> result = Lists.newArrayList();
        try {
            if (!StringUtils.isEmpty(serviceType)) {
                result.addAll(adDasCommonZkHelper.queryServiceNameByType(serviceType));
            } else {
                // 查询所有服务
                result.addAll(adDasCommonZkHelper.queryServiceNameByType("ad.das.api"));
                result.addAll(adDasCommonZkHelper.queryServiceNameByType("ad.das.base"));
                result.addAll(adDasCommonZkHelper.queryServiceNameByType("ad.das.increment"));
            }
        } catch (Exception e) {
            log.error("queryServiceList() occur error! serviceType={}", serviceType, e);
        }
        log.info("queryServiceList() serviceType={}, result={}", serviceType, result);
        return result;
    }

    @Override
    public String queryServicePb(String serviceName) {

        // 查询 ad_das_service_pb 信息，查询指定服务对应的PB包版本信息
        String result = "";
        List<AdDasServicePb> adDasServicePbs = adDasServicePbRepository
                .queryByService(serviceName, AdDasStatusEnum.DEFAULT.getCode());
        if (CollectionUtils.isEmpty(adDasServicePbs)) {
            return result;
        }
        result = adDasServicePbs.stream()
                .findFirst().map(AdDasServicePb::getPbVersion)
                .orElse("");
        return result;
    }

    @Override
    public List<String> queryPbVersions() {
        List<AdDasPb> adDasPbs = adDasPbRepository.queryAll();
        if (CollectionUtils.isEmpty(adDasPbs)) {
            return Lists.newArrayList();
        }
        return adDasPbs.stream().map(AdDasPb::getPbVersion).collect(Collectors.toList());
    }

    @Override
    public List<String> queryTableAll(String dataSource) {

        String dataSourceFinal = adDasPlatformDataAdapterMap
                .get().getOrDefault(dataSource, dataSource);
        log.info("queryTableAll dataSourceFinal={} dataSource={}", dataSourceFinal, dataSource);

        if (AD_DAS_DATASOURCE_DB_MAPPING.get() == null) {
            return Lists.newArrayList();
        }
        String dbName = AD_DAS_DATASOURCE_DB_MAPPING.get().get(dataSourceFinal);

        if (StringUtils.isEmpty(dbName)) {
            return Lists.newArrayList();
        }
        log.info("queryTableAll dataSource{} dbName {}", dataSourceFinal, dbName);
        List<AdDasTableSchema> tableSchemas = adDasTableSchemaRepository.queryTablesAll(dataSourceFinal, dbName);

        if (CollectionUtils.isEmpty(tableSchemas)) {
            return Lists.newArrayList();
        }
        return tableSchemas.stream().map(AdDasTableSchema::getTableName).collect(Collectors.toList());
    }

    @Override
    public List<String> queryPbAll() {
        // 查询 ad_das_pb 获取最新 pb包信息
        AdDasPb adDasPb = adDasPbRepository.queryLastPb();
        log.info("schemaHomePage() adDasPb={}", adDasPb);
        if (adDasPb == null) {
            throw AdDasServiceException.ofMessage(DATA_NOT_FOUND.getCode(), DATA_NOT_FOUND.getMsg());
        }
        String pbVersion = adDasPb.getPbVersion();
        List<AdDasPbClass> adDasPbClasses = adDasPbClassRepository
                .query(pbVersion, AdDasStatusEnum.DEFAULT.getCode());
        return adDasPbClasses.stream()
                .map(AdDasPbClass::getPbClass)
                .collect(Collectors.toList());
    }

    @Override
    public List<String> queryPbEnum() {

        // 查询 ad_das_pb 获取最新 pb包信息
        AdDasPb adDasPb = adDasPbRepository.queryLastPb();
        log.info("schemaHomePage() adDasPb={}", adDasPb);
        if (adDasPb == null) {
            throw AdDasServiceException.ofMessage(DATA_NOT_FOUND.getCode(), DATA_NOT_FOUND.getMsg());
        }
        String pbEnumVersion = adDasPb.getPbEnumVersion();

        List<String> pbClassEnums = Lists.newArrayList();
        try {

            AdDasPlatformProtoSchema adDasPlatformProtoSchema = new AdDasPlatformProtoSchema();
            String lastestPbVersion = adDasPb.getPbVersion();
            String lastestPbEnumVersion = adDasPb.getPbEnumVersion();
            adDasPlatformProtoSchema.setPbVersion(lastestPbVersion);
            adDasPlatformProtoSchema.setPbEnumVersion(lastestPbEnumVersion);
            log.info("AdDasIncrementServiceModelCacher defaultLoadPb() adDasPlatformProtoSchema={}", adDasPlatformProtoSchema);

            Class enumCls = adDasClassLoaderHelper.loadEnumClass(adDasPlatformProtoSchema, PB_ENUM_ADINSTANCE_STR);

            Object[] enumClassList = enumCls.getEnumConstants();
            for (Object enumClass : enumClassList) {
                log.info("enumClass={}", enumClass.getClass() + ":" + enumClass.toString());
                pbClassEnums.add(enumClass.toString());
            }
        } catch (Exception e) {
            log.error("queryPbEnum() ocurr error! pbEnumVersion={}, PB_ENUM_ADINSTANCE_STR={}",
                    pbEnumVersion, PB_ENUM_ADINSTANCE_STR, e);
        }

        return pbClassEnums;
    }

    @Override
    public List<AdDasStatusView> queryPbStatus() {
        return Arrays.stream(AdDasStatusEnum.values()).map(this::buildStatusView).collect(Collectors.toList());
    }

    @Override
    public List<AdDasTableMsgView> queryTableMsg(String dataSource, String tableName) {
        List<AdDasTableColumn> tableColumns = adDasTableColumnRepository.queryTableColumnAll(dataSource, tableName);
        List<AdDasTableMsgView> tableMsgViews = Lists.newArrayList();
        if (CollectionUtils.isEmpty(tableColumns)) {
            return tableMsgViews;
        }
        tableColumns.forEach(adDasTableColumn -> tableMsgViews.add(
                new AdDasTableMsgView(adDasTableColumn.getFieldName(), adDasTableColumn.getType())
        ));
        return tableMsgViews;
    }

    private AdDasStatusView buildStatusView(AdDasStatusEnum adDasStatusEnum) {
        AdDasStatusView adDasStatusView = new AdDasStatusView();
        adDasStatusView.setStatusCode(adDasStatusEnum.getCode());
        adDasStatusView.setStatusName(adDasStatusEnum.getDesc());
        return adDasStatusView;
    }

    private AdDasPlatformProtoSchema getProtoJarVersion(String pbVersion) {
        AdDasPlatformProtoSchema adDasPlatformProtoSchema = new AdDasPlatformProtoSchema();
        adDasPlatformProtoSchema.setPbVersion(pbVersion);
        adDasPlatformProtoSchema.setPbEnumVersion(adDasPlatformProtoEnumVersionConfig.get());

        return adDasPlatformProtoSchema;
    }
}
