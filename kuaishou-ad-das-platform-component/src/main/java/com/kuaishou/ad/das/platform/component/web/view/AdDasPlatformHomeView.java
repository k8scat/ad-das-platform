package com.kuaishou.ad.das.platform.component.web.view;

import java.io.Serializable;
import java.util.List;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-08-02
 */
@Data
public class AdDasPlatformHomeView implements Serializable {

    private static final long serialVersionUID = 3264004443144814626L;

    @ApiModelProperty(value = "oncall名称")
    private String oncallName;

    @ApiModelProperty(value = "changeLog")
    private List<String> changeLogs;

}
