package com.kuaishou.ad.das.platform.component.web.view;

import java.util.List;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-05-24
 */
@Data
public class AdDasPlatformIncreDetailView {

    @ApiModelProperty(value = "数据流组id")
    private Long dataStreamGroupId;

    @ApiModelProperty(value = "数据流组名称")
    private String dataStreamGroup;

    @ApiModelProperty(value = "增量流id")
    private Long increStreamId;

    @ApiModelProperty(value = "增量流名称")
    private String increStreamName;

    @ApiModelProperty(value = "增量流code")
    private String increStreamDesc;

    @ApiModelProperty(value = "增量流类型: 1-online 2-preOnline")
    private Integer increStreamType;

    @ApiModelProperty(value = "增量流关联Id, 当increStream=1时, 代表preOnline流id")
    private Long increStreamRelatedId;

    @ApiModelProperty(value = "增量流图详情")
    private AdDasPlatformIncrementGraphView incrementGraphView;

    @ApiModelProperty(value = "增量流表详情")
    private List<AdDasPlatformIncrementTableView> incrementTableViews;
}
