package com.kuaishou.ad.das.platform.component.web.view;

import java.io.Serializable;
import java.util.List;

import lombok.Data;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-10-09
 */
@Data
public class AdDasBenchmarkFileDatasView implements Serializable {

    private static final long serialVersionUID = -6557743435013922202L;

    private List<AdDasBenchmarkFileData> dataList;

}
