package com.kuaishou.ad.das.platform.component.common;


import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

import lombok.Getter;

/**
 * @author wangkai <wangkai14@kuaishou.com>
 * Created on 2021-01-04
 */
@Getter
public enum AdDasTaskRunStatus {
    /**
     * Das任务运行状态
     */
    SUCCESS(0, "运行中"),
    FAILED(1, "成功"),
    RUNNING(2, "失败"),
    CANCELED(3, "已取消");

    private static final Map<Integer,String> MAP = Arrays.stream(AdDasTaskRunStatus.values())
            .collect(Collectors.toMap(AdDasTaskRunStatus::getCode, AdDasTaskRunStatus::getDesc));

    private Integer code;
    private String desc;

    AdDasTaskRunStatus(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public static String getDesc(Integer code) {
        return MAP.get(code);
    }
}