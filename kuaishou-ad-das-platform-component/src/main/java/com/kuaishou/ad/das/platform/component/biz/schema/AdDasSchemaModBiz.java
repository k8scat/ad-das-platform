package com.kuaishou.ad.das.platform.component.biz.schema;


import com.kuaishou.ad.das.platform.component.web.param.AdDasPbClassColumnParam;
import com.kuaishou.ad.das.platform.component.web.view.AdDasSchemaPbDetailView;
import com.kuaishou.ad.das.platform.utils.model.AdDasUserView;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-11-27
 */
public interface AdDasSchemaModBiz {

    /**
     * 更新PB 包版本信息
     * @param pbJarVersion
     */
    void updatePbJar(AdDasUserView adDasUserView, String pbJarVersion,
                     String pbEnumVersion, String pbCommonVersion, Integer autoAddPbClass);

    /**
     * 新增标记 PB 类信息
     * @param pbJarVersion
     * @param pbClassName
     * @return
     */
    AdDasSchemaPbDetailView addPbClass(AdDasUserView adDasUserView, String pbJarVersion, String pbClassName);

    /**
     * 删除指定
     * @param pbJarVersion
     * @param pbClassName
     */
    void updateStatus(AdDasUserView adDasUserView, Long id, String pbJarVersion,
                      String pbClassName,Integer status);

    /**
     * 更新PB 类字段标记
     * @param adDasUserView
     * @param param
     */
    void updatePbClassColumn(AdDasUserView adDasUserView, AdDasPbClassColumnParam param);

}
