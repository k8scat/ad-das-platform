package com.kuaishou.ad.das.platform.component.web.param.benchmark;

import java.util.List;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author Zhang Jialin <zhangjialin@kuaishou.com>
 * Created on 2021-05-18
 */
@Data
public class BenchmarkUpdateParam {
    @ApiModelProperty(value = "基准id")
    private Long benchmarkId;
    @ApiModelProperty(value = "用户组id", notes = "填入当前基准的全部用户组id")
    private List<Long> userGroupIds;
}
