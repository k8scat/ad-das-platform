package com.kuaishou.ad.das.platform.component.model;

import lombok.Data;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-08-23
 */
@Data
public class AdDasParseParam {

    private Long timestamp;

    private String hdfsPath;

    private String table;

}
