package com.kuaishou.ad.das.platform.component.web.view;

import java.io.Serializable;
import java.util.List;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-11-22
 */
@Data
public class AdDasSchemaPbView implements Serializable {

    private static final long serialVersionUID = -5106783760575283492L;

    @ApiModelProperty(value = "当前PB包信息")
    private String pbJarVersion;

    @ApiModelProperty(value = "当前已标记PB类count")
    private Integer pbClassCount;

    @ApiModelProperty(value = "PB数据列表")
    private List<AdDasSchemaPbDetailView> schemaPbDetailViews;
}
