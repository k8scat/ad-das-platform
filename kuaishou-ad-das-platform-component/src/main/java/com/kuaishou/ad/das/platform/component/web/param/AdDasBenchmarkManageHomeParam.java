package com.kuaishou.ad.das.platform.component.web.param;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-11-22
 */
@Data
@ApiModel
public class AdDasBenchmarkManageHomeParam extends AdDasPageSearchParam implements Serializable {

    private static final long serialVersionUID = -1356442794273311826L;

    @ApiModelProperty(value = "基准id")
    private Long benchmarkId;

    @ApiModelProperty(value = "主表名称")
    private String mainTableName;

    @ApiModelProperty(value = "PB类名称")
    private String pbClassName;

    @ApiModelProperty(value = "主数据源")
    private String mainDataSource;

    @ApiModelProperty(value = "任务名称")
    private String taskName;

}
