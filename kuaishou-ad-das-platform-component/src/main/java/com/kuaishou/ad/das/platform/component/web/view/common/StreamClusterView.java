package com.kuaishou.ad.das.platform.component.web.view.common;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author Zhang Jialin <zhangjialin@kuaishou.com>
 * Created on 2021-05-18
 */
@Data
@ApiModel(value = "集群信息")
public class StreamClusterView {
    @ApiModelProperty(value = "集群id")
    private Long clusterId;
    @ApiModelProperty(value = "集群名称")
    private String clusterName;
    @ApiModelProperty(value = "集群URL")
    private String clusterUrl;
}
