package com.kuaishou.ad.das.platform.component.web.param;

import java.io.Serializable;
import java.util.List;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-12-23
 */
@Data
public class AdDasModifyIncrementParam implements Serializable {

    @ApiModelProperty(value = "操作类型：1-新增 2-删除")
    private Integer operationType;

    private List<AdDasModifyIncrementDetailParam> incrementDetailParam;

    private String serviceName;

    /**
     * 变更名称
     */
    private String modifyName;

    private String dataSource;

    @ApiModelProperty(value = "审批人")
    private String approver;

    /**
     * 工单id
     */
    private Long modifyId;

}
