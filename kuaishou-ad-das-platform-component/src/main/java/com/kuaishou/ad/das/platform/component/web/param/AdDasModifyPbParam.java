package com.kuaishou.ad.das.platform.component.web.param;

import java.io.Serializable;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-11-24
 */
@Data
public class AdDasModifyPbParam implements Serializable {

    private static final long serialVersionUID = -220428643563357368L;

    @ApiModelProperty(value = "变更名称")
    private String modifyName;

    @ApiModelProperty(value = "变更类型")
    private Integer modifyType;

    @ApiModelProperty(value = "最新Pb包版本")
    private String lastestPbVersion;

    @ApiModelProperty(value = "最新PbEnum包版本", allowEmptyValue = true)
    private String lastestPbEnumVersion = "";

    @ApiModelProperty(value = "最新PbCommon包版本", allowEmptyValue = true)
    private String lastestPbCommonVersion = "";

    @ApiModelProperty(value = "服务名称")
    private String serviceName;

    @ApiModelProperty(value = "服务当前PB包版本")
    private String curServicePbVersion;

    @ApiModelProperty(value = "服务当前PBEnum包版本", allowEmptyValue = true)
    private String curServicePbEnumVersion;

    @ApiModelProperty(value = "服务当前PBCommon包版本", allowEmptyValue = true)
    private String curServicePbCommonVersion;

    @ApiModelProperty(value = "目标变更版本")
    private String targetPbversion;

    @ApiModelProperty(value = "目标Enum变更版本", allowEmptyValue = true)
    private String targetPbEnumversion = "";

    @ApiModelProperty(value = "目标Common变更版本", allowEmptyValue = true)
    private String targetPbCommonversion = "";

    @ApiModelProperty(value = "git cr链接, 多个cr可 ; 分隔")
    private String crLinks;

    @ApiModelProperty(value = "pipline 打包链接")
    private String piplineLinks;

    @ApiModelProperty(value = "审批人")
    private String approver;
}
