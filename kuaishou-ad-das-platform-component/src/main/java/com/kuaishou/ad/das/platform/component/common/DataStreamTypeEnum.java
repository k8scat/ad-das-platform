package com.kuaishou.ad.das.platform.component.common;

import lombok.Getter;

/**
 * @author Zhang Jialin <zhangjialin@kuaishou.com>
 * Created on 2021-06-14
 */
@Getter
public enum DataStreamTypeEnum {
    /**
     * 基准
     */
    BENCHMARK(1),
    /**
     * 增量
     */
    INCREMENT(2);

    private Integer value;

    DataStreamTypeEnum(Integer value) {
        this.value = value;
    }
}
