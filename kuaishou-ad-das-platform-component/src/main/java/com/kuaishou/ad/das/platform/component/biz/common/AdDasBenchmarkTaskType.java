package com.kuaishou.ad.das.platform.component.biz.common;

import lombok.Getter;

/**
 * @author wangkai <wangkai14@kuaishou.com>
 * Created on 2020-12-30
 */
@Getter
public enum AdDasBenchmarkTaskType {
    /**
     * 基准任务类型
     */
    EDIT(0, "开发任务"),
    ONLINE(1, "正式任务");

    private Integer type;

    private String name;

    AdDasBenchmarkTaskType(Integer type, String name) {
        this.type = type;
        this.name = name;
    }
}
