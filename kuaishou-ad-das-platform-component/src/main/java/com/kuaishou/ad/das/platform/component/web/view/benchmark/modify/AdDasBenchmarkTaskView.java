package com.kuaishou.ad.das.platform.component.web.view.benchmark.modify;

import java.io.Serializable;
import java.util.List;

import com.kuaishou.ad.das.platform.component.web.view.AdDasSchemaPbClassView;
import com.kuaishou.ad.das.platform.core.benchmark.model.AdDasBenchmarkBaseTaskInfoView;
import com.kuaishou.ad.das.platform.core.benchmark.model.AdDasBenchmarkMainTableView;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author wangkai <wangkai14@kuaishou.com>
 * Created on 2020-12-28
 */
@Data
@ApiModel
public class AdDasBenchmarkTaskView implements Serializable {

    private static final long serialVersionUID = 5457487117138646796L;

    @ApiModelProperty(value = "基准表详情信息")
    private AdDasBenchmarkMainTableView mainTableView;

    @ApiModelProperty(value = "基准任务类型： 0-开发任务， 1-正式任务")
    private Integer taskType;

    @ApiModelProperty(value = "proto类字段信息")
    private List<AdDasSchemaPbClassView> pbClassViews;

    @ApiModelProperty(value = "基准任务基本信息")
    private List<AdDasBenchmarkBaseTaskInfoView> adDasBenchmarkBaseTaskInfoViews;

}