package com.kuaishou.ad.das.platform.component.web.view;

import java.io.Serializable;
import java.util.List;

import org.springframework.util.StringUtils;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.kuaishou.ad.das.platform.core.benchmark.model.AdDasBenchmarkCascadeTableView;
import com.kuaishou.ad.das.platform.core.benchmark.model.AdDasBenchmarkDb2PbColumnMsgView;
import com.kuaishou.ad.das.platform.core.benchmark.model.AdDasBenchmarkMainTableView;
import com.kuaishou.ad.das.platform.dal.model.AdDasBenchmark;
import com.kuaishou.ad.das.platform.dal.model.AdDasBenchmarkMsg;
import com.kuaishou.ad.das.platform.dal.model.AdDasDataStreamGroup;
import com.kuaishou.ad.das.platform.dal.model.AdDasModify;
import com.kuaishou.framework.util.ObjectMapperUtils;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-11-24
 */
@Data
public class AdDasModifyBenchmarkDataView implements Serializable {

    private static final long serialVersionUID = -4423311588631900784L;

    private Long modifyId;

    private String modifyName;

    private Integer modifyType;

    private Integer modifyStatus;

    private AdDasBenchmarkMainTableView mainTableView;

    @ApiModelProperty(value = "删除表")
    private Boolean deleteTable;

    private String operator;

    private String approver;

    private String reason;

    public static AdDasModifyBenchmarkDataView build(AdDasModify adDasModify,
            AdDasBenchmarkMsg adDasBenchmarkMsg, AdDasBenchmark benchmark, AdDasDataStreamGroup group) {
        AdDasModifyBenchmarkDataView adDasModifyBenchmarkDataView = new AdDasModifyBenchmarkDataView();
        adDasModifyBenchmarkDataView.setModifyId(adDasModify.getId());
        adDasModifyBenchmarkDataView.setModifyName(adDasModify.getModifyName());
        adDasModifyBenchmarkDataView.setModifyType(adDasModify.getModifyType());
        adDasModifyBenchmarkDataView.setModifyStatus(adDasModify.getModifyStatus());
        adDasModifyBenchmarkDataView.setDeleteTable(adDasBenchmarkMsg.getDeleteTable());
        adDasModifyBenchmarkDataView.setOperator(adDasModify.getOperator());
        adDasModifyBenchmarkDataView.setApprover(adDasModify.getApprover());
        adDasModifyBenchmarkDataView.setReason(adDasModify.getReason());
        AdDasBenchmarkMainTableView mainTableView = new AdDasBenchmarkMainTableView();
        mainTableView.setBenchmarkId(adDasBenchmarkMsg.getBenchmarkId());
        mainTableView.setDataSource(adDasBenchmarkMsg.getDataSource());
        mainTableView.setMainTable(adDasBenchmarkMsg.getMainTable());
        mainTableView.setPbClass(adDasBenchmarkMsg.getPbClass());
        mainTableView.setTableType(adDasBenchmarkMsg.getTableType());
        mainTableView.setPredictGrowth(adDasBenchmarkMsg.getPredictGrowth());
        mainTableView.setCommonSqls(adDasBenchmarkMsg.getCommonSqls());
        mainTableView.setTimeParams(adDasBenchmarkMsg.getTimeParams());
        mainTableView.setShardSqlFlag(adDasBenchmarkMsg.getShardSqlFlag());
        mainTableView.setShardSqlColumn(adDasBenchmarkMsg.getShardSqlColumn());
        mainTableView.setDeleteTable(adDasBenchmarkMsg.getDeleteTable());
        if (benchmark != null && group != null) {
            mainTableView.setBenchmarkName(benchmark.getBenchmarkName());
            mainTableView.setDataStreamGroupName(group.getDataStreamGroupName());
            mainTableView.setDataStreamGroupId(group.getId());
        }

        String cascadeColumnSchema = adDasBenchmarkMsg.getCascadeColumnSchema();
        if (!StringUtils.isEmpty(cascadeColumnSchema)) {
            List<AdDasBenchmarkCascadeTableView> cascadeTableViews = ObjectMapperUtils
                    .fromJSON(cascadeColumnSchema, List.class, AdDasBenchmarkCascadeTableView.class);
            mainTableView.setCascadeTableViews(cascadeTableViews);
        }

        List<AdDasBenchmarkDb2PbColumnMsgView> views = new Gson().fromJson(adDasBenchmarkMsg.getColumnSchema(),
                new TypeToken<List<AdDasBenchmarkDb2PbColumnMsgView>>() {
                }.getType());
        mainTableView.setDb2PbColumnMsgViews(views);
        adDasModifyBenchmarkDataView.setMainTableView(mainTableView);
        return adDasModifyBenchmarkDataView;
    }

}
