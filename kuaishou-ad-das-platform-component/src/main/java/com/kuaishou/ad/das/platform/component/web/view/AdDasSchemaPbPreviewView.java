package com.kuaishou.ad.das.platform.component.web.view;

import java.io.Serializable;
import java.util.List;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-11-22
 */
@Data
public class AdDasSchemaPbPreviewView implements Serializable {

    private static final long serialVersionUID = -7711733352356053878L;

    @ApiModelProperty(value = "pb类名")
    private String pbClassName;

    @ApiModelProperty(value = "pb类全名")
    private String pbClassFullName;

    @ApiModelProperty(value = "字段数量")
    private Integer columnCount;

    @ApiModelProperty(value = "是否新增字段： 1-是 0-否", allowEmptyValue = true)
    private Integer marked = 0;

    @ApiModelProperty(value = "pb类对应的字段明细")
    private List<AdDasSchemaPbClassView> adDasSchemaPbClassViews;
}
