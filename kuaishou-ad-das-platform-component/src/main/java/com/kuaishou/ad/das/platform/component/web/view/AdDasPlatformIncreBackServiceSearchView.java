package com.kuaishou.ad.das.platform.component.web.view;

import java.util.List;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author caoyinhang <caoyinhang@kuaishou.com>
 * Created on 2021-07-06
 */
@Data
public class AdDasPlatformIncreBackServiceSearchView {

    @ApiModelProperty(value = "增量回溯服务结果列表")
    private List<AdDasPlatformIncreBackServiceGraphView> increSearchViews;

    @ApiModelProperty(value = "总数")
    private Integer totalCount;
}
