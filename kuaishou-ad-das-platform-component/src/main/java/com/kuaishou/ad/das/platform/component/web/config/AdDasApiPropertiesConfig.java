package com.kuaishou.ad.das.platform.component.web.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import lombok.Data;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-10-15
 */
@Data
@Configuration
public class AdDasApiPropertiesConfig {

    @Value("${cas.serviceName}")
    private String casServiceName;

}
