package com.kuaishou.ad.das.platform.component.web.view;

import java.io.Serializable;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-08-03
 */
@Data
public class AdDasCommonDataView implements Serializable {

    private static final long serialVersionUID = 5103077011575024742L;

    @ApiModelProperty(value = "数据id")
    private Long dataId;

    @ApiModelProperty(value = "数据描述")
    private String dataDesc;
}
