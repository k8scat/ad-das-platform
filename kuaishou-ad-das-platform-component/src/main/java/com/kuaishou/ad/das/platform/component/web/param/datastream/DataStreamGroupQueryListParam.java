package com.kuaishou.ad.das.platform.component.web.param.datastream;

import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author Zhang Jialin <zhangjialin@kuaishou.com>
 * Created on 2021-05-17
 */
@Data
@ApiModel
public class DataStreamGroupQueryListParam {
    @ApiModelProperty(value = "过滤项-数据流组id", allowEmptyValue = true)
    private List<Long> dataStreamGroupIds;
    @ApiModelProperty(value = "过滤项-数据流组名称", allowEmptyValue = true)
    private String dataStreamGroupName;
    @ApiModelProperty(value = "过滤项-数据源组名称", allowEmptyValue = true)
    private List<String> dataSourceNames;
    @ApiModelProperty(value = "过滤项-增量Topic", allowEmptyValue = true)
    private List<String> incrementTopics;
    @ApiModelProperty(value = "过滤项-权限等级", allowEmptyValue = true)
    private Integer privilegeLevel;
    @ApiModelProperty(value = "过滤项-HDFS路径", allowEmptyValue = true)
    private List<String> hdfsPath;
    @ApiModelProperty(value = "页码", notes = "传-1返回全部", allowEmptyValue = true)
    private Integer pageNum;
    @ApiModelProperty(value = "单页条数", allowEmptyValue = true)
    private Integer pageSize;
}
