package com.kuaishou.ad.das.platform.component.web.view;

import java.io.Serializable;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-11-27
 */
@Data
public class AdDasServiceInstancesView implements Serializable {

    private static final long serialVersionUID = 604293675696753694L;

    @ApiModelProperty(value = "服务名称")
    private String serviceName;

    @ApiModelProperty(value = "服务实例名称")
    private String instanceName;

    @ApiModelProperty(value = "服务当前PB包版本")
    private String curPbVersion;

    @ApiModelProperty(value = "目标PB包版本")
    private String targetPbVersion;

    @ApiModelProperty(value = "发布状态")
    private Integer status;

}
