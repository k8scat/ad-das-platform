package com.kuaishou.ad.das.platform.component.web.view;

import lombok.Data;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-05-24
 */
@Data
public class AdDasPlatformIncrementTableView {

    private String table;

    private String pbEnum;

    private String operator;

    private String approver;

    private Long createTime;

    private Long updateTime;
}
