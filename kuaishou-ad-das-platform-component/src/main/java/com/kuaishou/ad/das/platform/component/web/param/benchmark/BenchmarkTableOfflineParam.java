package com.kuaishou.ad.das.platform.component.web.param.benchmark;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author Zhang Jialin <zhangjialin@kuaishou.com>
 * Created on 2021-05-26
 */
@Data
public class BenchmarkTableOfflineParam {
    @ApiModelProperty(value = "基准id")
    private Long benchmarkId;
    @ApiModelProperty(value = "表id")
    private Long commonDetailId;
}
