package com.kuaishou.ad.das.platform.component.web.param;

import java.io.Serializable;
import java.util.List;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-03-24
 */
@Data
public class AdDasPublishPbParam implements Serializable {

    @ApiModelProperty(value = "变更id")
    private Long modifyId;

    @ApiModelProperty(value = "需要发布的实例")
    private List<String> publishInstances;

    @ApiModelProperty(value = "1-发布, 2-终止, 3-回滚")
    private Integer modifyType;

    private Integer modifyStatus;

}
