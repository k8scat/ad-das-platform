package com.kuaishou.ad.das.platform.component.web.view;

import java.io.Serializable;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-11-22
 */
@Data
public class AdDasSchemaPbDetailView implements Serializable {

    private static final long serialVersionUID = -964162667901690880L;

    @ApiModelProperty(value = "主键id")
    private Long id;

    @ApiModelProperty(value = "PB类名")
    private String pbClassName;

    @ApiModelProperty(value = "PB类字段数")
    private Integer pbClassColumnCount;

    @ApiModelProperty(value = "状态： 1-有效 0-删除")
    private Integer pbStatus;

    @ApiModelProperty(value = "操作人")
    private String operator;

    private Long createTime;

    private Long updateTime;
}
