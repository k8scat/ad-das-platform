package com.kuaishou.ad.das.platform.component.biz.finance.impl;

import static com.google.common.util.concurrent.Uninterruptibles.sleepUninterruptibly;
import static com.kuaishou.ad.das.platform.utils.exception.ErrorCode.ACCOUNTID_OVERSIZE_ERROR;
import static com.kuaishou.ad.das.platform.utils.exception.ErrorCode.PARAM_ERROR;
import static com.kuaishou.ad.das.platform.utils.kconf.AdDasPlatformKconf.AD_DAS_FINANCE_BATCH_QUERY_ACCOUNTID_SIZE;
import static java.util.concurrent.TimeUnit.MILLISECONDS;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.google.common.collect.Lists;
import com.kuaishou.ad.das.platform.component.biz.finance.AdDasFinanceManagerBiz;
import com.kuaishou.ad.das.platform.dal.model.AdDspAccountBalance;
import com.kuaishou.ad.das.platform.dal.repository.AdDasChargeAccountBalanceRepository;
import com.kuaishou.ad.das.platform.utils.exception.AdDasServiceException;
import com.kuaishou.ad.das.platform.utils.model.AdDasUserView;

import lombok.extern.slf4j.Slf4j;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-08-06
 */
@Lazy
@Slf4j
@Service
public class AdDasFinanceManagerBizImpl implements AdDasFinanceManagerBiz {

    @Autowired
    private AdDasChargeAccountBalanceRepository accountBalanceRepository;

    private static final String ACCOUNT_BALANCE_DATA_HEADER_STR = "accountId,balance,contract_rebate,credit_balance," +
            "direct_rebate,extended_balance,pre_rebate,push_balance,rebate\r\n";

    @Override
    public List<byte[]> downloadAccountBalanceData(String accountIds, AdDasUserView adDasUserView) {

        if (StringUtils.isEmpty(accountIds)) {
            throw AdDasServiceException.ofMessage(PARAM_ERROR);
        }

        String[] idStrs = accountIds.trim().split(",");
        List<String> idList = Arrays.asList(idStrs);

        if (CollectionUtils.isEmpty(idList)) {
            log.info("accountIds={}", accountIds);
            return Lists.newArrayList();
        }

        if (idList.size() > AD_DAS_FINANCE_BATCH_QUERY_ACCOUNTID_SIZE.get()) {
            throw AdDasServiceException.ofMessage(ACCOUNTID_OVERSIZE_ERROR);
        }

        List<byte[]> result = Lists.newArrayList();
        result.add(ACCOUNT_BALANCE_DATA_HEADER_STR.getBytes());

        List<Long> accountIdList =  idList.stream()
                .map( idData -> Long.valueOf(idData.trim()))
                .collect(Collectors.toList());

        List<List<Long>> accountIdShardLists = Lists.partition(accountIdList, 200);
        for (List<Long> accountIdShardList : accountIdShardLists) {
            for (Long account : accountIdShardList) {

                AdDspAccountBalance adDspAccountBalance = accountBalanceRepository.queryById(account);
                String balanceStr = adDspAccountBalance.getDataString();
                log.info("balanceStr={}", balanceStr);
                result.add((balanceStr + "\r\n").getBytes());
            }

            sleepUninterruptibly(100, MILLISECONDS);
        }

        return result;
    }
}
