package com.kuaishou.ad.das.platform.component.biz.modify;

import java.util.List;

import com.kuaishou.ad.das.platform.component.web.param.AdDasModifySearchParam;
import com.kuaishou.ad.das.platform.component.web.param.benchmark.BenchmarkModifyQueryParam;
import com.kuaishou.ad.das.platform.component.web.view.AdDasModifyBenchmarkDataView;
import com.kuaishou.ad.das.platform.component.web.view.AdDasModifyDataListView;
import com.kuaishou.ad.das.platform.component.web.view.AdDasModifyIncrementView;
import com.kuaishou.ad.das.platform.component.web.view.AdDasModifyPbView;
import com.kuaishou.ad.das.platform.component.web.view.AdDasModifyTypeView;
import com.kuaishou.ad.das.platform.component.web.view.AdDasServiceInstancesView;
import com.kuaishou.ad.das.platform.component.web.view.AdDasStatusView;
import com.kuaishou.ad.das.platform.component.web.view.benchmark.modify.BenchmarkModifyQueryView;
import com.kuaishou.ad.das.platform.core.benchmark.model.AdDasBenchmarkMainTableView;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-12-06
 */
public interface AdDasModifyQueryBiz {

    List<AdDasStatusView> queryModifyStatusEnum();

    List<AdDasModifyTypeView> queryModifyTypeEnum();

    /**
     * 变更列表首页
     * @param param
     * @return
     */
    AdDasModifyDataListView modifyHome(AdDasModifySearchParam param);

    /**
     * 查询基准变更详情
     * @param modifyId
     * @return
     */
    AdDasModifyBenchmarkDataView benchmarkModifyData(Long modifyId);

    /**
     * 查询审批人
     * @return
     */
    List<String> queryApprover();

    /**
     * 查询PB变更工单详情
     * @param modifyId
     * @param modifyType
     * @return
     */
    AdDasModifyPbView pbModifyData(Long modifyId, Integer modifyType);

    /**
     * 根据服务名 查询对应ZK 节点下的 临时节点的 变更状态
     * @param serviceName
     * @return
     */
    List<AdDasServiceInstancesView> queryServiceInstance(Long modifyId, String serviceName);

    /**
     * 根据主 table 查询接入的详情
     * @param mainTableName
     * @return
     */
    AdDasBenchmarkMainTableView queryBenchmarkByTable(String mainTableName);

    /**
     *
     * @param modifyId
     * @param modifyType
     * @return
     */
    AdDasModifyIncrementView incrementModifyData(Long modifyId, Integer modifyType);

    BenchmarkModifyQueryView queryView(BenchmarkModifyQueryParam param);

}
