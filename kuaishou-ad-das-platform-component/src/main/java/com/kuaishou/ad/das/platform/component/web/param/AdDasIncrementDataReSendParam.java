package com.kuaishou.ad.das.platform.component.web.param;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-12-23
 */
@Data
@ApiModel
public class AdDasIncrementDataReSendParam implements Serializable {

    private static final long serialVersionUID = 886086486673374609L;

    @ApiModelProperty(value = "数据源")
    private String dataSource;

    @ApiModelProperty(value = "主表")
    private String mainTable;

    @ApiModelProperty(value = "主表类型： 0-单表 1-分表")
    private Integer tableType;

    @ApiModelProperty(value = "重发类型： 0-按照id重发 1-按照时间段重发")
    private Integer operateType;

    @ApiModelProperty(value = "重发数据idList", allowEmptyValue = true)
    private String dataIdList;

    @ApiModelProperty(value = "重发开始时间", allowEmptyValue = true)
    private Long startTime;

    @ApiModelProperty(value = "重发结束时间", allowEmptyValue = true)
    private Long endTime;
}
