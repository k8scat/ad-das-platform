package com.kuaishou.ad.das.platform.component.biz.benchmark.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import com.kuaishou.ad.das.platform.component.biz.benchmark.AdDasBenchmarkDevelopBiz;
import com.kuaishou.ad.das.platform.component.common.AdDasTaskRunStatus;
import com.kuaishou.ad.das.platform.component.web.param.AdDasBenchmarkDevelopHomeParam;
import com.kuaishou.ad.das.platform.component.web.param.AdDasBenchmarkDevelopTaskExecuteParam;
import com.kuaishou.ad.das.platform.component.web.view.benchmark.common.AdDasBenchmarkStatusView;
import com.kuaishou.ad.das.platform.component.web.view.benchmark.develop.AdDasBenchmarkDevelopBaseInfoView;
import com.kuaishou.ad.das.platform.component.web.view.benchmark.develop.AdDasBenchmarkDevelopHomeDetailView;
import com.kuaishou.ad.das.platform.component.web.view.benchmark.develop.AdDasBenchmarkDevelopHomeView;
import com.kuaishou.ad.das.platform.component.web.view.benchmark.develop.AdDasBenchmarkDevelopOutputDirectoryView;
import com.kuaishou.ad.das.platform.component.web.view.benchmark.develop.AdDasBenchmarkTaskEditRunRecordView;
import com.kuaishou.ad.das.platform.dal.model.AdDasBenchmarkTaskEdit;
import com.kuaishou.ad.das.platform.dal.model.AdDasBenchmarkTaskEditTestRunRecord;
import com.kuaishou.ad.das.platform.dal.repository.AdDasBenchmarkTaskEditRepository;
import com.kuaishou.ad.das.platform.dal.repository.AdDasBenchmarkTaskEditTestRunRecordRepository;
import com.kuaishou.ad.das.platform.utils.dasenum.AdDasModifyStatusEnum;
import com.kuaishou.ad.das.platform.utils.dasenum.AdDasTaskOutputDirectoryEnum;
import com.kuaishou.ad.das.platform.utils.model.AdDasUserView;

/**
 * @author wangkai <wangkai14@kuaishou.com>
 * Created on 2020-12-31
 */
@Lazy
@Service
public class AdDasBenchmarkDevelopBizImpl implements AdDasBenchmarkDevelopBiz {

    @Autowired
    private AdDasBenchmarkTaskEditRepository adDasBenchmarkTaskEditRepository;

    @Autowired
    AdDasBenchmarkTaskEditTestRunRecordRepository adDasBenchmarkTaskEditTestRunRecordRepository;

    @Override
    public AdDasBenchmarkDevelopBaseInfoView queryBenchmarkBaseInfo() {
        List<AdDasBenchmarkTaskEdit> adDasBenchmarkTaskEdits = adDasBenchmarkTaskEditRepository.getAll();
        AdDasBenchmarkDevelopBaseInfoView view = new  AdDasBenchmarkDevelopBaseInfoView();
        if (CollectionUtils.isEmpty(adDasBenchmarkTaskEdits)) {
            return view;
        }
        Set<String> mainTableNames = new HashSet<>();
        Set<String> pbClassNames = new HashSet<>();
        Set<String> mainDataSources = new HashSet<>();
        adDasBenchmarkTaskEdits.forEach(
                adDasBenchmarkTaskEdit -> {
                    mainTableNames.add(adDasBenchmarkTaskEdit.getMainTable());
                    mainDataSources.add(adDasBenchmarkTaskEdit.getDataSource());
                    pbClassNames.add(adDasBenchmarkTaskEdit.getPbClass());
                }
        );
        view.setMainDataSources(new ArrayList<>(mainDataSources));
        view.setPbClassNames(new ArrayList<>(pbClassNames));
        view.setMainTableNames(new ArrayList<>(mainTableNames));
        List<AdDasBenchmarkStatusView> adDasBenchmarkStatusViews = Arrays.stream(AdDasModifyStatusEnum.values())
                .map(adDasModifyStatusEnum -> {
            AdDasBenchmarkStatusView adDasBenchmarkStatusView = new AdDasBenchmarkStatusView();
            adDasBenchmarkStatusView.setStatusCode(adDasModifyStatusEnum.getCode());
            adDasBenchmarkStatusView.setStatusName(adDasModifyStatusEnum.getDesc());
            return adDasBenchmarkStatusView;
        }).collect(Collectors.toList());
        view.setAdDasBenchmarkStatusViews(adDasBenchmarkStatusViews);
        return view;
    }

    @Override
    public AdDasBenchmarkDevelopHomeView queryAdDasBenchmarkTaskEditList(AdDasBenchmarkDevelopHomeParam param) {
        int limit = param.getPageSize();
        int offset = param.getPageSize() * (param.getPageIndex() - 1);
        long total = adDasBenchmarkTaskEditRepository.getTotalByParams(param.getBenchmarkId(),
                param.getMainDataSource(), param.getPbClassName(), param.getMainTableName(), param.getTaskName(),param.getTaskStatus());
        AdDasBenchmarkDevelopHomeView adDasBenchmarkDevelopHomeView = new AdDasBenchmarkDevelopHomeView();
        if (total == 0) {
            return adDasBenchmarkDevelopHomeView;
        }
        List<AdDasBenchmarkTaskEdit> adDasBenchmarkTaskEdits = adDasBenchmarkTaskEditRepository.getByParams(param.getBenchmarkId(),
                param.getMainDataSource(), param.getPbClassName(),
                param.getMainTableName(), param.getTaskName(), param.getTaskStatus(), offset, limit);
        if(CollectionUtils.isEmpty(adDasBenchmarkTaskEdits)) {
            return adDasBenchmarkDevelopHomeView;
        }
        List<AdDasBenchmarkDevelopHomeDetailView> homeDetailViewList = new ArrayList<>();
        for (AdDasBenchmarkTaskEdit adDasBenchmarkTaskEdit : adDasBenchmarkTaskEdits) {
            AdDasBenchmarkDevelopHomeDetailView developHomeDetailView = new AdDasBenchmarkDevelopHomeDetailView();
            developHomeDetailView.setTaskId(adDasBenchmarkTaskEdit.getId());
            developHomeDetailView.setTaskName(adDasBenchmarkTaskEdit.getTaskName());
            developHomeDetailView.setTaskNum(adDasBenchmarkTaskEdit.getTaskNumber());
            developHomeDetailView.setBenchmarkTypeName("");
            developHomeDetailView.setMainDataSource(adDasBenchmarkTaskEdit.getDataSource());
            developHomeDetailView.setPbClassName(adDasBenchmarkTaskEdit.getPbClass());
            developHomeDetailView.setOperator(adDasBenchmarkTaskEdit.getOperator());
            developHomeDetailView.setCreateTime(adDasBenchmarkTaskEdit.getCreatTime());
            developHomeDetailView.setUpdateTime(adDasBenchmarkTaskEdit.getUpdateTime());
            developHomeDetailView.setTaskStatus(adDasBenchmarkTaskEdit.getTaskStatus());
            developHomeDetailView.setMainTableName(adDasBenchmarkTaskEdit.getMainTable());
            homeDetailViewList.add(developHomeDetailView);
        }
        adDasBenchmarkDevelopHomeView.setHomeDetailViewList(homeDetailViewList);
        adDasBenchmarkDevelopHomeView.setTotalCount(total);
        adDasBenchmarkDevelopHomeView.setPageNum(param.getPageIndex());
        adDasBenchmarkDevelopHomeView.setPageSize(param.getPageSize());
        return adDasBenchmarkDevelopHomeView;
    }

    @Override
    public List<AdDasBenchmarkDevelopOutputDirectoryView> queryOutputDirectory() {
        return Arrays.stream(
                AdDasTaskOutputDirectoryEnum.values()).map(item -> {
            AdDasBenchmarkDevelopOutputDirectoryView adDasBenchmarkDevelopOutputDirectoryView = new AdDasBenchmarkDevelopOutputDirectoryView();
            adDasBenchmarkDevelopOutputDirectoryView.setOutputDirectoryName(item.getDesc());
            adDasBenchmarkDevelopOutputDirectoryView.setOutputDirectoryType(item.getCode());
            return adDasBenchmarkDevelopOutputDirectoryView;
        }).collect(Collectors.toList());
    }

    @Override
    public String queryOutputDirectoryPath(AdDasUserView adDasUserView, Integer outputDirectoryType) {
        return "";
    }

    @Override
    public void taskExecute(AdDasUserView adDasUserView, AdDasBenchmarkDevelopTaskExecuteParam param) {
        AdDasBenchmarkTaskEditTestRunRecord item = new AdDasBenchmarkTaskEditTestRunRecord();
        String taskNumList = Arrays.stream(param.getTaskIdList().split(",")).map(
                taskId -> adDasBenchmarkTaskEditRepository.getByTaskId(Long.parseLong(taskId)).getTaskNumber()
        ).collect(Collectors.toList()).stream().map(String::valueOf).collect(Collectors.joining(","));
        item.setTaskRunName(param.getTaskRunName());
        item.setResultPath(param.getResultOutputPath());
        item.setRunTimestamp(param.getTaskRunTimeStamp());
        item.setTaskNumList(taskNumList);
        item.setTaskIdList(param.getTaskIdList());
        item.setTaskRunStatus(AdDasTaskRunStatus.RUNNING.getCode());
        item.setTaskRunDetail("");
        item.setOperator(adDasUserView.getUserName());
        item.setCreateTime(System.currentTimeMillis());
        item.setUpdateTime(System.currentTimeMillis());
        adDasBenchmarkTaskEditTestRunRecordRepository.insert(item);
    }

    @Override
    public void taskCancel(AdDasUserView adDasUserView, Long taskRecordId) {
        adDasBenchmarkTaskEditTestRunRecordRepository.update(AdDasTaskRunStatus.CANCELED.getCode(),
                taskRecordId, adDasUserView.getUserName(), System.currentTimeMillis());
    }

    @Override
    public List<AdDasBenchmarkTaskEditRunRecordView> taskExecuteDetail() {
        List<AdDasBenchmarkTaskEditRunRecordView> views = new ArrayList<>();
        List<AdDasBenchmarkTaskEditTestRunRecord> records = adDasBenchmarkTaskEditTestRunRecordRepository.getAll();
        if (CollectionUtils.isEmpty(records)) {
            return views;
        }
        records.forEach(item -> {
            AdDasBenchmarkTaskEditRunRecordView view = new AdDasBenchmarkTaskEditRunRecordView();
            view.setTaskRunName(item.getTaskRunName());
            view.setCreateTime(item.getCreateTime());
            view.setUpdateTime(item.getUpdateTime());
            view.setOperator(item.getOperator());
            view.setTaskRunStatus(AdDasTaskRunStatus.getDesc(item.getTaskRunStatus()));
            view.setTaskRunRecordId(item.getId());
            view.setResultOutputPath(item.getResultPath());
            views.add(view);
        });
        return views;
    }
}
