package com.kuaishou.ad.das.platform.component.model;

import java.io.Serializable;

import lombok.Data;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-06-10
 */
@Data
public class AdDasDataStreamUserModel implements Serializable {

    private String userName;

    private String ownerName;

}
