package com.kuaishou.ad.das.platform.component.web.view;

import java.io.Serializable;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-06-09
 */
@Data
public class AdDasPlatformModifyIncrementView implements Serializable {

    @ApiModelProperty(value = "变更id")
    private Long modifyId;

    @ApiModelProperty(value = "变更名称")
    private String modifyName;

    @ApiModelProperty(value = "变更类型")
    private Integer modifyType;

    @ApiModelProperty(value = "变更状态")
    private Integer modifyStatus;

    @ApiModelProperty(value = "驳回理由")
    private String reason;

    @ApiModelProperty(value = "删除表")
    private Boolean deleteTable;

    @ApiModelProperty(value = "操作人")
    private String operator;

    @ApiModelProperty(value = "审批人")
    private String approver;

    @ApiModelProperty(value = "数据流表详情")
    private AdDasPlatformIncreTableDetailView increTableDetailView;
}
