package com.kuaishou.ad.das.platform.component.web.view;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-05-24
 */
@Data
public class AdDasPlatformIncreTableCascadeSchemaView {

    @ApiModelProperty(value = "级联表名")
    private String cascadeTableName;

    @ApiModelProperty(value = "增量流级联表主键id key")
    private String primaryIdKey;

    @ApiModelProperty(value = "增量流级联表外键id key")
    private String foreignKey;

    @ApiModelProperty(value = "主表级联字段ey")
    private String cascadeKey;
}
