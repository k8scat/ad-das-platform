package com.kuaishou.ad.das.platform.component.web.param;

import java.io.Serializable;

import com.kuaishou.ad.das.platform.component.web.view.AdDasPlatformIncreTableDetailView;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-05-24
 */
@Data
public class AdDasPlatformIncreModifyParam extends AdDasPlatformIncreTableDetailView implements Serializable {

    @ApiModelProperty(value = "变更id")
    private Long modifyId;

    @ApiModelProperty(value = "变更名称")
    private String modifyName;

    @ApiModelProperty(value = "变更类型")
    private Integer modifyType;

    @ApiModelProperty(value = "审批人")
    private String approver;

    @ApiModelProperty(value = "是否删除主表")
    private boolean delTable;
}
