package com.kuaishou.ad.das.platform.component.web.view;

import java.io.Serializable;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-08-02
 */
@Data
public class AdDasBenchmarkTaskDetailView implements Serializable {

    private static final long serialVersionUID = 7042064104979263343L;

    @ApiModelProperty(value = "任务名称")
    private String taskName;

    @ApiModelProperty(value = "任务运行实例")
    private String runServiceNode;

    @ApiModelProperty(value = "数据量")
    private Long dataCount;

    @ApiModelProperty(value = "数据文件大小")
    private Long dataFileSize;

    @ApiModelProperty(value = "任务状态code")
    private Integer taskStatus;

    @ApiModelProperty(value = "任务状态描述")
    private String taskStatusDesc;

    @ApiModelProperty(value = "创建时间")
    private Long createTime;

    @ApiModelProperty(value = "更新时间")
    private Long updateTime;
}
