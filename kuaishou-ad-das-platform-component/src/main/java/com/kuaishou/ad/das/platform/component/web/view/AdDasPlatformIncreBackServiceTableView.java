package com.kuaishou.ad.das.platform.component.web.view;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author caoyinhang <caoyinhang@kuaishou.com>
 * Created on 2021-07-06
 */
@Data
public class AdDasPlatformIncreBackServiceTableView {

    @ApiModelProperty(value = "增量流名称")
    private String incrName;
    @ApiModelProperty(value = "数据源")
    private String dataSource;
    @ApiModelProperty(value = "消费组")
    private String consGroup;
    @ApiModelProperty(value = "消费类型")
    private String consType;
}
