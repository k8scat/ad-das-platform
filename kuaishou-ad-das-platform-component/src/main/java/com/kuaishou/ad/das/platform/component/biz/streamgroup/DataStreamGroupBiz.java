package com.kuaishou.ad.das.platform.component.biz.streamgroup;

import com.kuaishou.ad.das.platform.component.web.param.datastream.DataStreamGroupModifyParam;
import com.kuaishou.ad.das.platform.component.web.param.datastream.DataStreamGroupQueryListParam;
import com.kuaishou.ad.das.platform.component.web.view.datastream.DataStreamGroupQueryView;

/**
 * @author Zhang Jialin <zhangjialin@kuaishou.com>
 * Created on 2021-06-03
 */
public interface DataStreamGroupBiz {
    /**
     * 获取数据流组
     * @param param 查询参数
     * @return 数据流组列表
     */
    DataStreamGroupQueryView queryList(DataStreamGroupQueryListParam param);

    /**
     * 修改数据流组相关内容
     * @param param 参数
     */
    void mod(DataStreamGroupModifyParam param);

    void deleteById(Long dataId);
}
