package com.kuaishou.ad.das.platform.component.web.view.benchmark.manage;

import java.util.List;

import com.kuaishou.ad.das.platform.component.web.PageData;

import lombok.Data;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-11-22
 */
@Data
public class AdDasBenchmarkManageHomeView extends PageData {

    private List<AdDasBenchmarkManageHomeDetailView> homeDetailViews;
}
