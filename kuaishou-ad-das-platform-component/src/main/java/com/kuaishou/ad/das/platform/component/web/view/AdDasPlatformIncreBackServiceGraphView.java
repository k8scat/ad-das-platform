package com.kuaishou.ad.das.platform.component.web.view;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author caoyinhang <caoyinhang@kuaishou.com>
 * Created on 2021-07-06
 */
@Data
public class AdDasPlatformIncreBackServiceGraphView {

    @ApiModelProperty(value = "增量流名称")
    private String incrementName;
    @ApiModelProperty(value = "数据源")
    private String dataSource;
    @ApiModelProperty(value = "消费组")
    private String consumerGroup;
    @ApiModelProperty(value = "消费类型")
    private String topicType;
    @ApiModelProperty(value = "负责人")
    private String ownerName;
    @ApiModelProperty(value = "最近操作时间")
    private String operationTime;
    @ApiModelProperty(value = "操作人")
    private String operator;

}
