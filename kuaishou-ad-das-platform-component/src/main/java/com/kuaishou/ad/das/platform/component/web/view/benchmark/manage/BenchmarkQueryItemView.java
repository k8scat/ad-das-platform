package com.kuaishou.ad.das.platform.component.web.view.benchmark.manage;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;

import com.kuaishou.ad.das.platform.component.biz.usergroup.UserGroupWithBindDTO;
import com.kuaishou.ad.das.platform.component.web.view.common.MainTableSimpleView;
import com.kuaishou.ad.das.platform.component.web.view.common.StreamClusterView;
import com.kuaishou.ad.das.platform.component.web.view.common.StreamUserGroupView;
import com.kuaishou.ad.das.platform.dal.model.AdDasBenchmark;
import com.kuaishou.ad.das.platform.dal.model.AdDasDataStreamGroup;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author Zhang Jialin <zhangjialin@kuaishou.com>
 * Created on 2021-05-18
 */
@Data
public class BenchmarkQueryItemView {
    @ApiModelProperty(value = "基准id")
    private Long benchmarkId;
    @ApiModelProperty(value = "基准流英文名称")
    private String benchmarkName;
    @ApiModelProperty(value = "基准中文描述")
    private String benchmarkDesc;
    @ApiModelProperty(value = "数据流组id")
    private Long dataStreamGroupId;
    @ApiModelProperty(value = "数据流组名称")
    private String dataStreamGroupName;
    @ApiModelProperty(value = "数据流组中文描述")
    private String dataStreamGroupDesc;
    @ApiModelProperty(value = "主表信息")
    private List<MainTableSimpleView> mainTables;
    @ApiModelProperty(value = "集群信息")
    private StreamClusterView clusterInfo;
    @ApiModelProperty(value = "HDFS路径")
    private String hdfsPath;
    @ApiModelProperty(value = "使用方用户组信息")
    private List<StreamUserGroupView> userGroups;
    @ApiModelProperty(value = "创建时间")
    private Long createTime;
    @ApiModelProperty(value = "更新时间")
    private Long updateTime;
    @ApiModelProperty(value = "操作人")
    private String operatorName;

    @ApiModelProperty(value = "增量流类型: 1-online 2-preOnline")
    private Integer streamType;

    @ApiModelProperty(value = "增量流关联Id, 当streamType=1时, 代表preOnline流id, 反之代表online流id")
    private Long streamRelatedId;

    public static BenchmarkQueryItemView build(AdDasBenchmark po, List<UserGroupWithBindDTO> userGroups,
            AdDasDataStreamGroup dataStreamGroup, List<MainTableSimpleView> mainTables) {
        BenchmarkQueryItemView vo = new BenchmarkQueryItemView();
        vo.setBenchmarkId(po.getId());
        vo.setBenchmarkName(po.getBenchmarkName());
        vo.setBenchmarkDesc(po.getBenchmarkDesc());
        vo.setDataStreamGroupId(dataStreamGroup.getId());
        vo.setDataStreamGroupName(dataStreamGroup.getDataStreamGroupName());
        vo.setMainTables(CollectionUtils.isEmpty(mainTables) ? Collections.emptyList() : mainTables);
        StreamClusterView streamClusterView = new StreamClusterView();
        streamClusterView.setClusterName(po.getClusterName());
        vo.setClusterInfo(streamClusterView);
        if (CollectionUtils.isNotEmpty(userGroups)) {
            vo.setUserGroups(userGroups.stream().map(StreamUserGroupView::build).collect(Collectors.toList()));
        }
        vo.setOperatorName(po.getOperatorEmailPrefix());
        vo.setHdfsPath(po.getHdfsPath());
        vo.setCreateTime(po.getCreateTime());
        vo.setUpdateTime(po.getUpdateTime());
        return vo;
    }
}
