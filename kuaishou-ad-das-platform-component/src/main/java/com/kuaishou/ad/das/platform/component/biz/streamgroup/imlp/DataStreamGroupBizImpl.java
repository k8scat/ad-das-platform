package com.kuaishou.ad.das.platform.component.biz.streamgroup.imlp;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.common.collect.Sets;
import com.kuaishou.ad.das.platform.component.biz.streamgroup.DataStreamGroupBiz;
import com.kuaishou.ad.das.platform.component.web.param.datastream.DataStreamGroupModifyParam;
import com.kuaishou.ad.das.platform.component.web.param.datastream.DataStreamGroupQueryListParam;
import com.kuaishou.ad.das.platform.component.web.view.datastream.DataStreamGroupItem;
import com.kuaishou.ad.das.platform.component.web.view.datastream.DataStreamGroupQueryView;
import com.kuaishou.ad.das.platform.dal.dao.AdDasBenchmarkCondition;
import com.kuaishou.ad.das.platform.dal.model.AdDasBenchmark;
import com.kuaishou.ad.das.platform.dal.model.AdDasDataStreamGroup;
import com.kuaishou.ad.das.platform.dal.model.AdDasIncrement;
import com.kuaishou.ad.das.platform.dal.repository.AdDasBenchmarkRepository;
import com.kuaishou.ad.das.platform.dal.repository.AdDasDataStreamGroupRepository;
import com.kuaishou.ad.das.platform.dal.repository.AdDasIncrementRepository;
import com.kuaishou.ad.das.platform.utils.AdDasBeanCoreUtils;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Zhang Jialin <zhangjialin@kuaishou.com>
 * Created on 2021-06-03
 */
@Slf4j
@Component
public class DataStreamGroupBizImpl implements DataStreamGroupBiz {

    @Autowired
    private AdDasDataStreamGroupRepository dataStreamGroupRepository;

    @Autowired
    private AdDasBenchmarkRepository benchmarkRepository;

    @Autowired
    private AdDasIncrementRepository incrementRepository;

    @Override
    public DataStreamGroupQueryView queryList(DataStreamGroupQueryListParam param) {
        DataStreamGroupQueryView result = DataStreamGroupQueryView.getDefaultInstance();
        result.setPageNum(param.getPageNum());
        result.setPageSize(param.getPageSize());
        result.setTotalCount(0L);

        // TODO  需要筛选权限， 先查有权限的groupIds, 4.0.1权限先不做
        Set<Long> ownGroupIds = Sets.newHashSet();
        if (param.getPrivilegeLevel() > 0) {

        }

        // 1. 查询数据流组列表，作为主表
        List<AdDasDataStreamGroup> dataStreamGroupsDb = dataStreamGroupRepository
                .queryByCondition(param.getDataStreamGroupIds(), param.getDataStreamGroupName(),
                param.getDataSourceNames(), param.getIncrementTopics(), param.getHdfsPath());
        log.info("dataStreamGroupsDb.size={}", dataStreamGroupsDb.size());
        if (CollectionUtils.isEmpty(dataStreamGroupsDb)) {
            return result;
        }
        // 做一次groupId 去重
        Map<Long, AdDasDataStreamGroup> groupId2DataMap = dataStreamGroupsDb.stream()
                .collect(Collectors.toMap(AdDasDataStreamGroup::getId, Function.identity(), (key1, key2) -> key2));

        List<AdDasDataStreamGroup> dataStreamGroup = dataStreamGroupsDb;
        if (groupId2DataMap.size() < dataStreamGroupsDb.size()) {
            dataStreamGroup = groupId2DataMap.entrySet()
                    .stream().map(entryData -> entryData.getValue())
                    .collect(Collectors.toList());
        }

        // 2. 查基准流信息
        AdDasBenchmarkCondition benchmarkCondition = new AdDasBenchmarkCondition();
        benchmarkCondition.setDataStreamGroupIds(dataStreamGroup.stream()
                .map(AdDasDataStreamGroup::getId).collect(Collectors.toList()));
        benchmarkCondition.setHdfsPaths(param.getHdfsPath());

        // 3. 查基准主表信息
        AdDasBenchmarkCondition condition = new AdDasBenchmarkCondition();
        condition.setDataStreamGroupIds(dataStreamGroup.stream()
                .map(AdDasDataStreamGroup::getId).collect(Collectors.toList()));
        List<AdDasBenchmark> benchmarks = benchmarkRepository.query(condition);
        Map<Long, Long> dataStreamGroupId2BenchmarkCount = benchmarks.stream()
                .collect(Collectors.groupingBy(AdDasBenchmark::getDataStreamGroupId, Collectors.counting()));

        // 6. TODO 获取基准流数据源总数

        // 5. 获取增量流总数
        List<AdDasIncrement> adDasIncrements = incrementRepository.searchByCondition(dataStreamGroup.stream()
                .map(AdDasDataStreamGroup::getId).collect(Collectors.toList()),
                param.getIncrementTopics(), param.getDataSourceNames());
        Map<Long, Long> dataStreamGroupId2IncrementCount = adDasIncrements.stream()
                .collect(Collectors.groupingBy(AdDasIncrement::getDataStreamGroupId, Collectors.counting()));

        // 7. 构建返回
        List<DataStreamGroupItem> dataList = dataStreamGroup.stream()
                .map(groupPoItem -> {
            DataStreamGroupItem groupItem = new DataStreamGroupItem();
            groupItem.setDataStreamGroupName(groupPoItem.getDataStreamGroupName());
            groupItem.setDataStreamGroupId(groupPoItem.getId());
            groupItem.setCreateTime(groupPoItem.getCreateTime());
            groupItem.setUpdateTime(groupPoItem.getUpdateTime());
            groupItem.setDataStreamGroupDesc(groupPoItem.getDataStreamGroupDesc());
            groupItem.setOwnerName(groupPoItem.getOwnerName());
            groupItem.setOwnerEmailPrefix(groupPoItem.getOwnerEmailPrefix());
            int dataStreamCount = 0;
            int dataSourceCount = 0;
            if (dataStreamGroupId2BenchmarkCount.containsKey(groupPoItem.getId())) {
                dataStreamCount += dataStreamGroupId2BenchmarkCount.get(groupPoItem.getId());
            }
            if (dataStreamGroupId2IncrementCount.containsKey(groupPoItem.getId())) {
                dataStreamCount += dataStreamGroupId2IncrementCount.get(groupPoItem.getId());
                dataSourceCount += dataStreamGroupId2IncrementCount.get(groupPoItem.getId());
            }
            groupItem.setDataStreamCount(dataStreamCount);
            groupItem.setDataSourceCount(dataSourceCount);
            return groupItem;
        }).collect(Collectors.toList());
        // 7.1 内存分页
        result.setTotalCount((long) dataList.size());
        List<DataStreamGroupItem> subList = AdDasBeanCoreUtils.paging(dataList, param.getPageNum(), param.getPageSize());
        result.setDataList(subList);
        return result;
    }

    @Override
    public void mod(DataStreamGroupModifyParam param) {

        if (param.getDataStreamGroupId() == 0 || param.getDataStreamGroupId() == null) {

            AdDasDataStreamGroup adDasDataStreamGroup = new AdDasDataStreamGroup();
            AdDasBeanCoreUtils.copyPropertiesIgnoreNull(adDasDataStreamGroup, param);
            adDasDataStreamGroup.setCreateTime(System.currentTimeMillis());
            adDasDataStreamGroup.setUpdateTime(System.currentTimeMillis());
            dataStreamGroupRepository.insert(adDasDataStreamGroup);
            return;
        }

        AdDasDataStreamGroup adDasDataStreamGroup = new AdDasDataStreamGroup();
        AdDasBeanCoreUtils.copyPropertiesIgnoreNull(adDasDataStreamGroup, param);
        adDasDataStreamGroup.setId(param.getDataStreamGroupId());
        adDasDataStreamGroup.setUpdateTime(System.currentTimeMillis());
        dataStreamGroupRepository.update(adDasDataStreamGroup);
    }

    @Override
    public void deleteById(Long dataId) {
        dataStreamGroupRepository.deletById(dataId);
    }
}
