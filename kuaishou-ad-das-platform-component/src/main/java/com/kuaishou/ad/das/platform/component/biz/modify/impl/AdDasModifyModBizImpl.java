package com.kuaishou.ad.das.platform.component.biz.modify.impl;

import static com.kuaishou.ad.das.platform.utils.exception.ErrorCode.DATA_NOT_FOUND;
import static com.kuaishou.ad.das.platform.utils.exception.ErrorCode.NO_APPROVE_POWER;
import static com.kuaishou.ad.das.platform.utils.exception.ErrorCode.PARAM_ERROR;
import static com.kuaishou.ad.das.platform.utils.exception.ErrorCode.PB_VERSION_HAS_BEEN_PUBLISHED;
import static com.kuaishou.ad.das.platform.utils.exception.ErrorCode.SERVICE_TYPE_NOT_FOUND;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import com.kuaishou.ad.das.platform.component.biz.common.AdDasCommonBiz;
import com.kuaishou.ad.das.platform.component.biz.modify.AdDasModifyModBiz;
import com.kuaishou.ad.das.platform.component.web.param.AdDasModifyPbParam;
import com.kuaishou.ad.das.platform.component.web.param.AdDasPublishPbParam;
import com.kuaishou.ad.das.platform.component.web.view.AdDasModifyDataView;
import com.kuaishou.ad.das.platform.core.common.daszk.Helper.AdDasPlatformCommonZkHelper;
import com.kuaishou.ad.das.platform.core.common.daszk.model.AdDasServiceNodeSchema;
import com.kuaishou.ad.das.platform.dal.model.AdDasModify;
import com.kuaishou.ad.das.platform.dal.model.AdDasModifyPbMsg;
import com.kuaishou.ad.das.platform.dal.model.AdDasModifyServicePbRecord;
import com.kuaishou.ad.das.platform.dal.model.AdDasServicePb;
import com.kuaishou.ad.das.platform.dal.repository.AdDasModifyPbMsgRepository;
import com.kuaishou.ad.das.platform.dal.repository.AdDasModifyRepository;
import com.kuaishou.ad.das.platform.dal.repository.AdDasModifyServicePbRecordRepository;
import com.kuaishou.ad.das.platform.dal.repository.AdDasServicePbRepository;
import com.kuaishou.ad.das.platform.utils.AdDasBeanCoreUtils;
import com.kuaishou.ad.das.platform.utils.dasenum.AdDasModifyPublishStatusEnum;
import com.kuaishou.ad.das.platform.utils.dasenum.AdDasModifyStatusEnum;
import com.kuaishou.ad.das.platform.utils.dasenum.AdDasStatusEnum;
import com.kuaishou.ad.das.platform.utils.exception.AdDasServiceException;
import com.kuaishou.ad.das.platform.utils.model.AdDasUserView;

import lombok.extern.slf4j.Slf4j;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-12-06
 */
@Lazy
@Slf4j
@Service
public class AdDasModifyModBizImpl extends AdDasCommonBiz implements AdDasModifyModBiz {

    @Autowired
    private AdDasModifyRepository adDasModifyRepository;

    @Autowired
    private AdDasModifyPbMsgRepository adDasModifyPbMsgRepository;

    @Autowired
    private AdDasPlatformCommonZkHelper adDasCommonZkHelper;

    @Autowired
    private AdDasModifyServicePbRecordRepository adDasModifyServicePbRecordRepository;

    @Autowired
    private AdDasServicePbRepository adDasServicePbRepository;

    @Override
    public AdDasModifyDataView addModifyPb(AdDasUserView adDasUserView, AdDasModifyPbParam param) {

        // 检查 pbVersion
        if (StringUtils.isEmpty(param.getTargetPbversion())) {
            throw AdDasServiceException.ofMessage(PARAM_ERROR);
        }

        checkPbVersion(param.getTargetPbversion());

        if (param.getTargetPbversion().equals(param.getCurServicePbVersion())) {
            throw AdDasServiceException.ofMessage(PB_VERSION_HAS_BEEN_PUBLISHED);
        }

        AdDasModifyDataView result = new AdDasModifyDataView();
        //  写入 ad_das_modify 表
        Long timeStamp = System.currentTimeMillis();
        AdDasModify adDasModify = new AdDasModify();
        adDasModify.setModifyName(param.getModifyName());
        adDasModify.setModifyType(param.getModifyType());
        adDasModify.setOperator(adDasUserView.getUserName());
        adDasModify.setApprover(param.getApprover());
        adDasModify.setModifyStatus(AdDasModifyStatusEnum.APPROVING.getCode());
        adDasModify.setCreateTime(timeStamp);
        adDasModify.setUpdateTime(timeStamp);
        adDasModifyRepository.insert(adDasModify);

        // 查询
        List<AdDasModify> adDasModifies = adDasModifyRepository.queryByNameAndCreateTime(
                adDasModify.getModifyName(), adDasModify.getModifyType(),
                adDasModify.getOperator(), adDasModify.getCreateTime());

        AdDasModify adDasModifyNew = adDasModifies.get(0);

        //  写入 ad_das_modify_pb_msg表
        AdDasModifyPbMsg adDasModifyPbMsg = new AdDasModifyPbMsg();
        adDasModifyPbMsg.setModifyId(adDasModifyNew.getId());
        adDasModifyPbMsg.setCurLastestPbVersion(param.getLastestPbVersion());
        adDasModifyPbMsg.setServiceName(param.getServiceName());
        // 从 zk 上获取 service type
        String serviceType = "";
        try {
            serviceType = adDasCommonZkHelper.queryServiceTypeByServiceName(param.getServiceName());

        } catch (Exception e) {
            log.error("adDasCommonZkHelper.queryServiceTypeByServiceName() occur error! serviceName={}",
                    param.getServiceName(), e);
        }
        if (StringUtils.isEmpty(serviceType)) {
            throw AdDasServiceException.ofMessage(SERVICE_TYPE_NOT_FOUND);
        }
        adDasModifyPbMsg.setServiceType(serviceType);

        adDasModifyPbMsg.setCurPbVersion(param.getCurServicePbVersion());
        adDasModifyPbMsg.setTargetPbVersion(param.getTargetPbversion());
        adDasModifyPbMsg.setPreVersion(param.getCurServicePbVersion());
        adDasModifyPbMsg.setPiplineLinks(param.getPiplineLinks());
        adDasModifyPbMsg.setCrLinks(param.getCrLinks());
        adDasModifyPbMsg.setOperator(adDasUserView.getUserName());
        adDasModifyPbMsg.setCreateTime(timeStamp);
        adDasModifyPbMsg.setUpdateTime(timeStamp);

        log.info("adDasModifyPbMsg={}", adDasModifyPbMsg);
        adDasModifyPbMsgRepository.insert(adDasModifyPbMsg);

        AdDasBeanCoreUtils.copyPropertiesIgnoreNull(result, adDasModifyNew);

        return result;
    }

    @Override
    public void approveModifyPb(AdDasUserView adDasUserView, Long modifyId,
                                Integer approveType, String reason) {

        log.info("approveModifyPb() adDasUserView={}", adDasUserView);

        // 后端校验权限
        if (adDasUserView.getRoleType() != 1) {
            throw AdDasServiceException.ofMessage(NO_APPROVE_POWER.getCode(), NO_APPROVE_POWER.getMsg());
        }

        // check param
        List<AdDasModify> adDasModifies = adDasModifyRepository.queryByIdAndType(modifyId, null);
        if (CollectionUtils.isEmpty(adDasModifies)) {
            throw AdDasServiceException.ofMessage(DATA_NOT_FOUND.getCode(), DATA_NOT_FOUND.getMsg());
        }

        // 更新 ad_das_modify 的 approver & status & update_time
        adDasModifyRepository.updateApprove(modifyId, adDasUserView.getUserName(), approveType,
                reason, System.currentTimeMillis());

    }

    @Override
    public void publishModifyPb(AdDasUserView adDasUserView, AdDasPublishPbParam param) {

        // check param
        checkModifyData(param.getModifyId());

        AdDasModifyPbMsg adDasModifyPbMsg = getDasModifyPbMsg(param.getModifyId());
        String serviceName = adDasModifyPbMsg.getServiceName();
        String serviceType = adDasModifyPbMsg.getServiceType();
        if (StringUtils.isEmpty(serviceType)) {
            throw AdDasServiceException.ofMessage(SERVICE_TYPE_NOT_FOUND);
        }

        List<AdDasServicePb> servicePbs = adDasServicePbRepository
                .queryByService(serviceName, AdDasStatusEnum.DEFAULT.getCode());
        if (CollectionUtils.isEmpty(servicePbs)) {
            // 第一次接入, 写入 service_pb 表
            AdDasServicePb adDasServicePb = new AdDasServicePb();
            adDasServicePb.setServiceType(serviceType);
            adDasServicePb.setServiceName(serviceName);
            adDasServicePb.setServicePbStatus(AdDasStatusEnum.DEFAULT.getCode());
            adDasServicePb.setPbVersion(adDasModifyPbMsg.getTargetPbVersion());
            adDasServicePb.setCreateTime(System.currentTimeMillis());
            adDasServicePb.setOperator(adDasUserView.getUserName());
            adDasServicePb.setUpdateTime(System.currentTimeMillis());
            log.info("insert adDasServicePb={}", adDasServicePb);
            adDasServicePbRepository.insert(adDasServicePb);
        }

        AdDasModifyStatusEnum modifyStatusEnum = AdDasModifyStatusEnum.PUBLISH_ING;

        List<String> publishInstances = param.getPublishInstances();

        try {
            // 更新 ad_das_modify 的 status & update_time
            adDasModifyRepository.updateStatus(param.getModifyId(), modifyStatusEnum.getCode(),
                    adDasUserView.getUserName(), System.currentTimeMillis());
            // 初始化 service_pb_record 表
            initServicePbRecord(adDasModifyPbMsg, publishInstances);

            // 初始化 更新 实例对应的 targetVersion
            initServiceInstanceVersion(adDasModifyPbMsg, publishInstances);

            // 触发 ZK /prepb
            adDasCommonZkHelper.publishPbUpdateZkMsg(adDasModifyPbMsg, publishInstances);

        } catch (Exception e) {
            log.error("publishModifyPb() occur error! adDasUserView={}, modifyId={}",
                    adDasUserView, param.getModifyId(), e);
        }
    }

    @Override
    public void terminateModifyPb(AdDasUserView adDasUserView, AdDasPublishPbParam param) {

        // check param
        checkModifyData(param.getModifyId());

        // 更新 ad_das_modify status & update_time
        adDasModifyRepository.updateStatus(param.getModifyId(), param.getModifyStatus(),
                adDasUserView.getUserName(), System.currentTimeMillis());
    }

    @Override
    public void rollBackModifyPb(AdDasUserView adDasUserView, AdDasPublishPbParam param) {

        AdDasModifyPbMsg adDasModifyPbMsg = getDasModifyPbMsg(param.getModifyId());

        AdDasModifyStatusEnum modifyStatusEnum = AdDasModifyStatusEnum.ROLLBACK_ING;
        List<String> publishInstances = param.getPublishInstances();
        if (!CollectionUtils.isEmpty(publishInstances)) {
            // modifyStatusEnum = AdDasModifyStatusEnum.GRAY_ROLLBACK_ING;
        }

        try {
            // 更新 ad_das_modify 的 status & update_time
            adDasModifyRepository.updateStatus(param.getModifyId(), modifyStatusEnum.getCode(),
                    adDasUserView.getUserName(), System.currentTimeMillis());

            // 初始化 更新 实例对应的 targetVersion
            initServiceInstanceVersion(adDasModifyPbMsg, publishInstances);

            // 触发zk 发布
            adDasCommonZkHelper.rollbecckPbUpdateZkMsg(adDasModifyPbMsg);
        } catch (Exception e) {
            log.error("rollBackModifyPb() occur error! param={}", param, e);
        }
    }

    private void checkModifyData(Long modifyId) {
        // check param
        List<AdDasModify> adDasModifies = adDasModifyRepository.queryByIdAndType(modifyId, null);
        if (CollectionUtils.isEmpty(adDasModifies)) {
            throw AdDasServiceException.ofMessage(DATA_NOT_FOUND);
        }
    }

    private AdDasModifyPbMsg getDasModifyPbMsg(Long modifyId) {
        // 查询PB发布详情
        List<AdDasModifyPbMsg> adDasModifyPbMsgs = adDasModifyPbMsgRepository.queryByModifyId(modifyId);
        log.info("publishModifyPb() adDasModifyPbMsgs={}", adDasModifyPbMsgs);
        if (CollectionUtils.isEmpty(adDasModifyPbMsgs)) {
            throw AdDasServiceException.ofMessage(DATA_NOT_FOUND.getCode(), DATA_NOT_FOUND.getMsg());
        }

        AdDasModifyPbMsg dasModifyPbMsg = adDasModifyPbMsgs.get(0);

        return dasModifyPbMsg;
    }

    private void initServiceInstanceVersion(AdDasModifyPbMsg adDasModifyPbMsg, List<String> publishInstances) throws Exception {
        List<AdDasServiceNodeSchema> adDasServiceNodeSchemas = adDasCommonZkHelper
                .queryServiceNodeSchemas(adDasModifyPbMsg.getServiceName());
        log.info("publishModifyPb() adDasServiceNodeSchemas={}", adDasServiceNodeSchemas);
        Map<String, AdDasServiceNodeSchema> dasServiceNodeSchemaMap = new HashMap<>();
        for (AdDasServiceNodeSchema dasServiceNodeSchema : adDasServiceNodeSchemas) {

            String serviceNodeName = dasServiceNodeSchema.getContainerId();
            log.info("publishModifyPb() serviceNodeName={}", serviceNodeName);
            if (!CollectionUtils.isEmpty(publishInstances)) {
                if (publishInstances.contains(serviceNodeName)) {
                    dasServiceNodeSchemaMap.put(serviceNodeName, dasServiceNodeSchema);
                }
            } else {
                dasServiceNodeSchemaMap.put(serviceNodeName, dasServiceNodeSchema);
            }
        }

        adDasCommonZkHelper.updateServiceNodeSchemas(adDasModifyPbMsg.getServiceName(), dasServiceNodeSchemaMap);
    }

    private void initServicePbRecord(AdDasModifyPbMsg adDasModifyPbMsg, List<String> publishInstances) throws Exception {
        List<AdDasServiceNodeSchema> adDasServiceNodeSchemas = adDasCommonZkHelper
                .queryServiceNodeSchemas(adDasModifyPbMsg.getServiceName());
        log.info("publishModifyPb() adDasServiceNodeSchemas={}", adDasServiceNodeSchemas);

        // 初始化 写入 ad_das_modify_service_pb_record 表
        for (AdDasServiceNodeSchema adDasServiceNodeSchema : adDasServiceNodeSchemas) {

            String serviceNodeName = adDasServiceNodeSchema.getContainerId();
            log.info("publishModifyPb() serviceNodeName={}", serviceNodeName);
            if (!CollectionUtils.isEmpty(publishInstances) && !publishInstances.contains(serviceNodeName)) {
                // 不在灰度范围内
                return;
            }

            List<AdDasModifyServicePbRecord> adDasModifyServicePbRecords = adDasModifyServicePbRecordRepository
                    .queryByCondition(adDasModifyPbMsg.getModifyId(), adDasModifyPbMsg.getServiceName(), serviceNodeName);

            log.info("publishModifyPb() adDasModifyServicePbRecords={}", adDasModifyServicePbRecords);
            if (!CollectionUtils.isEmpty(adDasModifyServicePbRecords)) {
                continue;
            }

            AdDasModifyServicePbRecord adDasModifyServicePbRecord = new AdDasModifyServicePbRecord();
            adDasModifyServicePbRecord.setModifyId(adDasModifyPbMsg.getModifyId());
            adDasModifyServicePbRecord.setServiceName(adDasModifyPbMsg.getServiceName());
            adDasModifyServicePbRecord.setServiceNodeName(serviceNodeName);
            adDasModifyServicePbRecord.setServiceType(adDasModifyPbMsg.getServiceType());
            adDasModifyServicePbRecord.setCurPbVersion(adDasModifyPbMsg.getCurPbVersion());
            adDasModifyServicePbRecord.setTargetPbVersion(adDasModifyPbMsg.getTargetPbVersion());
            adDasModifyServicePbRecord.setPublishStatus(AdDasModifyPublishStatusEnum.PUBLISHING.getCode());
            adDasModifyServicePbRecord.setCreateTime(System.currentTimeMillis());
            adDasModifyServicePbRecord.setUpdateTime(System.currentTimeMillis());

            log.info("publishModifyPb() adDasModifyServicePbRecord={}", adDasModifyServicePbRecord);
            adDasModifyServicePbRecordRepository.insert(adDasModifyServicePbRecord);
        }
    }
}
