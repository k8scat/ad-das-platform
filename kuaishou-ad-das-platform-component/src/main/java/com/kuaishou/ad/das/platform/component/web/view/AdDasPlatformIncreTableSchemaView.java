package com.kuaishou.ad.das.platform.component.web.view;

import java.util.List;

import com.kuaishou.ad.das.platform.core.increment.model.AdDasPlatformTableColumnModel;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-05-24
 */
@Data
public class AdDasPlatformIncreTableSchemaView {

    @ApiModelProperty(value = "增量流表名称")
    private String tableName;

    @ApiModelProperty(value = "增量流表类型： 1-主表 2-级联表")
    private Integer tableType;

    @ApiModelProperty(value = "增量流表主键id")
    private String primaryIdKey;

    @ApiModelProperty(value = "增量流表字段schema配置")
    private List<AdDasPlatformTableColumnModel> tableColumnViews;

    @ApiModelProperty(value = "创建时间")
    private Long createTime;

    @ApiModelProperty(value = "更新时间")
    private Long updateTime;

    @ApiModelProperty(value = "操作人")
    private String operator;

    @ApiModelProperty(value = "操作类型： 1-新增 2-修改 3-删除")
    private Integer operatorType = 0;
}
