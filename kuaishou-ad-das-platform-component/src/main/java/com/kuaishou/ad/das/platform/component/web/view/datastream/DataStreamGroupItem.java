package com.kuaishou.ad.das.platform.component.web.view.datastream;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 *
 * @author Zhang Jialin <zhangjialin@kuaishou.com>
 * Created on 2021-05-18
 */
@Data
@ApiModel(value = "数据流组查询-元素项")
public class DataStreamGroupItem {
    @ApiModelProperty(value = "数据流组id")
    private Long dataStreamGroupId;
    @ApiModelProperty(value = "数据流组名称")
    private String dataStreamGroupName;
    @ApiModelProperty(value = "数据流组介绍")
    private String dataStreamGroupDesc;
    @ApiModelProperty(value = "数据流条数")
    private Integer dataStreamCount;
    @ApiModelProperty(value = "数据源个数")
    private Integer dataSourceCount;
    @ApiModelProperty(value = "负责人中文名")
    private String ownerName;
    @ApiModelProperty(value = "负责人邮箱前缀")
    private String ownerEmailPrefix;
    @ApiModelProperty(value = "创建时间")
    private Long createTime;
    @ApiModelProperty(value = "更新时间")
    private Long updateTime;
}
