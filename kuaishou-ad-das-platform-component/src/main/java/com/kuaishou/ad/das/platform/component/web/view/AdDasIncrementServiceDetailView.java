package com.kuaishou.ad.das.platform.component.web.view;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-12-23
 */
@Data
public class AdDasIncrementServiceDetailView {

    /**
     * 审核记录id
     */
    private Long modifyId;

    private String mainTable;

    private String pbClass;

    @ApiModelProperty(value = "数据展示的是编辑表中的数据")
    private Integer status;

    @ApiModelProperty(value = "处理类型")
    private Integer handleType;

    private String operator;

    private String approver;

    private Long createTime;

    private Long updateTime;
}
