package com.kuaishou.ad.das.platform.component.web.view;

import java.util.List;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author caoyinhang <caoyinhang@kuaishou.com>
 * Created on 2021-07-06
 */
@Data
public class AdDasPlatformIncreBackRecondSearchView {

    @ApiModelProperty(value = "增量回溯记录查询结果列表")
    private List<AdDasPlatformIncreBackRecondGraphView> increBackRecondGraphViews;

    @ApiModelProperty(value = "总数")
    private Integer totalCount;
}
