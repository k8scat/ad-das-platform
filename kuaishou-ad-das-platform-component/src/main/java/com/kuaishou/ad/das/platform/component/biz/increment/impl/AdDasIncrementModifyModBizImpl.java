package com.kuaishou.ad.das.platform.component.biz.increment.impl;

import static com.kuaishou.ad.das.platform.utils.KimUtil.sendAdDasModifyMsg;
import static com.kuaishou.ad.das.platform.utils.constant.AdDasApiConstant.DAS_INCREMENT_CLICKURL;
import static com.kuaishou.ad.das.platform.utils.exception.ErrorCode.DATA_NOT_FOUND;
import static com.kuaishou.ad.das.platform.utils.exception.ErrorCode.INCREMENT_DATA_NOT_FOUND;
import static com.kuaishou.ad.das.platform.utils.exception.ErrorCode.INCREMENT_TABLE_OR_PB_ENUM_ERROR;
import static com.kuaishou.ad.das.platform.utils.exception.ErrorCode.INCREMENT_TABLE_SCHEMA_ERROR;
import static com.kuaishou.ad.das.platform.utils.exception.ErrorCode.INCREMENT_TABLE_SCHEMA_NOT_NULL;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.google.common.collect.Lists;
import com.kuaishou.ad.das.platform.component.biz.increment.AdDasIncrementModifyModBiz;
import com.kuaishou.ad.das.platform.component.web.config.AdDasApiPropertiesConfig;
import com.kuaishou.ad.das.platform.component.web.param.AdDasEditIncrementServiceParam;
import com.kuaishou.ad.das.platform.component.web.param.AdDasEditIncrementStreamParam;
import com.kuaishou.ad.das.platform.component.web.param.AdDasPlatformIncreModifyParam;
import com.kuaishou.ad.das.platform.component.web.view.AdDasPlatformIncreTableSchemaView;
import com.kuaishou.ad.das.platform.dal.model.AdDasIncrement;
import com.kuaishou.ad.das.platform.dal.model.AdDasIncrementServiceInfo;
import com.kuaishou.ad.das.platform.dal.model.AdDasIncrementTableDetail;
import com.kuaishou.ad.das.platform.dal.model.AdDasModify;
import com.kuaishou.ad.das.platform.dal.model.AdDasModifyIncrementTableDetail;
import com.kuaishou.ad.das.platform.dal.model.AdDasModifyIncrementTableRecord;
import com.kuaishou.ad.das.platform.dal.repository.AdDasIncrementRepository;
import com.kuaishou.ad.das.platform.dal.repository.AdDasIncrementServiceInfoRepository;
import com.kuaishou.ad.das.platform.dal.repository.AdDasIncrementTableDetailRepository;
import com.kuaishou.ad.das.platform.dal.repository.AdDasModifyIncrementTableDetailRepository;
import com.kuaishou.ad.das.platform.dal.repository.AdDasModifyIncrementTableRecordRepository;
import com.kuaishou.ad.das.platform.dal.repository.AdDasModifyRepository;
import com.kuaishou.ad.das.platform.utils.AdDasBeanCoreUtils;
import com.kuaishou.ad.das.platform.utils.dasenum.AdDasModifyStatusEnum;
import com.kuaishou.ad.das.platform.utils.exception.AdDasServiceException;
import com.kuaishou.ad.das.platform.utils.exception.ErrorCode;
import com.kuaishou.ad.das.platform.utils.model.AdDasUserView;
import com.kuaishou.framework.util.ObjectMapperUtils;

import lombok.extern.slf4j.Slf4j;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-12-23
 */
@Lazy
@Slf4j
@Service
public class AdDasIncrementModifyModBizImpl implements AdDasIncrementModifyModBiz {

    public static final Set<Integer> MODIFY_ING_ENUM_SET = new HashSet<Integer>() {
        {
            add(AdDasModifyStatusEnum.EDITING.getCode());
            add(AdDasModifyStatusEnum.APPROVING.getCode());
            add(AdDasModifyStatusEnum.APPROVED.getCode());
            add(AdDasModifyStatusEnum.REJECTED.getCode());
            add(AdDasModifyStatusEnum.PUBLISH_ING.getCode());
            add(AdDasModifyStatusEnum.ENDING.getCode());
            add(AdDasModifyStatusEnum.ROLLBACK_ING.getCode());
        }
    };

    @Autowired
    private AdDasModifyRepository adDasModifyRepository;

    @Autowired
    private AdDasIncrementRepository adDasIncrementRepository;

    @Autowired
    private AdDasIncrementServiceInfoRepository incrementServiceInfoRepository;

    @Autowired
    private AdDasModifyIncrementTableDetailRepository modifyIncrementTableDetailRepository;

    @Autowired
    private AdDasModifyIncrementTableRecordRepository modifyIncrementTableRecordRepository;

    @Autowired
    private AdDasIncrementTableDetailRepository incrementTableDetailRepository;

    @Autowired
    private AdDasApiPropertiesConfig apiPropertiesConfig;

    @Override
    public void increModify(AdDasPlatformIncreModifyParam increModifyParam, AdDasUserView adDasUserView) {

        if (StringUtils.isEmpty(increModifyParam.getMainTable()) || StringUtils.isEmpty(increModifyParam.getPbEnum())) {
            throw AdDasServiceException.ofMessage(INCREMENT_TABLE_OR_PB_ENUM_ERROR);
        }

        // 判断 表是否已接入
        AdDasModifyIncrementTableDetail modifyIncrementTableDetail = modifyIncrementTableDetailRepository
                .queryMainTableLast(increModifyParam.getIncreStreamId(), increModifyParam.getMainTable());

        if (modifyIncrementTableDetail != null
                && (increModifyParam.getModifyId() == null || increModifyParam.getModifyId() <= 0)) {
            // 判断变更流程
            AdDasModify adDasModify = adDasModifyRepository
                    .queryById(modifyIncrementTableDetail.getModifyId());
            if (MODIFY_ING_ENUM_SET.contains(adDasModify.getModifyStatus())) {
                // 提示正在流程中
                throw AdDasServiceException.ofMessage(ErrorCode.MODIFY_IS_PROCESSING);
            }
        }

        if (StringUtils.isEmpty(increModifyParam.getApprover())) {
            throw AdDasServiceException.ofMessage(ErrorCode.MODIFY_NEED_APPROVER);
        }

        Map<String, AdDasPlatformIncreTableSchemaView> tableSchemaViewMap = increModifyParam
                .getTableSchemaViewMap();
        if (CollectionUtils.isEmpty(tableSchemaViewMap)) {
            throw AdDasServiceException.ofMessage(INCREMENT_TABLE_SCHEMA_NOT_NULL);
        }

        if (!CollectionUtils.isEmpty(increModifyParam.getCascadeTable()) && increModifyParam.getCascadeTable().size() + 1 != tableSchemaViewMap.size()) {
            throw AdDasServiceException.ofMessage(INCREMENT_TABLE_SCHEMA_ERROR);
        }

        AdDasModify adDasModify = null;
        if (increModifyParam.getModifyId() == null || increModifyParam.getModifyId() <= 0) {
            // 先写入 ad_das_modify 表
            adDasModify = new AdDasModify();
            adDasModify.setModifyName(increModifyParam.getModifyName());
            adDasModify.setModifyStatus(AdDasModifyStatusEnum.APPROVING.getCode());
            adDasModify.setModifyType(increModifyParam.getModifyType());
            adDasModify.setOperator(adDasUserView.getUserName());
            adDasModify.setApprover(increModifyParam.getApprover());
            adDasModify.setReason("");
            adDasModify.setCreateTime(System.currentTimeMillis());
            adDasModify.setUpdateTime(System.currentTimeMillis());
            adDasModifyRepository.insert(adDasModify);

            // 查询
            List<AdDasModify> adDasModifies = adDasModifyRepository.queryByNameAndCreateTime(
                    adDasModify.getModifyName(), adDasModify.getModifyType(),
                    adDasModify.getOperator(), adDasModify.getCreateTime());

            adDasModify = adDasModifies.get(0);
            addIncrementModify(adDasModify, increModifyParam, adDasUserView, tableSchemaViewMap);
        } else {
            adDasModify = adDasModifyRepository.queryById(increModifyParam.getModifyId());
            if (adDasModify == null) {
                throw AdDasServiceException.ofMessage(DATA_NOT_FOUND);
            }

            adDasModifyRepository.updateStatus(increModifyParam.getModifyId(), AdDasModifyStatusEnum.APPROVING.getCode(),
                    adDasUserView.getUserName(), System.currentTimeMillis());

            List<AdDasModifyIncrementTableDetail> modifyIncrementTableDetails = modifyIncrementTableDetailRepository
                    .queryByModifyId(increModifyParam.getModifyId());

            if (CollectionUtils.isEmpty(modifyIncrementTableDetails)) {
                throw AdDasServiceException.ofMessage(INCREMENT_DATA_NOT_FOUND);
            }

            AdDasModifyIncrementTableDetail mainIncrementTable = modifyIncrementTableDetails.stream()
                    .filter(incrementTableDetail -> incrementTableDetail.getTableType() == 1)
                    .findFirst().get();

            mainIncrementTable.setIncreId(increModifyParam.getIncreStreamId());
            mainIncrementTable.setDataSource(increModifyParam.getDataSource());
            mainIncrementTable.setIncreTable(increModifyParam.getMainTable());
            mainIncrementTable.setPbClassEnum(increModifyParam.getPbEnum());
            mainIncrementTable.setTableType(1);

            String cascadedTable = ObjectMapperUtils.toJSON(increModifyParam.getCascadeTable());
            mainIncrementTable.setCascadedTable(cascadedTable);
            String cascadeSchema = ObjectMapperUtils.toJSON(increModifyParam.getTableCascadeSchemaViewMap());
            mainIncrementTable.setCascadeSchema(cascadeSchema);

            AdDasPlatformIncreTableSchemaView mainTableSchema = tableSchemaViewMap.get(increModifyParam.getMainTable());
            mainIncrementTable.setPrimaryIdKey(mainTableSchema.getPrimaryIdKey());
            String tableColumn = ObjectMapperUtils.toJSON(mainTableSchema.getTableColumnViews());
            mainIncrementTable.setTableColumn(tableColumn);

            if (increModifyParam.isDelTable()) {
                mainIncrementTable.setOperatType(3);
            } else {
                mainIncrementTable.setOperatType(mainTableSchema.getOperatorType());
            }

            mainIncrementTable.setOperator(adDasUserView.getUserName());
            mainIncrementTable.setApprover(increModifyParam.getApprover());
            mainIncrementTable.setCreateTime(System.currentTimeMillis());
            mainIncrementTable.setUpdateTime(System.currentTimeMillis());
            // 先更新主表
            modifyIncrementTableDetailRepository.update(mainIncrementTable);

            Map<String, AdDasModifyIncrementTableDetail> table2IncrementTableDetailMap = modifyIncrementTableDetails.stream()
                    .filter(incrementTableDetail -> incrementTableDetail.getTableType() == 2)
                    .collect(Collectors.toMap(AdDasModifyIncrementTableDetail::getIncreTable, Function.identity(), (key1, key2) -> key2));

            // 再更新级联表
            if (!CollectionUtils.isEmpty(increModifyParam.getCascadeTable())) {

                // 后写入级联表
                for (String cascadeTable : increModifyParam.getCascadeTable()) {

                    if (table2IncrementTableDetailMap.containsKey(cascadedTable)) {

                        AdDasModifyIncrementTableDetail incrementTableDetail = table2IncrementTableDetailMap.get(cascadedTable);
                        incrementTableDetail.setIncreId(increModifyParam.getIncreStreamId());
                        incrementTableDetail.setDataSource(increModifyParam.getDataSource());
                        incrementTableDetail.setIncreTable(cascadeTable);
                        incrementTableDetail.setTableType(2);

                        AdDasPlatformIncreTableSchemaView cascadeTableSchema = tableSchemaViewMap.get(cascadeTable);
                        incrementTableDetail.setPrimaryIdKey(cascadeTableSchema.getPrimaryIdKey());
                        String cascdeTableColumn = ObjectMapperUtils.toJSON(cascadeTableSchema.getTableColumnViews());
                        incrementTableDetail.setTableColumn(cascdeTableColumn);
                        incrementTableDetail.setPbClassEnum(increModifyParam.getPbEnum());

                        if (increModifyParam.isDelTable()) {
                            incrementTableDetail.setOperatType(3);
                        } else {
                            incrementTableDetail.setOperatType(cascadeTableSchema.getOperatorType());
                        }

                        incrementTableDetail.setApprover(increModifyParam.getApprover());
                        incrementTableDetail.setOperator(adDasUserView.getUserName());
                        incrementTableDetail.setCreateTime(System.currentTimeMillis());
                        incrementTableDetail.setUpdateTime(System.currentTimeMillis());

                        modifyIncrementTableDetailRepository.update(incrementTableDetail);
                    } else {
                        AdDasModifyIncrementTableDetail cascadeModifyTableDetail = new AdDasModifyIncrementTableDetail();
                        cascadeModifyTableDetail.setModifyId(adDasModify.getId());
                        cascadeModifyTableDetail.setIncreId(increModifyParam.getIncreStreamId());
                        cascadeModifyTableDetail.setDataSource(increModifyParam.getDataSource());
                        cascadeModifyTableDetail.setIncreTable(cascadeTable);
                        cascadeModifyTableDetail.setTableType(2);

                        AdDasPlatformIncreTableSchemaView cascadeTableSchema = tableSchemaViewMap.get(cascadeTable);
                        cascadeModifyTableDetail.setPrimaryIdKey(cascadeTableSchema.getPrimaryIdKey());
                        String cascdeTableColumn = ObjectMapperUtils.toJSON(cascadeTableSchema.getTableColumnViews());
                        cascadeModifyTableDetail.setTableColumn(cascdeTableColumn);
                        cascadeModifyTableDetail.setPbClassEnum(increModifyParam.getPbEnum());

                        if (increModifyParam.isDelTable()) {
                            cascadeModifyTableDetail.setOperatType(3);
                        } else {
                            cascadeModifyTableDetail.setOperatType(cascadeTableSchema.getOperatorType());
                        }

                        cascadeModifyTableDetail.setApprover(increModifyParam.getApprover());
                        cascadeModifyTableDetail.setOperator(adDasUserView.getUserName());
                        cascadeModifyTableDetail.setCreateTime(System.currentTimeMillis());
                        cascadeModifyTableDetail.setUpdateTime(System.currentTimeMillis());

                        modifyIncrementTableDetailRepository.insert(cascadeModifyTableDetail);
                    }
                }

                // 删除变更废弃的 表
                for (AdDasModifyIncrementTableDetail cascadeTableData : modifyIncrementTableDetails) {

                    if (!increModifyParam.getCascadeTable().contains(cascadeTableData.getIncreTable()) && cascadeTableData.getTableType() == 2) {
                        modifyIncrementTableDetailRepository.deleteByModifyIdAndTable(cascadeTableData.getModifyId(),
                                cascadeTableData.getIncreTable());
                    }
                }
            }
        }

        // kim 通知 审批人 和 提交人
        String clickUrl = String.format(apiPropertiesConfig.getCasServiceName() + DAS_INCREMENT_CLICKURL, adDasModify.getId());
        log.info("clickUrl={}", clickUrl);
        String modifyStatus = AdDasModifyStatusEnum.APPROVING.getDesc();
        sendAdDasModifyMsg(increModifyParam.getModifyName(), clickUrl,
                modifyStatus, adDasUserView.getUserName(), increModifyParam.getApprover());
    }

    private void addIncrementModify(AdDasModify adDasModify, AdDasPlatformIncreModifyParam increModifyParam,
                                    AdDasUserView adDasUserView, Map<String, AdDasPlatformIncreTableSchemaView> tableSchemaViewMap) {

        // 后写入 ad_das_modify_increment_table_detail 表
        AdDasModifyIncrementTableDetail adDasModifyIncrementTableDetail = new AdDasModifyIncrementTableDetail();
        adDasModifyIncrementTableDetail.setModifyId(adDasModify.getId());
        adDasModifyIncrementTableDetail.setIncreId(increModifyParam.getIncreStreamId());
        adDasModifyIncrementTableDetail.setDataSource(increModifyParam.getDataSource());
        adDasModifyIncrementTableDetail.setIncreTable(increModifyParam.getMainTable());
        adDasModifyIncrementTableDetail.setPbClassEnum(increModifyParam.getPbEnum());
        adDasModifyIncrementTableDetail.setTableType(1);

        String cascadedTable = ObjectMapperUtils.toJSON(increModifyParam.getCascadeTable());
        adDasModifyIncrementTableDetail.setCascadedTable(cascadedTable);
        String cascadeSchema = ObjectMapperUtils.toJSON(increModifyParam.getTableCascadeSchemaViewMap());
        adDasModifyIncrementTableDetail.setCascadeSchema(cascadeSchema);

        AdDasPlatformIncreTableSchemaView mainTableSchema = tableSchemaViewMap.get(increModifyParam.getMainTable());
        adDasModifyIncrementTableDetail.setPrimaryIdKey(mainTableSchema.getPrimaryIdKey());
        String tableColumn = ObjectMapperUtils.toJSON(mainTableSchema.getTableColumnViews());
        adDasModifyIncrementTableDetail.setTableColumn(tableColumn);

        if (increModifyParam.isDelTable()) {
            adDasModifyIncrementTableDetail.setOperatType(3);
        } else {
            adDasModifyIncrementTableDetail.setOperatType(mainTableSchema.getOperatorType());
        }

        adDasModifyIncrementTableDetail.setOperator(adDasUserView.getUserName());
        adDasModifyIncrementTableDetail.setApprover(increModifyParam.getApprover());
        adDasModifyIncrementTableDetail.setCreateTime(System.currentTimeMillis());
        adDasModifyIncrementTableDetail.setUpdateTime(System.currentTimeMillis());

        // 先写入主表
        modifyIncrementTableDetailRepository.insert(adDasModifyIncrementTableDetail);

        if (!CollectionUtils.isEmpty(increModifyParam.getCascadeTable())) {
            // 后写入级联表
            for (String cascadeTable : increModifyParam.getCascadeTable()) {

                AdDasModifyIncrementTableDetail cascadeModifyTableDetail = new AdDasModifyIncrementTableDetail();
                cascadeModifyTableDetail.setModifyId(adDasModify.getId());
                cascadeModifyTableDetail.setIncreId(increModifyParam.getIncreStreamId());
                cascadeModifyTableDetail.setDataSource(increModifyParam.getDataSource());
                cascadeModifyTableDetail.setIncreTable(cascadeTable);
                cascadeModifyTableDetail.setTableType(2);

                AdDasPlatformIncreTableSchemaView cascadeTableSchema = tableSchemaViewMap.get(cascadeTable);
                cascadeModifyTableDetail.setPrimaryIdKey(cascadeTableSchema.getPrimaryIdKey());
                String cascdeTableColumn = ObjectMapperUtils.toJSON(cascadeTableSchema.getTableColumnViews());
                cascadeModifyTableDetail.setTableColumn(cascdeTableColumn);
                cascadeModifyTableDetail.setPbClassEnum(increModifyParam.getPbEnum());

                if (increModifyParam.isDelTable()) {
                    cascadeModifyTableDetail.setOperatType(3);
                } else {
                    cascadeModifyTableDetail.setOperatType(cascadeTableSchema.getOperatorType());
                }

                cascadeModifyTableDetail.setApprover(increModifyParam.getApprover());
                cascadeModifyTableDetail.setOperator(adDasUserView.getUserName());
                cascadeModifyTableDetail.setCreateTime(System.currentTimeMillis());
                cascadeModifyTableDetail.setUpdateTime(System.currentTimeMillis());

                modifyIncrementTableDetailRepository.insert(cascadeModifyTableDetail);
            }
        }
    }

    @Override
    public void approveModifyIncrement(Long modifyId, Integer approveType, String reason, AdDasUserView adDasUserView) {

        AdDasModify adDasModify = adDasModifyRepository.queryById(modifyId);
        if (adDasModify == null) {
            throw AdDasServiceException.ofMessage(DATA_NOT_FOUND);
        }

        adDasModifyRepository.updateApprove(modifyId, adDasUserView.getUserName(), approveType,
                reason, System.currentTimeMillis());

        // kim 通知 审批人 和 提交人
        String clickUrl = String.format(apiPropertiesConfig.getCasServiceName() + DAS_INCREMENT_CLICKURL, adDasModify.getId());
        log.info("clickUrl={}", clickUrl);
        String modifyStatus ;
        if (approveType == AdDasModifyStatusEnum.APPROVED.getCode()) {
            modifyStatus = AdDasModifyStatusEnum.APPROVED.getDesc();
        } else {
            modifyStatus = AdDasModifyStatusEnum.REJECTED.getDesc();
        }
        sendAdDasModifyMsg(adDasModify.getModifyName(), clickUrl, modifyStatus,
                adDasModify.getOperator(), adDasUserView.getUserName());
    }

    @Override
    public void publishModifyIncrement(Long modifyId, AdDasUserView adDasUserView) {

        AdDasModify adDasModify = adDasModifyRepository.queryById(modifyId);
        if (adDasModify == null) {
            throw AdDasServiceException.ofMessage(DATA_NOT_FOUND);
        }

        // 查询增量变更 表详情数据
        List<AdDasModifyIncrementTableDetail> modifyIncrementTableDetails = modifyIncrementTableDetailRepository
                .queryByModifyId(modifyId);

        AdDasModifyIncrementTableDetail mainTableDetail = modifyIncrementTableDetails.stream()
                .filter(modifyIncrementTableDetail -> modifyIncrementTableDetail.getTableType() == 1)
                .findFirst().get();

        // 查询 现有线上数据
        AdDasIncrementTableDetail incrementTableDetail = incrementTableDetailRepository
                .queryByIncreIdAndTable(mainTableDetail.getIncreId(), mainTableDetail.getIncreTable());

        if (incrementTableDetail == null) {
            // 第一次接入数据
            // 将变更数据 写入 ad_das_increment_table_detail 表
            for (AdDasModifyIncrementTableDetail modifyIncrementTableDetail : modifyIncrementTableDetails) {
                AdDasIncrementTableDetail adDasIncrementTableDetail = new AdDasIncrementTableDetail();
                BeanUtils.copyProperties(modifyIncrementTableDetail, adDasIncrementTableDetail);
                incrementTableDetailRepository.insert(adDasIncrementTableDetail);
            }
        } else {

            List<String> originTableNames = Lists.newArrayList(mainTableDetail.getIncreTable());
            List<AdDasIncrementTableDetail> incrementTableDetails = Lists.newArrayList();
            // 查询是否存在 级联表
            String cascadedTable = incrementTableDetail.getCascadedTable();
            if (!StringUtils.isEmpty(cascadedTable)) {
                List<String> cascadeTableList = ObjectMapperUtils
                        .fromJSON(incrementTableDetail.getCascadedTable(), List.class, String.class);

                List<AdDasIncrementTableDetail> cascadeIncrementTableDetails = incrementTableDetailRepository
                        .queryByIncreIdAndTables(mainTableDetail.getIncreId(), cascadeTableList);

                incrementTableDetails.addAll(cascadeIncrementTableDetails);
                originTableNames.addAll(cascadeTableList);
            }

            if (mainTableDetail.getOperatType() == 3) {
                // 先删除数据
                log.info("deleteByIncreIdAndTables() increId={}, originTableNames={}",
                        mainTableDetail.getIncreId(), originTableNames);
                incrementTableDetailRepository.deleteByIncreIdAndTables(mainTableDetail.getIncreId(), originTableNames);

            } else {
                // 先删除数据
                log.info("deleteByIncreIdAndTables() increId={}, originTableNames={}",
                        mainTableDetail.getIncreId(), originTableNames);
                incrementTableDetailRepository.deleteByIncreIdAndTables(mainTableDetail.getIncreId(), originTableNames);

                // 将变更数据 写入 ad_das_increment_table_detail 表
                for (AdDasModifyIncrementTableDetail modifyIncrementTableDetail : modifyIncrementTableDetails) {
                    AdDasIncrementTableDetail adDasIncrementTableDetail = new AdDasIncrementTableDetail();
                    BeanUtils.copyProperties(modifyIncrementTableDetail, adDasIncrementTableDetail);
                    incrementTableDetailRepository.insert(adDasIncrementTableDetail);
                }
            }

            // 先确认 记录表中是否有写入
            List<AdDasModifyIncrementTableRecord>  modifyIncrementTableRecords = modifyIncrementTableRecordRepository
                    .listByModifyId(modifyId);
            if (CollectionUtils.isEmpty(modifyIncrementTableRecords)) {
                modifyIncrementTableRecordRepository.delByModifyId(modifyId);
            }

            // 数据写入 增量变更记录表，方便回滚
            for (AdDasIncrementTableDetail adDasIncrementTableDetail : incrementTableDetails) {
                AdDasModifyIncrementTableRecord adDasModifyIncrementTableRecord = new AdDasModifyIncrementTableRecord();
                BeanUtils.copyProperties(adDasIncrementTableDetail, adDasModifyIncrementTableRecord);
                modifyIncrementTableRecordRepository.insert(adDasModifyIncrementTableRecord);
            }
        }

        // 最终更新
        adDasModifyRepository.updateStatus(modifyId, AdDasModifyStatusEnum.PUBLISH_SUC.getCode(), adDasUserView.getUserName(), System.currentTimeMillis());
        String clickUrl = String.format(DAS_INCREMENT_CLICKURL, adDasModify.getId());
        log.info("clickUrl={}", clickUrl);
        String modifyStatus = AdDasModifyStatusEnum.PUBLISH_SUC.getDesc();
        sendAdDasModifyMsg(adDasModify.getModifyName(), clickUrl, modifyStatus,
                adDasUserView.getUserName(), adDasModify.getApprover());
    }

    @Override
    public void terminateModifyIncrement(Long modifyId, AdDasUserView adDasUserView) {

        AdDasModify adDasModify = adDasModifyRepository.queryById(modifyId);
        if (adDasModify == null) {
            throw AdDasServiceException.ofMessage(DATA_NOT_FOUND);
        }

        adDasModifyRepository.updateStatus(modifyId, AdDasModifyStatusEnum.TERMINATION.getCode(),
                adDasUserView.getUserName(), System.currentTimeMillis());

        String clickUrl = String.format(apiPropertiesConfig.getCasServiceName() + DAS_INCREMENT_CLICKURL, adDasModify.getId());
        log.info("clickUrl={}", clickUrl);
        String modifyStatus = AdDasModifyStatusEnum.TERMINATION.getDesc();
        sendAdDasModifyMsg(adDasModify.getModifyName(), clickUrl, modifyStatus,
                adDasUserView.getUserName(), adDasModify.getApprover());
    }

    @Override
    public void rollbackModifyIncrement(Long modifyId, AdDasUserView adDasUserView) {

        AdDasModify adDasModify = adDasModifyRepository.queryById(modifyId);
        if (adDasModify == null) {
            throw AdDasServiceException.ofMessage(DATA_NOT_FOUND);
        }

        // 查询 增量变更记录表， 回滚
        List<AdDasModifyIncrementTableRecord>  modifyIncrementTableRecords = modifyIncrementTableRecordRepository
                .listByModifyId(modifyId);

        if (CollectionUtils.isEmpty(modifyIncrementTableRecords)) {

            // 删除 ad_das_increment_table_detail 表 数据
            incrementTableDetailRepository.deleteByIncreId(modifyId);
        } else {
            // 更新 写入 ad_das_increment_table_detail 表
            for (AdDasModifyIncrementTableRecord modifyIncrementTableRecord : modifyIncrementTableRecords) {
                AdDasIncrementTableDetail adDasIncrementTableDetail = new AdDasIncrementTableDetail();
                BeanUtils.copyProperties(modifyIncrementTableRecord, adDasIncrementTableDetail);
                incrementTableDetailRepository.insert(adDasIncrementTableDetail);
            }
        }

        adDasModifyRepository.updateStatus(modifyId, AdDasModifyStatusEnum.ROLLBACK_SUC.getCode(),
                adDasUserView.getUserName(), System.currentTimeMillis());

        // TODO kim 通知
        String clickUrl = String.format(apiPropertiesConfig.getCasServiceName() + DAS_INCREMENT_CLICKURL, adDasModify.getId());
        log.info("clickUrl={}", clickUrl);
        String modifyStatus = AdDasModifyStatusEnum.ROLLBACK_SUC.getDesc();
        sendAdDasModifyMsg(adDasModify.getModifyName(), clickUrl, modifyStatus,
                adDasUserView.getUserName(), adDasModify.getApprover());
    }

    @Override
    public void mod(AdDasEditIncrementStreamParam param) {

        AdDasIncrement adDasIncrement = new AdDasIncrement();
        AdDasBeanCoreUtils.copyPropertiesIgnoreNull(adDasIncrement, param);
        adDasIncrement.setUpdateTime(System.currentTimeMillis());

        if (param.getId() == 0 || param.getId() == null) {
            adDasIncrement.setCreateTime(System.currentTimeMillis());
            adDasIncrementRepository.insert(adDasIncrement);
            return;
        }

        adDasIncrement.setId(param.getId());
        adDasIncrementRepository.update(adDasIncrement);
    }

    @Override
    public void deleteById(Long dataId) {

        adDasIncrementRepository.deleteById(dataId);
    }

    @Override
    public void modServiceInfo(AdDasEditIncrementServiceParam param) {

        AdDasIncrementServiceInfo incrementServiceInfo = new AdDasIncrementServiceInfo();
        AdDasBeanCoreUtils.copyPropertiesIgnoreNull(incrementServiceInfo, param);

        if (param.getId() == null || param.getId() == 0) {

            incrementServiceInfoRepository.insert(incrementServiceInfo);
            return;
        }

        incrementServiceInfo.setId(param.getId());
        incrementServiceInfoRepository.update(incrementServiceInfo);
    }

    @Override
    public void deleteServiceInfoById(Long dataId) {
        incrementServiceInfoRepository.deleteById(dataId);
    }

}
