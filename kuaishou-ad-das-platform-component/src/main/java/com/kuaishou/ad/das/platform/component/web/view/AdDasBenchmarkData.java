package com.kuaishou.ad.das.platform.component.web.view;

import java.io.Serializable;

import lombok.Data;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-09-26
 */
@Data
public class AdDasBenchmarkData implements Serializable {

    private static final long serialVersionUID = -314332982402017294L;

    private String  id;

    private String dataJson;
}
