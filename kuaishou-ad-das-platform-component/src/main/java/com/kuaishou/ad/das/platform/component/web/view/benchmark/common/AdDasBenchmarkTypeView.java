package com.kuaishou.ad.das.platform.component.web.view.benchmark.common;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author wangkai <wangkai14@kuaishou.com>
 * Created on 2020-12-31
 */
@Data
@ApiModel
public class AdDasBenchmarkTypeView {

    @ApiModelProperty(value = "基准id")
    private Long benchmarkId;

    @ApiModelProperty(value = "基准类型名称")
    private String benchmarkName;


}
