package com.kuaishou.ad.das.platform.component.web;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-09-26
 */
@Data
public class PageData {
    @ApiModelProperty(value = "单页条数")
    private Integer pageSize;
    @ApiModelProperty(value = "页码")
    private Integer pageNum;
    @ApiModelProperty(value = "总条数")
    private Long totalCount;

}
