package com.kuaishou.ad.das.platform.component.web.param;

import java.io.Serializable;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-08-01
 */
@Data
public class AdDasEditBenchmarkStreamParam implements Serializable {

    private static final long serialVersionUID = -7502725393374308699L;

    @ApiModelProperty(value = "基准id", allowEmptyValue = true)
    private Long id;

    @ApiModelProperty(value = "基准流code, 用于生成基准流名称 和 hdfsPath", allowEmptyValue = true)
    private String benchmarkCode;

    @ApiModelProperty(value = "基准流名称", allowEmptyValue = true)
    private String benchmarkName;

    @ApiModelProperty(value = "基准流描述", allowEmptyValue = true)
    private String benchmarkDesc;

    @ApiModelProperty(value = "集群server名称", allowEmptyValue = true)
    private String clusterName;

    @ApiModelProperty(value = "数据流组id", allowEmptyValue = true)
    private Long dataStreamGroupId;

    @ApiModelProperty(value = "HDFS路径名", allowEmptyValue = true)
    private String hdfsPath;

    @ApiModelProperty(value = "操作人邮箱前缀", allowEmptyValue = true)
    private String operatorEmailPrefix;

    @ApiModelProperty(value = "是否删除", allowEmptyValue = true)
    private Integer isDelete;
}
