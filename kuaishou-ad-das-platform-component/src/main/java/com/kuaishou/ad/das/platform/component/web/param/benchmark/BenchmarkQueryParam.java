package com.kuaishou.ad.das.platform.component.web.param.benchmark;

import java.util.List;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author Zhang Jialin <zhangjialin@kuaishou.com>
 * Created on 2021-05-18
 */
@Data
public class BenchmarkQueryParam {
    @ApiModelProperty(value = "筛选项-数据流组id", allowEmptyValue = true)
    private Long dataStreamGroupId;
    @ApiModelProperty(value = "筛选项-基准id", allowEmptyValue = true)
    private List<Long> benchmarkIds;
    @ApiModelProperty(value = "筛选项-数据源名称", allowEmptyValue = true)
    private List<String> dataSourceNames;
    @ApiModelProperty(value = "筛选项-数据表名称", allowEmptyValue = true)
    private List<String> tables;
    @ApiModelProperty(value = "筛选项-HDFS路径", allowEmptyValue = true)
    private List<String> hdfsPaths;
    @ApiModelProperty(value = "单页条数", allowEmptyValue = true)
    private Integer pageSize;
    @ApiModelProperty(value = "页码", notes = "传入-1查询全部", allowEmptyValue = true)
    private Integer pageNum;
}
