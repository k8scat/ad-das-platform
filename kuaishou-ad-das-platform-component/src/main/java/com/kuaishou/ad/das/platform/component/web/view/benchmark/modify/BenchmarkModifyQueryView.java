package com.kuaishou.ad.das.platform.component.web.view.benchmark.modify;

import java.util.List;

import com.kuaishou.ad.das.platform.component.web.PageData;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author Zhang Jialin <zhangjialin@kuaishou.com>
 * Created on 2021-05-26
 */
@Data
public class BenchmarkModifyQueryView extends PageData {
    @ApiModelProperty(value = "数据列表")
    private List<BenchmarkModifyQueryItemView> dataList;
}
