package com.kuaishou.ad.das.platform.component.web.view;

import java.util.List;

import lombok.Data;

/**
 * 增量管理服务发布信息
 *
 * @author liutao <liutao06@kuaishou.com>
 * Created on 2021-01-05
 */
@Data
public class AdDasIncrementPublishInfoView {



    private List<AdDasIncrementPublishDetailInfo> detailInfoList;

}
