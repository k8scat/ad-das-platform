package com.kuaishou.ad.das.platform.component.biz.benchmark;

import com.kuaishou.ad.das.platform.component.web.param.AdDasEditBenchmarkStreamParam;
import com.kuaishou.ad.das.platform.component.web.param.AdDasModifyBenchmarkParam;
import com.kuaishou.ad.das.platform.component.web.view.AdDasModifyBenchmarkDataView;
import com.kuaishou.ad.das.platform.core.benchmark.model.AdDasBenchmarkMainTableView;
import com.kuaishou.ad.das.platform.dal.model.AdDasBenchmark;
import com.kuaishou.ad.das.platform.utils.model.AdDasUserView;

/**
 * @author wangkai <wangkai14@kuaishou.com>
 * Created on 2020-12-28
 */
public interface AdDasBenchmarkModifyBiz {

    Long createBenchmark(AdDasUserView adDasUserView, AdDasModifyBenchmarkParam param);

    void approveModifyBenchmark(AdDasUserView adDasUserView, Integer approveType, String reason, Long modifyId);

    void discardBenchmark(AdDasUserView adDasUserView, Long modifyId);

    void rollbackBenchmark(AdDasUserView adDasUserView, Long modifyId);

    void submitBenchmark(AdDasUserView adDasUserView, AdDasModifyBenchmarkParam param);

    void publishModifyBenchmark(AdDasUserView adDasUserView, Integer approveType, Long modifyId);

    Long createBenchmarkTask(AdDasUserView adDasUserView, AdDasModifyBenchmarkParam param);

    AdDasModifyBenchmarkDataView queryBenchmark(Long modifyId);

    AdDasModifyBenchmarkDataView queryPreviousBenchmark(Long modifyId);

    AdDasBenchmarkMainTableView queryCurBenchmark(Long benchmarkId, String mainTable, String dataSource);

    AdDasModifyBenchmarkDataView queryBenchmarkByTable(Long benchmarkId,String mainTable,String dataSource);

    void mod(AdDasEditBenchmarkStreamParam param);

    void deleteById(Long dataId);
}
