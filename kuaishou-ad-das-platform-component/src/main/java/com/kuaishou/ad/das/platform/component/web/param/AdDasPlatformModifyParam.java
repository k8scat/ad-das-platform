package com.kuaishou.ad.das.platform.component.web.param;

import java.io.Serializable;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-06-14
 */
@Data
public class AdDasPlatformModifyParam extends AdDasPageSearchParam implements Serializable {

    @ApiModelProperty(value = "开始时间", allowEmptyValue = true)
    private Long startTime;

    @ApiModelProperty(value = "结束时间", allowEmptyValue = true)
    private Long endTime;

    @ApiModelProperty(value = "变更名称", allowEmptyValue = true)
    private String modifyName;

    @ApiModelProperty(value = "变更类型", allowEmptyValue = true)
    private Integer modifyType;

    @ApiModelProperty(value = "变更状态", allowEmptyValue = true)
    private Integer modifyStatus;

    @ApiModelProperty(value = "表", allowEmptyValue = true)
    private String table;

    @ApiModelProperty(value = "操作人", allowEmptyValue = true)
    private String operator;

    @ApiModelProperty(value = "增量流id", allowEmptyValue = true)
    private Long increId;
}
