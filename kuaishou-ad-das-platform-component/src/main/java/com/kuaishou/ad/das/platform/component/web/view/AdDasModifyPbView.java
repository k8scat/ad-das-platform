package com.kuaishou.ad.das.platform.component.web.view;

import java.io.Serializable;

import lombok.Data;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-12-06
 */
@Data
public class AdDasModifyPbView implements Serializable {

    private static final long serialVersionUID = 5055306492911355968L;

    private String modifyName;

    private String modifyType;

    private Integer modifyStatus;

    private String operator;

    private String approver;

    private String reason;

    private AdDasModifyPbDetailView view;

}
