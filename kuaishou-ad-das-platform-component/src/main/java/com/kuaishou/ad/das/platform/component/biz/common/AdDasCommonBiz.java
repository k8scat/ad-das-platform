package com.kuaishou.ad.das.platform.component.biz.common;

import static com.kuaishou.ad.das.platform.utils.exception.ErrorCode.PB_CLASS_NOT_FOUND;
import static com.kuaishou.ad.das.platform.utils.exception.ErrorCode.PB_PARAM_ERROR;
import static com.kuaishou.ad.das.platform.utils.exception.ErrorCode.USER_NOT_LOGIN_ERROR;

import java.lang.reflect.Method;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.util.StringUtils;

import com.google.common.collect.Lists;
import com.google.protobuf.Descriptors;
import com.google.protobuf.Message;
import com.kuaishou.ad.das.platform.component.web.view.AdDasSchemaPbClassView;
import com.kuaishou.ad.das.platform.core.common.pbloader.AdDasClassLoaderHelper;
import com.kuaishou.ad.das.platform.utils.model.AdDasUserView;
import com.kuaishou.old.exception.ServiceException;

import lombok.extern.slf4j.Slf4j;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-11-29
 */
@Lazy
@Slf4j
public class AdDasCommonBiz {

    @Autowired
    protected AdDasClassLoaderHelper adDasClassLoaderHelper;

    protected void checkPbVersion(String pbVersion) {

        boolean flag = pbVersion.startsWith("kuaishou-ad-new-biz-proto-") && pbVersion.endsWith(".jar");
        if (!flag) {
            throw ServiceException.ofMessage(PB_PARAM_ERROR.getCode(), PB_PARAM_ERROR.getMsg());
        }
    }

    protected void checkUser(AdDasUserView adDasUserView) {

        if (adDasUserView == null || StringUtils.isEmpty(adDasUserView.getUserName())) {
            throw ServiceException.ofMessage(USER_NOT_LOGIN_ERROR.getCode(), USER_NOT_LOGIN_ERROR.getMsg());
        }
    }

    protected List<AdDasSchemaPbClassView> reflectPbClassView(Class subClaz) {

        List<AdDasSchemaPbClassView> result = Lists.newArrayList();

        try {
            // 首先根据 pb message class 名称得到 Class 对象。（坑1：注意内部类连接符为$,如 package_name.class_name$inner_class_name,
            // 另外注意脚本启动的时候要注意对 $ 转义，否则 shell 会以为 $inner_class_name 为变量）
            Method method = subClaz.getMethod("newBuilder");

            Object obj = method.invoke(null, null);
            Message.Builder msgBuilder = (Message.Builder) obj;       // 得到 builder
            Descriptors.Descriptor descriptor = msgBuilder.getDescriptorForType();   // 得到 descript

            List<Descriptors.FieldDescriptor> fieldDescriptors = descriptor.getFields();

            for (Descriptors.FieldDescriptor fieldDescriptor : fieldDescriptors) {

                AdDasSchemaPbClassView protoMsgView = new AdDasSchemaPbClassView();

                protoMsgView.setColumnFullName(fieldDescriptor.getFullName());
                protoMsgView.setColumnName(fieldDescriptor.getName());
                protoMsgView.setColumnJavaType(fieldDescriptor.getJavaType().toString());
                protoMsgView.setColumnPbType(fieldDescriptor.getType().toString());

                if (Descriptors.FieldDescriptor.JavaType.ENUM.equals(fieldDescriptor.getJavaType())) {
                    protoMsgView.setColumnEnumType(fieldDescriptor.getEnumType().getFullName());
                }

                if (fieldDescriptor.getJavaType() == Descriptors.FieldDescriptor.JavaType.MESSAGE) {
                    protoMsgView.setDefaultValue("");
                } else {
                    protoMsgView.setDefaultValue(fieldDescriptor.getDefaultValue().toString());
                }

                result.add(protoMsgView);
            }
        } catch (Exception e) {
            log.error("reflectPbClassView() occur error: class={}", subClaz, e);
        }

        return result;
    }

    protected void checkClassWithPbJar(String pbJarVersion, String pbClassName) {

        try {
           /* Class clz = adDasClassLoaderHelper.loadClassWithLoadJar(pbJarVersion, pbClassName);

            if (clz == null) {
                throw ServiceException.ofMessage(PB_CLASS_NOT_FOUND.getCode(), PB_CLASS_NOT_FOUND.getMsg());
            }*/
        } catch (Exception e) {
            log.error("checkClassWithPbJar() occur error! pbJarVersion={}, pbClassName={}",
                    pbJarVersion, pbClassName, e);
            throw ServiceException.ofMessage(PB_CLASS_NOT_FOUND.getCode(), PB_CLASS_NOT_FOUND.getMsg());
        }
    }
}
