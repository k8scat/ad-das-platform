package com.kuaishou.ad.das.platform.component.web.view.benchmark.common;

import java.io.Serializable;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author wangkai <wangkai14@kuaishou.com>
 * Created on 2020-12-24
 */
@Data
public class AdDasBenchmarkStatusView implements Serializable {
    private static final long serialVersionUID = -991259570028041121L;

    @ApiModelProperty(value = "状态编码： 0-全部")
    private Integer statusCode;

    @ApiModelProperty(value = "状态名称")
    private String statusName;
}
