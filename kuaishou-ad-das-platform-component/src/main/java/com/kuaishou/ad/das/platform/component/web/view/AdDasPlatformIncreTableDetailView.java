package com.kuaishou.ad.das.platform.component.web.view;

import java.util.List;
import java.util.Map;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-05-24
 */
@Data
public class AdDasPlatformIncreTableDetailView {

    @ApiModelProperty(value = "数据流组id")
    protected Long dataStreamGroupId;

    @ApiModelProperty(value = "数据流组名称")
    protected String dataStreamGroup;

    @ApiModelProperty(value = "数据流组描述")
    private String dataStreamGroupDesc;

    @ApiModelProperty(value = "增量流id")
    protected Long increStreamId;

    @ApiModelProperty(value = "增量流名称")
    protected String increStreamName;

    @ApiModelProperty(value = "增量流类型: 1-online 2-preOnline", allowEmptyValue = true)
    protected Integer increStreamType;

    @ApiModelProperty(value = "数据源")
    protected String dataSource;

    @ApiModelProperty(value = "增量流主表")
    protected String mainTable;

    @ApiModelProperty(value = "增量流主表pb类枚举")
    protected String pbEnum;

    @ApiModelProperty(value = "增量流级联表List")
    protected List<String> cascadeTable;

    @ApiModelProperty(value = "表字段关系")
    protected Map<String, AdDasPlatformIncreTableSchemaView> tableSchemaViewMap;

    @ApiModelProperty(value = "表级联映射关系", allowEmptyValue = true)
    protected Map<String, AdDasPlatformIncreTableCascadeSchemaView> tableCascadeSchemaViewMap;

}
