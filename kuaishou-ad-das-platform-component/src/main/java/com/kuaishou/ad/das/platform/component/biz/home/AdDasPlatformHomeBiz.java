package com.kuaishou.ad.das.platform.component.biz.home;

import com.kuaishou.ad.das.platform.component.web.view.AdDasPlatformHomeView;
import com.kuaishou.ad.das.platform.utils.model.AdDasUserView;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2021-08-06
 */
public interface AdDasPlatformHomeBiz {

    AdDasPlatformHomeView platformHome(AdDasUserView adDasUserView);
}
