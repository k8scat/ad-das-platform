package com.kuaishou.ad.das.platform.component.web.param;

import java.io.Serializable;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author caoyinhang <caoyinhang@kuaishou.com>
 * Created on 2021-07-08
 */
@Data
@ApiModel
public class AdDasIncrementRecondTableDataReSendParam implements Serializable {

    private static final long serialVersionUID = 4855232794975395298L;

    @ApiModelProperty(value = "数据源列表")
    private List<String> dataSourceList;

    @ApiModelProperty(value = "消费组名称列表")
    private List<String> consumerGroupList;

    @ApiModelProperty(value = "增量topic列表")
    private List<String> topicList;

    @ApiModelProperty(value = "操作人列表")
    private List<String> operatorList;

    @ApiModelProperty(value = "操作状态列表")
    private List<String> statusList;


}
