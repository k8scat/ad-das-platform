package com.kuaishou.ad.das.platform.component.biz.increment;

import java.util.List;

import com.kuaishou.ad.das.platform.component.web.param.AdDasPlatformModifyParam;
import com.kuaishou.ad.das.platform.component.web.view.AdDasIncrementPublishInfoView;
import com.kuaishou.ad.das.platform.component.web.view.AdDasIncrementServiceView;
import com.kuaishou.ad.das.platform.component.web.view.AdDasModifyDataListView;
import com.kuaishou.ad.das.platform.component.web.view.AdDasPlatformIncreDetailView;
import com.kuaishou.ad.das.platform.component.web.view.AdDasPlatformIncreSearchView;
import com.kuaishou.ad.das.platform.component.web.view.AdDasPlatformIncreTableDetailView;
import com.kuaishou.ad.das.platform.component.web.view.AdDasPlatformModifyIncrementView;


/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-12-23
 */
public interface AdDasIncrementModifyQueryBiz {

    AdDasIncrementServiceView getAdDasIncrementServiceView(String serviceName, Long modifyId);

    AdDasIncrementPublishInfoView incrementPublishInfoDetail(Long modifyId);

    /**
     * 数据流增量搜索列表
     * @param dataStreamGroupId
     * @param dataSource
     * @param table
     * @param dataTopic
     * @return
     */
    AdDasPlatformIncreSearchView increSearch(Long dataStreamGroupId, String dataSource,
                                                   String table, String dataTopic);

    /**
     * 增量详情
     * @param increStreamId
     * @param increStreamType
     * @return
     */
    AdDasPlatformIncreDetailView increDetail(Long increStreamId,
                                                   Integer increStreamType);

    /**
     * 增量表详情
     * @param increStreamId
     * @param increStreamType
     * @return
     */
    AdDasPlatformIncreTableDetailView increTableDetail(Long increStreamId,
                                                             Integer increStreamType, String mainTable);


    /**
     * 查询增量变更详情
     * @param modifyId
     * @param modifyType
     * @return
     */
    AdDasPlatformModifyIncrementView incrementModifyData(Long modifyId, Integer modifyType);

    /**
     * 增量变更记录
     * @param modifyParam
     * @return
     */
    AdDasModifyDataListView increRecord(AdDasPlatformModifyParam modifyParam);


    /**
     * 查询增量流中的 topic 列表
     * @return
     */
    List<String> getIncreTopics();
}
