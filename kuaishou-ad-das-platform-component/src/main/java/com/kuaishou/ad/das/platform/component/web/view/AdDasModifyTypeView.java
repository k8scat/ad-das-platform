package com.kuaishou.ad.das.platform.component.web.view;

import java.io.Serializable;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-11-24
 */
@Data
public class AdDasModifyTypeView implements Serializable {

    private static final long serialVersionUID = 3942766905913237557L;

    @ApiModelProperty(value = "变更类型编号")
    private Integer modifyType;

    @ApiModelProperty(value = "变更类型名称")
    private String modifyTypeName;
}
