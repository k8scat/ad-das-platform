package com.kuaishou.ad.das.platform.component.web.param.datastream;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author Zhang Jialin <zhangjialin@kuaishou.com>
 * Created on 2021-05-19
 */
@Data
@ApiModel
public class DataStreamGroupModifyParam {
    @ApiModelProperty(value = "数据流组id")
    private Long dataStreamGroupId;
    @ApiModelProperty(value = "数据流组名称", notes = "不传则不进行修改", allowEmptyValue = true)
    private String dataStreamGroupName;
    @ApiModelProperty(value = "数据流组介绍", notes = "不传则不进行修改", allowEmptyValue = true)
    private String dataStreamGroupDesc;
    @ApiModelProperty(value = "负责人的邮箱前缀", notes = "不传则不进行修改", allowEmptyValue = true)
    private String ownerEmailPrefix;
    @ApiModelProperty(value = "负责人的邮箱中文名", notes = "不传则不进行修改", allowEmptyValue = true)
    private String ownerName;
    @ApiModelProperty(value = "是否删除", notes = "不传则不进行修改", allowEmptyValue = true)
    private Integer isDelete;
}
