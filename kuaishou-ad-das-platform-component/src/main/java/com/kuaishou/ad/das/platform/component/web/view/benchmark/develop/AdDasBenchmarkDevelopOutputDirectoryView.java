package com.kuaishou.ad.das.platform.component.web.view.benchmark.develop;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author wangkai <wangkai14@kuaishou.com>
 * Created on 2020-12-24
 */
@Data
public class AdDasBenchmarkDevelopOutputDirectoryView {

    private static final long serialVersionUID = 7403994509691831860L;

    @ApiModelProperty(value = "输出目录类型")
    private Integer outputDirectoryType;

    @ApiModelProperty(value = "输出目录名称")
    private String  outputDirectoryName;
}
