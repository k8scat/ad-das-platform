package com.kuaishou.ad.das.platform.component.web.view;

import java.io.Serializable;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-11-22
 */
@Data
public class AdDasSchemaServiceView implements Serializable {

    private static final long serialVersionUID = 8417310388268119109L;

    @ApiModelProperty(value = "服务名")
    private String serviceName;

    @ApiModelProperty(value = "PB包版本")
    private String pbJarVersion;

    @ApiModelProperty(value = "操作人")
    private String operator;

    private Long createTime;

    private Long updateTime;
}
