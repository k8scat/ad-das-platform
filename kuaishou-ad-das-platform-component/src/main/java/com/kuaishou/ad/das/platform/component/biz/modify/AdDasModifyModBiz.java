package com.kuaishou.ad.das.platform.component.biz.modify;


import com.kuaishou.ad.das.platform.component.web.param.AdDasModifyPbParam;
import com.kuaishou.ad.das.platform.component.web.param.AdDasPublishPbParam;
import com.kuaishou.ad.das.platform.component.web.view.AdDasModifyDataView;
import com.kuaishou.ad.das.platform.utils.model.AdDasUserView;

/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-12-06
 */
public interface AdDasModifyModBiz {

    /**
     * 1.写入 ad_das_modify 表
     * 2.写入 ad_das_modify_pb_msg表
     * @param param
     * @return
     */
    AdDasModifyDataView addModifyPb(AdDasUserView adDasUserView, AdDasModifyPbParam param);

    /**
     * 审批 PB变更工单 更新 ad_das_modify 表
     * @param adDasUserView
     * @param approveType
     * @param reason
     * @return
     */
    void approveModifyPb(AdDasUserView adDasUserView, Long modifyId, Integer approveType, String reason);

    /**
     * 发布PB变更工单
     * 0. 更新ad_das_modify表中的工单状态为 发布中, 初始化写入ad_das_modify_service_pb_record
     * 1.触发 zk上 common/pb/das服务/prepb 的节点值变化
     * 例如：
     *   common/pb/ad.das.api/curpb
     *                       /prepb
     *                       /suc
     * 2. 服务实例切换pb 成功后 会写 /suc 节点表示更新完成， 另外会有定时任务监听扫描服务实例状态， 写入 ad_das_modify_service_pb_record
     *   当 /suc节点下的包含所有实例的状态后 更新 /curpb, 并更新 ad_das_modify
     *
     * @param adDasUserView
     * @param param
     * @return
     */
    void publishModifyPb(AdDasUserView adDasUserView, AdDasPublishPbParam param);

    /**
     * 终止PB变更工单 更新 ad_das_modify 表
     * @param adDasUserView
     * @param param
     * @return
     */
    void terminateModifyPb(AdDasUserView adDasUserView, AdDasPublishPbParam param);

    /**
     * 回滚PB变更工单
     * @param adDasUserView
     * @param param
     */
    void rollBackModifyPb(AdDasUserView adDasUserView, AdDasPublishPbParam param);

}
