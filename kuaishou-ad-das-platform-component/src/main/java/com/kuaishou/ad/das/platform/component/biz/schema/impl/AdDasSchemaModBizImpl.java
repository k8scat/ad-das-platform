package com.kuaishou.ad.das.platform.component.biz.schema.impl;

import static com.kuaishou.ad.das.platform.core.common.pbloader.AdDasClassLoaderHelper.getPbClassPrefix;
import static com.kuaishou.ad.das.platform.utils.exception.ErrorCode.DATA_EXISTED_ERROR;
import static com.kuaishou.ad.das.platform.utils.exception.ErrorCode.PB_CLASS_NOT_FOUND;
import static com.kuaishou.ad.das.platform.utils.kconf.AdDasPlatformKconf.adDasPlatformProtoEnumVersionConfig;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import com.google.common.collect.Lists;
import com.kuaishou.ad.das.platform.component.biz.common.AdDasCommonBiz;
import com.kuaishou.ad.das.platform.component.biz.schema.AdDasSchemaModBiz;
import com.kuaishou.ad.das.platform.component.web.param.AdDasPbClassColumnParam;
import com.kuaishou.ad.das.platform.component.web.view.AdDasSchemaPbClassView;
import com.kuaishou.ad.das.platform.component.web.view.AdDasSchemaPbDetailView;
import com.kuaishou.ad.das.platform.core.common.daszk.model.AdDasPlatformProtoSchema;
import com.kuaishou.ad.das.platform.core.common.pbloader.AdDasClassLoaderHelper;
import com.kuaishou.ad.das.platform.dal.model.AdDasPb;
import com.kuaishou.ad.das.platform.dal.model.AdDasPbClass;
import com.kuaishou.ad.das.platform.dal.model.AdDasPbClassColumn;
import com.kuaishou.ad.das.platform.dal.repository.AdDasPbClassColumnRepository;
import com.kuaishou.ad.das.platform.dal.repository.AdDasPbClassRepository;
import com.kuaishou.ad.das.platform.dal.repository.AdDasPbRepository;
import com.kuaishou.ad.das.platform.utils.AdDasBeanCoreUtils;
import com.kuaishou.ad.das.platform.utils.dasenum.AdDasStatusEnum;
import com.kuaishou.ad.das.platform.utils.exception.AdDasServiceException;
import com.kuaishou.ad.das.platform.utils.model.AdDasUserView;

import lombok.extern.slf4j.Slf4j;


/**
 * @author chenjiaxiong <chenjiaxiong@kuaishou.com>
 * Created on 2020-11-27
 */
@Lazy
@Slf4j
@Service
public class AdDasSchemaModBizImpl extends AdDasCommonBiz implements AdDasSchemaModBiz {

    @Autowired
    private AdDasPbRepository adDasPbRepository;

    @Autowired
    private AdDasPbClassRepository adDasPbClassRepository;

    @Autowired
    private AdDasPbClassColumnRepository adDasPbClassColumnRepository;

    @Autowired
    protected AdDasClassLoaderHelper adDasClassLoaderHelper;

    @Override
    public void updatePbJar(AdDasUserView adDasUserView, String pbJarVersion,
                            String pbEnumVersion, String pbCommonVersion, Integer autoAddPbClass) {

        checkUser(adDasUserView);

        checkPbVersion(pbJarVersion);

        // query
        AdDasPb adDasPbs = adDasPbRepository.queryByVersion(pbJarVersion);
        log.info("updatePbJar() adDasPbs={}", adDasPbs);

        if (adDasPbs != null) {
            throw AdDasServiceException.ofMessage(DATA_EXISTED_ERROR.getCode(), DATA_EXISTED_ERROR.getMsg());
        }

        // 校验现有 pbClass & pbClassColumn是否支持
        AdDasPb adDasPbListCur = adDasPbRepository.queryLastPb();
        log.info("updatePbJar() adDasPbListCur={}", adDasPbListCur);
        String curPbVersion = null;

        if (adDasPbListCur != null) {
            curPbVersion = adDasPbListCur.getPbVersion();
        }

        log.info("updatePbJar() curPbVersion={}", curPbVersion);
        // TODO 换成批量的方式
        // 将上一个版本的 pbClass & pbClassColumn 复制写入 ad_das_pb_class 和 ad_das_pb_class_column
        if (!StringUtils.isEmpty(curPbVersion) && autoAddPbClass == 1) {
            List<AdDasPbClass> adDasPbClasses = adDasPbClassRepository
                    .query(curPbVersion, AdDasStatusEnum.DEFAULT.getCode());

            log.info("updatePbJar() adDasPbClasses={}", adDasPbClasses);

            if (!CollectionUtils.isEmpty(adDasPbClasses)) {
                String finalCurPbVersion = curPbVersion;
                adDasPbClasses.forEach(adDasPbClass -> {
                    Long timestamp = System.currentTimeMillis();
                    String pbClass = adDasPbClass.getPbClass();
                    AdDasPbClass adDasPbClassNew = new AdDasPbClass();
                    adDasPbClassNew.setPbVersion(pbJarVersion);
                    adDasPbClassNew.setPbClass(adDasPbClass.getPbClass());
                    adDasPbClassNew.setOperator(adDasUserView.getUserName());
                    adDasPbClassNew.setPbClassColumnCount(adDasPbClass.getPbClassColumnCount());
                    adDasPbClassNew.setPbClassStatus(AdDasStatusEnum.DEFAULT.getCode());
                    adDasPbClassNew.setCreateTime(timestamp);
                    adDasPbClassNew.setUpdateTime(timestamp);

                    log.info("updatePbJar() adDasPbClass={}", adDasPbClass);
                    adDasPbClassRepository.insert(adDasPbClassNew);

                    List<AdDasPbClassColumn> adDasPbClassColumns = adDasPbClassColumnRepository
                            .query(finalCurPbVersion, pbClass);

                    List<AdDasPbClassColumn> adDasPbClassColumnsNew = Lists.newArrayList();
                    adDasPbClassColumns.forEach(adDasPbClassColumn -> {
                        AdDasPbClassColumn adDasPbClassColumnNew = new AdDasPbClassColumn();
                        AdDasBeanCoreUtils.copyPropertiesIgnoreNull(adDasPbClassColumnNew, adDasPbClassColumn);
                        adDasPbClassColumnNew.setId(null);
                        adDasPbClassColumnNew.setPbVersion(pbJarVersion);
                        adDasPbClassColumnNew.setOperator(adDasUserView.getUserName());
                        adDasPbClassColumnNew.setCreateTime(timestamp);
                        adDasPbClassColumnNew.setUpdateTime(timestamp);

                        adDasPbClassColumnsNew.add(adDasPbClassColumnNew);
                    });

                    log.info("updatePbJar() adDasPbClassColumnsNew={}", adDasPbClassColumnsNew);
                    adDasPbClassColumnRepository.batchInsert(adDasPbClassColumnsNew);
                });
            }
        }

        // 写入 ad_das_pb
        AdDasPb adDasPb = new AdDasPb();
        adDasPb.setOperator(adDasUserView.getUserName());
        adDasPb.setPbVersion(pbJarVersion);
        adDasPb.setCreateTime(System.currentTimeMillis());
        adDasPb.setUpdateTime(System.currentTimeMillis());
        log.info("updatePbJar() adDasPb={}", adDasPb);
        adDasPbRepository.insert(adDasPb);

    }

    @Override
    public AdDasSchemaPbDetailView addPbClass(AdDasUserView adDasUserView, String pbJarVersion, String pbClassName) {
        checkUser(adDasUserView);
        // check pb class 是否在指定的pb 包中
        checkClassWithPbJar(pbJarVersion, pbClassName);
        // check
        List<AdDasPbClass> adDasPbClasses = adDasPbClassRepository
                .queryByPbClass(pbJarVersion, pbClassName, null);
        log.info("addPbClass() adDasPbClasses={}", adDasPbClasses);
        if (!CollectionUtils.isEmpty(adDasPbClasses)) {
            AdDasPbClass adDasPbClass = adDasPbClasses.get(0);
            if (adDasPbClass.getPbClassStatus() == 1) {
                throw AdDasServiceException.ofMessage(DATA_EXISTED_ERROR.getCode(), DATA_EXISTED_ERROR.getMsg());
            } else {
                // update pb_class_status
                adDasPbClass.setPbClassStatus(1);
                adDasPbClass.setOperator(adDasUserView.getUserName());
                adDasPbClass.setUpdateTime(System.currentTimeMillis());

                adDasPbClassRepository.updateStatus(adDasPbClass);
                return convertSchemaPbDetailView(adDasPbClass);
            }
        }
        // insert
        AdDasPbClass adDasPbClassDb = new AdDasPbClass();
        adDasPbClassDb.setPbVersion(pbJarVersion);
        adDasPbClassDb.setPbClass(pbClassName);
        adDasPbClassDb.setPbClassStatus(AdDasStatusEnum.DEFAULT.getCode());
        adDasPbClassDb.setPbClassColumnCount(0);
        adDasPbClassDb.setOperator(adDasUserView.getUserName());
        adDasPbClassDb.setCreateTime(System.currentTimeMillis());
        adDasPbClassDb.setUpdateTime(System.currentTimeMillis());
        log.info("addPbClass() adDasPbClassDb={}", adDasPbClassDb);
        adDasPbClassRepository.insert(adDasPbClassDb);
        // TODO 默认将所有 pb 字段 标记入库
        List<AdDasPbClassColumn> adDasPbClassColumnsNew = Lists.newArrayList();
        List<AdDasSchemaPbClassView> columnList = Lists.newArrayList();
        try {
            // 反射解析对应的 PbClass 字段信息
            String pbClass = getPbClassPrefix() + pbClassName;
            AdDasPlatformProtoSchema adDasPlatformProtoSchema = new AdDasPlatformProtoSchema();
            adDasPlatformProtoSchema.setPbVersion(pbJarVersion);
            adDasPlatformProtoSchema.setPbEnumVersion(adDasPlatformProtoEnumVersionConfig.get());
            Class<?> clz = adDasClassLoaderHelper.loadClass(adDasPlatformProtoSchema, pbClass);
            // 反射解析指定PB版本的class 数据
            columnList = reflectPbClassView(clz);
        } catch (Exception e) {
            log.error("querySchemaPbClass() occur error! pbJarVersion={}", pbJarVersion, e);
        }
        if (!CollectionUtils.isEmpty(columnList)) {
            for (AdDasSchemaPbClassView adDasPbClass : columnList) {
                AdDasPbClassColumn adDasPbClassColumnNew = new AdDasPbClassColumn();
                adDasPbClassColumnNew.setId(null);
                adDasPbClassColumnNew.setPbVersion(pbJarVersion);
                adDasPbClassColumnNew.setPbClass(pbClassName);
                adDasPbClassColumnNew.setPbColumn(adDasPbClass.getColumnName());
                adDasPbClassColumnNew.setPbFullColumn(adDasPbClass.getColumnFullName());
                adDasPbClassColumnNew.setPbJavaType(adDasPbClass.getColumnJavaType());
                adDasPbClassColumnNew.setPbType(adDasPbClass.getColumnPbType());
                adDasPbClassColumnNew.setEnumType(adDasPbClass.getColumnEnumType());
                adDasPbClassColumnNew.setDefaultValue(adDasPbClass.getDefaultValue().toString());
                adDasPbClassColumnNew.setOperator(adDasUserView.getUserName());
                adDasPbClassColumnNew.setCreateTime(System.currentTimeMillis());
                adDasPbClassColumnNew.setUpdateTime(System.currentTimeMillis());
                adDasPbClassColumnsNew.add(adDasPbClassColumnNew);
            }
            log.info("updatePbJar() adDasPbClassColumnsNew={}", adDasPbClassColumnsNew);
            adDasPbClassColumnRepository.batchInsert(adDasPbClassColumnsNew);
            // 更新 ad_das_pb_class 表中的字段数量
            adDasPbClassRepository.updatePbClassColumnCount(pbJarVersion,
                    pbClassName, columnList.size(), adDasUserView.getUserName());
        }
        // query
        List<AdDasPbClass> adDasPbDb = adDasPbClassRepository.queryByPbClass(pbJarVersion, pbClassName,
                AdDasStatusEnum.DEFAULT.getCode());
        return adDasPbDb.stream().findFirst().map(this::convertSchemaPbDetailView).orElse(new AdDasSchemaPbDetailView());
    }

    private AdDasSchemaPbDetailView convertSchemaPbDetailView(AdDasPbClass adDasPbClass) {

        AdDasSchemaPbDetailView view = new AdDasSchemaPbDetailView();
        AdDasBeanCoreUtils.copyPropertiesIgnoreNull(view, adDasPbClass);

        return view;
    }

    @Override
    public void updateStatus(AdDasUserView adDasUserView, Long id,
                             String pbJarVersion, String pbClassName,
                             Integer status) {
        checkUser(adDasUserView);
        // check
        List<AdDasPbClass> adDasPbClasses = adDasPbClassRepository.queryByPbClass(pbJarVersion, pbClassName, null);

        if (!CollectionUtils.isEmpty(adDasPbClasses)) {
            AdDasPbClass adDasPbClass = adDasPbClasses.get(0);
            // update
            // update pb_class_status
            adDasPbClass.setPbClassStatus(status);
            adDasPbClass.setOperator(adDasUserView.getUserName());
            adDasPbClass.setUpdateTime(System.currentTimeMillis());
            adDasPbClassRepository.updateStatus(adDasPbClass);
        }
    }

    @Override
    public void updatePbClassColumn(AdDasUserView adDasUserView, AdDasPbClassColumnParam param) {

        String pbVersion = param.getPbVersion();
        String pbClassName = param.getPbClassName();

        log.info("updatePbClassColumn() adDasUserView={}, param={}", adDasUserView, param);
        // check param from ad_das_pb_class
        List<AdDasPbClass> adDasPbClasses = adDasPbClassRepository.queryByPbClass(pbVersion,
                pbClassName, AdDasStatusEnum.DEFAULT.getCode());

        log.info("updatePbClassColumn() adDasPbClasses={}", adDasPbClasses);

        if (CollectionUtils.isEmpty(adDasPbClasses)) {
            throw AdDasServiceException.ofMessage(PB_CLASS_NOT_FOUND.getCode(), PB_CLASS_NOT_FOUND.getMsg());
        }

        AdDasPbClass adDasPbClass = adDasPbClasses.get(0);

        List<AdDasPbClassColumn>  adDasPbClassColumnsOrigin = adDasPbClassColumnRepository
                .query(pbVersion, pbClassName);

        log.info("updatePbClassColumn() adDasPbClassColumnsOrigin={}", adDasPbClassColumnsOrigin);

        Map<String, AdDasPbClassColumn> pbClassColumnMap = adDasPbClassColumnsOrigin.stream()
                .collect(Collectors.toMap(AdDasPbClassColumn::getPbColumn, adDasPbClassColumn -> adDasPbClassColumn));

        List<AdDasSchemaPbClassView> pbClassColumns = param.getPbClassColumns();

        List<AdDasPbClassColumn> insertDbs = Lists.newArrayList();
        List<AdDasPbClassColumn> deleteDbs = Lists.newArrayList();

        int columnCount = adDasPbClassColumnsOrigin.size();

        //  区分新增 & 删除的数据
        for (AdDasSchemaPbClassView pbClassView : pbClassColumns) {

            String columnName = pbClassView.getColumnName();
            Integer marked = pbClassView.getMarked();
            if (marked == 0 && (!CollectionUtils.isEmpty(pbClassColumnMap) && pbClassColumnMap.containsKey(columnName))) {
                deleteDbs.add(pbClassColumnMap.get(columnName));
                columnCount --;
            }

            if (marked == 1 && (CollectionUtils.isEmpty(pbClassColumnMap)
                    || (!CollectionUtils.isEmpty(pbClassColumnMap) && !pbClassColumnMap.containsKey(columnName)))) {
                AdDasPbClassColumn adDasPbClassColumn = convertView2PbClassColumn(adDasUserView.getUserName(),
                        pbVersion,pbClassName, pbClassView);
                insertDbs.add(adDasPbClassColumn);
                columnCount ++;
            }
        }
        log.info("updatePbClassColumn() insertDbs={}, deleteDbs={}", insertDbs, deleteDbs);
        if (!CollectionUtils.isEmpty(deleteDbs)) {
            adDasPbClassColumnRepository.batchDelete(deleteDbs);
        }
        if (!CollectionUtils.isEmpty(insertDbs)) {
            adDasPbClassColumnRepository.batchInsert(insertDbs);
        }
        // 更新 ad_das_pb_class 表中的字段数量
        adDasPbClassRepository.updatePbClassColumnCount(adDasPbClass.getId(),
                adDasPbClass.getPbClass(), columnCount, adDasUserView.getUserName());
    }

    private AdDasPbClassColumn convertView2PbClassColumn(String userName, String pbVersion,
                                                         String pbClassName,
                                                         AdDasSchemaPbClassView adDasSchemaPbClassView) {
        AdDasPbClassColumn adDasPbClassColumn = new AdDasPbClassColumn();
        adDasPbClassColumn.setPbVersion(pbVersion);
        adDasPbClassColumn.setPbClass(pbClassName);
        adDasPbClassColumn.setPbFullColumn(adDasSchemaPbClassView.getColumnFullName());
        adDasPbClassColumn.setPbColumn(adDasSchemaPbClassView.getColumnName());
        adDasPbClassColumn.setPbJavaType(adDasSchemaPbClassView.getColumnJavaType());
        adDasPbClassColumn.setPbType(adDasSchemaPbClassView.getColumnPbType());
        adDasPbClassColumn.setEnumType(adDasSchemaPbClassView.getColumnEnumType());
        adDasPbClassColumn.setDefaultValue(adDasSchemaPbClassView.getDefaultValue().toString());
        adDasPbClassColumn.setOperator(userName);
        adDasPbClassColumn.setCreateTime(System.currentTimeMillis());
        adDasPbClassColumn.setUpdateTime(System.currentTimeMillis());
        return adDasPbClassColumn;
    }

}
