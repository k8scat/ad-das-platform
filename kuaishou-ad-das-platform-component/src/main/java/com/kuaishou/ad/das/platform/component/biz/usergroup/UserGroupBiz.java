package com.kuaishou.ad.das.platform.component.biz.usergroup;

import java.util.List;

import com.kuaishou.ad.das.platform.component.web.param.StreamUserGroupEditParam;
import com.kuaishou.ad.das.platform.dal.dao.AdDasUserGroupBindCondition;

/**
 * @author Zhang Jialin <zhangjialin@kuaishou.com>
 * Created on 2021-06-11
 */
public interface UserGroupBiz {
    /**
     * 编辑用户组
     * @param param 新建/修改参数
     */
    void editUserGroup(StreamUserGroupEditParam param);

    /**
     * 查询用户组和绑定关系
     * @param bindCondition 查询条件
     * @return 用户组和绑定关系列表
     */
    List<UserGroupWithBindDTO> queryUserGroupAndBind(AdDasUserGroupBindCondition bindCondition);
}
