package com.kuaishou.ad.das.platform.component.web.view.common;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author Zhang Jialin <zhangjialin@kuaishou.com>
 * Created on 2021-05-27
 */
@Data
public class TableItemView {
    @ApiModelProperty(value = "数据源名称")
    private String dataSourceName;
    @ApiModelProperty(value = "表名称")
    private String tableName;
}
